<?php

/**
 * Fired when the plugin is uninstalled.
 *
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 */

// If uninstall not called from WordPress, then exit.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}
 
global $wpdb;
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}a_inputfeld_md");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}md_trainingsmgr_bewertungen");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}md_trainingsmgr_certificate");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}md_trainingsmgr_coach2user");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}md_trainingsmgr_lastlogin");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}md_trainingsmgr_permission_function");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}md_trainingsmgr_permission_role");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}md_trainingsmgr_permission_role2function");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}md_trainingsmgr_registerkey");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}md_trainingsmgr_training_logs");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}md_trainingsmgr_tranings_date");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}md_trainingsmgr_tranings_dependency");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}md_trainingsmgr_tranings_lektionen");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}md_trainingsmgr_tranings_nachrichten");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}md_trainingsmgr_tranings_seiten");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}md_trainingsmgr_tranings_user");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}md_trainingsmgr_ts_exercise");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}md_trainingsmgr_ts_forms_data");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}md_trainingsmgr_ts_forms_fields");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}md_trainingsmgr_user_events");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}md_trainingsmgr_user_logs");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}user_grouper_plugin_firma");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}user_grouper_plugin_gruppe");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}user_grouper_plugin_gruppe_two_user");

$wpdb->query("DELETE FROM {$wpdb->options} WHERE `option_name` LIKE '%tspv2%'");

$res = $wpdb->get_results("SELECT * FROM {$wpdb->posts} WHERE post_type = 'lektionen' or post_type = 'trainings' or post_type = 'seiten' or post_type = 'formulare' or post_type = 'uebungen' or post_type = 'abzeichen' or post_type = 'zertifikate' or post_type = 'avatare' or post_type = 'appjournalentry' or post_type = 'nachrichten'");
$pageids = array();
foreach($res as $page)
{
   $pageids[] = $page->ID;
}
$ids = implode("','", $pageids);

$wpdb->query("DELETE FROM {$wpdb->posts} WHERE post_parent IN ('{$ids}')");

$wpdb->query("DELETE FROM {$wpdb->posts} WHERE post_type = 'lektionen' or post_type = 'trainings' or post_type = 'seiten' or post_type = 'formulare' or post_type = 'uebungen' or post_type = 'abzeichen' or post_type = 'zertifikate' or post_type = 'avatare' or post_type = 'appjournalentry' or post_type = 'nachrichten'");

$wpdb->query("DELETE FROM {$wpdb->posts} WHERE post_content LIKE '%[tspv2_%]%'");

$wpdb->query("DELETE FROM {$wpdb->usermeta} WHERE 
												meta_key ='studienid' or 
												meta_key = 'coachid' or 
												meta_key = 'pAdminId' or 
												meta_key = 'kAdminId' or 
												meta_key = 'dAdminId' or 
												meta_key = 'pCoachId' or 
												meta_key = 'notification-send-for-unread-messages' or 
												meta_key = 'mailbox_notifications' or
												meta_key = '_avatar_url' or
												meta_key = 'age' or
												meta_key = 'gender' or
												meta_key = 'place' or
												meta_key = 'coach_select_user' or
												meta_key = 'gender' or
												meta_key = 'tspv2_tspv2UserHasRoles' or
												meta_key = 'coaching_needed' or
												meta_key = 'coaching_mode' or
												meta_key = 'forward_content' or
												meta_key = 'can_save' or
												meta_key LIKE 'limesurvey%' or
												meta_key = 'studiengruppenid' or
												meta_key = 'team_leader' or
												meta_key = 'phonenumber' or
												meta_key = 'created_by' or
												meta_key = 'tspv2_my_avatar' or
												meta_key = 'coaching_notes' or
												meta_key = 'userInactivity' or
												meta_key = 'demo_user'
												");

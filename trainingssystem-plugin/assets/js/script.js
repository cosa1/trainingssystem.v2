//####################neu###############################
function dontNeedToSaveTrainings(){
	// Training Mgr
	jQuery( "#coach-user-savetraining" ).removeClass( "btn-info" )
	jQuery('#coach-user-savetraining .fa-save').addClass('d-none');
	// Vorlagen
	jQuery( "#savetrainingvorlagenuser" ).removeClass( "btn-info" )
	jQuery('#savetrainingvorlagenuser .fa-save').addClass('d-none');
}

function getTrainingByUser(id) {

	var ajaxurl = typeof trainingssystem_plugin_v2_ajax !== 'undefined' ? trainingssystem_plugin_v2_ajax.trainingssystem_plugin_v2_ajax_url : null;
	
	jQuery('#training-mgr-loading').html('<span class="userfeedback-loading"><i class="fas fa-spinner"></i></span>');
	jQuery('#training-mgr-notrainings').remove();
	jQuery('#sortable-user-trainings').html('');

	jQuery('.cleartable').attr("disabled", true);
	jQuery('#coach-user-savetraining, #coach-user-savetraining-date, #coach-user-addtrainingbutton, #savetrainingvorlagenuser, #vorlagenuseraddtrainingbutton').attr("disabled", true);

	if(id != undefined) {
		jQuery('#training-mgr-loading').removeClass('d-none');
		
		jQuery.ajax({
			type: 'POST',
			url: ajaxurl,
			data: {
				"action": "coach_user_training_mgr_gettraining_user",
				"userid": id,
			},
			success: function (data) {
				jQuery('#training-mgr-loading').addClass('d-none');
				jQuery('.cleartable').attr("disabled", false);
				jQuery('.vorlagenusertable tr.vorlagenuser-row, .vorlagenusertable tr.vorlagenuser-row td').addClass("cursor-pointer").removeClass("pointerdisabled");
				if(data == "") {
					jQuery('#sortable-user-trainings').html('');
				} else {
					dontNeedToSaveTrainings();
					jQuery("#training-mgr-info-modal").remove();
					jQuery("#training-mgr-date-modal").remove();
					jQuery("#sortable-user-trainings").append(data);
					jQuery("#training-mgr-info-modal").appendTo(jQuery("#sortable-user-trainings").parent());
					jQuery("#training-mgr-date-modal").appendTo(jQuery("#sortable-user-trainings").parent());
					var str = "";
					jQuery('#sortable-user-trainings').find('li:not(.noSortable)').each(function(e) {
						str += jQuery(this).attr('value') + '-';
					})
					jQuery('#sortable-user-trainings').attr('orderstring', str);
					jQuery(".connectedSortable").sortable({items: "li:not(.noSortable)"}).disableSelection();
					if(data.indexOf("coach-user-training-mgr-missing-dependent-training") > 0) {
						jQuery('#coach-user-training-mgr-deletedDependentTrainingWarning').removeClass("d-none");
					}
					jQuery('#coach-user-savetraining, #coach-user-savetraining-date, #coach-user-addtrainingbutton, #savetrainingvorlagenuser, #vorlagenuseraddtrainingbutton').attr("disabled", false);
				}
			},
			error: function (errorThrown) {
				//alert('error');
				jQuery('#training-mgr-loading').html('<span class="userfeedback-error"><i class="far fa-times-circle"></i></span>');
				console.error(errorThrown);
			}
		});
	}
}

(function ($) {
	jQuery(document).ready(function ($) {

		userFeedback("test5", "loading");
		var ajaxurl = typeof trainingssystem_plugin_v2_ajax !== 'undefined' ? trainingssystem_plugin_v2_ajax.trainingssystem_plugin_v2_ajax_url : null;

		//coach user trainings mgr ################################################

		$("#sortable-user-trainings").sortable().disableSelection();
		jQuery('#sortable-user-trainings').on('sortupdate', function (e, ui) {
			var newstr = "";
			jQuery('#sortable-user-trainings').find('li:not(.noSortable)').each(function(e) {
				newstr += jQuery(this).attr('value') + '-';
			})

			if(newstr != jQuery('#sortable-user-trainings').attr('orderstring')) {
				needToSaveTrainings();
			} else {
				// Entscheidung: lieber einmal zu oft speichern oder eine Änderung nicht anzeigen
				// hier: speichern
				//dontNeedToSaveTrainings();
			}
		})
		
		$("#sortable-user-trainings").sortable({items: "li:not(.noSortable)"}).disableSelection();
		$("#sortable-user-groups").sortable({items: "li:not(.noSortable)"}).disableSelection();

		$(".connectedSortable").sortable({items: "li:not(.noSortable)"}).disableSelection();

		function countTrainingLektion() {
			jQuery("#sortable-user-trainings > li").each(function (index) {
				let linecount = $(this).find(".connectedSortable li").length;
				let oldstring = $(this).find(".morelessons span").first().text();
				oldstring = oldstring.replace(/[0-9]/g, '');
				$(this).find(".morelessons span").html(linecount + " " + oldstring);
			});
		}

		function showGroupsForCompany(id) {
			jQuery('#company-grouplist').removeClass('d-none');
			jQuery('#sortable-user-groups').html('');
			jQuery('#company-mgr-nogroups').addClass('d-none');
			jQuery('#company-mgr-loading').html('<span class="userfeedback-loading"><i class="fas fa-spinner"></i></span>');
			jQuery('#company-mgr-loading').removeClass('d-none');
			
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					"action": "company_mgr_getgroups_company",
					"companyid": id,
				},
				success: function (data) {
					console.log(data);
					jQuery('#company-mgr-loading').addClass('d-none');
					if(data != "") {
						jQuery("#sortable-user-groups").html(data);
						jQuery('#company-mgr-nogroups').addClass('d-none');
						jQuery(".connectedSortable").sortable().disableSelection();
					} else {
						jQuery('#company-mgr-nogroups').removeClass('d-none');
						jQuery('#sortable-user-groups').html('');
					}
				},
				error: function (errorThrown) {
					console.error(errorThrown);
					jQuery('company-mgr-loading').html('<span class="userfeedback-error"><i class="far fa-times-circle"></i></span>');
				}
			});
		}


		// Gruppen zu Firma anzeigen

		jQuery(document).on('click', '.company-mgr-table tr.company-mgr-table-row', function(e) {
			if(!jQuery(this).hasClass('active')) {
				jQuery('.company-mgr-table tr.company-mgr-table-row').removeClass('active');
				jQuery(this).addClass('active');

				var id = jQuery(this).attr('companyid');
				var name = jQuery(this).find(".company-name").attr('value');
				var bossid = jQuery(this).find(".company-boss-id").attr('value');

				jQuery("#selected-company-id").val(id);
				jQuery("#selected-company-name").val(name);
				jQuery("#selected-company-bossid").val(bossid);
				showGroupsForCompany(id);
			}
		});

		jQuery("#modal-creategroup-input-name-search").on("change paste keyup", function(e) {
			setTimeout(function(){ 
				newGroupUsers();	
			}, 500);
		  });

		jQuery(document).on('click', '#clear-company-newgroup-button', function (e) {
			jQuery('#modal-creategroup-input-name-search').val('');
			newGroupUsers();
		});

		function newGroupUsers() {
						
			var tmps = ""+jQuery('#modal-creategroup-input-name-search').val().toLowerCase();
			console.log(tmps);
				
			//console.log(jQuery(this).val());
			jQuery('#modal-creategroup-check-members div').removeClass("d-flex").addClass("d-none");
	
			jQuery('#modal-creategroup-check-members div label').each(function (e) {
			if(jQuery(this).html().toLowerCase().indexOf(tmps) >= 0) {
				jQuery(this).parent().removeClass("d-none").addClass("d-flex");
			}
			});
		}

		jQuery('#modal-creategroup-input-name').focusout(function (e) {
			if(jQuery('#modal-creategroup-input-name').val().trim() == "") {
				jQuery('#modal-creategroup-input-name').parent().find('input').first().addClass('is-invalid');
				jQuery('#modal-creategroup-input-name').parent().find('.invalid-feedback').first().removeClass('d-none');
			} else {
				jQuery('#modal-creategroup-input-name').parent().find('input').first().removeClass('is-invalid');
				jQuery('#modal-creategroup-input-name').parent().find('.invalid-feedback').first().addClass('d-none');
			}
		})

		jQuery(document).on('click', '.creategroup-dialog', function (e) {
			var companyId = jQuery("#selected-company-id").val();
			var companyName = jQuery("#selected-company-name").val();
			jQuery('#modal-creategroup-check-members input:checkbox').prop('checked', false);
			jQuery('#create-group-company-name').html(companyName);
		});

		jQuery(document).on('submit', '#createGroupForm', function (e) {
			e.preventDefault();
			jQuery('.modal-creategroup').click();
		})

		jQuery(document).on('click', '.modal-creategroup', function (e) {
			jQuery('#modal-creategroup-input-name').parent().find('input').first().removeClass('is-invalid');
			jQuery('#modal-creategroup-input-name').parent().find('.invalid-feedback').first().addClass('d-none');

			var groupName = jQuery('#modal-creategroup-input-name').val().trim();
			var companyId = jQuery("#selected-company-id").val();

			if(groupName != "" && companyId != "") {
				userFeedback('company-creategroup-button', 'loading');

				var members = [];
				// jQuery("#modal-creategroup-select-members").find("option:selected").each(function (e) {members.push(jQuery(this).val())});
				jQuery("#modal-creategroup-check-members").find("input:checked").each(function (e) {members.push(jQuery(this).val())});

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "company_mgr_create_group",
						"companyId": companyId,
						"groupName": JSON.stringify(groupName),
						"members": JSON.stringify(members)
					},
					success: function (data) {
						console.log(data);
						userFeedback('company-creategroup-button', 'success');
						showGroupsForCompany(companyId);
						setTimeout(() => {
							jQuery('#createGroupModal').modal('hide');
							jQuery('#modal-creategroup-input-name').val('');
							jQuery('#modal-creategroup-input-name-search').val('');
							newGroupUsers();
							userFeedback('company-creategroup-button', 'text');
						}, 500);
					},
					error: function (errorThrown) {
						console.error(errorThrown);
						userFeedback('company-creategroup-button', 'error');
						setTimeout(() => {
							userFeedback('company-creategroup-button', 'text');
						}, 1500);
					}
				});
			} else {
				if(groupName == "") {
					jQuery('#modal-creategroup-input-name').parent().find('input').first().addClass('is-invalid');
					jQuery('#modal-creategroup-input-name').parent().find('.invalid-feedback').first().removeClass('d-none');
				}
			}
		});

		jQuery(document).on('click', '.delete-group', function (e) {
			var groupId = jQuery(this).parent().parent().val();
			var teamleaderId = jQuery(this).parent().parent().find('.select-team-leader-input').val();

			jQuery('#group-delete-id').val(groupId);
			jQuery('#group-delete-teamleaderid').val(teamleaderId);

			var groupname = jQuery(this).parent().parent().find('.listheader-sortable .listheader-sortable-text').html();
			jQuery('#deleteGroupName').html(groupname);

			jQuery('#deleteGroupModal').modal('show');
		});

		jQuery(document).on('click', '.deletegroup-confirm', function (e) {

			var groupId = jQuery('#group-delete-id').val();
			var teamleaderId = jQuery('#group-delete-teamleaderid').val();

			if (groupId != "") {
				userFeedback('group-delete-button', 'loading');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "company_mgr_delete_group",
						"groupId": parseInt(groupId),
						"teamleaderId": parseInt(teamleaderId)
					},
					success: function (data) {
						userFeedback('group-delete-button', 'success');
						setTimeout(() => {
							jQuery('#deleteGroupModal').modal('hide');
							jQuery('#group-delete-id').val('');
							jQuery('#group-delete-teamleaderid').val('');
							jQuery('#deleteGroupName').val('');
							jQuery("#gruppe" + parseInt(groupId)).remove();
							userFeedback('group-delete-button', 'text');
							if(jQuery('#sortable-user-groups').find('li').length == 0) {
								jQuery('#company-mgr-nogroups').removeClass('d-none');
							}
						}, 500);

					},
					error: function (errorThrown) {
						console.error(errorThrown);
						userFeedback('group-delete-button', 'error');
						setTimeout(() => {
							userFeedback('group-delete-button', 'text');
						}, 1500);
					}
				});
			}
		});

		jQuery(document).on('click', '.remove-groupmember', function (e) {

			var groupId = jQuery(this).parent().parent().parent().parent().val();
			var userId = jQuery(this).parent().val();
			var teamleaderId = jQuery(this).parent().parent().parent().parent().find('.select-team-leader-input').val();

			jQuery('#groupuser-delete-userid').val(userId);
			jQuery('#groupuser-delete-groupid').val(groupId);
			jQuery('#groupuser-delete-teamleaderid').val(teamleaderId);

			var username = jQuery(this).parent().find('.company-mgr-username').html();
			var groupname = jQuery(this).parent().parent().parent().parent().find('.listheader-sortable .listheader-sortable-text').html();
			jQuery('#deleteGroupuserName').html(username);
			jQuery('#deleteGroupuserGroupn').html(groupname);

			if(userId != teamleaderId){
				jQuery('#deleteGroupuserModal').find('#deletegroupuserwarning').addClass('d-none');
			}
			else{
				jQuery('#deleteGroupuserModal').find('#deletegroupuserwarning').removeClass('d-none');
			}
			
			jQuery('#deleteGroupuserModal').modal('show');
		});

		jQuery(document).on('click', '.deletegroupuser-confirm', function (e) {
			
			var userId = jQuery('#groupuser-delete-userid').val();
			var groupId = jQuery('#groupuser-delete-groupid').val();
			var companyId = jQuery("#selected-company-id").val();
			var teamleaderId = jQuery('#groupuser-delete-teamleaderid').val();

			if (groupId != "" && userId != "") {
				userFeedback('groupuser-delete-button', 'loading');
				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "company_mgr_remove_groupmember",
						"groupId": parseInt(groupId),
						"userId": parseInt(userId),
						"teamleaderId": parseInt(teamleaderId)
					},
					success: function (data) {
						userFeedback('groupuser-delete-button', 'success');
						setTimeout(() => {
							jQuery('#deleteGroupuserModal').modal('hide');
							jQuery('#groupuser-delete-userid').val('');
							jQuery('#groupuser-delete-groupid').val('');
							jQuery('#groupuser-delete-teamleaderid').val('');
							jQuery('#deleteGroupuserName').html('');
							jQuery('#deleteGroupuserGroupn').html('');
							jQuery("#companygroup" + parseInt(groupId) + "-member" + parseInt(userId)).remove();
							userFeedback('groupuser-delete-button', 'text');
						}, 500);
						showGroupsForCompany(companyId);
					},
					error: function (errorThrown) {
						console.error(errorThrown);
						userFeedback('groupuser-delete-button', 'error');
						setTimeout(() => {
							userFeedback('groupuser-delete-button', 'text');
						}, 1500);
					}
				});
			}
		});

		jQuery("#modal-addgroupmember-check-members-search").on("change paste keyup", function(e) {
			setTimeout(function(){ 
				addGroupUsers();	
			}, 500);
		  });

		jQuery(document).on('click', '#clear-company-addmember-button', function (e) {
			jQuery('#modal-addgroupmember-check-members-search').val('');
			addGroupUsers();
		});

		function addGroupUsers() {
						
			var tmps = ""+jQuery('#modal-addgroupmember-check-members-search').val().toLowerCase();
			console.log(tmps);
			
			jQuery('#modal-addgroupmember-check-members div').removeClass("d-flex").addClass("d-none");
	
			jQuery('#modal-addgroupmember-check-members div label').each(function (e) {
			if(jQuery(this).html().toLowerCase().indexOf(tmps) >= 0) {
				jQuery(this).parent().removeClass("d-none").addClass("d-flex");
			}
			});
		}

		jQuery(document).on('submit', '#addGroupMemberForm', function (e) {
			e.preventDefault();
			jQuery('.add-groupmember').click();
		});

		jQuery(document).on('click', '.add-groupmember', function (e) {
			var groupId = jQuery(this).parent().parent().val();
			var groupName = jQuery(this).parent().parent().find('.listheader-sortable-text').html();
			console.log(groupId);
			jQuery('#modal-addgroupmember-check-members input:checkbox').prop('checked', false);
			jQuery(this).parent().find('ul.connectedSortable li').each(function (e) {
				
				jQuery('#modal-addgroupmember-check-members #defaultCheckuserA'+jQuery(this).val()).prop('checked', true);
				jQuery('#modal-addgroupmember-check-members #defaultCheckuserA'+jQuery(this).val()).prop('disabled', true);
			});
			jQuery('#addmembermodal').val(groupId);
			jQuery('#modal-addmember-groupname').html(groupName);
		});


		jQuery(document).on('click', '.modal-addgroupmember', function (e) {
			var groupId = jQuery('#addmembermodal').val();
			var companyId = jQuery("#selected-company-id").val();

			if(groupId != "" && companyId != "") {
				userFeedback('company-addgroupmember-button', 'loading');

				var members = [];
				jQuery("#modal-addgroupmember-check-members").find("input:checked").each(function (e) {members.push(jQuery(this).val())});

				// console.log(members);
				// console.log(groupId);

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "company_mgr_add_groupmember",
						"groupId": parseInt(groupId),
						"members": JSON.stringify(members)
					},
					success: function (data) {
						console.log(data);
						userFeedback('company-addgroupmember-button', 'success');
						setTimeout(() => {
							jQuery('#addGroupMemberModal input:checkbox').removeAttr('checked');
							jQuery('#modal-addgroupmember-check-members-search').val('');
							addGroupUsers();
							jQuery('#addGroupMemberModal').modal('hide');
							userFeedback('company-addgroupmember-button', 'text');
						}, 500)
						showGroupsForCompany(companyId);
					},
					error: function (errorThrown) {
						console.error(errorThrown);
						userFeedback('company-addgroupmember-button', 'error');
						setTimeout(() => {
							userFeedback('company-addgroupmember-button', 'text');
						}, 1500)
					}
				});
			}

		});

		jQuery(document).on('change', '.select-team-leader-input', function(e){
			var thisButton = jQuery(this).parent().find("button#select-team-leader-input-feedback");
			userFeedback(thisButton, 'loading');
			var companyId = jQuery("#selected-company-id").val();
			var groupId = jQuery(this).parent().parent().parent().val();
			var userId = jQuery(this).val();
			
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data:{
					"action": "company_mgr_add_teamleader",
					"groupId": parseInt(groupId),
					"userId": parseInt(userId)
				},
				success: function(data){
					if(data == "1") {
						userFeedback(thisButton, 'success');
						setTimeout(()=>{
							userFeedback(thisButton, 'text');
							showGroupsForCompany(companyId);
						}, 1500);
					} else {
						userFeedback(thisButton, 'error');
						jQuery('#select-team-leader-input').val(jQuery('#select-team-leader-input').attr('data-initialvalue'));
						setTimeout(()=>{
							userFeedback(thisButton, 'text');
						}, 1500);
					}
				},
				error: function(errorThrown){
					console.error(errorThrown);
				}
			});
		});

		var trainingMgrIndex = null;
		jQuery(document).on('focus', '#autoCompleteInput', function (e) {
			jQuery('#autoCompleteInnerDiv').removeClass('d-none');
			trainingMgrIndex = 0;
		})

		jQuery(document).on('click', function (e) {
			if (!jQuery(e.target).closest('#training-mgr-user-side').length) {
				trainingMgrIndex = null;
				jQuery('#autoCompleteInnerDiv').addClass('d-none');
			}
		})

		jQuery(document).on('keydown', '#autoCompleteInput', function (e) {
			if(e.which == 38) {
				e.preventDefault();

				if(trainingMgrIndex != 0 && trainingMgrIndex != null) {
					if(trainingMgrIndex == 1) {
						
					} else {
						deactivateAllUser();
						trainingMgrIndex--;
						var i = 1;
						jQuery('#autoCompleteInnerDiv .list-group-item.d-flex:not(.disabled)').each(function(e) {
							if(i == trainingMgrIndex) {
								jQuery(this).addClass('active');
							}
							i++;
						})
						
						var indexActive = jQuery('#autoCompleteInnerDiv .list-group-item.d-flex.active').index();
						jQuery('#autoCompleteInnerDiv').animate({ 
							scrollTop: (indexActive-4)*jQuery('#autoCompleteInnerDiv .list-group-item.d-flex').outerHeight()
						}, 100);
						
					}
				}		
			} else if(e.which == 40) {
				e.preventDefault();

				var length = jQuery('#autoCompleteInnerDiv ul li.list-group-item.d-flex:not(.disabled)').length;
				if(trainingMgrIndex != null) {
					if(trainingMgrIndex == length) {
						
					} else {
						deactivateAllUser();
						trainingMgrIndex++;
						var i = 1;
						jQuery('#autoCompleteInnerDiv .list-group-item.d-flex:not(.disabled)').each(function(e) {
							if(i == trainingMgrIndex) {
								jQuery(this).addClass('active');
							}
							i++;
						})
						
						var indexActive = jQuery('#autoCompleteInnerDiv .list-group-item.d-flex.active').index();
						jQuery('#autoCompleteInnerDiv').animate({ 
							scrollTop: (indexActive-4)*jQuery('#autoCompleteInnerDiv .list-group-item.d-flex').outerHeight()
						}, 100);
						
					}
				}	
			} else if(e.which == 13) {
				e.preventDefault();
				if(jQuery('#autoCompleteInnerDiv .list-group-item.active').length == 1) {
					jQuery('#autoCompleteInnerDiv .list-group-item.active').click();
					jQuery('#training-mgr-clearsearch').click();
				}
			}
		})

		jQuery(document).on('input', '#autoCompleteInput', function (e) {
			deactivateAllUser();
			jQuery('#trainingMgrNoResults').remove();
			trainingMgrIndex = 0;
			jQuery('#autoCompleteInnerDiv').animate({ 
				scrollTop: 0
			}, 100);
			filterUser();
		})

		function filterUser() {
			var search = jQuery('#autoCompleteInput').val().toLowerCase();
			jQuery('#autoCompleteInnerDiv').find('.list-group-item').addClass('d-none').removeClass('d-flex');

			jQuery('#autoCompleteInnerDiv').find('.list-group-item').each(function (e) {
				var mail = jQuery(this).attr('data-usermail');
				var username = jQuery(this).attr('data-username');
				var studienid = jQuery(this).attr('data-studienid');
				var studiengruppenid = jQuery(this).attr('data-studiengruppe');
				var companyname = jQuery(this).attr('data-companyname');

				if(mail != undefined && mail.toLowerCase().indexOf(search) >= 0 ||
					username != undefined && username.toLowerCase().indexOf(search) >= 0 ||
					studienid != undefined && studienid.toLowerCase().indexOf(search) >= 0 ||
					studiengruppenid != undefined && studiengruppenid.toLowerCase().indexOf(search) >= 0 ||
					companyname != undefined && companyname.toLowerCase().indexOf(search) >= 0) {
					
					jQuery(this).addClass('d-flex').removeClass('d-none')
				}
			})

			if(jQuery('#autoCompleteInnerDiv').find('.list-group-item.d-flex').length == 1) {
				jQuery('#autoCompleteInnerDiv').find('.list-group-item.d-flex').addClass('active')
			}

			if(jQuery('#autoCompleteInnerDiv').find('.list-group-item.d-flex').length == 0) {
				jQuery('#autoCompleteInnerDiv ul').append('<li class="list-group-item list-group-item-info" id="trainingMgrNoResults">Es wurden keine Ergebnisse zu Ihrer Suche gefunden.</li>');
			}
		}

		function deactivateAllUser() {
			jQuery('#autoCompleteInnerDiv').find('.list-group-item').removeClass('active');
		}

		jQuery(document).on('click', '#training-mgr-clearsearch', function (e) {
			jQuery('#autoCompleteInput').val('');
			deactivateAllUser();
			filterUser();
			jQuery('#trainingMgrNoResults').remove();
			trainingMgrIndex = 0;
			jQuery('#autoCompleteInnerDiv').animate({ 
				scrollTop: 0
			}, 100);
		})

		jQuery(document).on('click', '.addstudiengruppe', function (e) {
			var sg_name = jQuery(this).attr('data-studiengruppe');
			var tn_ids = jQuery(this).attr('data-studiengruppenteilnehmer');

			jQuery(this).attr('disabled', true).addClass('disabled');

			var ids = tn_ids.split(",");

			for(var i = 0; i < ids.length; i++) {
				jQuery('#autoCompleteInnerDiv .list-group-item[data-userid="' + ids[i] + '"]').attr('data-studiengruppe', sg_name);
				jQuery('#autoCompleteInnerDiv .list-group-item[data-userid="' + ids[i] + '"]').click();
			}
		})

		jQuery(document).on('click', '.addcompanytotrainingmgr', function (e) {
			var company_name = jQuery(this).attr('data-companyname');
			var company_id = jQuery(this).attr('data-companyid');
			var company = jQuery(this).attr('data-company');
			var tn_ids = jQuery(this).attr('data-companymember');

			if(company_id != undefined && company != undefined && company) {
				jQuery('#autoCompleteInnerDiv .list-group-item[data-companyid="' + company_id + '"]').attr('disabled', true).addClass('disabled');
			}

			jQuery(this).attr('disabled', true).addClass('disabled');

			var ids = tn_ids.split(",");

			for(var i = 0; i < ids.length; i++) {
				jQuery('#autoCompleteInnerDiv .list-group-item[data-userid="' + ids[i] + '"]').attr('data-companyname', company_name);
				jQuery('#autoCompleteInnerDiv .list-group-item[data-userid="' + ids[i] + '"]').click();
			}
		})

		jQuery(document).on('click', '.adduser', function(e) {
			deactivateAllUser();

			var name = jQuery(this).attr('data-username');
			var id = jQuery(this).attr('data-userid')
			var mail =  jQuery(this).attr('data-usermail');
			var studienid = jQuery(this).attr('data-studienid');
			var studiengruppe = jQuery(this).attr('data-studiengruppe');
			var companyname = jQuery(this).attr('data-companyname');

			if(studiengruppe == undefined) {
				studiengruppe = "";
			}
			if(companyname == undefined) {
				companyname = "";
			}
		  
			if(name != "" && name != undefined && id != "" && id != undefined && studienid != undefined)
			{
				if(!mail) {
					mail = "-";
				}
				jQuery('#autoCompleteInnerDiv .list-group-item[data-userid="' + id + '"]').attr('disabled', true).addClass('disabled');

				jQuery('#trainingmgr-select-user-first').parent().remove();

				jQuery('#autoCompleteInnerDiv').addClass('d-none');
				jQuery('#autoCompleteInput').blur();

				jQuery(".custom-table tbody").first().append("<tr class='custom-table-row-body'><th scope='row'>"+studienid+"</th><th scope='row'>"+studiengruppe+"</th><th scope='row'>"+companyname+"</th><td valueid='"+id+"'>"+name+"</td><td>"+mail+"</td><td><i class='far fa-minus-square custom-table-remove-item'></i></td><td>&nbsp;</td></tr>");
				
				if(jQuery(".custom-table tbody").find("tr").length > 1) {
					jQuery('.custom-table tbody tr').first().find("td").last().html("<span class=\"badge badge-secondary\">Main</span>");
			
					jQuery("#master-alert-text").removeClass("d-none");
				}
				if(jQuery(".custom-table tbody").find("tr").length == 1) {
					getTrainingByUser(jQuery('.custom-table tbody tr').first().find("td").first().attr("valueid"));
				}
			}
		});

		function checkStudiengruppenDisabled() {
			jQuery('.addstudiengruppe.disabled').each(function (e) {
				var in_use = false;

				var tn_ids = jQuery(this).attr('data-studiengruppenteilnehmer');
				var ids = tn_ids.split(",");

				for(var i = 0; i < ids.length; i++) {
					if(jQuery('.custom-table tbody td[valueid="' + ids[i] + '"]').length > 0) {
						in_use = true;
					}
				}
				if(!in_use) {
					jQuery(this).attr('disabled', false).removeClass('disabled');
				}
			})
		}

		function checkCompanyDisabled() {
			jQuery('.addcompanytotrainingmgr.disabled').each(function (e) {
				var in_use = false;

				var tn_ids = jQuery(this).attr('data-companymember');
				var ids = tn_ids.split(",");

				for(var i = 0; i < ids.length; i++) {
					if(jQuery('.custom-table tbody td[valueid="' + ids[i] + '"]').length > 0) {
						in_use = true;
					}
				}
				if(!in_use) {
					jQuery(this).attr('disabled', false).removeClass('disabled');
				}
			})
		}


		//nutzer löschen
		jQuery(document).on('click', '.custom-table-remove-item', function (e) {
			jQuery('#coach-user-training-mgr-deletedDependentTrainingWarning').addClass("d-none");
			var tablerow2del = jQuery(this).parent().parent();
			var user2del = tablerow2del.find("td").first().attr("valueid");
			jQuery('#autoCompleteInnerDiv .list-group-item[data-userid="' + user2del + '"]').attr('disabled', false).removeClass('disabled');

			var firstuser = jQuery('.custom-table tbody tr').first().find("td").first().attr("valueid");
			if(firstuser == user2del) {
				var newfirstrow = jQuery('.custom-table tbody tr:nth-child(2)');
				newfirstrow.find("td").last().html("<span class=\"badge badge-secondary\">Main</span>");
				tablerow2del.remove();
				
				jQuery('#sortable-user-trainings').html('');
				var userid = jQuery('.custom-table tbody tr').first().find("td").first().attr("valueid");
				getTrainingByUser(userid);
			} else {
				tablerow2del.remove();
			}
			
			checkStudiengruppenDisabled();
			checkCompanyDisabled();

			if(jQuery(".custom-table tbody tr").length <= 1) {
				jQuery(".custom-table tbody tr").first().find("td").last().html("&nbsp;");
				jQuery("#master-alert-text").addClass("d-none");
			}
			
			if(jQuery(".custom-table tbody tr").length == 0) {
				jQuery('#coach-user-savetraining, #coach-user-savetraining-date, #coach-user-addtrainingbutton').attr("disabled", true);
				jQuery(".custom-table tbody").html('<tr><td id="trainingmgr-select-user-first" colspan="6"><div class="alert alert-info" role="alert"><h4 class="m-0 mb-2"><i class="fas fa-info-circle"></i>&nbsp;Info</h4><p class="mb-0">Wählen Sie Nutzer über die Suche aus. Verwenden Sie hierfür nach dem Klicken in das Textfeld die Pfeiltasten <i class="fas fa-caret-square-up d-inline"></i> und <i class="fas fa-caret-square-down"></i> und Enter oder scrollen und klicken Sie auf den ensprechenden Nutzer, um eine Auswahl zu treffen.</p></div></td></tr>');
			}
		});

		
		function needToSaveTrainings(){
			$( "#coach-user-savetraining" ).removeClass( "btn-info" )
			$('#coach-user-savetraining .fa-save').removeClass('d-none');
			$( "#coach-user-savetraining" ).addClass( "btn-info" )
			// Vorlagen
			$( "#savetrainingvorlagenuser" ).removeClass( "btn-info" )
			$('#savetrainingvorlagenuser .fa-save').removeClass('d-none');
			$( "#savetrainingvorlagenuser" ).addClass( "btn-info" )
			// $('html,body').animate({
			// 	scrollTop: $("#coach-user-savetraining").offset().top},
			// 	'fast');
		}
		dontNeedToSaveTrainings();

		jQuery('.ts-frontend-list').change(function (e) {
			needToSaveTrainings();
		});
		jQuery(document).on('click', '.cleartable', function (e) {
			dontNeedToSaveTrainings();
			jQuery('.custom-table-row-body').remove();
			jQuery(".custom-table tbody").html('<tr><td id="trainingmgr-select-user-first" colspan="6"><div class="alert alert-info" role="alert"><h4 class="m-0 mb-2"><i class="fas fa-info-circle"></i>&nbsp;Info</h4><p class="mb-0">Wählen Sie Nutzer über die Suche aus. Verwenden Sie hierfür nach dem Klicken in das Textfeld die Pfeiltasten <i class="fas fa-caret-square-up d-inline"></i> und <i class="fas fa-caret-square-down"></i> und Enter oder scrollen und klicken Sie auf den ensprechenden Nutzer, um eine Auswahl zu treffen.</p></div></td></tr>');
			jQuery("#master-alert-text").addClass("d-none");
			jQuery('#sortable-user-trainings').html('');
			jQuery('#coach-user-savetraining, #coach-user-savetraining-date, #coach-user-addtrainingbutton').attr("disabled", true);
			jQuery('#autoCompleteInput').val('');
			deactivateAllUser();
			filterUser();
			jQuery('#autoCompleteInnerDiv .list-group-item').removeClass('disabled').attr('disabled', false);
		});

		jQuery(document).on('click', '.morelessons', function (e) {
			jQuery(this).next().fadeToggle(150);
			jQuery(this).children().next().toggle();
		});

		var setTempRemove;
		jQuery(document).on('click', '.clear-user-training i', function (e) {
			var selected_training = jQuery(this).parent().parent().prev().html();
			setTempRemove = jQuery(this).parent().parent().parent().parent().attr('id');
			jQuery('#removeTrainingName').html(selected_training);
		});


		jQuery(document).on('click', '.training-remove-confirm', function (e) {
			checkForMissingDependentTraining(jQuery('#'+setTempRemove).attr("value"));
			needToSaveTrainings()
			jQuery('#removeTrainingModal').modal('hide')
			if(jQuery("#"+setTempRemove + " .coach-user-training-mgr-forward").is(':checked')) {
				jQuery("#coach-user-training-mgr-forward-none").attr("checked", true);
			}
			jQuery("#"+setTempRemove).remove();

			if(jQuery("#sortable-user-trainings .ui-state-default:not(.noSortable)").length === 0) {
				jQuery("#sortable-user-trainings .forward-training-none").addClass("d-none");
			}

			jQuery('#coach-user-training-mgr-training-unique-alert').addClass('d-none');
		});

		//remove Lektion from Training backend
		var setTempLektionRemove;
		jQuery(document).on('click', '.clear-training-lektion i', function (e) {
			var selected_lektion = jQuery(this).parent().parent().prev().html();
			setTempLektionRemove = jQuery(this).parent().parent().parent().parent().attr('id');
			jQuery('#removeLektionName').html(selected_lektion);
			jQuery('#removeLektionTrainingName').html(jQuery('#title').val());
			jQuery('#removeLektionModal').modal('show');
		});

		jQuery(document).on('click', '.lektion-remove-confirm', function (e) {
			let modalBtn = jQuery(this);
			userFeedback(modalBtn, 'loading');

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					"action": "backend_trainings_mgr_delete_lektion",
					"lektionId": jQuery("#"+setTempLektionRemove).val(),
					"trainingId": jQuery("#post_ID").val(),
				},
				success: function (data) {
					if(data == "1") {
						userFeedback(modalBtn, 'success');
						setTimeout(() => {
							userFeedback(modalBtn, 'text');
							jQuery('#removeLektionModal').modal('hide')
							jQuery("#"+setTempLektionRemove).remove();
						}, 1500);
					} else {
						userFeedback(modalBtn, 'error');
						setTimeout(() => {
							userFeedback(modalBtn, 'text');
						}, 1500);
					}
				},
				error: function (e) {
					console.error(e);
					userFeedback(modalBtn, 'error');
					setTimeout(() => {
						userFeedback(modalBtn, 'text');
					}, 1500);
				}
			});
		});

		// remove seite from backend training mgr
		var setTempSeiteRemove;
		jQuery(document).on('click', '.backend-training-mgr-remove-seite', function (e) {
			let selected_seite = jQuery(this).prev().html();
			setTempSeiteRemove = jQuery(this).parent().attr('id');
			jQuery('#removeSeitenName').html(selected_seite);
			jQuery('#removeSeiteTrainingName').html(jQuery('#title').val());
			jQuery('#removeSeitenModal').modal('show');
		});

		jQuery(document).on('click', '#seiten-remove-button', function(e) {
			let modalBtn = jQuery(this);
			userFeedback(modalBtn, 'loading');

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					"action": "backend_trainings_mgr_delete_seite",
					"seitenId": jQuery("#"+setTempSeiteRemove).val(),
					"lektionId": jQuery("#"+setTempSeiteRemove).attr("data-lektion-id"),
					"trainingId": jQuery("#post_ID").val(),
				},
				success: function (data) {
					if(data == "1") {
						userFeedback(modalBtn, 'success');
						setTimeout(() => {
							userFeedback(modalBtn, 'text');
							jQuery('#removeSeitenModal').modal('hide')
							jQuery("#"+setTempSeiteRemove).remove();
							countTrainingLektion();
						}, 1500);
					} else {
						userFeedback(modalBtn, 'error');
						setTimeout(() => {
							userFeedback(modalBtn, 'text');
						}, 1500);
					}
				},
				error: function (e) {
					console.error(e);
					userFeedback(modalBtn, 'error');
					setTimeout(() => {
						userFeedback(modalBtn, 'text');
					}, 1500);
				}
			});
		})

		jQuery(document).on('click', '.trash-button-company i', function (e) {
			e.stopPropagation();
			var selected_company_id = jQuery(this).parent().parent().attr('companyid');
			//var company_name = jQuery(this).parent().parent().find('.company-name').first().attr('value');
			jQuery("#selected-company-id").val(selected_company_id);
			var companyname = jQuery(this).parent().parent().find('.company-name').html();
			console.log(companyname);
			jQuery('#deleteCompanyName').html(companyname);
			//jQuery("#selected-company-name").val(company_name);

			//jQuery(this).parent().parent().remove();

		});
		jQuery(document).on('click', '.deletecompany-confirm', function (e) {
			var companyId = jQuery("#selected-company-id").val();
			if (companyId != "") {
				userFeedback('company-delete-button', 'loading');
				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "company_mgr_delete_company",
						"companyId": companyId
					},
					success: function (data) {
						userFeedback('company-delete-button', 'success');
						jQuery("#row-company-" + companyId).remove();
						setTimeout(() => {
							jQuery('#deleteCompanyModal').modal('hide');
							jQuery('#deleteCompanyName').html('');
							jQuery("#selected-company-id").val('');
							jQuery("#selected-company-name").val('');
							jQuery("#selected-company-bossid").val('');
							userFeedback('company-delete-button', 'text');
						}, 1500);
						location.reload();
					},
					error: function (errorThrown) {
						console.error(errorThrown);
						userFeedback('company-delete-button', 'error');
						setTimeout(() => {
							userFeedback('company-delete-button', 'text');
						}, 1500);
					}
				});
			}

		});


		jQuery(document).on('click', '.edit-button-company i', function (e) {
			e.stopPropagation();
			jQuery('#modal-update-firmaname-input').parent().find('input').first().removeClass('is-invalid');
			jQuery('#modal-update-firmaname-input').parent().find('.invalid-feedback').first().addClass('d-none');

			jQuery('#modal-select-update-chef-input').parent().find('input').first().removeClass('is-invalid');
			jQuery('#modal-select-update-chef-input').parent().find('.invalid-feedback').first().addClass('d-none');
			
			jQuery('#modal-select-update-kadmin-input').parent().find('input').first().removeClass('is-invalid');
			jQuery('#modal-select-update-kadmin-input').parent().find('.invalid-feedback').first().addClass('d-none');

			jQuery('#warning-chef').addClass('d-none');
			jQuery('#warning-kadmin').addClass('d-none');
			var selected_company_id = jQuery(this).parent().parent().attr('companyid');
			console.log(selected_company_id);
			var company_name = jQuery(this).parent().parent().find('.company-name').first().attr('value');
			var bossid = jQuery(this).parent().parent().find('.company-boss-id').first().attr('value');
			if(bossid == 0 || bossid == "") {
				jQuery('#warning-chef').removeClass('d-none');
			}
			var kAdminid = jQuery(this).parent().attr('kadminid');
			if((kAdminid == 0 || kAdminid == "") && jQuery('#warning-kadmin').length == 1) {
				jQuery('#warning-kadmin').removeClass('d-none');
			}
			//console.log("on click .edit-button-company i");
			// console.log(company_id);
			//console.log(company_name);

			//console.log(jQuery("#modal-update-firmaname-input").val());
			jQuery("#modal-update-firmaname-input").val(company_name);
			jQuery("#selected-company-id").val(selected_company_id);
			jQuery("#modal-select-update-chef-input").val(bossid);
			jQuery("#modal-select-update-kadmin-input").val(kAdminid);
			//console.log($("#modal-update-firmaname-input").val());

			//$("#modal-firmaname-input").val(company_name);

		});

		jQuery(document).on('click', '.clear-user-lektion', function (e) {

			checkForMissingDependentTraining(jQuery(this).parent().parent().parent().parent().attr("value"));
			jQuery(this).parent().remove();
			countTrainingLektion();
			needToSaveTrainings();
		});

		function addTrainingToTable(trainingid) {
			let isttraining = jQuery("#sortable-user-trainings #training" + trainingid);
			jQuery('#training-mgr-notrainings').remove();

			if (isttraining.length < 1) {
				jQuery('#training-mgr-loading').html('<span class="userfeedback-loading"><i class="fas fa-spinner"></i></span>');
				jQuery('#training-mgr-loading').removeClass('d-none');
				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "coach_user_training_mgr_gettraining",
						"trainingid": trainingid,
					},
					success: function (data) {
						jQuery('#training-mgr-loading').addClass('d-none');
						needToSaveTrainings()
						if(jQuery('#sortable-user-trainings .noSortable:not(.forward-training-none)').length > 0) {
							jQuery(data).insertBefore(jQuery('#sortable-user-trainings .noSortable:not(.forward-training-none)').first());
						} else {
							jQuery("#sortable-user-trainings").append(data);
						}
						jQuery('#sortable-user-trainings .forward-training-none').removeClass("d-none");
						jQuery("#sortable-user-trainings").sortable({items: "li:not(.noSortable)"}).disableSelection();
						jQuery(".connectedSortable").sortable({items: "li:not(.noSortable)"}).disableSelection();
					},
					error: function (errorThrown) {
						jQuery('#training-mgr-loading').html('<span class="userfeedback-error"><i class="far fa-times-circle"></i></span>');
						console.error(errorThrown);
					}
				});
			}
		}

		jQuery(document).on('submit', '#modal-addtraining-form', function (e) {
			e.preventDefault();
			filterTrainings();
		});

		jQuery(document).on("change paste keyup", "#modal-addtraining-input-search", function(e) {
			setTimeout(function(){ 
				filterTrainings();	
			}, 500);
		  });

		jQuery(document).on('click', '#clear-addtraining-search-button', function (e) {
			jQuery('#modal-addtraining-input-search').val('');
			filterTrainings();
		});

		function filterTrainings() {
			var tmps = ""+jQuery('#modal-addtraining-input-search').val().toLowerCase();
			console.log(tmps);
				
			//console.log(jQuery(this).val());
			jQuery('#modal-addtraining-check-trainings div').removeClass('d-flex').addClass('d-none');
	
			jQuery('#modal-addtraining-check-trainings div label').each(function (e) {
				if(typeof jQuery(this).attr('data-trainingname') !== typeof undefined && jQuery(this).attr('data-trainingname') !== false) {
					if(jQuery(this).attr('data-trainingname').toLowerCase().indexOf(tmps) >= 0) {
						jQuery(this).parent().addClass('d-flex').removeClass('d-none');
					}
				}
			});
		}

		jQuery(document).on('click', '.addtrainingbutton', function (e) {
			let selectedTrainings = [];
			jQuery(jQuery.find('#sortable-user-trainings > li:not(.noSortable)')).each(function (e) {
				selectedTrainings.push(jQuery(this).attr("value")+"");
			});

			jQuery(jQuery.find('.modal-select-training-input')).each(function (e) {
				if(jQuery.inArray(jQuery(this).attr("value"), selectedTrainings) != -1) {
					jQuery(this).attr('checked', 'checked');
				} else {
					jQuery(this).removeAttr('checked');
				}
			});

			var datedeptrainings = []
			jQuery(jQuery.find('#sortable-user-trainings > li.noSortable')).each(function (e) {
				datedeptrainings.push(jQuery(this).attr("value")+"");
			});

			jQuery(jQuery.find('.modal-select-training-input')).each(function (e) {
				if(jQuery.inArray(jQuery(this).attr("value"), datedeptrainings) != -1) {
					jQuery(this).prop('disabled', true);
				} else {
					jQuery(this).prop('disabled', false);
				}
			});

			jQuery('#addTrainingModal').modal('show');
		});

		jQuery(document).on('click', '.addtraining', function (e) {
			jQuery(this).parent().parent().parent().find('#sortable-user-trainings-select-trainings input:checkbox:checked').each(function (index) {
				console.log(index + ": " + jQuery(this).val());
				addTrainingToTable(jQuery(this).val());
			});
		});


		jQuery(document).on('change', '.trainingsart-radio', function() {
			jQuery(".trainingsart-radio").parent().removeClass("selected");
			jQuery( "input.trainingsart-radio:checked" ).parent().addClass("selected");
		});

		jQuery(document).on('submit', '#modal-addlektionen-form', function (e) {
			e.preventDefault();
			filterLektionen();
		});

		jQuery(document).on("change paste keyup", "#modal-addlektionen-input-search", function(e) {
			setTimeout(function(){ 
				filterLektionen();	
			}, 500);
		  });

		jQuery(document).on('click', '#clear-addlektionen-search-button', function (e) {
			jQuery('#modal-addlektionen-input-search').val('');
			filterLektionen();
		});

		function filterLektionen() {
			var tmps = ""+jQuery('#modal-addlektionen-input-search').val().toLowerCase();
			console.log(tmps);
				
			//console.log(jQuery(this).val());
			jQuery('#modal-addlektionen-check-lektionen div').removeClass('d-flex').addClass('d-none');
	
			jQuery('#modal-addlektionen-check-lektionen div label').each(function (e) {
			if(jQuery(this).html().toLowerCase().indexOf(tmps) >= 0) {
				jQuery(this).parent().addClass('d-flex').removeClass('d-none');
			}
			});
		}

		jQuery(document).on('click', '.addbutton-training-lektion', function (e) {
			var button = jQuery(this);
			userFeedback(button, 'loading');

			console.log(jQuery(this).parent().parent());
			console.log(jQuery(this).parent().parent().parent().val());
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					"action": "coach_user_training_mgr_gettraining_lektion",
					"trainingid": jQuery(this).parent().parent().parent().val(),
				},
				success: function (data) {
					console.log(data);
					jQuery("#lektionsmodal").html(data);
					let selectedLektionen = [];
					jQuery(jQuery.find('#sortable-user-trainings > li .training-mgr-lektionslite-item')).each(function (e) {
						selectedLektionen.push(jQuery(this).attr("value")+"");
					});
					jQuery(jQuery.find('.modal-select-lektionen-input')).each(function (e) {
						if(jQuery.inArray(jQuery(this).attr("value"), selectedLektionen) != -1) {
							jQuery(this).attr('checked', 'checked');
						}
					});
					userFeedback(button, 'success');
					jQuery('#addLektionModal').modal('show');
					setTimeout(() => {
						userFeedback(button, 'text');
					}, 1500);
				},
				error: function (errorThrown) {
					//alert('error');
					userFeedback(button, 'error');
					console.error(errorThrown);
					setTimeout(() => {
						userFeedback(button, 'text');
					}, 1500);
				}
			});
		});



		jQuery(document).on('click', '.addlektion', function (e) {
			// console.log(jQuery("#sortable-user-trainings-select-lektion").val());
			// console.log(jQuery(this).val());
			// let istlektion = jQuery("#sortable-user-trainings #training" + jQuery(this).val() + " .connectedSortable li[value='" + jQuery("#sortable-user-trainings-select-lektion").val() + "']");
			//
			// if (istlektion.length < 1) {
			//   jQuery("#sortable-user-trainings #training" + jQuery(this).val() + " .connectedSortable").
			//   append('<li class="ui-state-default" value="' + jQuery("#sortable-user-trainings-select-lektion").val() + '">' + jQuery("#sortable-user-trainings-select-lektion option:selected").text() + '<span class="ui-icon ui-icon-carat-2-n-s sortable-icon"></span><a class="clear-user-lektion"><span class="ui-icon ui-icon-minusthick"></span></a></li>');
			//   $(".connectedSortable-user").sortable().disableSelection();
			// 	countTrainingLektion();
			// }
			let button = jQuery(this);
			jQuery(this).parent().parent().parent().find('#sortable-user-trainings-select-lektionen input:checkbox:checked').each(function (index) {
				console.log(index + ": " + jQuery(this).val());
				console.log(index + ": " + jQuery(this).attr("name"));

				let istlektion = jQuery("#sortable-user-trainings #training" + button.val() + " .connectedSortable li[value='" + jQuery(this).val() + "']");

				if (istlektion.length < 1) {
					jQuery("#sortable-user-trainings #training" + button.val() + " .connectedSortable").
					//append('<li class="ui-state-default training-mgr-lektionslite-item ui-sortable-handle" value="' + jQuery( this ).val()+ '">' +jQuery( this ).text() + '<span class="ui-icon ui-icon-carat-2-n-s sortable-icon"></span><a class="clear-user-lektion"><span class="ui-icon ui-icon-minusthick"></span></a></li>');
					append('<li class="ui-state-default training-mgr-lektionslite-item ui-sortable-handle" value="' + jQuery(this).val() + '"><i class="fas fa-stream"></i>' + jQuery(this).attr("name") + '<span class="clear-user-lektion"><i class="far fa-minus-square"></i></span></li>');

					$(".connectedSortable-user").sortable().disableSelection();
					countTrainingLektion();
					needToSaveTrainings();
				}
			});
		});

		// Trainings Managaer Reset User Trainings
		jQuery(document).on('click', '.resettraininguser', function (e) {
			var trainings = {};
			jQuery("#sortable-user-trainings > li").each(function (index) {
				let training = "" + $(this).val();
				let auto = $(this).find('.trainingsart').val();
				trainings[training] = {
					"id": training,
					"auto": auto,
					"index" : index,
					"lektionen": new Array()
				}; //new Array();
				$(this).find(".connectedSortable li").each(function (index2) {
					let lektion = $(this).val();
					trainings[training]["lektionen"].push({
						"id": lektion
					});
				});
			});
			let users = [];

			// jQuery('#user-trainings-select-user input:checkbox:checked').each(function( index ) {
			//   console.log( index + ": " + $( this ).text() );
			// 	users.push($( this ).val());
			// });

			jQuery('.custom-table tbody tr').each(function (index) {
				console.log(jQuery(this).find("td").first().attr("valueid"));
				var id = jQuery(this).find("td").first().attr("valueid");
				console.log(index + ": " + $(this).text());
				users.push(id);
			});


            console.log("trainings", trainings);
			console.log("users", users);

			let trainingsstr = JSON.stringify(trainings);
			let usersstr = JSON.stringify(users);
			if (trainingsstr != "" && usersstr != "") {
				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "coach_user_training_mgr_reset_users_training",
						"trainings": trainingsstr,
						"users": usersstr
					},
					success: function (data) {
                        console.log(data);
						jQuery("#sortable-user-trainings").append(data);
						$(".connectedSortable").sortable().disableSelection();
                    },
					error: function (errorThrown) {
						//alert('error');
						console.error(errorThrown);
					}
				});
			}

		});

		// Trainings Manager Save User Trainings
		jQuery(document).on('click', '.savetraininguser', function (e) {
			userFeedback('coach-user-savetraining', 'loading');
			jQuery('#coach-user-training-mgr-training-unique-alert').addClass('d-none');
			jQuery("#coach-user-training-mgr-deletedDependentTrainingWarning").addClass("d-none");

			var trainings = {};
			var coachingmodi = {};
			var datetrainings = [];
			var deptrainings = [];
			jQuery("#sortable-user-trainings > li:not(.datetraining):not(.deptraining):not(.forward-training-none)").each(function (index) {
				let training = "" + $(this).val();
				let auto = 0;
				let enable = true;
				$(this).find('.checkautotraining input').each(function (e) {
					if(jQuery(this).is(':checked')) {
						auto = jQuery(this).val();
					}
				});
				var coachingOptSelected = 0;
				jQuery(this).find('.checkbox-coaching').each(function (e){
					if(jQuery(this).is(':checked')){
						coachingOptSelected = 1;
					}
				});

				if(coachingOptSelected == 0){
					coachingmodi[training] = "0";
				}
				else if(coachingOptSelected == 1){

					if(auto != 2){
						coachingmodi[training] = "1";
					}
					else{
						coachingmodi[training] = "2";
					}
				}

				enable = jQuery('#coach-user-training-mgr-enable-' + training).is(':checked');

				trainings[training] = {
					"id": training,
					"auto": auto,
					"enable": enable,
					"index" : index,
					"lektionen": new Array()
				}; //new Array();
				$(this).find(".connectedSortable li").each(function (index2) {
					let lektion = $(this).val();
					
					let lektionAuto = 0;
					if(auto == 2){
						if(jQuery(this).find("input.checkbox-coaching-lektion").is(":checked")){
							lektionAuto = "2";
						}
						else{
							lektionAuto = "1";
						}
					}
					else{
						lektionAuto = auto;
					}
					
					trainings[training]["lektionen"].push({
						"id": lektion,
						"lektionAuto": lektionAuto
					});
				});
			});

			jQuery("#sortable-user-trainings > li.datetraining").each(function (e) {
				datetrainings.push(jQuery(this).find('.dateArgs').first().val());
			});

			jQuery("#sortable-user-trainings > li.deptraining").each(function (e) {
				deptrainings.push(jQuery(this).find('.dateArgs').first().val());
			});

			let users = [];
			jQuery('.custom-table tbody tr').each(function (index) {
				var id = jQuery(this).find("td").first().attr("valueid");
				users.push(id);
			});


            // console.log("trainings", trainings);
			// console.log("users", users);
			// console.log("coachingmodi", coachingmodi);

			let trainingsstr = JSON.stringify(trainings);
			let usersstr = JSON.stringify(users);
			let coachingmodistr = JSON.stringify(coachingmodi);
			if (trainingsstr != "" && usersstr != "" && coachingmodistr != "") {
				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "coach_user_training_mgr_save_users_training",
						"trainings": trainingsstr,
						"users": usersstr,
						"coachingmodi": coachingmodistr,
						"datetrainings": JSON.stringify(datetrainings),
						"deptrainings": JSON.stringify(deptrainings),
						"redirectTraining": jQuery('input[name="coach-user-training-mgr-forward"]:checked').val(),
					},
					success: function (data) {
                        if(data != "1") {
							userFeedback('coach-user-savetraining', 'error');
							if(data == "-1") {
								jQuery('#coach-user-training-mgr-training-unique-alert').removeClass('d-none');
							}
							$(".connectedSortable").sortable().disableSelection();
						} else {
							userFeedback('coach-user-savetraining', 'success');
							dontNeedToSaveTrainings();
							getTrainingByUser(jQuery('.custom-table tbody tr').first().find("td").first().attr("valueid"));
						}
						setTimeout(() => {
							userFeedback('coach-user-savetraining', 'text');
						}, 1500);
                    },
					error: function (errorThrown) {
						userFeedback('coach-user-savetraining', 'error');
						setTimeout(() => {
							userFeedback('coach-user-savetraining', 'text');
						}, 1500);
					}
				});				
			}
		});

		// Start terminliche Zuweisung ++++++++++++++++++++++++++++++
		jQuery(document).on('submit', '#modal-date-addtraining-form', function (e) {
			e.preventDefault();
			filterTrainingsDateModal();
		});

		jQuery(document).on("change paste keyup", "#modal-date-addtraining-input-search", function(e) {
			setTimeout(function(){ 
				filterTrainingsDateModal();	
			}, 100);
		  });

		jQuery(document).on('click', '#clear-date-addtraining-search-button', function (e) {
			jQuery('#modal-date-addtraining-input-search').val('');
			filterTrainingsDateModal();
		});

		function filterTrainingsDateModal() {
			var tmps = ""+jQuery('#modal-date-addtraining-input-search').val().toLowerCase();
			
			jQuery('#modal-date-addtraining-check-trainings div.top:not(.disableTraining)').removeClass("d-flex").addClass("d-none");
	
			jQuery('#modal-date-addtraining-check-trainings div.top:not(.disableTraining) label').each(function (e) {
				if(typeof jQuery(this).attr('data-trainingname') !== typeof undefined && jQuery(this).attr('data-trainingname') !== false) {
					if(jQuery(this).attr('data-trainingname').toLowerCase().indexOf(tmps) >= 0) {
						jQuery(this).parent().parent().parent().addClass("d-flex").removeClass("d-none")
					}
				}
			});
		}

		jQuery(document).on('change', '.modal-date-select-training-input', function(e) {
			var checked = jQuery(this).is(':checked');
			jQuery(this).parent().parent().find('.date-training-modus').attr("disabled", !checked);
			jQuery(this).parent().parent().find('.redirect-training-modus').attr("disabled", !checked);
			jQuery(this).parent().parent().find('.ecoaching-training-modus').attr("disabled", !checked);
			jQuery(this).parent().parent().parent().find('.datedep-checkbox-coaching-lektion:not(.datedep-checkbox-coaching-lektion-first)').attr("disabled", !checked);
			if(!checked) {
				jQuery(this).parent().parent().parent().find('.extended-settings-area').first().removeClass('d-block').addClass('d-none');
				jQuery(this).parent().parent().parent().find('.extended-settings-area input.datedep-checkbox-coaching-lektion').prop('checked', true);
			} else if(jQuery(this).parent().parent().find('.date-training-modus').val() == "2") {
				jQuery(this).parent().parent().parent().find('.extended-settings-area').first().addClass('d-block').removeClass('d-none');
			}
		})

		jQuery(document).on('change', '.date-training-modus', function (e) {
			if(jQuery(this).val() == "2") {
				jQuery(this).parent().find('.ecoaching-training-modus').first().prop('checked', true).prop('disabled', true);
				jQuery(this).parent().parent().parent().find('.extended-settings-area').first().addClass('d-block').removeClass('d-none');
				jQuery(this).parent().parent().parent().find('.extended-settings-area input.datedep-checkbox-coaching-lektion').prop('checked', true);
			} else {
				jQuery(this).parent().find('.ecoaching-training-modus').first().prop('checked', false).prop('disabled', false);
				jQuery(this).parent().parent().parent().find('.extended-settings-area').first().removeClass('d-block').addClass('d-none');
			}
		})

		jQuery(document).on('click', '#modal-date-dateinput-clear', function (e) {
			jQuery('#modal-date-dateinput').val('');
		});

		jQuery(document).on('click', '#modal-date-clear-dependency', function (e) {

			jQuery('#modal-date-dependency-trainings input').prop('checked', false);

			jQuery('#modal-date-ambigious').addClass('d-none');
			jQuery('#modal-date-save').click();
		});

		jQuery(document).on('click', '#modal-date-clear-date', function (e) {

			jQuery('#modal-date-dateinput').val('');

			jQuery('#modal-date-ambigious').addClass('d-none');
			jQuery('#modal-date-save').click();
		});

		jQuery(document).on('click', '#modal-date-dependency-trainings input', function (e) {
			if(jQuery("#modal-date-dependency-trainings input:checked").length > 1) {
				jQuery('#modal-date-dependency-condition').attr("disabled", false);
			} else {
				jQuery('#modal-date-dependency-condition').attr("disabled", true);
			}
		});

		jQuery(document).on('click', '#modal-date-save', function (e) {
			jQuery('#modal-date-noselection').addClass('d-none');

			var date = false;
			if(jQuery('#modal-date-dateinput').val() != "") {
				date = true;
			} 
			var dep = false;
			if(jQuery('#modal-date-dependency-trainings input:checked').length > 0) { 
				dep = true;
			}
			
			if(date && dep) {
				jQuery('#modal-date-ambigious').removeClass('d-none');
			} else if((!date && !dep) || jQuery('#modal-date-addtraining-check-trainings input:checked').length == 0) {
				jQuery('#modal-date-noselection').removeClass('d-none');
			} else {

				if(date) {
					jQuery('#nav-date-tab').click();
				} else if (dep) {
					jQuery('#nav-dependency-tab').click();
				}

				userFeedback('modal-date-save', 'loading');

				let trainings = [];
				jQuery('#modal-date-addtraining-check-trainings .modal-date-select-training-input:checked').each(function (e) {
					var tr_obj = {
						tid: jQuery(this).val(), 
						mode: jQuery(this).parent().parent().find('.date-training-modus').val(), 
						ecoaching: jQuery(this).parent().parent().find('.ecoaching-training-modus').is(':checked') ? "1" : "0", 
						ecoachinglektion: {}, 
						redirect: jQuery(this).parent().parent().find('.redirect-training-modus').is(':checked'),
					};
					jQuery(this).parent().parent().parent().find('.datedep-checkbox-coaching-lektion').each(function (e) {
						if(jQuery(this).is(':checked')) {
							tr_obj.ecoachinglektion[jQuery(this).attr('data-lektionid')] = '2';
						} else {
							tr_obj.ecoachinglektion[jQuery(this).attr('data-lektionid')] = '1';
						}
					});
					trainings.push(tr_obj);
				});

				let deptrainings = [];
				jQuery('#modal-date-dependency-trainings input:checked').each(function (e) {
					deptrainings.push(jQuery(this).val());
				});

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "coach_user_training_mgr_create_datedep",
						"trainings": JSON.stringify(trainings),
						"mode": date ? "date" : "dependency",
						"date_date": jQuery('#modal-date-dateinput').val(),
						"dep_time": jQuery('#modal-date-dependency-time').val(),
						"dep_timemultiplier": jQuery('#modal-date-dependency-timemultiplier').val(),
						"dep_trainings": JSON.stringify(deptrainings),
						"dep_condition": jQuery('#modal-date-dependency-condition').val()
					},
					success: function (data) {
                        if(data.trim() == "") {
							userFeedback('modal-date-save', 'error');
						} else {
							userFeedback('modal-date-save', 'success');
							setTimeout(() => {
								jQuery('#training-mgr-date-modal').modal('hide');
								jQuery("#sortable-user-trainings").append(data);
								jQuery('#training-mgr-notrainings').remove();
								jQuery("#sortable-user-trainings").sortable({items: "li:not(.noSortable)"}).disableSelection();
								needToSaveTrainings();
							}, 500)
						}
						setTimeout(() => {
							userFeedback('modal-date-save', 'text');
						}, 1500);
                    },
					error: function (errorThrown) {
						userFeedback('modal-date-save', 'error');
						console.error(errorThrown);
						setTimeout(() => {
							userFeedback('modal-date-save', 'text');
						}, 1500);
					}
				});

				setTimeout(() => {
					userFeedback('modal-date-save', 'text');
				}, 1500)
			}
		})

		jQuery(document).on('show.bs.modal', '#training-mgr-date-modal', function (e) {
			jQuery('#modal-date-addtraining-check-trainings div.top').removeClass("disableTraining");

			jQuery('#sortable-user-trainings li.ui-state-default.mylist').each(function (e) {
				jQuery('#modal-date-addtraining-check-trainings div.top[data-trainingid="'+ jQuery(this).val() +'"]').addClass("disableTraining d-none").removeClass("d-flex");
			})

			jQuery('#modal-date-dependency-trainings input').parent().removeClass('d-flex').addClass('d-none');
			jQuery('#sortable-user-trainings li.ui-state-default.mylist').each(function (e) {
				console.log(jQuery(this).val())
				jQuery('#modal-date-dependency-trainings-'+jQuery(this).val()).parent().removeClass('d-none').addClass('d-flex');
			})
		});

		jQuery(document).on('hidden.bs.modal', '#training-mgr-date-modal', function (e) {
			jQuery('#modal-date-addtraining-input-search').val('');
			filterTrainingsDateModal();

			jQuery('#nav-date-tab').click();

			jQuery('#modal-date-addtraining-check-trainings input:not(.datedep-checkbox-coaching-lektion)').prop("checked", false);
			jQuery('#modal-date-addtraining-check-trainings input.datedep-checkbox-coaching-lektion').prop("checked", true);
			jQuery('#modal-date-addtraining-check-trainings .collapse').collapse('hide');
			jQuery('#modal-date-addtraining-check-trainings .extended-settings-area').addClass('d-none').removeClass('d-block');
			jQuery('#modal-date-addtraining-check-trainings div.top').addClass("d-flex").removeClass("d-none");
			jQuery('#modal-date-addtraining-check-trainings .date-training-modus').attr("disabled", true).val(0);

			jQuery('#modal-date-dateinput').val('');

			jQuery('#modal-date-dependency-time').val(0);
			jQuery('#modal-date-dependency-timemultiplier').val('days');
			jQuery('#modal-date-dependency-trainings input').parent().parent().addClass('d-flex').removeClass('d-none');
			jQuery('#modal-date-dependency-trainings input').prop('checked', false);
			jQuery('#modal-date-dependency-condition').val('or').attr("disabled", true);

			jQuery('#modal-date-noselection').addClass('d-none');
			jQuery('#modal-date-ambigious').addClass('d-none');
		})

		jQuery(document).on('click', '.delete-training-dep', function (e) {
			jQuery(this).parent().parent().parent().remove();
			jQuery('#coach-user-training-mgr-training-unique-alert').addClass('d-none');
			needToSaveTrainings();
		})

		jQuery(document).on('click', '.delete-training-date', function (e) {
			checkForMissingDependentTraining(jQuery(this).parent().parent().parent().attr("value"));
			jQuery(this).parent().parent().parent().remove();
			jQuery('#coach-user-training-mgr-training-unique-alert').addClass('d-none');
			needToSaveTrainings();
		})
		
		function checkForMissingDependentTraining(trainingIdDeleted) {
			let deletedDependentTraining = false;

			jQuery("#sortable-user-trainings > li.deptraining").each(function (e) {
				let dependentTraining = jQuery(this);
				let dependentTrainingString = jQuery(this).attr("data-deptrainings");
				let dependentTrainingsArray = dependentTrainingString.split(",");
				dependentTrainingsArray.forEach(item => {
					if(item == trainingIdDeleted) {
						deletedDependentTraining = true;
						dependentTraining.find(".listheader-sortable-text").html("<i class='fas fa-exclamation-triangle mr-2 text-warning'></i>" + dependentTraining.find(".listheader-sortable-text").html())
					}
				})
			});
			
			if(deletedDependentTraining) {
				jQuery("#coach-user-training-mgr-deletedDependentTrainingWarning").removeClass("d-none");
			}
		}
			
		jQuery(document).on('click', '.edit-training-date, .edit-training-dep', function (e) {

			var btn = jQuery(this);
			userFeedback(btn, 'loading');
			jQuery('#training-mgr-datedep-edit-modal-area').html('');
			var type = "";
			if(jQuery(this).hasClass('edit-training-date')) {
				type = "date";
			} else {
				type = "dep";
			}

			jQuery.ajax({
				type: 'POST',
					url: ajaxurl,
					data: {
						"action": "coach_user_training_mgr_edit_datedep",
						"trainingdatedepid": jQuery(btn).attr("data-trainingdatedepid"),
						"type": type
					},
					success: function (data) {
                        if(data.trim() == "") {
							userFeedback(btn, 'error');
						} else {
							userFeedback(btn, 'success');
							setTimeout(() => {
								jQuery('#training-mgr-datedep-edit-modal-area').html(data);
								jQuery('#training-mgr-datedep-edit-modal').modal('show');
							}, 500)
						}
						setTimeout(() => {
							userFeedback(btn, 'text');
						}, 1500);
                    },
					error: function (errorThrown) {
						userFeedback(btn, 'error');
						setTimeout(() => {
							userFeedback(btn, 'text');
						}, 1500);
					}
			});
		})

		jQuery(document).on('change', '#modal-datedep-edit-training-mode', function(e) {
			if(jQuery(this).val() == "2") {
				jQuery('#modal-datedep-edit-training-ecoaching').prop('checked', true).prop('disabled', true);
				jQuery('#extended-settings-area-editmodal').removeClass('d-none').addClass('d-block');
				jQuery(this).parent().parent().parent().find('#extended-settings-area-editmodal input.datedep-checkbox-coaching-lektion').prop('checked', true);
			} else {
				jQuery('#modal-datedep-edit-training-ecoaching').prop('checked', false).prop('disabled', false);
				jQuery('#extended-settings-area-editmodal').addClass('d-none').removeClass('d-block');
			}
		})

		jQuery(document).on('click', '#modal-datedep-edit-dateinput-clear', function (e) {
			jQuery('#modal-datedep-edit-dateinput').val('');
		});

		jQuery(document).on('click', '#modal-datedep-edit-dependency-trainings input', function (e) {
			if(jQuery("#modal-datedep-edit-dependency-trainings input:checked").length > 1) {
				jQuery('#modal-datedep-edit-dependency-condition').attr("disabled", false);
			} else {
				jQuery('#modal-datedep-edit-dependency-condition').attr("disabled", true);
			}
		});

		jQuery(document).on('show.bs.modal', '#training-mgr-datedep-edit-modal', function (e) {
			jQuery('#modal-datedep-edit-dependency-trainings input').parent().removeClass('d-flex').addClass('d-none');

			jQuery('#sortable-user-trainings li.ui-state-default.mylist:not(.noSortable)').each(function (e) {
				jQuery('#modal-datedep-edit-dependency-trainings-'+jQuery(this).val()).parent().removeClass('d-none').addClass('d-flex');
			})
		})

		jQuery(document).on('hidden.bs.modal', '#training-mgr-datedep-edit-modal', function (e) {
			jQuery('#training-mgr-datedep-edit-modal-area').html('');
		})

		jQuery(document).on('click', '#modal-datedep-edit-save', function (e) {
			
			var oldElement = jQuery('#sortable-user-trainings li[value="' + jQuery('#modal-datedep-edit-training-id').val() + '"]');

			var btn = jQuery(this);
			userFeedback(btn, 'loading');

			var data = {};
			data['action'] = "coach_user_training_mgr_change_datedep";
			data['trainingdatedepid'] = jQuery(btn).attr('data-trainingdatedepid');

			let deptrainings = [];
			jQuery('#modal-datedep-edit-dependency-trainings input:checked').each(function (e) {
				deptrainings.push(jQuery(this).val());
			});
			data['modal-datedep-edit-dependency-trainings'] = JSON.stringify(deptrainings);

			jQuery('.training-mgr-datedep-edit-modal-data :input:not(#modal-datedep-edit-dateinput-clear):not(.datedep-edit-dependency-training):not(.datedep-checkbox-coaching-lektion):not(.btn-link)').each(function (e) {
				if(jQuery(this).attr("type") != undefined && jQuery(this).attr("type") == "checkbox") {
					data[jQuery(this).attr('id')] = jQuery(this).is(':checked') ? "1" : "0";
				} else {
					data[jQuery(this).attr('id')] = jQuery(this).val();
				}
			})

			data['modal-datedep-edit-extendedecoaching'] = {};
			jQuery('#extended-settings-body-editmodal input.datedep-checkbox-coaching-lektion').each(function (e) {
				if(jQuery(this).is(':checked')) {
					data['modal-datedep-edit-extendedecoaching'][jQuery(this).attr('data-lektionid')] = '2';
				} else {
					data['modal-datedep-edit-extendedecoaching'][jQuery(this).attr('data-lektionid')] = '1';
				}
			});

			jQuery.ajax({
				type: 'POST',
					url: ajaxurl,
					data: data,
					success: function (data) {
                        if(data.trim() == "") {
							userFeedback(btn, 'error');
						} else {
							userFeedback(btn, 'success');
							setTimeout(() => {
								jQuery('#training-mgr-datedep-edit-modal').modal('hide');
							
								jQuery(data).insertAfter(jQuery(oldElement));
								jQuery(oldElement).remove();
								jQuery(".connectedSortable").sortable({items: "li:not(.noSortable)"}).disableSelection();
								needToSaveTrainings();
							}, 500);
						}
						setTimeout(() => {
							userFeedback(btn, 'text');
						}, 1500);
                    },
					error: function (errorThrown) {
						userFeedback(btn, 'error');
						setTimeout(() => {
							userFeedback(btn, 'text');
						}, 1500);
					}
			});
		})

		// Ende terminliche Zuweisung +++++++++++++++++++++++++++++++

		// Start Vorlagennutzer
		jQuery(document).on('click', '.vorlagenusertable tr.vorlagenuser-row', function(e) {
			if(!jQuery(this).hasClass("pointerdisabled")) {
				jQuery('.vorlagenusertable tr.vorlagenuser-row').removeClass('active');
				jQuery(this).addClass('active');
				jQuery('.vorlagenusertable tr.vorlagenuser-row, .vorlagenusertable tr.vorlagenuser-row td').addClass("pointerdisabled").removeClass("cursor-pointer");
				jQuery('#coach-user-training-mgr-deletedDependentTrainingWarning').addClass("d-none");
				getTrainingByUser(jQuery(this).attr('vuserid'));
			}
		});

		jQuery(document).on('click', '.savetrainingvorlagenuser', function (e) {
			jQuery('#coach-user-training-mgr-deletedDependentTrainingWarning').addClass("d-none");
			userFeedback('savetrainingvorlagenuser', 'loading');
            var trainings = {};
			var coachingmodi = {};
			var datetrainings = [];
			var deptrainings = [];
			jQuery("#sortable-user-trainings > li:not(.datetraining):not(.deptraining):not(.forward-training-none").each(function (index) {
				let training = "" + $(this).val();
				let auto = 0;
				let enable = true;
				$(this).find('.checkautotraining input').each(function (e) {
					if(jQuery(this).is(':checked')) {
						auto = jQuery(this).val();
					}
				});

				var coachingOptSelected = 0;
				jQuery(this).find('.checkbox-coaching').each(function (e){
					if(jQuery(this).is(':checked')){
						coachingOptSelected = 1;
					}
				});

				if(coachingOptSelected == 0){
					coachingmodi[training] = "0";
				}
				else if(coachingOptSelected == 1){

					if(auto != 2){
						coachingmodi[training] = "1";
					}
					else{
						coachingmodi[training] = "2";
					}
				}

				enable = jQuery('#coach-user-training-mgr-enable-' + training).is(':checked');

				trainings[training] = {
					"id": training,
					"auto": auto,
					"enable": enable,
					"index" : index,
					"lektionen": new Array()
				}; //new Array();
				$(this).find(".connectedSortable li").each(function (index2) {
					let lektion = $(this).val();

					let lektionAuto = 0;
					if(auto == 2){
						if(jQuery(this).find("input.checkbox-coaching-lektion").is(":checked")){
							lektionAuto = "2";
						}
						else{
							lektionAuto = "1";
						}
					}
					else{
						lektionAuto = auto;
					}
					trainings[training]["lektionen"].push({
						"id": lektion,
						"lektionAuto": lektionAuto
					});
				});
			});

			jQuery("#sortable-user-trainings > li.datetraining").each(function (e) {
				datetrainings.push(jQuery(this).find('.dateArgs').first().val());
			});


			jQuery("#sortable-user-trainings > li.deptraining").each(function (e) {
				deptrainings.push(jQuery(this).find('.dateArgs').first().val());
			});

			let users = [];

			// jQuery('#user-trainings-select-user input:checkbox:checked').each(function( index ) {
			//   console.log( index + ": " + $( this ).text() );
			// 	users.push($( this ).val());
			// });

			jQuery('.vorlagenusertable tr.vorlagenuser-row').each(function (index) {
				var id = jQuery(this).attr('vuserid');
				if(jQuery(this).hasClass('active')) {
					users.push(id);
				}
			});

			console.log("trainings", trainings);
			console.log("users", users);
			console.log("coachingmodi", coachingmodi);

			let trainingsstr = JSON.stringify(trainings);
			let usersstr = JSON.stringify(users);
			let coachingmodistr = JSON.stringify(coachingmodi);

			if (trainingsstr != "" && usersstr != "" && coachingmodistr != "") {
				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "coach_user_training_mgr_save_users_training",
						"trainings": trainingsstr,
						"users": usersstr,
						"coachingmodi": coachingmodistr,
						"deptrainings": JSON.stringify(deptrainings),
						"datetrainings": JSON.stringify(datetrainings),
						"redirectTraining": jQuery('input[name="coach-user-training-mgr-forward"]:checked').val(),
					},
					success: function (data) {
						if(data != "1") {
							console.log(data);
							userFeedback('savetrainingvorlagenuser', 'error');
							$(".connectedSortable").sortable().disableSelection();
						} else {
							userFeedback('savetrainingvorlagenuser', 'success');
							dontNeedToSaveTrainings();
							getTrainingByUser(jQuery('.custom-table tbody tr.active').attr('vuserid'));
						}
						setTimeout(() => {
							userFeedback('savetrainingvorlagenuser', 'text');
						}, 1500);
					},
					error: function (errorThrown) {
						//alert('error');
						userFeedback('savetrainingvorlagenuser', 'error');
						console.error(errorThrown);
						setTimeout(() => {
							userFeedback('savetrainingvorlagenuser', 'text');
						}, 1500);
					}
				});
			} else {
				userFeedback('savetrainingvorlagenuser', 'text');
			}
		});

		jQuery(document).on('focusout', '#modal-newvorlagenuser-name', function (e) {
			if(jQuery('#modal-newvorlagenuser-name').val().trim() == "") {
				jQuery('#modal-newvorlagenuser-name').parent().find('input').first().addClass('is-invalid');
				jQuery('#modal-newvorlagenuser-name').parent().parent().find('.invalid-feedback').first().addClass('d-block');
			} else {
				jQuery('#modal-newvorlagenuser-name').parent().find('input').first().removeClass('is-invalid');
				jQuery('#modal-newvorlagenuser-name').parent().parent().find('.invalid-feedback').first().removeClass('d-block');
			}
		});

		jQuery('#addvorlagenuserForm').submit(function (e) {
			e.preventDefault();
			jQuery('.addvorlagenuser').click();
		});

		// Modal Dialog "Vorlagen-Nutzer hinzufügen" event on clicking button "Speichern"
		jQuery(document).on('click', '.addvorlagenuser', function (e) {
			if(jQuery('#modal-newvorlagenuser-name').val().trim() != "") {
				userFeedback('addvorlagenuser', 'loading');
				// get "val" properties of html elements with ids "modal-firmaname-input"
				// and "modal-select-chef-input"
				let vorlagenuser_name = jQuery("#modal-newvorlagenuser-name").val();
				let vorlagenuser_trainerid = jQuery('#modal-newvorlagenuser-trainer').val();

				if (vorlagenuser_name != "") {
					// Send data using ajax to be processed by hook funktion defined
					// for action "wp_ajax_vorlagen_user_adduser".
					// See includes\hooks-define\class-trainingssystem-plugin-hooks-define-admin.php.
					jQuery.ajax({
						type: 'POST',
						url: ajaxurl,
						data: {
							"action": "vorlagen_user_adduser",
							"username": vorlagenuser_name,
							"coach_id": vorlagenuser_trainerid,
						},
						success: function (data) {
							if(data == "1") {
								userFeedback('addvorlagenuser', 'success');
								console.log(data);
								jQuery("#modal-newvorlagenuser-name").val('');
								jQuery('#modal-newvorlagenuser-trainer').val('');
								jQuery('#addvorlagenuserModal').modal('hide');
								getVorlagenUserList();
							} else {
								userFeedback('addvorlagenuser', 'error');
							}
							setTimeout(() => {
								userFeedback('addvorlagenuser', 'text');
							}, 1500);
						},
						error: function (errorThrown) {
							console.error(errorThrown);
							userFeedback('addvorlagenuser', 'error');
							setTimeout(() => {
								userFeedback('addvorlagenuser', 'text');
							}, 1500);
						}
					});
				} else {
					userFeedback('addvorlagenuser', 'error');
					setTimeout(() => {
						userFeedback('addvorlagenuser', 'text');
					}, 500);
				}
			} else {
				jQuery('#modal-newvorlagenuser-name').parent().find('input').first().addClass('is-invalid');
				jQuery('#modal-newvorlagenuser-name').parent().find('.invalid-feedback').first().removeClass('d-none');
			}

		});

		function getVorlagenUserList() {
			var cols = jQuery('.custom-table').find('thead').first().find('th').length;
			jQuery('.custom-table').find('tbody').first().html('<tr><td class="text-center" colspan="' + cols + '"><span class="userfeedback-loading"><i class="fas fa-spinner"></i></span></td></tr>');
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					"action": "vorlagen_user_getvorlagenuser"
				},
				success: function (data) {
					jQuery('.custom-table').find('tbody').first().html(data);
				},
				error: function (errorThrown) {
					jQuery('.custom-table').find('tbody').first().first().html('<span class="userfeedback-error"><i class="far fa-times-circle"></i></span>');
					console.error(errorThrown);
				}
			});
		}

		jQuery(document).on('focusout', '#updateVorlagenuserName', function (e) {
			if(jQuery('#updateVorlagenuserName').val().trim() == "") {
				jQuery('#updateVorlagenuserName').parent().parent().find('input').first().addClass('is-invalid');
				jQuery('#updateVorlagenuserName').parent().parent().find('.invalid-feedback').first().addClass('d-block');
			} else {
				jQuery('#updateVorlagenuserName').parent().parent().find('input').first().removeClass('is-invalid');
				jQuery('#updateVorlagenuserName').parent().parent().find('.invalid-feedback').first().removeClass('d-block');
			}
		});

		jQuery(document).on('submit', '#updateVorlagenuserForm', function (e) {
			e.preventDefault();
			jQuery('.updatevorlagenuser-confirm').click();
		});

		jQuery(document).on('click', '.vorlagenuser-edit', function (e) {
			e.stopPropagation();
			var vuserid = jQuery(this).attr("data-vuserid");
			var btn = jQuery(this);
			
			jQuery('#updateVorlageModalArea').html('');

			if(vuserid != undefined) {
				
				userFeedback(btn, 'loading');
				
				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "vorlagen_user_edit_modal",
						"vuserid": vuserid
					},
					success: function (data) {
						jQuery('#updateVorlageModalArea').html(data);

						setTimeout(() => {
							jQuery('#updateVorlagenuserModal').modal('show');
						}, 500);

						userFeedback(btn, 'success');
						setTimeout(() => {
							userFeedback(btn, 'text');
						}, 1500);
					},
					error: function (errorThrown) {
						userFeedback(btn, 'error');
						setTimeout(() => {
							userFeedback(btn, 'text');
						}, 1500);
					}
				});
			} else {
				userFeedback(btn, 'error');
				setTimeout(() => {
					getVorlagenUserList();
					userFeedback(btn, 'text');
				}, 1500);
			}
		});

		jQuery(document).on('hidden.bs.modal', '#updateVorlagenuserModal', function(e) {
			jQuery('#updateVorlageModalArea').html('');
		});

		jQuery(document).on('click', '.vorlagenuser-delete', function(e) {
			e.stopPropagation();
			jQuery('#deleteVorlagenuserRegisterkey').prop('checked', true);
			var id = jQuery(this).find('span').first().html();
			var name = jQuery(this).find('span').last().html();

			jQuery('#deleteVorlagenuserId').val(parseInt(id));
			jQuery('#deleteVorlagenuserName').html(name);
			jQuery('#deleteVorlagenuserModal').modal('show');
		});

		jQuery(document).on('click', '.deletevorlagenuser-confirm', function (e) {
			var id = jQuery('#deleteVorlagenuserId').val();
			var deleteRegisterkeys = jQuery('#deleteVorlagenuserRegisterkey').is(':checked');
			console.log(deleteRegisterkeys);

			if(id != "") {
				userFeedback('deletevorlagenuser-confirm', 'loading');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "vorlagen_user_delete",
						"vorlagenuserid": id,
						"deleteregisterkeys": deleteRegisterkeys
					},
					success: function (data) {
						if(data == "1") {
							userFeedback('deletevorlagenuser-confirm', 'success');
							getVorlagenUserList();
							setTimeout(() => {
								jQuery('#deleteVorlagenuserId').val('');
								jQuery('#deleteVorlagenuserRegisterkey').prop('checked', true)
								jQuery('#deleteVorlagenuserModal').modal('hide');
								userFeedback('deletevorlagenuser-confirm', 'text');
							}, 500);
							location.reload();
						} else {
							userFeedback('deletevorlagenuser-confirm', 'error');
							setTimeout(() => {
								userFeedback('deletevorlagenuser-confirm', 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback('deletevorlagenuser-confirm', 'error');
						setTimeout(() => {
							userFeedback('deletevorlagenuser-confirm', 'text');
						}, 1500);
						console.error(errorThrown);
					}
				});			

			} else {
				userFeedback('deletevorlagenuser-confirm', 'error');
				setTimeout(() => {
					userFeedback('deletevorlagenuser-confirm', 'text');
				}, 1500);
			}
		});

		jQuery(document).on('click', '.updatevorlagenuser-confirm', function(e) {
			var id = jQuery('#updateVorlagenuserId').val();
			var name = jQuery('#updateVorlagenuserName').val();
			var trainer = jQuery('#updateVorlagenuserTrainer').val();

			if(id != "" && name != "") {
				userFeedback('updatevorlagenuser-confirm', 'loading');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "vorlagen_user_update",
						"vorlagenuserid": id,
						"vorlagenusername": name,
						"vorlagenusertrainer": trainer,
					},
					success: function (data) {
						if(data == "1") {
							userFeedback('updatevorlagenuser-confirm', 'success');
							getVorlagenUserList();
							setTimeout(() => {
								jQuery('#updateVorlagenuserId').val('');
								jQuery('#updateVorlagenuserName').val('');
								jQuery('#updateVorlagenuserTrainer').val('');

								jQuery('#updateVorlagenuserModal').modal('hide');
								userFeedback('updatevorlagenuser-confirm', 'text');
							}, 500);
						} else {
							userFeedback('updatevorlagenuser-confirm', 'error');
							setTimeout(() => {
								userFeedback('updatevorlagenuser-confirm', 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback('updatevorlagenuser-confirm', 'error');
						setTimeout(() => {
							userFeedback('updatevorlagenuser-confirm', 'text');
						}, 1500);
						console.error(errorThrown);
					}
				});
			} else {
				if(name.trim() == "") {
					jQuery('#updateVorlagenuserName').parent().parent().find('input').first().addClass('is-invalid');
					jQuery('#updateVorlagenuserName').parent().parent().find('.invalid-feedback').first().removeClass('d-none');
				}
				userFeedback('updatevorlagenuser-confirm', 'error');
				setTimeout(() => {
					userFeedback('updatevorlagenuser-confirm', 'text');
				}, 1500);
			}
		});
		
		jQuery('#modal-firmaname-input').focusout(function (e) {
			if(jQuery('#modal-firmaname-input').val().trim() == "") {
				jQuery('#modal-firmaname-input').parent().find('input').first().addClass('is-invalid');
				jQuery('#modal-firmaname-input').parent().find('.invalid-feedback').first().removeClass('d-none');
			} else {
				jQuery('#modal-firmaname-input').parent().find('input').first().removeClass('is-invalid');
				jQuery('#modal-firmaname-input').parent().find('.invalid-feedback').first().addClass('d-none');
			}
		})

		jQuery(document).on('submit', '#newFirmaForm', function(e) {
			e.preventDefault();
			jQuery('.addcompany').click();
		});

		// Modal Dialog "Unternehmen hinzufügen" event on clicking button "Speichern"
		jQuery(document).on('click', '.addcompany', function (e) {

			jQuery('#modal-firmaname-input').parent().find('input').first().removeClass('is-invalid');
			jQuery('#modal-firmaname-input').parent().find('.invalid-feedback').first().addClass('d-none');

			jQuery('#modal-select-chef-input').parent().find('input').first().removeClass('is-invalid');
			jQuery('#modal-select-chef-input').parent().find('.invalid-feedback').first().addClass('d-none');
			
			jQuery('#modal-select-kadmin-input').parent().find('input').first().removeClass('is-invalid');
			jQuery('#modal-select-kadmin-input').parent().find('.invalid-feedback').first().addClass('d-none');
			// get "val" properties of html elements with ids "modal-firmaname-input"
			// and "modal-select-chef-input"
			let company_name = jQuery("#modal-firmaname-input").val().trim();
			let bossid = jQuery("#modal-select-chef-input").val();
			let kAdminid = 0;
			if(jQuery("#modal-select-kadmin-input").length == 1) {
				kAdminid = jQuery("#modal-select-kadmin-input").val();
			}
			
			if (company_name != "") {
				userFeedback('company-new-button', 'loading');
				// Send data using ajax to be processed by hook funktion defined
				// for action "company_mgr_add_company".
				// See includes\hooks-define\class-trainingssystem-plugin-hooks-define-admin.php.
				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "company_mgr_add_company",
						"company_name": JSON.stringify(company_name),
						"bossid": bossid,
						"kadminid": kAdminid
					},
					success: function (data) {
						if(data != "0") {
							setTimeout(() => {
								location.reload(); 
							}, 1500);
							userFeedback('company-new-button', 'success');
						} else {
							userFeedback('company-new-button', 'error');
							jQuery("#bosserrormodal").modal();
							setTimeout(() => {
								userFeedback('company-new-button', 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						//alert('error');
						console.error(errorThrown);
						userFeedback('company-new-button', 'error');
						setTimeout(() => {
							userFeedback('company-new-button', 'text');
						}, 1500);
					}
				});
				
			} else {
				if(company_name == "") {
					jQuery('#modal-firmaname-input').parent().find('input').first().addClass('is-invalid');
					jQuery('#modal-firmaname-input').parent().find('.invalid-feedback').first().removeClass('d-none');
				}
				userFeedback('company-new-button', 'error');
				setTimeout(() => {
					userFeedback('company-new-button', 'text');
				}, 1500);
			}

		});

		jQuery('#modal-update-firmaname-input').focusout(function (e) {
			if(jQuery('#modal-update-firmaname-input').val().trim() == "") {
				jQuery('#modal-update-firmaname-input').parent().find('input').first().addClass('is-invalid');
				jQuery('#modal-update-firmaname-input').parent().find('.invalid-feedback').first().removeClass('d-none');
			} else {
				jQuery('#modal-update-firmaname-input').parent().find('input').first().removeClass('is-invalid');
				jQuery('#modal-update-firmaname-input').parent().find('.invalid-feedback').first().addClass('d-none');
			}
		})

		jQuery(document).on('submit', '#updateFirmaForm', function (e) {
			e.preventDefault();
			jQuery('.updatecompany').click();
		})

		// Modal Dialog "Unternehmen Editieren" event on clicking button "Speichern"
		jQuery(document).on('click', '.updatecompany', function (e) {
			jQuery('#modal-update-firmaname-input').parent().find('input').first().removeClass('is-invalid');
			jQuery('#modal-update-firmaname-input').parent().find('.invalid-feedback').first().addClass('d-none');

			jQuery('#modal-select-update-chef-input').parent().find('input').first().removeClass('is-invalid');
			jQuery('#modal-select-update-chef-input').parent().find('.invalid-feedback').first().addClass('d-none');
			
			jQuery('#modal-select-update-kadmin-input').parent().find('input').first().removeClass('is-invalid');
			jQuery('#modal-select-update-kadmin-input').parent().find('.invalid-feedback').first().addClass('d-none');
			// get "val" properties of html elements with ids "modal-firmaname-input"
			// and "modal-select-chef-input"
			let company_update_name = jQuery("#modal-update-firmaname-input").val().trim();
			let company_update_bossid = jQuery("#modal-select-update-chef-input").val();
			let company_id = jQuery("#selected-company-id").val();
			let kadminid = jQuery("#modal-select-update-kadmin-input").val();

			if (company_update_name != "") {
				userFeedback('company-update-button', 'loading');
				// Send data using ajax to be processed by hook funktion defined
				// for action "company_mgr_add_company".
				// See includes\hooks-define\class-trainingssystem-plugin-hooks-define-admin.php.
				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "company_mgr_update_company",
						"company_name": JSON.stringify(company_update_name),
						"company_id": company_id,
						"bossid": company_update_bossid,
						"kadminid": kadminid
					},
					success: function (data) {
						console.log(data);
						if(data != "0") {
							userFeedback('company-update-button', 'success');
							setTimeout(() => {
								location.reload();
							}, 1500);
						} else {
							jQuery("#bosserrormodal").modal();
							userFeedback('company-update-button', 'error');
							setTimeout(() => {
								userFeedback('company-update-button', 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						console.error(errorThrown);
						userFeedback('company-update-button', 'error');
						setTimeout(() => {
							userFeedback('company-update-button', 'text');
						}, 1500);
					}
				});
			} else {
				if(company_update_name == "") {
					jQuery('#modal-update-firmaname-input').parent().find('input').first().addClass('is-invalid');
					jQuery('#modal-update-firmaname-input').parent().find('.invalid-feedback').first().removeClass('d-none');
				}

				userFeedback('company-update-button', 'error');
				setTimeout(() => {
					userFeedback('company-update-button', 'text');
				}, 1500);
			}

		});

		//ende coach user trainings mgr ################################################

		//start Benutzer bearbeiten
		jQuery(document).on('focusout', '#input-field-mail', function(e) {
			if(!validateMail(jQuery('#input-field-mail').val())) {
				if(jQuery('#input-field-user-name').length == 1) { //User manuell anlegen, Fehler "User mit dieser Mail existiert bereits" zurücksetzen
					jQuery('#user-create-manually-error').removeClass('d-block');
					jQuery('#user-create-manually-error').html('');
				}
				
				jQuery('#input-field-mail').parent().find('input').first().addClass('is-invalid');
				jQuery('#input-field-mail').parent().find('.invalid-feedback').first().removeClass('d-none');
			} else {
				jQuery('#input-field-mail').parent().find('input').first().removeClass('is-invalid');
				jQuery('#input-field-mail').parent().find('.invalid-feedback').first().addClass('d-none');
			}
		});

		jQuery(document).on('keypress', '#useredit-form input', function (e) {
			if(e.which == "13") {
				e.preventDefault();
				jQuery('#usereditsave-button').click();
			}
		});

		jQuery(document).on('click', '.usereditsave', function (e) {

			var mailExists = false;
			if(jQuery('#input-field-mail').length) {
				mailExists = true;
			}
			if(!mailExists || validateMail(jQuery('#input-field-mail').val())) {
				userFeedback('usereditsave-button', 'loading');
				let data = {};

				data["action"] = "user_update_details";
				data["userid"] = jQuery("#input-field-userid").val();
				if(mailExists) {
					data["usermail"] = jQuery("#input-field-mail").val();
				}
				data["studienid"] = jQuery("#input-field-studienid").val();
				data["studiengruppenid"] = jQuery("#input-field-studiengruppenid").val();
				data["coachid"] = jQuery('#selectbox-trainer').val();
				data["userInactivity"] = jQuery('#input-field-userInactivity').val();
				jQuery('#useredit-form').find('input.cansave-checkbox').each(function() {
					if(jQuery(this).attr("type") == "checkbox" && jQuery(this).is(':checked')) {
						data['canSave'] = 1;
					}else {
						data['canSave'] = 0;
					}
				})
				data['permissions'] = [];
				jQuery('#useredit-form').find('input.permission-checkbox').each(function() {
					if(jQuery(this).attr("type") == "checkbox" && jQuery(this).is(':checked')) {
						data['permissions'].push(jQuery(this).val());
					}
				})
				if(data['permissions'].length == 0) {
					data['permissions'] = "";
				}

				if(data["userid"].trim() != "" && (!mailExists || data["usermail"].trim() != "")) {
					jQuery.ajax({
						type: 'POST',
						url: ajaxurl,
						data: data,
						success: function (data) {
							if(data == 1) {
								userFeedback('usereditsave-button', 'success');
								setTimeout(() => {
									var url = new URL(jQuery('#user-detail-backlink').attr("href"));
									const urlParams = new URLSearchParams(url.search);
									window.location.href = window.location.href + "&backlink=" + urlParams.get("page_id");
								}, 1000);
								
							} else {
								userFeedback('usereditsave-button', 'error');
								console.error(data);
							}
							setTimeout(() => {
								userFeedback('usereditsave-button', 'text');
							}, 1500);
						},
						error: function (errorThrown) {
							userFeedback('usereditsave-button', 'error');
							console.error(errorThrown);
							setTimeout(() => {
								userFeedback('usereditsave-button', 'text');
							}, 1500);
						}
					});
				} else {
					userFeedback('usereditsave-button', 'error');
					setTimeout(() => {
						userFeedback('usereditsave-button', 'text');
					}, 1500);
				}	
			} else {
				if(mailExists) {
					jQuery('#input-field-mail').parent().find('input').first().addClass('is-invalid');
					jQuery('#input-field-mail').parent().find('.invalid-feedback').first().removeClass('d-none');
				}
				userFeedback('usereditsave-button', 'error');
				setTimeout(() => {
					userFeedback('usereditsave-button', 'text');
				}, 1500);
			}
		});

		jQuery(document).on('click', '.deleteuserbutton', function (e) {
			userFeedback('deleteuser-button', 'loading');
			let userid = jQuery("#input-field-userid").val();

			if(userid.trim() != "") {
				if(confirm("Es wird der User mit all seinen Eingaben gelöscht.\nMöchten Sie das wirklich?")) {
					jQuery.ajax({
						type: 'POST',
						url: ajaxurl,
						data: {
							"action": "user_details_delete",
							"userid": userid,
						},
						success: function (data) {
							if(data == 1) {
								userFeedback('deleteuser-button', 'success');
								setTimeout(() => {
									window.location.href = user_list_link;
								}, 1000);
							} else {
								userFeedback('deleteuser-button', 'error');
								console.error(data);
							}
							setTimeout(() => {
								userFeedback('deleteuser-button', 'text');
							}, 1500);
						},
						error: function (errorThrown) {
							userFeedback('deleteuser-button', 'error');
							console.error(errorThrown);
							setTimeout(() => {
								userFeedback('deleteuser-button', 'text');
							}, 1500);
						}
					});					
				} else {
					userFeedback('deleteuser-button', 'text');
				}
			} else {
				userFeedback('deleteuser-button', 'text');
			}
		
		});		

		jQuery(document).on('click', '.resend-registration-mail', function (e) {
			var userid = jQuery(this).attr('userid');

			if(userid != "" && userid != "0") {
				userFeedback('resend-registration-mail', 'loading');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "user_details_resend_registration",
						"userid": userid
					},
					success: function (data) {
						if(data == "1") {
							userFeedback('resend-registration-mail', 'success');

							setTimeout(() => {
								userFeedback('resend-registration-mail', 'text');
							}, 1500);
							
						} else {
							userFeedback('resend-registration-mail', 'error');
							setTimeout(() => {
								userFeedback('resend-registration-mail', 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback('resend-registration-mail', 'error');
						setTimeout(() => {
							userFeedback('resend-registration-mail', 'text');
						}, 1500);
					}
				});

			} else {
				userFeedback('resend-registration-mail', 'error');
				setTimeout(() => {
					userFeedback('resend-registration-mail', 'text');
				}, 1500);
			}
		});
		//ende Benutzer bearbeiten

		//start coach user trainings mgr BACKEND ################################################

		jQuery(document).on('submit', '#modal-addlektionen-backend-form', function (e) {
			e.preventDefault();
			filterLektionenBackend();
		});

		jQuery(document).on("change paste keyup", "#modal-addlektionen-backend-input-search", function(e) {
			setTimeout(function(){ 
				filterLektionenBackend();	
			}, 500);
		  });

		jQuery(document).on('click', '#clear-addlektionen-backend-search-button', function (e) {
			jQuery('#modal-addlektionen-backend-input-search').val('');
			filterLektionenBackend();
		});

		function filterLektionenBackend() {
			var tmps = ""+jQuery('#modal-addlektionen-backend-input-search').val().toLowerCase();
				
			//console.log(jQuery(this).val());
			jQuery('#modal-addlektionen-backend-check-lektionen div').removeClass('d-flex').addClass('d-none');
	
			jQuery('#modal-addlektionen-backend-check-lektionen div label').each(function (e) {
			if(jQuery(this).html().toLowerCase().indexOf(tmps) >= 0) {
				jQuery(this).parent().addClass('d-flex').removeClass('d-none');
			}
			});
		}

		//modal ausgeben
		jQuery(document).on('click', '#backend-trainings-mgr-lektion-add', function (e) {
			userFeedback('backend-trainings-mgr-lektion-add', 'loading');
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					"action": "backend_trainings_mgr_gettraining_lektionen",
				},
				success: function (data) {
					userFeedback('backend-trainings-mgr-lektion-add', 'success');
					setTimeout(() => {
						userFeedback('backend-trainings-mgr-lektion-add', 'text');
					}, 1500);
					jQuery("#lektionsmodalBackend").html(data);

					var lektionen = {};

					jQuery("#sortable-user-trainings > li").each(function (index) {
						let lektionid = "" + $(this).val();
						lektionen[lektionid] = lektionid;
					});

					jQuery('#sortable-user-trainings-select-lektionen').find('.modal-select-lektionen-input').each(function(index) {
						if(lektionen[jQuery(this).val()] != undefined) {
							jQuery(this).parent().remove();
						}
					});

					jQuery('#addLektionModalBackend').modal('show');
				},
				error: function (errorThrown) {
					//alert('error');
					userFeedback('backend-trainings-mgr-lektion-add', 'error');
					setTimeout(() => {
						userFeedback('backend-trainings-mgr-lektion-add', 'text');
					}, 1500);
					console.error(errorThrown);
				}
			});
		});

		jQuery(document).on('click', '.backendaddlektion', function (e) {
			let lektionen = [];

			jQuery(this).parent().parent().parent().find('#sortable-user-trainings-select-lektionen input:checkbox:checked').each(function (index) {
				lektionen.push(jQuery(this).val());
			});

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					"action": "backend_trainings_mgr_add_lektionen",
					"lektionen": lektionen,
					"trainingid": jQuery("#post_ID").val()
				},
				success: function (data) {
					jQuery("#sortable-user-trainings").append(data);
					$(".connectedSortable").sortable().disableSelection();
				},
				error: function (errorThrown) {
					console.error(errorThrown);
				}
			});
		});

		jQuery(document).on('submit', '#modal-addseiten-backend-form', function (e) {
			e.preventDefault();
			filterSeitenBackend();
		});

		jQuery(document).on("change paste keyup", "#modal-addseiten-backend-input-search", function(e) {
			setTimeout(function(){ 
				filterSeitenBackend();	
			}, 500);
		  });

		jQuery(document).on('click', '#clear-addseiten-backend-search-button', function (e) {
			jQuery('#modal-addseiten-backend-input-search').val('');
			filterSeitenBackend();
		});

		function filterSeitenBackend() {
			var tmps = ""+jQuery('#modal-addseiten-backend-input-search').val().toLowerCase();
				
			jQuery('#modal-addseiten-backend-check-seiten div').removeClass('d-flex').addClass('d-none');
	
			jQuery('#modal-addseiten-backend-check-seiten div label').each(function (e) {
			if(jQuery(this).html().toLowerCase().indexOf(tmps) >= 0) {
				jQuery(this).parent().addClass('d-flex').removeClass('d-none');
			}
			});
		}

		//modal ausgeben
		jQuery(document).on('click', '.backend-trainings-mgr-seiten-add', function (e) {
			var button = jQuery(this);
			userFeedback(button, 'loading');
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					"action": "backend_trainings_mgr_gettraining_seiten",
					"lektionid": jQuery(this).parent().parent().val()
				},
				success: function (data) {
					userFeedback(button, 'success');
					setTimeout(() => {
						userFeedback(button, 'text');
					}, 1500);
					jQuery("#lektionsmodalBackend").html(data);

					var seiten = {};
					jQuery('#sortable-user-trainings li ul li').each(function (e) {
						var seitenid = jQuery(this).val();
						seiten[seitenid] = seitenid;
					});

					jQuery('#sortable-user-trainings-select-seiten').find('.modal-select-seiten-input').each(function(index) {
						if(seiten[jQuery(this).val()] != undefined) {
							jQuery(this).parent().remove();
						}
					});

					jQuery('#addSeitenModalBackend').modal('show');
				},
				error: function (errorThrown) {
					//alert('error');
					userFeedback(button, 'error');
					setTimeout(() => {
						userFeedback(button, 'text');
					}, 1500);
					console.error(errorThrown);
				}
			});
		});


		jQuery(document).on('click', '.backendaddseiten', function (e) {
			let button = jQuery(this);
			jQuery(this).parent().parent().parent().find('#sortable-user-trainings-select-seiten input:checkbox:checked').each(function (index) {

				jQuery("#sortable-user-trainings #lektion" + button.val() + " .connectedSortable").
				append('<li class="ui-state-default training-mgr-lektionslite-item ui-sortable-handle" value="' + jQuery(this).val() + '" data-lektion-id="' + button.val() + '"><i class="fas fa-stream"></i><span>' + jQuery(this).attr("name") + '</span></li>');

				$(".connectedSortable-user").sortable().disableSelection();
				countTrainingLektion();
				//}
			});
		});



		jQuery(document).on('click', '#backend-trainings-mgr-training-save', function (e) {
			userFeedback('backend-trainings-mgr-training-save', 'loading');
			var lektionen = {};
			var i = 0;
			jQuery("#sortable-user-trainings > li").each(function (index) {
				let lektionid = "" + $(this).val();
				lektionen[i] = {
					"id": lektionid,
					"seiten": new Array()
				};
				$(this).find(".connectedSortable li").each(function (index2) {
					let seitenid = $(this).val();
					lektionen[i]["seiten"].push({
						"id": seitenid
					});
				});
				i++;
			});

			let lektionstr = JSON.stringify(lektionen);
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					"action": "backend_trainings_mgr_save_training",
					"lektionen": lektionstr,
					"trainingid": jQuery("#post_ID").val()
				},
				success: function (data) {
					if(data == "1") {
						userFeedback('backend-trainings-mgr-training-save', 'success');
						setTimeout(() => {
							userFeedback('backend-trainings-mgr-training-save', 'text');
							location.reload();
						}, 1500);
					} else {
						userFeedback('backend-trainings-mgr-training-save', 'error');
						setTimeout(() => {
							userFeedback('backend-trainings-mgr-training-save', 'text');
						}, 1500);
					}
				},
				error: function (errorThrown) {
					console.error(errorThrown);
					userFeedback('backend-trainings-mgr-training-save', 'error');
					setTimeout(() => {
						userFeedback('backend-trainings-mgr-training-save', 'text');
					}, 1500);
				}
			});

		});

		jQuery(document).on('click', '#checkbox-trainings-mgr-options-showNavigation', function () {
			if(jQuery('#checkbox-trainings-mgr-options-showNavigation').is(':checked')) {
				jQuery('#checkbox-trainings-mgr-options-showPagination').removeAttr("disabled");
				jQuery('#navigation-type-all').removeAttr("disabled");
				jQuery('#navigation-type-all-circle').removeAttr("disabled");
				jQuery('#navigation-type-all-bar').removeAttr("disabled");
				jQuery('#navigation-type-fcl').removeAttr("disabled");
				jQuery('#navigation-type-neighbors').removeAttr("disabled");
			} else {
				jQuery('#checkbox-trainings-mgr-options-showPagination').attr("disabled", true);
				jQuery('#navigation-type-all').attr("disabled", true);
				jQuery('#navigation-type-all-circle').attr("disabled", true);
				jQuery('#navigation-type-all-bar').attr("disabled", true);
				jQuery('#navigation-type-fcl').attr("disabled", true);
				jQuery('#navigation-type-neighbors').attr("disabled", true);
			}
		});

		jQuery(document).on('click', '#checkbox-trainings-mgr-options-showPagination', function () {
			if(jQuery('#checkbox-trainings-mgr-options-showPagination').is(':checked')) {
				jQuery('#navigation-type-all').removeAttr("disabled");
				jQuery('#navigation-type-all-circle').removeAttr("disabled");
				jQuery('#navigation-type-all-bar').removeAttr("disabled");
				jQuery('#navigation-type-fcl').removeAttr("disabled");
				jQuery('#navigation-type-neighbors').removeAttr("disabled");
				jQuery('#navigation-type-dropdown').removeAttr("disabled");
			} else {
				jQuery('#navigation-type-all').attr("disabled", true);
				jQuery('#navigation-type-all-circle').attr("disabled", true);
				jQuery('#navigation-type-all-bar').attr("disabled", true);
				jQuery('#navigation-type-fcl').attr("disabled", true);
				jQuery('#navigation-type-neighbors').attr("disabled", true);
				jQuery('#navigation-type-dropdown').attr("disabled", true);
			}
		});

		jQuery(document).on('click', '#trainings-mgr-options-save-button', function(e){
			e.preventDefault();

			userFeedback('trainings-mgr-options-save-button', 'loading');

			var showLektionsliste = "";
			if(jQuery('#checkbox-trainings-mgr-options-lektionsliste').is(':checked')){
				showLektionsliste = "1";
			}
			else{
				showLektionsliste = "0";
			}

			var showNavigation = "0";
			if(jQuery('#checkbox-trainings-mgr-options-showNavigation').is(':checked')){
				showNavigation = "1";
			}

			var showTrainingName = "";
			if(jQuery('#checkbox-show-training-name').is(':checked')){
				showTrainingName = "1";
			}

			var showPagination = "0";
			if(jQuery('#checkbox-trainings-mgr-options-showPagination').is(':checked')){
				showPagination = "1";
			}

			var showLektionenButton = "0"; 
			if(jQuery('#checkbox-trainings-mgr-options-showLektionenButton').is(':checked')){
				showLektionenButton = "1";
			}

			var contentPlacement = "";
			if(jQuery('#radio-content-above-list').is(':checked')){
				contentPlacement = "top";
			}
			else if(jQuery('#radio-content-below-list').is(':checked')){
				contentPlacement = "bottom";
			}

			var skipDashboard = "";
			if(jQuery('#checkbox-trainings-mgr-options-skipdashboard').is(':checked')){
				skipDashboard = "1";
			}

			var disableUserEvents = "";
			if(jQuery('#checkbox-trainings-mgr-options-disableUserEvents').is(':checked')){
				disableUserEvents = "1";
			}

			var showAppSleepMultiSave = "";
			if(jQuery('#checkbox-trainings-mgr-options-showAppSleepMultiSave').is(':checked')){
				showAppSleepMultiSave = "1";
			}

			var showAppSleepSingleSave = "";
			if(jQuery('#checkbox-trainings-mgr-options-showAppSleepSingleSave').is(':checked')){
				showAppSleepSingleSave = "1";
			}

			var navigationType = "";
			if(jQuery('input[name="trainings-mgr-options-navigation-type"]:checked').length === 1){
				navigationType = jQuery('input[name="trainings-mgr-options-navigation-type"]:checked').val();
			}
			
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					"action": "backend_trainings_mgr_save_options",
					"showLektionsliste": showLektionsliste,
					"showNavigation": showNavigation,
					"showTrainingName": showTrainingName,
					"showPagination": showPagination,
					"showLektionenButton": showLektionenButton,
					"contentPlacement": contentPlacement,
					"skipDashboard": skipDashboard,
					"disableUserEvents": disableUserEvents,
					"showAppSleepMultiSave": showAppSleepMultiSave,
					"showAppSleepSingleSave": showAppSleepSingleSave,
					"navigationType": navigationType,
					"trainingid": jQuery("#post_ID").val()
				},
				success: function(data){
					console.log(data);
					userFeedback('trainings-mgr-options-save-button', 'success');
					setTimeout(() => {
						userFeedback('trainings-mgr-options-save-button', 'text');
					}, 1500);
				},
				error: function(errorThrown){
					console.error(errorThrown);
					userFeedback('trainings-mgr-options-save-button', 'error');
					setTimeout(() => {
						userFeedback('trainings-mgr-options-save-button', 'text');
					}, 1500);
				}
			});
			
		});



		// ende coach user trainings mgr BACKEND ################################################

		// Inhalt an Führungskraft/kAdmin freigeben / forward_content START
		
		// Speichern der ID des freigegebenen Formulars
		jQuery(document).on('click', '.forwardcontent-button', function(e){
			var thisButton = jQuery(this);
			userFeedback(thisButton, 'loading');
			var formId = jQuery(this).parent().find("input").val();
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					"action": "forward_content_saveforwardcontentid",
					"formid": formId
				},
				success: function(data){
					if(data == "1"){
						userFeedback(thisButton, 'success');
						setTimeout(()=> {
							userFeedback(thisButton, 'text');
							jQuery('[data-formid='+formId+']').addClass("form-frame");
							jQuery(thisButton).css('display', 'none');
							jQuery('#forwardContentButtonReset_'+formId).css('display', 'block');
						}, 1500);
					}
					else{
						userFeedback(thisButton, 'error');
						setTimeout(()=> {
							userFeedback(thisButton, 'text');
						}, 1500);
					}

				},
				error: function(errorThrown){
					userFeedback(thisButton, 'error');
					setTimeout(()=> {
						userFeedback(thisButton, 'text');
					}, 1500);
					console.error(errorThrown);
				}
			});
		});

		// Zurücksetzen der ID des zuvor freigegebenen Formulars
		jQuery(document).on('click', '.forwardcontent-button-reset', function(e){
			var thisButton = jQuery(this);
			userFeedback(thisButton, 'loading');
			var formId = jQuery(this).parent().find("input").val();
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					"action": "forward_content_resetforwardcontentid",
					"formid": formId
				},
				success: function(data){
					if(data == "1"){
						userFeedback(thisButton, 'success');
						setTimeout(()=> {
							userFeedback(thisButton, 'text');
							jQuery('[data-formid='+formId+']').removeClass("form-frame");
							jQuery(thisButton).css('display', 'none');
							jQuery('#forwardContentButton_'+formId).css('display', 'block');
						}, 1500);
					}
					else{
						userFeedback(thisButton, 'error');
						setTimeout(()=> {
							userFeedback(thisButton, 'text');
						}, 1500);
					}

				},
				error: function(errorThrown){
					userFeedback(thisButton, 'error');
					setTimeout(()=> {
						userFeedback(thisButton, 'text');
					}, 1500);
					console.error(errorThrown);
				}
			});
		});
		// Inhalt an Führungskraft/kAdmin freigeben / forward_content ENDE

		// GBU-Mittelwerte START

		// Verhalten Dropdown zum Wechseln der Gruppen
		jQuery(document).on('change', '.gbu-mittelwerte-select', function(e){
			var thisDropdown = this;
			var thisTable = jQuery(this).parent().parent().parent().parent();
			var gbuMittelwerteSelects = jQuery(this).parent().parent().find("select");
			var selectedOption = jQuery(this).find("option:selected").val();
	
			var oldVal = "0";
			var selectIndex = 0;
			jQuery(gbuMittelwerteSelects).each(function(index){
				if(this != thisDropdown){
					oldVal = jQuery(this).find("option:disabled").val();
					jQuery(this).find("option:disabled").attr("disabled", false);
					jQuery(this).find("option[value='"+selectedOption+"']").attr("disabled", true);
				}
				else{
					selectIndex = index;
				}
			});
	
			var tableRows = jQuery(thisTable).find("tbody tr");
			jQuery(tableRows).find("td[opt='"+selectIndex+"'][groupid='"+oldVal+"']").addClass("d-none");
			jQuery(tableRows).find("td[opt='"+selectIndex+"'][groupid='"+selectedOption+"']").removeClass("d-none");
	
		});
		// GBU-Mittelwerte ENDE

		// import csv  ################################################
		jQuery(document).on('change', '#csvImportFileInput', function(e) {
			var fileInput = jQuery('#csvImportFileInput');

			// reset everything for new file
			setCsvImportErrorDialog();
			//get the file name
			var fileName = e.target.files[0].name;
			//replace the "Choose a file" label
			jQuery(this).next('label').html(fileName);

			if(fileInput.get(0).files.length > 0) {
				var file = fileInput.get(0).files[0];

				// Test file type by first 4 bytes
				const extensionFR = new FileReader()
				extensionFR.onloadend = function(e) {
					if (e.target.readyState === FileReader.DONE) {
							const uint = new Uint8Array(e.target.result);
							let bytes = [];
							uint.forEach((byte) => {
									bytes.push(byte.toString(16));
							})
							const hex = bytes.join('').toUpperCase();

							// 6E616D65 is hex for a csv file
							if (hex === '6E616D65') {
								var reader = new FileReader();

								reader.onload = function (e) {
									// split rows by new line
									var rows = reader.result.split(/\r\n|\n/);
									var splitchar = '';

									if(rows.length > 1) {
										// find split character for cols
										if(rows[0].indexOf(',') !== -1) {
											splitchar = ',';
										} else if(rows[0].indexOf(';') !== -1) {
											splitchar = ';';
										}
										if(splitchar === '') {
											setCsvImportErrorDialog("Diese Datei wird nicht unterstützt. Bitte wählen Sie eine komma- oder semikolonseparierte CSV-Datei.");
										} else {
											jQuery('#csvImportCheckSubmit').prop('disabled', false);
											jQuery('#csvImportTestArea').html('');
										}
									} else {
										setCsvImportErrorDialog("Diese Datei hat keinen Inhalt.");
									}
								}
								reader.readAsText(file);
							} else {
								setCsvImportErrorDialog("Diese Datei wird nicht unterstützt. Bitte wählen Sie eine komma- oder semikolonseparierte CSV-Datei.");
							}
						}
					}
				const blob = file.slice(0, 4);
				extensionFR.readAsArrayBuffer(blob);
			} else {
				setCsvImportErrorDialog("Bitte wählen Sie eine CSV zum Import.");
			}
		});

		function setCsvImportErrorDialog(message=null) {
			var errorArea = jQuery('#csvImportErrorArea');

			if(message == null) {
				jQuery(errorArea).addClass('d-none');
			} else {
				jQuery(errorArea).removeClass('d-none');
				scrollToTop(jQuery('#csvImportUploadArea').offset().top);
				errorArea.html('<h4 class="mt-0 mb-2"><i class="fas fa-times-circle"></i>&nbsp;Fehler</h4><p class="mb-0">'+message+'</p>');
			}
			
			jQuery('#csvImportCheckSubmit').prop('disabled', true);
		}

		jQuery(document).on('click', '#csvImportRemoveFile', function (e) {
			csvImportResetFileInput(true);
		});

		function csvImportResetFileInput(clear_html, reset_alert = true) {
			jQuery('#csvImportBatchSize').prop("disabled", false);
			jQuery('#csvImportFileInput').val('');
			jQuery('#csvImportFileInput').next('label').html('CSV-Datei auswählen');
			jQuery('#csvImportFileInput').prop('disabled', false);
			if(reset_alert)
				setCsvImportErrorDialog();
			jQuery('#csvImportCheckSubmit').prop('disabled', true);
			jQuery('#csvImportSubmit').prop('disabled', true);
			if(clear_html)
				jQuery('#csvImportTestArea').html('');
		}

		jQuery(document).on('click', '#csvImportCheckSubmit', function (e) {
			userFeedback('csvImportCheckSubmit', 'loading');
			jQuery('#csvImportFileInput').prop('disabled', true);
			jQuery('#csvImportTestArea').html('');
			jQuery('#csvImportSubmit').prop('disabled', true);

			var data = new FormData();
			data.append('action', 'csvimport_test');
			data.append('file', jQuery('#csvImportFileInput').get(0).files[0]);

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				processData: false,
				contentType: false,
				success: function(data){
					try {
						var res = jQuery.parseJSON(data);
						if(res != undefined && res != null) {

							if(res.status == 'success') {
								setCsvImportErrorDialog();
								scrollToTop(jQuery('#csvImportUploadArea').offset().top);
								jQuery('#csvImportSubmit').prop('disabled', false);
							} else {
								setCsvImportErrorDialog("Diese Datei kann nicht importiert werden. Es sind Fehler bei einzelnen Datensätzen aufgetreten.")
								jQuery('#csvImportFileInput').prop('disabled', false);
							}

							jQuery('#csvImportTestArea').html(res.code);
							jQuery('[data-toggle="tooltip"]').tooltip();

							userFeedback('csvImportCheckSubmit', 'success');
							setTimeout(()=> {
								userFeedback('csvImportCheckSubmit', 'text');
							}, 1500);
						} else {
							userFeedback('csvImportCheckSubmit', 'error');
							setTimeout(()=> {
								userFeedback('csvImportCheckSubmit', 'text');
							}, 1500);
						}
					} catch(e) {
						userFeedback('csvImportCheckSubmit', 'error');
						setTimeout(()=> {
							userFeedback('csvImportCheckSubmit', 'text');
						}, 1500);
					}
				},
				error: function(errorThrown){
					userFeedback('csvImportCheckSubmit', 'error');
					setTimeout(()=> {
						userFeedback('csvImportCheckSubmit', 'text');
					}, 1500);
					console.error(errorThrown);
				}
			});
		});

		jQuery(document).on('click', '#csvImportSubmit', function (e) {
			jQuery('#csvImportBatchSize').prop("disabled", true);
			userFeedback('csvImportSubmit', 'loading');
			
			sendCsvImport(0);
		});

		function sendCsvImport(startIndex) {

			var batchSize = parseInt(jQuery('#csvImportBatchSize').val());

			var data = {};

			var total = jQuery('#csvImportTestArea').find('.csv-import-row').length;
			var limit = 0;
			if((startIndex + batchSize) < total) {
				limit = startIndex + batchSize;
			} else {
				limit = total;
			}

			for(var i = startIndex; i < limit; i++) {
				// Collect data
				data[i] = {};
				var j = 0;
				jQuery('#csv-import-row-' + i).find('.csv-import-cell').each(function (e) {
					data[i][j] = jQuery(this).attr('data-value');
					j++;
				});

				// Animate Loading for selected cells
				jQuery('#csv-import-row-' + i).find('.csvImportTableCell').html('<span class="userfeedback-span userfeedback-loading"><i class="fas fa-spinner align-middle"></i></span>');

				// Scroll to first row in batch
				if(i == startIndex) {
					scrollToTop(jQuery('#csv-import-row-' + i + ' td').first().offset().top);
				}
			}
			
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					action: "csvimport_import",
					content: JSON.stringify(data)
				},
				success: function(data){
					try {
						var res = jQuery.parseJSON(data);
						if(res != undefined && res != null) {

							if(res.success != undefined && res.success != null) {
								jQuery.each(res.success, function(key, value) {
									jQuery('#csv-import-row-'+key).find('.csvImportTableCell').first().html('<i data-toggle="tooltip" title="'+ value +'" class="fas fa-check-circle text-success"></i>');
								});
							}

							if(res.failed != undefined && res.failed != null) {
								jQuery.each(res.failed, function(key, value) {
									jQuery('#csv-import-row-'+key).find('.csvImportTableCell').first().html('<i data-toggle="tooltip" title="'+ value +'" class="fas fa-times-circle text-danger"></i>');
								})
							}

							jQuery('[data-toggle="tooltip"]').tooltip();

							if(limit < total) {
								sendCsvImport(limit);
							} else {
								endCsvImport();
							}
							
						} else {
							userFeedback('csvImportSubmit', 'error');
							setCsvImportErrorDialog("Es ist ein unbekannter Fehler aufgetreten.");
							setTimeout(()=> {
								userFeedback('csvImportSubmit', 'text');
							}, 1500);
						}
					} catch(e) {
						userFeedback('csvImportSubmit', 'error');
						setCsvImportErrorDialog("Es ist ein Fehler aufgetreten.");
						setTimeout(()=> {
							userFeedback('csvImportSubmit', 'text');
						}, 1500);
					}
				},
				error: function(errorThrown){
					setCsvImportErrorDialog("Es ist ein Fehler in der Kommunikation aufgetreten.");
					userFeedback('csvImportSubmit', 'error');
					setTimeout(()=> {
						userFeedback('csvImportSubmit', 'text');
					}, 1500);
					console.error(errorThrown);
				}
			});
		}

		function endCsvImport() {
			scrollToTop(jQuery('#csvImportUploadArea').offset().top);

			var successed = true;
			if(jQuery('#csvImportTestArea').find('.csv-import-row .csvImportTableCell .fa-times-circle').length > 0) {
				successed = false;
			}

			if(successed) {
				userFeedback('csvImportSubmit', 'success');
				setTimeout(()=> {
					userFeedback('csvImportSubmit', 'text');
					csvImportResetFileInput(false);
				}, 2500);
			} else {
				userFeedback('csvImportSubmit', 'error');
				setCsvImportErrorDialog("Es konnten nicht alle Datensätze erfolgreich importiert werden. Versuchen Sie es bei Bedarf erneut.");
				setTimeout(()=> {
					userFeedback('csvImportSubmit', 'text');
					csvImportResetFileInput(false, false);
				}, 2500);
			}

			
		}
		// ende import csv  ################################################

		// start CSV-Export Training #######################################
		jQuery(document).on("click", ".csv-download-start", function (e) {
			document.getElementById("csv-download").classList.remove("csv-download-start");
			var xhr = new XMLHttpRequest();
			xhr.open('POST', ajaxurl, true);
			xhr.responseType = 'arraybuffer';
			xhr.onload = function () {
					if (this.status === 200) {
						if(this.response.byteLength > 1) {
							var filename = "";
							var disposition = xhr.getResponseHeader('Content-Disposition');
							if (disposition && disposition.indexOf('attachment') !== -1) {
									var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
									var matches = filenameRegex.exec(disposition);
									if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
							}
							var type = xhr.getResponseHeader('Content-Type');

							var blob = typeof File === 'function'
									? new File([this.response], filename, { type: type })
									: new Blob([this.response], { type: type });
							if (typeof window.navigator.msSaveBlob !== 'undefined') {
									// IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
									window.navigator.msSaveBlob(blob, filename);
							} else {
									var URL = window.URL || window.webkitURL;
									var downloadUrl = URL.createObjectURL(blob);

									if (filename) {
											// use HTML5 a[download] attribute to specify filename
											var a = document.createElement("a");
											// safari doesn't support this yet
											if (typeof a.download === 'undefined') {
													window.location = downloadUrl;
											} else {
													a.href = downloadUrl;
													a.download = filename;
													document.body.appendChild(a);
													a.click();
											}
									} else {
											window.location = downloadUrl;
									}

									setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
							}
						}
					}
					document.getElementById("csv-download").classList.add("csv-download-start");
			};
			xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			var data = document.getElementById("csv-download-data").value;
			var singleuser = document.getElementById("csv-download-singleuser").value;
			var groupbygroups = document.getElementById("csv-download-groupbygroups").value;
			xhr.send("action=coach_download_csv&data="+data+"&singleuser="+singleuser+"&groupbygroups="+groupbygroups);
		});
		// ende CSV-Export Training +#################################################

		// start Training Ex-/import #################################################

		jQuery(document).on('click', '.mediendownload', function (e) {
			$('#trainingsexportloading').modal('show');

			var xhr = new XMLHttpRequest();
			xhr.open('POST', ajaxurl, true);
			xhr.responseType = 'arraybuffer';
			xhr.onload = function () {
					$('#trainingsexportloading').modal('hide');
					if (this.status === 200) {
						if(this.response.byteLength > 1) {
							var filename = "";
							var disposition = xhr.getResponseHeader('Content-Disposition');
							if (disposition && disposition.indexOf('attachment') !== -1) {
									var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
									var matches = filenameRegex.exec(disposition);
									if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
							}
							var type = xhr.getResponseHeader('Content-Type');

							var blob = typeof File === 'function'
									? new File([this.response], filename, { type: type })
									: new Blob([this.response], { type: type });
							if (typeof window.navigator.msSaveBlob !== 'undefined') {
									// IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
									window.navigator.msSaveBlob(blob, filename);
							} else {
									var URL = window.URL || window.webkitURL;
									var downloadUrl = URL.createObjectURL(blob);

									if (filename) {
											// use HTML5 a[download] attribute to specify filename
											var a = document.createElement("a");
											// safari doesn't support this yet
											if (typeof a.download === 'undefined') {
													window.location = downloadUrl;
											} else {
													a.href = downloadUrl;
													a.download = filename;
													document.body.appendChild(a);
													a.click();
											}
									} else {
											window.location = downloadUrl;
									}

									setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
							}
						} else {
							alert("Fehler! Es konnte keine ZIP-Datei generiert werden.")
						}
					}
			};
			xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			xhr.send("action=trainings_ex_import_download_medien&trainingid="+jQuery(this).find("input").first().val());
		});

		jQuery(document).on('click', '.importMedienButton', function (e) {
			document.getElementById('successArea').className = "d-none alert alert-success";
			document.getElementById('errorArea').className = "d-none alert alert-success";
			jQuery('#importMedienModal').modal('hide');
			jQuery('#trainingimportloading').modal('show');


			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					"action": "trainings_ex_import_import_medien",
					"file": jQuery('#medienDatei').val()
				},
				success: function (data) {
					jQuery('#trainingimportloading').modal('hide');
					if(data == "success") {
						document.getElementById('successArea').className = "alert alert-success";
						document.getElementById('successArea').innerHTML = "Die Mediendatei wurde erfolgreich importiert.";
					} else {
						document.getElementById('errorArea').className = "alert alert-danger";
						document.getElementById('errorArea').innerHTML = "Es ist ein Fehler aufgetreten.";
					}
				},
				error: function (errorThrown) {
					jQuery('#trainingimportloading').modal('hide');
					document.getElementById('errorArea').className = "alert alert-danger";
					document.getElementById('errorArea').innerHTML = "Es ist ein unbekannter Fehler aufgetreten.";
				}
			});
		});

		jQuery(document).on('click', '.trainingdownload', function (e) {
			$('#trainingsexportloading').modal('show');

			var xhr = new XMLHttpRequest();
			xhr.open('POST', ajaxurl, true);
			xhr.responseType = 'arraybuffer';
			xhr.onload = function () {
					$('#trainingsexportloading').modal('hide');
					if (this.status === 200) {
						if(this.response.byteLength > 1) {
							var filename = "";
							var disposition = xhr.getResponseHeader('Content-Disposition');
							if (disposition && disposition.indexOf('attachment') !== -1) {
									var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
									var matches = filenameRegex.exec(disposition);
									if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
							}
							var type = xhr.getResponseHeader('Content-Type');

							var blob = typeof File === 'function'
									? new File([this.response], filename, { type: type })
									: new Blob([this.response], { type: type });
							if (typeof window.navigator.msSaveBlob !== 'undefined') {
									// IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
									window.navigator.msSaveBlob(blob, filename);
							} else {
									var URL = window.URL || window.webkitURL;
									var downloadUrl = URL.createObjectURL(blob);

									if (filename) {
											// use HTML5 a[download] attribute to specify filename
											var a = document.createElement("a");
											// safari doesn't support this yet
											if (typeof a.download === 'undefined') {
													window.location = downloadUrl;
											} else {
													a.href = downloadUrl;
													a.download = filename;
													document.body.appendChild(a);
													a.click();
											}
									} else {
											window.location = downloadUrl;
									}

									setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
							}
						}
					}
			};
			xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			xhr.send("action=trainings_ex_import_download_training&trainingid="+jQuery(this).find("input").first().val());
		});

		jQuery(document).on('change', '#trainingDatei', function (e) {
			if(jQuery('#trainingDatei').get(0).files[0] != undefined) {
				document.getElementById('import_nofilewarning').className = "d-none alert alert-danger";
			}
		});

		jQuery(document).on('click', '.importTrainingButton', function (e) {
			document.getElementById('import_nofilewarning').className = "d-none alert alert-danger";
			if(jQuery('#trainingDatei').get(0).files[0] != undefined) {
				document.getElementById('successArea').className = "d-none alert alert-success";
				document.getElementById('errorArea').className = "d-none alert alert-danger";
				jQuery('#importTrainingModal').modal('hide');
				jQuery('#trainingimportloading').modal('show');

				var data = new FormData();
				data.append('action', 'trainings_ex_import_import_training');
				data.append('file', jQuery('#trainingDatei').get(0).files[0]);


				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: data,
					processData: false,
					contentType: false,
					success: function (data) {
						jQuery('#trainingimportloading').modal('hide');
						if(data == "success") {
							document.getElementById('successArea').className = "alert alert-success";
							document.getElementById('successArea').innerHTML = "Die Trainingsdatei wurde erfolgreich importiert. Laden Sie die Seite neu, um die Änderungen zu sehen.";
						} else {
							document.getElementById('errorArea').className = "alert alert-danger";
							document.getElementById('errorArea').innerHTML = "Es ist ein Fehler aufgetreten.";
						}
					},
					error: function (errorThrown) {
						jQuery('#trainingimportloading').modal('hide');
						document.getElementById('errorArea').className = "alert alert-danger";
						document.getElementById('errorArea').innerHTML = "Es ist ein unbekannter Fehler aufgetreten.";
					}
				});
			} else {
				document.getElementById('import_nofilewarning').className = "alert alert-danger";
			}
		
		});

		jQuery(document).on('click', '.trainingduplicate', function (e) {
			jQuery('#duplicateTrainingId').val(jQuery(this).find("input").first().val());

			jQuery('#duplicateTrainingModal').modal('show');
		});

		jQuery(document).on('click', '.duplicateTrainingButton', function (e) {
			var trainingId = jQuery('#duplicateTrainingId').val();
			var trainingAppend = jQuery('#duplicateTrainingAppend').val();
			var lektionAppend = jQuery('#duplicateLektionAppend').val();
			var seitenAppend = jQuery('#duplicateSeitenAppend').val();
			var tsformAppend = jQuery('#duplicateTrainingTsFormAppend').val();

			if(trainingId != undefined && trainingId != "" && trainingAppend != undefined && lektionAppend != undefined && seitenAppend != undefined && tsformAppend != undefined) {
				var button = jQuery('#duplicateTrainingButton');
				userFeedback(button, 'loading');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "trainings_ex_import_duplicate_training",
						"trainingid": trainingId,
						"trainingAppend": trainingAppend,
						"lektionAppend": lektionAppend,
						"seitenAppend": seitenAppend,
						"tsformAppend": tsformAppend
					},
					success: function (data) {
						if(data == "1") {
							userFeedback(button, 'success');
							setTimeout(() => {
								userFeedback(button, 'text');
								location.reload();
							}, 1500);
						} else {
							userFeedback(button, 'error');
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback(button, 'error');
						setTimeout(() => {
							userFeedback(button, 'text');
						}, 1500);
					}
				});
			} else {
				userFeedback('duplicateTrainingButton', 'error');
				setTimeout(() => {
					userFeedback('duplicateTrainingButton', 'text');
				}, 1500);
			}
		});

		// ende Training Ex-/import #################################################

		// start übungen ############################################################
		jQuery(document).on('click', '.backend-export-ts-exercise', function (e) {
			var exerciseid = jQuery(this).data('exerciseid');
			var button = jQuery(this);

			if(exerciseid == undefined || exerciseid == "") {
				userFeedback(button, 'error');
				setTimeout(() => {
					userFeedback(button, 'text');
				}, 1500);
			} else {
				userFeedback(button, 'loading');

				var xhr = new XMLHttpRequest();
				xhr.open('POST', ajaxurl, true);
				xhr.responseType = 'arraybuffer';
				xhr.onload = function () {
						if (this.status === 200) {
							if(this.response.byteLength > 1) {
								var filename = "";
								var disposition = xhr.getResponseHeader('Content-Disposition');
								if (disposition && disposition.indexOf('attachment') !== -1) {
										var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
										var matches = filenameRegex.exec(disposition);
										if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
								}
								var type = xhr.getResponseHeader('Content-Type');
	
								var blob = typeof File === 'function'
										? new File([this.response], filename, { type: type })
										: new Blob([this.response], { type: type });
								if (typeof window.navigator.msSaveBlob !== 'undefined') {
										// IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
										window.navigator.msSaveBlob(blob, filename);
								} else {
										var URL = window.URL || window.webkitURL;
										var downloadUrl = URL.createObjectURL(blob);
	
										if (filename) {
												// use HTML5 a[download] attribute to specify filename
												var a = document.createElement("a");
												// safari doesn't support this yet
												if (typeof a.download === 'undefined') {
														window.location = downloadUrl;
												} else {
														a.href = downloadUrl;
														a.download = filename;
														document.body.appendChild(a);
														a.click();
												}
										} else {
												window.location = downloadUrl;
										}
	
										setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
								}
							} else {
								userFeedback(button, 'error');
								setTimeout(() => {
									userFeedback(button, 'text');
								}, 1500);
							}
						
							userFeedback(button, 'success');
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);
						} else {
							userFeedback(button, 'error');
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);
						}
				};
				xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
				xhr.send("action=backend_exercise_export&exerciseid="+exerciseid);
			}
		});

		jQuery(document).on('click', '.backend-duplicate-ts-exercise', function (e) {
			var exerciseid = jQuery(this).data('exerciseid');
			var button = jQuery(this);

			if(exerciseid == undefined || exerciseid == "") {
				userFeedback(button, 'error');
				setTimeout(() => {
					userFeedback(button, 'text');
				}, 1500);
			} else {
				userFeedback(button, 'loading');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "backend_exercise_duplicate",
						"exerciseid": exerciseid
					},
					success: function (data) {
						if(data != "") {
							userFeedback(button, 'success');
							location.href = data;
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);
						} else {
							userFeedback(button, 'error');
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback(button, 'error');
						setTimeout(() => {
							userFeedback(button, 'text');
						}, 1500);
					}
				});
			}
		});

		jQuery(document).on('hidden.bs.modal', '#backend-import-ts-exercise-modal', function (e) {
			jQuery('#importTsExerciseModalFile').val('');
		});

		jQuery(document).on('click', '#importTsExerciseClearButton', function (e) {
			jQuery('#importTsExerciseModalFile').val('');
		});

		jQuery(document).on('click', '#backend-import-ts-exercise-confirm', function (e) {
			if(jQuery('#importTsExerciseModalFile').get(0).files.length > 0 && jQuery('#importTsExerciseModalFile').get(0).files[0] != undefined) {
				userFeedback('backend-import-ts-exercise-confirm', 'loading');
	
				var data = new FormData();
				data.append('action', 'backend_exercise_import');
				for(var i = 0; i < jQuery('#importTsExerciseModalFile').get(0).files.length; i++) {
					data.append('file[]', jQuery('#importTsExerciseModalFile').get(0).files[i]);
				}
				
				
				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: data,
					processData: false,
					contentType: false,
					success: function (data) {
						if(data != "") {
							userFeedback('backend-import-ts-exercise-confirm', 'success');
							location.href = data;
							setTimeout(() => {
								userFeedback('backend-import-ts-exercise-confirm', 'text');
							}, 1500);
						} else {
							userFeedback('backend-import-ts-exercise-confirm', 'error');
							setTimeout(() => {
								userFeedback('backend-import-ts-exercise-confirm', 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback('backend-import-ts-exercise-confirm', 'error');
						setTimeout(() => {
							userFeedback('backend-import-ts-exercise-confirm', 'text');
						}, 1500);
					}
				});

			} else {
				jQuery('#importTsExerciseModalFile').addClass('is-invalid');
				userFeedback('backend-import-ts-exercise-confirm', 'error');
				setTimeout(() => {
					userFeedback('backend-import-ts-exercise-confirm', 'text');
				}, 1500);
			}
		});

		jQuery(document).on('change', '#importTsExerciseModalFile', function(e) {
			if(jQuery('#importTsExerciseModalFile').get(0).files.length > 0) {
				jQuery('#importTsExerciseModalFile').removeClass('is-invalid');
			}
		});
		// ende Übungen #############################################################

		// start Formulare ##########################################################

		// Frontend Formulare
		jQuery(document).on('click', '.ts-forms-multiformbutton', function (e) {
			var formid = jQuery(this).data('formid');
			var formindex = 0;
			jQuery(this).parent().parent().find('.ts-forms-formwrapper').each(function (e) {
				if(jQuery(this).data('formindex') > formindex) {
					formindex = jQuery(this).data('formindex');
				}
			});
			formindex++;

			if(formid != undefined && formindex >= 0) {

				var button = jQuery(this);
				userFeedback(button, 'loading');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "forms_get_multiform_append",
						"formid": formid,
						"index": formindex
					},
					success: function (data) {
						if(data != "") {
							userFeedback(button, 'success');
							var lastformgroup = jQuery(button).parent().parent().find('.ts-forms-formwrapper').last();
							jQuery(data).insertAfter(lastformgroup);
							tsFormsReorderOrdering(lastformgroup.parent());
							tsFormsDeleteDisable(lastformgroup.parent());
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);
						} else {
							userFeedback(button, 'error');
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback(button, 'error');
						setTimeout(() => {
							userFeedback(button, 'text');
						}, 1500);
					}
				});
			}
		});

		jQuery(document).on('focusout', '.ts-forms-element[type=number]', function (e) {
			var adjusted = false;
			if(jQuery(this).attr('min') !== undefined && jQuery(this).val().trim() != "" && parseFloat(jQuery(this).val()) < parseFloat(jQuery(this).attr('min'))) {
				jQuery(this).val(jQuery(this).attr('min'));
				adjusted = true;
			}
			if(jQuery(this).attr('max') !== undefined && jQuery(this).val().trim() != "" && parseFloat(jQuery(this).val()) > parseFloat(jQuery(this).attr('max'))) {
				jQuery(this).val(jQuery(this).attr('max'));
				adjusted = true;
			}
			if(adjusted) {
				jQuery(this).parent().append('<span class="text-danger">Die eingegebene Zahl lag außerhalb des erlaubten Bereichs und wurde deshalb korrigiert.</span>')
				setTimeout(() => {
					jQuery(this).parent().find('.text-danger').remove();
				}, 5000);
			}
		});

		jQuery(document).on('click', '.ts-forms-deletemultiform', function (e) {
			let formElement = jQuery(this).parent().parent().parent();
			jQuery(this).parent().parent().remove();
			tsFormsReorderOrdering(formElement);
			tsFormsDeleteDisable(formElement);
		});

		jQuery(document).on('click', '.ts-forms-multiform-ordering-up', function (e) {
			if(jQuery(this).parent().parent().prev()) {
				jQuery(this).parent().parent().insertBefore(jQuery(this).parent().parent().prev());
			}
			tsFormsReorderOrdering(jQuery(this).parent().parent().parent());
		})

		jQuery(document).on('click', '.ts-forms-multiform-ordering-down', function (e) {
			if(jQuery(this).parent().parent().next()) {
				jQuery(this).parent().parent().insertAfter(jQuery(this).parent().parent().next());
			}
			tsFormsReorderOrdering(jQuery(this).parent().parent().parent());
		})

		function tsFormsReorderOrdering(formElement) {
			let i = 0;
			if(formElement.find('.ts-forms-multiform-ordering-up').length > 0) {
				formElement.find('.ts-forms-formwrapper').each(function (e) {
					if(i == 0 && i == (formElement.find('.ts-forms-formwrapper').length-1)) {
						jQuery(this).find('.ts-forms-multiform-ordering-up').addClass("d-none");
						jQuery(this).find('.ts-forms-multiform-ordering-down').addClass("d-none");
					} else if(i == 0) {
						jQuery(this).find('.ts-forms-multiform-ordering-up').addClass("d-none");
						jQuery(this).find('.ts-forms-multiform-ordering-down').removeClass("d-none");
					} else if(i == (formElement.find('.ts-forms-formwrapper').length-1)) {
						jQuery(this).find('.ts-forms-multiform-ordering-up').removeClass("d-none");
						jQuery(this).find('.ts-forms-multiform-ordering-down').addClass("d-none");
					} else {
						jQuery(this).find('.ts-forms-multiform-ordering-up').removeClass("d-none");
						jQuery(this).find('.ts-forms-multiform-ordering-down').removeClass("d-none");
					}
					i++;
				})
			}
		}

		function tsFormsDeleteDisable(formElement) {
			formElement.find('.ts-forms-formwrapper .ts-forms-deletemultiform').removeClass("disabled");
			formElement.find('.ts-forms-formwrapper .ts-forms-deletemultiform').attr("disabled", false);
			
			if(formElement.find('.ts-forms-formwrapper').length == 1) {
				formElement.find('.ts-forms-formwrapper').first().find('.ts-forms-deletemultiform').addClass("disabled");
				formElement.find('.ts-forms-formwrapper').first().find('.ts-forms-deletemultiform').attr("disabled", true);
			}
		}

		jQuery(document).on('change paste keyup', '.ts-forms-element.is-invalid, .ts-forms-formwrapper.submitmanually .form-control.is-invalid', function (e) {
			var type = getFormType(jQuery(this));
			// These elements currently don't have a required attribute
			if(type != "slider" && type != "checkbox") {
				if(type == "radio" || type == "scale" || type == "matrix") {
					if(jQuery(this).prop('required') && jQuery('input[name='+jQuery(this).attr('name')+']:checked').length == 1) {
						jQuery('input[name='+jQuery(this).attr('name')+']').removeClass('is-invalid');
						if(type === "scale") {
							jQuery(this).parent().parent().parent().find('.help-block').addClass('d-none');
						} else {
							jQuery(this).parent().parent().find('.help-block').addClass('d-none');
						}
					}
				} else {
					if(jQuery(this).prop('required') && jQuery(this).val() != "") {
						jQuery(this).removeClass('is-invalid');
						jQuery(this).parent().find('.help-block').addClass('d-none');
					}
				}
			}
		});

		jQuery(document).on('click', '.ts-forms-submitmanually', function (e) {
			var link = jQuery(this).data('tsformslink');
			if(link == undefined) link = '';
			var identifier = jQuery(this).data('tsformsidentifier');
			if(identifier != undefined && identifier != "") {
				saveTsForms(jQuery(this), link, identifier, false);
			}
		})

		jQuery(document).on('click', '.tsformsSubmitButton', function(e) {
			userFeedback(jQuery(this), 'loading');
			saveTsForms(jQuery(this), jQuery(this).attr('data-link'), null, false);
		});

		jQuery(document).on('click', '.ts-forms-captcha-reload', function(e) {
			var button = jQuery(this);
			userFeedback(button, 'loading');

			var fieldid = jQuery(button).attr("data-fieldid");

				if(fieldid != "") {

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "forms_captcha_reload",
						"fieldid": fieldid
					},
					success: function (data) {
						if(data != "") {
							userFeedback(button, 'success');
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);
							jQuery('.ts-forms-captcha-image').attr("src", data);
						} else {
							userFeedback(button, 'error');
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback(button, 'error');
						setTimeout(() => {
							userFeedback(button, 'text');
						}, 1500);
					}
				});
			} else {
				userFeedback(button, 'error');
				setTimeout(() => {
					userFeedback(button, 'text');
				}, 1500);
			}
		});
		
		function saveTsForms(button, link, identifier, isLastPage) {
			var first = true;
			var success = true;
			var manualSubmit = true;
			var extendedFeedback = false;
			var extendedFeedbackModal = "";
			
			userFeedback(button, 'loading');

			jQuery('.ts-forms-captcha').removeClass('is-invalid');
			jQuery('.ts-forms-captcha').parent().find('.help-block.text-danger').addClass("d-none");

			if(identifier == null) {
				identifier = '.ts-forms-element';
				manualSubmit = false;
			}

			if(jQuery(identifier).length > 0) {

				if(jQuery(button).attr('data-extendedfeedback') == "true") {
					extendedFeedback = true;
					extendedFeedbackModal = '#tsformsExtendedFeedbackModal-'+jQuery(button).attr('data-extendedfeedbackModalid');
					jQuery(extendedFeedbackModal).find('.tsformsExtendedFeedbackModalText').html('');
				}				

				if(extendedFeedback) {
					jQuery(extendedFeedbackModal+'-icon').html('<span class="userfeedback-loading"><i class="fas fa-spinner align-middle" style="font-size:6rem !important"></i></span>');
					jQuery(extendedFeedbackModal).modal('show');
					jQuery(extendedFeedbackModal).data('bs.modal')._config.keyboard = false;
					jQuery(extendedFeedbackModal).data('bs.modal')._config.backdrop = 'static';
				}

				jQuery('.ts-forms-formwrapper').each(function (e) {
					jQuery(this).find(identifier).each(function (f) {
						var type = getFormType(jQuery(this));
						// These elements currently don't have a required attribute
						if(type != "slider" && type != "checkbox") {
							if(type == "radio" || type == "scale" || type == "matrix") {
								if(jQuery(this).prop('required') && !jQuery(this).prop('disabled') && jQuery('input[name='+jQuery(this).attr('name')+']:checked').length == 0) {
									jQuery('input[name='+jQuery(this).attr('name')+']').addClass('is-invalid');
									if(type === "scale" && !jQuery('input[name='+jQuery(this).attr('name')+']').first().hasClass('ts-forms-scale-nonvisual')) {
										jQuery(this).parent().parent().parent().find('.help-block').removeClass('d-none');
									} else {
										jQuery(this).parent().parent().find('.help-block').removeClass('d-none');
									}
									

									if(first) {
										scrollToTop(jQuery(this).parent().parent().offset().top);
										first = false;
									}
									success = false;
								}
							} else {
								if(jQuery(this).prop('required') && !jQuery(this).prop('disabled') && jQuery(this).val() == "") {
									jQuery(this).addClass('is-invalid');
									jQuery(this).parent().find('.help-block').removeClass('d-none');
									if(first) {
										scrollToTop(jQuery(this).parent().offset().top);
										first = false;
									}
									success = false;
								}
							}
						}
						
					});
				});

				if(success) {

					var formdata = new FormData();
					var data = {};

					jQuery('.ts-forms-formwrapper').each(function (e) {
						var formid = jQuery(this).data('formid');
						if(data[formid] == undefined) {
							data[formid] = {};
							formindex = 0;
						} else {
							formindex = Object.keys(data[formid]).length;
						}
						if(data[formid][formindex] == undefined) {
							data[formid][formindex] = {};
						}
						jQuery(this).find(identifier).each(function (f) {
							var type = getFormType(jQuery(this));
							if(type == "checkbox" || type == "radio") {
								if(jQuery(this).prop('checked')) {
									data[formid][formindex][jQuery(this).data('fieldid')] = jQuery(this).val();
								}
							} else if(type == "file") {
								formdata.append(jQuery(this).attr('id'), jQuery(this).get(0).files[0]);
								data[formid][formindex][jQuery(this).data('fieldid')] = "__FILE__";
							
							} else if(type == "scale" || type == "matrix") {
								if(jQuery(this).prop('checked')) {
									if(data[formid][formindex][jQuery(this).data('fieldid')] == undefined) {
										data[formid][formindex][jQuery(this).data('fieldid')] = {};
									} 
									data[formid][formindex][jQuery(this).data('fieldid')][jQuery(this).attr("data-optionid")] = jQuery(this).val();
								} else {
									if(data[formid][formindex][jQuery(this).data('fieldid')] == undefined) {
										data[formid][formindex][jQuery(this).data('fieldid')] = {};
									} 
									if(data[formid][formindex][jQuery(this).data('fieldid')][jQuery(this).attr("data-optionid")] == undefined) {
										data[formid][formindex][jQuery(this).data('fieldid')][jQuery(this).attr("data-optionid")] = "";
									}
								}
							} else {
								if(jQuery(this).val() != "") {
									data[formid][formindex][jQuery(this).data('fieldid')] = jQuery(this).val();
								}
							}
						});
					});

					formdata.append("action", "forms_save_data");
					formdata.append("datastr", JSON.stringify(data));
					jQuery.ajax({
						type: 'POST',
						url: ajaxurl,
						data: formdata,
						processData: false,
						contentType: false,
						success: function (data) {
							if(data == "success") {
								userFeedback(button, 'success');
								if(link != "" && link != null) {
									if (isLastPage == true && jQuery(".abzeichen-modale").length != 0) {
										checkProgress(button);
									} else {
										setTimeout(() => {
											location.href = link;
										}, 100);
									}
								}
								if(extendedFeedback) {
									jQuery(extendedFeedbackModal+'-icon').html('<span class="userfeedback-success"><i class="fas fa-check-circle align-middle" style="font-size:6rem !important"></i></span>');
									jQuery(extendedFeedbackModal).data('bs.modal')._config.keyboard = true;
									jQuery(extendedFeedbackModal).data('bs.modal')._config.backdrop = true;
									if(button.attr('data-extendedfeedbacktext') != "") {
										jQuery(extendedFeedbackModal).find('.tsformsExtendedFeedbackModalText').html(jQuery(button).attr('data-extendedfeedbacktext'));
									}
									setTimeout(() => {
										jQuery(extendedFeedbackModal).modal('hide');
									}, 5000);
								}
								jQuery('.ts-forms-captcha').val('');
								jQuery('.ts-forms-captcha-reload').click();
								setTimeout(() => {
									userFeedback(button, 'text');
								}, 1500);
							} else if(data == "mailsuccess") {
								jQuery(button).removeAttr('style');
								jQuery(button).html('<span class="userfeedback-success d-flex align-items-center"><i class="fas fa-check-circle"></i>&nbsp;E-Mail versandt</span>');
								
								if(extendedFeedback) {
									jQuery(extendedFeedbackModal+'-icon').html('<span class="userfeedback-success"><i class="fas fa-check-circle align-middle" style="font-size:6rem !important"></i></span>');
									jQuery(extendedFeedbackModal).data('bs.modal')._config.keyboard = true;
									jQuery(extendedFeedbackModal).data('bs.modal')._config.backdrop = true;
									if(button.attr('data-extendedfeedbacktext') != "") {
										jQuery(extendedFeedbackModal).find('.tsformsExtendedFeedbackModalText').html(jQuery(button).attr('data-extendedfeedbacktext'));
									}
									setTimeout(() => {
										jQuery(extendedFeedbackModal).modal('hide');
									}, 5000);
								}

								if(link != "" && link != null) {
									setTimeout(() => {
										location.href = link;
									}, 5000);
								}
							} else if(data == "mailerror") {
								if(extendedFeedback) {
									jQuery(extendedFeedbackModal+'-icon').html('<span class="userfeedback-error"><i class="fas fa-times-circle align-middle" style="font-size:6rem !important"></i></span>');
									jQuery(extendedFeedbackModal).data('bs.modal')._config.keyboard = true;
									jQuery(extendedFeedbackModal).data('bs.modal')._config.backdrop = true;
									if(button.attr('data-extendedfeedbacktextfail') != "") {
										jQuery(extendedFeedbackModal).find('.tsformsExtendedFeedbackModalText').html(jQuery(button).attr('data-extendedfeedbacktextfail'));
									}
									setTimeout(() => {
										jQuery(extendedFeedbackModal).modal('hide');
									}, 5000);
								}

								var oldhtml = jQuery(button).find('.userfeedback-text').html();
								jQuery(button).find('.userfeedback-text').addClass("d-flex align-items-center");
								jQuery(button).find('.userfeedback-text').html('<i class="fas fa-times-circle"></i>&nbsp;E-Mail nicht versandt</span>');
								userFeedback(button, 'error');
								setTimeout(() => {
									jQuery(button).removeAttr('style');
									userFeedback(button, 'text');
									setTimeout(() => {
										jQuery(button).find('.userfeedback-text').html(oldhtml);
										jQuery(button).find('.userfeedback-text').removeClass("d-flex align-items-center");
									}, 5000);
								}, 1500);
							} else if(data == "captchaerror") {

								jQuery('.ts-forms-captcha').addClass('is-invalid');
								jQuery('.ts-forms-captcha').parent().find('.help-block.text-danger').first().removeClass("d-none");
								scrollToTop(jQuery('.ts-forms-captcha').parent().parent().offset().top);

								if(extendedFeedback) {
									jQuery(extendedFeedbackModal+'-icon').html('<span class="userfeedback-error"><i class="fas fa-times-circle align-middle" style="font-size:6rem !important"></i></span>');
									jQuery(extendedFeedbackModal).data('bs.modal')._config.keyboard = true;
									jQuery(extendedFeedbackModal).data('bs.modal')._config.backdrop = true;
									if(button.attr('data-extendedfeedbacktextfail') != "") {
										jQuery(extendedFeedbackModal).find('.tsformsExtendedFeedbackModalText').html(jQuery(button).attr('data-extendedfeedbacktextfail'));
									}
									setTimeout(() => {
										jQuery(extendedFeedbackModal).modal('hide');
									}, 5000);
								}

								var oldhtml = jQuery(button).find('.userfeedback-text').html();
								var oldstyle = jQuery(button).attr("style");
								jQuery(button).removeAttr('style');
								userFeedback(button, 'error');
								jQuery(button).find('.userfeedback-text').html(oldhtml);
								jQuery(button).find('.userfeedback-text').addClass("d-flex align-items-center");
								jQuery(button).find('.userfeedback-text').html('<i class="fas fa-times-circle"></i>&nbsp;Captcha nicht korrekt</span>');
								userFeedback(button, 'text');
								setTimeout(() => {
									jQuery(button).find('.userfeedback-text').html(oldhtml);
									jQuery(button).attr("style", oldstyle);
									jQuery(button).find('.userfeedback-text').removeClass("d-flex align-items-center");
								}, 5000);
							} else {
								if(extendedFeedback) {
									jQuery(extendedFeedbackModal+'-icon').html('<span class="userfeedback-error"><i class="fas fa-times-circle align-middle" style="font-size:6rem !important"></i></span>');
									jQuery(extendedFeedbackModal).data('bs.modal')._config.keyboard = true;
									jQuery(extendedFeedbackModal).data('bs.modal')._config.backdrop = true;
									if(button.attr('data-extendedfeedbacktextfail') != "") {
										jQuery(extendedFeedbackModal).find('.tsformsExtendedFeedbackModalText').html(jQuery(button).attr('data-extendedfeedbacktextfail'));
									}
									setTimeout(() => {
										jQuery(extendedFeedbackModal).modal('hide');
									}, 5000);
								}

								userFeedback(button, 'error');
								setTimeout(() => {
									userFeedback(button, 'text');
								}, 1500);
							}
						},
						error: function (errorThrown) {
							if(extendedFeedback) {
								jQuery(extendedFeedbackModal+'-icon').html('<span class="userfeedback-error"><i class="fas fa-times-circle align-middle" style="font-size:6rem !important"></i></span>');
								jQuery(extendedFeedbackModal).data('bs.modal')._config.keyboard = true;
								jQuery(extendedFeedbackModal).data('bs.modal')._config.backdrop = true;
								if(button.attr('data-extendedfeedbacktextfail') != "") {
									jQuery(extendedFeedbackModal).find('.tsformsExtendedFeedbackModalText').html(jQuery(button).attr('data-extendedfeedbacktextfail'));
								}
								setTimeout(() => {
									jQuery(extendedFeedbackModal).modal('hide');
								}, 5000);
							}

							userFeedback(button, 'error');
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);
						}
					});
				} else {
					if(extendedFeedback) {
						jQuery(extendedFeedbackModal+'-icon').html('<span class="userfeedback-error"><i class="fas fa-times-circle align-middle" style="font-size:6rem !important"></i></span>');
						jQuery(extendedFeedbackModal).data('bs.modal')._config.keyboard = true;
						jQuery(extendedFeedbackModal).data('bs.modal')._config.backdrop = true;
						if(button.attr('data-extendedfeedbacktextfail') != "") {
							jQuery(extendedFeedbackModal).find('.tsformsExtendedFeedbackModalText').html(jQuery(button).attr('data-extendedfeedbacktextfail'));
						}
						setTimeout(() => {
							jQuery(extendedFeedbackModal).modal('hide');
						}, 5000);
					}

					userFeedback(button, 'error');
					setTimeout(() => {
						userFeedback(button, 'text');
					}, 1500);
				}
			} else {
				if(link != "" && link != null) {
					if (isLastPage == true && jQuery(".abzeichen-modale").length != 0) {
						checkProgress(button);
					}
					else  {
						setTimeout(() => {
							location.href = link;
					}, 100);
					}
				}
			}
		}
		badgeContinue = function (link) {	
			setTimeout(() => {
				location.href = link;
			}, 100);	
		}
		function checkProgress(button) {	
			var trainingid = jQuery("#training-not-fin").attr("data-id");	
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					"action": "check_progress",
					"trainingid": trainingid,
				},
				success: function (data) {
					if (data == 1000) {
						jQuery('#training-fin').modal('show');
						userFeedback(button, 'text');
					}else {
						jQuery('#training-not-fin').modal('show');
						userFeedback(button, 'text');
						jQuery('#userProgress').html("Ihr Trainingsfortschritt: " + data/10 + "%");
						jQuery('.img-overlay').css("width", 100-(data/10)+"%");
					}
				}
			}); 
		}
		function scrollToTop(elementTop) {
			var marginTop = 5;
			if(jQuery('#wpadminbar').length != 0) {
				marginTop += jQuery('#wpadminbar').outerHeight();
			}
			jQuery([document.documentElement, document.body]).animate({
				scrollTop: elementTop-marginTop
			}, 1000);
		}

		function getFormType(element) {
			var tagName = jQuery(element).prop("tagName").toLowerCase();
			var inputType = "";
			if(jQuery(element).attr("type") != undefined) {
				inputType = jQuery(element).attr("type").toLowerCase();
			}

			if(tagName == "select" || tagName == "textarea") {
				return tagName;
			}

			if(tagName == "input" && inputType != "") {
				if(inputType == "radio" || inputType == "checkbox" || inputType == "file") {
					if(element.attr("data-scale") != undefined) {
						return "scale";
					} else if(element.attr("data-matrix") != undefined) {
						return "matrix";
					} else {
						return inputType;
					}
				}
				if(inputType == "range" || (inputType == "text" && element.attr("data-slider-id") != undefined)) {
					return "slider";
				}
				return "text";
			} else {
				return undefined;
			}
		}

		jQuery(document).on('click', '.media-grid-download', function (e) {
			var fileid = jQuery(this).attr('data-fileid');
			var button = jQuery(this);

			if(fileid != null && fileid != undefined) {
				userFeedback(button, 'loading');

				var xhr = new XMLHttpRequest();
				xhr.open('POST', ajaxurl, true);
				xhr.responseType = 'arraybuffer';
				xhr.onload = function () {
						$('#trainingsexportloading').modal('hide');
						if (this.status === 200) {
							if(this.response.byteLength > 1) {
								var filename = "";
								var disposition = xhr.getResponseHeader('Content-Disposition');
								if (disposition && disposition.indexOf('attachment') !== -1) {
										var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
										var matches = filenameRegex.exec(disposition);
										if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
								}
								var type = xhr.getResponseHeader('Content-Type');

								var blob = typeof File === 'function'
										? new File([this.response], filename, { type: type })
										: new Blob([this.response], { type: type });
								if (typeof window.navigator.msSaveBlob !== 'undefined') {
										// IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
										window.navigator.msSaveBlob(blob, filename);
								} else {
										var URL = window.URL || window.webkitURL;
										var downloadUrl = URL.createObjectURL(blob);

										if (filename) {
												// use HTML5 a[download] attribute to specify filename
												var a = document.createElement("a");
												// safari doesn't support this yet
												if (typeof a.download === 'undefined') {
														window.location = downloadUrl;
												} else {
														a.href = downloadUrl;
														a.download = filename;
														document.body.appendChild(a);
														a.click();
														userFeedback(button, 'success');
														setTimeout(() => {
															userFeedback(button, 'text');
														}, 1500);
												}
										} else {
												window.location = downloadUrl;
												userFeedback(button, 'success');
												setTimeout(() => {
													userFeedback(button, 'text');
												}, 1500);
										}

										setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
								}
							} else {
								userFeedback(button, 'error');
								setTimeout(() => {
									userFeedback(button, 'text');
								}, 1500);
							}
						} else {
							userFeedback(button, 'error');
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);
						}
				};
				xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
				xhr.send("action=media_grid_download_file&fileid="+fileid);
			} else {
				userFeedback(button, 'error');
				setTimeout(() => {
					userFeedback(button, 'text');
				}, 1500);
			}
		})

		jQuery(document).on('click', '.media-grid-button', function (e) {
			var fileid = jQuery(this).attr('data-fileid');
			var modalid = jQuery(this).attr('data-modalid');
			var defaultimage = jQuery(this).attr('data-defaultimage');
			var title = jQuery(this).attr('data-title');
			var desc = jQuery(this).attr('data-desc');
			var timestamp = jQuery(this).attr('data-timestampstring');

			var button = jQuery(this);

			if(fileid != null && fileid != undefined && modalid != null && modalid != undefined && defaultimage != null && defaultimage != undefined) {

				userFeedback(button, 'loading');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "media_grid_details",
						"fileid": fileid
					},
					success: function (data) {
						if(data != "" && data != "0") {
							userFeedback(button, 'success');
							jQuery('#' + modalid + ' .modal-body').html(data);

							if(title != "") {
								jQuery('#' + modalid + '-label').html(title);
							} else {
								jQuery('#' + modalid + '-label').html("Details zur Datei");
							}
							if(desc != "") {
								jQuery('#' + modalid + ' .modal-body').find('.media-grid-desc').html(desc);
							} else {
								jQuery('#' + modalid + ' .modal-body').find('.media-grid-desc').remove();
							}
							
							jQuery('#' + modalid + ' .modal-body').find('.media-grid-timestamp').html(timestamp);
							jQuery('#' + modalid + ' .modal-body').find('.media-grid-defaultimage').attr("src", defaultimage);
							
							jQuery('#' + modalid + ' .modal-footer .media-grid-download').attr('data-fileid', fileid);
							jQuery('#' + modalid).modal('show');
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);
						} else {
							userFeedback(button, 'error');
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback(button, 'error');
						setTimeout(() => {
							userFeedback(button, 'text');
						}, 1500);
					}
				});

			} else {
				userFeedback(button, 'error');
				setTimeout(() => {
					userFeedback(button, 'text');
				}, 1500);
			}
		})

		jQuery(document).on('click', '.media-grid-delete', function (e) {
			var dataid = jQuery(this).attr('data-dataid');
			var fileid = jQuery(this).attr('data-fileid');

			var button = jQuery(this);

			if(dataid != null && dataid != undefined && dataid != "" && fileid != null && fileid != undefined && fileid != "") {
				
				userFeedback(button, 'loading');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "media_grid_delete",
						"dataid": dataid,
						"fileid": fileid
					},
					success: function (data) {
						if(data == "1") {
							userFeedback(button, 'success');
							
							setTimeout(() => {
								location.reload();
							}, 500);
						} else {
							userFeedback(button, 'error');
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback(button, 'error');
						setTimeout(() => {
							userFeedback(button, 'text');
						}, 1500);
					}
				});

			} else {
				userFeedback(button, 'error');
				setTimeout(() => {
					userFeedback(button, 'loading');
				}, 1500);
			}
		});

		jQuery('.media-grid-modal').on('hidden.bs.modal', function(e) {
			jQuery(this).find('.modal-title').first().html('');
			jQuery(this).find('.modal-body').first().html('');
			jQuery(this).find('.modal-footer .media-grid-download').first().attr('data-fileid', '');
		})

		jQuery(document).on('click', '.media-grid-open-modal', function (e) {
			jQuery(this).parent().find('.card-footer .media-grid-button').first().click();
		})

		jQuery(document).on('click', '.media-grid-project-delete', function (e) {
			var projectname = jQuery(this).attr('data-projectname');
			var uploadfieldid = jQuery(this).attr('data-uploadfieldid');
			var projectfieldid = jQuery(this).attr('data-projectfieldid');
			var modalid = jQuery(this).attr('data-modalid');

			jQuery('#'+modalid).find('.media-grid-delete-confirm').attr('data-projectname', projectname);
			jQuery('#'+modalid).find('.media-grid-delete-confirm').attr('data-uploadfieldid', uploadfieldid);
			jQuery('#'+modalid).find('.media-grid-delete-confirm').attr('data-projectfieldid', projectfieldid);

			jQuery('#'+modalid).modal('show');
		});

		jQuery('.media-grid-delete-modal').on('hidden.bs.modal', function (e) {
			jQuery('.media-grid-delete-modal').find('.media-grid-delete-confirm').attr('data-projectname', '');
			jQuery('.media-grid-delete-modal').find('.media-grid-delete-confirm').attr('data-uploadfieldid', '');
			jQuery('.media-grid-delete-modal').find('.media-grid-delete-confirm').attr('data-projectfieldid', '');
		});

		jQuery(document).on('click', '.media-grid-delete-confirm', function (e) {
			var projectname = jQuery(this).attr('data-projectname');
			var uploadfieldid = jQuery(this).attr('data-uploadfieldid');
			var projectfieldid = jQuery(this).attr('data-projectfieldid');
			var button = jQuery(this);

			if(projectname != null && projectname != undefined && 
				uploadfieldid != null && uploadfieldid != "" && uploadfieldid != undefined && 
				projectfieldid != null && projectfieldid != undefined) {
				
				userFeedback(button, 'loading');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "media_grid_delete_project",
						"projectname": projectname,
						"uploadfieldid": uploadfieldid,
						"projectfieldid": projectfieldid
					},
					success: function (data) {
						if(data == "1") {
							userFeedback(button, 'success');
							
							setTimeout(() => {
								location.reload();
							}, 500);
						} else {
							userFeedback(button, 'error');
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback(button, 'error');
						setTimeout(() => {
							userFeedback(button, 'text');
						}, 1500);
					}
				});

			} else {
				userFeedback(button, 'error');
				setTimeout(() => {
					userFeedback(button, 'text');
				}, 1500);
			}
		})

		jQuery(document).on("change", ".visualInput-stars input", function(){
			jQuery(this).parent().parent().find("input").each(function(e){
				jQuery(this).parent().find("label .fa-star").removeClass("visualInput-stars-selected");
			});

			jQuery(this).parent().parent().find("input").each(function(e){
				
				jQuery(this).parent().find("label .fa-star").addClass("visualInput-stars-selected");

				if(jQuery(this).is(":checked")){
					return false;
				}
			});
		});

		jQuery(document).on("mouseenter", "div.form-group:has(div.visualInput-stars)", function(){

			jQuery(document).on("mouseenter", ".visualInput-stars label .fa-star", function(){
				var thisStar = jQuery(this).parent().parent().find("input").prop("id");
				jQuery(this).parent().parent().parent().find("input").each(function(e){
					jQuery(this).parent().find("label .fa-star").removeClass("visualInput-stars-selected");
				});
	
				jQuery(this).parent().parent().parent().find("input").each(function(e){
	
					jQuery(this).parent().find("label .fa-star").addClass("visualInput-stars-selected");

					if(jQuery(this).prop("id") == thisStar){
						return false;
					}
				});
			});
		});

		jQuery(document).on("mouseleave", "div.form-group:has(div.visualInput-stars)", function(){

				var thisStar = jQuery(this).find("input:checked").prop("id");
				jQuery(this).find("input").each(function(e){
					jQuery(this).parent().find("label .fa-star").removeClass("visualInput-stars-selected");
				});

				if(jQuery(this).find("input:checked").length >= 1){
					jQuery(this).find("input").each(function(e){
						
						jQuery(this).parent().find("label .fa-star").addClass("visualInput-stars-selected");

						if(jQuery(this).prop("id") == thisStar){
							return false;
						}
					});
				}
		});
		
		// Backend Formulare bearbeiten
		function copyTextToClipboard(text) {
			var $temp = jQuery("<input>");
			jQuery("body").append($temp);
			$temp.val(text).select();
			document.execCommand("copy");
			$temp.remove();
		}
	
		jQuery(document).on("click", ".copy-shortcode-button", function() {
			var button = jQuery(this);
			copyTextToClipboard(button.parent().find('code').first().html());
			userFeedback(button, 'success');
			setTimeout(() => {
					userFeedback(button, 'text');
			}, 1000);
		});

		jQuery(document).on('click', '#formular-is-multiform-checkbox', function (e) {
			if(jQuery('#formular-is-multiform-checkbox').is(':checked')) {
				jQuery('#formular-multiform-orientation-above').removeAttr('disabled');
				jQuery('#formular-multiform-orientation-next').removeAttr('disabled');
				jQuery('#formular-multiform-ordering').removeAttr('disabled');
			} else {
				jQuery('#formular-multiform-orientation-above').prop('disabled', true);
				jQuery('#formular-multiform-orientation-next').prop('disabled', true);
				jQuery('#formular-multiform-ordering').prop('disabled', true);
			}
		});

		jQuery(document).on('click', '#formular-submitmanually', function (e) {
			if(jQuery('#formular-submitmanually').is(':checked')) {
				jQuery('#formular-submitmanuallylink').removeAttr('disabled');
			} else {
				jQuery('#formular-submitmanuallylink').prop('disabled', true);
			}
		});

		jQuery(document).on('click', '#formular-sendbymail', function (e) {
			if(jQuery('#formular-sendbymail').is(':checked')) {
				jQuery('#formular-sendtomail').removeAttr('disabled');
			} else {
				jQuery('#formular-sendtomail').prop('disabled', true);
			}
		});

		jQuery(document).on('click', '#formular-send_receipt', function (e) {
			if(jQuery('#formular-send_receipt').is(':checked')) {
				jQuery('#formular-send_receipt_subject').removeAttr('disabled');
				jQuery('#formular-send_receipt_text').removeAttr('disabled');
			} else {
				jQuery('#formular-send_receipt_subject').prop('disabled', true);
				jQuery('#formular-send_receipt_text').prop('disabled', true);
			}
		});

		jQuery(document).on('keypress', '#formular-additional-classes', function(e) {
			if(e.which == "13") {
				sendFormOptions();
				e.preventDefault();
			}
		});

		jQuery(document).on('keypress', '#formular-sendtomail', function(e) {
			if(e.which == "13") {
				sendFormOptions();
				e.preventDefault();
			}
		});

		jQuery(document).on('click', '#backend-form-options-save-button', function(e) {
			e.preventDefault();
			sendFormOptions();
		});

		function sendFormOptions() {
			userFeedback('backend-form-options-save-button', 'loading');
			jQuery('#formular-sendtomail').removeClass('is-invalid');
			jQuery('#formular-sendtomail-invalidfeedback').addClass('d-none');

			var formid = jQuery('#formular-options-id').val();
			var multiform = jQuery('#formular-is-multiform-checkbox').prop('checked');
			var multiform_ordering = jQuery('#formular-multiform-ordering').prop('checked');
			var classes = jQuery('#formular-additional-classes').val();
			var multiformorientation = "";
			if(jQuery('#formular-multiform-orientation-above').is(':checked')) {
				multiformorientation = "above";
			} else if(jQuery('#formular-multiform-orientation-next').is(':checked')) {
				multiformorientation = "next";
			}
			var submitmanually = jQuery('#formular-submitmanually').is(':checked');
			var submitmanuallylink = jQuery('#formular-submitmanuallylink').val();
			var sendbymail = jQuery('#formular-sendbymail').is(':checked');
			var sendtomail = jQuery('#formular-sendtomail').val();
			var sendreceipt = jQuery('#formular-send_receipt').is(':checked');
			var sendreceipt_subject = jQuery('#formular-send_receipt_subject').val();
			var sendreceipt_text = jQuery('#formular-send_receipt_text').val();
			var normaldata = jQuery('#formular-normaldata').is(':checked');
			var keepdata = jQuery('#formular-keepdata').is(':checked');
			var saveonce = jQuery('#formular-saveonce').is(':checked');
			var nosave = jQuery('#formular-nosave').is(':checked');

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					"action": "backend_forms_save_options",
					"formid": formid,
					"multiform": multiform,
					"multiformordering": multiform_ordering,
					"multiformorientation": multiformorientation,
					"classes": classes,
					"submitmanually": submitmanually,
					"submitmanuallylink": submitmanuallylink,
					"sendbymail": sendbymail,
					"sendtomail": sendtomail,
					"sendreceipt": sendreceipt,
					"sendreceipt_subject": sendreceipt_subject,
					"sendreceipt_text": sendreceipt_text,
					"normaldata": normaldata,
					"keep_data": keepdata,
					"saveonce": saveonce,
					"nosave": nosave
				},
				success: function (data) {
					if(data == "success") {
						userFeedback('backend-form-options-save-button', 'success');
						setTimeout(() => {
							userFeedback('backend-form-options-save-button', 'text');
						}, 1500);
					} else if(data == "invalidmail") {
						userFeedback('backend-form-options-save-button', 'error');
						jQuery('#formular-sendtomail').addClass('is-invalid');
						jQuery('#formular-sendtomail-invalidfeedback').removeClass('d-none');
						setTimeout(() => {
							userFeedback('backend-form-options-save-button', 'text');
						}, 1500);
					} else {
						userFeedback('backend-form-options-save-button', 'error');
						setTimeout(() => {
							userFeedback('backend-form-options-save-button', 'text');
						}, 1500);
					}
				},
				error: function (errorThrown) {
					userFeedback('backend-form-options-save-button', 'error');
					setTimeout(() => {
						userFeedback('backend-form-options-save-button', 'text');
					}, 1500);
				}
			});
		};

		jQuery(document).on('click', '#backendDeleteFormData', function(e) {
			
			jQuery('#backend-form-delete-data-formid').val(jQuery(this).attr('data-formid'));
			jQuery('#backend-form-delete-data-fieldid').val('all');

			jQuery('#deleteDataModal .modal-body .formdelete').removeClass('d-none');

			jQuery('#deleteDataModal').modal('show')
		});

		jQuery(document).on('click', '.backendDeleteFormFieldData', function(e) {
			
			jQuery('#backend-form-delete-data-formid').val(jQuery(this).attr('data-formid'));
			jQuery('#backend-form-delete-data-fieldid').val(jQuery(this).attr('data-fieldid'));

			jQuery('#deleteDataModal .modal-body .fielddelete').removeClass('d-none');

			jQuery('#deleteDataModal').modal('show')
		});

		jQuery(document).on('hidden.bs.modal', '#deleteDataModal', function (e) {
			jQuery('#backend-form-delete-data-formid').val('');
			jQuery('#backend-form-delete-data-fieldid').val('');

			jQuery('#deleteDataModal .modal-body .formdelete').addClass('d-none');
			jQuery('#deleteDataModal .modal-body .fielddelete').addClass('d-none');
		});

		jQuery(document).on('click', '#deleteDataModalConfirm', function (e) {
			var formid = jQuery('#backend-form-delete-data-formid').val();
			var fieldid = jQuery('#backend-form-delete-data-fieldid').val();

			if(formid != undefined && fieldid != undefined) {

				userFeedback('deleteDataModalConfirm', 'loading');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "backend_forms_delete_data",
						"formid": formid,
						"fieldid": fieldid,
					},
					success: function (data) {
						if(data == "1") {
							userFeedback('deleteDataModalConfirm', 'success');
							loadFormFields();
							setTimeout(() => {
								userFeedback('deleteDataModalConfirm', 'text');
								jQuery('#deleteDataModal').modal('hide')
							}, 1500);
						} else {
							userFeedback('deleteDataModalConfirm', 'error');
							setTimeout(() => {
								userFeedback('deleteDataModalConfirm', 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback('deleteDataModalConfirm', 'error');
						setTimeout(() => {
							userFeedback('deleteDataModalConfirm', 'text');
						}, 1500);
					}
				});

			} else {
				userFeedback('deleteDataModalConfirm', 'error');
				setTimeout(() => {
					userFeedback('deleteDataModalConfirm', 'text');
				}, 1500);
			}
		});

		jQuery(document).on('click', '#backend-forms-pages-button', function() {
			userFeedback('backend-forms-pages-button', 'loading');

			jQuery('#backend-forms-pages-list').html('');
			var formid = jQuery('#backend-forms-pages-formid').val();

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					"action": "backend_forms_get_pages",
					"formid": formid
				},
				success: function (data) {
					userFeedback('backend-forms-pages-button', 'success');
					jQuery('#backend-forms-pages-list').html(data);
					setTimeout(() => {
						userFeedback('backend-forms-pages-button', 'text');
					}, 1500);
				},
				error: function (errorThrown) {
					userFeedback('backend-forms-pages-button', 'error');
					setTimeout(() => {
						userFeedback('backend-forms-pages-button', 'text');
					}, 1500);
				}
			});
		});

		jQuery(document).on('click', '#assembleModalButton', function (e) {
			if(jQuery('#assembleFormModalWrapper').html().trim() == "") {
				userFeedback('assembleModalButton', 'loading');
				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "backend_forms_assemble_modal"
					},
					success: function (data) {
						userFeedback('assembleModalButton', 'success');
						// Clear Formfield Edit Div because the same IDs are used there
						jQuery('#editFormFieldModalContainer').html('');
						jQuery('#assembleFormModalWrapper').html(data);
						jQuery('#assembleFormModal').modal('show');
						setTimeout(() => {
							userFeedback('assembleModalButton', 'text');
						}, 1500);
					},
					error: function (errorThrown) {
						userFeedback('assembleModalButton', 'error');
						setTimeout(() => {
							userFeedback('assembleModalButton', 'text');
						}, 1500);
					}
				});
			} else {
				jQuery('#assembleFormModal').modal('show');
			}
		})
		
		jQuery(document).on('change', '#textfeldType', function () {
			if(jQuery('#textfeldType').val() == "number") {
				jQuery('#textfeld-number-options').collapse('show');
			} else {
				jQuery('#textfeld-number-options').collapse('hide');
				jQuery('#textfeld-number-options').find('input').val('');
			}
		});

		jQuery(document).on('focusout', '.select-option-text', function() {
			var input = jQuery(this).val();
			var textField = jQuery(this).parent().parent().find('.select-option-value').first();
			// Replace target input only if target is not empty. Uncomment if replace should only take place if target empty
			//if(textField.val() == "") {
				textField.val(input);
			//}
		});

		jQuery(document).on('click', '#selectAddOptionButton', function() {
			var table = jQuery('#selectOptionTable tbody');
			var rowid = 0;
			table.find('tr').each(function(e) {
				if(jQuery(this).data('rowid') > rowid) {
					rowid = jQuery(this).data('rowid');
				}
			});
			rowid++;

			table.append('<tr data-rowid="'+rowid+'"><td class="align-middle text-center"><button type="button" class="btn btn-danger btn-sm selectOptionRemove" title="Option löschen"><i class="fas fa-minus-square"></i></button></td>' +
							'<td class="align-middle"><input type="text" class="form-control select-option-text newFormFieldInput" placeholder="Text" id="selectOption-text-'+rowid+'"></td>' +
							'<td class="align-middle"><input type="text" class="form-control select-option-value newFormFieldInput" placeholder="Value" id="selectOption-value-'+rowid+'"></td>' + 
							'<td class="align-middle text-nowrap"><div class="form-check"><input type="radio" class="form-check-input position-relative m-0 mr-2 newFormFieldInput" name="selectOption-selected" id="selectOption-selected-'+rowid+'"><label class="form-check-label m-0" for="selectOption-selected-'+rowid+'">standardmäßig anzeigen</label></div></td></tr>');
							
			if(table.find('tr').length > 1) {
				jQuery('.selectOptionRemove').prop('disabled', false);
			}
		});

		jQuery(document).on('click', '.selectOptionRemove', function() {
			if(jQuery('#selectOptionTable tbody').find('tr').length > 1) {
				var deleteSelected = false;
				if(jQuery(this).parent().parent().find('input[type=radio]:checked').length == 1) {
					deleteSelected = true;
				}
				jQuery(this).parent().parent().remove();

				if(deleteSelected) {
					jQuery('#selectOptionTable tbody').find('tr').first().find('input[type=radio]').first().prop('checked', true);
				}

				if(jQuery('#selectOptionTable tbody').find('tr').length == 1) {
					jQuery('#selectOptionTable tbody').find('tr').first().find('button').first().prop('disabled', true);
				}
			}

		});

		jQuery(document).on('focusout', '.radiobutton-text', function() {
			var input = jQuery(this).val();
			var textField = jQuery(this).parent().parent().find('.radiobutton-value').first();
			// Replace target input no mather if target is empty or not. Uncomment if replace should only take place if target is empty
			//if(textField.val() == "") {
				textField.val(input);
			//}
		});

		jQuery(document).on('click', '#radioButtonProButton', function() {
			if(jQuery('#radioButtonsTable').hasClass('proUser')) {
				jQuery('#radioButtonsTable').removeClass('proUser');
				jQuery('#radioButtonsSelectOne').addClass('d-none');
				jQuery(this).html('<i class="fas fa-plus-square"></i>');
			} else {
				jQuery('#radioButtonsTable').addClass('proUser');
				jQuery('#radioButtonsSelectOne').removeClass('d-none');
				jQuery(this).html('<i class="fas fa-minus-square"></i>');
			}
		});

		jQuery(document).on('click', '#radioButtonAddButton', function() {
			var table = jQuery('#radioButtonsTable tbody');
			var rowid = 0;
			table.find('tr').each(function(e) {
				if(jQuery(this).data('rowid') > rowid) {
					rowid = jQuery(this).data('rowid');
				}
			});
			rowid++;

			var newRow = '<tr data-rowid="'+rowid+'"><td class="align-middle text-center"><button type="button" class="btn btn-sm btn-danger radioButtonRemove" title="Radio-Button löschen" disabled><i class="fas fa-minus-square"></i></button></td>' +
							'<td class="align-middle"><input type="text" class="form-control radiobutton-text newFormFieldInput" placeholder="Text" id="radioButton-text-'+rowid+'"></td>' +
							'<td class="align-middle"><input type="text" class="form-control radiobutton-value newFormFieldInput" placeholder="Value" id="radioButton-value-'+rowid+'"></td>' +
							'<td class="align-middle text-center"><input type="checkbox" class="form-check-input position-relative m-0 radiobutton-disabled newFormFieldInput" id="radioButton-disabled-'+rowid+'" style="margin-top: 0 !important;"></td>' +
							'<td class="align-middle text-center"><input type="radio" class="form-check-input position-relative m-0 radiobutton-selected newFormFieldInput" id="radioButton-selected-'+rowid+'" name="newRadiobuttonSelected"';
							if(!jQuery('#radioNoneSelected').prop('checked')) {
								newRow += ' disabled ';
							}
							newRow += 'style="margin-top: 0 !important;"></td>' +
							'<td class="align-middle"><input type="text" class="form-control radiobutton-css newFormFieldInput" placeholder="CSS-Klassen" id="radioButton-css-'+rowid+'"></td></tr>';
			table.append(newRow);

			if(table.find('tr').length > 1) {
				jQuery('.radioButtonRemove').prop('disabled', false);
			}
		});

		jQuery(document).on('change', '#radioNoneSelected', function() {
			if(jQuery(this).prop('checked')) {
				jQuery('.radiobutton-selected').prop('checked', false);
				jQuery('.radiobutton-selected').prop('disabled', false);
			} else {
				jQuery('.radiobutton-selected').prop('checked', false);
				jQuery('.radiobutton-selected').prop('disabled', true);
			}
		});

		jQuery(document).on('click', '.radioButtonRemove', function(e) {
			if(jQuery('#radioButtonsTable tbody').find('tr').length > 1) {
				jQuery(this).parent().parent().remove();

				if(jQuery('#radioButtonsTable tbody').find('tr').length == 1) {
					jQuery('#radioButtonsTable tbody').find('tr').first().find('button').first().prop('disabled', true);
				}
			}
		});

		jQuery(document).on('focusout', '#checkboxLabel', function() {
			var input = jQuery(this).val();
			var textField = jQuery('#checkboxValue').first();
			// Replace target input only if target is not empty. Uncomment if replace should only take place if target empty
			//if(textField.val() == "") {
				textField.val(input);
			//}
		});

		jQuery(document).on('change', '#scaleSelectcount', function (e) {
			var val = jQuery('#scaleSelectcount').val();
			if(val < 2 || val > 99) {
				jQuery('#scaleSelectcount').addClass('is-invalid');
				jQuery('#scaleSelectcount').parent().find('.help-text').removeClass('d-none');
			} else {
				jQuery('#scaleSelectcount').removeClass('is-invalid');
				jQuery('#scaleSelectcount').parent().find('.help-text').addClass('d-none');
			}
		})

		jQuery(document).on('click', '#scaleAddOptionButton', function() {
			var table = jQuery('#scaleOptionTable tbody');
			var rowid = 0;
			table.find('tr').each(function(e) {
				if(jQuery(this).data('rowid') > rowid) {
					rowid = jQuery(this).data('rowid');
				}
			});
			rowid++;

			table.append('<tr data-rowid="' + rowid + '">' + 
							'<td class="align-middle text-center"><button type="button" class="btn btn-danger btn-sm scaleOptionRemove" title="Option löschen" disabled><i class="fas fa-minus-square"></i></button></td>' +
							'<td class="align-middle"><input type="text" class="form-control scale-option-text newFormFieldInput" placeholder="Unterüberschrift" id="scaleOption-subheadline-' + rowid + '"></td>' + 
							'<td class="align-middle"><input type="text" class="form-control scale-option-text newFormFieldInput" placeholder="Label links" id="scaleOption-labelLeft-' + rowid + '"></td>' + 
							'<td class="align-middle text-center"><i class="fas fa-ellipsis-h h1 m-0"></i></td>' + 
							'<td class="align-middle"><input type="text" class="form-control scale-option-value newFormFieldInput" placeholder="Label rechts" id="scaleOption-labelRight-' + rowid + '"></td>' +
						'</tr>');

			if(table.find('tr').length > 1) {
				jQuery('.scaleOptionRemove').prop('disabled', false);
			}
		});

		jQuery(document).on('click', '.scaleOptionRemove', function() {
			if(jQuery('#scaleOptionTable tbody').find('tr').length > 1) {
				
				jQuery(this).parent().parent().remove();

				if(jQuery('#scaleOptionTable tbody').find('tr').length == 1) {
					jQuery('#scaleOptionTable tbody').find('tr').first().find('button').first().prop('disabled', true);
				}
			}

		});

		jQuery(document).on('change', '#scaleVisualInput', function(e){
			var visualInputOption = jQuery(this).find('option:selected').val();
			if(visualInputOption == "stars" || visualInputOption == "smileys"){
				jQuery(this).parent().parent().find('#scalenumberLabels').prop("checked", false);
				jQuery(this).parent().parent().find('#scalenumberLabels').prop("disabled", true);
			}
			else if(visualInputOption == "numberboxes"){
				jQuery(this).parent().parent().find('#scalenumberLabels').prop("checked", true);
				jQuery(this).parent().parent().find('#scalenumberLabels').prop("disabled", true);
			}
			else{
				jQuery(this).parent().parent().find('#scalenumberLabels').prop("disabled", false);
			}
		});

		jQuery(document).on('change', '#matrixSelectcount', function (e) {
			jQuery(this).parent().find('.help-text').addClass("d-none");

			var table = jQuery('#matrixHeadingsTable tbody');

			var curr = table.find('tr').length;
			var updated = jQuery(this).val();

			if(updated != curr && updated >= 2 && updated <= 11) {
				if(updated < curr) {
					while(jQuery('#matrixHeadingsTable tbody tr').length > updated) {
						jQuery('#matrixHeadingsTable tbody tr').last().remove();
					}
				} else if(updated > curr) {
					while(jQuery('#matrixHeadingsTable tbody tr').length < updated) {
						var rowid = 0;
						table.find('tr').each(function(e) {
							if(jQuery(this).data('rowid') > rowid) {
								rowid = jQuery(this).data('rowid');
							}
						});
						rowid++;

						table.append('<tr data-rowid="'+rowid+'">' +
										'<td class="align-middle text-center">' + (parseInt(rowid)+1) + '</td>' +
										'<td class="align-middle"><input type="text" class="form-control matrix-heading-text newFormFieldInput" data-arrayName="headings" placeholder="Text" id="matrixOption-text-'+rowid+'"></td>' + 
										'<td class="align-middle"><input type="text" class="form-control matrix-heading-value newFormFieldInput" data-arrayName="headings" placeholder="Value" id="matrixOption-value-'+rowid+'" value="' + (parseInt(rowid)+1) + '"></td>' + 
									'</tr>');
					}
				}
			} else {
				if(updated < 2) {
					jQuery(this).val(2);
				} else if(updated > 11 ) {
					jQuery(this).val(11);
				}

				jQuery(this).parent().find('.help-text').removeClass("d-none");
				setTimeout(() => {
					jQuery(this).parent().find('.help-text').addClass("d-none");
				}, 1500);
			}
		});

		jQuery(document).on('click', '#matrixAddQuestionButton', function() {
			var table = jQuery('#matrixQuestionTable tbody');
			var rowid = 0;
			table.find('tr').each(function(e) {
				if(jQuery(this).data('rowid') > rowid) {
					rowid = jQuery(this).data('rowid');
				}
			});
			rowid++;

			table.append('<tr data-rowid="'+ rowid +'">' +
							'<td class="align-middle text-center"><button type="button" class="btn btn-danger btn-sm matrixQuestionRemove" title="Frage löschen"><i class="fas fa-minus-square"></i></button></td>' + 
							'<td class="align-middle"><input type="text" class="form-control matrix-option-question newFormFieldInput" data-arrayName="questions" placeholder="Frage" id="matrixOption-question-' + rowid + '"></td>' + 
							'<td class="align-middle"><input type="text" class="form-control matrix-option-meta newFormFieldInput" data-arrayName="questions" placeholder="Meta" id="matrixOption-meta-' + rowid + '"></td>' + 
							'<td class="align-middle text-center"><i class="fas fa-ellipsis-h h1 m-0"></i></td>' + 
						'</tr>');

			if(table.find('tr').length > 1) {
				jQuery('.matrixQuestionRemove').prop('disabled', false);
			}
		});

		jQuery(document).on('click', '.matrixQuestionRemove', function() {
			if(jQuery('#matrixQuestionTable tbody').find('tr').length > 1) {
				
				jQuery(this).parent().parent().remove();

				if(jQuery('#matrixQuestionTable tbody').find('tr').length == 1) {
					jQuery('#matrixQuestionTable tbody').find('tr').first().find('button').first().prop('disabled', true);
				}
			}

		});
		
		jQuery(document).on('click', '#matrixHeadingsPositionRotate', function (e) {
			if(jQuery('#matrixHeadingsPositionRotate').is(':checked') || jQuery('#matrixHeadingsPositionRotateOnMobile').is(':checked')) {
				jQuery('#matrixHeadingsRotateHeightArea').removeClass('d-none');
			} else {
				jQuery('#matrixHeadingsRotateHeightArea').addClass('d-none');
			}
		})

		jQuery(document).on('click', '#matrixHeadingsPositionRotateOnMobile', function (e) {
			if(jQuery('#matrixHeadingsPositionRotateOnMobile').is(':checked') || jQuery('#matrixHeadingsPositionRotate').is(':checked')) {
				jQuery('#matrixHeadingsRotateHeightArea').removeClass('d-none');
			} else {
				jQuery('#matrixHeadingsRotateHeightArea').addClass('d-none');
			}
		})

		jQuery(document).on('keydown', '.newFormFieldInput', function(e) {
			if(e.which == 13) {
				newFormField();
				e.preventDefault();
				return false;
			}
		});

		jQuery(document).on('click', '#newFormFieldSubmit', function() {
			newFormField();
		});

		function newFormField() {
			var selectedTab = jQuery('#nav-tabContent div.tab-pane.active').first();
			var type = selectedTab.find('.newFormFieldType').first().val();
			var formId = jQuery('#createNewFieldFormId').val();
			
			userFeedback('newFormFieldSubmit', 'loading');
			jQuery('#newFormfieldErrorText').addClass('d-none');

			if(type != undefined && type.trim() != "" && formId != undefined && formId.trim() != "") {

				var data = {};

				if(type != "select" && type != "radio" && type != "scale" && type != "matrix") {
					selectedTab.find(':input:not([type=hidden]):not([data-toggle=collapse]):not(.btn.btn-danger):not(#radioButtonProButton):not(#selectAddOptionButton):not(#radioButtonAddButton):not(#radioNoneSelected):not(#scaleAddOptionButton):not(#matrixAddQuestionButton)').each(function (e) {
						if(jQuery(this).prop('tagName').toLowerCase() != "select" && jQuery(this).prop('tagName').toLowerCase() != "textarea" && (jQuery(this).attr('type').toLowerCase() == "radio" || jQuery(this).attr('type').toLowerCase() == "checkbox")) {
							data[jQuery(this).attr('id')] = jQuery(this).is(':checked');
						} else {
							data[jQuery(this).attr('id')] = jQuery(this).val();
						}
					});
				} else {
					selectedTab.find(':input:not([type=hidden]):not([data-toggle=collapse]):not(.btn.btn-danger):not(#radioButtonProButton):not(#selectAddOptionButton):not(#radioButtonAddButton):not(#radioNoneSelected):not(#scaleAddOptionButton):not(#matrixAddQuestionButton)').each(function (e) {
						var elementId = jQuery(this).attr('id');
						var hasNumber = /\d/;
						if(hasNumber.test(elementId)) {
							var splitId = elementId.split('-');
							var arrayName = 'elements';
							
							if(jQuery(this).attr('data-arrayName') !== undefined && jQuery(this).attr('data-arrayName') !== false) {
								arrayName = jQuery(this).attr('data-arrayName');
							}
							if(data[arrayName] === undefined) {
								data[arrayName] = {};
							}
							
							var optionName = splitId[splitId.length-2];
							var optionId = splitId[splitId.length-1];

							if(data[arrayName][optionId] == undefined) {
								data[arrayName][optionId] = {};
							}
							
							if(jQuery(this).prop('tagName').toLowerCase() != "select" && jQuery(this).prop('tagName').toLowerCase() != "textarea" && (jQuery(this).attr('type').toLowerCase() == "radio" || jQuery(this).attr('type').toLowerCase() == "checkbox")) {
								data[arrayName][optionId][optionName] = jQuery(this).is(':checked');
							} else {
								data[arrayName][optionId][optionName] = jQuery(this).val();
							}
						} else {
							if(jQuery(this).prop('tagName').toLowerCase() != "select" && jQuery(this).prop('tagName').toLowerCase() != "textarea" && (jQuery(this).attr('type').toLowerCase() == "radio" || jQuery(this).attr('type').toLowerCase() == "checkbox")) {
								data[jQuery(this).attr('id')] = jQuery(this).is(':checked');
							} else {
								data[jQuery(this).attr('id')] = jQuery(this).val();
							}
						}
					});
				}

				var jsondata = JSON.stringify(data);
				
				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "backend_forms_new_field",
						"formid": formId,
						"type": type,
						"data": jsondata
					},
					success: function (data) {
						if(data == "success") {
							userFeedback('newFormFieldSubmit', 'success');
							jQuery('#assembleFormModal').modal('hide');
							jQuery('#assembleFormModal').on('hidden.bs.modal', function (e) {
								jQuery('#assembleFormModal').remove();
							});
							
							loadFormFields();

							setTimeout(() => {
								userFeedback('newFormFieldSubmit', 'text');
							}, 1500);
						} else {
							if(data == "invaliddata") {
								jQuery('#newFormfieldErrorText').removeClass('d-none');
							}
							userFeedback('newFormFieldSubmit', 'error');
							setTimeout(() => {
								userFeedback('newFormFieldSubmit', 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback('newFormFieldSubmit', 'error');
						setTimeout(() => {
							userFeedback('newFormFieldSubmit', 'text');
						}, 1500);
					}
				});
				
			} else {
				userFeedback('newFormFieldSubmit', 'error');
				setTimeout(() => {
					userFeedback('newFormFieldSubmit', 'text');
				}, 1500);
			}
		}

		function loadFormFields() {
			var formId = jQuery('#createNewFieldFormId').val();

			if(formId != undefined && formId.trim() != "") {

				jQuery('#form-fields-area').html('');
				jQuery('#formFieldsLoading').removeClass('d-none');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "backend_forms_get_fields",
						"formid": formId
					},
					success: function (data) {
						if(data.trim() != "") {
							jQuery('#formFieldsLoading').addClass('d-none');
							jQuery('#form-fields-area').html(data);
						}
					},
					error: function (errorThrown) {
						
					}
				});
			}
		}

		jQuery('#sortable-form-fields').sortable({
			stop: function(event, ui) { 
				var sortstring = "";

				jQuery('#sortable-form-fields').find('li:not(.list-group-item)').each(function (e) {
					sortstring += jQuery(this).data('fieldid') + '-';
				});
				
				if(jQuery('#sortable-form-fields').data('order') == sortstring) {
					jQuery('#formSaveSortOrderButton').addClass('d-none');
				} else {
					jQuery('#formSaveSortOrderButton').removeClass('d-none');
				}
			}
		
		}).disableSelection();

		jQuery(document).on('click', '#formSaveSortOrderButton', function (e) {
			var formId = jQuery('#createNewFieldFormId').val();

			if(formId != undefined && formId.trim() != "") {

				userFeedback('formSaveSortOrderButton', 'loading');
				jQuery('#sortable-form-fields').sortable("disable");

				var data = [];
				var i = 0;
				jQuery('#sortable-form-fields').find('li').each(function (e) {
					data[i] = jQuery(this).data('fieldid');
					i++;
				});

				var datastr = "";
				for(var j = 0; j < data.length; j++) {
					datastr += data[j] + "-";
				}

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "backend_forms_save_order",
						"formid": formId,
						"data": JSON.stringify(data)
					},
					success: function (data) {
						if(data == "success") {
							userFeedback('formSaveSortOrderButton', 'success');
							jQuery('#sortable-form-fields').attr('order', datastr)
							jQuery('#sortable-form-fields').sortable("enable");
							setTimeout(() => {
								userFeedback('formSaveSortOrderButton', 'text');
								jQuery('#formSaveSortOrderButton').addClass('d-none');
							}, 1500);
						} else {
							userFeedback('formSaveSortOrderButton', 'error');
							setTimeout(() => {
								userFeedback('formSaveSortOrderButton', 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback('formSaveSortOrderButton', 'error');
						setTimeout(() => {
							userFeedback('formSaveSortOrderButton', 'text');
						}, 1500);
					}
				});

			}
		});

		jQuery(document).on('click', '.backend-formfield-edit', function (e) {
			var fieldid = jQuery(this).data('fieldid');
			var button = jQuery(this);

			if(fieldid != undefined && fieldid != "") {
				userFeedback(button, 'loading');
				jQuery('#editFormfieldErrorText').addClass('d-none');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "backend_forms_edit_field",
						"fieldid": fieldid
					},
					success: function (data) {
						if(data != "") {
							userFeedback(button, 'success');
							// Clear the Assemble Modal Container Div because the same IDs are used there
							jQuery('#assembleFormModalWrapper').html('');
							jQuery('#editFormFieldModalContainer').html(data);
							jQuery('#editFormFieldModal').modal('show');
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);
						} else {
							userFeedback(button, 'error');
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback('formSaveSortOrderButton', 'error');
						setTimeout(() => {
							userFeedback('formSaveSortOrderButton', 'text');
						}, 1500);
					}
				});

			} else {
				userFeedback(button, 'error');
				setTimeout(() => {
					userFeedback(button, 'text');
				}, 1500);
			}
		});

		jQuery(document).on('click', '.backend-formfield-duplicate', function (e) {
			var fieldid = jQuery(this).data('fieldid');
			var button = jQuery(this);

			if(fieldid != undefined && fieldid != "") {
				userFeedback(button, 'loading');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "backend_forms_duplicate_field",
						"fieldid": fieldid
					},
					success: function (data) {
						if(data == "1") {
							userFeedback(button, 'success');
							
							setTimeout(() => {
								userFeedback(button, 'text');
								loadFormFields();
							}, 500);
						} else {
							userFeedback(button, 'error');
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback('formSaveSortOrderButton', 'error');
						setTimeout(() => {
							userFeedback('formSaveSortOrderButton', 'text');
						}, 1500);
					}
				});

			} else {
				userFeedback(button, 'error');
				setTimeout(() => {
					userFeedback(button, 'text');
				}, 1500);
			}
		});

		jQuery(document).on('click', '#editFormFieldSubmit', function(e) {
			var type = jQuery('#editFormFieldType').val();
			var fieldid = jQuery('#editFormFieldId').val();
			
			userFeedback('editFormFieldSubmit', 'loading');
			jQuery('#editFormfieldErrorText').addClass('d-none');

			if(type != undefined && type.trim() != "" && fieldid != undefined && fieldid.trim() != "") {

				var data = {};

				if(type != "select" && type != "radio" && type != "scale" && type != "matrix") {
					jQuery('#editFormFieldModalContainer').find(':input:not([type=hidden]):not([data-toggle=collapse]):not(.btn.btn-danger):not(#radioButtonProButton):not(#selectAddOptionButton):not(#radioButtonAddButton):not(#radioNoneSelected):not(#scaleAddOptionButton):not(#matrixAddQuestionButton)').each(function (e) {
						if(jQuery(this).prop('tagName').toLowerCase() != "select" && jQuery(this).prop('tagName').toLowerCase() != "textarea" && (jQuery(this).attr('type').toLowerCase() == "radio" || jQuery(this).attr('type').toLowerCase() == "checkbox")) {
							data[jQuery(this).attr('id')] = jQuery(this).is(':checked');
						} else {
							data[jQuery(this).attr('id')] = jQuery(this).val();
						}
					});
				} else {
					jQuery('#editFormFieldModalContainer').find(':input:not([type=hidden]):not([data-toggle=collapse]):not(.btn.btn-danger):not(#radioButtonProButton):not(#selectAddOptionButton):not(#radioButtonAddButton):not(#radioNoneSelected):not(#scaleAddOptionButton):not(#matrixAddQuestionButton)').each(function (e) {
						var elementId = jQuery(this).attr('id');
						var hasNumber = /\d/;
						if(hasNumber.test(elementId)) {
							var splitId = elementId.split('-');

							var arrayName = 'elements';
							
							if(jQuery(this).attr('data-arrayName') !== undefined && jQuery(this).attr('data-arrayName') !== false) {
								arrayName = jQuery(this).attr('data-arrayName');
							}
							if(data[arrayName] === undefined) {
								data[arrayName] = {};
							}

							var optionName = splitId[splitId.length-2];
							var optionId = splitId[splitId.length-1];

							if(data[arrayName][optionId] == undefined) {
								data[arrayName][optionId] = {};
							}
							
							if(jQuery(this).prop('tagName').toLowerCase() != "select" && jQuery(this).prop('tagName').toLowerCase() != "textarea" && (jQuery(this).attr('type').toLowerCase() == "radio" || jQuery(this).attr('type').toLowerCase() == "checkbox")) {
								data[arrayName][optionId][optionName] = jQuery(this).is(':checked');
							} else {
								data[arrayName][optionId][optionName] = jQuery(this).val();
							}
						} else {
							if(jQuery(this).prop('tagName').toLowerCase() != "select" && jQuery(this).prop('tagName').toLowerCase() != "textarea" && (jQuery(this).attr('type').toLowerCase() == "radio" || jQuery(this).attr('type').toLowerCase() == "checkbox")) {
								data[jQuery(this).attr('id')] = jQuery(this).is(':checked');
							} else {
								data[jQuery(this).attr('id')] = jQuery(this).val();
							}
						}
					});
				}

				var jsondata = JSON.stringify(data);
				
				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "backend_forms_update_field",
						"fieldid": fieldid,
						"type": type,
						"data": jsondata
					},
					success: function (data) {
						if(data == "success") {
							userFeedback('editFormFieldSubmit', 'success');
							jQuery('#editFormFieldModal').modal('hide');
							jQuery('#editFormFieldModal').on('hidden.bs.modal', function (e) {
								jQuery('#editFormFieldModalContainer').html('');
							});
							
							loadFormFields();

							setTimeout(() => {
								userFeedback('editFormFieldSubmit', 'text');
							}, 1500);
						} else {
							if(data == "invaliddata") {
								jQuery('#editFormfieldErrorText').removeClass('d-none');
							}
							userFeedback('editFormFieldSubmit', 'error');
							setTimeout(() => {
								userFeedback('editFormFieldSubmit', 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback('editFormFieldSubmit', 'error');
						setTimeout(() => {
							userFeedback('editFormFieldSubmit', 'text');
						}, 1500);
					}
				});
				
			} else {
				userFeedback('editFormFieldSubmit', 'error');
				setTimeout(() => {
					userFeedback('editFormFieldSubmit', 'text');
				}, 1500);
			}
		});

		jQuery(document).on('click', '.backend-formfield-delete', function (e) {
			jQuery('#deleteFormfieldId').val(jQuery(this).data('fieldid'));
		});

		jQuery(document).on('click', '#deleteFormfieldConfirmButton', function (e) {
			var formId = jQuery('#createNewFieldFormId').val();
			var fieldid = jQuery('#deleteFormfieldId').val();

			userFeedback('deleteFormfieldConfirmButton', 'loading');

			if(formId != undefined && formId.trim() != "" && fieldid != undefined && fieldid.trim() != "") {

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "backend_forms_delete_field",
						"formid": formId,
						"fieldid": fieldid
					},
					success: function (data) {
						if(data == "success") {
							userFeedback('deleteFormfieldConfirmButton', 'success');
							jQuery('#li-formfield-'+fieldid).remove();
							jQuery('#deleteFormfieldId').val('');

							setTimeout(() => {
								jQuery('#deleteFormfieldModal').modal('hide');
								userFeedback('deleteFormfieldConfirmButton', 'text');
							}, 1500);
						} else {
							userFeedback('deleteFormfieldConfirmButton', 'error');
							setTimeout(() => {
								userFeedback('deleteFormfieldConfirmButton', 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback('deleteFormfieldConfirmButton', 'error');
						setTimeout(() => {
							userFeedback('deleteFormfieldConfirmButton', 'text');
						}, 1500);
					}
				});

			}
		});

		jQuery(document).on('click', '#previewFormButton', function (e) {
			var formId = jQuery('#createNewFieldFormId').val();

			if(formId != undefined && formId.trim() != "") {

				userFeedback('previewFormButton', 'loading');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "backend_forms_get_preview",
						"formid": formId
					},
					success: function (data) {
						if(data != "") {
							userFeedback('previewFormButton', 'success');
							jQuery('#previewFormModalContainer').html(data);
							jQuery('#previewFormModal').modal('show');

							setTimeout(() => {
								userFeedback('previewFormButton', 'text');
							}, 1500);
						} else {
							userFeedback('previewFormButton', 'error');
							setTimeout(() => {
								userFeedback('previewFormButton', 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback('previewFormButton', 'error');
						setTimeout(() => {
							userFeedback('previewFormButton', 'text');
						}, 1500);
					}
				});

			}
		});

		jQuery(document).on('keydown', '#previewFormModalContainer input', function (e) {
			if(e.keyCode == 13) {
				e.preventDefault();
			}
		});
		
		jQuery(document).on('click', '.backend-export-ts-form', function (e) {
			var formid = jQuery(this).data('formid');
			var button = jQuery(this);

			if(formid == undefined || formid == "") {
				userFeedback(button, 'error');
				setTimeout(() => {
					userFeedback(button, 'text');
				}, 1500);
			} else {
				userFeedback(button, 'loading');

				var xhr = new XMLHttpRequest();
				xhr.open('POST', ajaxurl, true);
				xhr.responseType = 'arraybuffer';
				xhr.onload = function () {
						if (this.status === 200) {
							if(this.response.byteLength > 1) {
								var filename = "";
								var disposition = xhr.getResponseHeader('Content-Disposition');
								if (disposition && disposition.indexOf('attachment') !== -1) {
										var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
										var matches = filenameRegex.exec(disposition);
										if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
								}
								var type = xhr.getResponseHeader('Content-Type');
	
								var blob = typeof File === 'function'
										? new File([this.response], filename, { type: type })
										: new Blob([this.response], { type: type });
								if (typeof window.navigator.msSaveBlob !== 'undefined') {
										// IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
										window.navigator.msSaveBlob(blob, filename);
								} else {
										var URL = window.URL || window.webkitURL;
										var downloadUrl = URL.createObjectURL(blob);
	
										if (filename) {
												// use HTML5 a[download] attribute to specify filename
												var a = document.createElement("a");
												// safari doesn't support this yet
												if (typeof a.download === 'undefined') {
														window.location = downloadUrl;
												} else {
														a.href = downloadUrl;
														a.download = filename;
														document.body.appendChild(a);
														a.click();
												}
										} else {
												window.location = downloadUrl;
										}
	
										setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
								}
							} else {
								userFeedback(button, 'error');
								setTimeout(() => {
									userFeedback(button, 'text');
								}, 1500);
							}
						}
						userFeedback(button, 'success');
						setTimeout(() => {
							userFeedback(button, 'text');
						}, 1500);
				};
				xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
				xhr.send("action=backend_forms_export&formid="+formid);
			}
		});

		jQuery(document).on('click', '.backend-duplicate-ts-form', function (e) {
			var formid = jQuery(this).data('formid');
			var button = jQuery(this);

			if(formid == undefined || formid == "") {
				userFeedback(button, 'error');
				setTimeout(() => {
					userFeedback(button, 'text');
				}, 1500);
			} else {
				userFeedback(button, 'loading');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "backend_forms_duplicate",
						"formid": formid
					},
					success: function (data) {
						if(data != "") {
							userFeedback(button, 'success');
							location.href = data;
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);
						} else {
							userFeedback(button, 'error');
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback(button, 'error');
						setTimeout(() => {
							userFeedback(button, 'text');
						}, 1500);
					}
				});
			}
		});

		jQuery(document).on('hidden.bs.modal', '#backend-import-ts-form-modal', function (e) {
			jQuery('#importTsFormModalFile').val('');
		});

		jQuery(document).on('click', '#importTsFormClearButton', function (e) {
			jQuery('#importTsFormModalFile').val('');
		});

		jQuery(document).on('click', '#backend-import-ts-form-confirm', function (e) {
			if(jQuery('#importTsFormModalFile').get(0).files.length > 0 && jQuery('#importTsFormModalFile').get(0).files[0] != undefined) {
				userFeedback('backend-import-ts-form-confirm', 'loading');
	
				var data = new FormData();
				data.append('action', 'backend_forms_import');
				for(var i = 0; i < jQuery('#importTsFormModalFile').get(0).files.length; i++) {
					data.append('file[]', jQuery('#importTsFormModalFile').get(0).files[i]);
				}
				
				
				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: data,
					processData: false,
					contentType: false,
					success: function (data) {
						if(data != "") {
							userFeedback('backend-import-ts-form-confirm', 'success');
							location.href = data;
							setTimeout(() => {
								userFeedback('backend-import-ts-form-confirm', 'text');
							}, 1500);
						} else {
							userFeedback('backend-import-ts-form-confirm', 'error');
							setTimeout(() => {
								userFeedback('backend-import-ts-form-confirm', 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback('backend-import-ts-form-confirm', 'error');
						setTimeout(() => {
							userFeedback('backend-import-ts-form-confirm', 'text');
						}, 1500);
					}
				});

			} else {
				jQuery('#importTsFormModalFile').addClass('is-invalid');
				userFeedback('backend-import-ts-form-confirm', 'error');
				setTimeout(() => {
					userFeedback('backend-import-ts-form-confirm', 'text');
				}, 1500);
			}
		});

		jQuery(document).on('change', '#importTsFormModalFile', function(e) {
			if(jQuery('#importTsFormModalFile').get(0).files.length > 0) {
				jQuery('#importTsFormModalFile').removeClass('is-invalid');
			}
		});

		jQuery(document).on('click', '#doaction', function (e) {
			if(jQuery('#bulk-action-selector-top').val() == "export_forms") {
				e.preventDefault();
				exportForms(false);
			}
		});

		jQuery(document).on('click', '#doaction2', function (e) {
			if(jQuery('#bulk-action-selector-bottom').val() == "export_forms") {
				e.preventDefault();
				exportForms(true);
			}
		});

		jQuery(document).on('click', '#backend-ts-forms-export-bulk', function (e) {
			e.preventDefault();
			exportForms(false);
		});

		function exportForms(isBottom) {
			var ids = [];

			jQuery('input[name="post[]"]:checked').each(function (e) {
				ids.push(jQuery(this).val());
			});

			if(isBottom) {
				scrollToTop(jQuery('#backend-ts-forms-export-bulk').offset().top);
			}

			userFeedback('backend-ts-forms-export-bulk', 'loading');
			
			var xhr = new XMLHttpRequest();
			xhr.open('POST', ajaxurl, true);
			xhr.responseType = 'arraybuffer';
			xhr.onload = function () {
					if (this.status === 200) {
						if(this.response.byteLength > 1) {
							var filename = "";
							var disposition = xhr.getResponseHeader('Content-Disposition');
							if (disposition && disposition.indexOf('attachment') !== -1) {
									var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
									var matches = filenameRegex.exec(disposition);
									if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
							}
							var type = xhr.getResponseHeader('Content-Type');

							var blob = typeof File === 'function'
									? new File([this.response], filename, { type: type })
									: new Blob([this.response], { type: type });
							if (typeof window.navigator.msSaveBlob !== 'undefined') {
									// IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
									window.navigator.msSaveBlob(blob, filename);
							} else {
									var URL = window.URL || window.webkitURL;
									var downloadUrl = URL.createObjectURL(blob);

									if (filename) {
											// use HTML5 a[download] attribute to specify filename
											var a = document.createElement("a");
											// safari doesn't support this yet
											if (typeof a.download === 'undefined') {
													window.location = downloadUrl;
											} else {
													a.href = downloadUrl;
													a.download = filename;
													document.body.appendChild(a);
													a.click();
											}
									} else {
											window.location = downloadUrl;
									}

									setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
							}
						} else {
							userFeedback('backend-ts-forms-export-bulk', 'error');
							setTimeout(() => {
								userFeedback('backend-ts-forms-export-bulk', 'text');
							}, 1500);
						}
					}
					userFeedback('backend-ts-forms-export-bulk', 'success');
					setTimeout(() => {
						userFeedback('backend-ts-forms-export-bulk', 'text');
					}, 1500);
			};
			xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			xhr.send("action=backend_forms_export_bulk&ids="+JSON.stringify(ids));
		}
		// ende Formulare ###########################################################

		// Start Übungen
		jQuery(document).on('click', '#backend-exercise-pages-button', function() {
			userFeedback('backend-exercise-pages-button', 'loading');

			jQuery('#backend-exercise-pages-list').html('');
			var exerciseid = jQuery('#backend-exercise-pages-formid').val();

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					"action": "backend_exercise_get_pages",
					"exerciseid": exerciseid
				},
				success: function (data) {
					userFeedback('backend-exercise-pages-button', 'success');
					jQuery('#backend-exercise-pages-list').html(data);
					setTimeout(() => {
						userFeedback('backend-exercise-pages-button', 'text');
					}, 1500);
				},
				error: function (errorThrown) {
					userFeedback('backend-exercise-pages-button', 'error');
					setTimeout(() => {
						userFeedback('backend-exercise-pages-button', 'text');
					}, 1500);
				}
			});
		});

		jQuery(document).on('click', '.exerciseFavoriteButton', function (e) {
			var button = jQuery(this);
			handleExerciseClick('favorite', button);
		})

		jQuery(document).on('click', '.exerciseUnfavoriteButton', function (e) {
			var button = jQuery(this);
			handleExerciseClick('unfavorite', button);
		})

		function handleExerciseClick(mode, button) {
			userFeedback(button, 'loading');

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					"action": "exercise_click_handle",
					"mode": mode,
					"exerciseid": jQuery(button).attr("data-exerciseid"),
					"trainingid": jQuery(button).attr("data-trainingid"),
					"lektionid": jQuery(button).attr("data-lektionid"),
					"seitenid": jQuery(button).attr("data-seitenid"),
					"url": jQuery(button).attr("data-url"),
				},
				success: function (data) {
					if(data == 1) {
						userFeedback(button, 'success');
						
						setTimeout(() => {
							if(mode == "favorite") {
								jQuery(button).parent().find('.exerciseFavoriteButton').addClass('d-none');
								jQuery(button).parent().find('.exerciseUnfavoriteButton').removeClass('d-none');
							} else {
								jQuery(button).parent().find('.exerciseFavoriteButton').removeClass('d-none');
								jQuery(button).parent().find('.exerciseUnfavoriteButton').addClass('d-none');
								
								if(jQuery(button).parent().hasClass('favoriteExerciseItemButtonArea')) {
									jQuery('.exercise-' + jQuery(button).attr("data-exerciseid")).parent().remove();

									if(jQuery('.favoriteExerciseItemButtonArea').length == 0) {
										location.reload();
									}
								}
							}
							userFeedback(button, 'text');
						}, 1500);
					} else {
						userFeedback(button, 'error');
						setTimeout(() => {
							userFeedback(button, 'text');
						}, 1500);
					}
				},
				error: function (errorThrown) {
					userFeedback(button, 'error');
					setTimeout(() => {
						userFeedback(button, 'text');
					}, 1500);
				}
			});
		}

		jQuery(document).on('click', '#backend-exercise-options-save-button', function(e){
			e.preventDefault();
			saveExerciseOptions()
		});

		function saveExerciseOptions(){

			userFeedback('backend-exercise-options-save-button', 'loading');
			var postid = jQuery('#exercise-options-id').val();
			var exerciseHeadline = "";
			var exerciseShowBackground = 0;
			var exerciseShowFavButton = 0;

			exerciseHeadline = jQuery('#exerciseHeadlineType').find(':selected').val();
			var exerciseHeadlineJSON = JSON.stringify(exerciseHeadline);

			if(jQuery('#exerciseShowBackground').is(':checked')){
				exerciseShowBackground = 1;
			}
			var exerciseShowBackgroundJSON = JSON.stringify(exerciseShowBackground);


			if(jQuery('#exerciseShowFavButton').is(':checked')){
				exerciseShowFavButton = 1;
			}
			var exerciseShowFavButtonJSON = JSON.stringify(exerciseShowFavButton);

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data:{
					"action": "admin_backend_save_exercise_options",
					"postid": postid,
					"exerciseHeadline": exerciseHeadlineJSON,
					"exerciseShowBackground": exerciseShowBackgroundJSON,
					"exerciseShowFavButton": exerciseShowFavButtonJSON
				},
				success: function(data){
					userFeedback('backend-exercise-options-save-button', 'success');
					setTimeout(() => {
						userFeedback('backend-exercise-options-save-button', 'text');
					}, 1500);
				},
				error: function(errorThrown){
					userFeedback('backend-exercise-options-save-button', 'error');
					setTimeout(() => {
						userFeedback('backend-exercise-options-save-button', 'text');
					}, 1500);
				}
			});
		}

		// ENDE Übungen

		// START Zertifikate
		jQuery(document).on('click', '#zertifikat-save-button', function(e){
			e.preventDefault();
			saveZertifikat();
		});

		function saveZertifikat(){
			
			userFeedback('zertifikat-save-button', 'loading');
			var certificate_id = jQuery('#zertifikat-post-id').val();
			var training_id = jQuery('#zertifikat-training-dropdown').find(':selected').val();

			var certificateHeader = {};
			var certificatContent = {};
			var certificatFooter = {};

			// Zertifikat-Header
			certificateHeader['logo_url'] = jQuery('#zertifikat-logo-url').val();
			certificateHeader['logo_id'] = jQuery('#zertifikat-logo-id').val();
			certificateHeader['logo_height'] = jQuery('#zertifikat-logo-height').val();
			certificateHeader['logo_width'] = jQuery('#zertifikat-logo-width').val();
			certificateHeader['logo_hwratio'] = jQuery('#zertifikat-logo-hwratio').val();
			if(jQuery('#zertifikat-logo-orientation-left').is(':checked')){
				certificateHeader['logo_orientation'] = "left";
			}
			else if(jQuery('#zertifikat-logo-orientation-center').is(':checked')){
				certificateHeader['logo_orientation'] = "center";
			}
			else if(jQuery('#zertifikat-logo-orientation-right').is(':checked')){
				certificateHeader['logo_orientation'] = "right";
			}

			// Zertifikat-Inhalt
			certificatContent['titel'] = jQuery('#zertifikat-titel').val();
			certificatContent['titel_color'] = jQuery('#zertifikat-titel-color').val();
			if(jQuery('#zertifikat-titel-orientation-left').is(':checked')){
				certificatContent['titel_orientation'] = "left";
			}
			else if(jQuery('#zertifikat-titel-orientation-center').is(':checked')){
				certificatContent['titel_orientation'] = "center";
			}
			else if(jQuery('#zertifikat-titel-orientation-right').is(':checked')){
				certificatContent['titel_orientation'] = "right";
			}

			certificatContent['additional_info_text'] = jQuery('#zertifikat-additional-info-text').val();

			if(jQuery('#zertifikat-add-alt-info-text').is(':checked')){
				certificatContent['add_alt_info_text'] = "1";
			}
			else{
				certificatContent['add_alt_info_text'] = "0";
			}

			certificatContent['alt_info_text'] = jQuery('#zertifikat-alt-info-text').val();

			certificatContent['additional_info_color'] = jQuery('#zertifikat-additional-info-color').val();
			if(jQuery('#zertifikat-introduction-orientation-left').is(':checked')){
				certificatContent['introduction_orientation'] = "left";
			}
			else if(jQuery('#zertifikat-introduction-orientation-center').is(':checked')){
				certificatContent['introduction_orientation'] = "center";
			}
			else if(jQuery('#zertifikat-introduction-orientation-right').is(':checked')){
				certificatContent['introduction_orientation'] = "right";
			}

			certificatContent['training_title'] = jQuery('#zertifikat-training-dropdown').find(':selected').text();
			certificatContent['training_id'] = training_id;

			if(jQuery('#zertifikat-add-alt-training-title').is(':checked')){
				certificatContent['add_alt_training_title'] = "1";
			}
			else{
				certificatContent['add_alt_training_title'] = "0";
			}

			certificatContent['alt_training_title'] = jQuery('#zertifikat-alt-training-title').val();

			certificatContent['training_title_color'] = jQuery('#zertifikat-training-name-color').val();
			if(jQuery('#zertifikat-training-title-orientation-left').is(':checked')){
				certificatContent['training_title_orientation'] = "left";
			}
			else if(jQuery('#zertifikat-training-title-orientation-center').is(':checked')){
				certificatContent['training_title_orientation'] = "center";
			}
			else if(jQuery('#zertifikat-training-title-orientation-right').is(':checked')){
				certificatContent['training_title_orientation'] = "right";
			}

			certificatContent['additional_traininginfo_text'] = jQuery('#zertifikat-additional-traininginfo-text').val();
			certificatContent['additional_traininginfo_color'] = jQuery('#zertifikat-additional-traininginfo-color').val();
			if(jQuery('#zertifikat-additional-traininginfo-orientation-left').is(':checked')){
				certificatContent['additional_traininginfo_orientation'] = "left";
			}
			else if(jQuery('#zertifikat-additional-traininginfo-orientation-center').is(':checked')){
				certificatContent['additional_traininginfo_orientation'] = "center";
			}
			else if(jQuery('#zertifikat-additional-traininginfo-orientation-right').is(':checked')){
				certificatContent['additional_traininginfo_orientation'] = "right";
			}

			certificatContent['closing_location'] = jQuery('#zertifikat-closing-location').val();
			certificatContent['closing_persons'] = jQuery('#zertifikat-closing-persons').val();
			certificatContent['closing_img_url'] = jQuery('#zertifikat-closing-img-url').val();
			certificatContent['closing_img_id'] = jQuery('#zertifikat-closing-img-id').val();
			certificatContent['closing_img_height'] = jQuery('#zertifikat-closing-img-height').val();
			certificatContent['closing_img_width'] = jQuery('#zertifikat-closing-img-width').val();
			certificatContent['closing_img_hwratio'] = jQuery('#zertifikat-closing-img-hwratio').val();
			certificatContent['closing_color'] = jQuery('#zertifikat-closing-color').val();
			
			if(jQuery('#zertifikat-closing-orientation-left').is(':checked')){
				certificatContent['closing_orientation'] = "left";
			}
			else if(jQuery('#zertifikat-closing-orientation-center').is(':checked')){
				certificatContent['closing_orientation'] = "center";
			}
			else if(jQuery('#zertifikat-closing-orientation-right').is(':checked')){
				certificatContent['closing_orientation'] = "right";
			}

			// Zertifikat-Footer
			certificatFooter['footer_text'] = jQuery('#zertifikat-footer-text').val();
			certificatFooter['footer_color'] = jQuery('#zertifikat-footer-color').val();
			
			if(jQuery('#zertifikat-add-userid').is(':checked')){
				certificatFooter['add_userid'] = "1";
			}
			else{
				certificatFooter['add_userid'] = "0";
			}
			
			if(jQuery('#zertifikat-add-validationcode').is(':checked')){
				certificatFooter['add_validationcode'] = "1";
			}
			else{
				certificatFooter['add_validationcode'] = "0";
			}

			var jsonCertificateHeader = JSON.stringify(certificateHeader);
			var jsonCertificateContent = JSON.stringify(certificatContent);
			var jsonCertificateFooter = JSON.stringify(certificatFooter);

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data:{
					"action": "admin_backend_save_zertifikat",
					"certificate_id": certificate_id,
					"training_id": training_id,
					"certificate_header": jsonCertificateHeader,
					"certificate_content": jsonCertificateContent,
					"certificate_footer": jsonCertificateFooter
				},
				success: function(data){
					userFeedback('zertifikat-save-button', 'success');
					setTimeout(() => {
						userFeedback('zertifikat-save-button', 'text');
					}, 1500);
				},
				error: function(errorThrown){
					userFeedback('zertifikat-save-button', 'error');
					setTimeout(() => {
						userFeedback('zertifikat-save-button', 'text');
					}, 1500)
				}
			});
		}

		jQuery(document).on('click', '#zertifikat-logo-button', function(e){
			e.preventDefault();
			// Media Selection
			var file_frame = wp.media.frames.file_frame = wp.media({
				title: 'Bild für Logo auswählen oder hochladen',
				library: { 
					type: ['image/jpeg', 'image/png', 'image/jpg']
				},
				button: {
					text: 'Als Logo auswählen'
				},
				multiple: false
			});
	
			file_frame.on('select', function(){
				var attachment = file_frame.state().get('selection').first().toJSON();
				jQuery('#zertifikat-logo-url').val(attachment['url']);
				jQuery('#zertifikat-logo-id').val(attachment['id']);
				jQuery('#zertifikat-no-logo').addClass('d-none');
				jQuery('#zertifikat-logo-preview').removeClass('d-none');
				jQuery('#zertifikat-logo-preview').find('img').attr('src', attachment['url']);
				// Werte für die beiden Eingabefelder zum Festlegen der Größe setzen
				var logoHeightWidthRatio = attachment['height']/attachment['width'];
				var heightLogo = 25;
				var widthLogo = heightLogo/logoHeightWidthRatio;
	
				if(widthLogo > 160){
					widthLogo = 160;
					heightLogo = widthLogo*logoHeightWidthRatio;
				}
	
				jQuery('#zertifikat-logo-options').removeClass('d-none');
				jQuery('#zertifikat-remove-logo-button').removeClass('d-none');
				
				jQuery('#zertifikat-logo-height').attr('max', Math.floor(heightLogo));
				jQuery('#zertifikat-logo-width').attr('max', Math.floor(widthLogo));
				jQuery('#zertifikat-logo-height').val(Math.floor(heightLogo));
				jQuery('#zertifikat-logo-width').val(Math.floor(widthLogo));
				jQuery('#zertifikat-logo-hwratio').val(logoHeightWidthRatio);
			});
	
			file_frame.on('open', function(){
				
				if(jQuery('#zertifikat-logo-id').val().trim() != ""){
					var selection = file_frame.state().get('selection');
					var selectedImageId = jQuery('#zertifikat-logo-id').val();
					var attachment = wp.media.attachment(selectedImageId);
					attachment.fetch();
					selection.add(attachment ? [attachment] : []);
				}
				
			});
	
			// Rufe Modal für Media Selection auf
			file_frame.open();
		});

		jQuery(document).on('input', '#zertifikat-logo-height', function(){
			var imageHeight = parseInt(jQuery('#zertifikat-logo-height').val());
			var imageHeightWidthRatio = parseFloat(jQuery('#zertifikat-logo-hwratio').val());
			var newImageWidth = imageHeight/imageHeightWidthRatio;
			jQuery('#zertifikat-logo-width').val(Math.floor(newImageWidth));
		});

		jQuery(document).on('click', '#zertifikat-remove-logo-button', function(e){
			e.preventDefault();
			jQuery('#zertifikat-remove-logo-button').addClass('d-none');
			jQuery('#zertifikat-logo-options').addClass('d-none');
			jQuery('#zertifikat-logo-url').val('');
			jQuery('#zertifikat-logo-id').val('');
			jQuery('#zertifikat-no-logo').removeClass('d-none');
			jQuery('#zertifikat-logo-preview').addClass('d-none');
			jQuery('#zertifikat-logo-preview').find('img').removeAttr('src')
		});

		jQuery(document).on('click', '#zertifikat-closing-img-button', function(e){
			e.preventDefault();
			var file_frame_closing_img = wp.media.frames.file_frame = wp.media({
				title: 'Bild auswählen oder hochladen',
				library: { 
					type: ['image/jpeg', 'image/png', 'image/jpg']
				},
				button: {
					text: 'Auswählen'
				},
				multiple: false
			});
	
			file_frame_closing_img.on('select', function(){
				var attachment = file_frame_closing_img.state().get('selection').first().toJSON();
				jQuery('#zertifikat-closing-img-url').val(attachment['url']);
				jQuery('#zertifikat-closing-img-id').val(attachment['id']);
				jQuery('#zertifikat-no-closing-img').addClass('d-none');
				jQuery('#zertifikat-closing-img-preview').removeClass('d-none');
				jQuery('#zertifikat-closing-img-preview').find('img').attr('src', attachment['url']);
				// Werte für die beiden Eingabefelder zum Festlegen der Größe setzen
				var logoHeightWidthRatio = attachment['height']/attachment['width'];
				var heightLogo = 25;
				var widthLogo = heightLogo/logoHeightWidthRatio;
	
				if(widthLogo > 160){
					widthLogo = 160;
					heightLogo = widthLogo*logoHeightWidthRatio;
				}
	
				
				jQuery('#zertifikat-closing-img-options').removeClass('d-none');
				jQuery('#zertifikat-remove-closing-img-button').removeClass('d-none');
				
				jQuery('#zertifikat-closing-img-height').attr('max', Math.floor(heightLogo));
				jQuery('#zertifikat-closing-img-width').attr('max', Math.floor(widthLogo));
				jQuery('#zertifikat-closing-img-height').val(Math.floor(heightLogo));
				jQuery('#zertifikat-closing-img-width').val(Math.floor(widthLogo));
				jQuery('#zertifikat-closing-img-hwratio').val(logoHeightWidthRatio);
				
			});
	
			file_frame_closing_img.on('open', function(){
				
				if(jQuery('#zertifikat-closing-img-id').val().trim() != ""){
					var selection = file_frame_closing_img.state().get('selection');
					var selectedImageId = jQuery('#zertifikat-closing-img-id').val();
					var attachment = wp.media.attachment(selectedImageId);
					attachment.fetch();
					selection.add(attachment ? [attachment] : []);
				}
				
			});
	
			// Rufe Modal für Media Selection auf
			file_frame_closing_img.open();
		});

		jQuery(document).on('click', '#zertifikat-add-alt-info-text', function(e){
			if(jQuery('#zertifikat-add-alt-info-text').is(':checked')) {
				jQuery('#zertifikat-alt-info-text').removeAttr('disabled');
			}
			else{
				jQuery('#zertifikat-alt-info-text').prop('disabled', true);
			}
		});

		jQuery(document).on('click', '#zertifikat-add-alt-training-title', function(e){
			if(jQuery('#zertifikat-add-alt-training-title').is(':checked')) {
				jQuery('#zertifikat-alt-training-title').removeAttr('disabled');
			}
			else{
				jQuery('#zertifikat-alt-training-title').prop('disabled', true);
			}
		});
	
		jQuery(document).on('input', '#zertifikat-closing-img-height', function(){
			var imageHeight = parseInt(jQuery('#zertifikat-closing-img-height').val());
			var imageHeightWidthRatio = parseFloat(jQuery('#zertifikat-closing-img-hwratio').val());
			var newImageWidth = imageHeight/imageHeightWidthRatio;
			jQuery('#zertifikat-closing-img-width').val(Math.floor(newImageWidth));
		});
	
		jQuery(document).on('click', '#zertifikat-remove-closing-img-button', function(e){
			e.preventDefault();
			jQuery('#zertifikat-remove-closing-img-button').addClass('d-none');
			jQuery('#zertifikat-closing-img-options').addClass('d-none');
			jQuery('#zertifikat-closing-img-url').val('');
			jQuery('#zertifikat-closing-img-id').val('');
			jQuery('#zertifikat-no-closing-img').removeClass('d-none');
			jQuery('#zertifikat-closing-img-preview').addClass('d-none');
			jQuery('#zertifikat-closing-img-preview').find('img').removeAttr('src')
		});

		jQuery(document).on('click', '#zertifikat-add-userid', function(e){
			if(jQuery('#zertifikat-add-userid').is(':checked')){
				jQuery('#zertifikat-add-validationcode').removeAttr('disabled');
			}
			else{
				jQuery('#zertifikat-add-validationcode').prop('disabled', true);
			}
		});
		
		jQuery(document).on('click', '.backend-duplicate-zertifikat', function(){
			var zertifikatId = jQuery(this).attr('zertifikat-id');
			var button = jQuery(this);

			if(zertifikatId == undefined || zertifikatId == ""){
				userFeedback(button, 'error');
				setTimeout(() => {
					userFeedback(button, 'text');
				}, 1500);
			}
			else{
				userFeedback(button, 'loading');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "admin_backend_duplicate_zertifikat",
						"zertifikatId":zertifikatId
					},
					success: function(data){
						if(data != ""){
							userFeedback(button, 'success');
							location.href = data;
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);	
						}
						else{
							userFeedback(button, 'error');
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback(button, 'error');
						setTimeout(() => {
							userFeedback(button, 'text');
						}, 1500);
					}
				});
			}
		});
		// ENDE Zertifikate

		// START Avatare
		jQuery(document).on('click', '#avatar-image-button', function(e){
			e.preventDefault();
			// Media Selection
			var file_frame_image = wp.media.frames.file_frame = wp.media({
				title: 'Bilddatei auswählen oder hochladen',
				library: { 
					type: ['image/jpeg', 'image/png', 'image/jpg']
				},
				button: {
					text: 'Auswählen'
				},
				multiple: false
			});

			file_frame_image.on('select', function(){
				var attachment = file_frame_image.state().get('selection').first().toJSON();
				jQuery('#avatar-image-url').val(attachment['url']);
				jQuery('#avatar-image-id').val(attachment['id']);
				jQuery('#avatar-no-image').addClass('d-none');
				jQuery('#avatar-image-preview').removeClass('d-none');
				jQuery('#avatar-image-preview').find('img').attr('src', attachment['url']);
				jQuery('#avatar-remove-image-button').removeClass('d-none');
			});

			file_frame_image.on('open', function(){			
				if(jQuery('#avatar-image-id').val().trim() != ""){
					var selection = file_frame_image.state().get('selection');
					var selectedImageId = jQuery('#avatar-image-id').val();
					var attachment = wp.media.attachment(selectedImageId);
					attachment.fetch();
					selection.add(attachment ? [attachment] : []);
				}				
			});

			// Rufe Modal für Media Selection auf
			file_frame_image.open();
		});

		jQuery(document).on('click', '#avatar-remove-image-button', function(e){
			e.preventDefault();
			jQuery('#avatar-remove-image-button').addClass('d-none');
			jQuery('#avatar-image-url').val('');
			jQuery('#avatar-image-id').val('');
			jQuery('#avatar-no-image').removeClass('d-none');
			jQuery('#avatar-image-preview').addClass('d-none');
			jQuery('#avatar-image-preview').find('img').removeAttr('src')
		});

		jQuery(document).on('click', '#avatar-gif-button', function(e){
			e.preventDefault();
			// Media Selection
			var file_frame_gif = wp.media.frames.file_frame = wp.media({
				title: 'GIF-Datei auswählen oder hochladen',
				library: { 
					type: 'image/gif' 
				},
				button: {
					text: 'Auswählen'
				},
				multiple: false
			});

			file_frame_gif.on('select', function(){
				var attachment = file_frame_gif.state().get('selection').first().toJSON();
				jQuery('#avatar-gif-url').val(attachment['url']);
				jQuery('#avatar-gif-id').val(attachment['id']);
				jQuery('#avatar-no-gif').addClass('d-none');
				jQuery('#avatar-gif-preview').removeClass('d-none');
				jQuery('#avatar-gif-preview').find('img').attr('src', attachment['url']);
				jQuery('#avatar-remove-gif-button').removeClass('d-none');
			});

			file_frame_gif.on('open', function(){			
				if(jQuery('#avatar-gif-id').val().trim() != ""){
					var selection = file_frame_gif.state().get('selection');
					var selectedImageId = jQuery('#avatar-gif-id').val();
					var attachment = wp.media.attachment(selectedImageId);
					attachment.fetch();
					selection.add(attachment ? [attachment] : []);
				}				
			});

			// Rufe Modal für Media Selection auf
			file_frame_gif.open();
		});

		jQuery(document).on('click', '#avatar-remove-gif-button', function(e){
			e.preventDefault();
			jQuery('#avatar-remove-gif-button').addClass('d-none');
			jQuery('#avatar-gif-url').val('');
			jQuery('#avatar-gif-id').val('');
			jQuery('#avatar-no-gif').removeClass('d-none');
			jQuery('#avatar-gif-preview').addClass('d-none');
			jQuery('#avatar-gif-preview').find('img').removeAttr('src')
		});

		jQuery(document).on('click', '#backend-avatar-options-save-button', function(e){
			e.preventDefault();
			saveAvatarOptions()
		});

		function saveAvatarOptions(){

			userFeedback('backend-avatar-options-save-button', 'loading');
			var postid = jQuery('#avatar-options-id').val();
			var avatarImage = {};
			var avatarGif = {};

			avatarImage['url'] = jQuery('#avatar-image-url').val();
			avatarImage['id'] = jQuery('#avatar-image-id').val();
			avatarGif['url'] = jQuery('#avatar-gif-url').val();
			avatarGif['id'] = jQuery('#avatar-gif-id').val();

			var avatarImageJSON = JSON.stringify(avatarImage);
			var avatarGifJSON = JSON.stringify(avatarGif);

			var isDefaultAvatar = jQuery('#default-avatar-checkbox').is(':checked');

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data:{
					"action": "admin_backend_save_avatar_options",
					"postid": postid,
					"avatarImage": avatarImageJSON,
					"avatarGif": avatarGifJSON,
					"isDefaultAvatar": isDefaultAvatar
				},
				success: function(data){
					userFeedback('backend-avatar-options-save-button', 'success');
					setTimeout(() => {
						userFeedback('backend-avatar-options-save-button', 'text');
					}, 1500);
				},
				error: function(errorThrown){
					userFeedback('backend-avatar-options-save-button', 'error');
					setTimeout(() => {
						userFeedback('backend-avatar-options-save-button', 'text');
					}, 1500);
				}
			});
		}

		jQuery(document).on('click', '#select-avatar-button', function(e){
			e.preventDefault();
			var thisButton = jQuery(this);
			userFeedback(thisButton, 'loading');
			var avatarid = jQuery(thisButton).parent().find('#select-avatar-id').val();
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data:{
					"action": "select_avatar",
					"avatarid": avatarid
				},
				success: function(data){
					userFeedback(thisButton, 'success');
					setTimeout(() => {
						//userFeedback(thisButton, 'text');
						location.reload();
					}, 1500);
				},
				error: function(errorThrown){
					userFeedback(thisButton, 'error');
					setTimeout(() => {
						userFeedback(thisButton, 'text');
					}, 1500);
				}
			});
		});

		// ENDE Avatare

		// START Abzeichen
		jQuery(document).on('click', '#abzeichen-lektionsabzeichen-checkbox', function(e){
			if(jQuery('#abzeichen-lektionsabzeichen-checkbox').is(':checked')){
				jQuery('#abzeichen-lektionsabzeichen-options').removeClass('d-none');
				var trainingId = jQuery('#abzeichen-training-dropdown').find(':selected').val();
				jQuery('.backend-abzeichen-training-lektionsliste[training-id="'+trainingId+'"]').removeClass('d-none');
			}
			else{
				jQuery('#abzeichen-lektionsabzeichen-options').addClass('d-none');
			}
		});

		jQuery(document).on('click', '[name=abzeichen-count-lektionsabzeichen]', function(e){
			if(jQuery('#abzeichen-count-lektionsabzeichen-single').is(':checked')){
				jQuery('.lektionsabzeichen-lektion-incomplete').addClass('d-none');
			}
			else if(jQuery('#abzeichen-count-lektionsabzeichen-double').is(':checked')){
				jQuery('.lektionsabzeichen-lektion-incomplete').removeClass('d-none');
			}
		});

		jQuery(document).on('change', '#abzeichen-training-dropdown', function(e){
			var trainingId = jQuery('#abzeichen-training-dropdown').find(':selected').val();
			jQuery('.backend-abzeichen-training-lektionsliste').addClass('d-none');
			jQuery('.backend-abzeichen-training-lektionsliste[training-id="'+trainingId+'"]').removeClass('d-none');
		});

		jQuery(document).on('click', '.backend-lektionsabzeichen-select-image', function(e){
			e.preventDefault();
			var thisButton = jQuery(this);

			var file_frame = wp.media.frames.file_frame = wp.media({
				title: 'Bilddatei auswählen oder hochladen',
				library: { 
					type: ['image/jpeg', 'image/png', 'image/jpg']
				},
				button: {
					text: 'Auswählen'
				},
				multiple: false
			});

			file_frame.on('select', function(){
				var attachment = file_frame.state().get('selection').first().toJSON();
				jQuery(thisButton).parent().find('.backend-lektionsabzeichen-image-url').val(attachment['url']);
				jQuery(thisButton).parent().find('.backend-lektionsabzeichen-image-id').val(attachment['id']);
				jQuery(thisButton).parent().parent().parent().find('.backend-lektionsabzeichen-no-image').addClass('d-none');
				jQuery(thisButton).parent().parent().parent().find('.backend-lektionsabzeichen-image-preview').removeClass('d-none');
				jQuery(thisButton).parent().parent().parent().find('.backend-lektionsabzeichen-image-preview img').attr('src', attachment['url']);
				jQuery(thisButton).parent().find('.backend-lektionsabzeichen-remove-image').removeClass('d-none');
			});

			file_frame.on('open', function(){			
				if(jQuery(thisButton).parent().find('.backend-lektionsabzeichen-image-id').val().trim() != ""){
					var selection = file_frame.state().get('selection');
					var selectedImageId = jQuery(thisButton).parent().find('.backend-lektionsabzeichen-image-id').val();
					var attachment = wp.media.attachment(selectedImageId);
					attachment.fetch();
					selection.add(attachment ? [attachment] : []);	
				}				
			});

			// Rufe Modal für Media Selection auf
			file_frame.open();
		});

		jQuery(document).on('click', '.backend-lektionsabzeichen-remove-image', function(e){
			e.preventDefault();
			var thisButton = jQuery(this);
			jQuery(thisButton).addClass('d-none');
			jQuery(thisButton).parent().find('.backend-lektionsabzeichen-image-url').val('');
			jQuery(thisButton).parent().find('.backend-lektionsabzeichen-image-id').val('');
			jQuery(thisButton).parent().parent().parent().find('.backend-lektionsabzeichen-no-image').removeClass('d-none');
			jQuery(thisButton).parent().parent().parent().find('.backend-lektionsabzeichen-image-preview').addClass('d-none');
			jQuery(thisButton).parent().parent().parent().find('.backend-lektionsabzeichen-image-preview img').removeAttr('src');
		});

		jQuery(document).on('click', '#backend-abzeichen-options-save-button', function(e){
			e.preventDefault();
			userFeedback('backend-abzeichen-options-save-button','loading');
			var abzeichenId = jQuery('#abzeichen-options-id').val();
			var trainingId = jQuery('#abzeichen-training-dropdown').find(':selected').val();
			var hasLektionsabzeichen = jQuery('#abzeichen-lektionsabzeichen-checkbox').is(':checked');
			var countLektionsabzeichen = "";
			if(jQuery('#abzeichen-count-lektionsabzeichen-single').is(':checked')) {
				countLektionsabzeichen = "single";
			} else if(jQuery('#abzeichen-count-lektionsabzeichen-double').is(':checked')) {
				countLektionsabzeichen = "double";
			}

			var lektionsabzeichenListe = {};
			jQuery('.backend-abzeichen-training-lektionsliste:not(.d-none)').find('.backend-abzeichen-lektion').each(function(){
				var lektionsabzeichen = {};
				var lektionsabzeichenCompleteURL = jQuery(this).find('.lektionsabzeichen-lektion-complete .backend-lektionsabzeichen-image-url').val();
				var lektionsabzeichenCompleteID = jQuery(this).find('.lektionsabzeichen-lektion-complete .backend-lektionsabzeichen-image-id').val();
				var lektionsabzeichenIncompleteURL = jQuery(this).find('.lektionsabzeichen-lektion-incomplete .backend-lektionsabzeichen-image-url').val();
				var lektionsabzeichenIncompleteID = jQuery(this).find('.lektionsabzeichen-lektion-incomplete .backend-lektionsabzeichen-image-id').val();
				lektionsabzeichen['lektionsabzeichenCompleteURL'] = lektionsabzeichenCompleteURL;
				lektionsabzeichen['lektionsabzeichenCompleteID'] = lektionsabzeichenCompleteID;
				lektionsabzeichen['lektionsabzeichenIncompleteURL'] = lektionsabzeichenIncompleteURL;
				lektionsabzeichen['lektionsabzeichenIncompleteID'] = lektionsabzeichenIncompleteID;
				lektionsabzeichenListe[jQuery(this).attr('lektion-id')] = lektionsabzeichen;
			});
			var lektionsabzeichenListeJSON = JSON.stringify(lektionsabzeichenListe);
			
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data:{
					"action": "admin_backend_abzeichen_save_options",
					"abzeichenId": abzeichenId,
					"trainingId": trainingId,
					"hasLektionsabzeichen": hasLektionsabzeichen,
					"countLektionsabzeichen": countLektionsabzeichen,
					"lektionsabzeichen": lektionsabzeichenListeJSON
				},
				success: function(data){
					userFeedback('backend-abzeichen-options-save-button', 'success');
					setTimeout(() => {
						userFeedback('backend-abzeichen-options-save-button', 'text');
					}, 1500);
				},
				error: function(errorThrown){
					userFeedback('backend-abzeichen-options-save-button', 'error');
					setTimeout(() => {
						userFeedback('backend-abzeichen-options-save-button', 'text');
					}, 1500);
				}
			});
			
		});
		// ENDE Abzeichen

		// START Dashboard
		jQuery(document).on('click', '#dashboard-certificate-no-name-link', function(){
			jQuery('#dashboardCertificateNoNameModal').modal('show');
		});

		jQuery(document).on('click', '#dashboard-certificate-no-name-button', function(){
			jQuery('#dashboardCertificateNoNameModal').modal('hide');
		});
		// ENDE Dashboard

		// Start StudienExport #######################################################

		jQuery(document).on("change paste keyup", ".studyexport-training-search", function(e) {
			var trainingid = jQuery(this).attr('data-trainingid');
			setTimeout(function(){ 
				filterStudyExportTraining(trainingid);	
			}, 300);
		  });

		jQuery(document).on('click', '.studyexport-training-clear-search-button', function (e) {
			var trainingid = jQuery(this).attr('data-trainingid');
			jQuery('#studyexport-training-'+trainingid+'-search').val('');
			filterStudyExportTraining(trainingid);	
		});

		function filterStudyExportTraining(trainingid) {
			var tmps = ""+jQuery('#studyexport-training-'+trainingid+'-search').val().toLowerCase();
			
			jQuery('#studyexport-training-'+trainingid+'-check div').css("display","none")
	
			jQuery('#studyexport-training-'+trainingid+'-check div label').each(function (e) {
				if(jQuery(this).html().toLowerCase().indexOf(tmps) >= 0) {
					jQuery(this).parent().css("display","");
				}
			});
		}

		jQuery(document).on("change paste keyup", ".studyexport-lektion-search", function(e) {
			var trainingid = jQuery(this).attr('data-trainingid');
			var lektionid = jQuery(this).attr('data-lektionid');
			setTimeout(function(){ 
				filterStudyExportLektion(trainingid, lektionid);	
			}, 300);
		  });

		jQuery(document).on('click', '.studyexport-lektion-clear-search-button', function (e) {
			var trainingid = jQuery(this).attr('data-trainingid');
			var lektionid = jQuery(this).attr('data-lektionid');
			jQuery('#studyexport-training-'+trainingid+'-lektion-'+lektionid+'-search').val('');
			filterStudyExportLektion(trainingid, lektionid);	
		});

		function filterStudyExportLektion(trainingid, lektionid) {
			var tmps = ""+jQuery('#studyexport-training-'+trainingid+'-lektion-'+lektionid+'-search').val().toLowerCase();
			
			jQuery('#studyexport-training-'+trainingid+'-lektion-'+lektionid+'-check div').css("display","none")
	
			jQuery('#studyexport-training-'+trainingid+'-lektion-'+lektionid+'-check div label').each(function (e) {
				if(jQuery(this).html().toLowerCase().indexOf(tmps) >= 0) {
					jQuery(this).parent().css("display","");
				}
			});
		}

		jQuery(document).on("change paste keyup", "#studyexport-users-search", function(e) {
			setTimeout(function(){ 
				filterStudyExportUser();	
			}, 300);
		  });

		jQuery(document).on('click', '#studyexport-users-clear-search-button', function (e) {
			jQuery('#studyexport-users-search').val('');
			filterStudyExportUser();	
		});

		function filterStudyExportUser() {
			var tmps = ""+jQuery('#studyexport-users-search').val().toLowerCase();
			
			jQuery('#studyexport-users-check div').css("display","none")
	
			jQuery('#studyexport-users-check div label').each(function (e) {
				if(jQuery(this).html().toLowerCase().indexOf(tmps) >= 0) {
					jQuery(this).parent().css("display","");
				}
			});
		}

		jQuery(document).on('click', '#studyexport-users-select-button', function (e) {
			jQuery('#studyexport-users-check div').each(function (e) {
				if(jQuery(this).css('display') != "none") {
					jQuery(this).find('input').prop('checked', true); 
				}
			});
		})

		jQuery(document).on('click', '#studyexport-users-remove-button', function (e) {
			jQuery('#studyexport-users-check div').each(function (e) {
				if(jQuery(this).css('display') != "none") {
					jQuery(this).find('input').prop('checked', false); 
				}
			});
		})

		jQuery(document).on('submit', '#studyExportForm', function(e) {
			e.preventDefault();
			jQuery('#studyExportButton').click();
		})

		jQuery(document).on('click', '#studyExportButton', function(e) {
			e.preventDefault();

			var exportdata = {};
			exportdata['trainings'] = {};

			jQuery('.studyexport-training-check').each(function () {
				if(jQuery(this).is(':checked')) {
					var trainingid = jQuery(this).val();
					exportdata['trainings'][trainingid] = {};
					exportdata['trainings'][trainingid]['tsformids'] = [];
					exportdata['trainings'][trainingid]['lektionen'] = {};

					jQuery('.studyexport-training-'+trainingid+'-formselect').each(function() {
						if(jQuery(this).is(':checked')) {
							exportdata['trainings'][trainingid]['tsformids'].push(jQuery(this).val());
						}
					})
				}
			})
			jQuery('.studyexport-lektion-check').each(function () {
				if(jQuery(this).is(':checked')) {
					var trainingid = jQuery(this).attr('data-trainingid');
					if(exportdata['trainings'][trainingid] == undefined) {
						exportdata['trainings'][trainingid] = {};
						exportdata['trainings'][trainingid]['tsformids'] = [];
						exportdata['trainings'][trainingid]['lektionen'] = {};
					}
					var lektionid = jQuery(this).val();
					exportdata['trainings'][trainingid]['lektionen'][lektionid] = [];
					jQuery('.studyexport-lektion-'+lektionid+'-formselect').each(function () {
						if(jQuery(this).is(':checked')) {
							exportdata['trainings'][trainingid]['lektionen'][lektionid].push(jQuery(this).val());
						}
					})
				}
			})

			var users = [];
			jQuery('.studyexport-users-formselect').each(function () {
				if(jQuery(this).is(':checked')) {
					users.push(jQuery(this).val());
				}
			});

			jQuery('#studyExportError').addClass('d-none');
			jQuery('#studyexportloading').modal('show');
			userFeedback('studyExportButton', 'loading');

			var xhr = new XMLHttpRequest();
			xhr.open('POST', ajaxurl, true);
			xhr.responseType = 'arraybuffer';
			xhr.onload = function () {
					$('#studyexportloading').modal('hide');
					userFeedback('studyExportButton', 'success');
					setTimeout(() => {
						userFeedback('studyExportButton', 'text');
					}, 1500);

					if (this.status === 200) {
						console.log(this.response);
						if(this.response.byteLength > 1) {
							var filename = "";
							var disposition = xhr.getResponseHeader('Content-Disposition');
							if (disposition && disposition.indexOf('attachment') !== -1) {
									var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
									var matches = filenameRegex.exec(disposition);
									if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
							}
							var type = xhr.getResponseHeader('Content-Type');

							var blob = typeof File === 'function'
									? new File([this.response], filename, { type: type })
									: new Blob([this.response], { type: type });
							if (typeof window.navigator.msSaveBlob !== 'undefined') {
									// IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
									window.navigator.msSaveBlob(blob, filename);
							} else {
									var URL = window.URL || window.webkitURL;
									var downloadUrl = URL.createObjectURL(blob);

									if (filename) {
											// use HTML5 a[download] attribute to specify filename
											var a = document.createElement("a");
											// safari doesn't support this yet
											if (typeof a.download === 'undefined') {
													window.location = downloadUrl;
											} else {
													a.href = downloadUrl;
													a.download = filename;
													document.body.appendChild(a);
													a.click();
											}
									} else {
											window.location = downloadUrl;
									}

									setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
							}
						} else {
							userFeedback('studyExportButton', 'error');
							jQuery('#studyExportError').removeClass('d-none');
							setTimeout(() => {
								userFeedback('studyExportButton', 'text');
							}, 1500);
						}
					}
			};
			xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			xhr.send("action=studyexport&data="+JSON.stringify(exportdata)+"&user="+JSON.stringify(users));
		})

		// start button navi lektion  ################################################

		submitFormsLek = function (link) {
			saveTsForms(jQuery('#pagebuttons-lektionen-button'), link, null, false);
		}


		submitFormsBack = function (link) {
			saveTsForms(jQuery('#pagebuttons-back-button'), link, null, false);
		}

		submitFormsNext = function (link,isLastPage) {
			saveTsForms(jQuery('#pagebuttons-next-button'), link, null, isLastPage);	
		}

		submitFormsPage = function(element, link) {
			saveTsForms(jQuery(element), link, null, false);
		}

		submitDropdownFormsPage = function(event, element, link) {
			event.stopPropagation();
			saveTsForms(jQuery(element), link, null, false);
		}
		// ende button navi lektion  ################################################

		// Checktable Start ################################################

		//add id to each row in table:
		jQuery( ".check-table tbody tr" ).each(function(index) {
			jQuery(this ).attr("id","row_"+index);
		});

		//toggle checkmark on click event:
		jQuery(".check-table td:not(:first-child)").click(function() {
			var this_table_row = jQuery(this).parent().attr('id');
			/*
			// (Vorherige) Variante 1 - HEAVY CHECK MARK
			if(!(jQuery('#'+this_table_row).parent().parent().hasClass("check-table-gr-h")&&jQuery(this).is(":nth-child(4)"))){
				jQuery('#'+this_table_row).children().find("img").remove();
				jQuery(this).text("✔");
			}
			// Variante 1 - Ende
			*/
			
			// Variante 2 - (einfaches) CHECK MARK
			var checkIcon = "\u2713"; 
			if(!(jQuery('#'+this_table_row).parent().parent().hasClass("check-table-gr-h")&&jQuery(this).is(":nth-child(4)"))){
				jQuery('#'+this_table_row).children(".check-table td:not(:first-child):contains("+checkIcon+")").text("");
				jQuery(this).text(checkIcon);
			}
			// Variante 2 - Ende

		});

		// Umschalten Sichtbarkeit letzte Spalte
		jQuery(".check-table-gr-h td:nth-child(2), .check-table-gr-h td:nth-child(3)").on("click", function(e){
			var thisRow = jQuery(this).parent();
			// Verbergen von Spalte 4
			if(jQuery(this).is(":nth-child(2)")){
				if(jQuery(thisRow).find(":nth-child(4)").children().is("p")){
					jQuery(thisRow).find(":nth-child(4) p").css("visibility", "hidden");
				}
				else if(jQuery(thisRow).find(":nth-child(4)").children().is("ul")){
					jQuery(thisRow).find(":nth-child(4) ul").css("visibility", "hidden");
				}
				else if(jQuery(thisRow).find(":nth-child(4)").children().is("ol")){
					jQuery(thisRow).find(":nth-child(4) ol").css("visibility", "hidden");
				}
			}
			// Zeigen von Spalte 4
			else if(jQuery(this).is(":nth-child(3)")){
				if(jQuery(thisRow).find(":nth-child(4)").children().is("p")){
					jQuery(thisRow).find(":nth-child(4) p").css("visibility", "visible");
				}
				else if(jQuery(thisRow).find(":nth-child(4)").children().is("ul")){
					jQuery(thisRow).find(":nth-child(4) ul").css("visibility", "visible");
				}
				else if(jQuery(thisRow).find(":nth-child(4)").children().is("ol")){
					jQuery(thisRow).find(":nth-child(4) ol").css("visibility", "visible");
				}
			}
		});

		// Checktable Ende ################################################

		// GDPR Cookie Compliance Plugin v2.0.6 ################################################

		jQuery('#moove_gdpr_cookie_info_bar').addClass('d-none');
		setTimeout(() => {
			if (!jQuery( "#moove_gdpr_cookie_info_bar.moove-gdpr-info-bar-hidden" ).length ) {
				jQuery('.change-settings-button').click();
				jQuery("head").append('<style></style>');
				var newStyleElement = jQuery("head").children(':last');
				newStyleElement.html('.menu-item-strict-necessary-cookies{display:none!important;}');
			}
		}, 500);

		// ende ################################################

		// User Profil Seite
		jQuery('#user-mail').focusout(function(e) {
			if(!validateMail(jQuery('#user-mail').val())) {
				jQuery('#user-mail').parent().find('input').first().addClass('is-invalid');
				jQuery('#user-mail').parent().find('.invalid-feedback').first().removeClass('d-none');
			} else {
				jQuery('#user-mail').parent().find('input').first().removeClass('is-invalid');
				jQuery('#user-mail').parent().find('.invalid-feedback').first().addClass('d-none');
			}
		});

		jQuery(document).on('click', '#user-profil-submit-mail', function(e){
			e.preventDefault();
			if(validateMail(jQuery('#user-mail').val())){
				jQuery('#userprofileSaveMailModal').modal('show');
			}
			else{
				jQuery('#user-mail').parent().find('input').first().addClass('is-invalid');
				jQuery('#user-mail').parent().find('.invalid-feedback').first().removeClass('d-none');
			}
		});

		jQuery('#form-profile-mail').on('submit', function (e) {
			e.preventDefault();
			jQuery('#form-profile-mail').find('input').blur();
			jQuery('#user-profil-submit-mail').click();
		});

		jQuery(document).on('click', '#user-profile-savemail-button', function(e){
			userFeedback('user-profile-savemail-button', 'loading');
			var data = {};
			data['action'] = "user_profil_save_mail";
			jQuery('#form-profile-mail').find('input').each(function() {
			
				if(jQuery(this).attr("type") == "radio") {
					if(jQuery(this).is(':checked')){
						data[jQuery(this).attr('name')] = jQuery(this).val();
					}
				} else {
					data[jQuery(this).attr('name')] = jQuery(this).val();
				}
			});

			console.log(data);
			
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				success: function (data) {
					if(data == "1") {
						userFeedback('user-profile-savemail-button', 'success');
						setTimeout(() => {
							location.reload();
						}, 1500);
					} else {
						userFeedback('user-profile-savemail-button', 'error');
						setTimeout(() => {
							userFeedback('user-profile-savemail-button', 'text');
						}, 1500);
					}

				},
				error: function (errorThrown) {
					userFeedback('user-profile-savemail-button', 'error');
					setTimeout(() => {
						userFeedback('user-profile-savemail-button', 'text');
					}, 1500);
					console.error(errorThrown);
				}
			});

		});

		jQuery('#form-profile-password input').on('keydown', function (e) {
			if(e.keyCode == 13) {
				e.preventDefault();
				jQuery('#form-profile-password').find('input').blur();
				jQuery('#user-profil-submit-password').click();
			}
		});

		jQuery(document).on('click', '#user-profil-submit-password', function(e){
			e.preventDefault();
			jQuery('#userprofileSavePasswordModal').modal('show');
		});

		jQuery(document).on('click', '#user-profile-savepassword-button', function(e){
			userFeedback('user-profile-savepassword-button', 'loading');

			jQuery('#user-profile-password-tooweak').addClass("d-none");
			jQuery('#user-pw0').removeClass('is-invalid');
			jQuery('#user-pw0').parent().find('.invalid-feedback').first().addClass('d-none');
			jQuery('#user-pw1').removeClass('is-invalid');
			jQuery('#user-pw2').removeClass('is-invalid');
			jQuery('#user-pw2').parent().find('.invalid-feedback').first().addClass('d-none');

			var data = {};
			data['action'] = "user_profil_save_password";
			jQuery('#form-profile-password').find('input').each(function() {
				if(jQuery(this).attr("type") == "checkbox") {
					data[jQuery(this).attr('name')] = jQuery(this).is(':checked');
				} else {
					data[jQuery(this).attr('name')] = jQuery(this).val();
				}
			});

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				success: function (data) {
					if(data == "1") {
						userFeedback('user-profile-savepassword-button', 'success');
						jQuery('#form-profile-password').find('input').each(function() {
							jQuery(this).val('');
						});
						setTimeout(() => {
							location.href=site_url;
						}, 1500);
					} else {
						userFeedback('user-profile-savepassword-button', 'error');
						if(data == "tooweak") {
							jQuery('#user-profile-password-tooweak').removeClass("d-none");
						} else if(data == "falseold") {
							jQuery('#user-pw0').addClass('is-invalid');
							jQuery('#user-pw0').parent().find('.invalid-feedback').first().removeClass('d-none');
						} else if(data == "falseconfirm") {
							jQuery('#user-pw1').addClass('is-invalid');
							jQuery('#user-pw2').addClass('is-invalid');
							jQuery('#user-pw2').parent().find('.invalid-feedback').first().removeClass('d-none');
						}
						setTimeout(() => {
							jQuery('#userprofileSavePasswordModal').modal('hide');
							userFeedback('user-profile-savepassword-button', 'text');
						}, 1500);
					}
				},
				error: function (errorThrown) {
					userFeedback('user-profile-savepassword-button', 'error');
					jQuery('#user-profile-password-tooweak').addClass("d-none");
					setTimeout(() => {
						userFeedback('user-profile-savepassword-button', 'text');
					}, 1500);
					console.error(errorThrown);
				}
			});

		});

		jQuery(document).on('click', '#user-profil-submit-delete-content', function(e){
			e.preventDefault();
			jQuery('#userprofileDeleteContentModal').modal('show');
		});
		
		jQuery(document).on('click', '#user-profile-deletecontent-button', function(e){
			userFeedback('user-profile-deletecontent-button', 'loading');
			var data = {};
			data['action'] = "user_profil_delete_content";
			jQuery('#form-profil-delete-content').find('input').each(function() {
				if(jQuery(this).attr("type") == "checkbox") {
					data[jQuery(this).attr('name')] = jQuery(this).is(':checked');
				} else {
					data[jQuery(this).attr('name')] = jQuery(this).val();
				}
			});

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				success: function (data) {
					if(data == "1") {
						userFeedback('user-profile-deletecontent-button', 'success');
						setTimeout(() => {
							location.reload();
						}, 1500);
					} else {
						userFeedback('user-profile-deletecontent-button', 'error');
						setTimeout(() => {
							userFeedback('user-profile-deletecontent-button', 'text');
						}, 1500);
					}
				},
				error: function (errorThrown) {
					userFeedback('user-profile-deletecontent-button', 'error');
					setTimeout(() => {
						userFeedback('user-profile-deletecontent-button', 'text');
					}, 1500);
					console.error(errorThrown);
				}
			});
		});

		jQuery(document).on('click', '#user-profil-submit-delete-account', function(e){
			e.preventDefault();
			jQuery('#userprofileDeleteAccountModal').modal('show');
		});

		jQuery(document).on('click', '#user-profile-deleteaccount-button', function(e){
			userFeedback('user-profile-deleteaccount-button', 'loading');
			var data = {};
			data['action'] = "user_profil_delete_account";
			jQuery('#form-profil-delete-account').find('input').each(function() {
				if(jQuery(this).attr("type") == "checkbox") {
					data[jQuery(this).attr('name')] = jQuery(this).is(':checked');
				} else {
					data[jQuery(this).attr('name')] = jQuery(this).val();
				}
			});

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				success: function (data) {
					if(data == "1") {
						userFeedback('user-profile-deleteaccount-button', 'success');
						setTimeout(() => {
							location.href=site_url;
						}, 1500);
						
					} else {
						userFeedback('user-profile-deleteaccount-buttont', 'error');
						setTimeout(() => {
							userFeedback('user-profile-deleteaccount-button', 'text');
						}, 1500);
					}
				},
				error: function (errorThrown) {
					userFeedback('user-profile-deleteaccount-button', 'error');
					setTimeout(() => {
						userFeedback('user-profile-deleteaccount-button', 'text');
					}, 1500);
					console.error(errorThrown);
				}
			});
		});

		jQuery('#form-profile-name input').on('keydown', function (e) {
			if(e.keyCode == 13) {
				e.preventDefault();
				jQuery('#form-profile-name').find('input').blur();
				jQuery('#user-profil-submit-name').click();
			}
		});

		jQuery(document).on('click', '#user-profil-submit-name', function(e){
			e.preventDefault();
			jQuery('#userprofileSaveNameModal').modal('show');
		});

		jQuery(document).on('click', '#user-profile-savename-button', function(e){
			userFeedback('user-profile-savename-button', 'loading');
			var data = {};
			data['action'] = 'user_profil_save_name';
			jQuery('#form-profile-name').find('input').each(function(){
				if(jQuery(this).attr("type") == "checkbox") {
					data[jQuery(this).attr('name')] = jQuery(this).is(':checked');
				} else {
					data[jQuery(this).attr('name')] = jQuery(this).val();
				}
			});

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				success: function(data){
					if(data == "1"){
						userFeedback('user-profile-savename-button', 'success');
						setTimeout(()=>{
							location.reload();
						}, 1500);
					} 
					else{
						userFeedback('user-profile-savename-button', 'error');
						setTimeout(() => {
							userFeedback('user-profile-savename-button', 'text');
						}, 1500);
					}

				},
				error: function(errorThrown){
					userFeedback('user-profile-savename-button', 'error');
					setTimeout(() => {
						userFeedback('user-profile-savename-button', 'text');
					}, 1500);
					console.error(errorThrown);
				}
			});
		});

		jQuery('#form-profile-phonenumber input').on('keydown', function (e) {
			if(e.keyCode == 13) {
				e.preventDefault();
				jQuery('#form-profile-phonenumber').find('input').blur();
				jQuery('#user-profil-submit-phonenumber').click();
			}
		});

		jQuery(document).on('click', '#user-profil-submit-phonenumber', function(e){
			e.preventDefault();
			jQuery('#userprofileSavePhonenumberModal').modal('show');
		});

		jQuery(document).on('click', '#user-profile-savephonenumber-button', function(e){
			userFeedback('user-profile-savephonenumber-button', 'loading');
			var data = {};
			data['action'] = 'user_profil_save_phonenumber';
			jQuery('#form-profile-phonenumber').find('input').each(function(){
				if(jQuery(this).attr("type") == "checkbox") {
					data[jQuery(this).attr('name')] = jQuery(this).is(':checked');
				} else {
					data[jQuery(this).attr('name')] = jQuery(this).val();
				}
			});

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				success: function(data){
					if(data == "1"){
						userFeedback('user-profile-savephonenumber-button', 'success');
						setTimeout(()=>{
							location.reload();
						}, 1500);
					} 
					else{
						userFeedback('user-profile-savephonenumber-button', 'error');
						setTimeout(() => {
							userFeedback('user-profile-savephonenumber-button', 'text');
						}, 1500);
					}

				},
				error: function(errorThrown){
					userFeedback('user-profile-savephonenumber-button', 'error');
					setTimeout(() => {
						userFeedback('user-profile-savephonenumber-button', 'text');
					}, 1500);
					console.error(errorThrown);
				}
			});
		});


		jQuery(document).on('click', '#user-profil-submit-privacy', function(e){
			e.preventDefault();
			jQuery('#userprofileSavePrivacyModal').modal('show');
		});

		jQuery(document).on('click', '#user-profil-submit-reject-privacy', function(e){
			e.preventDefault();
			jQuery('#userprofileSavePrivacyModal').modal('show');
		});

		jQuery(document).on('click', '#user-profile-savePrivacy-button', function(e){
			userFeedback('user-profile-savePrivacy-button', 'loading');
			var data = {};
			data['action'] = 'user_profil_save_privacy';
			jQuery('#form-profile-privacy').find('input').each(function(){
				if(jQuery(this).attr("type") == "checkbox") {
					data[jQuery(this).attr('name')] = jQuery(this).is(':checked');
				} else {
					data[jQuery(this).attr('name')] = jQuery(this).val();
				}
			});

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				success: function(data){
					if(data == "1"){
						userFeedback('user-profile-savePrivacy-button', 'success');
						setTimeout(()=>{
							location.reload();
						}, 1500);
					} 
					else{
						userFeedback('user-profile-savePrivacy-button', 'error');
						setTimeout(() => {
							userFeedback('user-profile-savePrivacy-button', 'text');
						}, 1500);
					}

				},
				error: function(errorThrown){
					userFeedback('user-profile-savePrivacy-button', 'error');
					setTimeout(() => {
						userFeedback('user-profile-savePrivacy-button', 'text');
					}, 1500);
					console.error(errorThrown);
				}
			});
		});

		// Ende User Profil Seite

		// Freischaltcodes
		jQuery('#code-input').focusout(function(e) {
			if(!jQuery.isNumeric(jQuery('#code-input').val())) {
				jQuery('#code-input').parent().find('input').first().addClass('is-invalid');
				jQuery('#code-input').parent().find('.invalid-feedback').first().removeClass('d-none');
			} else {
				jQuery('#code-input').parent().find('input').first().removeClass('is-invalid');
				jQuery('#code-input').parent().find('.invalid-feedback').first().addClass('d-none');
			}
		});

		jQuery('#registerkey-submit').click(function (e) {
			e.preventDefault();
			redeemRegisterkey('form-registerkey-redeem');
		});

		jQuery('#form-registerkey-redeem').submit(function(e) {
			e.preventDefault();
			redeemRegisterkey('form-registerkey-redeem');
		})

		function redeemRegisterkey(formid) {
			userFeedback('registerkey-submit', 'loading');

			var data = {};
			data['action'] = "user_redeem_registerkey";
			jQuery('#' + formid).find('input').each(function() {
				if(jQuery(this).attr("type") == "checkbox") {
					data[jQuery(this).attr('name')] = jQuery(this).is(':checked');
				} else {
					data[jQuery(this).attr('name')] = jQuery(this).val();
				}
			});
			
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				success: function (data) {
					if(data == "1") {
						userFeedback('registerkey-submit', 'success');
						jQuery('#code-input').val('');
						
					} else {
						if(data == "invalid") {
							jQuery('#code-input').parent().find('input').first().addClass('is-invalid');
							jQuery('#code-input').parent().find('.invalid-feedback').first().removeClass('d-none');
						}
						userFeedback('registerkey-submit', 'error');
					}
					setTimeout(() => {
						userFeedback('registerkey-submit', 'text');
					}, 1500);
				},
				error: function (errorThrown) {
					userFeedback('registerkey-submit', 'error');
					setTimeout(() => {
						userFeedback('registerkey-submit', 'text');
					}, 1500);
				}
			});
		}

		// Ende Freischaltocdes

		// Nutzer manuell anlegen
		// E-Mail Listener kommt von Nutzer bearbeiten
		jQuery('#input-field-user-name').focusout(function(e) {
			if(jQuery('#input-field-user-name').val().trim() == "") {
				jQuery('#user-create-manually-error').removeClass('d-block');
				jQuery('#user-create-manually-error').html('');

				jQuery('#input-field-user-name').parent().find('input').first().addClass('is-invalid');
				jQuery('#input-field-user-name').parent().find('.invalid-feedback').first().removeClass('d-none');
			} else {
				jQuery('#input-field-user-name').parent().find('input').first().removeClass('is-invalid');
				jQuery('#input-field-user-name').parent().find('.invalid-feedback').first().addClass('d-none');
			}
		});
		
		jQuery('#create-user-manually-form').submit(function(e) {
			e.preventDefault();
			createUserManually('create-user-manually-form');
		});

		jQuery('#create-user-manually-button').click(function(e) {
			e.preventDefault();
			createUserManually('create-user-manually-form');
		})

		function createUserManually(formid) {
			jQuery('#user-create-manually-error').removeClass('d-block');
			jQuery('#user-create-manually-error').html('');

			if(jQuery('#input-field-user-name').val().trim() != "" && validateMail(jQuery('#input-field-mail').val())) { 
				userFeedback('create-user-manually-button', 'loading');

				var data = {};
				data['action'] = "create_user_manually";
				jQuery('#' + formid).find('input:not(.permission-checkbox), select').each(function() {
					if(jQuery(this).attr("type") == "checkbox") {
						data[jQuery(this).attr('name')] = jQuery(this).is(':checked');
					} else {
						data[jQuery(this).attr('name')] = jQuery(this).val();
					}
				});

				data['permissions'] = [];
				jQuery('#' + formid).find('input.permission-checkbox').each(function() {
					if(jQuery(this).attr("type") == "checkbox" && jQuery(this).is(':checked')) {
						data['permissions'].push(jQuery(this).val());
					}
				})
				
				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: data,
					success: function (data) {
						if(data == "1") {
							userFeedback('create-user-manually-button', 'success');
							setTimeout(() => {
								jQuery('#'+formid).find('input, select').each(function() {
									if(jQuery(this).attr("type") != "hidden") {
										if(jQuery(this).attr("type") == "checkbox") {
											if(jQuery(this).is('#checkbox-create-user-manually')){
												jQuery(this).prop('checked', true);
											}
											else{
												jQuery(this).prop('checked', false);
											}
										} else {
											jQuery(this).val('');
										}
									}
								});
							}, 500);
						} else {
							if(data != "0") {
								jQuery('#user-create-manually-error').html(data);
								jQuery('#user-create-manually-error').addClass('d-block');
							}
							if(data.includes("Berechtigung")) {
								jQuery('#'+formid).find('input, select').each(function() {
									if(jQuery(this).attr("type") != "hidden") {
										if(jQuery(this).attr("type") == "checkbox") {
											if(jQuery(this).is('#checkbox-create-user-manually')){
												jQuery(this).prop('checked', true);
											}
											else{
												jQuery(this).prop('checked', false);
											}
										} else {
											jQuery(this).val('');
										}
									}
								});

								setTimeout(() => {
									jQuery('#user-create-manually-error').removeClass('d-block');
									jQuery('#user-create-manually-error').html('');
								}, 5000);
							}
							userFeedback('create-user-manually-button', 'error');
						}
						setTimeout(() => {
							userFeedback('create-user-manually-button', 'text');
						}, 1500);
					},
					error: function (errorThrown) {
						userFeedback('create-user-manually-button', 'error');
						setTimeout(() => {
							userFeedback('create-user-manually-button', 'text');
						}, 1500);
					}
				});
			} else {
				if(!validateMail(jQuery('#input-field-mail').val())) {
					jQuery('#input-field-mail').parent().find('input').first().addClass('is-invalid');
					jQuery('#input-field-mail').parent().find('.invalid-feedback').first().removeClass('d-none');
				}
				if(jQuery('#input-field-user-name').val().trim() == "") {
					jQuery('#input-field-user-name').parent().find('input').first().addClass('is-invalid');
					jQuery('#input-field-user-name').parent().find('.invalid-feedback').first().removeClass('d-none');
				}
			}
		}
		// Ende Nutzer manuell anlegen

		// Freischaltcodes
		jQuery('#input-code, #maxcount, #studiengruppenid').on('keyup', function (e) {
			if(e.keyCode == 13) {
				jQuery(this).blur();
				newRegisterkey('registerkey-new-form');
			}
		});

		jQuery('#input-code').on('keypress', function(e){
			return e.metaKey || // cmd/ctrl
			  e.which <= 0 || // arrow keys
			  e.which == 8 || // delete key
			  /[0-9]/.test(String.fromCharCode(e.which)); // numbers
		});

		jQuery('#input-code').focusout(function(e) {
			if(isNaN(jQuery('#input-code').val()) || jQuery('#input-code').val() == "") {
				jQuery('#input-code').addClass('is-invalid');
				jQuery('#input-code').parent().find('.invalid-feedback').first().removeClass('d-none');
			} else {
				jQuery('#input-code').removeClass('is-invalid');
				jQuery('#input-code').parent().find('.invalid-feedback').first().addClass('d-none');
				jQuery('#input-code').parent().find('.invalid-feedback').last().addClass('d-none');
			}
		});

		jQuery('#maxcount').on('keypress', function(e){
			return e.metaKey || // cmd/ctrl
			  e.which <= 0 || // arrow keys
			  e.which == 8 || // delete key
			  /[0-9]/.test(String.fromCharCode(e.which)); // numbers
		});

		jQuery('#maxcount').focusout(function(e) {		
			if(/^[1-9]\d*$/.test(jQuery('#maxcount').val()) || jQuery('#maxcount').val() == "") {
				jQuery('#maxcount').removeClass('is-invalid');
				jQuery('#maxcount').parent().find('.invalid-feedback').first().addClass('d-none');
			} else {
				jQuery('#maxcount').addClass('is-invalid');
				jQuery('#maxcount').parent().find('.invalid-feedback').first().removeClass('d-none');
			}
		});

		jQuery(document).on('change', '#select-company', function (e) {
			jQuery(this).parent().parent().find('#select-group').first().find('option:not(:first)').attr('disabled', true);
			var companyid = jQuery(this).val();
			jQuery(this).parent().parent().find('#select-group').first().find('option[data-companyid="'+companyid+'"]').removeAttr('disabled');
		})

		jQuery('#registerkey-new-form').submit(function(e) {
			e.preventDefault();
			newRegisterkey('registerkey-new-form');
		});

		jQuery('#registerkey-new-button').click(function(e) {
			e.preventDefault();
			newRegisterkey('registerkey-new-form');
		})

		jQuery(document).on('change', '#select-group', function (e) {
			if(jQuery(this).hasClass('is-invalid') && jQuery(this).val() != "" && jQuery(this).val() != 0) {
				jQuery(this).removeClass('is-invalid');
				jQuery(this).parent().find('.invalid-feedback').removeClass('d-block');
			}
		})

		function newRegisterkey(formid) {
			userFeedback('registerkey-new-button', 'loading');

			jQuery('#input-code').parent().find('input').first().removeClass('is-invalid');
			jQuery('#input-code').parent().find('.invalid-feedback').first().addClass('d-none');
			jQuery('#input-code').parent().find('.invalid-feedback').last().addClass('d-none');
			
			jQuery('#maxcount').parent().find('input').first().removeClass('is-invalid');
			jQuery('#maxcount').parent().find('.invalid-feedback').first().addClass('d-none');

			var data = {};
			data['action'] = "registerkey_add";
			jQuery('#' + formid).find('input, select').each(function() {
				data[jQuery(this).attr('name')] = jQuery(this).val();
			});

			var grouperror = false;
			if(jQuery('#select-company').val()!=0 && jQuery('#select-group option:not(:disabled)').length <=1) {
				jQuery('#select-group').addClass('is-invalid');
				jQuery('#select-group').parent().find('.invalid-feedback').addClass('d-block');
				grouperror = true;
			}
			
			if((/^[1-9]\d*$/.test(jQuery('#maxcount').val()) || jQuery('#maxcount').val() == "") && data['code'] != "" && !isNaN(data['code']) && !grouperror) {
				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: data,
					success: function (data) {
						if(data == "1") {
							userFeedback('registerkey-new-button', 'success');
							setTimeout(() => {
								location.reload();
							}, 1500);
						} else {
							userFeedback('registerkey-new-button', 'error');
							if(data == "duplicate") {
								jQuery('#input-code').parent().find('input').first().addClass('is-invalid');
								jQuery('#input-code').parent().find('.invalid-feedback').last().removeClass('d-none');
							}
						}
						setTimeout(() => {
							userFeedback('registerkey-new-button', 'text');
						}, 1500);
					},
					error: function (errorThrown) {
						userFeedback('registerkey-new-button', 'error');
						setTimeout(() => {
							userFeedback('registerkey-new-button', 'text');
						}, 1500);
					}
				});
			} else {
				if(!/^[1-9]\d*$/.test(jQuery('#maxcount').val()) && jQuery('#maxcount').val() != "") {
					jQuery('#maxcount').parent().find('input').first().addClass('is-invalid');
					jQuery('#maxcount').parent().find('.invalid-feedback').first().removeClass('d-none');
				}

				if(data['code'] == "" || isNaN(data['code'])) {
					jQuery('#input-code').parent().find('input').first().addClass('is-invalid');
					jQuery('#input-code').parent().find('.invalid-feedback').first().removeClass('d-none');
				}

				userFeedback('registerkey-new-button', 'error');
				setTimeout(() => {
					userFeedback('registerkey-new-button', 'text');
				}, 1500);
			}
		}

		jQuery(document).on('click', '.delete-register-key', function (e) {
			var regcode = jQuery(this).find('span').first().html();
			var openregcode = jQuery("#open_register_code").val();
			jQuery('#registerkey-delete-code').val(regcode);
			if(regcode == openregcode){
				jQuery('#deleteRegisterkeyModal').find("div.alert").removeClass("d-none");
			}
			else{
				jQuery('#deleteRegisterkeyModal').find("div.alert").addClass("d-none");
			}
			jQuery('#deleteRegisterkeyModal').modal('show');
		});

		jQuery(document).on('hidden.bs.modal', '#deleteRegisterkeyModal', function (e) {
			jQuery('#registerkey-delete-code').val('');
			jQuery('#open_register_code').val('');
			jQuery('#registerkey-delete-demologin').addClass('d-none');
			userFeedback('registerkey-delete-button', 'text');
		});

		jQuery(document).on('click', '.deleteregisterkey-confirm', function(e) {
			var regcode = jQuery('#registerkey-delete-code').val();
			jQuery('#registerkey-delete-demologin').addClass('d-none');

			if(regcode != "") {
				userFeedback('registerkey-delete-button', 'loading');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "registerkey_delete",
						"registerkey": regcode
					},
					success: function (data) {
						if(data == "1") {
							userFeedback('registerkey-delete-button', 'success');
							setTimeout(() => {
								location.reload();
							}, 1500);
						} else {
							if(data == "registerkey-demologin") {
								jQuery('#registerkey-delete-demologin').removeClass('d-none');
								userFeedback('registerkey-delete-button', 'error');
							} else {
								userFeedback('registerkey-delete-button', 'error');
								setTimeout(() => {
									userFeedback('registerkey-delete-button', 'text');
								}, 1500);
							}
						}
					},
					error: function (errorThrown) {
						userFeedback('registerkey-delete-button', 'error');
						setTimeout(() => {
							userFeedback('registerkey-delete-button', 'text');
						}, 1500);
						console.error(errorThrown);
					}
				});
			}
		});

		jQuery(document).on('click', '.edit-register-key', function (e) {
			var options = jQuery(this).find('span');
			var code = jQuery(options[0]).html();
			var count = jQuery(options[1]).html();
			var maxCount = jQuery(options[2]).html();
			
			jQuery('#updateRegisterkeyCode').html(code);
			jQuery('#registerkey-update-code').val(code);
			jQuery('#maxCountInput').val(parseInt(maxCount));
			jQuery('#maxCountInput').prop('min', parseInt(count));

			jQuery('#updateRegisterkeyModal').modal('show');
		});

		jQuery(document).on('keyup', '#maxCountInput', function (e) {
			if(e.keyCode == 13) {
				jQuery(this).blur();
				jQuery('#registerkey-update-button').click();
			}
		});

		jQuery(document).on('click', '.edit-vorlage', function (e) {
			
			jQuery('#select-update-vorlage').val(jQuery(this).attr("data-vorlagenid"));
			jQuery('#update-vorlage-registerkey').val(jQuery(this).attr("data-registerkey"));

			jQuery('#updateVorlageModal').modal('show');
		});

		jQuery(document).on('hidden.bs.modal', '#updateVorlageModal', function (e) {
			jQuery('#select-update-vorlage').val(0);
			jQuery('#update-vorlage-registerkey').val('');
			jQuery('#registerkey-update-toolow').removeClass('d-block');
		});

		jQuery(document).on('click', '.updateregisterkey-confirm', function(e) {
			var regcode = jQuery('#registerkey-update-code').val();
			var maxCount = jQuery('#maxCountInput').val();
			jQuery('#registerkey-update-toolow').removeClass('d-block');

			if(regcode != "" && (maxCount >= 0 || maxCount == "")) {
				userFeedback('registerkey-update-button', 'loading');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "registerkey_update",
						"registerkey": regcode,
						"maxcount": maxCount
					},
					success: function (data) {
						if(data == "1") {
							userFeedback('registerkey-update-button', 'success');
							setTimeout(() => {
								location.reload();
							}, 1500);
						} else {
							if(data == "toolow") {
								jQuery('#registerkey-update-toolow').addClass('d-block');
							}
							userFeedback('registerkey-update-button', 'error');
							setTimeout(() => {
								userFeedback('registerkey-update-button', 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback('registerkey-update-button', 'error');
						setTimeout(() => {
							userFeedback('registerkey-update-button', 'text');
						}, 1500);
						console.error(errorThrown);
					}
				});
			}
		});

		jQuery(document).on('click', '#vorlage-update-button', function(e) {
			var vorlage = jQuery('#select-update-vorlage').val();
			var registerkey = jQuery('#update-vorlage-registerkey').val();

			if(vorlage != "" && registerkey != "") {
				userFeedback('vorlage-update-button', 'loading');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "registerkey_vorlage_update",
						"vorlage": vorlage,
						"registerkey": registerkey
					},
					success: function (data) {
						if(data == "1") {
							userFeedback('vorlage-update-button', 'success');

							if(vorlage == 0) {
								jQuery('#table tr[data-registerkey="' + registerkey + '"] td.vorlagen-td div.vorlagen-div').first().html('<i>keine oder gelöschte Vorlage</i>');
								jQuery('#table tr[data-registerkey="' + registerkey + '"] td.vorlagen-td span.edit-vorlage').first().attr("data-vorlagenid", 0);
							} else {
								var newname = jQuery('#select-update-vorlage option[value="'+ vorlage + '"]').html();
								jQuery('#table tr[data-registerkey="' + registerkey + '"] td.vorlagen-td div.vorlagen-div').first().html(newname);
								jQuery('#table tr[data-registerkey="' + registerkey + '"] td.vorlagen-td span.edit-vorlage').first().attr("data-vorlagenid", vorlage);
							}
							setTimeout(() => {
								userFeedback('vorlage-update-button', 'text');
								jQuery('#updateVorlageModal').modal('hide');
							}, 1500);
						} else {
							userFeedback('vorlage-update-button', 'error');
							setTimeout(() => {
								userFeedback('vorlage-update-button', 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback('vorlage-update-button', 'error');
						setTimeout(() => {
							userFeedback('vorlage-update-button', 'text');
						}, 1500);
						console.error(errorThrown);
					}
				});
			} else {
				userFeedback('vorlage-update-button', 'error');
				setTimeout(() => {
					userFeedback('vorlage-update-button', 'text');
				}, 1500);
			}
		});

		// Ende Freischaltcodes

		// Anfang Tooltip
		jQuery(document).ready(function(e){
			jQuery('[data-toggle="tooltip"]').tooltip();
		});
		// Ende Tooltip
		
		// Anfang Systemstatistiken

		// Zurücksetzen Zoom in Linecharts
		jQuery(document).on('click', '#systemstatistiken-reset-zoom-user-active', function(e){
			lineChartUserActive.resetZoom();
		});
		jQuery(document).on('click', '#systemstatistiken-reset-zoom-user-login', function(e){
			lineChartUserLogin.resetZoom();
		});
		jQuery(document).on('click', '#systemstatistiken-reset-zoom-user-input', function(e){
			lineChartUserInput.resetZoom();
		});
		jQuery(document).on('click', '#systemstatistiken-reset-zoom-user-lektionen', function(e){
			lineChartLektion.resetZoom();
		});

		// Wechsel Stunde/Tag/Monat für line-Charts
		// line-Chart systemstatistics-user-active.html ("Anzahl hinzugefügte Benutzer")
		jQuery(document).on('click', '#lineChartUserActiveHourButton', function(e){
			changeDateRepresentationLineChart("lineChartUserActiveHourButton","useractive", 0);
		});
		jQuery(document).on('click', '#lineChartUserActiveDayButton', function(e){
			changeDateRepresentationLineChart("lineChartUserActiveDayButton","useractive", 1);
		});

		jQuery(document).on('click', '#lineChartUserActiveMonthButton', function(e){
			changeDateRepresentationLineChart("lineChartUserActiveMonthButton","useractive", 2);
		});
		// line-Chart systemstatistics-user-login.html ("Zeitpunkt des letzten Logins")
		jQuery(document).on('click', '#lineChartUserLoginHourButton', function(e){
			changeDateRepresentationLineChart("lineChartUserLoginHourButton","userlogin", 0);
		});
		jQuery(document).on('click', '#lineChartUserLoginDayButton', function(e){
			changeDateRepresentationLineChart("lineChartUserLoginDayButton","userlogin", 1);
		});

		jQuery(document).on('click', '#lineChartUserLoginMonthButton', function(e){
			changeDateRepresentationLineChart("lineChartUserLoginMonthButton","userlogin", 2);
		});
		// line-Chart systemstatistics-user-input.html ("Anzahl Benutzereingaben")
		jQuery(document).on('click', '#lineChartUserInputHourButton', function(e){
			changeDateRepresentationLineChart("lineChartUserInputHourButton","userinput", 0);
		});
		jQuery(document).on('click', '#lineChartUserInputDayButton', function(e){
			changeDateRepresentationLineChart("lineChartUserInputDayButton","userinput", 1);
		});

		jQuery(document).on('click', '#lineChartUserInputMonthButton', function(e){
			changeDateRepresentationLineChart("lineChartUserInputMonthButton","userinput", 2);
		});
		// line-Chart systemstatistics-user-lektionen.html ("Anzahl abgeschlossene Lektionen")
		jQuery(document).on('click', '#lineChartUserLektionenHourButton', function(e){
			changeDateRepresentationLineChart("lineChartUserLektionenHourButton","userlektionen", 0);
		});
		jQuery(document).on('click', '#lineChartUserLektionenDayButton', function(e){
			changeDateRepresentationLineChart("lineChartUserLektionenDayButton","userlektionen", 1);
		});

		jQuery(document).on('click', '#lineChartUserLektionenMonthButton', function(e){
			changeDateRepresentationLineChart("lineChartUserLektionenMonthButton","userlektionen", 2);
		});

		// Funktion zum Wechseln der Repräsentation (Stunde/Tag/Monat) für die line-Charts
		function changeDateRepresentationLineChart(button, chart, id){
			userFeedback(button, 'loading');
			// Abrufen der Eingabefelder für einen potentiell gewählten Zeitraum
			var systemstatistikenStartDate = jQuery("#systemstatistikenStartDate").val();
			var systemstatistikenEndDate = jQuery("#systemstatistikenEndDate").val();
			jQuery.ajax({
				type: 'GET',
				url: ajaxurl,
				data:{
					"action": "systemstatistiken_settimeperiod",
					"systemstatistikenStartDate": systemstatistikenStartDate,
					"systemstatistikenEndDate": systemstatistikenEndDate
				},
				success(data){
					var chartData = JSON.parse(data);

					// Chart: Anzahl hinzugefügte Benutzer
					if(chart === "useractive"){
						// Stunde
						if(id == 0){
							if(lineChartUserActiveState != 0){
								lineChartUserActive.resetZoom();
								chartData["usercreatedateHourraw"] = chartData["usercreatedateHourraw"].replace(/x/g, '"x"');
								chartData["usercreatedateHourraw"] = chartData["usercreatedateHourraw"].replace(/y/g, '"y"');
								chartData["usercreatedateHourraw"] = chartData["usercreatedateHourraw"].replace(/'/g, '"');
								chartData["usercreatedateHourraw"] = JSON.parse(chartData["usercreatedateHourraw"]);
									
								lineChartUserActive.config.data.datasets[0].data = chartData["usercreatedateHourraw"];
								lineChartUserActive.config.data.datasets[0].backgroundColor = "rgba(255,0,0,0.5)";
								lineChartUserActive.config.data.datasets[0].label = "Benutzer/Stunde";
								lineChartUserActive.update();
								lineChartUserActiveState = 0;
							}
						}
						// Tag
						else if(id == 1){
							if(lineChartUserActiveState != 1){
								lineChartUserActive.resetZoom();
								chartData["usercreatedateDayraw"] = chartData["usercreatedateDayraw"].replace(/x/g, '"x"');
								chartData["usercreatedateDayraw"] = chartData["usercreatedateDayraw"].replace(/y/g, '"y"');
								chartData["usercreatedateDayraw"] = chartData["usercreatedateDayraw"].replace(/'/g, '"');
								chartData["usercreatedateDayraw"] = JSON.parse(chartData["usercreatedateDayraw"]);
	
								lineChartUserActive.config.data.datasets[0].data = chartData["usercreatedateDayraw"];
								lineChartUserActive.config.data.datasets[0].backgroundColor = "rgba(0,255,0,0.5)";
								lineChartUserActive.config.data.datasets[0].label = "Benutzer/Tag";
								lineChartUserActive.update();
								lineChartUserActiveState = 1;
							}
						}
						// Monat
						else if(id == 2){
							if(lineChartUserActiveState != 2){
								lineChartUserActive.resetZoom();
								chartData["usercreatedateMounthraw"] = chartData["usercreatedateMounthraw"].replace(/x/g, '"x"');
								chartData["usercreatedateMounthraw"] = chartData["usercreatedateMounthraw"].replace(/y/g, '"y"');
								chartData["usercreatedateMounthraw"] = chartData["usercreatedateMounthraw"].replace(/'/g, '"');
								chartData["usercreatedateMounthraw"] = JSON.parse(chartData["usercreatedateMounthraw"]);
	
								lineChartUserActive.config.data.datasets[0].data = chartData["usercreatedateMounthraw"];
								lineChartUserActive.config.data.datasets[0].backgroundColor = "rgba(0,0,255,0.5)";
								lineChartUserActive.config.data.datasets[0].label = "Benutzer/Monat";
								lineChartUserActive.update();
								lineChartUserActiveState = 2;
							}
						}
					}
					// Chart: Zeitpunkt des letzten Logins
					else if(chart === "userlogin"){
						// Stunde
						if(id == 0){
							if(lineChartUserLoginState != 0){
								lineChartUserLogin.resetZoom();
								chartData["lastloginHourraw"] = chartData["lastloginHourraw"].replace(/x/g, '"x"');
								chartData["lastloginHourraw"] = chartData["lastloginHourraw"].replace(/y/g, '"y"');
								chartData["lastloginHourraw"] = chartData["lastloginHourraw"].replace(/'/g, '"');
								chartData["lastloginHourraw"] = JSON.parse(chartData["lastloginHourraw"]);
	
								lineChartUserLogin.config.data.datasets[0].data = chartData["lastloginHourraw"];
								lineChartUserLogin.config.data.datasets[0].backgroundColor = "rgba(255,0,0,0.5)";
								lineChartUserLogin.config.data.datasets[0].label = "Benutzer/Stunde";
								lineChartUserLogin.update();
								lineChartUserLoginState = 0;
							}
						}
						// Tag
						else if(id == 1){
							if(lineChartUserLoginState != 1){
								lineChartUserLogin.resetZoom();
								chartData["lastloginDayraw"] = chartData["lastloginDayraw"].replace(/x/g, '"x"');
								chartData["lastloginDayraw"] = chartData["lastloginDayraw"].replace(/y/g, '"y"');
								chartData["lastloginDayraw"] = chartData["lastloginDayraw"].replace(/'/g, '"');
								chartData["lastloginDayraw"] = JSON.parse(chartData["lastloginDayraw"]);
	
								lineChartUserLogin.config.data.datasets[0].data = chartData["lastloginDayraw"];
								lineChartUserLogin.config.data.datasets[0].backgroundColor = "rgba(0,255,0,0.5)";
								lineChartUserLogin.config.data.datasets[0].label = "Benutzer/Tag";
								lineChartUserLogin.update();
								lineChartUserLoginState = 1;
							}
						}
						// Monat
						else if(id == 2){
							if(lineChartUserLoginState != 2){
								lineChartUserLogin.resetZoom();
								chartData["lastloginMounthraw"] = chartData["lastloginMounthraw"].replace(/x/g, '"x"');
								chartData["lastloginMounthraw"] = chartData["lastloginMounthraw"].replace(/y/g, '"y"');
								chartData["lastloginMounthraw"] = chartData["lastloginMounthraw"].replace(/'/g, '"');
								chartData["lastloginMounthraw"] = JSON.parse(chartData["lastloginMounthraw"]);
	
								lineChartUserLogin.config.data.datasets[0].data = chartData["lastloginMounthraw"];
								lineChartUserLogin.config.data.datasets[0].backgroundColor = "rgba(0,0,255,0.5)";
								lineChartUserLogin.config.data.datasets[0].label = "Benutzer/Monat";
								lineChartUserLogin.update();
								lineChartUserLoginState = 2;
							}
						}
			
					}
					// Chart: Anzahl Benutzereingaben
					else if(chart === "userinput"){
						// Stunde
						if(id == 0){
							if(lineChartUserInputState != 0){
								lineChartUserInput.resetZoom();
								chartData["inputHourraw"] = chartData["inputHourraw"].replace(/x/g, '"x"');
								chartData["inputHourraw"] = chartData["inputHourraw"].replace(/y/g, '"y"');
								chartData["inputHourraw"] = chartData["inputHourraw"].replace(/'/g, '"');
								chartData["inputHourraw"] = JSON.parse(chartData["inputHourraw"]);
	
								lineChartUserInput.config.data.datasets[0].data = chartData["inputHourraw"];
								lineChartUserInput.config.data.datasets[0].backgroundColor = "rgba(255,0,0,0.5)";
								lineChartUserInput.config.data.datasets[0].label = "Eingaben/Stunde";
								lineChartUserInput.update();
								lineChartUserInputState = 0;
							}
						}
						// Tag
						else if(id == 1){
							if(lineChartUserInputState != 1){
								lineChartUserInput.resetZoom();
								chartData["inputDayraw"] = chartData["inputDayraw"].replace(/x/g, '"x"');
								chartData["inputDayraw"] = chartData["inputDayraw"].replace(/y/g, '"y"');
								chartData["inputDayraw"] = chartData["inputDayraw"].replace(/'/g, '"');
								chartData["inputDayraw"] = JSON.parse(chartData["inputDayraw"]);
	
								lineChartUserInput.config.data.datasets[0].data = chartData["inputDayraw"];
								lineChartUserInput.config.data.datasets[0].backgroundColor = "rgba(0,255,0,0.5)";
								lineChartUserInput.config.data.datasets[0].label = "Eingaben/Tag";
								lineChartUserInput.update();
								lineChartUserInputState = 1;
							}
						}
						// Monat
						else if(id == 2){
							if(lineChartUserInputState != 2){
								lineChartUserInput.resetZoom();
								chartData["inputMounthraw"] = chartData["inputMounthraw"].replace(/x/g, '"x"');
								chartData["inputMounthraw"] = chartData["inputMounthraw"].replace(/y/g, '"y"');
								chartData["inputMounthraw"] = chartData["inputMounthraw"].replace(/'/g, '"');
								chartData["inputMounthraw"] = JSON.parse(chartData["inputMounthraw"]);
	
								lineChartUserInput.config.data.datasets[0].data = chartData["inputMounthraw"];
								lineChartUserInput.config.data.datasets[0].backgroundColor = "rgba(0,0,255,0.5)";
								lineChartUserInput.config.data.datasets[0].label = "Eingaben/Monat";
								lineChartUserInput.update();
								lineChartUserInputState = 2;
							}
						}
					}
					// Chart: Anzahl abgeschlossene Lektionen
					else if(chart === "userlektionen"){
						if(id == 0){
							if(lineChartLektionState != 0){
								lineChartLektion.resetZoom();
								chartData["lektionHourraw"] = chartData["lektionHourraw"].replace(/x/g, '"x"');
								chartData["lektionHourraw"] = chartData["lektionHourraw"].replace(/y/g, '"y"');
								chartData["lektionHourraw"] = chartData["lektionHourraw"].replace(/'/g, '"');
								chartData["lektionHourraw"] = JSON.parse(chartData["lektionHourraw"]);
	
								lineChartLektion.config.data.datasets[0].data = chartData["lektionHourraw"];
								lineChartLektion.config.data.datasets[0].backgroundColor = "rgba(255,0,0,0.5)";
								lineChartLektion.config.data.datasets[0].label = "Lektionen/Stunde";
								lineChartLektion.update();
								lineChartLektionState = 0;
							}
						}
						else if(id == 1){
							if(lineChartLektionState != 1){
								lineChartLektion.resetZoom();
								chartData["lektionDayraw"] = chartData["lektionDayraw"].replace(/x/g, '"x"');
								chartData["lektionDayraw"] = chartData["lektionDayraw"].replace(/y/g, '"y"');
								chartData["lektionDayraw"] = chartData["lektionDayraw"].replace(/'/g, '"');
								chartData["lektionDayraw"] = JSON.parse(chartData["lektionDayraw"]);
	
								lineChartLektion.config.data.datasets[0].data = chartData["lektionDayraw"];
								lineChartLektion.config.data.datasets[0].backgroundColor = "rgba(0,255,0,0.5)";
								lineChartLektion.config.data.datasets[0].label = "Lektionen/Tag";
								lineChartLektion.update();
								lineChartLektionState = 1;
							}
						}
						else if(id == 2){
							if(lineChartLektionState != 2){
								lineChartLektion.resetZoom();
								chartData["lektionMounthraw"] = chartData["lektionMounthraw"].replace(/x/g, '"x"');
								chartData["lektionMounthraw"] = chartData["lektionMounthraw"].replace(/y/g, '"y"');
								chartData["lektionMounthraw"] = chartData["lektionMounthraw"].replace(/'/g, '"');
								chartData["lektionMounthraw"] = JSON.parse(chartData["lektionMounthraw"]);
	
								lineChartLektion.config.data.datasets[0].data = chartData["lektionMounthraw"];
								lineChartLektion.config.data.datasets[0].backgroundColor = "rgba(0,0,255,0.5)";
								lineChartLektion.config.data.datasets[0].label = "Lektionen/Monat";
								lineChartLektion.update();
								lineChartLektionState = 2;
							}
						}
			
					}
					userFeedback(button, 'success');
					setTimeout(()=>{
						userFeedback(button, 'text');
					},1500);					
				}
			});


		}

		// Zurücksetzen Systemstatistiken		
		jQuery(document).on('click', '#systemstatistics-reset-user-log-button', function(e) {
				userFeedback('systemstatistics-reset-user-log-button', 'loading');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "systemstatistiken_reset"
					},
					success: function (data) {
						console.log(data);
						if(data == "1") {
							userFeedback('systemstatistics-reset-user-log-button', 'success');
							setTimeout(() => {
								location.reload();
							}, 1500);
						} else {
							userFeedback('systemstatistics-reset-user-log-button', 'error');
							setTimeout(() => {
								userFeedback('systemstatistics-reset-user-log-button', 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback('systemstatistics-reset-user-log-button', 'error');
						setTimeout(() => {
							userFeedback('systemstatistics-reset-user-log-button', 'text');
						}, 1500);
						console.error(errorThrown);
					}
				});
			//}
		});

		// Erstellen der Login-History des aktuellen Tages Systemstatistiken
		jQuery(document).on('click','#systemstatistikenloginhistorybutton',function(e){
			userFeedback('systemstatistikenloginhistorybutton','loading');
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data:{
					"action": "systemstatistiken_generateloginhistory"
				},
				success: function(data){
					data = data.replace(/user_login/g, '"user_login"');
					data = data.replace(/user_id/g, '"user_id"');
					data = data.replace(/ldate/g, '"ldate"');
					data = data.replace(/'/g, '"');
					var loginHistoryData = JSON.parse(data);
					if(loginHistoryData.length != 0){
						for(var i=0;i<loginHistoryData.length;i++){
							var node = document.createElement("p");
							var loginHistoryText = loginHistoryData[i].user_login+" (ID: "+loginHistoryData[i].user_id+") hat sich angemeldet. - "+loginHistoryData[i].ldate;

							var textnode = document.createTextNode(loginHistoryText);
							node.appendChild(textnode);
							document.getElementById("systemstatistics-login-history").appendChild(node);
							}
						}
						else{
							var node = document.createElement("p");
							var loginHistoryText = "Bisher keine Aktivität verfügbar";
							var textnode = document.createTextNode(loginHistoryText);
							node.appendChild(textnode);
							document.getElementById("systemstatistics-login-history").appendChild(node);
						}
					document.getElementById('systemstatistikenloginhistorybutton').hidden = true;
					document.getElementById('systemstatistikenloginhistorybuttonhide').hidden = false;
					userFeedback('systemstatistikenloginhistorybuttonhide','success');
					setTimeout(()=>{
						userFeedback('systemstatistikenloginhistorybuttonhide','text');
					},1500);
					userFeedback('systemstatistikenloginhistorybutton','text');
					
				},
				error: function(errorThrown){
					userFeedback('systemstatistikenloginhistorybutton','error');
					setTimeout(()=>{
						userFeedback('systemstatistikenloginhistorybutton', 'text');
					}, 1500);
					console.error(errorThrown);
				}
			})
		});

		jQuery(document).on('click', '#systemstatistikenloginhistorybuttonhide', function(e){
			userFeedback('systemstatistikenloginhistorybuttonhide','loading');
			document.getElementById('systemstatistikenloginhistorybutton').hidden = false;
			document.getElementById('systemstatistikenloginhistorybuttonhide').hidden = true;
			document.getElementById('systemstatistics-login-history').innerHTML = "";
		})

		// Hilfsfunktion, die für die Darstellung von Stunden/Minuten/Sekunden bei der Login-History eine 0 bei Werte zwischen 0 und 9 voranstellt
		function systemstatisticsLoginHistoryAddZeros(t){
			if(t < 10){
				t = "0" + t;
			}
			return t;
		}

		// Form Auswahl Wertebereich bar-Chart countpage
		jQuery(document).on('click', '#systemstatistiken-countpage-submit', function(e){
			e.preventDefault();
			systemstatisticsSetRangeOfValuesCountpage();
		});

		jQuery(document).on('submit','#systemstatistiken-countpage-form',function(e){
			e.preventDefault();
			systemstatisticsSetRangeOfValuesCountpage();

		});
		// Form Auswahl Wertebereich bar-Chart sumpage
		jQuery(document).on('click', '#systemstatistiken-sumpage-submit', function(e){
			e.preventDefault();
			systemstatisticsSetRangeOfValuesSumpage();
		});

		jQuery(document).on('submit','#systemstatistiken-sumpage-form',function(e){
			e.preventDefault();
			systemstatisticsSetRangeOfValuesSumpage();

		});

		function systemstatisticsSetRangeOfValuesCountpage(){
			userFeedback('systemstatistiken-countpage-submit', 'loading');
			var startValueCountpage = jQuery('#systemstatistiken-countpage-startvalue').val();
			var endValueCountpage = jQuery('#systemstatistiken-countpage-endvalue').val();
			var systemstatistikenStartDate = jQuery("#systemstatistikenStartDate").val();
			var systemstatistikenEndDate = jQuery("#systemstatistikenEndDate").val();
			var data = new FormData();
			data = {
				"action":"systemstatistiken_setrangeofvaluescountpage",
				"startValueCountpage": startValueCountpage,
				"endValueCountpage": endValueCountpage,
				"systemstatistikenStartDate": systemstatistikenStartDate,
				"systemstatistikenEndDate": systemstatistikenEndDate
			};
			
			if(typeof userid !== 'undefined'){
				data['userid'] = userid;
			}
			else{
				data['userid'] = null;
			}
			jQuery.ajax({
				type: 'GET',
				url: ajaxurl,
				data:data,
				success: function(data){
					var chartData = JSON.parse(data);
					barChartUserActiveCountpage.config.data.datasets[0].data = chartData.values;
					barChartUserActiveCountpage.config.data.labels = chartData.labels;
					barChartUserActiveCountpage.update();
					userFeedback('systemstatistiken-countpage-submit', 'success');
					setTimeout(() =>{
						userFeedback('systemstatistiken-countpage-submit', 'text');
					}, 1500);
				},
				error: function (errorThrown){
					userFeedback('systemstatistiken-countpage-submit', 'error');
					setTimeout(() =>{
						userFeedback('systemstatistiken-countpage-submit', 'text');
					}, 1500);
					console.error(errorThrown);
				}

			});
		}

		function systemstatisticsSetRangeOfValuesSumpage(){
			userFeedback('systemstatistiken-sumpage-submit', 'loading');
			var startValueSumpage = jQuery('#userActiveSumpageStartvalue').val();
			var endValueSumpage = jQuery('#userActiveSumpageEndvalue').val();
			var systemstatistikenStartDate = jQuery("#systemstatistikenStartDate").val();
			var systemstatistikenEndDate = jQuery("#systemstatistikenEndDate").val();
			var data = new FormData();
			data = {
				"action":"systemstatistiken_setrangeofvaluessumpage",
				"startValueSumpage": startValueSumpage,
				"endValueSumpage": endValueSumpage,
				"systemstatistikenStartDate": systemstatistikenStartDate,
				"systemstatistikenEndDate": systemstatistikenEndDate,
			};

			if(typeof userid !== 'undefined'){
				data['userid'] = userid;
			}
			else{
				data['userid'] = null;
			}

			jQuery.ajax({
				type: 'GET',
				url: ajaxurl,
				data: data,
				success: function(data){
					var chartData = JSON.parse(data);
					barChartUserActiveSumpage.config.data.datasets[0].data = chartData.values;
					barChartUserActiveSumpage.config.data.labels = chartData.labels;
					barChartUserActiveSumpage.update();
					userFeedback('systemstatistiken-sumpage-submit', 'success');
					setTimeout(() =>{
						userFeedback('systemstatistiken-sumpage-submit', 'text');
					}, 1500);
				},
				error: function (errorThrown){
					userFeedback('systemstatistiken-sumpage-submit', 'error');
					setTimeout(() =>{
						userFeedback('systemstatistiken-sumpage-submit', 'text');
					}, 1500);
					console.error(errorThrown);
				}

			});
		}

		// Form Auswahl Zeitraum Systemstatistiken
		jQuery(document).on('click','#systemstatistiken-timeperiod-submit',function(e){
			e.preventDefault();
			systemstatisticsSetInterval();

		});

		jQuery(document).on('submit','#systemstatistiken-timeperiod-form',function(e){
			e.preventDefault();
			systemstatisticsSetInterval();

		});

		function systemstatisticsSetInterval(){
			userFeedback('systemstatistiken-timeperiod-submit', 'loading');
			var systemstatistikenStartDate = jQuery("#systemstatistikenStartDate").val();
			var systemstatistikenEndDate = jQuery("#systemstatistikenEndDate").val();

			jQuery.ajax({
				type: 'GET',
				url: ajaxurl,
				data:{
					"action": "systemstatistiken_settimeperiod",
					"systemstatistikenStartDate": systemstatistikenStartDate,
					"systemstatistikenEndDate": systemstatistikenEndDate
				},
				success: function (data){
					// Parsen der Daten
					var chartData = JSON.parse(data);
					// Sonderbehandlung für die line-Charts, da diese noch als String und nicht im geeigneten Format vorliegen
					// Chart: lineChartUserActive
					// Hour
					chartData["usercreatedateHourraw"] = chartData["usercreatedateHourraw"].replace(/x/g, '"x"');
					chartData["usercreatedateHourraw"] = chartData["usercreatedateHourraw"].replace(/y/g, '"y"');
					chartData["usercreatedateHourraw"] = chartData["usercreatedateHourraw"].replace(/'/g, '"');
					chartData["usercreatedateHourraw"] = JSON.parse(chartData["usercreatedateHourraw"]);
					// Day
					chartData["usercreatedateDayraw"] = chartData["usercreatedateDayraw"].replace(/x/g, '"x"');
					chartData["usercreatedateDayraw"] = chartData["usercreatedateDayraw"].replace(/y/g, '"y"');
					chartData["usercreatedateDayraw"] = chartData["usercreatedateDayraw"].replace(/'/g, '"');
					chartData["usercreatedateDayraw"] = JSON.parse(chartData["usercreatedateDayraw"]);
					// Month
					chartData["usercreatedateMounthraw"] = chartData["usercreatedateMounthraw"].replace(/x/g, '"x"');
					chartData["usercreatedateMounthraw"] = chartData["usercreatedateMounthraw"].replace(/y/g, '"y"');
					chartData["usercreatedateMounthraw"] = chartData["usercreatedateMounthraw"].replace(/'/g, '"');
					chartData["usercreatedateMounthraw"] = JSON.parse(chartData["usercreatedateMounthraw"]);
							
					// Chart: lineChartUserLogin
					// Hour
					chartData["lastloginHourraw"] = chartData["lastloginHourraw"].replace(/x/g, '"x"');
					chartData["lastloginHourraw"] = chartData["lastloginHourraw"].replace(/y/g, '"y"');
					chartData["lastloginHourraw"] = chartData["lastloginHourraw"].replace(/'/g, '"');
					chartData["lastloginHourraw"] = JSON.parse(chartData["lastloginHourraw"]);
					// Day
					chartData["lastloginDayraw"] = chartData["lastloginDayraw"].replace(/x/g, '"x"');
					chartData["lastloginDayraw"] = chartData["lastloginDayraw"].replace(/y/g, '"y"');
					chartData["lastloginDayraw"] = chartData["lastloginDayraw"].replace(/'/g, '"');
					chartData["lastloginDayraw"] = JSON.parse(chartData["lastloginDayraw"]);
					// Month
					chartData["lastloginMounthraw"] = chartData["lastloginMounthraw"].replace(/x/g, '"x"');
					chartData["lastloginMounthraw"] = chartData["lastloginMounthraw"].replace(/y/g, '"y"');
					chartData["lastloginMounthraw"] = chartData["lastloginMounthraw"].replace(/'/g, '"');
					chartData["lastloginMounthraw"] = JSON.parse(chartData["lastloginMounthraw"]);
							
					// Chart: lineChartUserInput
					// Hour
					chartData["inputHourraw"] = chartData["inputHourraw"].replace(/x/g, '"x"');
					chartData["inputHourraw"] = chartData["inputHourraw"].replace(/y/g, '"y"');
					chartData["inputHourraw"] = chartData["inputHourraw"].replace(/'/g, '"');
					chartData["inputHourraw"] = JSON.parse(chartData["inputHourraw"]);
					// Day
					chartData["inputDayraw"] = chartData["inputDayraw"].replace(/x/g, '"x"');
					chartData["inputDayraw"] = chartData["inputDayraw"].replace(/y/g, '"y"');
					chartData["inputDayraw"] = chartData["inputDayraw"].replace(/'/g, '"');
					chartData["inputDayraw"] = JSON.parse(chartData["inputDayraw"]);
					// Month
					chartData["inputMounthraw"] = chartData["inputMounthraw"].replace(/x/g, '"x"');
					chartData["inputMounthraw"] = chartData["inputMounthraw"].replace(/y/g, '"y"');
					chartData["inputMounthraw"] = chartData["inputMounthraw"].replace(/'/g, '"');
					chartData["inputMounthraw"] = JSON.parse(chartData["inputMounthraw"]);
						
					// Chart: lineChartLektion
					// Hour
					chartData["lektionHourraw"] = chartData["lektionHourraw"].replace(/x/g, '"x"');
					chartData["lektionHourraw"] = chartData["lektionHourraw"].replace(/y/g, '"y"');
					chartData["lektionHourraw"] = chartData["lektionHourraw"].replace(/'/g, '"');
					chartData["lektionHourraw"] = JSON.parse(chartData["lektionHourraw"]);
					// Day
					chartData["lektionDayraw"] = chartData["lektionDayraw"].replace(/x/g, '"x"');
					chartData["lektionDayraw"] = chartData["lektionDayraw"].replace(/y/g, '"y"');
					chartData["lektionDayraw"] = chartData["lektionDayraw"].replace(/'/g, '"');
					chartData["lektionDayraw"] = JSON.parse(chartData["lektionDayraw"]);
					// Month
					chartData["lektionMounthraw"] = chartData["lektionMounthraw"].replace(/x/g, '"x"');
					chartData["lektionMounthraw"] = chartData["lektionMounthraw"].replace(/y/g, '"y"');
					chartData["lektionMounthraw"] = chartData["lektionMounthraw"].replace(/'/g, '"');
					chartData["lektionMounthraw"] = JSON.parse(chartData["lektionMounthraw"]);
							
					// line-Charts
					// line-Chart: systemstatistics-user-active.html ("Anzahl hinzugefügte Benutzer")
					if(lineChartUserActiveState == 0){
						lineChartUserActive.config.data.datasets[0].data = chartData["usercreatedateHourraw"];
					}
					else if(lineChartUserActiveState == 1){
						lineChartUserActive.config.data.datasets[0].data = chartData["usercreatedateDayraw"];
					}
					else if(lineChartUserActiveState == 2){
						lineChartUserActive.config.data.datasets[0].data = chartData["usercreatedateMounthraw"];
					}
					lineChartUserActive.update();
					// line-Chart: systemstatistics-user-login.html ("Zeitpunkt des letzten Logins")
					if(lineChartUserLoginState == 0){
						lineChartUserLogin.config.data.datasets[0].data = chartData["lastloginHourraw"];
					}
					else if(lineChartUserLoginState == 1){
						lineChartUserLogin.config.data.datasets[0].data = chartData["lastloginDayraw"];
					}
					else if(lineChartUserLoginState == 2){
						lineChartUserLogin.config.data.datasets[0].data = chartData["lastloginMounthraw"];
					}
					lineChartUserLogin.update();
					// line-Chart: systemstatistics-user-input.html ("Anzahl Benutzereingaben")
					if(lineChartUserInputState == 0){
						lineChartUserInput.config.data.datasets[0].data = chartData["inputHourraw"];
					}
					else if(lineChartUserInputState == 1){
						lineChartUserInput.config.data.datasets[0].data = chartData["inputDayraw"];
					}
					else if(lineChartUserInputState == 2){
						lineChartUserInput.config.data.datasets[0].data = chartData["inputMounthraw"];
					}
					lineChartUserInput.update();
					// line-Chart: systemstatistics-user-lektionen.html ("Anzahl abgeschlossene Lektionen")
					if(lineChartLektionState == 0){
						lineChartLektion.config.data.datasets[0].data = chartData["lektionHourraw"];
					}
					else if(lineChartLektionState == 1){
						lineChartLektion.config.data.datasets[0].data = chartData["lektionDayraw"];
					}
					else if(lineChartLektionState == 2){
						lineChartLektion.config.data.datasets[0].data = chartData["lektionMounthraw"];
					}
					lineChartLektion.update();
							
					// bar-Charts
					// bar-Chart: systemstatistics-user-active-counthour.html ("Benutzeraktivität (gemessen in Seitenaufrufe/Uhrzeit)")
					barUserActiveCounthour.config.data.datasets[0].data = chartData["countHourActive"].values;
					barUserActiveCounthour.config.data.labels = chartData["countHourActive"].labels;
					barUserActiveCounthour.update();

					/*
					// bar-Chart: systemstatistics-user-active-sumhour.html ("Benutzeraktivität (gemessen in Sekunden/Uhrzeit)")
					// AKTUELL AUSGEBLENDET
					barUserActiveSumhour.config.data.datasets[0].data = chartData["sumHourActive"].values;
					barUserActiveSumhour.config.data.labels = chartData["sumHourActive"].labels;
					barUserActiveSumhour.update();
					*/

					// bar-Chart: systemstatistics-user-active-countweekday.html ("Benutzeraktivität (gemessen in Seitenaufrufe/Wochentag)")
					barUserActiveCountWeekday.config.data.datasets[0].data = chartData["countWeekdayActive"].values;
					barUserActiveCountWeekday.config.data.labels = chartData["countWeekdayActive"].labels;
					barUserActiveCountWeekday.update();

					/*
					// bar-Chart: systemstatistics-user-active-sumweekday.html ("Benutzeraktivität (gemessen in Sekunden/Wochentag)")
					// AKTUELL AUSGEBLENDET
					barUserActiveSumweekday.config.data.datasets[0].data = chartData["sumWeekdayActive"].values;
					barUserActiveSumweekday.config.data.labels = chartData["sumWeekdayActive"].labels;
					barUserActiveSumweekday.update();
					*/

					// bar-Chart: systemstatistics-user-active-countpage.html ("Benutzeraktivität (gemessen in Seitenaufrufe/Seite)")
					barChartUserActiveCountpage.config.data.datasets[0].data = chartData["countPageView"].values;
					barChartUserActiveCountpage.config.data.labels = chartData["countPageView"].labels;
					barChartUserActiveCountpage.update();
					// bar-Chart: systemstatistics-user-active-sumpage.html ("Benutzeraktivität (gemessen in Sekunden/Seite)")
					barChartUserActiveSumpage.config.data.datasets[0].data = chartData["sumPageView"].values;
					barChartUserActiveSumpage.config.data.labels = chartData["sumPageView"].labels;
					barChartUserActiveSumpage.update();

					// pie-Charts
					// pie-Chart: systemstatistics-user-active-countbrowser.html ("Browserverteilung (gemessen in Seitenaufrufen)")
					pieUserActiveCountBrowser.config.data.datasets[0].data = chartData["countBrowser"].values;
					pieUserActiveCountBrowser.config.data.labels = chartData["countBrowser"].labels;
					pieUserActiveCountBrowser.update();
					// pie-Chart: systemstatistics-user-active-countdevicetype.html ("Verteilung der Gerätetypen (gemessen in Seitenaufrufen)")
					pieUserActiveCountDeviceType.config.data.datasets[0].data = chartData["countDeviceType"].values;
					pieUserActiveCountDeviceType.config.data.labels = chartData["countDeviceType"].labels;
					pieUserActiveCountDeviceType.update();

					// horizontalBar-Charts
					// horizontalBar-Chart: systemstatistics-user-training-count.html ("Anzahl abgeschlossene Trainings")
					horizontalBarUserTrainingCount.config.data.datasets[0].data = chartData["trainingFinUserCountValues"];
					horizontalBarUserTrainingCount.config.data.labels = chartData["trainingFinUserCountLbl"];
					horizontalBarUserTrainingCount.update();
							
					userFeedback('systemstatistiken-timeperiod-submit', 'success');
					setTimeout(() =>{
						userFeedback('systemstatistiken-timeperiod-submit', 'text');
					}, 1500);
				},
				error: function (errorThrown){
					userFeedback('systemstatistiken-timeperiod-submit', 'error');
					setTimeout(() =>{
						userFeedback('systemstatistiken-timeperiod-submit', 'text');
					}, 1500);
					console.error(errorThrown);
				}
			});
		}
		// Export Charts als Excel-Datei/CSV-Datei
		// line-Charts
		jQuery(document).on('click','#systemstatistiken-excelexport-user-active', function(e){
			var filename = "Anzahl_hinzugefuegte_Benutzer.csv";
			var dataPoints = [];
			var dataLabels = [];

			for(var i=0; i<lineChartUserActive.config.data.datasets[0].data.length;i++){
				dataPoints.push(lineChartUserActive.config.data.datasets[0].data[i].x);
				dataLabels.push(lineChartUserActive.config.data.datasets[0].data[i].y);
			}
			var keys = ["Zeit","Anzahl_Benutzer"];
			systemstatisticsExportChartToExcel(filename, dataPoints, dataLabels, keys);
		});

		jQuery(document).on('click','#systemstatistiken-excelexport-user-login', function(e){
			var filename = "Zeitpunkt_letzter_Login.csv";
			var dataPoints = [];
			var dataLabels = [];

			for(var i=0; i<lineChartUserLogin.config.data.datasets[0].data.length;i++){
				dataPoints.push(lineChartUserLogin.config.data.datasets[0].data[i].x);
				dataLabels.push(lineChartUserLogin.config.data.datasets[0].data[i].y);
			}
			var keys = ["Zeit","Anzahl_Benutzer"];
			systemstatisticsExportChartToExcel(filename, dataPoints, dataLabels, keys);
		});

		jQuery(document).on('click','#systemstatistiken-excelexport-user-input', function(e){
			var filename = "Anzahl_Benutzereingaben.csv";
			var dataPoints = [];
			var dataLabels = [];

			for(var i=0; i<lineChartUserInput.config.data.datasets[0].data.length;i++){
				dataPoints.push(lineChartUserInput.config.data.datasets[0].data[i].x);
				dataLabels.push(lineChartUserInput.config.data.datasets[0].data[i].y);
			}
			var keys = ["Zeit","Anzahl_Eingaben"];
			systemstatisticsExportChartToExcel(filename, dataPoints, dataLabels, keys);
		});

		jQuery(document).on('click','#systemstatistiken-excelexport-user-lektionen', function(e){
			var filename = "Anzahl_abgeschlossene_Lektionen.csv";
			var dataPoints = [];
			var dataLabels = [];

			for(var i=0; i<lineChartLektion.config.data.datasets[0].data.length;i++){
				dataPoints.push(lineChartLektion.config.data.datasets[0].data[i].x);
				dataLabels.push(lineChartLektion.config.data.datasets[0].data[i].y);
			}
			var keys = ["Zeit","Anzahl_Lektionen"];
			systemstatisticsExportChartToExcel(filename, dataPoints, dataLabels, keys);
		});

		// bar-Charts
		jQuery(document).on('click','#systemstatistiken-excelexport-counthour', function(e){
			var filename = "Benutzeraktivitaet_Seitenaufrufe_Uhrzeit.csv";
			var dataPoints = barUserActiveCounthour.config.data.datasets[0].data;
			var dataLabels = barUserActiveCounthour.config.data.labels;
			var keys = ["Seitenaufrufe","Uhrzeit"];
			systemstatisticsExportChartToExcel(filename, dataPoints, dataLabels, keys);
		});

		jQuery(document).on('click','#systemstatistiken-excelexport-sumhour', function(e){
			var filename = "Benutzeraktivitaet_Sekunden_Uhrzeit.csv";
			var dataPoints = barUserActiveSumhour.config.data.datasets[0].data;
			var dataLabels = barUserActiveSumhour.config.data.labels;
			var keys = ["Sekunden","Uhrzeit"];
			systemstatisticsExportChartToExcel(filename, dataPoints, dataLabels, keys);
		});

		jQuery(document).on('click','#systemstatistiken-excelexport-countweekday', function(e){
			var weekdays = ["Montag","Dienstag","Mittwoch", "Donnerstag", "Freitag", "Samstag","Sonntag"];
			var filename = "Benutzeraktivitaet_Seitenaufrufe_Wochentag.csv";
			var dataPoints = barUserActiveCountWeekday.config.data.datasets[0].data;
			var dataLabels =[] ;
			// Die Wochentage werden innerhalb der Daten als numerische Werte gespeichert (Montag = 0, etc.), 
			// hier werden diese für die CSV-Datei in Strings mit den Namen der Tage umgewandelt
			for(var i=0;i<barUserActiveCountWeekday.config.data.labels.length;i++){
				dataLabels[i] = weekdays[parseInt(barUserActiveCountWeekday.config.data.labels[i])];
			}
			var keys = ["Seitenaufrufe","Wochentag"];
			systemstatisticsExportChartToExcel(filename, dataPoints, dataLabels, keys);
		});

		jQuery(document).on('click','#systemstatistiken-excelexport-sumweekday', function(e){
			var weekdays = ["Montag","Dienstag","Mittwoch", "Donnerstag", "Freitag", "Samstag","Sonntag"];
			var filename = "Benutzeraktivitaet_Sekunden_Wochentag.csv";
			var dataPoints = barUserActiveSumweekday.config.data.datasets[0].data;
			var dataLabels = [];
			// Die Wochentage werden innerhalb der Daten als numerische Werte gespeichert (Montag = 0, etc.), 
			// hier werden diese für die CSV-Datei in Strings mit den Namen der Tage umgewandelt
			for(var i=0;i<barUserActiveSumweekday.config.data.labels.length;i++){
				dataLabels[i] = weekdays[parseInt(barUserActiveSumweekday.config.data.labels[i])];
			}
			var keys = ["Sekunden","Wochentag"];
			systemstatisticsExportChartToExcel(filename, dataPoints, dataLabels, keys);
		});

		jQuery(document).on('click','#systemstatistiken-excelexport-countpage', function(e){
			var filename = "Benutzeraktivitaet_Seitenaufrufe_Seite.csv";
			var dataPoints = barChartUserActiveCountpage.config.data.datasets[0].data;
			var dataLabels = barChartUserActiveCountpage.config.data.labels;
			var keys = ["Seitenaufrufe","Seite"];
			systemstatisticsExportChartToExcel(filename, dataPoints, dataLabels, keys);
		});

		jQuery(document).on('click','#systemstatistiken-excelexport-sumpage', function(e){
			var filename = "Benutzeraktivitaet_Sekunden_Seite.csv";
			var dataPoints = barChartUserActiveSumpage.config.data.datasets[0].data;
			var dataLabels = barChartUserActiveSumpage.config.data.labels;
			var keys = ["Sekunden","Seite"];
			systemstatisticsExportChartToExcel(filename, dataPoints, dataLabels, keys);
		});

		// pie-Charts
		jQuery(document).on('click','#systemstatistiken-excelexport-countbrowser', function(e){
			var filename = "Verteilung_Browser.csv";
			var dataPoints = pieUserActiveCountBrowser.config.data.datasets[0].data;
			var dataLabels = pieUserActiveCountBrowser.config.data.labels;
			var keys = ["Anzahl","Browser"];
			systemstatisticsExportChartToExcel(filename, dataPoints, dataLabels, keys);
		});

		jQuery(document).on('click','#systemstatistiken-excelexport-countdevicetype', function(e){
			var filename = "Verteilung_Geraetetypen.csv";
			var dataPoints = pieUserActiveCountDeviceType.config.data.datasets[0].data;
			var dataLabels = pieUserActiveCountDeviceType.config.data.labels;
			var keys = ["Anzahl","Geraetetyp"];
			systemstatisticsExportChartToExcel(filename, dataPoints, dataLabels, keys);
		});

		// horizontalbar-Charts
		jQuery(document).on('click','#systemstatistiken-excelexport-training-count', function(e){
			var filename = "Anzahl_abgeschlossene_Trainings.csv";
			var dataPoints = horizontalBarUserTrainingCount.config.data.datasets[0].data;
			var dataLabels = horizontalBarUserTrainingCount.config.data.labels;
			var keys = ["Anzahl","Training"];
			systemstatisticsExportChartToExcel(filename, dataPoints, dataLabels, keys);
		});

		function systemstatisticsExportChartToExcel(filename, dataPoints, dataLabels, keys){
			// Erzeuge leeren String für die CSV-/Excel-Datei
			var csvFile = "";
			// Füge die Überschriften/Schlüssel als erste Zeile dem String hinzu
			csvFile += keys.join(',')+'\n';
			//Füge Zeile für Zeile die einzelnen Datenpunkte und Label hinzu
			for(var i = 0;i<dataPoints.length;i++){
				csvFile += dataPoints[i]+","+dataLabels[i];
				if(i+1!= dataPoints.length){
					csvFile+= '\n';
				}
			}
			// Erzeuge Download
			var blobChart = new Blob([csvFile],{type: 'text/csv;charset=utf-8;'});
            if(navigator.msSaveBlob){
            	navigator.msSaveBlob(blobChart, filename);
            }
            else{
            	var link = document.createElement("a");
                if(link.download !== undefined){
                    var url = URL.createObjectURL(blobChart);
                    link.setAttribute("href", url);
        	        link.setAttribute("download", filename);
                	link.style.visibility = "hidden";
                    document.body.appendChild(link);
                    link.click();
                  	document.body.removeChild(link);
                }
            }
			
		}

		// Export Charts als PDF-Datei

		// line-Charts
		jQuery(document).on('click','#systemstatistiken-pdfexport-user-active', function(e){
			var dataPoints = [];
			var dataLabels = [];
			// Sortiere Daten, damit diese in zeitlich aufsteigender Reihenfolge angezeigt werden
			lineChartUserActive.config.data.datasets[0].data.sort(function(a,b){
				var keyA = a.x;
				var keyB = b.x;
				if(keyA < keyB) return -1;
				if(keyA > keyB) return 1;
				return 0;
			});
			for(var i=0;i<lineChartUserActive.config.data.datasets[0].data.length;i++){
				dataPoints.push(lineChartUserActive.config.data.datasets[0].data[i].x);
				dataLabels.push(lineChartUserActive.config.data.datasets[0].data[i].y);
			}

			var dataHeadline = lineChartUserActive.config._config.options.scales.x.title.text + " - " + lineChartUserActive.config._config.options.scales.y.title.text;
			systemstatisticsGeneratePDF(dataLabels,dataPoints,'Anzahl hinzugefügte Benutzer', 'Anzahl_hinzugefügte_Benutzer', 'chartJSContainerUserActive', dataHeadline);
		});

		jQuery(document).on('click','#systemstatistiken-pdfexport-user-login', function(e){
			var dataPoints = [];
			var dataLabels = [];
			// Sortiere Daten, damit diese in zeitlich aufsteigender Reihenfolge angezeigt werden
			lineChartUserLogin.config.data.datasets[0].data.sort(function(a,b){
				var keyA = a.x;
				var keyB = b.x;
				if(keyA < keyB) return -1;
				if(keyA > keyB) return 1;
				return 0;
			});
			for(var i=0;i<lineChartUserLogin.config.data.datasets[0].data.length;i++){
				dataPoints.push(lineChartUserLogin.config.data.datasets[0].data[i].x);
				dataLabels.push(lineChartUserLogin.config.data.datasets[0].data[i].y);
			}
			var dataHeadline = lineChartUserLogin.config._config.options.scales.x.title.text + " - " + lineChartUserLogin.config._config.options.scales.y.title.text;
			systemstatisticsGeneratePDF(dataLabels,dataPoints,'Zeitpunkt des letzten Logins', 'Zeitpunkt_letzter_Login', 'chartJSContainerlastlogin', dataHeadline);
		});	
		
		jQuery(document).on('click','#systemstatistiken-pdfexport-user-input', function(e){
			var dataPoints = [];
			var dataLabels = [];
			// Sortiere Daten, damit diese in zeitlich aufsteigender Reihenfolge angezeigt werden
			lineChartUserInput.config.data.datasets[0].data.sort(function(a,b){
				var keyA = a.x;
				var keyB = b.x;
				if(keyA < keyB) return -1;
				if(keyA > keyB) return 1;
				return 0;
			});
			for(var i=0;i<lineChartUserInput.config.data.datasets[0].data.length;i++){
				dataPoints.push(lineChartUserInput.config.data.datasets[0].data[i].x);
				dataLabels.push(lineChartUserInput.config.data.datasets[0].data[i].y);
			}
			var dataHeadline = lineChartUserInput.config._config.options.scales.x.title.text + " - " + lineChartUserInput.config._config.options.scales.y.title.text;
			systemstatisticsGeneratePDF(dataLabels,dataPoints,'Anzahl Benutzereingaben', 'Anzahl_Benutzereingaben', 'chartJSContainerinput', dataHeadline);
		});
		
		jQuery(document).on('click','#systemstatistiken-pdfexport-user-lektionen', function(e){
			var dataPoints = [];
			var dataLabels = [];
			// Sortiere Daten, damit diese in zeitlich aufsteigender Reihenfolge angezeigt werden
			lineChartLektion.config.data.datasets[0].data.sort(function(a,b){
				var keyA = a.x;
				var keyB = b.x;
				if(keyA < keyB) return -1;
				if(keyA > keyB) return 1;
				return 0;
			});
			for(var i=0;i<lineChartLektion.config.data.datasets[0].data.length;i++){
				dataPoints.push(lineChartLektion.config.data.datasets[0].data[i].x);
				dataLabels.push(lineChartLektion.config.data.datasets[0].data[i].y);
			}
			var dataHeadline = lineChartLektion.config._config.options.scales.x.title.text + " - " + lineChartLektion.config._config.options.scales.y.title.text;
			systemstatisticsGeneratePDF(dataLabels,dataPoints,'Anzahl abgeschlossene Lektionen', 'Anzahl_abgeschlossene_Lektionen', 'chartJSContainerlektion', dataHeadline);
		});	
		
		// bar-Charts
		jQuery(document).on('click','#systemstatistiken-pdfexport-counthour', function(e){
			var dataPoints = barUserActiveCounthour.config.data.datasets[0].data;
			var dataLabels = barUserActiveCounthour.config.data.labels;
			var dataHeadline = barUserActiveCounthour.config._config.options.scales.x.title.text + " - " + barUserActiveCounthour.config._config.options.scales.y.title.text;
			systemstatisticsGeneratePDF(dataPoints,dataLabels,'Benutzeraktivität (gemessen in Seitenaufrufe/Uhrzeit)', 'Benutzeraktivitaet_Seitenaufrufe_Uhrzeit', 'chartJSContainercounthouractive', dataHeadline);
		});

		jQuery(document).on('click', '#systemstatistiken-pdfexport-sumhour', function(e){
			var dataPoints = barUserActiveSumhour.config.data.datasets[0].data;
			var dataLabels = barUserActiveSumhour.config.data.labels;
			var dataHeadline = barUserActiveSumhour.config._config.options.scales.x.title.text + " - " + barUserActiveSumhour.config._config.options.scales.y.title.text;
			systemstatisticsGeneratePDF(dataPoints,dataLabels,'Benutzeraktivität (gemessen in Sekunden/Uhrzeit)', 'Benutzeraktivitaet_Sekunden_Uhrzeit', 'chartJSContainersumhouractive', dataHeadline);
		});

		jQuery(document).on('click', '#systemstatistiken-pdfexport-countweekday', function(e){
			var monthStrings = ["Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag"];
			var dataPoints = barUserActiveCountWeekday.config.data.datasets[0].data;
			var dataLabels = barUserActiveCountWeekday.config.data.labels;
			// Die Labels in lesbare Form bringen
			for(var i=0; i< dataLabels.length; i++){
				dataLabels[i] = monthStrings[parseInt(dataLabels[i])];
			}
			var dataHeadline = barUserActiveCountWeekday.config._config.options.scales.x.title.text + " - " + barUserActiveCountWeekday.config._config.options.scales.y.title.text;
			systemstatisticsGeneratePDF(dataPoints,dataLabels,'Benutzeraktivität (gemessen in Seitenaufrufe/Wochentag)', 'Benutzeraktivitaet_Seitenaufrufe_Wochentag', 'chartJSContainercountweekdayactive', dataHeadline);
		});

		jQuery(document).on('click', '#systemstatistiken-pdfexport-sumweekday', function(e){
			var monthStrings = ["Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag"];
			var dataPoints = barUserActiveSumweekday.config.data.datasets[0].data;
			var dataLabels = barUserActiveSumweekday.config.data.labels;
			// Die Labels in lesbare Form bringen
			for(var i=0; i< dataLabels.length; i++){
				dataLabels[i] = monthStrings[parseInt(dataLabels[i])];
			}
			var dataHeadline = barUserActiveSumweekday.config._config.options.scales.x.title.text + " - " + barUserActiveSumweekday.config._config.options.scales.y.title.text;
			systemstatisticsGeneratePDF(dataPoints,dataLabels,'Benutzeraktivität (gemessen in Sekunden/Wochentag)', 'Benutzeraktivitaet_Sekunden_Wochentag', 'chartJSContainersumweekdayactive', dataHeadline);
		});

		jQuery(document).on('click', '#systemstatistiken-pdfexport-countpage', function(e){
			var dataPoints = barChartUserActiveCountpage.config.data.datasets[0].data;
			var dataLabels = barChartUserActiveCountpage.config.data.labels;
			var dataHeadline = barChartUserActiveCountpage.config._config.options.scales.x.title.text + " - " + barChartUserActiveCountpage.config._config.options.scales.y.title.text;
			systemstatisticsGeneratePDF(dataPoints,dataLabels,'Benutzeraktivität (gemessen in Seitenaufrufe/Seite)', 'Benutzeraktivitaet_Seitenaufrufe_Seite', 'chartJSContainercountpageactive', dataHeadline);
		});

		jQuery(document).on('click', '#systemstatistiken-pdfexport-sumpage', function(e){
			var dataPoints = barChartUserActiveSumpage.config.data.datasets[0].data;
			var dataLabels = barChartUserActiveSumpage.config.data.labels;
			var dataHeadline = barChartUserActiveSumpage.config._config.options.scales.x.title.text + " - " + barChartUserActiveSumpage.config._config.options.scales.y.title.text;
			systemstatisticsGeneratePDF(dataPoints,dataLabels,'Benutzeraktivität (gemessen in Sekunden/Seite)', 'Benutzeraktivitaet_Sekunden_Seite', 'chartJSContainersumpageactive', dataHeadline);
		});

		// pie-Charts
		jQuery(document).on('click', '#systemstatistiken-pdfexport-countbrowser', function(e){
			var dataPoints = pieUserActiveCountBrowser.config.data.datasets[0].data;
			var dataLabels = pieUserActiveCountBrowser.config.data.labels;
			var dataHeadline =  "Browser - Anzahl Seitenaufrufe" ;
			systemstatisticsGeneratePDF(dataPoints,dataLabels,'Browserverteilung (gemessen in Seitenaufrufen)', 'Browserverteilung', 'chartJSContainercountbrowser', dataHeadline);
		});

		jQuery(document).on('click', '#systemstatistiken-pdfexport-countdevicetype', function(e){
			var dataPoints = pieUserActiveCountDeviceType.config.data.datasets[0].data;
			var dataLabels = pieUserActiveCountDeviceType.config.data.labels;
			var dataHeadline =  "Gerätetyp - Anzahl Seitenaufrufe" ;
			systemstatisticsGeneratePDF(dataPoints,dataLabels,'Verteilung der Gerätetypen (gemessen in Seitenaufrufen)', 'Verteilung_Gerätetypen', 'chartJSContainercountdevicetype', dataHeadline);
		});

		// horizontal-Chart
		jQuery(document).on('click', '#systemstatistiken-pdfexport-training-count', function(e){
			var dataPoints = horizontalBarUserTrainingCount.config.data.datasets[0].data;
			var dataLabels = horizontalBarUserTrainingCount.config.data.labels;
			var dataHeadline = horizontalBarUserTrainingCount.config._config.options.scales.y.title.text + " - " + horizontalBarUserTrainingCount.config._config.options.scales.x.title.text;
			systemstatisticsGeneratePDF(dataPoints,dataLabels,'Anzahl abgeschlossene Trainings', 'Anzahl_abgeschlossene_Trainings', 'chartJSContainertrainingcount', dataHeadline);
		});

		function systemstatisticsGeneratePDF(dataPoints, dataLabels, title,pdfname, canvasid, dataHeadline){
			// Festlegen der Breite des Rands (in mmm)
			var pdfMarginOben = 25;
			var pdfMarginUnten = 20;
			var pdfMarginLinks = 25;
			var pdfMarginRechts = 25;
			// Angaben zum Format
			var pdfOrientation = 'p' // Portrait/Hochformat
			var pdfFormat = 'a4';
			var doc = new jspdf.jsPDF({orientation: pdfOrientation, format: pdfFormat});
			doc.page = 1;
			var pdfCenter = doc.internal.pageSize.width/2;
			// Überschrift
			doc.setFontSize(18);
			doc.setFont('helvetica', 'bold');
			doc.setTextColor(0,0,0);
			doc.text(title, pdfCenter, pdfMarginOben,{align: 'center', maxWidth: 160});
			// Anzahl an Zeilen, die die Überschrift abdeckt
			var titleLines = Math.ceil((doc.getStringUnitWidth(title)*doc.getFontSize()/(72/25.6))/160);
			// Diagramm in PDF einfügen
			var canvas = document.getElementById(canvasid);
			// Abstand zwischen Überschrift und Bild in mm
			var pdfOffset = 10*titleLines; // Offset der zusätzlich addiert wird (abhängig von der Anzahl an Zeilen, die die Überschrift abdeckt)
			// Fontsize und LineHeightFactor werden jeweils in pt angegeben, 0,352778 ist Faktor zum Umrechnen in mm
			var currentPositionHeight = pdfMarginOben + doc.getFontSize()*0.352778+doc.getLineHeightFactor()*0.352778*pdfOffset;
			var imageWidth = doc.internal.pageSize.width - pdfMarginLinks - pdfMarginRechts;
			var imageHeight = imageWidth/1.5;
			doc.addImage(canvas.toDataURL("image/png"), 'PNG', pdfMarginLinks, currentPositionHeight, imageWidth, imageHeight);
			// Füge Seitenzahl hinzu
			doc.setFontSize(12);
			doc.setFont('helvetica', 'normal');
			doc.setTextColor(0,0,0);
			doc.text("-"+doc.page+"-", pdfCenter, doc.internal.pageSize.height-10,{align: 'center'});
			// Füge weitere Seite hinzu
            doc.addPage(pdfFormat,pdfOrientation);
			doc.page++;
            doc.text("-"+doc.page+"-", pdfCenter, doc.internal.pageSize.height-10,{align: 'center'});
        	doc.setFontSize(18);
			doc.setFont('helvetica', 'bold');
			doc.setTextColor(0,0,0);
			doc.text(title+" - Daten", pdfCenter, pdfMarginOben, {align: 'center', maxWidth: 160});
			// Anzahl an Zeilen, die die Überschrift abdeckt
			titleLines = Math.ceil((doc.getStringUnitWidth(title+" - Daten")*doc.getFontSize()/(72/25.6))/160);
			pdfOffset = 10*titleLines; // Offset der zusätzlich addiert wird (abhängig von der Anzahl an Zeilen, die die Überschrift abdeckt)
			currentPositionHeight = pdfMarginOben+doc.getFontSize()*0.352778+doc.getLineHeightFactor()*0.352778+pdfOffset;
			doc.setFontSize(12);
			doc.setFont('helvetica', 'italic');
			doc.setTextColor(0,0,0);
			doc.text(dataHeadline, pdfMarginLinks, currentPositionHeight);
			pdfOffset = 5;
			currentPositionHeight = currentPositionHeight + doc.getFontSize()*0.352778+doc.getLineHeightFactor()*0.352778+pdfOffset;
			doc.setFontSize(11);
            doc.setFont('helvetica', 'normal');
            var textarray = [];
			for(var i=0; i<dataPoints.length; i++){
				textarray.push(dataLabels[i]+" - "+dataPoints[i]);
			}
			var maxHeight = doc.internal.pageSize.height;

			for(var j=0; j<textarray.length;j++){
				// Falls das aktuelle Element nicht mehr auf die Seite passt
				if(currentPositionHeight + pdfMarginUnten > maxHeight){

					doc.addPage(pdfFormat, pdfOrientation);
					doc.page++;
					doc.setFontSize(12);
					doc.setFont('helvetica', 'normal');
					doc.setTextColor(0,0,0);
					doc.text("-"+doc.page+"-", pdfCenter, doc.internal.pageSize.height-10,{align: 'center'});
					doc.setFontSize(18);
					doc.setFont('helvetica', 'bold');
					doc.setTextColor(0,0,0);
					doc.text(title+" - Daten", pdfCenter, pdfMarginOben, {align: 'center'});
					// Anzahl an Zeilen, die die Überschrift abdeckt
					titleLines = Math.ceil((doc.getStringUnitWidth(title+" - Daten")*doc.getFontSize()/(72/25.6))/160);
					pdfOffset = 10*titleLines; // Offset der zusätzlich addiert wird (abhängig von der Anzahl an Zeilen, die die Überschrift abdeckt)
					currentPositionHeight = pdfMarginOben + doc.getFontSize()*0.352778+doc.getLineHeightFactor()*0.352778+pdfOffset;
					doc.setFontSize(12);
					doc.setFont('helvetica', 'italic');
					doc.setTextColor(0,0,0);
					doc.text(dataHeadline, pdfMarginLinks, currentPositionHeight);
					pdfOffset = 5;
					currentPositionHeight = currentPositionHeight + doc.getFontSize()*0.352778+doc.getLineHeightFactor()*0.352778+pdfOffset;
					doc.setFontSize(11);
                	doc.setFont('helvetica', 'normal');
				}
				doc.text(textarray[j],pdfMarginLinks, currentPositionHeight);
                // Abstand zwischen den einzelnen Zeilen in mm
                pdfOffset = 0.5;
                currentPositionHeight += doc.getFontSize()*0.352778+doc.getLineHeightFactor()*0.352778+pdfOffset;
			}
			doc.save(pdfname);
		}
				
		// Ende Systemstatistiken

		// Coaching-Overview Start
		jQuery(document).on('click', '#coaching-overview-user-coach-select-all', function (e) {
			jQuery('.coaching-overview-user-coach-select').each(function (e) {
				jQuery(this).prop("checked", true);
				jQuery('#coaching-overview-user-coach-select-button').attr("disabled", false);
			});
		});

		jQuery(document).on('click', '.coaching-overview-user-coach-select', function (e) {
			if(jQuery('.coaching-overview-user-coach-select:checked').length > 1) {
				jQuery('#coaching-overview-user-coach-select-button').attr("disabled", false);
			} else {
				jQuery('#coaching-overview-user-coach-select-button').attr("disabled", true);
			}
		});

		jQuery(document).on('click', '#coaching-overview-user-coach-select-button', function (e) {
			jQuery('#coachingOverviewUsers2CoachModalCount').html(jQuery('.coaching-overview-user-coach-select:checked').length);

			jQuery('#coachingOverviewUsers2CoachModal').modal('show');
		});

		jQuery(document).on('click', '#coaching-overview-users2coach-button', function () {
			let btn = jQuery(this);
			userFeedback(btn, 'loading');

			let checkedCoach = jQuery('input[name="multi_coaches"]:checked').val();
			if(checkedCoach) {
				let users = [];
				jQuery('.coaching-overview-user-coach-select:checked').each(function (e) {
					users.push(jQuery(this).val());
				});
				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data:{
						"action":"coaching_overview_multiAssignCoach",
						"users":users,
						"coach":checkedCoach
					},
					success: function (data){
						if(data == "1") {
							userFeedback(btn, 'success');
							setTimeout(() =>{
								location.reload();
							}, 1500);
						}
						else{
							userFeedback(btn, 'error');
							setTimeout(() => {
								userFeedback(btn, 'text');
							}, 1500);
						}
					},
					error: function (errorThrown){
						userFeedback(btn,'error');
						setTimeout(()=>{
							userFeedback(btn, 'text');
						}, 1500);
						console.error(errorThrown);
					}
				});

			} else {
				userFeedback(btn, 'error');
				jQuery('#coaching-overview-users2coach-modal-errormsg').removeClass("d-none");
				setTimeout(() => {
					userFeedback(btn, 'text');
					jQuery('#coaching-overview-users2coach-modal-errormsg').addClass("d-none");
				}, 2500);
			}
		});

		jQuery(document).on('click', '#coaching-overview-user2coach-button', function(e){
			var thisButton = jQuery(this);
			userFeedback(jQuery(thisButton) , 'loading');
			var thisUser2CoachModal = jQuery(thisButton).parentsUntil("div.modal.fade.show").parent();
			var thisUser2CoachModalId = jQuery(thisUser2CoachModal).attr("id");
			var checkedElement = jQuery(thisUser2CoachModal).find("input.form-check-input:checked"); // Gechecktes Element, sofern vorhanden. Muss definitv überprüft werden, ob ein Element ausgewählt wurde
			var userID = thisUser2CoachModalId.replace("coachingOverviewUser2CoachModal","");
			if(checkedElement.length == 0){
				jQuery("#coaching-overview-user2coach-modal-errormsg_"+userID).css("display", "block");
				userFeedback(jQuery(thisButton), 'error');
				setTimeout(() => {
					userFeedback(jQuery(thisButton), 'text');
					jQuery("#coaching-overview-user2coach-modal-errormsg_"+userID).css("display", "none");
				}, 2500);
			}
			else{
				var idCheckedElement = jQuery(checkedElement).attr("id");
				idCheckedElement = idCheckedElement.replace("user_","");
				idCheckedElement = idCheckedElement.replace(userID, "");
				idCheckedElement = idCheckedElement.replace("_", "");
				var coachID = idCheckedElement.replace("coach_","");

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data:{
						"action":"coaching_overview_assignCoach",
						"userID":userID,
						"coachID":coachID
					},
					success: function (data){
						if(data == "1") {
							userFeedback(jQuery(thisButton), 'success');
							setTimeout(() =>{
								location.reload();
							}, 1500);
						}
						else{
							userFeedback(jQuery(thisButton), 'error');
							setTimeout(() => {
								userFeedback(jQuery(thisButton), 'text');
							}, 1500);
						}
					},
					error: function (errorThrown){
						userFeedback(jQuery(thisButton),'error');
						setTimeout(()=>{
							userFeedback(jQuery(thisButton), 'text');
						}, 1500);
						console.error(errorThrown);
					}
				});
			}
		});

		jQuery(document).on('click', '.coaching-overview-save-coaching-notes-button', function(e){
			var thisButton = jQuery(this)
			userFeedback(jQuery(thisButton),'loading');
			var userid = jQuery(thisButton).parent().parent().parent().attr('user-id');
			var coachingNotes = jQuery(thisButton).parent().find("#coaching_notes").val();
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data:{
					"action": "coaching_overview_save_coaching_notes",
					"userid" : userid,
					"coachingNotes": coachingNotes
				},
				success: function(data){
					if(data == "1") {
						userFeedback(jQuery(thisButton), 'success');
						setTimeout(() =>{
							location.reload();
						}, 1500);
					}
					else{
						userFeedback(jQuery(thisButton), 'error');
						setTimeout(() => {
							userFeedback(jQuery(thisButton), 'text');
						}, 1500);
					}
				},
				error: function(errorThrown){
					userFeedback(jQuery(thisButton),'error');
						setTimeout(()=>{
							userFeedback(jQuery(thisButton), 'text');
					}, 1500);
					console.error(errorThrown);
				}
			});
		});
		// Coaching-Overview Ende

		// Auswahl Coachingmodus Start

		// Einfache Auswahl durch User 
		jQuery(document).on('click', '.coachingmode-selection-button-single', function (e){
			var thisButton = jQuery(this);
			var trainingsID = jQuery(thisButton).val();
			userFeedback(jQuery(thisButton), 'loading');
			var coachingMode = 0;
			jQuery(document).find('.coachingmode-selection-radio-container input').each(function(e){
				if(jQuery(this).is(':checked')){
					coachingMode = jQuery(this).val();
				}
			});
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data:{
					"action": "coachingmode_form_saveCoachingMode",
					"trainingsID": trainingsID,
					"coachingMode": coachingMode,
					"coachingModeFormMulti": 0
				},
				success: function(data){
					console.log(data);
					if(data == "1"){
						userFeedback(jQuery(thisButton), 'success');
						setTimeout(()=>{
							jQuery(thisButton).css('display', 'none');
							jQuery('#'+trainingsID+'-coachingmode-selection-alert-msg').css('display', 'block');
						},1500);
					}
					else{
						userFeedback(jQuery(thisButton), 'error');
						setTimeout(() =>{
							userFeedback(jQuery(thisButton), 'text');
						}, 1500);
					}
				},
				error: function(errorThrown){
					userFeedback(jQuery(thisButton),'error');
					setTimeout(()=>{
						userFeedback(jQuery(thisButton), 'text');
					}, 1500);
					console.error(errorThrown);
				}
			});
		});

		// Mehrfache Auswahl durch User
		jQuery(document).on('click', '.coachingmode-selection-button-multi', function (e){
			var thisButton = jQuery(this);
			var trainingsID = jQuery(thisButton).val();
			userFeedback(jQuery(thisButton), 'loading');
			var coachingMode = 0;
			jQuery(document).find('.coachingmode-selection-radio-container input').each(function(e){
				if(jQuery(this).is(':checked')){
					coachingMode = jQuery(this).val();
				}
			});
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data:{
					"action": "coachingmode_form_saveCoachingMode",
					"trainingsID": trainingsID,
					"coachingMode": coachingMode,
					"coachingModeFormMulti": 1
				},
				success: function(data){
					console.log(data);
					if(data == "1"){
						userFeedback(jQuery(thisButton), 'success');
						setTimeout(()=>{
							userFeedback(jQuery(thisButton), 'text');
						},1500);
					}
					else{
						userFeedback(jQuery(thisButton), 'error');
						setTimeout(() =>{
							userFeedback(jQuery(thisButton), 'text');
						}, 1500);
					}
				},
				error: function(errorThrown){
					userFeedback(jQuery(thisButton),'error');
					setTimeout(()=>{
						userFeedback(jQuery(thisButton), 'text');
					}, 1500);
					console.error(errorThrown);
				}
			});
		});

		// Reset des Eintrags/der "Sperre" für Coaching-Modus-Formular
		jQuery(document).on('click', '.training-mgr-reset-coachingmodeformentry-button', function(e){
			var thisButton = jQuery(this);
			var trainingsID = jQuery(thisButton).val();
			userFeedback(jQuery(thisButton), 'loading');
			var users = [];
			jQuery('.custom-table tbody tr').each(function(index){
				var id = jQuery(this).find("td").first().attr("valueid");
				users.push(id);
			});
			var usersstr = JSON.stringify(users);

			if(usersstr != ""){
				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data:{
						"action": "coachingmode_form_resetCoachingModeFormMeta",
						"users": usersstr,
						"training": trainingsID
					},
					success: function(data){
						if(data == "1"){
							userFeedback(jQuery(thisButton), 'success');
							setTimeout(()=>{
								userFeedback(jQuery(thisButton), 'text');
							},1500);
						}
						else{
							userFeedback(jQuery(thisButton), 'error');
							setTimeout(() =>{
								userFeedback(jQuery(thisButton), 'text');
							}, 1500);
						}
					},
					error: function(errorThrown){
						userFeedback(jQuery(thisButton),'error');
						setTimeout(()=>{
							userFeedback(jQuery(thisButton), 'text');
						}, 1500);
						console.error(errorThrown);
					}
				});
			}
		});
		// Auswahl Coachingmodus Ende

		// Admin Backend Addon Page - Patch
		jQuery(document).on('submit', '#admin-backend-patch-form', function (e) {
			e.preventDefault();
			submitPatch(0, false);
		});

		jQuery(document).on('click', '#admin-backend-submit-patch', function(e) {
			e.preventDefault();
			submitPatch(0, false);
		});

		jQuery(document).on('change', '#patchFileInput', function(e) {
			if(jQuery('#patchFileInput').get(0).files[0] != undefined) {
				jQuery('#admin-backend-submit-patch').prop('disabled', false);
			} else {
				jQuery('#admin-backend-submit-patch').prop('disabled', true);
			}
		});

		jQuery(document).on('click', '.patch-reload', function(e) {
			e.preventDefault();
			submitPatch(parseInt(jQuery(this).attr('value')), true);
		})

		function submitPatch(index, onlyone) {

			if(jQuery('#patchFileInput').get(0).files[0] != undefined) {

				userFeedback('admin-backend-submit-patch', 'loading');

				var files = getPatchFiles();
				
				jQuery('#patch-installed').remove();
				jQuery('#patch-error-text').remove();

				if(!onlyone) {
					var fileList = jQuery('#patchFileList');
					fileList.find('tbody').first().html('');

					for(var i = 0; i < jQuery('#patchFileInput').get(0).files.length; i++) {

						var file = jQuery('#patchFileInput').get(0).files[i];

						fileList.find('tbody').first().append('<tr><td>'+file.name+'</td><td></td><td></td></tr>');
					}

					fileList.removeClass('d-none');
				}

				sendPatchAjax(files, index, onlyone);
				
			} else {
				userFeedback('admin-backend-submit-patch', 'error');
				setTimeout(() => {
					userFeedback('admin-backend-submit-patch', 'text');
				}, 1500);
			}
		}

		function getPatchFiles() {
			var files = [];

			for(var i = 0; i < jQuery('#patchFileInput').get(0).files.length; i++) {

				var file = jQuery('#patchFileInput').get(0).files[i];

				files.push(file);
			}

			return files;
		}

		function sendPatchAjax(files, index, onlyone) {

			var fileList = jQuery('#patchFileList');

			if(index < files.length) {

				var status = fileList.find('td:contains('+files[index].name+')').parent().find('td');
				status.eq(1).html('<span class="userfeedback-loading"><i class="fas fa-spinner"></i></span>');
				status.eq(2).html('');

				var data = new FormData();
				data.append('action', 'admin_backend_patch_file');
				data.append('file', files[index]);

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: data,
					processData: false,
					contentType: false,
					success: function (data) {
						if(data == "success") {
							status.eq(1).html('<span class="userfeedback-success"><i class="far fa-check-circle"></i></span>');
							status.eq(2).html('');
							if(!onlyone) {
								sendPatchAjax(files, ++index);
							} else {
								sendPatchAjax(files, files.length);
							}
						} else {
							status.eq(1).html('<span class="userfeedback-error"><i class="far fa-times-circle"></i></span>');
							status.eq(2).html('<span class="userfeedback-error cursor-pointer patch-reload" value="'+index+'"><i class="fas fa-redo-alt"></i></span>');
							if(!onlyone) {
								sendPatchAjax(files, ++index);
							} else {
								sendPatchAjax(files, files.length);
							}
						}
					},
					error: function (errorThrown) {
						status.eq(1).html('<span class="userfeedback-error"><i class="far fa-times-circle"></i></span>');
						status.eq(2).html('<span class="userfeedback-error cursor-pointer patch-reload" value="'+index+'"><i class="fas fa-redo-alt"></i></span>');
						if(!onlyone) {
							sendPatchAjax(files, ++index);
						} else {
							sendPatchAjax(files, files.length);
						}
					}
				});
			} else if(index == files.length) {
				var success = true;
				fileList.find('tbody').first().find('tr').each(function (e) {
					if(jQuery(this).find('td').eq(1).html() == '<span class="userfeedback-error"><i class="far fa-times-circle"></i></span>') {
						success = false;
					}
				});

				jQuery('#patch-installed').remove();
				jQuery('#patch-error-text').remove();

				if(success) {
					userFeedback('admin-backend-submit-patch', 'success');
					jQuery('#patchFileInput').val('');
					jQuery('#admin-backend-submit-patch').prop('disabled', true);
					fileList.parent().append('<div id="patch-installed" class="alert mt-4 text-center alert-success">Patch erfolgreich installiert!</div>');
					setTimeout(() => {
						userFeedback('admin-backend-submit-patch', 'text');
					}, 1500);
				} else {
					userFeedback('admin-backend-submit-patch', 'error');
					fileList.parent().append('<div id="patch-error-text" class="alert mt-4 text-center alert-danger">Patch konnte nicht erfolgreich installiert werden.<br>Mit einem Klick auf <i class="fas fa-redo-alt"></i> kann versucht werden, den Patch nochmals zu installieren.</div>');
					setTimeout(() => {
						userFeedback('admin-backend-submit-patch', 'text');
					}, 1500);
				}
			}
		}
		// ENDE Admin Backend Addon Page - Patch

		// ANFANG Admin Backend Berechtigungen
		jQuery('#permissionsNewRoleForm').submit(function (e) {
			e.preventDefault();
			jQuery('#permissionsNewRoleButton').click();
		})

		jQuery(document).on('click', '#permissionsNewRoleButton', function (e) {

			userFeedback('permissionsNewRoleButton', 'loading');
			
			jQuery('#permissionsNewRoleErrorAlert').addClass('d-none');
			jQuery('#permissionsNewRoleErrorAlert p').first().html('');

			var data = {};
			data['action'] = 'admin_backend_permissions_new_role';
			jQuery('#permissionsNewRoleForm input, #permissionsNewRoleForm textarea').each(function (e) {
				data[jQuery(this).attr('id')] = jQuery(this).val();
			});
			
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				success: function (data) {
					if(data == "success") {
						userFeedback('permissionsNewRoleButton', 'success');

						jQuery('#permissionsNewRoleForm input, #permissionsNewRoleForm textarea').each(function (e) {
							jQuery(this).val('');
						});

						setTimeout(() => {
							jQuery('#permissionsNewRoleModal').modal('hide');
							reloadPermissionsTable();
						}, 500);

						setTimeout(() => {
							userFeedback('permissionsNewRoleButton', 'text');
						}, 1500);
					} else {
						userFeedback('permissionsNewRoleButton', 'error');

						if(data != "") {
							jQuery('#permissionsNewRoleErrorAlert').removeClass('d-none');
							jQuery('#permissionsNewRoleErrorAlert p').first().html(data);
						}

						setTimeout(() => {
							userFeedback('permissionsNewRoleButton', 'text');
						}, 1500);
					}
				},
				error: function (errorThrown) {
					userFeedback('permissionsNewRoleButton', 'error');

					jQuery('#permissionsNewRoleErrorAlert').removeClass('d-none');
					jQuery('#permissionsNewRoleErrorAlert p').first().html('Ein unbekannter Fehler ist aufgetreten.');

					setTimeout(() => {
						userFeedback('permissionsNewRoleButton', 'text');
					}, 1500);
				}
			});
		});

		jQuery(document).on('click', '.admin-backend-permissions-role-delete', function(e) {
			var roleid = jQuery(this).data('roleid');
			var roletitle = jQuery(this).data('roletitle');

			jQuery(this).tooltip('hide');

			jQuery('#permissionsDeleteRoleTitle').html(roletitle);
			jQuery('#permissionsDeleteRoleId').val(roleid);

			jQuery('#permissionsDeleteRoleModal').modal('show');
		});

		jQuery(document).on('click', '#permissionsDeleteRoleConfirm', function (e) {
			var button = jQuery('#permissionsDeleteRoleConfirm');

			var roleid = jQuery('#permissionsDeleteRoleId').val();

			if(roleid != undefined && roleid != "") {

				userFeedback(button, 'loading');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						action: 'admin_backend_permissions_delete_role',
						roleid: roleid,
					},
					success: function (data) {
						if(data == "success") {
							userFeedback(button, 'success');
	
							setTimeout(() => {
								userFeedback(button, 'text');
								jQuery('#permissionsDeleteRoleModal').modal('hide');
								reloadPermissionsTable();
								jQuery('#oldPremissionsFoundDiv').addClass('d-none');
							}, 500);
						} else {
							userFeedback(button, 'error');
	
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback(button, 'error');
	
						setTimeout(() => {
							userFeedback(button, 'text');
						}, 1500);
					}
				});

			} else {
				userFeedback(button, 'error');

				setTimeout(() => {
					userFeedback(button, 'text');
				}, 1500);
			}
		});

		jQuery(document).on('click', '.admin-backend-permissions-role-edit', function(e) {
			var roleid = jQuery(this).data('roleid');
			var roletitle = jQuery(this).data('roletitle');
			var roledesc = jQuery(this).data('roledesc');

			jQuery(this).tooltip('hide');

			jQuery('#permissionsEditRoleTitle').val(roletitle);
			jQuery('#permissionsEditRoleId').val(roleid);
			jQuery('#permissionsEditRoleDesc').val(roledesc);

			jQuery('#permissionsEditRoleModal').modal('show');
		});

		jQuery('#permissionsEditRoleForm').submit(function (e) {
			e.preventDefault();
			jQuery('#permissionsEditRoleButton').click();
		})

		jQuery(document).on('click', '#permissionsEditRoleButton', function (e) {

			userFeedback('permissionsEditRoleButton', 'loading');
			
			jQuery('#permissionsEditRoleErrorAlert').addClass('d-none');
			jQuery('#permissionsEditRoleErrorAlert p').first().html('');

			var data = {};
			data['action'] = 'admin_backend_permissions_edit_role';
			jQuery('#permissionsEditRoleForm input, #permissionsEditRoleForm textarea').each(function (e) {
				data[jQuery(this).attr('id')] = jQuery(this).val();
			});
			
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				success: function (data) {
					if(data == "success") {
						userFeedback('permissionsEditRoleButton', 'success');

						setTimeout(() => {
							jQuery('#permissionsEditRoleModal').modal('hide');
							reloadPermissionsTable();
						}, 500);

						setTimeout(() => {
							userFeedback('permissionsEditRoleButton', 'text');
							
							jQuery('#permissionsEditRoleForm input, #permissionsEditRoleForm textarea').each(function (e) {
								jQuery(this).val('');
							});
						}, 1500);
					} else {
						userFeedback('permissionsEditRoleButton', 'error');

						if(data != "") {
							jQuery('#permissionsEditRoleErrorAlert').removeClass('d-none');
							jQuery('#permissionsEditRoleErrorAlert p').first().html(data);
						}

						setTimeout(() => {
							userFeedback('permissionsEditRoleButton', 'text');
						}, 1500);
					}
				},
				error: function (errorThrown) {
					userFeedback('permissionsEditRoleButton', 'error');

					jQuery('#permissionsEditRoleErrorAlert').removeClass('d-none');
					jQuery('#permissionsEditRoleErrorAlert p').first().html('Ein unbekannter Fehler ist aufgetreten.');

					setTimeout(() => {
						userFeedback('permissionsEditRoleButton', 'text');
					}, 1500);
				}
			});
		});

		jQuery(document).on('click', '.admin-backend-permissions-role-prio', function(e) {
			var roleid = jQuery(this).data('roleid');
			var priodir = jQuery(this).data('priodir');
			var button = jQuery(this);

			if(roleid != undefined && roleid != "") {

				jQuery(button).tooltip('hide');
				userFeedback(button, 'loading');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						action: 'admin_backend_permissions_update_role',
						roleid: roleid,
						priodir: priodir,
					},
					success: function (data) {
						if(data == "success") {
							userFeedback(button, 'success');
	
							setTimeout(() => {
								reloadPermissionsTable();
							}, 500);
						} else {
							userFeedback(button, 'error');
	
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback(button, 'error');
	
						setTimeout(() => {
							userFeedback(button, 'text');
						}, 1500);
					}
				});

			} else {
				userFeedback(button, 'error');

				setTimeout(() => {
					userFeedback(button, 'text');
				}, 1500);
			}
		});

		function reloadPermissionsTable() {
			jQuery('#permissions-table-content').html('<div class="w-100 text-center"><span class="userfeedback-loading"><i class="fas fa-spinner"></i></span></div>');

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					action: 'admin_backend_permissions_table',
				},
				success: function (data) {
					jQuery('#permissions-table-content').html(data);
					jQuery('[data-toggle="tooltip"]').tooltip({ container: '.wrapper-admin-backend'} );
				},
				error: function (errorThrown) {
					location.reload();
				}
			});
		}

		jQuery(document).on('click', '#admin-backend-permissions-button', function (e) {

			var data = {};
			data['action'] = "admin_backend_permissions_save";

			jQuery('#admin-backend-permissions-form input').each(function (e) {
				data[jQuery(this).attr('id')] = jQuery(this).is(':checked');
			});
			
			userFeedback('admin-backend-permissions-button', 'loading');

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				success: function (data) {
					if(data == "success") {
						userFeedback('admin-backend-permissions-button', 'success');

						setTimeout(() => {
							location.reload();

							userFeedback('admin-backend-permissions-button', 'text');
						}, 1500);
					} else {
						userFeedback('admin-backend-permissions-button', 'error');

						setTimeout(() => {
							userFeedback('admin-backend-permissions-button', 'text');
						}, 1500);
					}
				},
				error: function (errorThrown) {
					userFeedback('admin-backend-permissions-button', 'error');

					setTimeout(() => {
						userFeedback('admin-backend-permissions-button', 'text');
					}, 1500);
				}
			});

		});
		
		jQuery(document).on('click', '#admin-backend-permissions-updatefunctions', function (e) {
			var button = jQuery(this);
			userFeedback(button, 'loading');

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					action: 'admin_backend_permissions_update',
				},
				success: function (data) {
					if(data == "success") {
						userFeedback(button, 'success');

						location.reload();

						setTimeout(() => {
							userFeedback(button, 'text');
						}, 1500);
					} else {
						userFeedback(button, 'error');

						setTimeout(() => {
							userFeedback(button, 'text');
						}, 1500);
					}
				},
				error: function (errorThrown) {
					userFeedback(button, 'error');

					setTimeout(() => {
						userFeedback(button, 'text');
					}, 1500);
				}
			});
		});

		jQuery(document).on('click', '.admin-backend-permissions-default', function (e) {
			var button = jQuery(this);
			userFeedback(button, 'loading');

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					action: 'admin_backend_permissions_default',
				},
				success: function (data) {
					if(data == "success") {
						userFeedback(button, 'success');

						location.reload();

						setTimeout(() => {
							userFeedback(button, 'text');
						}, 1500);
					} else {
						userFeedback(button, 'error');

						setTimeout(() => {
							userFeedback(button, 'text');
						}, 1500);
					}
				},
				error: function (errorThrown) {
					userFeedback(button, 'error');

					setTimeout(() => {
						userFeedback(button, 'text');
					}, 1500);
				}
			});
		});

		jQuery(document).on('click', '#admin-backend-permissions-migrate', function (e) {
			userFeedback('admin-backend-permissions-migrate', 'loading');

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					action: 'admin_backend_permissions_migrate',
				},
				success: function (data) {
					if(data == "success") {
						userFeedback('admin-backend-permissions-migrate', 'success');

						setTimeout(() => {
							jQuery('#admin-backend-permissions-migrate').parent().parent().parent().remove();
						}, 1500);
					} else {
						userFeedback('admin-backend-permissions-migrate', 'error');

						setTimeout(() => {
							userFeedback('admin-backend-permissions-migrate', 'text');
						}, 1500);
					}
				},
				error: function (errorThrown) {
					userFeedback('admin-backend-permissions-migrate', 'error');

					setTimeout(() => {
						userFeedback('admin-backend-permissions-migrate', 'text');
					}, 1500);
				}
			});
		});
		// ENDE Admin Backend Berechtigungen

		// START User Modus
		jQuery(document).on('click', '#user-detail-startusermodus', function (e) {
			userFeedback('user-detail-startusermodus', 'loading');

			var coach_select_user = jQuery('#user-detail-startusermodus').val();

			if(coach_select_user != "" && coach_select_user != undefined) {

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						coach_select_user: coach_select_user,
						action: 'user_modus_start',
					},
					success: function (data) {
						if(data == "1") {
							userFeedback('user-detail-startusermodus', 'success');
	
							setTimeout(() => {
	
								var url = new URL(jQuery('#user-detail-backlink').attr("href"));
								const urlParams = new URLSearchParams(url.search);
								window.location.href = window.location.href + "&backlink=" + urlParams.get("page_id");
	
							}, 1500);
						} else {
							userFeedback('user-detail-startusermodus', 'error');
	
							setTimeout(() => {
								userFeedback('user-detail-startusermodus', 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback('user-detail-startusermodus', 'error');
	
						setTimeout(() => {
							userFeedback('user-detail-startusermodus', 'text');
						}, 1500);
					}
				});

			} else {
				userFeedback('user-detail-startusermodus', 'error');

				setTimeout(() => {
					userFeedback('user-detail-startusermodus', 'text');
				}, 1500);
			}
		});

		jQuery(document).on('click', '#user-mode-overlay-disable-button', function (e) {
			userFeedback('user-mode-overlay-disable-button', 'loading');

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					coach_clear_user: true,
					action: 'user_modus_disable',
				},
				success: function (data) {
					if(data == "1") {
						userFeedback('user-mode-overlay-disable-button', 'success');

						setTimeout(() => {

							userFeedback('user-mode-overlay-disable-button', 'text');
							location.reload();

						}, 1500);
					} else {
						userFeedback('user-mode-overlay-disable-button', 'error');

						setTimeout(() => {
							userFeedback('user-mode-overlay-disable-button', 'text');
						}, 1500);
					}
				},
				error: function (errorThrown) {
					userFeedback('user-mode-overlay-disable-button', 'error');

					setTimeout(() => {
						userFeedback('user-mode-overlay-disable-button', 'text');
					}, 1500);
				}
			});
		})

		// ENDE User Modus

		// START Settingspage
		jQuery(document).on('click', '#settingsSave', function (e) {
			jQuery('#settingsNotAllowedAlert').addClass("d-none");
			var saveButton = jQuery(this)
			userFeedback(saveButton, 'loading');
			var data = {};
			var settings = {};

			jQuery('.tspv2settings input:checkbox:checked').each(function () {
				settings[this.id] = 1;
			});

			jQuery('.tspv2settings select:not([multiple]) option:selected').each(function () {
				var selectedKey = jQuery(this).attr("data-key");
				settings[selectedKey] = this.value;
			});

			jQuery('.tspv2settings select[multiple] option:selected').each(function () {
				var selectedKey = jQuery(this).attr("data-key");
				if(settings[selectedKey] == undefined) {
					settings[selectedKey] = [];
				}
				settings[selectedKey].push(this.value);
			});

			jQuery('.tspv2settings input[type="text"]:not(.tspv2SettingsDashboard), .tspv2settings input[type="number"], .tspv2settings textarea:not(.tspv2SettingsDashboard)').each(function () {
				settings[this.id] = this.value;
			});

			settings['dashboard_items'] = [];
			jQuery('#tspv2SettingsDashboardItems tr').each(function (e) {
				var item = {};
				item['title'] = jQuery(this).find('input').first().val();
				item['css'] = jQuery(this).find('input').last().val();
				item['content'] = jQuery(this).find('textarea').first().val();
				settings['dashboard_items'].push(item);
			});

			data['action'] = "saveSettings";
			data['setting'] = settings;

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: data,
					success: function (data) {
						if(data == "1") {
							userFeedback(saveButton, 'success');
							setTimeout(() => {
								userFeedback(saveButton, 'text');
							}, 1500)
						} else {
							userFeedback(saveButton, 'error');
							if(data == "not allowed") {
								jQuery('#settingsNotAllowedAlert').removeClass("d-none");
							}
							setTimeout(() => {
								userFeedback(saveButton, 'text');
							}, 1500)
						}
					},
					error: function (errorThrown) {
						userFeedback(saveButton, 'error');
						setTimeout(() => {
							userFeedback(saveButton, 'text');
						}, 1500)
					}
				});
		});

		jQuery(document).on('click', '#backendSettingsDeleteDemoUser', function (e) {
			userFeedback('backendSettingsDeleteDemoUser', 'loading');

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					action: 'delete_demouser'
				},
				success: function (data) {
					if(data == "1") {
						userFeedback('backendSettingsDeleteDemoUser', 'success');
						jQuery('#backendSettingsDemoUserCount').html('0');
						jQuery('#demologin_maxaccounts').parent().find('small.text-danger').remove();
						jQuery('label[for="demologin_maxaccounts"]').find('i.fas').remove();
						
						jQuery('#settingsNotAllowedAlert').addClass("d-none");
						jQuery('#privacy_terms_URL').removeAttr("disabled");
						jQuery('#privacy_terms_URL').parent().find('span.text-danger').first().remove();
						setTimeout(() => {
							userFeedback('backendSettingsDeleteDemoUser', 'text');
						}, 1500)
					} else {
						userFeedback('backendSettingsDeleteDemoUser', 'error');
						setTimeout(() => {
							userFeedback('backendSettingsDeleteDemoUser', 'text');
						}, 1500)
					}
				},
				error: function (errorThrown) {
					userFeedback('backendSettingsDeleteDemoUser', 'error');
					setTimeout(() => {
						userFeedback('backendSettingsDeleteDemoUser', 'text');
					}, 1500)
				}
			});
		});

		jQuery(document).on('focusout', '#privacy_terms_URL', function(e) {

			if(jQuery('#privacy_terms_URL').val() == ""){
				jQuery('#hide_profile_privacy_terms').attr('checked', true);;
			}
			else{
				jQuery('#hide_profile_privacy_terms').attr('checked', false);;
			}

			if(jQuery('#privacy_terms_URL').val() != jQuery('#privacy_terms_URL').attr('data-oldsetting')) { // if changed
				jQuery('#settingsPrivacyTermsChangedModal').modal('show');
			}
		});

		jQuery(document).on('click', '#settingsPrivacyTermsRevert', function (e) {
			jQuery('#privacy_terms_URL').val(jQuery('#privacy_terms_URL').attr('data-oldsetting'));

			if(jQuery('#privacy_terms_URL').attr('data-oldsetting') == ""){
				jQuery('#hide_profile_privacy_terms').attr('checked', true);;
			}
			else{
				jQuery('#hide_profile_privacy_terms').attr('checked', false);;
			}

			jQuery('#settingsPrivacyTermsChangedModal').modal('hide');
		});

		jQuery(document).on('change', '#hide_mails', function(e){
			jQuery('#hide_profile_mail').attr('checked', jQuery(this).is(':checked'));
		});

		jQuery(document).on('change', '.user-event-settings-default-days', function (e) {
			jQuery('.user_event_settings_level_' + jQuery(this).attr('data-level')).val(jQuery(this).val());
		});

		jQuery(document).on('change', '#dashboard_hide_notrainings', function (e) {
			jQuery('#no_trainings_text_dashboard').attr('disabled', !jQuery(this).is(':checked'));
		});

		jQuery(document).on('click', '#tspv2SettingsDashboardAddItem', function (e) {
			var i = jQuery('#tspv2SettingsDashboardItems tr').length;
			jQuery('#tspv2SettingsDashboardItemsNone').addClass('d-none')
			jQuery('#tspv2SettingsDashboardItems').append('<tr id="tspv2DashboardItemRow-'+i+'" data-row="'+i+'" class="align-middle">' + 
								'<th scope="row" class="align-middle">'+ (i+1) +'</th>'+
								'<td class="align-middle">'+
									'<input type="text" class="form-control tspv2SettingsDashboard" placeholder="Titel">'+
								'</td>'+
								'<td class="align-middle">'+
									'<input type="text" class="form-control tspv2SettingsDashboard" placeholder="CSS-Klasse(n)">'+
								'</td>'+
								'<td class="align-middle">'+
									'<textarea rows="5" class="form-control tspv2SettingsDashboard" placeholder="Inhalt (Shortcode und/oder HTML-Code)"></textarea>'+
								'</td>'+
								'<td class="align-middle"><div class="d-flex flex-column">'+
									'<button type="button" class="btn btn-sm btn-light tspv2SettingsDashboardItemUp" title="Element nach oben schieben"><i class="fas fa-arrow-up"></i></button>'+
									'<button type="button" class="btn btn-sm btn-light tspv2SettingsDashboardItemDown mt-1" title="Element nach unten schieben"><i class="fas fa-arrow-down"></i></button>'+
									'<button type="button" class="btn btn-sm btn-danger tspv2SettingsDashboardItemDelete mt-1" title="Element entfernen"><i class="fas fa-trash-alt"></i></button>'+
								'</div></td>'+
							'</tr>')
			reorganizeDashboardItems();
		});

		jQuery(document).on('click', '.tspv2SettingsDashboardItemDown', function (e) {
			jQuery(this).parent().parent().parent().next().after(jQuery(this).parent().parent().parent());
			reorganizeDashboardItems();
		});

		jQuery(document).on('click', '.tspv2SettingsDashboardItemUp', function (e) {
			jQuery(this).parent().parent().parent().after(jQuery(this).parent().parent().parent().prev());
			reorganizeDashboardItems();
		});

		jQuery(document).on('click', '.tspv2SettingsDashboardItemDelete', function (e) {
			jQuery(this).parent().parent().parent().remove();
			reorganizeDashboardItems()
		});

		function reorganizeDashboardItems() {
			jQuery('.tspv2SettingsDashboardItemUp, .tspv2SettingsDashboardItemDown').removeClass("d-none");
			var i = 1;
			jQuery('#tspv2SettingsDashboardItems tr').each(function (e) {
				jQuery(this).find('th').first().html(i);
				i++;
			});
			jQuery('#tspv2SettingsDashboardItems tr').first().find('.tspv2SettingsDashboardItemUp').addClass("d-none");
			jQuery('#tspv2SettingsDashboardItems tr').last().find('.tspv2SettingsDashboardItemDown').addClass("d-none");
			if(jQuery('#tspv2SettingsDashboardItems tr').length == 0) {
				jQuery('#tspv2SettingsDashboardItemsNone').removeClass('d-none')
			}
		}

		jQuery(document).on('change', '#demologin_registerkey', function (e) {
			if(jQuery(this).val() == "") {
				jQuery('#demologin_autodelete').attr("disabled", true);
				jQuery('#demologin_maxaccounts').attr("disabled", true);
				jQuery('#demologin_modaltext').attr("disabled", true);
				jQuery('#demologin_modalbuttontext').attr("disabled", true);
				jQuery('#backendSettingsDeleteDemoUser').attr("disabled", true);
				jQuery('#backendSettingsDemoUserModalToggle').attr("disabled", true);
			} else {
				jQuery('#demologin_autodelete').removeAttr("disabled");
				jQuery('#demologin_maxaccounts').removeAttr("disabled");
				jQuery('#demologin_modaltext').removeAttr("disabled");
				jQuery('#demologin_modalbuttontext').removeAttr("disabled");
				jQuery('#backendSettingsDeleteDemoUser').removeAttr("disabled");
				jQuery('#backendSettingsDemoUserModalToggle').removeAttr("disabled");
			}
		})

		jQuery(document).on('click', '#adminBackendSaveUserEventSettings', function (e) {
			var data = {};
			var saveButton = jQuery('#adminBackendSaveUserEventSettings');

			jQuery('#userEventsSettingsTable input').each(function (e) {
				if(data[jQuery(this).attr("data-type")] == undefined) {
					data[jQuery(this).attr("data-type")] = {};
				}
				if(data[jQuery(this).attr("data-type")][jQuery(this).attr("data-level")] == undefined) {
					data[jQuery(this).attr("data-type")][jQuery(this).attr("data-level")] = {};
				}
				data[jQuery(this).attr("data-type")][jQuery(this).attr("data-level")] = jQuery(this).val();
			});

			userFeedback(saveButton, 'loading');

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					action: "save_settings_user_event",
					data: JSON.stringify(data)
				},
				success: function (data) {
					if(data == "1") {
						userFeedback(saveButton, 'success');
						setTimeout(() => {
							userFeedback(saveButton, 'text');
						}, 1500)
					} else {
						userFeedback(saveButton, 'error');
						setTimeout(() => {
							userFeedback(saveButton, 'text');
						}, 1500)
					}
				},
				error: function (errorThrown) {
					userFeedback(saveButton, 'error');
					setTimeout(() => {
						userFeedback(saveButton, 'text');
					}, 1500)
				}
			});
		});

		jQuery(document).on('click', '#adminBackendDeleteUserEvents', function (e) {
			var type = [];
			var level = [];
			var btn = jQuery('#adminBackendDeleteUserEvents');

			var user = jQuery('#userEventsDeleteForUser').val();
			jQuery('#userEventsDeleteForType option:selected').each(function (e) {
				type.push(jQuery(this).val());
			});
			jQuery('#userEventsDeleteForLevel option:selected').each(function (e) {
				level.push(jQuery(this).val());
			});
			
			if(user != "" && type.length > 0 && level.length > 0) {

				userFeedback(btn, 'loading');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						action: "user_events_delete",
						user: user,
						level: JSON.stringify(level),
						type: JSON.stringify(type)
					},
					success: function (data) {
						if(data == "1") {
							userFeedback(btn, 'success');
							setTimeout(() => {
								userFeedback(btn, 'text');
							}, 1500)
						} else {
							userFeedback(btn, 'error');
							setTimeout(() => {
								userFeedback(btn, 'text');
							}, 1500)
						}
					},
					error: function (errorThrown) {
						userFeedback(btn, 'error');
						setTimeout(() => {
							userFeedback(btn, 'text');
						}, 1500)
					}
				});

			} else {

				if(user == "") {
					jQuery('#userEventsDeleteForUser').addClass('is-invalid');
				}
				if(type.length == 0) {
					jQuery('#userEventsDeleteForType').addClass('is-invalid');
				}
				if(level.length == 0) {
					jQuery('#userEventsDeleteForLevel').addClass('is-invalid');
				}

				userFeedback(btn, 'error');
				setTimeout(() => {
					userFeedback(btn, 'text');
				}, 1500)
			}
		});

		jQuery(document).on('change', '#userEventsDeleteForUser, #userEventsDeleteForType, #userEventsDeleteForLevel', function (e) {
			jQuery(this).removeClass('is-invalid');
		})
		// ENDE Settingspage

		// START Future Trainigns
		jQuery(document).on('click', '#adminBackendAssignTrainingsDate', function (e) {
			userFeedback('adminBackendAssignTrainingsDate', 'loading');

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					action: "backend_manually_assign_trainings",
				},
				success: function (data) {
					userFeedback('adminBackendAssignTrainingsDate', 'success');
					setTimeout(() => {
						location.reload();
					}, 1500);
				},
				error: function (errorThrown) {
					userFeedback('adminBackendAssignTrainingsDate', 'error');
					setTimeout(() => {
						userFeedback('adminBackendAssignTrainingsDate', 'text');
					}, 1500);
				}
			});
		})

		jQuery(document).on('click', '.trainingDateAssignNow', function (e) {
			var btn = jQuery(this);
			userFeedback(btn, 'loading');

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					action: "backend_assign_training_date_now",
					id: jQuery(this).attr('data-trainingdateid'),
				},
				success: function (data) {
					if(data === "1") {
						userFeedback(btn, 'success');
						setTimeout(() => {
							location.reload();
						}, 1500);
					} else {
						userFeedback(btn, 'error');
						setTimeout(() => {
							userFeedback(btn, 'text');
						}, 1500);
					}
				},
				error: function (errorThrown) {
					userFeedback(btn, 'error');
					setTimeout(() => {
						userFeedback(btn, 'text');
					}, 1500);
				}
			});
		})
		// ENDE Future Trainings

		// START Datenexport
		jQuery(document).on('click', '#tsFormsSource', function (e) {
			if(jQuery('#tsFormsSource').is(':checked')) {
				jQuery('#dataNoSource').addClass('d-none');
				jQuery('#tsformsDataContainer').removeClass('d-none');
			} else {
				jQuery('#tsformsDataContainer').addClass('d-none');

				if(jQuery('.dataexport-data-element:not(.d-none)').length == 0) {
					jQuery('#dataNoSource').removeClass('d-none');
				}
			}
		});

		jQuery(document).on('click', '#tsFormsSourceModalButton', function (e) {
			jQuery('#tsFormsSourceModal').modal('show');
		});

		jQuery(document).on('click', '#tsFormsMultiSource', function (e) {
			if(jQuery('#tsFormsMultiSource').is(':checked')) {
				jQuery('#dataNoSource').addClass('d-none');
				jQuery('#tsformsMultiDataContainer').removeClass('d-none');
			} else {
				jQuery('#tsformsMultiDataContainer').addClass('d-none');

				if(jQuery('.dataexport-data-element:not(.d-none)').length == 0) {
					jQuery('#dataNoSource').removeClass('d-none');
				}
			}
		});

		jQuery(document).on('click', '#tsFormsMultiSourceModalButton', function (e) {
			jQuery('#tsFormsMultiSourceModal').modal('show');
		});

		jQuery(document).on('click', '#systemstatisticSource', function (e) {
			if(jQuery('#systemstatisticSource').is(':checked')) {
				jQuery('#dataNoSource').addClass('d-none');
				jQuery('#systemstatisticDataContainer').removeClass('d-none');
			} else {
				jQuery('#systemstatisticDataContainer').addClass('d-none');

				if(jQuery('.dataexport-data-element:not(.d-none)').length == 0) {
					jQuery('#dataNoSource').removeClass('d-none');
				}
			}
		});

		jQuery(document).on('click', '#systemstatisticSourceModalButton', function (e) {
			jQuery('#systemstatisticSourceModal').modal('show');
		});

		// Start TS Forms Single Source
		jQuery(document).on("change paste keyup", "#dataexport-tsforms-search", function(e) {
			setTimeout(function(){ 
				filterDataExportTsForms();	
			}, 300);
		  });

		jQuery(document).on('click', '#dataexport-tsforms-clear-search-button', function (e) {
			jQuery('#dataexport-tsforms-search').val('');
			filterDataExportTsForms();	
		});

		function filterDataExportTsForms() {
			var tmps = ""+jQuery('#dataexport-tsforms-search').val().toLowerCase();
			
			jQuery('#dataexport-tsforms-check div').removeClass("d-flex").addClass("d-none");
	
			jQuery('#dataexport-tsforms-check div label').each(function (e) {
				if(jQuery(this).html().toLowerCase().indexOf(tmps) >= 0) {
					jQuery(this).parent().removeClass("d-none").addClass("d-flex");
				}
			});
			dataExportCountTsForms();
		}

		jQuery(document).on('click', '#dataexport-tsforms-select-button', function (e) {
			jQuery('#dataexport-tsforms-check div').each(function (e) {
				if(!jQuery(this).hasClass('d-none')) {
					jQuery(this).find('input').prop('checked', true); 
				}
			});
			dataExportCountTsForms();
		})

		jQuery(document).on('click', '#dataexport-tsforms-remove-button', function (e) {
			jQuery('#dataexport-tsforms-check div').each(function (e) {
				if(!jQuery(this).hasClass('d-none')) {
					jQuery(this).find('input').prop('checked', false); 
				}
			});
			dataExportCountTsForms();
		})

		jQuery(document).on('click', '.dataexport-tsforms-formselect', function (e) {
			dataExportCountTsForms();
		})

		function dataExportCountTsForms() {
			var i = 0;
			jQuery('#dataexport-tsforms-check div').each(function (e) {
				if(!jQuery(this).hasClass('d-none')) {
					i++;
				}
			});

			var j = 0;

			jQuery('#dataexport-tsforms-check div').each(function (e) {
				if(jQuery(this).find('input').is(':checked')) {
					j++;
				}
			});

			if(i == 0) {
				jQuery('#dataexport-tsforms-count').html('keine');
			} else {
				jQuery('#dataexport-tsforms-count').html(i);
			}
			if(j == 0) {
				jQuery('#dataexport-tsforms-selected').html('keinen');
			} else {
				jQuery('#dataexport-tsforms-selected').html(j);
			}
		}

		jQuery(document).on('click', '#dataexport-tsforms-filter-button', function (e) {
			jQuery('#dataexportFormsType').val('tsforms');
			jQuery('#dataexportFormsTarget').val('single');
			jQuery('#dataExportFormsFilterModalLabel').html("TS Forms nach Training und/oder Lektion filtern");
			jQuery('#dataExportFormsFilterModal').modal('show');
		})
		// ------------------------------------------------------------------

		// Start TS Forms Multi Source
		jQuery(document).on("change paste keyup", "#dataexport-tsforms-multi-search", function(e) {
			setTimeout(function(){ 
				filterDataExportTsFormsMulti();	
			}, 300);
		  });

		jQuery(document).on('click', '#dataexport-tsforms-multi-clear-search-button', function (e) {
			jQuery('#dataexport-tsforms-multi-search').val('');
			filterDataExportTsFormsMulti();	
		});

		function filterDataExportTsFormsMulti() {
			var tmps = ""+jQuery('#dataexport-tsforms-multi-search').val().toLowerCase();
			
			jQuery('#dataexport-tsforms-multi-check div').removeClass("d-flex").addClass("d-none");
	
			jQuery('#dataexport-tsforms-multi-check div label').each(function (e) {
				if(jQuery(this).html().toLowerCase().indexOf(tmps) >= 0) {
					jQuery(this).parent().removeClass("d-none").addClass("d-flex");
				}
			});
			dataExportCountTsFormsMulti();
		}

		jQuery(document).on('click', '#dataexport-tsforms-multi-select-button', function (e) {
			jQuery('#dataexport-tsforms-multi-check div').each(function (e) {
				if(!jQuery(this).hasClass('d-none')) {
					jQuery(this).find('input').prop('checked', true); 
				}
			});
			dataExportCountTsFormsMulti();
		})

		jQuery(document).on('click', '#dataexport-tsforms-multi-remove-button', function (e) {
			jQuery('#dataexport-tsforms-multi-check div').each(function (e) {
				if(!jQuery(this).hasClass('d-none')) {
					jQuery(this).find('input').prop('checked', false); 
				}
			});
			dataExportCountTsFormsMulti();
		})

		jQuery(document).on('click', '.dataexport-tsforms-multi-formselect', function (e) {
			dataExportCountTsFormsMulti();
		})

		function dataExportCountTsFormsMulti() {
			var i = 0;
			jQuery('#dataexport-tsforms-multi-check div').each(function (e) {
				if(!jQuery(this).hasClass('d-none')) {
					i++;
				}
			});

			var j = 0;

			jQuery('#dataexport-tsforms-multi-check div').each(function (e) {
				if(jQuery(this).find('input').is(':checked')) {
					j++;
				}
			});

			if(i == 0) {
				jQuery('#dataexport-tsforms-multi-count').html('keine');
			} else {
				jQuery('#dataexport-tsforms-multi-count').html(i);
			}
			if(j == 0) {
				jQuery('#dataexport-tsforms-multi-selected').html('keinen');
			} else {
				jQuery('#dataexport-tsforms-multi-selected').html(j);
			}
		}

		jQuery(document).on('click', '#dataexport-tsforms-multi-filter-button', function (e) {
			jQuery('#dataexportFormsType').val('tsforms');
			jQuery('#dataexportFormsTarget').val('multi');
			jQuery('#dataExportFormsFilterModalLabel').html("TS Forms nach Training und/oder Lektion filtern");
			jQuery('#dataExportFormsFilterModal').modal('show');
		})
		// ------------------------------------------------------------------

		// Start Systemstatistic Source
		jQuery(document).on("change paste keyup", "#dataexport-systemstatistics-search", function(e) {
			setTimeout(function(){ 
				filterDataExportSystemstatisticsTraining();	
			}, 300);
		  });

		jQuery(document).on('click', '#dataexport-systemstatistics-clear-search-button', function (e) {
			jQuery('#dataexport-systemstatistics-search').val('');
			filterDataExportSystemstatisticsTraining();	
		});

		function filterDataExportSystemstatisticsTraining() {
			var tmps = ""+jQuery('#dataexport-systemstatistics-search').val().toLowerCase();
			
			jQuery('#dataexport-systemstatistics-check div').removeClass("d-flex").addClass("d-none");
	
			jQuery('#dataexport-systemstatistics-check div label').each(function (e) {
				if(jQuery(this).html().toLowerCase().indexOf(tmps) >= 0) {
					jQuery(this).parent().removeClass("d-none").addClass("d-flex");
				}
			});
			dataExportCountSystemstatistics();
		}

		jQuery(document).on('click', '#dataexport-systemstatistics-select-button', function (e) {
			jQuery('#dataexport-systemstatistics-check div').each(function (e) {
				if(!jQuery(this).hasClass('d-none')) {
					jQuery(this).find('input').prop('checked', true); 
				}
			});
			dataExportCountSystemstatistics();
		})

		jQuery(document).on('click', '#dataexport-systemstatistics-remove-button', function (e) {
			jQuery('#dataexport-systemstatistics-check div').each(function (e) {
				if(!jQuery(this).hasClass('d-none')) {
					jQuery(this).find('input').prop('checked', false); 
				}
			});
			dataExportCountSystemstatistics();
		})

		jQuery(document).on('click', '.dataexport-systemstatistics-trainingselect', function (e) {
			dataExportCountSystemstatistics();
		})

		function dataExportCountSystemstatistics() {
			var i = 0;
			jQuery('#dataexport-systemstatistics-check div').each(function (e) {
				if(!jQuery(this).hasClass('d-none')) {
					i++;
				}
			});

			var j = 0;

			jQuery('#dataexport-systemstatistics-check div').each(function (e) {
				if(jQuery(this).find('input').is(':checked')) {
					j++;
				}
			});

			if(i == 0) {
				jQuery('#dataexport-systemstatistics-count').html('keine');
			} else {
				jQuery('#dataexport-systemstatistics-count').html(i);
			}
			if(j == 0) {
				jQuery('#dataexport-systemstatistics-selected').html('keinen');
			} else {
				jQuery('#dataexport-systemstatistics-selected').html(j);
			}
		}
		// ------------------------------------------------------------------

		// START Filter Modal Function
		jQuery(document).on('hidden.bs.modal', '#dataExportFormsFilterModal', function (e) {
			// Reset Modal to default

			jQuery('#dataExportFormsFilterModalLabel').html('');
			jQuery('#dataexportFormsType').val('');
			jQuery('#dataexportFormsTarget').val('');

			// Checkboxes
			jQuery('#dataExportFormsFilterModal input[type=checkbox]').prop('checked', false);
			jQuery('#dataExportFormsFilterModal input[type=checkbox]').prop('indeterminate', false);

			// + Icons in Card-Headers
			jQuery('#dataExportFormsFilterModal button[data-toggle="collapse"]').attr("aria-expanded", false);
			jQuery('#dataExportFormsFilterModal button[data-toggle="collapse"]').first().attr("aria-expanded", true);

			// Collapsing Card-Bodys
			jQuery('#dataExportFormsFilterModal .collapse').removeClass("show");
			jQuery('#dataExportFormsFilterModal .collapse').first().addClass("show");
		})

		jQuery(document).on('click', '.dataExportFormsFilterLektionAll', function (e) {
			var tid = jQuery(this).attr("data-trainingid");

			jQuery("input[data-lektiontrainingid='" + tid + "']").prop("checked", jQuery(this).is(':checked'));
		});

		jQuery(document).on('click', '.dataExportFormsFilterLektion', function (e) {
			var tid = jQuery(this).attr("data-lektiontrainingid");

			if(jQuery("#dataExportFormsFilterModal input[data-lektiontrainingid='" + tid + "']:checked").length == 
				jQuery("#dataExportFormsFilterModal input[data-lektiontrainingid='" + tid + "']").length) {

				jQuery("#dataExportFormsFilterLektionAll" + tid).prop("indeterminate", false);
				jQuery("#dataExportFormsFilterLektionAll" + tid).prop("checked", true);

			} else if(jQuery("#dataExportFormsFilterModal input[data-lektiontrainingid='" + tid + "']:checked").length == 0) {

				jQuery("#dataExportFormsFilterLektionAll" + tid).prop("indeterminate", false);
				jQuery("#dataExportFormsFilterLektionAll" + tid).prop("checked", false);

			} else {

				jQuery("#dataExportFormsFilterLektionAll" + tid).prop("indeterminate", true);
				jQuery("#dataExportFormsFilterLektionAll" + tid).prop("checked", false);
				
			}
		})

		jQuery(document).on('click', '#dataExportFormsFilterSendButton', function (e) {
			var button = jQuery(this);

			var ids = {};
			jQuery('#dataExportFormsFilterModal input[type=checkbox]').each(function (e) {
				if(jQuery(this).attr("data-lektiontrainingid") !== undefined && jQuery(this).is(':checked')) {
					if(ids[jQuery(this).attr("data-lektiontrainingid")] == undefined) {
						ids[jQuery(this).attr("data-lektiontrainingid")] = [];
					}
					ids[jQuery(this).attr("data-lektiontrainingid")].push(jQuery(this).val());
				}
			});
			var type = jQuery('#dataexportFormsType').val();
			var target = jQuery('#dataexportFormsTarget').val();
			
			if(ids != {}) {
				userFeedback(button, 'loading');

				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						ids: ids,
						type: type,
						action: 'dataexport_forms_filter',
					},
					success: function (data) {
						if(data != "0") {
							userFeedback(button, 'success');

							var ids = data.split(";");
							for(var i = 0; i < ids.length; i++) {
								if(target == "single") {
									jQuery('#dataexport-tsforms-form-' + ids[i] + '-checkbox').prop('checked', true);
								} else if(target == "multi") {
									jQuery('#dataexport-tsforms-multi-form-' + ids[i] + '-checkbox').prop('checked', true);
								}
							}
							if(target == "single") {
								dataExportCountTsForms();
							} else if(target == "multi") {
								dataExportCountTsFormsMulti();
							}
	
							setTimeout(() => {
								jQuery('#dataExportFormsFilterModal').modal('hide');
								userFeedback(button, 'text');
							}, 1500);
						} else {
							userFeedback(button, 'error');
	
							setTimeout(() => {
								userFeedback(button, 'text');
							}, 1500);
						}
					},
					error: function (errorThrown) {
						userFeedback(button, 'error');
	
						setTimeout(() => {
							userFeedback(button, 'text');
						}, 1500);
					}
				});

			} else {
				userFeedback(button, 'error');
				setTimeout(() => {
					userFeedback(button, 'text');
				}, 1500);
			}
		})
		// ------------------------------------------------------------------

		// Start User Select
		jQuery(document).on('click', '.dataexport-date-reset', function (e) {
			jQuery(this).parent().parent().find('.dataexport-date-field').val(jQuery(this).parent().parent().find('.dataexport-date-field').attr('data-default'));
			filterDataExportDate();
		})

		jQuery(document).on('change', '.dataexport-date-field', function (e) {
			filterDataExportDate();
		})

		function filterDataExportDate() {
			var startDate = Date.parse(jQuery('#dataexport-startDate').val());
			var endDate = Date.parse(jQuery('#dataexport-endDate').val());

			jQuery('#dataexport-users-check li').removeClass("d-flex").addClass("d-none")
	
			jQuery('#dataexport-users-check li label').each(function (e) {

				var regDate = Date.parse(jQuery(this).parent().find('input').attr('data-registerdate'));
				if(startDate <= regDate && regDate <= endDate) {
					jQuery(this).parent().removeClass("d-none").addClass("d-flex");
				}
			});

			dataExportCountUsers();
		}

		jQuery(document).on("change paste keyup", "#dataexport-users-search", function(e) {
			setTimeout(function(){ 
				filterDataExportUser();	
			}, 300);
		  });

		jQuery(document).on('click', '#dataexport-users-clear-search-button', function (e) {
			jQuery('#dataexport-users-search').val('');
			filterDataExportUser();	
		});

		function filterDataExportUser() {
			var tmps = ""+jQuery('#dataexport-users-search').val().toLowerCase();
			
			jQuery('#dataexport-users-check li').removeClass("d-flex").addClass("d-none")
	
			jQuery('#dataexport-users-check li.dataexport-users-item input').each(function (e) {
				if(jQuery(this).attr("data-usermail").toLowerCase().indexOf(tmps) >= 0 || jQuery(this).attr("data-studienid").toLowerCase().indexOf(tmps) >= 0 || jQuery(this).attr("data-username").toLowerCase().indexOf(tmps) >= 0) {
					jQuery(this).parent().removeClass("d-none").addClass("d-flex");
				}
			});

			jQuery('#dataexport-users-check li.dataexport-users-addstudiengruppe').each(function (e) {
				if(jQuery(this).attr("data-studiengruppe").toLowerCase().indexOf(tmps) >= 0) {
					jQuery(this).removeClass("d-none").addClass("d-flex");
				}
			});

			jQuery('#dataexport-users-check li.dataexport-users-addcompany').each(function (e) {
				if(jQuery(this).attr("data-companyname").toLowerCase().indexOf(tmps) >= 0) {
					jQuery(this).removeClass("d-none").addClass("d-flex");
				}
			});
			dataExportCountUsers();
		}

		jQuery(document).on('click', '#dataexport-users-select-button', function (e) {
			jQuery('#dataexport-users-check li').each(function (e) {
				if(!jQuery(this).hasClass('d-none')) {
					jQuery(this).find('input').prop('checked', true); 
				}
			});
			dataExportCountUsers();
		})

		jQuery(document).on('click', '#dataexport-users-remove-button', function (e) {
			jQuery('#dataexport-users-check li').each(function (e) {
				if(!jQuery(this).hasClass('d-none')) {
					jQuery(this).find('input').prop('checked', false); 
				}
			});
			dataExportCountUsers();
		})

		jQuery(document).on('click', '.dataexport-users-formselect', function (e) {
			dataExportCountUsers();
		})

		jQuery(document).on('click', '#dataexport-users-check li.dataexport-users-addstudiengruppe', function (e) {
			var tn_ids = jQuery(this).attr('data-studiengruppenteilnehmer');

			var ids = tn_ids.split(",");

			for(var i = 0; i < ids.length; i++) {
				if(!jQuery('#dataexport-users-check .dataexport-users-formselect[data-userid="' + ids[i] + '"]').is(':checked')) {
					jQuery('#dataexport-users-check .dataexport-users-formselect[data-userid="' + ids[i] + '"]').click();
				}
			}
			dataExportCountUsers();
		});

		jQuery(document).on('click', '#dataexport-users-check li.dataexport-users-addcompany', function (e) {
			var tn_ids = jQuery(this).attr('data-companymember');

			var ids = tn_ids.split(",");

			for(var i = 0; i < ids.length; i++) {
				if(!jQuery('#dataexport-users-check .dataexport-users-formselect[data-userid="' + ids[i] + '"]').is(':checked')) {
					jQuery('#dataexport-users-check .dataexport-users-formselect[data-userid="' + ids[i] + '"]').click();
				}
			}
			dataExportCountUsers();
		});

		function dataExportCountUsers() {
			var i = 0;
			jQuery('#dataexport-users-check li:not(.dataexport-users-addstudiengruppe):not(.dataexport-users-addcompany)').each(function (e) {
				if(!jQuery(this).hasClass('d-none')) {
					i++;
				}
			});

			var j = 0;

			jQuery('#dataexport-users-check li:not(.dataexport-users-addstudiengruppe):not(.dataexport-users-addcompany)').each(function (e) {
				if(jQuery(this).find('input').is(':checked')) {
					j++;
				}
			});

			if(i == 0) {
				jQuery('#dataexport-users-count').html('keine');
			} else {
				jQuery('#dataexport-users-count').html(i);
			}
			if(j == 0) {
				jQuery('#dataexport-users-selected').html('keinen');
			} else {
				jQuery('#dataexport-users-selected').html(j);
			}
		}
		// ------------------------------------------------------------------

		jQuery(document).on('click', '#dataexport-start', function (e) {
			jQuery('#dataExportLoading').modal('show');
			userFeedback('dataexport-start', 'loading');
			jQuery('#dataexport-error-alert').addClass("d-none");

			var data = {};
			data["studyid"] = jQuery('#dataexport-option-studyid').is(':checked');
			data["studygroupid"] = jQuery('#dataexport-option-studygroupid').is(':checked');
			data["groupdata"] = jQuery('#dataexport-option-groupdata').is(':checked');
			data["cleanheader"] = jQuery('#dataexport-option-cleanheader').is(':checked');
			
			if(jQuery('#tsFormsSource').is(':checked')) {
				data['tsforms'] = [];
				jQuery('.dataexport-tsforms-formselect').each(function (e) {
					if(jQuery(this).is(':checked')) {
						data['tsforms'].push(jQuery(this).val());
					}
				});
			}
			if(jQuery('#tsFormsMultiSource').is(':checked')) {
				data['tsformsmulti'] = [];
				jQuery('.dataexport-tsforms-multi-formselect').each(function (e) {
					if(jQuery(this).is(':checked')) {
						data['tsformsmulti'].push(jQuery(this).val());
					}
				});
			}
			if(jQuery('#systemstatisticSource').is(':checked')) {
				data['systemstatistics'] = {};
				data['systemstatistics']['statistics'] = [];
				data['systemstatistics']['trainings'] = [];

				jQuery('.dataexport-systemstatistics-trainingselect').each(function (e) {
					if(jQuery(this).is(':checked')) {
						data['systemstatistics']['trainings'].push(jQuery(this).val());
					}
				});

				jQuery('.dataexport-systemstatistics-statistic').each(function (e) {
					if(jQuery(this).is(':checked')) {
						data['systemstatistics']['statistics'].push(jQuery(this).val());
					}
				});
			}

			var users = [];
			jQuery('.dataexport-users-formselect').each(function () {
				if(jQuery(this).is(':checked')) {
					users.push(jQuery(this).val());
				}
			});

			var xhr = new XMLHttpRequest();
			xhr.open('POST', ajaxurl, true);
			xhr.responseType = 'arraybuffer';
			xhr.onload = function () {
					$('#dataExportLoading').modal('hide');
					userFeedback('dataexport-start', 'success');
					setTimeout(() => {
						userFeedback('dataexport-start', 'text');
					}, 1500);

					if (this.status === 200) {
						if(this.response.byteLength > 1) {
							var filename = "";
							var disposition = xhr.getResponseHeader('Content-Disposition');
							if (disposition && disposition.indexOf('attachment') !== -1) {
									var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
									var matches = filenameRegex.exec(disposition);
									if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
							}
							var type = xhr.getResponseHeader('Content-Type');

							var blob = typeof File === 'function'
									? new File([this.response], filename, { type: type })
									: new Blob([this.response], { type: type });
							if (typeof window.navigator.msSaveBlob !== 'undefined') {
									// IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
									window.navigator.msSaveBlob(blob, filename);
							} else {
									var URL = window.URL || window.webkitURL;
									var downloadUrl = URL.createObjectURL(blob);

									if (filename) {
											// use HTML5 a[download] attribute to specify filename
											var a = document.createElement("a");
											// safari doesn't support this yet
											if (typeof a.download === 'undefined') {
													window.location = downloadUrl;
											} else {
													a.href = downloadUrl;
													a.download = filename;
													document.body.appendChild(a);
													a.click();
											}
									} else {
											window.location = downloadUrl;
									}

									setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
							}
						} else {
							userFeedback('dataexport-start', 'error');
							jQuery('#dataexport-error-alert').removeClass("d-none");
							setTimeout(() => {
								userFeedback('dataexport-start', 'text');
							}, 1500);
						}
					} else {
						userFeedback('dataexport-start', 'error');
						jQuery('#dataexport-error-alert').removeClass("d-none");
						setTimeout(() => {
							userFeedback('dataexport-start', 'text');
						}, 1500);
					}
			};
			xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			xhr.send("action=dataexport_export&users="+JSON.stringify(users)+"&data=" + JSON.stringify(data));
		});
		// ENDE Datenexport

		// Start Nearest Locations
		jQuery(document).on('keyup', '#nearest-location-plz', function (e) {
			if(jQuery(this).val().length == 5) {
				jQuery('#nearest-location-submit').click();
				jQuery(this).blur();
			}
		})

		jQuery(document).on('click', '#nearest-location-submit', function (e) {
			userFeedback('nearest-location-submit', 'loading');
			jQuery('#nearest-location-plz').prop("disabled", true);

			jQuery("#nearest-locations-map").trigger('update', {
				deletePlotKeys: ['location']
			});

			var plz = jQuery('#nearest-location-plz').val();

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					action: 'get_nearest_location',
					plz: plz,
					locations: jQuery('#nearest-locations-json').val(),
					limit: jQuery('#nearest-locations-limit').val(),
					subject: jQuery('#nearest-locations-subject').val(),
					showTooltips: jQuery('#nearest-locations-showTooltips').val(),
				},
				success: function (data) {
					userFeedback('nearest-location-submit', 'success');
					jQuery('#nearest-location-plz').prop("disabled", false);

					jQuery('#nearest-locations-area').html(data);

					let showTooltips = jQuery('#nearest-locations-showTooltips').val();

					if(jQuery('#nearest-locations-area .alert-danger').length == 0) {

						let plotsToDelete = [];
						for(let i = 0; i < Object.keys(jQuery("#nearest-locations-map").data('mapael').options.plots).length; i++) {
							plotsToDelete.push(i);
						}

						jQuery("#nearest-locations-map").trigger('update', {
							deletePlotKeys: plotsToDelete, // weirdly 'all' does not work here
						});
						
						var i = 0;
						var obj = {};
						jQuery('#nearest-locations-list').find('li.list-group-item').each(function (e) {
							obj[i] = {
								type: "circle",
								size: 25,
								latitude: jQuery(this).attr('data-lat'),
								longitude: jQuery(this).attr('data-long'),
							}

							if(showTooltips) {
								obj[i]['tooltip'] = {
									content: jQuery(this).find('.nearest-location-item-name').html().replace("<br>", " "),
								};
							} else {
								obj[i]['text'] = {
									content: jQuery(this).find('.nearest-location-item-name').html().replace("<br>", " "),
									position: "top",
									attrs: {
										"font-size": 16,
										"font-weight": "bold",
										fill: "#000000"
									},
									attrsHover: {
										"font-size": 16,
										"font-weight": "bold",
										fill: "#000000",
									}
								};
								obj[i]['attrs'] = {
									opacity: 1
								};
								obj[i]['attrsHover'] = {
									transform: "s1.5",
								};
							}
							i++;
						});

						obj[i] = {
							type: "circle",
							size: 25,
							latitude: jQuery('#nearest-location-city').attr('data-lat'),
							longitude: jQuery('#nearest-location-city').attr('data-long'),
							text: {
								content: "⦿",
								position: "inner",
								attrs: {
									"font-size": 16,
									"font-weight": "bold",
									fill: "#000"
								},
							},
							tooltip: { content: document.getElementById("nearest-locations-myLocationTooltip").value},
							attrs: {
								opacity: 1,
								fill: '#FFB403'
							},
						};

						jQuery("#nearest-locations-map").trigger('update', {
							newPlots: obj,
						});
					}

					setTimeout(() => {
						userFeedback('nearest-location-submit', 'text');
					}, 1500);
				},
				error: function (errorThrown) {
					userFeedback('nearest-location-submit', 'error');
					jQuery('#nearest-location-plz').prop("disabled", false);

					setTimeout(() => {
						userFeedback('nearest-location-submit', 'text');
					}, 1500);
				}
			});
		})

        // START TODO-LIST
    
        // ADD TODO
        jQuery(document).on('click', '#add_todo_btn', function (e) {
            e.preventDefault();
            var button = jQuery(this);
            userFeedback(button, 'loading');


            if(jQuery(this).parent().prev().children().attr('id') != 'add_todo_input'){
              var todo = jQuery(this).prev(".systemTodo").html();
			  var posttype = $(this).attr("data-post");
              var mode = posttype;
            }else{
              var todo = jQuery("#add_todo_input").val();
              var mode = 'mytodo'
            }

            jQuery.ajax({
                type: "POST",
                url: ajaxurl,
                data: {
                "action": 'add_todo',
                "todo": todo,
                "mode": mode,
            },
            success: function (data) {
                userFeedback(button, 'success');
                todoId = data/10
                if(mode == 'systemtodo'){
                  jQuery('.system-todo')
                .prepend('<li class="list-group-item border-bottom pb-0" data-id="'+todoId +'"><div class="form-check mb-3 ml-3 d-flex align-items-center" style="justify-content: space-between;"><input class="form-check-input update-todo" type="checkbox" value="" id="id'+todoId +'"><label class="form-check-label ml-2" for="id'+todoId +'" >'+ todo + '</label><i class="remove-todo fas fa-trash-alt mr-3"></i></div></li>')
                jQuery("#add_todo_input").val("");
                }else{
                jQuery('.my-todo')
                .prepend('<li class="list-group-item border-bottom pb-0" data-id="'+todoId +'"><div class="form-check mb-3 ml-3 d-flex align-items-center" style="justify-content: space-between;"><input class="form-check-input update-todo" type="checkbox" value="" id="id'+todoId +'"><label class="form-check-label ml-2" for="id'+todoId +'" >'+ todo + '</label><i class="remove-todo fas fa-trash-alt mr-3"></i></div></li>')
                jQuery("#add_todo_input").val("");
                }
                userFeedback(button, 'text');
            },
            });   
        }); 

        //DELETE TODO
        jQuery(document).on('click', '.remove-todo', function (e) {
            e.preventDefault();
            var button = jQuery(this);
            userFeedback(button, 'loading');
			button.removeClass( "fas fa-trash-alt" )
            var dataId = jQuery(this).parent().parent().attr("data-id");
            var li = jQuery(this).parent().parent();
            jQuery.ajax({
                type: "POST",
                url: ajaxurl,
                data: {
                "action": 'del_todo',
                "dataId" : dataId,
            },
            success: function (data) {
                userFeedback(button, 'success');
                li.remove();
                userFeedback(button, 'text');
            },
            });   
        }); 

        //UPDATE TODO CHECKBOX
        jQuery(document).on('click', '.update-todo', function (e) {
            // var button = jQuery(this);
            var dataId = jQuery(this).parent().parent().attr("data-id");
            console.log("DATAID :" + dataId)
            
            if (!jQuery(this).is(":checked")) {

                console.log("JA CHECKED")
            // do something if the checkbox is NOT checked
                var isChecked = 0;
            }
            else{
                var isChecked = 1;

            }

        jQuery.ajax({
                type: "POST",
                url: ajaxurl,
                data: {
                "action": 'update_todo',
                "dataId" : dataId,
                "isChecked" : isChecked,
            },
            success: function (data) {
                //do nothing
            },
            });    
        }); 
        //END TODO LIST

		// START Admin Backend App Audio ----------------------------------------------
		jQuery(document).on('click', '.backend-admin-app-audio-row-delete' , function (e) {
			jQuery('#lektionId').val(jQuery(this).attr('data-traininglektion'));
			jQuery('#audioId').val(jQuery(this).attr('data-audio'));
			jQuery('#action').val('del');

			jQuery('#backend-admin-app-audio-submit').click();

			userFeedback(jQuery(this), 'loading');
			userFeedback('backend-admin-app-audio-submit', 'loading');
		})
		// END Admin Backend App Audio ----------------------------------------------

		jQuery(document).on('click', '#search_user_btn', function (e) {
			userFeedback('search_user_btn', 'loading');
		});

		jQuery(document).on('click', '#close_search_user_btn', function (e) {
			userFeedback('close_search_user_btn', 'loading');
		});

		function userFeedback(id, state) {
			if(jQuery.type(id) === "string") {
				var button = jQuery('#'+id);
			} else if(jQuery.type(id) === "object") {
				var button = id;
			}
			var spans = button.find("span.userfeedback-text");
			if(spans.length == 0) {
				var text = button.html();
				button.css("width", button.outerWidth()+2 +'px');
				button.css("height", button.outerHeight() +'px');
				button.html('<span class="userfeedback-span userfeedback-text d-flex align-items-center justify-content-center">'+text+'</span><span class="d-none userfeedback-span userfeedback-loading align-items-center justify-content-center"><i class="fas fa-spinner"></i></span><span class="d-none userfeedback-span userfeedback-success align-items-center justify-content-center"><i class="far fa-check-circle"></i></span><span class="d-none userfeedback-span userfeedback-error align-items-center justify-content-center"><i class="far fa-times-circle"></i></span>');
			}
			if(state == 'loading') {
				button.attr("disabled", "disabled");
				button.find("span").each(function()  {
					if(jQuery(this).hasClass('userfeedback-span') && jQuery(this).hasClass('userfeedback-loading')) {
						jQuery(this).removeClass('d-none').addClass('d-flex');
					} else if(jQuery(this).hasClass('userfeedback-span')) {
						jQuery(this).removeClass('d-flex').addClass('d-none');
					}
				});
			}
			if(state == 'error') {
				button.removeAttr("disabled");
				button.find("span").each(function() {
					if(jQuery(this).hasClass('userfeedback-span') && jQuery(this).hasClass('userfeedback-error')) {
						jQuery(this).removeClass('d-none').addClass('d-flex');
					} else if(jQuery(this).hasClass('userfeedback-span')) {
						jQuery(this).removeClass('d-flex').addClass('d-none');
					}
				});
			}
			if(state == 'success') {
				button.removeAttr("disabled");
				button.find("span").each(function() {
					if(jQuery(this).hasClass('userfeedback-span') && jQuery(this).hasClass('userfeedback-success')) {
						jQuery(this).removeClass('d-none').addClass('d-flex');
					} else if(jQuery(this).hasClass('userfeedback-span')) {
						jQuery(this).removeClass('d-flex').addClass('d-none');
					}
				});
			}
			if(state == 'text') {
				button.removeAttr("disabled");
				button.find("span").each(function() {
					if(jQuery(this).hasClass('userfeedback-span') && jQuery(this).hasClass('userfeedback-text')) {
						jQuery(this).removeClass('d-none').addClass('d-flex');
					} else if(jQuery(this).hasClass('userfeedback-span')) {
						jQuery(this).removeClass('d-flex').addClass('d-none');
					}
				});
			}
		}

		function validateMail(mail) {
			if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,24})+$/.test(mail))
			{
				return true;
			} else {
				return false;
			}
		}

		// Force reload when back-button of browser is used.
		window.addEventListener("pageshow", function(evt){
			if(evt.persisted){
				setTimeout(function(){
					window.location.reload();
				},10);
			}
		}, false);

		jQuery(document).on('click', '.trainingssystem-demo-login-btn', function (e) {
				e.preventDefault();
				var button = jQuery(this);
				jQuery(button).parent().find('.trainingssystem-demo-login-fulltext').addClass('d-none');
				userFeedback(button, 'loading');

				var href = $(this).attr("data-link");
				var userid = $(this).attr("data-id");
				var mode = $(this).attr("data-mode");

				jQuery.ajax({
					type: "POST",
					url: ajaxurl,
					data: {
					"action": 'demo_user',
					"userid" : userid,
					"mode": mode,
				},
				success: function (data) {
					if(data == "1") {
						userFeedback(button, 'success');
						window.location.href = href;
					} else {
						userFeedback(button, 'error');
						if(data == "full") {
							jQuery(button).parent().find('.trainingssystem-demo-login-fulltext').removeClass('d-none');
						}
						setTimeout(() => {
							userFeedback(button, 'text');
						}, 1500);
					}
				},
				error: function (e) {
					userFeedback(button, 'error');
					setTimeout(() => {
						userFeedback(button, 'text');
					}, 1500);
				}});   
		});

		jQuery(document).on('keyup', '.demologinAccountTakeoverInput', function (e) {
			if(e.keyCode == 13) {
				jQuery(this).blur();
				jQuery(this).parent().parent().parent().find('.demologinAccountTakeoverConfirm').click();
			} else if(jQuery(this).attr('type') == "email" && jQuery(this).val().trim() != "" && validateMail(jQuery(this).val())) {
				jQuery(this).removeClass('is-invalid');
			} else if(jQuery(this).attr('type') == "text" && jQuery(this).val().trim() != "") {
				jQuery(this).removeClass('is-invalid');
			}
		})

		jQuery(document).on('click', '.demologinAccountTakeoverConfirm', function (e) {
			var button = jQuery(this);

			jQuery(this).parent().parent().find('.demologinAccountTakeoverUsername').removeClass('is-invalid');
			jQuery(this).parent().parent().find('.demologinAccountTakeoverUsername').first().parent().find('.text-danger').first().addClass('d-none');
			jQuery(this).parent().parent().find('.demologinAccountTakeoverMail').removeClass('is-invalid');
			jQuery(this).parent().parent().find('.demologinAccountTakeoverMail').first().parent().find('.text-danger').first().addClass('d-none');
			
			var username = jQuery(this).parent().parent().find('.demologinAccountTakeoverUsername').first().val();
			var mail = jQuery(this).parent().parent().find('.demologinAccountTakeoverMail').first().val();

			if(username.trim() != "" && mail.trim() != "" && validateMail(mail)) {
				userFeedback(button, 'loading');

				jQuery.ajax({
					type: "POST",
					url: ajaxurl,
					data: {
						"action": 'demo_user_takeover',
						"username" : username,
						"mail": mail,
					},
				success: function (data) {
					if(data == "1") {
						userFeedback(button, 'success');
						jQuery(button).attr("disabled", true);
						jQuery(button).parent().parent().find('.demologinAccountTakeoverSuccess').removeClass('d-none');
					} else {
						userFeedback(button, 'error');
						if(data == "username") {
							jQuery(button).parent().parent().find('.demologinAccountTakeoverUsername').first().parent().find('.text-danger').first().removeClass('d-none');
						}
						if(data == "mail") {
							jQuery(button).parent().parent().find('.demologinAccountTakeoverMail').first().parent().find('.text-danger').first().removeClass('d-none');
						}
						setTimeout(() => {
							userFeedback(button, 'text');
						}, 1500);
					}
				},
				error: function (e) {
					userFeedback(button, 'error');
					setTimeout(() => {
						userFeedback(button, 'text');
					}, 1500);
				}});   
			} else {
				userFeedback(button, 'error');

				if(username.trim() == "") {
					jQuery(this).parent().parent().find('.demologinAccountTakeoverUsername').addClass('is-invalid');
				}

				if(mail.trim() == ""  || !validateMail(mail)) {
					jQuery(this).parent().parent().find('.demologinAccountTakeoverMail').addClass('is-invalid');
				}

				setTimeout(() => {
					userFeedback(button, 'text');
				}, 1500);
			}
		})


		// START - DEMODATEN ANLEGEN
		function applyDemoData(type, button, feeback) {
			var noncefield = jQuery("#trainingssystem-plugin-v2createdemodata").val();
			var dataArr = [];
			var saveButton = button;
			data = {};
			var checkedPages = [];
			var pageoverride = 0;
			if (jQuery('#pageoverride').is(':checked')) {
				pageoverride = 1;
				console.log(pageoverride)
			}
			userFeedback(saveButton, 'loading');
			jQuery('.standard_pages input:checkbox:checked').each(function () {
				checkedPages.push(jQuery(this).val());
			});
			data['action'] = "backend_admin_create";
			data['noncefield'] = noncefield;
			data[type] = type;
			data['pageoverride'] = pageoverride;
			data['checkedPages'] = checkedPages;

			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				success: function (data) {
					dataArr = data.split('<br>');
					userFeedback(saveButton, 'success');
					setTimeout(() => {
						userFeedback(saveButton, 'text');
					}, 1500)
					if (type == "insert_demo_data") {
						if (data != 0) {
							jQuery(feeback)
								.append('<div class="alert-success mt-3"><i class="fas fa-check-circle"></i>&nbsp;Demo-Training wurde hinzugefügt!</div>');
						} else {
							jQuery(feeback)
								.append('<div class="alert-danger mt-3"><i class="fas fa-times-circle"></i>&nbsp;Demo-Training existiert bereits!</div>');
						}
					} else {
						if (type == "insert_demo_pages_single") {
							jQuery(feeback).empty();
						}
						for (var i = 0; i < dataArr.length; i++) {

							if (dataArr[i].includes('nicht geändert')) {
								jQuery(feeback)
									.append('<div class="alert-danger mt-3"><i class="fas fa-times-circle"></i> ' + dataArr[i] + '</div>');
							}
							else {
								if (dataArr[i] != 0) {
									jQuery(feeback)
										.append('<div class="alert-success mt-3"><i class="fas fa-check-circle"></i> ' + dataArr[i] + '</div>');
								}
							}
						}
					}
				},
				error: function (errorThrown) {
					userFeedback(saveButton, 'error');
					setTimeout(() => {
						userFeedback(saveButton, 'text');
					}, 1500)
				}
			});
		}
		//Demo Training (Wiki) 
		jQuery(document).one('click', '#insert_demo_data_button', function (e) {
			applyDemoData("insert_demo_data", jQuery(this), "#insert_demo_data_form");
		});
		//Standardseiten alle
		jQuery(document).one('click', '#insert_demo_pages_button', function (e) {
			applyDemoData("insert_demo_pages", jQuery(this), "#demo_anlegen_feedback");
		});
		//Standardseiten single
		jQuery(document).on('click', '#insert_demo_pages_single_button', function (e) {
			applyDemoData("insert_demo_pages_single", jQuery(this), "#demo_anlegen_feedback");
		});
        // ENDE - DEMODATEN ANLEGEN
		
		// AUTO-LOGOUT
		var userlogged = $("#inactive-modal").attr("data-userlogged");
		var isLoggedOut = 0;

		if (userlogged == 1){
    
			var ajaxurl = typeof trainingssystem_plugin_v2_ajax !== 'undefined' ? trainingssystem_plugin_v2_ajax.trainingssystem_plugin_v2_ajax_url : null;
			var logouttime = typeof trainingssystem_plugin_v2_ajax !== 'undefined' ? trainingssystem_plugin_v2_ajax.trainingssystem_plugin_v2_logout_time : 900000;  //900000ms = 15min
			
			var inactive_logout_time = 0;
			var inactive_logout_tabID;
			var message = 0;

			inactive_logout_tabID = sessionStorage.inactive_logout_tabID && sessionStorage.closedLastTab !== '2' ? sessionStorage.inactive_logout_tabID : sessionStorage.inactive_logout_tabID = Math.random();
			localStorage.setItem("browserTabID", inactive_logout_tabID);
			
			jQuery(document).on("mousemove", resetTimer.bind(this));
			jQuery(document).on("mousedown", resetTimer.bind(this));
			jQuery(document).on("keydown", resetTimer.bind(this));
			jQuery(document).on("DOMMouseScroll", resetTimer.bind(this));
			jQuery(document).on("mousewheel", resetTimer.bind(this));
			jQuery(document).on("touchmove", resetTimer.bind(this));
			jQuery(document).on("MSPointerMove", resetTimer.bind(this));
			startTimer();

			function startTimer() {
				inactive_logout_time = setTimeout(inactiveModal, logouttime-30000);
			}

			function resetTimer() {
				clearTimeout(inactive_logout_time); 

				localStorage.setItem("browserTabID", inactive_logout_tabID);
				try {
					startTimer();
				} catch (e) {
					if (e instanceof TypeError) {
						console.log(e, true);
					} else {
					console.log(e, true);
					}
				}
			}
		
			function inactiveModal() {
				var countdown = 60;
				if (message === 0) {
					message = 1;
					var browserTabID = localStorage.getItem("browserTabID");
					if (parseFloat(browserTabID) === parseFloat(inactive_logout_tabID)) {
						jQuery('#inactive-modal').removeAttr('hidden');
						jQuery('#inactive-modal').modal('show');

						refreshIntervalId = setInterval(function(){ 
									if (countdown >= 0) {
										timeleft = countdown--;
										jQuery("#continue-session .countdown").html(' (' + timeleft + ')');
										if(timeleft== 0) {
											logout_now();
										}
									}
									}, 1000);
									jQuery('#inactive-modal').on('hide.bs.modal', function (e) {        
										clearInterval(refreshIntervalId);
										message = 0;
						});
					}
				}           
			}
			function logout_now() { 
				saveTsForms('continue-session-done', null, null, false);
				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "logout_session",
					},
					success: function (data) {
						localStorage.setItem("isLoggedOut", 1);
						location.reload();
					}
				});     
			}
		} else {
			isLoggedOut = localStorage.getItem("isLoggedOut");

			if(isLoggedOut==1){
				localStorage.setItem("isLoggedOut", 0);
					jQuery('#logged-out-modal').removeAttr('hidden');
					jQuery('#logged-out-modal').modal('show');
			}
		}
    });
})(jQuery);


/**
 * BLOCK: Basic
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 *
 * Styles:
 *        editor.css — Editor styles for the block.
 *        style.css  — Editor & Front end styles for the block.
 */
(function() {
  var __ = wp.i18n.__; // The __() for internationalization.
  var el = wp.element.createElement; // The wp.element.createElement() function to create elements.
  var registerBlockType = wp.blocks.registerBlockType; // The registerBlockType() to register blocks.

  // const {
  //     RichText,
  //     AlignmentToolbar,
  //     BlockControls,
  //     InspectorControls,
  //     TextControl
  // } = wp.editor;
  var RichText = wp.editor.RichText;

  registerBlockType('gb/trainingssystem-block-accordion', { // Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
    title: 'Accordion', // Block title.
    icon: 'welcome-add-page', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
    category: 'md-trainingssystem-9872', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
    attributes: {
        headline : {
            type:   'string',
        },
        content : {
            type:   'string',
        }
    },
    edit(props) {
        var attributes = props.attributes;

         return el(
                    'div',
                    { className: props.className },
                    el( RichText, {
                        tagName: 'h2',
                        inline: true,
                        placeholder: 'Überschrift',
                        value: attributes.headline,
                        onChange: function( value ) {
                            props.setAttributes( { headline: value } );
                        },
                    } ),
                    el( RichText, {
                        tagName: 'p',
                        inline: true,
                        placeholder: 'Inhalt',
                        value: attributes.content,
                        onChange: function( value ) {
                            props.setAttributes( { content: value } );
                        },
                    } ),
         );
    },
    save({attributes, className}) {
        //gutenberg will save attributes we can use in server-side callback
       return null;
    },
  });

})();

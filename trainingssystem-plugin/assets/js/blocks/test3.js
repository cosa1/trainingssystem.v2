console.log("test blocks3");
/**
 * BLOCK: Basic
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 *
 * Styles:
 *        editor.css — Editor styles for the block.
 *        style.css  — Editor & Front end styles for the block.
 */
(function() {
  var __ = wp.i18n.__; // The __() for internationalization.
  var el = wp.element.createElement; // The wp.element.createElement() function to create elements.
  var registerBlockType = wp.blocks.registerBlockType; // The registerBlockType() to register blocks.

  // const {
  //     RichText,
  //     AlignmentToolbar,
  //     BlockControls,
  //     InspectorControls,
  //     TextControl
  // } = wp.editor;
  var RichText = wp.editor.RichText;

  registerBlockType('gb/trainingssystem-block-test3', { // Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
    title: 'Test3', // Block title.
    icon: 'shield-alt', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
    category: 'md-trainingssystem-9872', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
    attributes: {
        images : {
            default: [],
            type:   'array',

        },
        content : {
            default: "kein content",
            type:   'string',

        }
    },
    edit({attributes, setAttributes, className, focus, id}) {
      var content =attributes.content;
      console.log(attributes);

         function onChangeContent( newContent ) {
             setAttributes( { content: newContent,images:["img1","img2"],textblock: newContent} );
         }

         return el(RichText,
             {
                 tagName: 'p',
                 className: className,
                 onChange: onChangeContent,
                 value: content,
             }
         );

    },
    save({attributes, className}) {
        //gutenberg will save attributes we can use in server-side callback
       return null;
    },
  });

})();

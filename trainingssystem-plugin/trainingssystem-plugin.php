<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              -
 * @since             1.0.0
 * @package           Trainingssystem_Plugin
 *
 * @wordpress-plugin
 * Plugin Name:       TrainOn
 * Plugin URI:        https://www.train-on.net
 * Description:       Dieses Plugin erweitert WordPress um ein Lernmanagementsystem. Es können Online-Trainings erstellt und in Lektionen und Seiten gegliedert werden. Daneben stehen zahlreiche weitere Funktionen, u.a. zum E-Coaching und für die Nutzerinteraktion zur Verfügung.
 * Version:           2.5.3.2
 * Author:            Tim Mallwitz, Hamid Mergan
 * Author URI:        https://www.train-on.net
 * Text Domain:       trainingssystem-plugin-v2
 * Domain Path:       /languages
 * License:           GNU/GPLv3
 */

require_once plugin_dir_path(__FILE__) . 'vendor/autoload.php';

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

function constants()
{
		$pluginname = 'TRAININGSSYSTEM_PLUGIN_';
		define($pluginname.'SLUG','trainingssystem-plugin-v2');//TRAININGSSYSTEM_PLUGIN_SLUG

		define($pluginname.'DBUPDATE','tspv2_');//TRAININGSSYSTEM_PLUGIN_DBUPDATE

		define($pluginname.'SHORTCODE_PREFIX','tspv2_');//TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX
		define($pluginname.'WIDGET_PREFIX','tspv2_');//TRAININGSSYSTEM_PLUGIN_WIDGET_PREFIX

		define($pluginname.'SETTINGS','tspv2settings');//TRAININGSSYSTEM_PLUGIN_SETTINGS
		define($pluginname.'USER_HAS_ROLES','tspv2UserHasRoles');//TRAININGSSYSTEM_PLUGIN_USER_HAS_ROLES

		define($pluginname.'THEME_SLUG','coachlightv2');//TRAININGSSYSTEM_PLUGIN_THEME_SLUG

		define($pluginname.'DB_VERSION', '1.0'); //TRAININGSSYSTEM_PLUGIN_DB_VERSION

		define($pluginname.'ACTION_PREFIX','tspv2_');//TRAININGSSYSTEM_PLUGIN_ACTION_PREFIX
		define($pluginname.'ACTION_LEKTION_FIN',TRAININGSSYSTEM_PLUGIN_ACTION_PREFIX."lektion_fin");//TRAININGSSYSTEM_PLUGIN_ACTION_LEKTION_FIN
		define($pluginname.'ACTION_TRAINING_FIN',TRAININGSSYSTEM_PLUGIN_ACTION_PREFIX."training_fin");//TRAININGSSYSTEM_PLUGIN_ACTION_TRAINING_FIN

		/*für V1 compatiblität*/
		define($pluginname.'DB_PREFIX', 'md_trainingsmgr_'); //TRAININGSSYSTEM_PLUGIN_DB_PREFIX ->md_trainingsmgr_

		define($pluginname.'DB_TRAINING_LASTLOGIN',TRAININGSSYSTEM_PLUGIN_DB_PREFIX.'lastlogin'); //TRAININGSSYSTEM_PLUGIN_DB_TRAINING_LASTLOGIN
		define($pluginname.'DB_TRAINING_LEKTIONEN',TRAININGSSYSTEM_PLUGIN_DB_PREFIX.'tranings_lektionen'); //TRAININGSSYSTEM_PLUGIN_DB_TRAINING_LEKTIONEN
		define($pluginname.'DB_TRAINING_SEITEN',TRAININGSSYSTEM_PLUGIN_DB_PREFIX.'tranings_seiten'); //TRAININGSSYSTEM_PLUGIN_DB_TRAINING_SEITEN
		define($pluginname.'DB_TRAINING_NACHRICHTEN',TRAININGSSYSTEM_PLUGIN_DB_PREFIX.'tranings_nachrichten'); //TRAININGSSYSTEM_PLUGIN_DB_TRAINING_SEITEN
		define($pluginname.'DB_TRAINING_USER',TRAININGSSYSTEM_PLUGIN_DB_PREFIX.'tranings_user'); //TRAININGSSYSTEM_PLUGIN_DB_TRAINING_USER
		define($pluginname.'DB_TRAINING_LOGS',TRAININGSSYSTEM_PLUGIN_DB_PREFIX.'training_logs'); //TRAININGSSYSTEM_PLUGIN_DB_TRAINING_LOGS
		define($pluginname.'DB_FIRMA', 'user_grouper_plugin_firma');	//TRAININGSSYSTEM_PLUGIN_DB_FIRMA
		define($pluginname.'DB_GRUPPE', 'user_grouper_plugin_gruppe');	//TRAININGSSYSTEM_PLUGIN_DB_GRUPPE
		define($pluginname.'DB_GRUPPE_2_USER', 'user_grouper_plugin_gruppe_two_user'); //TRAININGSSYSTEM_PLUGIN_DB_GRUPPE_2_USER

        define($pluginname.'DB_REGISTERKEY', TRAININGSSYSTEM_PLUGIN_DB_PREFIX.'registerkey'); //TRAININGSSYSTEM_PLUGIN_DB_REGISTERKEY
        define($pluginname.'DB_USER_LOGS',TRAININGSSYSTEM_PLUGIN_DB_PREFIX.'user_logs'); //TRAININGSSYSTEM_PLUGIN_DB_USER_LOGS

        define($pluginname.'DB_TRAINING_BEWERTUNG',TRAININGSSYSTEM_PLUGIN_DB_PREFIX.'bewertungen'); //TRAININGSSYSTEM_PLUGIN_DB_TRAINING_BEWERTUNGEN

		define($pluginname.'DB_CERTIFICATE',TRAININGSSYSTEM_PLUGIN_DB_PREFIX.'certificate'); //TRAININGSSYSTEM_PLUGIN_DB_CERTIFICATE
    
        define($pluginname.'DB_FORM_FIELDS',TRAININGSSYSTEM_PLUGIN_DB_PREFIX.'ts_forms_fields'); //TRAININGSSYSTEM_PLUGIN_DB_FORM_FIELDS
        define($pluginname.'DB_FORM_DATA',TRAININGSSYSTEM_PLUGIN_DB_PREFIX.'ts_forms_data'); //TRAININGSSYSTEM_PLUGIN_DB_FORM_DATA

		define($pluginname.'DB_EXERCISE',TRAININGSSYSTEM_PLUGIN_DB_PREFIX.'ts_exercise'); //TRAININGSSYSTEM_PLUGIN_DB_EXERCISE
    
		define($pluginname.'DB_PERMISSION_FUNCTION',TRAININGSSYSTEM_PLUGIN_DB_PREFIX.'permission_function'); //TRAININGSSYSTEM_PLUGIN_DB_PERMISSION_FUNCTION
		define($pluginname.'DB_PERMISSION_ROLE',TRAININGSSYSTEM_PLUGIN_DB_PREFIX.'permission_role'); //TRAININGSSYSTEM_PLUGIN_DB_PERMISSION_ROLE
		define($pluginname.'DB_PERMISSION_ROLE2FUNCTION',TRAININGSSYSTEM_PLUGIN_DB_PREFIX.'permission_role2function'); //TRAININGSSYSTEM_PLUGIN_DB_PERMISSION_ROLE2FUNCTION

        define($pluginname.'AJAX',str_replace("-","_",TRAININGSSYSTEM_PLUGIN_SLUG."_ajax")); //TRAININGSSYSTEM_PLUGIN_AJAX
		define($pluginname.'AJAX_URL',str_replace("-","_",TRAININGSSYSTEM_PLUGIN_SLUG."_ajax_url")); //TRAININGSSYSTEM_PLUGIN_AJAX_URL
		define($pluginname.'LOGOUT_TIME',str_replace("-","_",TRAININGSSYSTEM_PLUGIN_SLUG."_logout_time")); //TRAININGSSYSTEM_PLUGIN_LOGOUT_TIME

		define($pluginname.'DB_TRAINING_DATE',TRAININGSSYSTEM_PLUGIN_DB_PREFIX.'tranings_date'); //TRAININGSSYSTEM_PLUGIN_DB_TRAINING_DATE
		define($pluginname.'DB_TRAINING_DEP',TRAININGSSYSTEM_PLUGIN_DB_PREFIX.'tranings_dependency'); //TRAININGSSYSTEM_PLUGIN_DB_TRAINING_DEP
		
		define($pluginname.'DB_USER_EVENT',TRAININGSSYSTEM_PLUGIN_DB_PREFIX.'user_event'); //TRAININGSSYSTEM_PLUGIN_DB_USER_EVENT
		
		define($pluginname.'TEMPLATE', 'v1');
}

constants();


/**
 * Plugin update
 */
use YahnisElsts\PluginUpdateChecker\v5\PucFactory;
$myUpdateChecker = PucFactory::buildUpdateChecker(
	'https://devprojects2.th-luebeck.de/updateserver/wp-update-server/?action=get_metadata&slug='.TRAININGSSYSTEM_PLUGIN_SLUG,
	__FILE__,
	TRAININGSSYSTEM_PLUGIN_SLUG//'trainingssystem-plugin-v2'

);

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-trainingssystem-plugin-activator.php
 */
function activate_trainingssystem_plugin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-trainingssystem-plugin-activator.php';
	Trainingssystem_Plugin_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-trainingssystem-plugin-deactivator.php
 */
function deactivate_trainingssystem_plugin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-trainingssystem-plugin-deactivator.php';
	Trainingssystem_Plugin_Deactivator::deactivate();
}

/**
 * The code that runs during plugin update.
 * This action is documented in includes/class-trainingssystem-plugin-updator.php
 */
function updating_trainingssystem_plugin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-trainingssystem-plugin-updator.php';
	Trainingssystem_Plugin_Updator::updating();
}

register_activation_hook( __FILE__, 'activate_trainingssystem_plugin' );
register_deactivation_hook( __FILE__, 'deactivate_trainingssystem_plugin' );
add_action( 'plugins_loaded', 'updating_trainingssystem_plugin' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-trainingssystem-plugin.php';


/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_trainingssystem_plugin() {

	$plugin = new Trainingssystem_Plugin();
	$plugin->run();

}
run_trainingssystem_plugin();




// ____o__ __o____                          o                 o                                                                    o                                         o              o      __o     
// /   \   /   \                         _<|>_             _<|>_                                                                 <|>                                       <|>            <|>   o/  v\    
// 	 \o/                                                                                                                      < >                                       < >            < >  /|    <\   
// 	  |        \o__ __o     o__ __o/    o    \o__ __o     o    \o__ __o     o__ __o/      __o__    __o__   o      o    __o__   |        o__  __o   \o__ __o__ __o        \o            o/   //    o/   
// 	 < >        |     |>   /v     |    <|>    |     |>   <|>    |     |>   /v     |      />  \    />  \   <|>    <|>  />  \    o__/_   /v      |>   |     |     |>        v\          /v         /v    
// 	  |        / \   < >  />     / \   / \   / \   / \   / \   / \   / \  />     / \     \o       \o      < >    < >  \o       |      />      //   / \   / \   / \         <\        />         />     
// 	  o        \o/        \      \o/   \o/   \o/   \o/   \o/   \o/   \o/  \      \o/      v\       v\      \o    o/    v\      |      \o    o/     \o/   \o/   \o/           \o    o/         o/       
// 	 <|         |          o      |     |     |     |     |     |     |    o      |        <\       <\      v\  /v      <\     o       v\  /v __o   |     |     |             v\  /v         /v        
// 	 / \       / \         <\__  / \   / \   / \   / \   / \   / \   / \   <\__  < >  _\o__</  _\o__</       <\/>  _\o__</     <\__     <\/> __/>  / \   / \   / \             <\/>         /> __o__/_ 
// 																				  |                           /                                                                                        
// 																		  o__     o                          o                                                                                         
// 																		  <\__ __/>                       __/>                                                                                         

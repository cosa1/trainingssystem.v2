=== TrainOn ===
Tags: trainingssystem, learning management system, 
Requires at least: 5.6
Tested up to: 6.7
Stable tag: 4.3
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html

Dieses Plugin erweitert WordPress um ein Lernmanagementsystem. Es können Online-Trainings erstellt und in Lektionen und Seiten gegliedert werden. Daneben stehen zahlreiche weitere Funktionen, u.a. zum E-Coaching und für die Nutzerinteraktion zur Verfügung.

== Description ==

Mit TrainOn kann eine WordPress-Installation zu einer Lernplattform umgewandelt werden. Nach der Installation des Plugins können Trainings und Lektionen erstellt werden, denen schließlich Seiten unterzuordnen sind. Weiterhin können bestimmte Trainingsinhalte für einzelne User freigegeben oder gesperrt werden. Auch ein automatisches Freischalten der Inhalte, welche eine Lektion jeweils nach Abschluss der Vorgängerlektion zugänglich macht, ist möglich. Außerdem kann über einen User-Modus das Training durch einen Coach begleitet werden, da Eingaben in bestimmten Trainingsinhalten abgerufen werden können.

== Installation ==

Das Plugin kann ganz einfach über das WordPress-Backend und den Menüpunkt „Plugins–>Installieren“ installiert werden. Nachdem die ZIP-Datei über den Button „Plugin hochladen“ erfolgreich hochgeladen wurde, muss es nur noch aktiviert werden. Dass das funktioniert hat, erkennt man daran, dass im WordPress-Backend nun der Menüpunkt „TrainOn“ hinzugekommen ist.

Nachdem die WordPress-Installation aufgesetzt und das Plugin installiert wurde, sind noch einige wenige Schritte erforderlich, um die Plattform schließlich in Betrieb zu nehmen. Zunächst ist das Erzeugen eines Hauptmenüs empfehlenswert. Dies ist über das WordPress-Backend und den Punkt „Design–>Menüs“ möglich. Hier muss ein Menü mit einem beliebigen Titel wie z.B. „Hauptmenü“ erstellt werden. Beim Erstellen sollte darauf geachtet werden, dass die Checkbox „Primary Menu“ unter der Einstellung „Position in Theme“ gesetzt ist.

Sobald das Menü erstellt wurde, muss diesem mindestens ein Eintrag hinzugefügt werden. Dazu muss auf der linken Seite unter „Menüeinträge hinzufügen“ ein oder mehrere Elemente, z.B. eine neu erstellte Seite „Startseite“ oder „Home“, ausgewählt werden.

Um die Funktionen von TrainOn im Frontend zur Verfügung zu stellen, müssen dafür entsprechende Seiten (Standardseiten) erstellt werden. Dies muss nicht manuell durchgeführt werden, sondern kann über den Punkt „TrainOn->Einstellungen–>Standardseiten anlegen“ erledigt werden. Über den Punkt „TrainOn–>Einstellungen->Seitenzuteilung“ können die Funktionen den so erzeugten Seiten zugeteilt werden.

Wurde das Menü erzeugt, kann dieses in den Einstellungen von TrainOn im Abschnitt „Seitenzuteilung“ unter „Standardmenü anzeigen in (Menü)“ ausgewählt werden, um das Menü als Standardmenü auf der Plattform einzubinden. Wenn das Menü auf diese Weise eingefügt wurde, ist es möglich, bestimmte Menüpunkte wie bspw. Verwaltung oder Nachrichten optional ausblenden zu lassen.

Alternativ kann das Menü auch selbst erstellt werden. Dazu muss, wie zuvor bereits beschrieben, das Menü unter „Design–>Menüs“ nach Belieben zusammengestellt werden. Unter der Einstellung „Standardmenü anzeigen in (Menü)“ muss hier aber kein Menü, sondern die Option „nicht zugewiesen“ ausgewählt werden.

Um Probleme bei der Zugehörigkeit von Trainings, Lektionen sowie Seiten vorzubeugen, ist folgender Schritt notwendig: Wählt man über das Admin-Menü „Einstellungen“ und dann den Punkt „Permalinks“, sollte dort „Einfach“ ausgewählt sein. Falls nicht, sollte das entsprechend angepasst werden.

== Betrieb ==

Um die Lernplattform mit TrainOn betreiben zu können, sind inzwischen __keine__ zusätzlichen Plugins mehr erforderlich.

Es gelten allgemein die Anforderungen an eine WordPress-Installation (siehe https://de.wordpress.org/about/requirements/). Folglich sollte PHP (min. 8.1.x) und MySQL (min. 5.6) installiert sein. Für größere Projekte ab ca. 100 Accounts wird empfohlen, dass das PHP-Memory-Limit einstellbar ist, beispielsweise auf 256MB. Die maximale Ausführungszeit (max_execution_time) sollte zudem auch einstellbar sein, beispielsweise auf mindestens 120 Sekunden bei einem Projekt ab etwa 10 Trainings mit je 50 Seiten.

Es ist empfohlen, das Theme Coachlight (https://bitbucket.org/cosa1/coachlight.v2/) zu installieren.

== Changelog ==

= 2.5.3.2 = 

* Bugfix: Der Shortcode "ts_forms_conditional" funktioniert nun wieder für MultiFormulare und Formulare, welche aus mehreren Feldern bestehen

= 2.5.3.1 = 

* Strukturelle Anpassungen des Plugins

= 2.5.3 = 

* Zertifikate: Es können nun mehrere Zertifikate einem Training zugeordnet werden
* Zertifikate: Ein alternativer Titel für das Training kann ins Teilnahmezertifikat eingebunden werden
* Zertifikate: Ein Validierungscode kann ins Teilnahmezertifikat eingebunden werden
* Zertifikate-Übersicht: Zertifikate-Übersicht wurde angepasst, sodass nun mehrere Zertifikate für ein Training heruntergeladen werden können
* Zertifikate-Übersicht: Der Titel des Zertifikats kann nun optional für jedes Zertifikat in der Zertifikate-Übersicht eingeblendet werden
* Dashboard: Dashboard-Element "Abzeichen" wurde angepasst, sodass nun mehrere Zertifikate für ein Training heruntergeladen werden können
* Dashboard: Der Titel des Zertifikats kann nun optional für jedes Zertifikat im Dashboard-Element "Abzeichen" eingeblendet werden
* Dashboard: Die Links zum Download der Zertifikate können optional im Dashboard-Element "Abzeichen" nun ausgeblendet werden
* Bugfix: Für Unternehmen und Teams können keine leeren Namen mehr vergeben werden
* Bugfix: Der Inhalt von Nachrichten kann nun im User-Modus eingesehen werden

= 2.5.2 = 

* Info-Card-Element: Neuen Shortcode für die Darstellung von Informationen in Form von durchklickbaren Karten ergänzt
* Lektionsübersicht: Beitragsbilder und Beschreibungen werden nun auch in der Listendarstellung der Lektionsübersicht für jede Lektion eines Trainings eingefügt
* Check-Ergebnisse: Optionalen Parameter ergänzt, mit dem festgelegt werden kann, ob die letzten Ergebnisse sortiert vorliegen sollen
* App-Daten-Output: Ladezeiten verringert
* Bugfix: Beim Swiping-Element werden für Farbwerte keine Alpha-Werte mehr erlaubt
* Bugfix: Die Bedienelemente eines MultiForms erhalten nun im Speichermodus "Einmalige Speicherung" das "disabled"-Attribut, nachdem die Speicherung der Eingaben erfolgt ist

= 2.5.1 = 

* Coaching-Übersicht: Elemente "ID" und "Registrierungsdatum" ergänzt
* Coaching-Übersicht: Neuen Aktivitäts-Zustand ergänzt, welcher angezeigt wird, wenn Teilnehmende aktuell keine neuen Trainings mit Coaching aktiv bearbeiten
* Dashboard: Im Dashboard-Item "Fortschritt" können Trainings nun optional in Kategorien organisiert werden
* Anzeigen von Beiträgen: Darstellung der letzten Beiträge als Liste ergänzt
* Anzeigen von Beiträgen: Veröffentlichungsdaten der Beiträge können optional angezeigt werden
* Einstellungen: In den Einstellungen können die Elemente der Coaching-Übersicht nach Bedarf ausgewählt werden
* Einstellungen: Einstellungen zur Inaktivität von Teilnehmenden von "Sonstiges" zu "Coaching-Übersicht" verschoben
* Bugfix: Abruf der Aktivität von Teilnehmenden in der Coaching-Übersicht überarbeitet, sodass nach Abschluss von Lektionen oder Trainings nicht der Zustand "Keine Daten vorhanden" angezeigt wird

= 2.5.0.2 = 

* Bugfix: Fehler beim Coachingmodus-Formular behoben, der dazu geführt hat, dass nach einer Auswahl der Trainingsmodus sowie die Aktivierung des Trainings nicht gespeichert wurden, sodass alle Trainings im Trainingsmodus "individuell" und ausgeblendet sind

= 2.5.0.1 = 

* __Achtung:__ Diese Version erfordert mindestens PHP Version 8.1

= 2.5.0 = 

* Rebranding des Plugins: Rebranding des "Trainingssystem-Plugin" zu "TrainOn"
* Anzeigen von Beiträgen (Posts): Alternative Darstellung als Slideshow ergänzt
* Visualisierungen für die betriebliche Gesundheitsanalyse: Schriftgröße in den einzelnen Charts der visuellen Auswertungen erhöht
* Bugfix: Das Demo-Training in den TrainOn-Einstellungen wird nun wieder korrekt zusammengestellt
* Bugfix: Mit dem Datenexport exportierte Eingaben in Textfeldern vom Typ "Date" und "Datetime local" werden nun im Format "DD.MM.YYYY" bzw. "DD.MM.YYYYY hh:mm" dargestellt

= 2.4.6.1 = 

* Dashboard: Für Demo-Nutzer wird nicht mehr der zufällig generierte Benutzername in der Überschrift eingeblendet
* Bugfix: Die Suche nach Seiten bei der Zusammenstellung von Trainings im Backend funktioniert nun wieder
* Bugfix: Demo-Nutzern wird in der Zertifikate-Übersicht sowie im Dashboard nicht mehr der Link zum eigenen Profil angezeigt
* Bugfix: Führungskräfte und Krankenkassenadmins können für Unternehmen nun abgewählt werden

= 2.4.6 = 

* Übungen: Export, Import und Duplizieren von Übungen ist nun möglich
* Trainings Ex-/Import: Ex-/Import von Übungen in Trainings ist nun möglich
* App: User-Modus für App-Daten
* Trainingssystem-Einstellungen: Drittanbieter-Bibliotheken können bei Bedarf ausgeschaltet werden
* Berechtigungssystem: Berechtigungen zur Vergabe der "Krankenkassenadmin"-Rolle und zur Zuweisung von Führungskräften für Unternehmen ergänzt
* Backend/Trainings: Die mehrfache Nutzung von Lektionen und Seiten wird nun verhindert 
* Bugfix: Shortcodes ohne Parameter erzeugen keinen Fehler beim Trainings Ex-/Import mehr
* Bugfix: Validierung von E-Mail-Adresse angepasst
* Bugfix: Das Anzeigen aller Beiträge über den Shortcode "tsvp2_latest_posts" funktioniert nun wieder
* Bugfix: Das Aktualisieren von Funktionen im Berechtigungssystem funktioniert nun wieder

= 2.4.5 = 

* Tab-Element: Neuen Shortcode für eine Tab-Struktur zur Darstellung von Informationen ergänzt
* Datenexport: Header der CSV-Datei überarbeitet: "-" werden nicht mehr in Titel eingefügt, bei Feldtypen "Skala" und "Matrix" wird ein Zähler für die einzelnen Optionen ergänzt, Sortierung der Variablennamen angepasst
* Coaching-Übersicht: Mehreren Teilnehmenden kann nun auf einmal ein E-Coach zugewiesen werden 
* Datenexport: Bei der Systemstatistik "Beendete Lektionen" wurde der Fortschritt in Prozent entfernt
* Seitennavigation: Navigation in Trainings via Dropdown möglich
* Systemstatistiken: Besuchsdauer einer Seite wird nun verlässlich getrackt
* Abzeichen-Modal: Neuen Parameter ergänzt, sodass nun ein eigener Link für "Weiter"-Button hinterlegt werden kann
* PLZ-Suche: Zwei Parameter ergänzt, über die Darstellung als Tooltip und Text für den eigenen Standort festgelegt werden können
* Menüs: Aktives Menü-Element wird nun hervorgehoben
* Bugfix: Sortierung der Benutzer in Nutzerliste bei mehreren Nutzerlisten-Berechtigungen ist nun korrekt
* Bugfix: Beim Swiping-Element kann nur noch die oberste Karte gegriffen werden
* Bugfix: Bug beim Swiping-Element in Safari behoben, bei der die Overlay-Icons nicht mehr mittig angezeigt wurden

= 2.4.4 = 

* Trainingszuweisung: Option "Weiterleitung nach Login" nun auch auf Seite "Trainingszuweisung" über normale Zuweisung und Terminzuweisung für Trainings einstellbar
* Trainingszuweisung: Über Option "Training aktivieren" kann ein Training für einen Nutzer ein- bzw. ausgeblendet werden
* Trainingszuweisung: Bei der Nutzerauswahl werden nun nur noch Studien-Gruppen angezeigt, wenn mindestens eine Person in dieser aufgrund von Berechtigungen sichtbar ist
* Profil-Seite: Layout und Elemente des Profils können in Trainingssystem-Einstellungen nach Bedarf ausgewählt werden
* Nachrichtenfunktion: Der Betreff und der Inhalt der automatisierten Benachrichtigungen an E-Coaches für das Trainings-Feedback kann in den Trainingssystem-Einstellungen optional angepasst werden
* Nutzerlisten-Berechtigungen: Die Listen an Nutzern von mehreren Nutzerlisten-Berechtigungen werden in der Nutzerliste, bei der Trainingszuweisung und bei der Auswahl von Nutzern für Teams nun zusammengeführt
* Berechtigungen: Neue Berechtigung "Trainings - auch verborgene sehen" ergänzt, über die Sicht auf ausgeblendete Trainings gesteuert werden kann
* Swiping-Element: "Zurück"-Button kann optional ausgeblendet werden, zwei verschiedene Modi für "Zurück"-Button ergänzt
* Swiping-Element: "Wiederholen"-Button kann optional eingeblendet werden, optionale Parameter zur Gestaltung des Buttons ergänzt
* Bugfix: Beim Swiping-Element können die Stapel an bereits sortierten Karten nun nicht mehr gesehen werden 
* Bugfix: Die PDF-Funktion druckt nun mehrzeilige Ausgaben als Text des Shortcodes "coach_output_format_data"
* Bugfix: Im Datenexport werden nun die korrekten Unternehmen bei der Nutzerauswahl angezeigt, wenn man nur die Berechtigung hat, die eigenen Unternehmen zu sehen
* Bugfix: Beim Datenexport werden nun Benutzer-, Unternehmens- und Team-Namen sowie Studien- und Studiengruppen-IDs von unzulässigen Zeichen bereinigt
* Bugfix: Bei der Trainingszuweisung ist der Button "Auswahl zurücksetzen" unterhalb der Nutzertabelle beim aktiven Laden von Trainings nicht mehr anklickbar, sodass ein inkonsistenter Zustand verhindert wird
* Bugfix: Bei der Trainingszuweisung werden neu hinzugefügte Trainings nun wieder unterhalb des bereits bestehenden Sets an Trainings der Nutzer-Gruppe eingefügt

= 2.4.3 = 

* Nutzerliste: User-ID als Spalte ergänzt, Suche nach User-ID möglich
* Datenexport: Suche nach Teilnehmenden nun auch über E-Mail-Adresse, Studiengruppen-ID und Unternehmen möglich
* Datenexport: Systemstatistik "Trainingsfortschritt" ergänzt
* Trainingszuweisung: Hinweis ergänzt, welcher darüber informiert, wenn ein Training entfernt wird, welches als Bedingung in einer Terminzuweisung (Zeitliche Abhängigkeit) eingetragen wurde
* Swiping-Element: Auswahl von Farbe für Button und Icon des "Zurück"-Buttons ergänzt
* PLZ-Suche: Text kann nun optional oberhalb von Namen des Orts angegeben werden 
* Conditional Content: Feldtypen Skala und Matrix werden nun unterstützt
* PDF-Funktion: Für Eingabefelder werden nun auch die Typen "Date", "Datetime local", "E-Mail", "Month", "Number", "Telefonnr.", "Time", "URL" und "Week" unterstützt
* MultiForm: Manuelle Sortierung der Felder durch User kann aktiviert werden
* Menüs: Einzelne Menü-Elemente können für Demo-Accounts (Erweiterter Demo-Zugang) über "Design->Menüs" ausgeblendet werden
* App: Datenausgabe überarbeitet
* App: Informationen in Tooltip bei der Chart-Darstellung erweitert
* Bugfix: Trainings mit dem Trainingsmodus "Begleitet" können wieder über die Terminzuweisung zugewiesen werden
* Bugfix: Fehler bei Login aufgrund von Trainings mit aktivierter Weiterleitung-Option, welche aus Trainings-Set entfernt wurden, wurde behoben
* Bugfix: Fehler im Shortcode "coach_output_gbu_chart", welcher bei 0 Eingaben in einem hinterlegten Formular entsteht, wurde behoben
* Bugfix: Beim Gesundheits-Check (Care4Care) werden nun Rückmeldungen auch bei einem Wert von 0 angezeigt
* Bugfix: Die Option "Export mit Formular-Titeln als Header" im Datenexport unterstützt nun wieder die Feld-Typen Skala und Matrix

= 2.4.2 = 
* __Achtung:__ Diese Version erfordert mindestens PHP Version 8.0
* Swiping-Element zum Bewerten von Karten ergänzt
* Seitennummerierung um ziffernlose Darstellung erweitert
* Sicht auf Informationen in Nutzer-Detail-Seite angepasst
* Neue Berechtigung für Sicht auf Nutzungsstatistiken in Nutzer-Detail-Seite ergänzt
* Ausgabe von App-Daten auf Plattform nun möglich
* Anzahl an Antworten pro Unternehmen/Team wird nun in Kopf der Tabellen für die Mittelwerte angezeigt (BGA)
* Bugfix: Der Trainingsfortschritt kann im eigenen Profil wieder eingesehen werden
* Bugfix: Hinterlegte URL für Empfehlungen wird in Gesundheits-Check (Care4Care) wieder eingebunden

= 2.4.1 = 
* Performance-Verbesserung für Suchfunktion der Nutzerliste
* BGA: Parameter für Shortcode "tspv2_coach_output_gbu_mittelwerte" ergänzt, welcher PDF-Download der Ergebnisse pro Unternehmen erlaubt
* Neue Berechtigungen ergänzt, über die Sicht auf Trainingsfortschritt und Unternehmens-Details sowie Vergabe von Benutzerrollen gesteuert werden können.

= 2.4.0 = 
* Erweiterter Demo-Zugang
* Unbegrenzte Einlösungen von Freischaltcodes möglich
* JOLanDA-Check erweitert
* Arbeitsplatztrainings- und Gesundheitstrainings-Check (Projekt Implement) ergänzt
* Neue Typen der Seitennavigation hinzugefügt
* BGA: Grafiken für Mittelwerte und Häufigkeiten können getrennt voneinander als PDF heruntergeladen werden
* Performance-Verbesserung auf Unternehmen-Seite
* Allgemeiner Registrieren-Link auf Login-Seite ist nun ausblendbar
* Bugfixes und Erweiterungen vorhandener Funktionen

= 2.3.11 = 
* Grafische Dateneingabe (Zahlen in Boxen, Sterne, Smileys) für Skalen ergänzt
* Übersicht über zukünftige Trainingszuweisungen in Admin-Backend
* Anpassungen am JOLanDA-Check
* Bugfixes und Erweiterungen vorhandener Funktionen

= 2.3.10 = 
* Layout-Fixes im PDF-Modul
* Erweiterung des Arbeitsplatz-Checks (Care4Care-Projekt)
* Anpassungen des Gesundheits-Checks (Care4Care-Projekt)
* Fortschrittsanzeige im Dashboard: Optional alle Trainings anzeigbar
* Bugfixes und Erweiterungen vorhandener Funktionen

= 2.3.9.1 = 
* Performance-Verbesserungen
* Bugfixes und Erweiterungen vorhandener Funktionen

= 2.3.9 = 
* Pro Lektion einstellbares E-Coaching für terminlich bzw. in Abhängigkeit zugewiesene Trainings möglich
* Information über Aktivität in Coaching-Übersicht angepasst
* Formular-Eingaben werden bei automatischen Logout gespeichert
* Check-Ergebnisse können nun auch in einem Netzdiagramm (Radar-Chart) dargestellt werden
* Bugfixes und Erweiterungen vorhandener Funktionen

= 2.3.8 = 
* Terminzuweisung und abhängige Zuweisung für Vorlagen
* Seitennavigation kann per Option ausgeblendet werden
* Anzeige des Speicherdatums bei freigegebenen Inhalten
* Farben in Diagrammen für BGA anpassbar
* Optionen in Übungen hinzugefügt
* E-Mail-Adressen für Rollen auf Kontaktseite ausblenden
* Namenshinweis für Rollen "Führungskraft", "Krankenkassenadmin" und "Teamleitung" im Dashboard per Option einblendbar 
* Bugfixes und Erweiterungen vorhandener Funktionen

= 2.3.7 = 
* Neuer Post-Typ "Avatar"
* Shortcodes für Auswahl eines Avatars und Anzeige des gewählten Avatars
* Lektionsabzeichen anlegebar
* Arbeitsplatz-Check (Care4Care) erweitert
* Dashboard kann in Einstellungen individuell angepasst werden
* Fortschritt für Unternehmen und Teams einsehbar
* Neues Admin-Backend
* In Coaching-Übersicht können Notizen für zugeordnete Teilnehmende verfasst werden
* Aktivität von Teilnehmenden wird in Coaching-Übersicht angezeigt
* Bugfixes, Layout-Überarbeitungen und Erweiterungen vorhandener Funktionen

= 2.3.6 = 
* Captcha-Funktionalität für TS-Forms
* Zertifikate duplizieren
* Option User-Events löschen
* Touch-Unterstützung für Drag&Drop-Elemente
* PLZ-Suche mit Karte (iSleep-Projekt)
* Option für Formulartitel als Spaltentitel im CSV-Export (iSleep-Projekt)
* Bugfixes, Layout-Überarbeitungen und Erweiterungen vorhandener Funktionen

= 2.3.5.1 =
* Trainings können dupliziert werden
* Bugfixes, Layout-Überarbeitungen und Erweiterungen vorhandener Funktionen 

= 2.3.5 =
* Flexiblere Menüs: Ein- und Ausblenden je nach Studiengruppe oder Login-Zustand
* Datenexport aus mehrmaligem Speichern
* Interaktionselement: Begriffe bestimmten Kategorien zuordnen
* Interaktionselement: Kategorien mit Schiebereglern bewerten
* Check für das Jolanda-Projekt
* Offene Registrierung per Vorlage 
* User-Events können für einzelne Trainings deaktiviert werden
* Bugfixes, Layout-Überarbeitungen und Erweiterungen vorhandener Funktionen 

= 2.3.4 =
* Benachrichtigung von E-Coaches auf Lektionsebene 
* Neue Coaching-Übersicht mit Feedback-Status und Fortschrittsanzeige
* Zusammenlegung mehrerer Datensätze in einer Grafik auf dem Dashboard
* Empfehlungen im Gesundheits-Check ermöglicht
* Neue Forms-Option: Matrix-Überschriften auf Smartphones drehen
* Performance der Seite zum Anlegen von Freischaltecodes verbessert
* Option zum Ausblenden der Navigationsleiste in Trainings ergänzt 
* Duplizieren von Formularfeldern ermöglicht 
* To-do-Listen sind anlegebar
* Bugfixes und Erweiterungen vorhandener Funktionen

= 2.3.3 =
* Aktivitäten im Dashboard
* Erweiterung des persönlichen Menüs um Check-Ergebnisse und Favoriten
* Einmaliges Speichern eines Formulars
* Grafische GBU-Ausgaben u.a. um farbliche Unterlegung erweitert
* Tabellarischer Vergleich der GBU-Ausgaben nach Gruppen  
* Bugfix Datenexport bei Skala und Matrix und Erweiterung der Optionen bei Formularen allgemein
* Vorlagen für Codes/Links nachträglich ändern
* Weiterleitung bei erstem Login (z.B. zur Erstbefragung) in Vorlage einstellbar 
* Überarbeitete Ausgabe der Schlafdaten aus der App 
* Neue Registrierungsseite mit Teilnahmebedingungen und Hilfestellung
* Individueller Text nach Registrierung einstellbar
* Direkte Verlinkung über Login hinweg in ein Training bzw. eine Befragung ermöglicht
* Hinweis auf freigegebene Trainings/Befragungen im Dashboard
* Neuer Gesundheits-Check
* Bugfixes und Erweiterungen vorhandener Funktionen

= 2.3.2 =
* Terminzuweisung von Trainings/Befragungen 
* Studienübersicht
* Studiengruppen-ID zur Zuweisung von Trainings/Befragungen für ganze Gruppen
* Unterstützung für veraltetes PDF Print-Plugin entfernt 
* Ausgabe von in der App eingegebenen Daten ermöglicht
* Neue Ergebnisse im Dashboard anzeigen
* Arbeitsplatz-Check (Care4Care)
* Gemittelte Werte von Befragungen können tabellarisch ausgegeben werden (GBU/BGA)
* Bugfixes und Erweiterungen vorhandener Funktionen

= 2.3.1 = 
* Dashboard hinzugefügt. Kann als neue interne Startseite eingestellt werden
* Checks können gespeichert werden
* Teilnahmezertifikate können erstellt werden
* Einstellmöglichkeiten für Trainings (Lektionsliste überspringen, Inhalt platzieren)
* Bugfixes und Erweiterungen vorhandener Funktionen

= 2.3.0.1 =
* Trainings fortsetzen
* Kleinere Fehlerbehebungen

= 2.3.0 =
* Kompabilität mit dem NinjaForm Plugin entfernt
* __Achtung:__ Sind auf Ihrer Plattform noch Formulareingaben in NinjaForms vorhanden, aktualisieren Sie nicht und konvertieren Sie diese zuerst! Ein Export ist mit Version 2.3.0 nicht mehr möglich!
* __Hinweis:__ Erstellen Sie ein Backup der Datenbank bevor Sie dieses Update durchführen!

= 2.2.9 =
* PDF-Funktion unterstützt nun Bilder und den Shortcode coach_output_gbu_chart
* Für Gruppen von Firmen können jetzt Teamleitungen festgelegt werden
* Shortcode coach_output_gbu_chart um Sicht für Teamleitungen sowie PDF-Auswertung für jede Gruppe erweitert
* Berechtigungstabelle vervollständigt (Teamleitungen, weitere Funktionen etc.)
* Individuelle Hilfe/Kontaktinformationen per Shortcode ausgeben
* Eine Telefonnummer kann im Profil hinterlegt werden (für Coaching, Beratung, Hilfe, Kontakt etc.)
* TS-Forms: Mehrere Empfänger können beim E-Mail-Versand von Formulareingaben hinterlegt werden
* TS-Forms: E-Mail-Bestätigung für erfolgreich abgesendetes Formular möglich
* E-Mail-Vorlagen für Mailbox-Benachrichtigungen innerhalb der Trainingssystem-Einstellungen eingefügt
* REST-API für Schlafeffizienz erweitert
* Möglichkeit für das direkte Fortsetzen von Trainings integriert
* Bugfixes und Erweiterungen vorhandener Funktionen

= 2.2.8 =
* Neue Nutzerliste
* Übungen als eigener Typ mit Möglichkeit der Favorisierung und Shortcode tspv2_exercise_favorites als Übersichtsseite der Lieblingsübungen
* GBU-Ausgabe von Matrixdaten
* Gamification-Modal: Overlay 
* Visualisierung von Befragungsergebnissen für Führungskräfte/Krankenkassenadmin
* App-Anbindung: Tagebucheinträge löschen (Nils Hodys, Lüneburg)
* Automatischer Logout: Bugfix und Textänderung
* Individuelle Bezeichnungen für Menüpunkte vergeben
* Überarbeitung Demodaten anlegen
* Bugfix Berechtigungen: Zugriff für Rolle Trainings-Entwickler auf Vorschauseiten
* Bugfixes und Erweiterungen vorhandener Funktionen

= 2.2.7 =
* Einstellungen des Trainingssystems überarbeitet
* Zugriffssteuerung einzelner Seiten überarbeitet (sicherheitsrelevant)
* Formulare absenden für nicht angemeldete User
* Grafische Auswertung Gruppen/Firmen (noch Beta/in Testphase)
* Hilfetext und Doku im Trainingssystem-Menü aktualisiert
* Schiebespiel: mehrere Wörter pro Kategorie und mehrere Spiele pro Seite möglich
* Bugfixes und Erweiterungen vorhandener Funktionen

= 2.2.6 =
* Rollensystem überarbeitet, insb. Rolle für Trainingsentwicklung
* Basic Ninja-Forms Export (ohne Multiforms)
* Überarbeitung Datenexport von Systemstatistiken
* Überarbeitung Coaching-/Supervisoren-Übersicht
* Gamification: Abzeichenverleihung am Ende von Lektionen und Trainings
* Überarbeitung automatischer Logout
* Bugfixes und Erweiterungen vorhandener Funktionen

= 2.2.5 =
* __Achtung:__ Diese Version erfordert mindestens PHP Version 7.4
* Neues Rollensystem im FrontEnd und BackEnd
* Datenexport TS-Forms
* Rolle "Redakteur" nun für Trainingserstellung (kein Admin mehr erforderlich)
* Neuer Formulartyp Matrix
* Coaching-Modus 
* Benutzer*innen können Freigabe zurückziehen (Shortcode forward_content)
* Benutzer*innen mit Rolle 'Subscriber' wird Nachrichten-Tab nicht mehr angezeigt, falls sie keinen Coach haben
* Radio-Buttons zurücksetzen 
* Automatischer Logout im Backend einstellbar 
* Demo-Zugang 
* Carousel für Bilder (Slideshow)
* Latest-Posts: Letzte Beiträge anzeigen
* API für die Trainingssystem-App
* Bugfixes und Erweiterungen vorhandener Funktionen

= 2.2.4 =
* Neues Formularfeld Skala
* Neues Formularfeld Upload
* Rollenspezifische Ausgabe für Uploads  
* Export für Skala
* Vor- und Nachname im Profil möglich
* Freigabe von Texten für Führungskräfte
* Freigabe von Texten für "Krankenkassenadmin"
* Kleine Titel für Accordions (wieder) möglich
* Registrierung für Firmen mit Code und Gruppenzuteilung
* Timer für Texteingabe möglich
* Tooltips von Bootstrap integriert
* Code und Link pro Gruppe einer Firma
* Code und Link ohne Vorlage
* Bugfixes, Überarbeitungen und optische Anpassungen

= 2.2.3 =
* Kategorien für Trainings
* PDF Funktion
* Drag&Drop Spiel
* Anpassungen TS Forms Conditional
* Neue Funktionen TS Forms
* Viele kleinere BugFixes und Optimierungen

= 2.2.2 =
* Verbesserungen bei TS Forms und TS Forms bearbeiten
* BugFix: User werden u.U. doppelt hinzugefügt und können nicht mehr gelöscht werden
* Erweiterung des TS Forms Conditional Shortcodes
* Weitere kleiner Verbesserungen und Fehlerbehebungen

= 2.2.1 =
* TS Forms können bearbeitet werden
* Eingaben aus TS Forms können per E-Mail versandt werden
* Slider unterstützen Ampelfarben
* Anpassung der Shortcodes copyfieldtext und copycheckbox an die TS Forms
* Coaching-Übersicht
* Trainingsmodus wählen
* Viele kleine Verbesserungen und Fehlerbehebungen

= 2.2.0 =
* TS Forms als Ersatz für das externe NinjaForm Plugin
* Möglichkeit halb-automatischen Konvertierung in das neue Format
* Überschriften für Verwaltungsseiten können alternativ im Shortcode mit dem Attribut titel überschrieben werden
* Anpassungen Barrierefreiheit und HTML-Konformität des Trainingssystem-Plugins
* Optimierungen beim Passwort-Vergessen Prozess
* Weitere kleinere Verbesserungen und Fehlerbehebungen

= 2.1.1 =
* Integration von LimeSurvey
* Option zur systemweiten Ausblendung von E-Mail Adressen
* Überarbeitung der Systemstatistiken
* Neue Möglichkeit zum PDF Export
* Neue Rolle Datenadmin zum Datenexport von Benutzerdaten
* Vereinheitlichung der Benennung
* Viele kleinere Verbesserungen und Fehlerbehebungen

= 2.1.0 =
* Bootstrap 3 durch Version 4 ersetzt. Siehe dazu im WordPress-Backend unter Trainingssystem -> Patch. __Achtung: Trainings könnten Darstellungsfehler enthalten, die dann händisch korrigiert werden müssen.__ __Hinweis:__ das Theme benötigt ebenfalls ein Update.
* Wording im Hauptmenü überarbeitet
* Einführen eines Systembenutzers, der die Inhalte eines gelöschten Benutzers erhält
* Behebt ein Problem, dass das Trainingssystem im Internet Explorer nicht funktionierte
* Beim Löschen des Trainingssystem-Plugins werden alle Trainingssystem-Daten aus der Datenbank gelöscht
* Viele kleinere BugFixes und Optimierungen

= 2.0.6 =
* Vorlagen und Freischaltocdes können gelöscht/bearbeitet werden
* Daten von Nutzern werden beim Löschen aus der Datenbank entfernt
* Verbessertes Feedback bei Interaktion mit Verwaltungsseiten
* CSV-Import unterstützt nun Freischaltcodes
* Viele kleine Verbesserungen und Fehlerbehebungen

= 2.0.5 =
* __Achtung:__ Diese Version erfordert mindestens PHP Version 7.2
* Shortcode [tspv2_inputregiserkey] __entfernt__. Bitte entfernen Sie diesen aus dem Quellcode der Profil-Seite oder legen Sie die Profil-Seite mit der Option vorhandene ersetzen im WordPress Backend unter Trainingssystem -> Demodaten anlegen -> individuell hinzufügen neu an
* Feedback auf Buttons im Frontend
* Neue Dokumentation im Admin-Bereich
* Die Benutzer-Rollen können im Frontend bearbeitet werden
* Überarbeitung der Nutzer-Profil Seite
* Trainings werden nun gespeichert ohne den Fortschritt zu überschreiben
* kleinere Anpassungen und Optimierungen

= 2.0.4 =
* Nutzerdetails können von einem Admin bearbeitet werden oder mitsamt den getätigten Eingaben aus dem System gelöscht werden
* Nutzer können E-Mail-Benachrichtigungen deaktivieren
* Studienteilnehmer-ID kann über eine CSV-Datei importiert werden
* Freischaltocdes funktionieren
* kleinere Anpassungen und Optimierungen

= 2.0.3 =
* E-Mail Benachrichtigung bei ungelesenen Nachrichten behoben
* Warnung vor Zurücksetzen bei Trainings speichern

= 2.0.2 =
* Bugfix der Nachrichtenfunktion
* Zeitzonenproblematik behoben

= 2.0.1 =
* Administration im Frontend
* Trainingsarten: sequentiell, individuell, begleitet
* User Liste mit Rückmeldung über ausstehendes Feedback
* User Detail-Seite
* Nachrichtenfunktion für das begleitete Training
* Trainingszusammenstellungen als Vorlagen anlegen
* Codes zum Freischalten von Accounts und Verbinden mit Vorlagen
* Import und Export von Trainings
* Backend Einstellmöglichkeiten
* Bugfixes
* Designanpassungen

= 1.1.1 =
* Anpassung Fortschrittsberechnung
* Media-Queries für Gamification-Plugin
* Darstellungsanpassung für Bootstrap-Themes
* Darstellungsanpassung für Ninja-Forms 3.3.9
* Abstände für Listen, Absätze und Bilder im Accordion

= 1.1.0 =
* Gamification-Plugin integriert
* Slider aktualisiert: min, max und step können jetzt im Shortcode vergeben werden

= 1.0.9 =
* Coaching-Overview Fehler LastLogin behoben
* Ninjaforms Bildausgabe

= 1.0.8 =
* Coaching-Overview Fehler in der Datumsanzeige behoben
* Coaching-Overview Edit Button in der Feedbackspalte eingebaut

= 1.0.7 =
* Coaching-Overview Fehler behoben

= 1.0.6 =
* Coaching-Overview Suche verbessert
* Bugfix Navigation für kleinere Bildschirme
* Multi-Felder sind nun auch als textarea-Element verfügbar
* Bugfix für Radio-Buttons im User-Modus
* Bugfix Slider im Backend

= 1.0.5 =
* Bugfix Darstellung Firefox
* Fehler beim Setzen des Datums nach Abschluss einer Lektion
* Anbindung des Plugins "PDF & Print"
* Bugfix Slider
* CSS-Anpassungen

= 1.0.4 =
* Dropdown für Trainerauswahl in den Nutzereinstellungen
* Studieneinstellung sind jetzt schon beim Erstellen des Nutzers vorhanden
* Slider
* Datum speichern bei abgeschlossener Lektion
* CSS um Textarea optisch in ein Textfeld umzuwandeln

= 1.0.3 =
* Trennung von Theme und Trainingssystem-Plugin
* Darstellung für Links in Trainings
* Bugfix für Checkbox-Listen im User-Modus
* Backend-Ansicht für Coaches
* Lektionen-Button
* Pagination mit Anzeige des Fortschritts

= 1.0.2 =
* Bugfix in "Accordion"

= 1.0.1 =
* Bugfix für Checkbox-Listen und Radio-Listen im User-Modus

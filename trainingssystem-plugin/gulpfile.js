/**
 * Gulpfile.
 *
 * A simple implementation of Gulp.
 *
 *
 * @since 1.0.0
 * @author Markus
 */



/**
 * Load Plugins.
 *
 * Load gulp plugins and assing them semantic names.
 */
var gulp         = require('gulp'); // Gulp of-course

// CSS related plugins.
var sass         = require('gulp-sass'); // Gulp pluign for Sass compilation
var autoprefixer = require('gulp-autoprefixer'); // Autoprefixing magic
var minifycss    = require('gulp-uglifycss'); // Minifies CSS files

// JS related plugins.
var concat       = require('gulp-concat'); // Concatenates JS files
var babel        = require('gulp-babel');

// Utility related plugins.
var rename       = require('gulp-rename'); // Renames files E.g. style.css -> style.min.css
var sourcemaps   = require('gulp-sourcemaps'); // Maps code in a compressed file (E.g. style.css) back to it’s original position in a source file (E.g. structure.scss, which was later combined with other css files to generate style.css)
var notify       = require('gulp-notify'); // Sends message notification to you
var zip          = require('gulp-zip');
var clean        = require('gulp-clean');
var once         = require('gulp-once');
var minify       = require("gulp-babel-minify");


var outputfolder ="./output/";
var releasefolder ="./output/trainingssystem-plugin-v2/";
var styleSRC ="./assets/sass/basic.scss";
var styleAdminSRC ="./assets/sass/admin-backend.scss";
var styleDestination="./output/trainingssystem-plugin-v2/assets/css"
var styleFileName = 'style'
var styleAdminFileName = 'admin-style'
var styleDevFolder = "./assets/css/";
var jsCustomSRC = './assets/js/**/*.js';
var jsCustomFile = 'script';
var jsCustomDestination ="./output/trainingssystem-plugin-v2/assets/js";

 gulp.task('cleanrelease', function() {
   // run only once.
   // for the next call to the clean task, once will call done with
   // the same arguments as the first call.
   return gulp.src(outputfolder, {read: false, allowEmpty: true })
      .pipe(once())
        .pipe(clean());
 });

 gulp.task( 'copyfiles', gulp.series('cleanrelease', function () {
  return gulp.src( [
    "./admin/**",
    "./public/**",
    "./includes/**",
    "./templates/**",
    "./languages/**",
    "./vendor/**",
    "./index.php",
    "./readme.txt",
    "./trainingssystem-plugin.php",
    "./uninstall.php",
    "./gpl-3.txt",
    ] , {
    base: './'
  })
  .pipe(gulp.dest(releasefolder))
  .pipe( notify( { message: 'TASK: "copyfiles" Completed!', onLast: true } ) );
}));

 gulp.task( 'copyassetsfiles', function () {
   return gulp.src( [
     "./assets/img/**",
     "./assets/external/**",
     "./assets/pdf/**",
     "./assets/csv/**",
     './assets/css/blocks/**',
   ], {
    base: './'
    })
   .pipe(gulp.dest(releasefolder+""))
   .pipe( notify( { message: 'TASK: "copyassetsfiles" Completed!', onLast: true } ) );
 });


 gulp.task('styles', function () {
    return gulp.src( styleSRC )
    .pipe( concat(styleFileName + '.css'))
    .pipe( rename( {
      basename: styleFileName,
    }))
 		.pipe( sourcemaps.init() )
 		.pipe( sass( {
 			errLogToConsole: true,
 			outputStyle: 'compact',
 			//outputStyle: 'compressed',
 			// outputStyle: 'nested',
 			// outputStyle: 'expanded',
 			precision: 10
 		} ) )
 		.pipe( sourcemaps.write( { includeContent: false } ) )
 		.pipe( sourcemaps.init( { loadMaps: true } ) )
 		.pipe( autoprefixer(
 			'last 2 version',
 			'> 1%',
 			'safari 5',
 			'ie 8',
 			'ie 9',
 			'opera 12.1',
 			'ios 6',
 			'android 4' ) )

 		// .pipe( sourcemaps.write ( styleDestination ) )
 		// .pipe( gulp.dest( styleDestination ) )


 		//.pipe( rename( { suffix: '.min' } ) )
 		.pipe( minifycss( {
 			maxLineLen: 10
 		}))
     .pipe( gulp.dest( styleDestination ) )
 		.pipe( notify( { message: 'TASK: "styles" Completed!', onLast: true } ) )
 });

 gulp.task('styleAdmin', function () {
  return gulp.src( styleAdminSRC )
  .pipe( concat(styleAdminFileName + '.css'))
  .pipe( rename( {
    basename: styleAdminFileName,
  }))
   .pipe( sourcemaps.init() )
   .pipe( sass( {
     errLogToConsole: true,
     outputStyle: 'compact',
     precision: 10
   } ) )
   .pipe( sourcemaps.write( { includeContent: false } ) )
   .pipe( sourcemaps.init( { loadMaps: true } ) )
   .pipe( autoprefixer(
     'last 2 version',
     '> 1%',
     'safari 5',
     'ie 8',
     'ie 9',
     'opera 12.1',
     'ios 6',
     'android 4' ) )
   .pipe( minifycss( {
     maxLineLen: 10
   }))
   .pipe( gulp.dest( styleDestination ) )
   .pipe( notify( { message: 'TASK: "styles" Completed!', onLast: true } ) )
});

 gulp.task( 'skripts', function() {
  return gulp.src( jsCustomSRC )
   .pipe( concat( jsCustomFile + '.js' ) )
   //.pipe( gulp.dest( jsCustomDestination ) )
   .pipe( rename( {
     basename: jsCustomFile,
 //    suffix: '.min'
   }))
    .pipe(babel({
      presets: ["@babel/preset-env"],
      sourceType: "script"
    }))
   .pipe( minify() ) //alt uglify()
   .pipe( gulp.dest( jsCustomDestination ) )
   .pipe( notify( { message: 'TASK: "customJs" Completed!', onLast: true } ) );
});

gulp.task('buildzip', function () {
  return gulp.src(outputfolder+'/**')
       .pipe(zip('trainingssystem-plugin-v2.zip'))
       .pipe(gulp.dest(outputfolder))
       .pipe( notify( { message: 'TASK: "buildzip" Completed!', onLast: true } ) );
});

gulp.task('deletestyle', function() {
  return gulp.src(styleDevFolder+styleFileName+".css", {read: false, allowEmpty: true })
    .pipe(once())
    .pipe(clean());
});

gulp.task('deleteadminstyle', function() {
  return gulp.src(styleDevFolder+styleAdminFileName+".css", {read: false, allowEmpty: true })
    .pipe(once())
    .pipe(clean());
});

gulp.task('compilefrontendstyle', function() {
  return gulp.src( styleSRC )
    .pipe( concat(styleFileName + '.css'))
    .pipe( rename( {
      basename: styleFileName,
    }))
 		.pipe( sass() )
    .pipe( gulp.dest( styleDevFolder ) )
     .pipe( notify( { message: 'TASK: "compilestyles" Completed!', onLast: true } ) )
});

gulp.task('compileadminstyle', function() {
  return gulp.src( styleAdminSRC )
    .pipe( concat(styleAdminFileName + '.css'))
    .pipe( rename( {
      basename: styleAdminFileName,
    }))
 		.pipe( sass() )
    .pipe( gulp.dest( styleDevFolder ) )
     .pipe( notify( { message: 'TASK: "compilestyles" Completed!', onLast: true } ) )
});

gulp.task('compilestyles', gulp.series('deletestyle', 'deleteadminstyle', 'compilefrontendstyle' , 'compileadminstyle'));

gulp.task( 'customcopy',  gulp.series('copyfiles','copyassetsfiles','styles','styleAdmin','skripts','buildzip')); 

gulp.task( 'buildplugin',  gulp.series('copyfiles','copyassetsfiles','styles','styleAdmin','skripts','buildzip'));
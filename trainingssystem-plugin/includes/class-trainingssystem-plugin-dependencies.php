<?php


/**
 * The class responsible for orchestrating the actions and filters of the
 * core plugin.
 */


require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-trainingssystem-plugin-loader.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/database/class-trainingssystem-plugin-database.php';
require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-trainingssystem-plugin-twig.php';
require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-trainingssystem-plugin-zxcvbn.php';


require_once plugin_dir_path(dirname(__FILE__)) . 'includes/hooks-define/class-trainingssystem-plugin-hooks-define-admin.php';
require_once plugin_dir_path(dirname(__FILE__)) . 'includes/hooks-define/class-trainingssystem-plugin-hooks-define-public.php';
require_once plugin_dir_path(dirname(__FILE__)) . 'includes/hooks-define/class-trainingssystem-plugin-hooks-define-overall.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/accordion/class-trainingssystem-plugin-module-accordion.php';
require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/user-remover/class-trainingssystem-plugin-module-user-remover-public.php';
require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/user-remover/class-trainingssystem-plugin-module-user-remover-admin.php';
// PDF V2:
require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/pdf-print/class-trainingssystem-plugin-module-pdfdownload.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/show-hide/class-trainingssystem-plugin-module-show-hide.php';
require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/progress/class-trainingssystem-plugin-module-progress.php';
require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/custom-posttypes/class-trainingssystem-plugin-module-custom-posttyps.php';
require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/demodata/class-trainingssystem-plugin-demodata.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/blocks/class-trainingssystem-plugin-block-settings.php';
//require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/module/blocks/class-trainingssystem-plugin-block-test.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/user-remover/class-trainingssystem-plugin-module-user-remover-admin.php';
require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/user-remover/class-trainingssystem-plugin-module-user-remover-public.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/coach-user-training-mgr/class-trainingssystem-plugin-module-coach-user-training-mgr-public.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/vorlagen-user-mgr/class-trainingssystem-plugin-module-vorlagen-user-mgr-public.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/coach-user-lastlogin/class-trainingssystem-plugin-module-coach-user-lastlogin.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/settingspage/class-trainingssystem-plugin-module-settingspage.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/backend-trainings-mgr/class-trainingssystem-plugin-module-backend-trainings-mgr.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/frontend-menu/class-trainingssystem-plugin-module-frontend-menu.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/backend-admin-menu/class-trainingssystem-plugin-module-backend-admin-menu.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/user-meta-fields/class-trainingssystem-plugin-module-user-meta-fields.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/user-importer/class-trainingssystem-plugin-module-user-importer.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/trainings-ex-import/class-trainingssystem-plugin-module-trainings-ex-import.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/user-logs/class-trainingssystem-plugin-module-logger.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/help/class-trainingssystem-plugin-module-help.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/user-create/class-trainingssystem-plugin-module-user-create.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/user-list/class-trainingssystem-plugin-module-user-list.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/company/class-trainingssystem-plugin-module-company.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/company-details/class-trainingssystem-plugin-module-company-details.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/check-theme/class-trainingssystem-plugin-module-check-theme.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/redirect/class-trainingssystem-plugin-module-redirect.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/user-modus/class-trainingssystem-plugin-module-user-modus.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/media/class-trainingssystem-plugin-module-media.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/user-registration-key/class-trainingssystem-plugin-module-user-registration-key.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/berechtigung/class-trainingssystem-plugin-module-berechtigung.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/systemstatistics/class-trainingssystem-plugin-module-systemstatistics.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/excerpt/class-trainingssystem-plugin-module-excerpt.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/bewertung/bewertung.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/mailbox/class-trainingssystem-plugin-module-mailbox.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/abzeichen/class-trainingssystem-plugin-module-abzeichen.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/zertifikate/class-trainingssystem-plugin-module-zertifikate.php';
require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/avatar/class-trainingssystem-plugin-module-userprofil.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/study-list/class-trainingssystem-plugin-module-study-overview.php';


require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/comments/class-trainingssystem-plugin-module-comments.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/avatar/class-trainingssystem-plugin-module-userprofil.php';
require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/avatar/class-trainingssystem-plugin-module-avatar-widget.php';
require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/avatar/class-trainingssystem-plugin-module-avatar.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/swiping-element/class-trainingssystem-plugin-module-swiping-card.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/tab-element/class-trainingssystem-plugin-module-tab-element.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/info-card-element/class-trainingssystem-plugin-module-info-card-element.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/backend-help-top/class-trainingssystem-plugin-module-backend-help-top.php';
require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/AlertForBrowser/class-trainingssystem-plugin-module-BrowserDetect.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/addon-patch/class-trainingssystem-plugin-module-addon-patch.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/copy-checkbox-list/class-trainingssystem-plugin-module-copy-checkbox-list.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/copy-fieldtext/class-trainingssystem-plugin-module-copy-fieldtext.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/copy-checktable-items/class-trainingssystem-plugin-module-copy-checktable-items.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/coaching-overview/class-trainingssystem-plugin-module-coaching-overview.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/interactive-elements/class-trainingssystem-plugin-module-interactive.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/reset-radios/class-trainingssystem-plugin-module-reset-radios.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/coachingmode-form/class-trainingssystem-plugin-module-coachingmode-form.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/forms/class-trainingssystem-plugin-module-ts-forms.php';
require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/forms/class-trainingssystem-plugin-module-ts-forms-output.php';
require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/forms-media-grid/class-trainingssystem-plugin-module-forms-media-grid.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/App/class-trainingssystem-plugin-module-app.php';
require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/AppAdminPanel/AppAdminPanelModule.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/study-export/class-trainingssystem-plugin-module-studyexport.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/timer/class-trainingssystem-plugin-module-ts-timer.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/company/class-trainingssystem-plugin-module-company-forward-content.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/login-logout/class-trainingssystem-plugin-module-logout.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/latest-posts/class-trainingssystem-plugin-module-latest-posts.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/carousel/class-trainingssystem-plugin-module-ts-carousel.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/login-logout/class-trainingssystem-plugin-module-demo-login.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/login-logout/class-trainingssystem-plugin-module-inactive-logout.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/data-export/class-trainingssystem-plugin-module-data-export.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/company/class-trainingssystem-plugin-module-company-gbu-chart.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/exercise/class-trainingssystem-plugin-module-exercise.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/contact-info/class-trainingssystem-plugin-module-contact-info.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/dashboard/class-trainingssystem-plugin-module-dashboard.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/evaluate-categories/class-trainingssystem-plugin-module-evaluate.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/checks/class-trainingssystem-plugin-module-checks.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/coach-output-app-data/class-trainingssystem-plugin-module-app-data.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/xmlrpc/class-trainingssystem-plugin-module-xmlrpc.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/user-event/class-trainingssystem-plugin-module-user-event.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/todo-list/class-trainingssystem-plugin-module-todo-list.php';

require_once plugin_dir_path(dirname(__FILE__)) . 'includes/module/nearest-location/class-trainingssystem-plugin-module-nearest-location.php';

/**
 * The class responsible for defining internationalization functionality
 * of the plugin.
 */
require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-trainingssystem-plugin-i18n.php';

/**
 * The class responsible for defining all actions that occur in the admin area.
 */
require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-trainingssystem-plugin-admin.php';

/**
 * The class responsible for defining all actions that occur in the public-facing
 * side of the site.
 */
require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-trainingssystem-plugin-public.php';

<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Markus Domin, Helge Nissen <->
 */
class Trainingssystem_Plugin
{

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      Trainingssystem_Plugin_Loader    $loader    Maintains and registers all hooks for the plugin.
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $plugin_name    The string used to uniquely identify this plugin.
     */
    protected $plugin_name;

    /**
     * The current version of the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $version    The current version of the plugin.
     */
    protected $version;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function __construct()
    {

        $this->plugin_name = 'trainingssystem-plugin';
        $this->version = '1.0.0';

        $this->load_dependencies();
        $this->set_locale();

        $trainingssystem_Plugin_Hooks_Define_Overall = new Trainingssystem_Plugin_Hooks_Define_Overall($this->loader);
        $trainingssystem_Plugin_Hooks_Define_Overall->define_hooks();

        if (is_admin()) {
            $trainingssystem_Plugin_Hooks_Define_Admin = new Trainingssystem_Plugin_Hooks_Define_Admin($this->loader,$this->plugin_name,$this->version);
            $trainingssystem_Plugin_Hooks_Define_Admin->define_hooks();
            
        } else {
            $trainingssystem_Plugin_Hooks_Define_Public = new Trainingssystem_Plugin_Hooks_Define_Public($this->loader,$this->plugin_name,$this->version);
            $trainingssystem_Plugin_Hooks_Define_Public->define_hooks();
        }

        // if ( function_exists( 'the_gutenberg_project' ) ) {
        //     $Trainingssystem_Plugin_Module_Block_Settings = new Trainingssystem_Plugin_Module_Block_Settings();
        //     $this->loader->add_filter( 'block_categories', $Trainingssystem_Plugin_Module_Block_Settings, 'mdblock_categories',10,2);
        //     $Trainingssystem_Plugin_Module_Block_Settings->init_blocks($this->loader);
        // }

    }

    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - Trainingssystem_Plugin_Loader. Orchestrates the hooks of the plugin.
     * - Trainingssystem_Plugin_i18n. Defines internationalization functionality.
     * - Trainingssystem_Plugin_Admin. Defines all hooks for the admin area.
     * - Trainingssystem_Plugin_Public. Defines all hooks for the public side of the site.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function load_dependencies()
    {
        /**
         * Load all dependencies 
         * of the plugin.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-trainingssystem-plugin-dependencies.php';

        // PSR-4 autoloading
        require_once plugin_dir_path(__DIR__) . 'vendor/composer/autoload_psr4.php';

        $this->loader = new Trainingssystem_Plugin_Loader();

    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the Trainingssystem_Plugin_i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function set_locale()
    {

        $plugin_i18n = new Trainingssystem_Plugin_i18n();

        $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');

    }

    /**
     * Check if this is a request at the backend.
     *
     * @return bool true if is admin request, otherwise false.
     */
    private function is_admin_request()
    {
        /**
         * Get current URL.
         *
         * @link https://wordpress.stackexchange.com/a/126534
         */
        $current_url = home_url(add_query_arg(null, null));

        /**
         * Get admin URL and referrer.
         *
         * @link https://core.trac.wordpress.org/browser/tags/4.8/src/wp-includes/pluggable.php#L1076
         */
        $admin_url = strtolower(admin_url());
        $referrer = strtolower(wp_get_referer());

        /**
         * Check if this is a admin request. If true, it
         * could also be a AJAX request from the frontend.
         */
        if (0 === strpos($current_url, $admin_url)) {
            /**
             * Check if the user comes from a admin page.
             */
            if (0 === strpos($referrer, $admin_url)) {
                return true;
            } else {
                /**
                 * Check for AJAX requests.
                 *
                 * @link https://gist.github.com/zitrusblau/58124d4b2c56d06b070573a99f33b9ed#file-lazy-load-responsive-images-php-L193
                 */
                if (function_exists('wp_doing_ajax')) {
                    return !wp_doing_ajax();
                } else {
                    return !(defined('DOING_AJAX') && DOING_AJAX);
                }
            }
        } else {
            return false;
        }
    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    1.0.0
     */
    public function run()
    {
        $this->loader->run();
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @since     1.0.0
     * @return    string    The name of the plugin.
     */
    public function get_plugin_name()
    {
        return $this->plugin_name;
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @since     1.0.0
     * @return    Trainingssystem_Plugin_Loader    Orchestrates the hooks of the plugin.
     */
    public function get_loader()
    {
        return $this->loader;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @since     1.0.0
     * @return    string    The version number of the plugin.
     */
    public function get_version()
    {
        return $this->version;
    }

}

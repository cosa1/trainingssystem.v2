<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Markus Domin, Helge Nissen <->
 */
class Trainingssystem_Plugin_Database_Lektion {



	private $title;
	private $id;
	private $excerpt;
	private $url;
	private $image_url;
	private $seitenliste;
	private $training_auto;
	private $enable;
	private $firstPageUrl;
	private $pages;
	private $fin;
	private $lastpageid;
    private $lastpageindex;
    private $findate;
    private $feedback;
    private $tsformids;

	/**
	 * Initialize the collections used to maintain the actions and filters.
	 *
	 * @since    1.0.0
	 */
	public function __construct($id=null,$title=null,$excerpt=null,$url=null,$image_url=null, $seitenliste=null,$training_auto=null,$enable=null,$pages =array(),$firstPageUrl=null,$fin=null,$lastpageid=null,$lastpageindex=null, $findate=null, $feedback=null, $tsformids = array()) {
		$this->id = $id;
		$this->title = $title;
		$this->excerpt = $excerpt;
		$this->url = $url;
		$this->image_url = $image_url;
		$this->seitenliste = $seitenliste;
		$this->training_auto = $training_auto;
		$this->enable = $enable;
		$this->pages = $pages;
		$this->firstPageUrl = $firstPageUrl;
		$this->fin = $fin;
		$this->lastpageid=$lastpageid;
        $this->lastpageindex=$lastpageindex;
        $this->findate = $findate;
        $this->feedback = $feedback;
        $this->tsformids = $tsformids;
	}

    /**
     * Get the value of Register all actions and filters for the plugin.
     *
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of Register all actions and filters for the plugin.
     *
     * @param mixed title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Excerpt
     *
     * @return mixed
     */
    public function getExcerpt()
    {
        return $this->excerpt;
    }

    /**
     * Set the value of Excerpt
     *
     * @param mixed excerpt
     *
     * @return self
     */
    public function setExcerpt($excerpt)
    {
        $this->excerpt = $excerpt;

        return $this;
    }

    /**
     * Get the value of Url
     *
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set the value of Url
     *
     * @param mixed url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get the value of Image Url
     *
     * @return mixed
     */
    public function getImageUrl()
    {
        return $this->image_url;
    }

    /**
     * Set the value of Image Url
     *
     * @param mixed image_url
     *
     * @return self
     */
    public function setImageUrl($image_url)
    {
        $this->image_url = $image_url;

        return $this;
    }

    /**
     * Get the value of Seitenliste
     *
     * @return mixed
     */
    public function getSeitenliste()
    {
        return $this->seitenliste;
    }

    /**
     * Set the value of Seitenliste
     *
     * @param mixed seitenliste
     *
     * @return self
     */
    public function setSeitenliste($seitenliste)
    {
        $this->seitenliste = $seitenliste;

        return $this;
    }

    /**
     * Get the value of Training Auto
     *
     * @return mixed
     */
    public function getTrainingAuto()
    {
        return $this->training_auto;
    }

    /**
     * Set the value of Training Auto
     *
     * @param mixed training_auto
     *
     * @return self
     */
    public function setTrainingAuto($training_auto)
    {
        $this->training_auto = $training_auto;

        return $this;
    }

    /**
     * Get the value of Enable
     *
     * @return mixed
     */
    public function getEnable()
    {
        return $this->enable;
    }

    /**
     * Set the value of Enable
     *
     * @param mixed enable
     *
     * @return self
     */
    public function setEnable($enable)
    {
        $this->enable = $enable;

        return $this;
    }


    /**
     * Get the value of First Page Url
     *
     * @return mixed
     */
    public function getFirstPageUrl()
    {
        return $this->firstPageUrl;
    }

    /**
     * Set the value of First Page Url
     *
     * @param mixed firstPageUrl
     *
     * @return self
     */
    public function setFirstPageUrl($firstPageUrl)
    {
        $this->firstPageUrl = $firstPageUrl;

        return $this;
    }


    /**
     * Get the value of Pages
     *
     * @return mixed
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * Set the value of Pages
     *
     * @param mixed pages
     *
     * @return self
     */
    public function setPages($pages)
    {
        $this->pages = $pages;

        return $this;
    }


    /**
     * Get the value of Fin
     *
     * @return mixed
     */
    public function getFin()
    {
        return $this->fin;
    }

    /**
     * Set the value of Fin
     *
     * @param mixed fin
     *
     * @return self
     */
    public function setFin($fin)
    {
        $this->fin = $fin;

        return $this;
    }


    /**
     * Get the value of Lastpageid
     *
     * @return mixed
     */
    public function getLastpageid()
    {
        return $this->lastpageid;
    }

    /**
     * Set the value of Lastpageid
     *
     * @param mixed lastpageid
     *
     * @return self
     */
    public function setLastpageid($lastpageid)
    {
        $this->lastpageid = $lastpageid;

        return $this;
    }

    /**
     * Get the value of Lastpageindex
     *
     * @return mixed
     */
    public function getLastpageindex()
    {
        return $this->lastpageindex;
    }

    /**
     * Set the value of Lastpageindex
     *
     * @param mixed lastpageindex
     *
     * @return self
     */
    public function setLastpageindex($lastpageindex)
    {
        $this->lastpageindex = $lastpageindex;

        return $this;
    }

    /**
     * Get the value of Findate
     * 
     * @return mixed 
     */
    public function getFindate() 
    {
        return $this->findate;
    }

    /**
     * Set the value of Findate
     * 
     * @param mixed findate
     * 
     * @return self
     */
    public function setFindate($findate)
    {
        $this->findate = $findate;

        return $this;
    }

    /**
     * Get the value of feedback
     * 
     * @return mixed
     */
    public function getFeedback() 
    {
        return $this->feedback;
    }

    /**
     * Set the value of feedback
     * 
     * @return self
     */
    public function setFeedback($feedback) 
    {
        $this->feedback = $feedback;

        return $this;
    }

    /**
     * 
     * @return feedback available or not
     */
    public function isFeedback() 
    {
        return $this->feedback != 0;
    }

    /**
     * Get the value of TS Form IDs
     *
     * @return Array of TS Form IDs
     */
    public function getTsFormIds()
    {
        return $this->tsformids;
    }

    /**
     * Set the value of TS Form IDs
     *
     * @param Array TS Form IDs
     *
     * @return self
     */
    public function setTsFormIds($tsformids = array())
    {
        $this->tsformids = $tsformids;

        return $this;
    }

    /**
     * Checks if Training has a specific TS Form ID
     * 
     * @param int TS Form ID
     * 
     * @return boolean true/false
     */
    public function hasTsFormId($tsformid) {
        foreach($this->tsformids as $id) {
            if($id == $tsformid) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the last page of a lektion a user has visited
     * 
     * @return null if no page has ever been visited | Trainingssystem_Plugin_Database_Trainingseite else
     */
    public function getLastPage() {
        $ret = null;

        if($this->lastpageid != 0) {
            if(isset($this->pages[$this->lastpageindex])) {
                $ret = $this->pages[$this->lastpageindex];
            }
        }

        return $ret;
    }
}

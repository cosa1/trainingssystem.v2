<?php

/**
 * Datenbank-Model Klasse für ein Zertifikat 
 * 
 * @author Tim Mallwitz
 */

 class Trainingssystem_Plugin_Database_Certificate {

    private $id;
    private $certificate_id;
    private $training_id;
    private $certificate_header;
    private $certificate_content;
    private $certificate_footer;
    private $title;

    public function __construct($_id, $_certificate_id, $_training_id, $_certificate_header, $_certificate_content, $_certificate_footer, $_title = ""){
        $this->id = $_id;
        $this->certificate_id = $_certificate_id;
        $this->training_id = $_training_id;
        $this->certificate_header = $_certificate_header;
        $this->certificate_content = $_certificate_content;
        $this->certificate_footer = $_certificate_footer;
        $this->title = $_title;
    }

    public function getId(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getCertificate_id(){
        return $this->certificate_id;
    }

    public function setCertificate_id($certificate_id){
        $this->certificate_id = $certificate_id;
    }

    public function getTraining_id(){
        return $this->training_id;
    }

    public function setTraining_id($training_id){
        $this->training_id = $training_id;
    }

    public function getCertificate_header(){
        return $this->certificate_header;
    }

    public function setCertificate_header(){
        $this->certificate_header = $certificate_header;
    }

    public function getCertificate_content(){
        return $this->certificate_content;
    }

    public function setCertificate_content(){
        $this->certificate_content = $certificate_content;
    }

    public function getCertificate_footer(){
        return $this->certificate_footer;
    }

    public function setCertificate_footer(){
        $this->certificate_footer = $certificate_footer;
    }

    public function getTitle(){
		return $this->title;
	}
	public function setTitle($title){
		$this->title = $title;
	}
 }
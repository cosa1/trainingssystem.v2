<?php
/**
 * Datenbank-Model Klasse für eine Berechtigungs-Rolle
 *
 * @author     Max Sternitzke
 */
class Trainingssystem_Plugin_Database_Permission_Role {

    private $id;
    private $title;
    private $description;
    private $priority;
    private $permissions;


    public function __construct($_id, $_title, $_description, $_priority = null){

        $this->id = $_id;
        $this->title = $_title;
        $this->description = $_description;
        $this->priority = $_priority;
        $this->permissions = array();
    }

    /**
     * @return String
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param String $_id
     */
    public function setId($_id)
    {
        $this->id = $_id;
    }

    /**
     * @return String
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param String $_title
     */
    public function setTitle($_title)
    {
        $this->title = $_title;
    }

    /**
     * @return String
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param String $_description
     */
    public function setDescription($_description)
    {
        $this->description = $_description;
    }

    /**
     * @return Integer
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param Integer $_priority
     */
    public function setPriority($_priority)
    {
        $this->priority = $_priority;
    }

    /**
     * @return Array of Trainingssystem_Database_Permission_Role2Function Objects
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    /**
     * @param Array $_permissions
     */
    public function setPermissions($_permissions)
    {
        $this->permissions = $_permissions;
    }

    /**
     * Checks if a function_id exists in the permissions Array and has the value 1
     * 
     * @param String function_id
     * 
     * @return boolean found
     */
    public function hasPermission($function_id) {
        
        foreach($this->permissions as $permission) {
            if(strcmp($function_id, $permission->getFunctionId()) === 0 && strcmp($permission->getValue(), "1") === 0) {
                return true;
            }
        }
        
        return false;
    }
}

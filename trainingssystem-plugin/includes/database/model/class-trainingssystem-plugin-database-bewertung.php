<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Ali Parnan
 */
class Trainingssystem_Plugin_Database_Bewertung {


	private $bewertung;
	private $userid;
	private $pageid;
	private $datum;

	/**
	 * Initialize the collections used to maintain the actions and filters.
	 *
	 * @since    1.0.0
	 */
	public function __construct($bewertung,$userid,$pageid,$datum) {

		$this->bewertung=$bewertung;
		$this->userid=$userid;
		$this->pageid=$pageid;
		$this->datum=$datum;
	}

    /**
     * @return mixed
     */
    public function getBewertung()
    {
        return $this->bewertung;
    }

    /**
     * @param mixed $bewertung
     */
    public function setBewertung($bewertung)
    {
        $this->bewertung = $bewertung;
    }

    /**
     * @return mixed
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * @param mixed $userid
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;
    }

    /**
     * @return mixed
     */
    public function getPageid()
    {
        return $this->pageid;
    }

    /**
     * @param mixed $pageid
     */
    public function setPageid($pageid)
    {
        $this->pageid = $pageid;
    }

    /**
     * @return mixed
     */
    public function getDatum()
    {
        return $this->datum;
    }

    /**
     * @param mixed $datum
     */
    public function setDatum($datum)
    {
        $this->datum = $datum;
    }

	


}

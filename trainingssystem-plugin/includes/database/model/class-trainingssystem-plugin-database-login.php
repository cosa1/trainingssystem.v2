<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Markus Domin, Helge Nissen <->
 */
class Trainingssystem_Plugin_Database_login {

        private $userid;
        private $lastlogin;


        public function __construct($userid=null,$lastlogin=null){

            $this->userid = $userid;
            $this->lastlogin = $lastlogin;
        }

    /**
     * @return null
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * @param null $userid
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;
    }

    /**
     * @return null
     */
    public function getLastlogin()
    {
        return $this->lastlogin;
    }

    /**
     * @param null $lastlogin
     */
    public function setLastlogin($lastlogin)
    {
        $this->lastlogin = $lastlogin;
    }

}

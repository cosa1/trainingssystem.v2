<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Markus Domin, Helge Nissen <->
 */
class Trainingssystem_Plugin_Database_Companygroup {

	private $id;
	private $name;
	private $company;
	private $members;

	public function __construct($id=null,$name=null, $company=null, $members=null){
		$this->id = $id;
		$this->name = $name;
		$this->company = $company;
		$this->members = $members;
	}
		
	public function getId()
    {
        return $this->id;
    }
	
	public function setId($id)
    {
        $this->id = $id;
		return $this;
    }

	public function getName()
    {
        return $this->name;
    }
	
	public function setName($name)
    {
        $this->name = $name;
		return $this;
    }

	public function getCompany()
    {
        return $this->company;
    }
	
	public function setCompany($company)
    {
        $this->company = $company;
		return $this;
    }
	
	public function getMembers()
    {
        return $this->members;
    }
	
	public function setMembers($members)
    {
        $this->members = $members;
		return $this;
    }
   
}

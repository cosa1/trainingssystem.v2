<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Markus Domin, Helge Nissen <->
 */
class Trainingssystem_Plugin_Database_Nutzer {

	private $name;
	private $id;
	private $mail;
	private $wpuser;
	private $trainings;
	private $lastlogin;
	private $studienid;
	private $trainerName;
	private $trainerId;
	private $isVorlagennutzer;

	/**
	 * Initialize the collections used to maintain the actions and filters.
	 *
	 * @since    1.0.0
	 */
	public function __construct($name=null,$id=null, $mail=null,$wpuser=null,$trainings=null,$lastlogin=null, $studienid=null) {
		$this->id = $id;
		$this->name = $name;
		$this->mail = $mail;
		$this->wpuser = $wpuser;
		$this->trainings = $trainings;
		$this->lastlogin = $lastlogin;
		$this->studienid = $studienid;
		$this->trainerName = null;
		$this->isVorlagennutzer = null;
	}

	/**
	 * get Nutzer name
	 *
	 * @since    1.0.0
	 */
	public function getName() {
	  return $this->name;
	}

	/**
	 * set Nutzer name
	 *
	 * @since    1.0.0
	 */
	public function setName($name) {
	  $this->name = $name;
	}

	/**
	 * get Nutzer id
	 *
	 * @since    1.0.0
	 */
	public function getId() {
	  return $this->id;
	}

	public function setMail($mail) {
	  $this->mail = $mail;
	}

	/**
	 * get Nutzer id
	 *
	 * @since    1.0.0
	 */
	public function getMail() {
	  return $this->mail;
	}

	



	/**
	 * Get the value of trainings
	 */ 
	public function getTrainings()
	{
		return $this->trainings;
	}

	/**
	 * Set the value of trainings
	 *
	 * @return  self
	 */ 
	public function setTrainings($trainings)
	{
		$this->trainings = $trainings;

		return $this;
	}

	/**
	 * Get the value of wpuser
	 */ 
	public function getWpuser()
	{
		return $this->wpuser;
	}

	/**
	 * Set the value of wpuser
	 *
	 * @return  self
	 */ 
	public function setWpuser($wpuser)
	{
		$this->wpuser = $wpuser;

		return $this;
	}

	/**
	 * Get the value of lastlogin
	 */
	public function getLastlogin() 
	{
		return $this->lastlogin;
	}

	/**
	 * Set the value of lastlogin
	 * 
	 * @return self
	 */
	public function setLastlogin($lastlogin) 
	{
		$this->lastlogin = $lastlogin;

		return $this;
	}

	/**
	 * Get the value of studienid
	 */
	public function getStudienid() 
	{
		return $this->studienid;
	}

	/**
	 * Set the value of studienid
	 * 
	 * @return self
	 */
	public function setStudienid($studienid) 
	{
		$this->studienid = $studienid;

		return $this;
	}

	/**
	 * Get the value of trainerName
	 */
	public function getTrainerName() 
	{
		return $this->trainerName;
	}

	/**
	 * Set the value of trainerName
	 * 
	 * @return self
	 */
	public function setTrainerName($trainerName) 
	{
		$this->trainerName = $trainerName;

		return $this;
	}

	/**
	 * Get the value of trainerId
	 */
	public function getTrainerId() 
	{
		return $this->trainerId;
	}

	/**
	 * Set the value of trainerId
	 * 
	 * @return self
	 */
	public function setTrainerId($trainerId) 
	{
		$this->trainerId = $trainerId;

		return $this;
	}

	public function isVorlagennutzer() {
		return $this->isVorlagennutzer;
	}

	public function setVorlagennutzer($bool) {
		$this->isVorlagennutzer = $bool;

		return $this;
	}
}

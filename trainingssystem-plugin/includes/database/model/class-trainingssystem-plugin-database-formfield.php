<?php
/**
 * Datenbank-Model Klasse für ein Formular Formfield
 *
 * @author     Max Sternitzke
 */
class Trainingssystem_Plugin_Database_Formfield {

    private $fieldid;
    private $formid;
    private $type;
    private $attributes;
    private $index;
    private $formdata;
    private $countSubmissions;
    private $captchaData;

    public function __construct($_fieldid = 0, $_formid = 0, $_type = "", $_attr = "", $_index = 0, $_formdata = null, $countSubmissions = 0){

        $this->fieldid = $_fieldid;
        $this->formid = $_formid;
        $this->type = $_type;
        $this->attributes = $_attr;
        $this->index = $_index;
        $this->formdata = $_formdata;
        $this->countSubmissions = $countSubmissions;
        $this->captchaData = null;
    }

    /**
     * @return int
     */
    public function getFieldId()
    {
        return $this->fieldid;
    }

    /**
     * @param int $_fieldid
     */
    public function setFieldId($_fieldid)
    {
        $this->fieldid = $_fieldid;
    }

    /**
     * @return int
     */
    public function getFormId()
    {
        return $this->formid;
    }

    /**
     * @param int $_formid
     */
    public function setFormId($_formid)
    {
        $this->formid = $_formid;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $_type
     */
    public function setType($_type)
    {
        $this->type = $_type;
    }

    /**
     * @return JSON encoded String
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @return JSON decoded Array
     */
    public function getAttributesJsonDecoded()
    {
        return json_decode($this->attributes, true);
    }

    /**
     * @param JSON string $_attr
     */
    public function setAttributes($_attr)
    {
        $this->attributes = $_attr;
    }

    /**
     * @return int
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @param int $_index
     */
    public function setIndex($_index)
    {
        $this->index = $_index;
    }

    /**
     * @return FormData
     */
    public function getFormdata()
    {
        return $this->formdata;
    }

    /**
     * @param FormData $_formdata
     */
    public function setFormdata($_formdata)
    {
        $this->formdata = $_formdata;
    }

    /**
     * @return int
     */
    public function getCountSubmissions()
    {
        return $this->countSubmissions;
    }

    /**
     * @param int $_countSubmissions
     */
    public function setCountSubmissions($_countSubmissions)
    {
        $this->countSubmissions = $_countSubmissions;
    }

    /**
     * @return String Captcha-Data
     */
    public function getCaptchaData()
    {
        return $this->captchaData;
    }

    /**
     * @param String $_captchaData
     */
    public function setCaptchaData($_captchaData)
    {
        $this->captchaData = $_captchaData;
    }

}

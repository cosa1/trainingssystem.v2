<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Max Sternitzke
 */
class Trainingssystem_Plugin_Database_List_User {

	private $id;
	private $name;
	private $mail;
	private $lastlogin;
	private $registrationdate;
	private $trainings;
	private $companys;
	private $studienid;
	private $studiengruppenid;
	private $status;
	private $activity;
	private $activityDays;

	/**
	 * Initialize the collections used to maintain the actions and filters.
	 *
	 * @since    1.0.0
	 */
	public function __construct($id=null,$name=null, $mail=null,$lastlogin=null, $trainings=array(), $studienid=null, $studiengruppenid = null, $registrationdate = null) {
		$this->id = $id;
		$this->name = $name;
		$this->mail = $mail;
		$this->lastlogin = $lastlogin;
		$this->trainings = $trainings;
		$this->studienid = $studienid;
		$this->studiengruppenid = $studiengruppenid;
		$this->registrationdate = $registrationdate;
		$this->status = null;
		$this->activity = null;
	}

	/**
	 * get Nutzer name
	 *
	 * @since    1.0.0
	 */
	public function getName() {
	  return $this->name;
	}

	/**
	 * set Nutzer name
	 *
	 * @since    1.0.0
	 */
	public function setName($name) {
	  $this->name = $name;
	}

	/**
	 * get Nutzer id
	 *
	 * @since    1.0.0
	 */
	public function getId() {
	  return $this->id;
	}

	/**
	 * set Nutzer Mail
	 */
	public function setMail($mail) {
	  $this->mail = $mail;
	}

	/**
	 * get Nutzer Mail
	 *
	 * @since    1.0.0
	 */
	public function getMail() {
	  return $this->mail;
	}

	/**
	 * Get the value of lastlogin
	 */
	public function getLastlogin() 
	{
		return $this->lastlogin;
	}

	/**
	 * Set the value of lastlogin
	 * 
	 * @return self
	 */
	public function setLastlogin($lastlogin) 
	{
		$this->lastlogin = $lastlogin;

		return $this;
	}

	/**
	 * Get the value of trainings
	 */ 
	public function getTrainings()
	{
		return $this->trainings;
	}

	/**
	 * Set the value of trainings
	 *
	 * @return  self
	 */ 
	public function setTrainings($trainings)
	{
		$this->trainings = $trainings;

		return $this;
	}

	/** 
	 * Add training at index trainingsid
	 */
	public function addTraining($trainingid, $training) 
	{
		$this->trainings[$trainingid] = $training;
	}

	/**
	 * check if a training at index trainingid exists
	 * 
	 * @return true or false
	 */
	public function trainingExists($trainingid) 
	{
		return isset($this->trainings[$trainingid]);
	}

	/**
	 * return training at index trainingid
	 */
	public function getTraining($trainingid) 
	{
		return ($this->trainingExists($trainingid)) ? $this->trainings[$trainingid] : null;
	}

	/**
	 * Get the value of companys
	 */ 
	public function getCompanys()
	{
		return $this->companys;
	}

	/**
	 * Set the value of companys
	 *
	 * @return  self
	 */ 
	public function setCompanys($companys)
	{
		$this->companys = $companys;

		return $this;
	}

	/**
	 * Get the value of studienid
	 */ 
	public function getStudienid()
	{
		return $this->studienid;
	}

	/**
	 * Set the value of studienid
	 *
	 * @return  self
	 */ 
	public function setStudienid($studienid)
	{
		$this->studienid = $studienid;

		return $this;
	}

	/**
	 * Get the value of studiengruppenid
	 */ 
	public function getStudiengruppenid()
	{
		return $this->studiengruppenid;
	}

	/**
	 * Set the value of studiengruppenid
	 *
	 * @return  self
	 */ 
	public function setStudiengruppenid($studiengruppenid)
	{
		$this->studiengruppenid = $studiengruppenid;

		return $this;
	}

	/**
	 * Get the value of registrationdate
	 */
	public function getRegistrationdate() 
	{
		return $this->registrationdate;
	}

	/**
	 * Set the value of registrationdate
	 * 
	 * @return self
	 */
	public function setRegistrationdate($registrationdate) 
	{
		$this->registrationdate = $registrationdate;

		return $this;
	}

	/**
	 * Calculate and get the value of status
	 */ 
	public function getStatus() 
	{
		$dates = array();
		foreach($this->getTrainings() as $training) 
		{
			foreach($training->getLektionsliste() as $lektion) 
			{
				if($lektion->getFin() == 100 && !$lektion->isFeedback() && $lektion->getFindate() != null && $lektion->getTrainingAuto() == 2)
				{
					$dates[] = $lektion->getFindate();
				}
			}
		}
		
		if(count($dates) > 0) {
			asort($dates);
			$this->status = "Wartet seit " . date("d.m.Y", strtotime($dates[0]));
		}

		return $this->status;
	} 

	/**
	 * Set the value of status
	 *
	 * @return  self
	 */ 
	public function setStatus($status)
	{
		$this->status = $status;

		return $this;
	}

	/**
	 * Get the value of activity
	 */ 
	public function getActivity() {
		return $this->activity;
	}

	/**
	 * Set the value of activity
	 *
	 * @return  self
	 */ 
	public function setActivity($activity) {
		$this->activity = $activity;

		return $this;
	}

	/**
	 * Returns a formatted string for activity as it should be a DateTime object
	 * 
	 * @return mixed
	 */
	public function getActivityFormatted() {
		if($this->activity === true) {
			return true;
		}
		if($this->activity == "trainings finished") {
			return "trainings finished";
		}
		if($this->activity == "trainings not started") {
			return "trainings not started";
		}
		if($this->activity == "trainings not in progress") {
			return "trainings not in progress";
		}
		if(is_null($this->activity)) {
			return null;
		}
		if($this->activity instanceof DateTime) {
			return $this->activity->format("d.m.Y");
		}
		return $this->activity;
	}

	/**
	 * Get the value of activity days
	 */ 
	public function getActivityDays() {
		return $this->activityDays;
	}

	/**
	 * Set the value of activity days
	 *
	 * @return  self
	 */ 
	public function setActivityDays($activityDays) {
		$this->activityDays = $activityDays;

		return $this;
	}
}

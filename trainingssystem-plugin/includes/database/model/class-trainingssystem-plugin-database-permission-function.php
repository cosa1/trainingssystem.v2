<?php
/**
 * Datenbank-Model Klasse für eine Berechtigungs-Funktion
 *
 * @author     Max Sternitzke
 */
class Trainingssystem_Plugin_Database_Permission_Function {

    private $id;
    private $title;
    private $description;


    public function __construct($_id, $_title, $_description){

        $this->id = $_id;
        $this->title = $_title;
        $this->description = $_description;
    }

    /**
     * @return String
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param String $_id
     */
    public function setId($_id)
    {
        $this->id = $_id;
    }

    /**
     * @return String
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param String $_title
     */
    public function setTitle($_title)
    {
        $this->title = $_title;
    }

    /**
     * @return String
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param String $_description
     */
    public function setDescription($_description)
    {
        $this->description = $_description;
    }
}

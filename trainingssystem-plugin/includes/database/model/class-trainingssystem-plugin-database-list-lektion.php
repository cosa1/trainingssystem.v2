<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Max Sternitzke
 */
class Trainingssystem_Plugin_Database_List_Lektion {

	private $id;
	private $title;
	private $fin;
	private $findate;
	private $feedback;
	private $lastseitenid;
	private $trainingAuto;
	private $lektionEnable;

	/**
	 * Initialize the collections used to maintain the actions and filters.
	 *
	 * @since    1.0.0
	 */
	public function __construct($id=null,$title=null, $fin=null, $findate=null, $feedback=null, $last_seiten_id=null, $trainingAuto = null, $lektionEnable = false) {
		$this->id = $id;
		$this->title = $title;
		$this->fin = $fin;
		$this->findate = $findate;
		$this->feedback = $feedback;
		$this->lastseitenid = $last_seiten_id;
		$this->trainingAuto = $trainingAuto;
		$this->lektionEnable = $lektionEnable;
	}

	/**
	 * get Training title
	 *
	 * @since    1.0.0
	 */
	public function getTitle() {
	  return $this->title;
	}

	/**
	 * set Training title
	 *
	 * @since    1.0.0
	 */
	public function setTitle($title) {
	  $this->title = $title;
	}

	/**
	 * get Training id
	 *
	 * @since    1.0.0
	 */
	public function getId() {
	  return $this->id;
	}

	/**
	 * set Training Progress
	 */
	public function setFin($fin) {
	  $this->fin = $fin;
	}

	/**
	 * get Training Progress
	 *
	 * @since    1.0.0
	 */
	public function getFin() {
	  return $this->fin;
	}

	/**
     * Get the value of Findate
     * 
     * @return mixed 
     */
    public function getFindate() 
    {
        return $this->findate;
    }

    /**
     * Set the value of Findate
     * 
     * @param mixed findate
     * 
     * @return self
     */
    public function setFindate($findate)
    {
        $this->findate = $findate;

        return $this;
    }

    /**
     * Get the value of feedback
     * 
     * @return mixed
     */
    public function getFeedback() 
    {
        return $this->feedback;
    }

    /**
     * Set the value of feedback
     * 
     * @return self
     */
    public function setFeedback($feedback) 
    {
        $this->feedback = $feedback;

        return $this;
    }

    /**
     * 
     * @return feedback available or not
     */
    public function isFeedback() 
    {
        return $this->feedback != 0;
	}

	/**
     * Get the value of trainingAuto
     * 
     * @return Int
     */
    public function getTrainingAuto() 
    {
        return $this->trainingAuto;
    }

	/**
     * Set the value of trainingAuto
     * 
     * @return self
     */
    public function setTrainingAuto($trainingAuto) 
    {
        $this->trainingAuto = $trainingAuto;

        return $this;
    }

	/**
     * Get the value of lektionEnable
     *
     * @return mixed
     */
    public function getlektionEnable() 
    {
		return $this->lektionEnable;
    }

    /**
     * Set the value of lektionEnable
     *
     * @param mixed lektionEnable
     *
     * @return self
     */
    public function setlektionEnable($lektionEnable) 
    {
        $this->lektionEnable = $lektionEnable;

        return $this;
    }
}

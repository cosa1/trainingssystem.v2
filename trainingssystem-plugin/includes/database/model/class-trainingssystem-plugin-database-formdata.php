<?php
/**
 * Datenbank-Model Klasse für ein Formular Formdata
 *
 * @author     Max Sternitzke
 */
class Trainingssystem_Plugin_Database_Formdata {

    private $dataid;
    private $fieldid;
    private $userid;
    private $data;
    private $save_date;
    private $formgroup;


    public function __construct($_dataid, $_fieldid, $_userid, $_data, $_save_date, $_formgroup = 0){

        $this->dataid = $_dataid;
        $this->fieldid = $_fieldid;
        $this->userid = $_userid;
        $this->data = $_data;
        $this->save_date = $_save_date;
        $this->formgroup = $_formgroup;
    }

    /**
     * @return int
     */
    public function getDataId()
    {
        return $this->dataid;
    }

    /**
     * @param int $_dataid
     */
    public function setDataId($_dataid)
    {
        $this->dataid = $_dataid;
    }

    /**
     * @return int
     */
    public function getFieldId()
    {
        return $this->fieldid;
    }

    /**
     * @param int $_fieldid
     */
    public function setFieldId($_fieldid)
    {
        $this->fieldid = $_fieldid;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userid;
    }

    /**
     * @param int $_userid
     */
    public function setUserId($_userid)
    {
        $this->userid = $_userid;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return trim($this->data);
    }

    /**
     * @return boolean
     */
    public function dataIsArray() {
        if(strpos($this->data, "[") === false && strpos($this->data, "{") === false) {
            return false;
        }

        json_decode($this->data);
        if(json_last_error() == JSON_ERROR_NONE) {
            return true;
        }
        return false;
    }

    /**
     * @return Array
     */
    public function getDataJsonDecoded() {
        return json_decode(trim($this->data), true);
    }

    /**
     * @param string $_data
     */
    public function setData($_data)
    {
        $this->data = $_data;
    }

    /**
     * @return string unformatted date
     */
    public function getSaveDate()
    {
        return $this->save_date;
    }

    /**
     * @return string formatted date
     */
    public function getSaveDateFormatted()
    {
        $mysqldate = strtotime($this->save_date);
        return date("j\.n\.Y G\:i\:s", $mysqldate);
    }

    /**
     * @param string $_save_date
     */
    public function setSaveDate($_save_date)
    {
        $this->save_date = $_save_date;
    }

    /**
     * @return int
     */
    public function getFormgroup()
    {
        return $this->formgroup;
    }

    /**
     * @param int $_formgroup
     */
    public function setFormgroup($_formgroup)
    {
        $this->formgroup = $_formgroup;
    }
}

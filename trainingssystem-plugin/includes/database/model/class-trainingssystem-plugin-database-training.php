<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Markus Domin, Helge Nissen <->
 */
class Trainingssystem_Plugin_Database_Training {



	private $title;
	private $id;
	private $excerpt;
	private $url;
	private $imageurl;
    private $lektionsliste;
    private $zertifikat;
    private $categories;
    private $tsformids;
    private $showLektionsliste;

	/**
	 * Initialize the collections used to maintain the actions and filters.
	 *
	 * @since    1.0.0
	 */
	public function __construct($id=null,$title=null,$excerpt=null,$url=null,$imageurl=null, $lektionsliste=null, $categories = array(), $tsformids = array()) {
		$this->id = $id;
		$this->title = $title;
		$this->excerpt = $excerpt;
		$this->url = $url;
		$this->imageurl = $imageurl;
        $this->lektionsliste = $lektionsliste;
        $this->categories = $categories;
        $this->tsformids = $tsformids;
        $this->showLektionsliste;
	}


    /**
     * Get the value of Register all actions and filters for the plugin.
     *
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of Register all actions and filters for the plugin.
     *
     * @param mixed title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Excerpt
     *
     * @return mixed
     */
    public function getExcerpt()
    {
        return $this->excerpt;
    }

    /**
     * Set the value of Excerpt
     *
     * @param mixed excerpt
     *
     * @return self
     */
    public function setExcerpt($excerpt)
    {
        $this->excerpt = $excerpt;

        return $this;
    }

    /**
     * Get the value of Url
     *
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set the value of Url
     *
     * @param mixed url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get the value of Image Url
     *
     * @return mixed
     */
    public function getImageUrl()
    {
        return $this->imageurl;
    }

    /**
     * Set the value of Image Url
     *
     * @param mixed imageurl
     *
     * @return self
     */
    public function setImageUrl($imageurl)
    {
        $this->imageurl = $imageurl;

        return $this;
    }

    /**
     * Get the value of Lektionsliste
     *
     * @return mixed
     */
    public function getLektionsliste()
    {
        return $this->lektionsliste;
    }

    /**
     * Set the value of Lektionsliste
     *
     * @param mixed lektionsliste
     *
     * @return self
     */
    public function setLektionsliste($lektionsliste)
    {
        $this->lektionsliste = $lektionsliste;

        return $this;
    }

		/**
     * Set the value of Lektionsliste
     *
     * @param mixed lektionsliste
     *
     * @return self
     */
    public function lektioncount()
    {
        return count($this->lektionsliste);
    }

     /**
     * Get the value of Lektionsliste
     *
     * @return mixed
     */
    public function getFin()
    {
        $sum=0;
        if(count($this->lektionsliste)>0){
            foreach ($this->lektionsliste as $key => $value) {
                $sum+=$value->getFin();
            }
            if($sum>0){
                $sum = $sum / count($this->lektionsliste);
            }
        }
       
        return round($sum);
    }


    /**
     * Get the value of Lektionsliste
     *
     * @return mixed
     */
    public function getZertifikat()
    {
        return $this->zertifikat;
    }

    /**
     * Set the value of Lektionsliste
     *
     * @param mixed lektionsliste
     *
     * @return self
     */
    public function setZertifikat($zertifikat)
    {
        $this->zertifikat = $zertifikat;

        return $this;
    }

    /**
     * Get the value of Categories
     *
     * @return Array of Category-Strings
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set the value of Categories
     *
     * @param Array Categories
     *
     * @return self
     */
    public function setCategories($categories = array())
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * Checks if Training has a specific category
     * 
     * @param String Category
     * 
     * @return boolean true/false
     */
    public function hasCategory($category) {
        foreach($this->categories as $c) {
            if($c == $category) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the value of TS Form IDs
     *
     * @return Array of TS Form IDs
     */
    public function getTsFormIds()
    {
        return $this->tsformids;
    }

    /**
     * Set the value of TS Form IDs
     *
     * @param Array TS Form IDs
     *
     * @return self
     */
    public function setTsFormIds($tsformids = array())
    {
        $this->tsformids = $tsformids;

        return $this;
    }

    /**
     * Checks if Training has a specific TS Form ID
     * 
     * @param int TS Form ID
     * 
     * @return boolean true/false
     */
    public function hasTsFormId($tsformid) {
        foreach($this->tsformids as $id) {
            if($id == $tsformid) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the last page of a training a user has visited/page a user visits next time
     * 
     * @return null if no page has ever been visited | Trainingssystem_Plugin_Database_Trainingseite else
     */
    public function getLastPage() {
        $ret = null;
        $lektionid = null;

        foreach($this->lektionsliste as $lid => $lektion) {
            if($lektion->getLastPageId() != 0) {
                if(isset($lektion->getPages()[$lektion->getLastPageIndex()])) {
                    $ret = $lektion->getPages()[$lektion->getLastPageIndex()];
                }
            }
            if(!is_null($ret) && (count($lektion->getPages())-1) == $ret->getIndex() && $lektion->getLastPageId() != 0) {
                $ret = null;
                $lektionid = $lid;
            }
        }
        if(is_null($ret) && !is_null($lektionid) && isset($this->lektionsliste[$lektionid+1])) {
            if($this->lektionsliste[$lektionid]->getTrainingAuto() == 1 && $this->lektionsliste[$lektionid]->getFin() == 100) {
                return $this->lektionsliste[$lektionid+1]->getPages()[0] ?? null;
            }
            if($this->lektionsliste[$lektionid]->getTrainingAuto() == 2 && $this->lektionsliste[$lektionid]->getFin() == 100 && $this->lektionsliste[$lektionid]->isFeedback()) {
                return $this->lektionsliste[$lektionid+1]->getPages()[0] ?? null;
            }
            else if($this->lektionsliste[$lektionid]->getTrainingAuto() == 2 && $this->lektionsliste[$lektionid]->getFin() == 100 && !$this->lektionsliste[$lektionid]->isFeedback()){
                return $this->lektionsliste[$lektionid]->getPages()[(count($this->lektionsliste[$lektionid]->getPages())-1)] ?? null;
            }

            $lektionsListe = $this->lektionsliste[$lektionid+1]->getPages();
            return new Trainingssystem_Plugin_Database_Trainingseite(array("url" => $lektionsListe[0]->getUrl()));
        }
        
        return $ret;
    }

    public function showLektionsliste()
    {
        return $this->showLektionsliste ?? 1;
    }

    public function setShowLektionsliste($showLektionsliste)
    {
        $this->showLektionsliste = $showLektionsliste;

        return $this;
    }

    public function trainingEnabled() {
        return isset($this->lektionsliste[0]) ? $this->lektionsliste[0]->getEnable() : false;
    }
}

<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Max Sternitzke
 */
class Trainingssystem_Plugin_Database_List_Training {

	private $id;
	private $title;
	private $lektionsliste;
	private $trainingauto;
	private $trainingEnable;

	/**
	 * Initialize the collections used to maintain the actions and filters.
	 *
	 * @since    1.0.0
	 */
	public function __construct($id=null,$title=null, $lektionsliste=array(), $trainingauto=null, $trainingEnable = false) {
		$this->id = $id;
		$this->title = $title;
		$this->lektionsliste = $lektionsliste;
		$this->trainingauto = $trainingauto;
		$this->trainingEnable = $trainingEnable;
	}

	/**
	 * get Training title
	 *
	 * @since    1.0.0
	 */
	public function getTitle() {
	  return $this->title;
	}

	/**
	 * set Training title
	 *
	 * @since    1.0.0
	 */
	public function setTitle($title) {
	  $this->title = $title;
	}

	/**
	 * get Training id
	 *
	 * @since    1.0.0
	 */
	public function getId() {
	  return $this->id;
	}

	/**
     * Get the value of Lektionsliste
     *
     * @return mixed
     */
    public function getLektionsliste()
    {
        return $this->lektionsliste;
    }

    /**
     * Set the value of Lektionsliste
     *
     * @param mixed lektionsliste
     *
     * @return self
     */
    public function setLektionsliste($lektionsliste)
    {
        $this->lektionsliste = $lektionsliste;

        return $this;
	}
	
	/**
	 * add lektion at index lektionid
	 */
	public function addLektion($lektionid, $lektion) 
	{
		$this->lektionsliste[$lektionid] = $lektion;
	}

	/**
	 * check if a lektion at index lektionid exists
	 * 
	 * @return true or false
	 */
	public function lektionExists($lektionid) 
	{
		return isset($this->lektionsliste[$lektionid]);
	}

	/**
	 * get the lektion at index lektionid
	 */
	public function getLektion($lektionid) 
	{
		return ($this->lektionExists($lektionid)) ? $this->lektionsliste[$lektionid] : null;
	}

	/**
     * Get the value of Lektionsliste
     *
     * @return mixed
     */
    public function getFin()
    {
        $sum=0;
        if(count($this->lektionsliste)>0){
            foreach ($this->lektionsliste as $key => $value) {
                $sum+=$value->getFin();
            }
            if($sum>0){
                $sum = $sum / count($this->lektionsliste);
            }
        }
       
        return round($sum);
	}

    /**
     * Get the value of training_auto
     *
     * @return mixed
     */
    public function getTrainingauto() 
    {
		return $this->trainingauto;
    }

    /**
     * Set the value of training_auto
     *
     * @param mixed trainingauto
     *
     * @return self
     */
    public function setTrainingauto($trainingauto) 
    {
        $this->trainingauto = $trainingauto;

        return $this;
    }

	/**
     * Get the value of trainingEnable
     *
     * @return mixed
     */
    public function getTrainingEnable() 
    {
		return $this->trainingEnable;
    }

    /**
     * Set the value of trainingEnable
     *
     * @param mixed trainingEnable
     *
     * @return self
     */
    public function setTrainingEnable($trainingEnable) 
    {
        $this->trainingEnable = $trainingEnable;

        return $this;
    }

	public function toJson() {
		$ret = array();

		$ret['trainingId'] = $this->id;
		$ret['trainingMode'] = $this->trainingauto;
		$ret['trainingEnable'] = $this->trainingEnable;
		$ret['lektionsliste'] = array();
		foreach($this->lektionsliste as $lektion) {
			$ret['lektionsliste'][] = $lektion->getId();
		}

		return json_encode($ret);
	}
}

<?php
/**
 * Datenbank-Model Klasse für eine Berechtigungs-Rolle
 *
 * @author     Max Sternitzke
 */
class Trainingssystem_Plugin_Database_Permission_Role2Function {

    private $id;
    private $function_id;
    private $role_id;
    private $value;


    public function __construct($_id, $_function_id, $_role_id, $_value = 0){

        $this->id = $_id;
        $this->function_id = $_function_id;
        $this->role_id = $_role_id;
        $this->value = $_value;
    }

    /**
     * @return Integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Integer $_id
     */
    public function setId($_id)
    {
        $this->id = $_id;
    }

    /**
     * @return String
     */
    public function getFunctionId()
    {
        return $this->function_id;
    }

    /**
     * @param String $_function_id
     */
    public function setFunctionId($_function_id)
    {
        $this->function_id = $_function_id;
    }

    /**
     * @return String
     */
    public function getRoleId()
    {
        return $this->role_id;
    }

    /**
     * @param String $_role_id
     */
    public function setRoleId($_role_id)
    {
        $this->role_id = $_role_id;
    }

    /**
     * @return Integer
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param Integer $_value
     */
    public function setValue($_value)
    {
        $this->value = $_value;
    }
}

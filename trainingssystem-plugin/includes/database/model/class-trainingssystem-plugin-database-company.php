<?php

class Trainingssystem_Plugin_Database_Company {

	private $id;
	private $name;
	private $boss;
	private $kAdmin;
    private $groups;
    private $memberCount;


	public function __construct($id=null, $name=null, $boss=null, $kAdmin = null){
		$this->id = $id;
		$this->name = $name;
        $this->boss = $boss;
        $this->kAdmin = $kAdmin;
        $this->memberCount = 0;
	}
		
	public function getId()
    {
        return $this->id;
    }
	
	public function setId($id)
    {
        $this->id = $id;
		return $this;
    }

	public function getName()
    {
        return $this->name;
    }
	
	public function setName($name)
    {
        $this->name = $name;
		return $this;
    }
	
	public function getBoss()
    {
        return $this->boss;
    }
	
	public function setBoss($boss)
    {
        $this->boss = $boss;
		return $this;
    }

    public function getKAdmin()
    {
        return $this->kAdmin;
    }
	
	public function setKAdmin($kAdmin)
    {
        $this->kAdmin = $kAdmin;
		return $this;
    }

    public function getGroups() {
        return $this->groups;
    }

    public function setGroups(Array $groups) {
        $this->groups = $groups;
        return $this;
    }

    public function getMembersCount() {
        $c = 0;
        if($this->memberCount != 0) {
            $c = $this->memberCount;
        } elseif(!is_null($this->groups)) {
            foreach($this->groups as $g) {
                $c += !is_null($g->getMembers()) ? count($g->getMembers()) : 0;
            }
        }
        return $c;
    }

    public function setMemberCount($memberCount) {
        $this->memberCount = $memberCount;
        return $this;
    }
}

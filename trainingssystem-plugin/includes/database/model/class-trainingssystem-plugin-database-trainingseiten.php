<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Markus Domin, Helge Nissen <->
 */
class Trainingssystem_Plugin_Database_Trainingseite {


	private $title;
	private $id;
	private $excerpt;
	private $url;
	private $image_url;
	private $training_auto;
	private $enable;
	private $index;
	private $trainingid;
	private $lektionid;

	/**
	 * Initialize the collections used to maintain the actions and filters.
	 *
	 * @since    1.0.0
	 */

	public function __construct(array $data) {
		$params = array(
			'id' => null,
			'index' => null,
			'title' => null,
			'excerpt' => null,
			'url' => null,
			'image_url' => null,
			'training_auto' => null,
			'enable' => null,
			'trainingid' => null,
			'lektionid' => null
		);    // default params
   if ($data) $data = array_replace($params, $data);

		$this->id = $data["id"];
		$this->index = $data["index"];
		$this->title = $data["title"];
		$this->excerpt = $data["excerpt"];
		$this->url = $data["url"];
		$this->image_url = $data["image_url"];
		$this->training_auto = $data["training_auto"];
		$this->enable = $data["enable"];
		$this->trainingid=$data["trainingid"];
		$this->lektionid=$data["lektionid"];
	}


    /**
     * Get the value of Register all actions and filters for the plugin.
     *
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of Register all actions and filters for the plugin.
     *
     * @param mixed title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Excerpt
     *
     * @return mixed
     */
    public function getExcerpt()
    {
        return $this->excerpt;
    }

    /**
     * Set the value of Excerpt
     *
     * @param mixed excerpt
     *
     * @return self
     */
    public function setExcerpt($excerpt)
    {
        $this->excerpt = $excerpt;

        return $this;
    }

    /**
     * Get the value of Url
     *
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set the value of Url
     *
     * @param mixed url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get the value of Image Url
     *
     * @return mixed
     */
    public function getImageUrl()
    {
        return $this->image_url;
    }

    /**
     * Set the value of Image Url
     *
     * @param mixed image_url
     *
     * @return self
     */
    public function setImageUrl($image_url)
    {
        $this->image_url = $image_url;

        return $this;
    }

    /**
     * Get the value of Training Auto
     *
     * @return mixed
     */
    public function getTrainingAuto()
    {
        return $this->training_auto;
    }

    /**
     * Set the value of Training Auto
     *
     * @param mixed training_auto
     *
     * @return self
     */
    public function setTrainingAuto($training_auto)
    {
        $this->training_auto = $training_auto;

        return $this;
    }

    /**
     * Get the value of Enable
     *
     * @return mixed
     */
    public function getEnable()
    {
        return $this->enable;
    }

    /**
     * Set the value of Enable
     *
     * @param mixed enable
     *
     * @return self
     */
    public function setEnable($enable)
    {
        $this->enable = $enable;

        return $this;
    }


    /**
     * Get the value of Index
     *
     * @return mixed
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * Set the value of Index
     *
     * @param mixed index
     *
     * @return self
     */
    public function setIndex($index)
    {
        $this->index = $index;

        return $this;
    }


    /**
     * Get the value of Trainingid
     *
     * @return mixed
     */
    public function getTrainingid()
    {
        return $this->trainingid;
    }

    /**
     * Set the value of Trainingid
     *
     * @param mixed trainingid
     *
     * @return self
     */
    public function setTrainingid($trainingid)
    {
        $this->trainingid = $trainingid;

        return $this;
    }

    /**
     * Get the value of Lektionid
     *
     * @return mixed
     */
    public function getLektionid()
    {
        return $this->lektionid;
    }

    /**
     * Set the value of Lektionid
     *
     * @param mixed lektionid
     *
     * @return self
     */
    public function setLektionid($lektionid)
    {
        $this->lektionid = $lektionid;

        return $this;
    }

}

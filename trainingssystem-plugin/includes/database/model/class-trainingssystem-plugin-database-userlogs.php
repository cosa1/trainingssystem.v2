<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Markus Domin, Helge Nissen <->
 */
class Trainingssystem_Plugin_Database_Userlogs {

	private $id;
	private $browser_name;
	private $browser_version;
	private $device_type;
	private $engine_name;
	private $os_name;
	private $os_version;
	private $device_model;
	private $device_manufaktur;
	private $pageid;
	private $uri;
	private $date_time;
	private $referred;
	private $ip;
    private $userid;
    private $duration;


    /**
     * Trainingssystem_Plugin_Database_Userlogs constructor.
     * @param $id
     * @param $browser_name
     * @param $browser_version
     * @param $device_type
     * @param $engine_name
     * @param $os_name
     * @param $os_version
     * @param $device_model
     * @param $device_manufaktur
     * @param $pageid
     * @param $uri
     * @param $datetime
     * @param $referred
     * @param $ip
     * @param $userid
     */
    public function __construct($id = null, $browser_name = null, $browser_version = null, $device_type = null, $engine_name = null, $os_name = null, $os_version = null, $device_model = null, $device_manufaktur = null, $pageid = null, $uri = null, $datetime = null, $referred = null, $ip = null, $userid = null, $duration = 0)
    {
        $this->id = $id;
        $this->browser_name = $browser_name;
        $this->browser_version = $browser_version;
        $this->device_type = $device_type;
        $this->engine_name = $engine_name;
        $this->os_name = $os_name;
        $this->os_version = $os_version;
        $this->device_model = $device_model;
        $this->device_manufaktur = $device_manufaktur;
        $this->pageid = $pageid;
        $this->uri = $uri;
        $this->date_time = $datetime;
        $this->referred = $referred;
        $this->ip = $ip;
        $this->userid = $userid;
        $this->duration = $duration;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getBrowserName()
    {
        return $this->browser_name;
    }

    /**
     * @param mixed $browser_name
     */
    public function setBrowserName($browser_name)
    {
        $this->browser_name = $browser_name;
    }

    /**
     * @return mixed
     */
    public function getBrowserVersion()
    {
        return $this->browser_version;
    }

    /**
     * @param mixed $browser_version
     */
    public function setBrowserVersion($browser_version)
    {
        $this->browser_version = $browser_version;
    }

    /**
     * @return mixed
     */
    public function getDeviceType()
    {
        return $this->device_type;
    }

    /**
     * @param mixed $device_type
     */
    public function setDeviceType($device_type)
    {
        $this->device_type = $device_type;
    }

    /**
     * @return mixed
     */
    public function getEngineName()
    {
        return $this->engine_name;
    }

    /**
     * @param mixed $engine_name
     */
    public function setEngineName($engine_name)
    {
        $this->engine_name = $engine_name;
    }

    /**
     * @return mixed
     */
    public function getOsName()
    {
        return $this->os_name;
    }

    /**
     * @param mixed $os_name
     */
    public function setOsName($os_name)
    {
        $this->os_name = $os_name;
    }

    /**
     * @return mixed
     */
    public function getOsVersion()
    {
        return $this->os_version;
    }

    /**
     * @param mixed $os_version
     */
    public function setOsVersion($os_version)
    {
        $this->os_version = $os_version;
    }

    /**
     * @return mixed
     */
    public function getDeviceModel()
    {
        return $this->device_model;
    }

    /**
     * @param mixed $device_model
     */
    public function setDeviceModel($device_model)
    {
        $this->device_model = $device_model;
    }

    /**
     * @return mixed
     */
    public function getDeviceManufaktur()
    {
        return $this->device_manufaktur;
    }

    /**
     * @param mixed $device_manufaktur
     */
    public function setDeviceManufaktur($device_manufaktur)
    {
        $this->device_manufaktur = $device_manufaktur;
    }

    /**
     * @return mixed
     */
    public function getPageid()
    {
        return $this->pageid;
    }

    /**
     * @param mixed $pageid
     */
    public function setPageid($pageid)
    {
        $this->pageid = $pageid;
    }

    /**
     * @return mixed
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param mixed $uri
     */
    public function setUri($uri)
    {
        $this->uri = $uri;
    }

    /**
     * @return mixed
     */
    public function getDatetime()
    {
        return $this->date_time;
    }

    /**
     * @param mixed $date_time
     */
    public function setDatetime($date_time)
    {
        $this->date_time = $date_time;
    }

    /**
     * @return mixed
     */
    public function getReferred()
    {
        return $this->referred;
    }

    /**
     * @param mixed $referred
     */
    public function setReferred($referred)
    {
        $this->referred = $referred;
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param mixed $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return mixed
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * @param mixed $userid
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;
    }

     /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
	 * Initialize the collections used to maintain the actions and filters.
	 *
	 * @since    1.0.0
	 */

}

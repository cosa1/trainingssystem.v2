<?php
/**
 * Datenbank-Model Klasse für ein User Event
 *
 * @author     Max Sternitzke
 */
class Trainingssystem_Plugin_Database_User_Event {

    private $id;
    private $title;
    private $subtitle;
    private $userId;
    private $postId;
    private $url;
    private $eventType;
    private $eventLevel;
	private $origin;
    private $date;

	private $fontAwesome;
	private $imageUrl;


    public function __construct($id, $title, $subtitle, $userId, $postId, $url, $eventType, $eventLevel, $origin, $date) {

        $this->id = $id;
        $this->title = $title;
        $this->subtitle = $subtitle;
        $this->userId = $userId;
        $this->postId = $postId;
        $this->url = $url;
        $this->eventType = $eventType;
        $this->eventLevel = $eventLevel;
		$this->origin = $origin;
        $this->date = $date;
    }

    public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getTitle(){
		return $this->title;
	}

	public function setTitle($title){
		$this->title = $title;
	}

	public function getSubtitle(){
		return $this->subtitle;
	}

	public function setSubtitle($subtitle){
		$this->subtitle = $subtitle;
	}

	public function getUserId(){
		return $this->userId;
	}

	public function setUserId($userId){
		$this->userId = $userId;
	}

	public function getPostId(){
		return $this->postId;
	}

	public function setPostId($postId){
		$this->postId = $postId;
	}

	public function getUrl(){
		return $this->url;
	}

	public function setUrl($url){
		$this->url = $url;
	}

	public function getEventType(){
		return $this->eventType;
	}

	public function setEventType($eventType){
		$this->eventType = $eventType;
	}

	public function getEventLevel(){
		return $this->eventLevel;
	}

	public function setEventLevel($eventLevel){
		$this->eventLevel = $eventLevel;
	}

	public function getOrigin(){
		return $this->origin;
	}

	public function setOrigin($origin){
		$this->origin = $origin;
	}

	public function getDate(){
		return $this->date;
	}

	public function setDate($date){
		$this->date = $date;
	}

	public function getFontAwesome(){
		return $this->fontAwesome;
	}

	public function setFontAwesome($fontawesome){
		$this->fontAwesome = $fontawesome;
	}

	public function isFontAwesome() {
		return !is_null($this->fontAwesome);
	}

	public function getImageUrl(){
		return $this->imageUrl;
	}

	public function setImageUrl($imageUrl){
		$this->imageUrl = $imageUrl;
	}

	public function isImageUrl() {
		return !is_null($this->imageUrl);
	}
}

<?php
/**
 * Created by PhpStorm.
 * User: Ali Parnan
 * Date: 31.01.19
 * Time: 09:38
 */

class Trainingssystem_Plugin_Database_Registerkey{

    private $code;
    private $vorlageid;
    private $vorlageidoptional;
    private $creatorUserId;
    private $createDate;
    private $countCode;
    private $maxCount;
    private $registerlink;
    private $vorlagennutzername;
    private $vorlagennutzername2;
    private $company;
    private $companygroup;
    private $studygroupid;

    /**
     * Trainingssystem_Plugin_Database_Registerkey constructor.
     * @param $code
     * @param $vorlageid
     * @param $creatorUserId
     * @param $createDate
     * @param $countCode
     * @param $maxCount
     * @param registerLink
     */
    public function __construct($code, $vorlageid, $vorlageidoptional, $creatorUserId, $createDate, $countCode, $maxCount, $registerlink, $vorlagennutzername, $vorlagennutzername2, $company, $companygroup, $studygroupid)
    {
        $this->code = $code;
        $this->vorlageid = $vorlageid;
        $this->vorlageidoptional = $vorlageidoptional;
        $this->creatorUserId = $creatorUserId;
        $this->createDate = $createDate;
        $this->countCode = $countCode;
        $this->maxCount = $maxCount;
        $this->registerlink = $registerlink;
        $this->vorlagennutzername = $vorlagennutzername;
        $this->vorlagennutzername2 = $vorlagennutzername2;
        $this->company = $company;
        $this->companygroup = $companygroup;
        $this->studygroupid = $studygroupid;
    }
    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getVorlageid()
    {
        return $this->vorlageid;
    }

    /**
     * @param mixed $vorlageid
     */
    public function setVorlageid($vorlageid)
    {
        $this->vorlageid = $vorlageid;
    }

    /**
     * @return mixed
     */
    public function getVorlageidOptional()
    {
        return $this->vorlageidoptional;
    }

    /**
     * @param mixed $vorlageid
     */
    public function setVorlageidOptional($vorlageidoptional)
    {
        $this->vorlageidoptional = $vorlageidoptional;
    }

    /**
     * @return mixed
     */
    public function getCreatorUserId()
    {
        return $this->creatorUserId;
    }

    /**
     * @param mixed $creatorUserId
     */
    public function setCreatorUserId($creatorUserId)
    {
        $this->creatorUserId = $creatorUserId;
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param mixed $createDate
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    }

    /**
     * @return mixed
     */
    public function getCountCode()
    {
        return $this->countCode;
    }

    /**
     * @param mixed $countCode
     */
    public function setCountCode($countCode)
    {
        $this->countCode = $countCode;
    }

    /**
     * @return mixed
     */
    public function getMaxCount()
    {
        return $this->maxCount;
    }

    /**
     * @param mixed $maxCount
     */
    public function setMaxCount($maxCount)
    {
        $this->maxCount = $maxCount;
    }

    public function getRegisterlink() 
    {
        return $this->registerlink;
    }

    public function setRegisterlink($registerlink)
    {
        $this->registerlink = $registerlink;
    }

    public function getVorlagennutzerName() 
    {
        return $this->vorlagennutzername;
    }

    public function setVorlagennutzerName($vorlagennutzername)
    {
        $this->vorlagennutzername = $vorlagennutzername;
    }

    public function getVorlagennutzerName2() 
    {
        return $this->vorlagennutzername2;
    }

    public function setVorlagennutzerName2($vorlagennutzername2)
    {
        $this->vorlagennutzername2 = $vorlagennutzername2;
    }

    public function getCompany() 
    {
        return $this->company;
    }

    public function setCompany($company)
    {
        $this->company = $company;
    }

    public function getCompanygroup() 
    {
        return $this->companygroup;
    }

    public function setCompanygroup($companygroup)
    {
        $this->companygroup = $companygroup;
    }

    public function getStudygroupid() 
    {
        return $this->studygroupid;
    }

    public function setStudygroupid($studygroupid)
    {
        $this->studygroupid = $studygroupid;
    }
}
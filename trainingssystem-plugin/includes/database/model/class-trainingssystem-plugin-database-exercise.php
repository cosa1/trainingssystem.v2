<?php
/**
 * Datenbank-Model Klasse für eine Übung aka Exercise
 *
 * @author     Max Sternitzke
 */
class Trainingssystem_Plugin_Database_Exercise {

    private $id;
    private $exercise_id;
    private $user_id;
    private $favorite_date;
    private $training_id;
    private $lektion_id;
    private $seiten_id;
    private $url;
	private $title;
	private $content;


    public function __construct($_id, $_exercise_id, $_userid, $_favorite_date, $_training_id, $_lektion_id, $_seiten_id, $_url, $_title = "", $_content = ""){
        $this->id = $_id;
        $this->exercise_id = $_exercise_id;
        $this->user_id = $_userid;
        $this->favorite_date = $_favorite_date;
        $this->training_id = $_training_id;
        $this->lektion_id = $_lektion_id;
        $this->seiten_id = $_seiten_id;
        $this->url = $_url;
		$this->title = $_title;
		$this->content = $_content;
    }

    public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getExercise_id(){
		return $this->exercise_id;
	}

	public function setExercise_id($exercise_id){
		$this->exercise_id = $exercise_id;
	}

	public function getUser_id(){
		return $this->user_id;
	}

	public function setUser_id($user_id){
		$this->user_id = $user_id;
	}

	public function getFavorite_date(){
		return $this->favorite_date;
	}

    public function getFavorite_dateFormatted() {
        return date('j\.n\.Y G\:i \U\h\r', strtotime($this->favorite_date));
    }

	public function setFavorite_date($favorite_date){
		$this->favorite_date = $favorite_date;
	}

	public function getTraining_id(){
		return $this->training_id;
	}

	public function setTraining_id($training_id){
		$this->training_id = $training_id;
	}

	public function getLektion_id(){
		return $this->lektion_id;
	}

	public function setLektion_id($lektion_id){
		$this->lektion_id = $lektion_id;
	}

	public function getSeiten_id(){
		return $this->seiten_id;
	}

	public function setSeiten_id($seiten_id){
		$this->seiten_id = $seiten_id;
	}

	public function getUrl(){
		return $this->url;
	}

	public function setUrl($url){
		$this->url = $url;
	}

	public function getTitle(){
		return $this->title;
	}

	public function setTitle($title){
		$this->title = $title;
	}

	public function getContent(){
		return $this->content;
	}

	public function setContent($content){
		$this->content = $content;
	}
}

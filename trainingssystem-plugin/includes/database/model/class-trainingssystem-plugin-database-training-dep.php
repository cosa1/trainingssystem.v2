<?php
/**
 * Datenbank-Modell für die trainings_date Tabelle
 *
 * @author  Max Sternitzke <max.sternitzke@th-luebeck.de>
 */
class Trainingssystem_Plugin_Database_Training_Dep {


	private $id;
	private $trainingId;
	private $trainingMode;
	private $ecoachingEnable;
	private $lektionSettings;
    private $userId;
    private $timeAdd;
	private $timeMultiplier;
	private $dependentTrainings;
	private $dependentCondition;
	private $redirect;
    private $createdBy;
    private $createdAt;

	public function __construct($id, $trainingId, $trainingMode, $ecoachingEnable, $lektionSettings, $userId, $timeAdd, $timeMultiplier, $dependentTrainings, $dependentCondition, $redirect, $createdBy, $createdAt) {
		$this->id = $id;
		$this->trainingId = $trainingId;
		$this->trainingMode = $trainingMode;
		$this->ecoachingEnable = $ecoachingEnable;
		$this->lektionSettings = $lektionSettings;
		$this->userId = $userId;
        $this->timeAdd = $timeAdd;
		$this->timeMultiplier = $timeMultiplier;
		$this->dependentTrainings = $dependentTrainings;
		$this->dependentCondition = $dependentCondition;
		$this->redirect = $redirect;
        $this->createdBy = $createdBy;
        $this->createdAt = $createdAt;
	}

    public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getTrainingId(){
		return $this->trainingId;
	}

	public function setTrainingId($trainingId){
		$this->trainingId = $trainingId;
	}

	public function getTrainingMode(){
		return $this->trainingMode;
	}

	public function setTrainingMode($trainingMode){
		$this->trainingMode = $trainingMode;
	}

	public function getEcoachingEnable(){
		return $this->ecoachingEnable;
	}

	public function setEcoachingEnable($ecoachingEnable){
		$this->ecoachingEnable = $ecoachingEnable;
	}

	public function getlektionSettings(){
		return $this->lektionSettings;
	}

	public function setlektionSettings($lektionSettings){
		$this->lektionSettings = $lektionSettings;
	}

	public function lektionSettingsIsArray() {
		if(is_array($this->lektionSettings)) {
			return true;
		}
        if(strpos($this->lektionSettings, "[") === false && strpos($this->lektionSettings, "{") === false) {
            return false;
        }

        json_decode($this->lektionSettings);
        if(json_last_error() == JSON_ERROR_NONE) {
            return true;
        }
        return false;
    }

    public function getLektionSettingsJsonDecoded() {
		return (is_array($this->lektionSettings)) ? $this->lektionSettings : json_decode(trim($this->lektionSettings), true);
    }

	public function lektionSettingsIsDifferent() {
		if(!$this->lektionSettingsIsArray()) {
			return false;
		}
		$old_val = $this->getLektionSettingsJsonDecoded()[array_key_first($this->getLektionSettingsJsonDecoded())];
		foreach($this->getLektionSettingsJsonDecoded() as $val) {
			if($old_val == $val) {
				continue;
			}
			return true;
		}
		return false;
	}

	public function getUserId(){
		return $this->userId;
	}

	public function setUserId($userId){
		$this->userId = $userId;
	}

	public function getTimeAdd(){
		return $this->timeAdd;
	}

	public function setTimeAdd($timeAdd){
		$this->timeAdd = $timeAdd;
	}

	public function getTimeMultiplier(){
		return $this->timeMultiplier;
	}

	public function setTimeMultiplier($timeMultiplier){
		$this->timeMultiplier = $timeMultiplier;
	}

	public function getDependentTrainings(){
		return $this->dependentTrainings;
	}

    public function dependentTrainingsIsArray() {
        if(strpos($this->dependentTrainings, "[") === false && strpos($this->dependentTrainings, "{") === false) {
            return false;
        }

        json_decode($this->dependentTrainings);
        if(json_last_error() == JSON_ERROR_NONE) {
            return true;
        }
        return false;
    }

	public function getDependentTrainingsJsonDecoded(){
		return $this->dependentTrainingsIsArray() ? json_decode(trim($this->dependentTrainings), true) : array();
	}

	public function hasDependentTraining($tid){

		if(!$this->dependentTrainingsIsArray()) return false;
		
		$depTrainings = json_decode(trim($this->dependentTrainings), true);

		foreach($depTrainings as $depTraining) {
			if($depTraining == $tid) 
				return true;
		}
		return false;
	}

	public function setDependentTrainings($dependentTrainings){
		$this->dependentTrainings = $dependentTrainings;
	}

	public function getDependentCondition(){
		return $this->dependentCondition;
	}

	public function setDependentCondition($dependentCondition){
		$this->dependentCondition = $dependentCondition;
	}

	public function getRedirect(){
		return $this->redirect;
	}

	public function setRedirect($redirect){
		$this->redirect = $redirect;
	}

	public function getCreatedBy(){
		return $this->createdBy;
	}

	public function setCreatedBy($createdBy){
		$this->createdBy = $createdBy;
	}

	public function getCreatedAt(){
		return $this->createdAt;
	}

	public function setCreatedAt($createdAt){
		$this->createdAt = $createdAt;
	}

	public function toJson($anonym = false) {
		$ret = [];

		if(!$anonym) {
			$ret['id'] = $this->id;
		}
		$ret['trainingId'] = $this->trainingId;
		$ret['trainingMode'] = $this->trainingMode;
		$ret['ecoachingEnable'] = $this->ecoachingEnable;
		$ret['lektionSettings'] = $this->lektionSettings;
		if(!$anonym) {
			$ret['userId'] = $this->userId;
		}
		$ret['timeAdd'] = $this->timeAdd;
		$ret['timeMultiplier'] = $this->timeMultiplier;
		$ret['dependentTrainings'] = $this->dependentTrainings;
		$ret['dependentCondition'] = $this->dependentCondition;
		$ret['redirect'] = $this->redirect;

		return json_encode($ret);
	}
}
<?php

/**
 * @author     Max Sternitzke <max.sternitzke@th-luebeck.de>
 */
interface Trainingssystem_Plugin_Database_Training_Dep_Dao extends Trainingssystem_Plugin_Database_Dao{

	public function iniDB();
	public function updating();
}

<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Markus Domin, Helge Nissen <->
 */
interface Trainingssystem_Plugin_Database_Nutzer_Dao extends Trainingssystem_Plugin_Database_Dao{

	public function iniDB();
	public function updating();
	public function getAllNutzer();
	public function getNutzer($id);
	public function getNutzers($ids);
	public function updateNutzer($nutzer);
	public function deleteNutzer($user_id);
    public function maybeUpdateLastPageOfLektion($lastpage, $userid, $usermodus);
	public function checkpermissions($trainingsID,$userid,$postID, $canSeeHiddenTrainings);
	public function getTrainings($userid);
	public function setTraining($training);
	public function setTrainingByUserId($userid, $training);
	public function removeAllTrainings($userid);

}

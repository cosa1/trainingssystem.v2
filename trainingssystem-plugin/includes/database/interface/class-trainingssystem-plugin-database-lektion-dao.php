<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Markus Domin, Helge Nissen <->
 */
interface Trainingssystem_Plugin_Database_Lektion_Dao extends Trainingssystem_Plugin_Database_Dao{

	public function updating();
	public function getAllLektion();
	public function getLektion($trainingsID);
	public function getLektion2($trainingsID);
	public function getLektionenSeitenAutostartUser($trainingsID, $postID, $userid);
	public function getAllLektionen();
	public function getLektionByID($id);
	
}

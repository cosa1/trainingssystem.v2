<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Markus Domin, Helge Nissen <->
 */
interface Trainingssystem_Plugin_Database_Company_Dao extends Trainingssystem_Plugin_Database_Dao {

	public function iniDB();
	public function updating();
	public function getAllCompanies();
	public function getMyCompanies($user_id);
	public function getCompanyById($id);
	public function insertCompany($name, $bossid, $kAdminId);
	public function deleteCompany($id);
	public function updateCompany($company_id, $company_name, $boss_id, $kAdminId);
	
	public function getGroups($company_id);
	
}

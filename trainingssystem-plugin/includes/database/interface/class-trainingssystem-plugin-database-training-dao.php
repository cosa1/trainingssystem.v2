<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run public function to execute the list of actions and filters.
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Markus Domin, Helge Nissen <->
 */
interface Trainingssystem_Plugin_Database_Training_Dao extends Trainingssystem_Plugin_Database_Dao{

	public function iniDB();
	public function updating();
	public function getAllTrainings();
	public function getTrainingById($id);
	public function getTraining($id);
	public function getTrainingByUser($id, $userid);
	public function updateTraining($training);
	public function deleteTraining($id);
	public function insertTraining($training);
	public function createTrainingFull($training);
	public function clearTrainingyById($id);
	public function saveTrainingyById($id,$lektionen);
}

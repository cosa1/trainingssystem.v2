<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Markus Domin, Helge Nissen <->
 */
interface Trainingssystem_Plugin_Database_Companygroup_Dao extends Trainingssystem_Plugin_Database_Dao{

	public function iniDB();
	public function updating();
	public function insertCompanygroup($name, $company_id);
	public function insertCompanygroupWithMembers($groupName, $companyId, $members);
	public function updateCompanygroup($id, $name);
	public function deleteCompanygroup($id);
	public function getCompanygroupById($group_id);
	public function getMembers($groupId);
	
}

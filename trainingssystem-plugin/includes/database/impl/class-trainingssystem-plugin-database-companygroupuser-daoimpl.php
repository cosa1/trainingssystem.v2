<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Markus Domin, Helge Nissen <->
 */
class Trainingssystem_Plugin_Database_CompanygroupUser_Daoimple implements Trainingssystem_Plugin_Database_CompanygroupUser_Dao{

        public static $dbprefix = TRAININGSSYSTEM_PLUGIN_DB_GRUPPE_2_USER;

        public function __construct(){
            $this->iniDB();

        }

        public function iniDB(){

            global $wpdb;
            $table_name = $wpdb->prefix . self::$dbprefix;
            $charset_collate = $wpdb->get_charset_collate();
            $tableversionsstr =[
                "CREATE TABLE IF NOT EXISTS $table_name(
                    gruppenid INT NOT NULL ,
                    userid INT NOT NULL
                    )$charset_collate;"
            ];
            Trainingssystem_Plugin_Database_Coach_impl::updateDBTable(self::$dbprefix,$table_name, $tableversionsstr);
 
        }

    /**
     * database updating beim plugin update
     *
     * @since    1.0.0
     */
	public function updating(){

	}
	
	/** 
	 *  Teilnehmer in Gruppen einteilen 
	 **/
    public function addUserToGroup($userId, $groupId){
        global $wpdb;
		$table_name = $wpdb->prefix . $this::$dbprefix;

		return $wpdb->insert($table_name, array('gruppenid' => $groupId, 'userid' => $userId));
	}
	
	/**
	 *	Teilnehmer aus Gruppen l�schen
	 **/
	public function deleteUserFromGroup($userId, $groupId){
        global $wpdb;
		$table_name = $wpdb->prefix . $this::$dbprefix;
		$wpdb->delete($table_name, array('gruppenid' => $groupId, 'userid' => $userId));
	}
	
	public function emptyGroup($groupId) {
		global $wpdb;
		$table_name = $wpdb->prefix . $this::$dbprefix;
		$wpdb->delete($table_name, array('gruppenid' => $groupId));
	}


    public function getGroupIdByUserId($userId){
        global $wpdb;
        $table_name = $wpdb->prefix . $this::$dbprefix;

        $sql = $wpdb->prepare("SELECT * FROM $table_name where userid = %d", $userId);
		return $wpdb->get_row($sql);
    }


	public function getUserIdsByGroupId($groupId){
		global $wpdb;
        $table_name = $wpdb->prefix . $this::$dbprefix;

        $sql = $wpdb->prepare("SELECT userid FROM $table_name WHERE gruppenid = %d GROUP BY userid", $groupId);
		return $wpdb->get_col($sql);
	}

    function deleteUserInput($user_id){
        global $wpdb;
        $datatable=$wpdb->prefix.$this::$dbprefix;
        $wpdb->delete( $datatable, array( 'userid' => $user_id ) );
    }

    /**
     * Get all User-IDs by boss-ID of company
     * 
     * @param Int Boss-ID
     * 
     * @return Array of Integers
     */
    public function getUserIdsByBossId($bossid) {
        global $wpdb;

        $table_comp = $wpdb->prefix . Trainingssystem_Plugin_Database_Company_Daoimple::$dbprefix;
        $table_gr = $wpdb->prefix . Trainingssystem_Plugin_Database_Companygroup_Daoimple::$dbprefix;
		$table_g2u = $wpdb->prefix . $this::$dbprefix;

        $sql = $wpdb->prepare("SELECT userid FROM $table_g2u INNER JOIN $table_gr ON $table_g2u.gruppenid = $table_gr.id INNER JOIN $table_comp ON $table_gr.firmaid = $table_comp.id WHERE $table_comp.bossid = %d", $bossid);
        $data = $wpdb->get_results($sql, OBJECT);

        if($data == null) return null;

        $userids = array();
        foreach($data as $row) {
            $userids[] = $row->userid;
        }
        return $userids;
    }
}

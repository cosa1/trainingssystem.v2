<?php
/**
 * Functions for Permissions Rol2Function Database Table
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Max Sternitzke
 */
class Trainingssystem_Plugin_Database_Permission_Role2Function_Daoimple implements Trainingssystem_Plugin_Database_Permission_Role2Function_Dao {

    public static $dbprefix = TRAININGSSYSTEM_PLUGIN_DB_PERMISSION_ROLE2FUNCTION;

    public function __construct(){
        $this->iniDB();
    }

    public function iniDB(){
        global $wpdb;

        $table_name = $wpdb->prefix . self::$dbprefix;

        $charset_collate = $wpdb->get_charset_collate();

        $tableversionsstr =[
            "CREATE TABLE IF NOT EXISTS $table_name (
                `ID` int(20) NOT NULL AUTO_INCREMENT,
                `function_id` varchar(128) NOT NULL,
                `role_id` varchar(128) NOT NULL,
                `value` int(20) NOT NULL DEFAULT 0,
                PRIMARY KEY (`ID`)
              )$charset_collate;"
        ];
        Trainingssystem_Plugin_Database_Coach_impl::updateDBTable(self::$dbprefix,$table_name, $tableversionsstr);
    }

    /**
     * database updating beim plugin update
     *
     * @since    1.0.0
     */
	public function updating(){

    }

    /**
     * Get the Permissions for a Role
     * 
     * @param String role_id
     * 
     * @return Array of Trainingssystem_Plugin_Database_Permission_Role2Function Objects
     */
    public function getPermissions($role_id) {
        global $wpdb;
        $table_name = $wpdb->prefix . self::$dbprefix;

        $result = [];

        if(!empty($role_id)) {
            $data = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE role_id = %s", $role_id), OBJECT);

            if($data != null) {
                foreach ($data as $row){

                    $result[] = new Trainingssystem_Plugin_Database_Permission_Role2Function($row->ID, $row->function_id, $row->role_id, $row->value);
                }
            }
        }
        return $result;
    }

    /**
     * Löscht alle Berechtigungs-Einträge aus der Zuordnungstabelle role2function mit eine gegebene Rollen-ID
     * 
     * @param String role_id
     * 
     * @return boolean success
     */
    public function deleteByRole($role_id) {
        global $wpdb;
        $table_name = $wpdb->prefix . self::$dbprefix;

        $sql = $wpdb->prepare("DELETE FROM $table_name WHERE role_id = %s", $role_id);

        return $wpdb->query($sql) !== false;
    }

    /**
     * Fügt eine Berechtigung in die Zuordnungstabelle ein
     * 
     * @param String role_id
     * @param String function_id
     * @param Integer value
     * 
     * @return boolean success
     */
    public function insertPermission($role_id, $function_id, $value) {
        global $wpdb;
        $table_name_data = $wpdb->prefix . self::$dbprefix;

        $sql = $wpdb->insert($table_name_data,
                                array(
                                    "role_id" => $role_id,
                                    "function_id" => $function_id,
                                    "value" => $value,
                                ),
                                array("%s", "%s", "%d")
                            );
        return $sql !== false;
    }

    /**
     * Fügt eine neue Berechtigungs-Zuordnung aus einem Trainingssystem_Plugin_Database_Permission_Role2Function-Objekt in die Datenbank ein
     * 
     * @param Trainingssystem_Plugin_Database_Permission_Role2Function Berechtigungs-Zuordnung
     * 
     * @return boolean success
     */
    public function insertPermissionObject(Trainingssystem_Plugin_Database_Permission_Role2Function $r2f) {
        return $this->insertPermission($r2f->getRoleId(), $r2f->getFunctionId(), $r2f->getValue());
    }

    /**
     * Clears the table
     * 
     * @return boolean success
     */
    public function clearTable() {
        global $wpdb;
        $table_name = $wpdb->prefix . self::$dbprefix;

        $sql = "DELETE FROM $table_name";

        return $wpdb->query($sql) !== false;
    }
}

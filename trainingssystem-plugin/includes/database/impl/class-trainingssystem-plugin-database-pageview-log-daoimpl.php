<?php

/**
 * Register all actions and filters for the plugin.
 *
 * @link       -
 * @since      1.0.0
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @author     Markus Domin, Helge Nissen <->
 */
class Trainingssystem_Plugin_Loader
{
    /**
     * Initialize the collections used to maintain the actions and filters.
     *
     * @since    1.0.0
     */
    public function __construct()
    {
    }

    public function iniDB()
    {
			//create table für saveuserpageview()

			
    }

    /**
     * database updating beim plugin update
     *
     * @since    1.0.0
     */
	public function updating(){

	}

    /**
     * soll alls action beim page aufruf gestartet werden und in die datenbank dann Userid & Pageid sowie zeitstemple speichern.
     *
     * @return [type] [description]
     */
    public function saveuserpageview()
    {

    }
}

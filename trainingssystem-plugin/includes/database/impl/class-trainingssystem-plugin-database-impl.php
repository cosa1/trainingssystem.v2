<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Markus Domin, Helge Nissen <->
 */
class Trainingssystem_Plugin_Database_Coach_impl{

	/**
	 * Initialize the collections used to maintain the actions and filters.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

	}

	public static function updateDBTable($tableoptionname,$table_name, $tableversionsstr){

		global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();
        require_once ABSPATH . 'wp-admin/includes/upgrade.php';
        $table_version = count($tableversionsstr)-1; //muss hochgezählt werden
        $installed_ver = get_option(TRAININGSSYSTEM_PLUGIN_DBUPDATE.$tableoptionname);
        if (!$installed_ver) {
            $installed_ver = 0; //datenbank neue anlegen
        }else{
            $installed_ver++;
        }

 /*nur für debug zwecke*/ 
 //$installed_ver = 0;
 /*ende debug*/

 //nur der erste befehl im Array wird mit bdDelta ausgeführt alle anderen mit wpdb->query,
 // Das bedeutet das nur der erste befehl zum anlegen der Tabelle genutzt werden kann.
        for($i = $installed_ver;$i<=$table_version;$i++){
            //error_log($tableversionsstr[$i]);
           // error_log( "Version=".$table_version );
            $log=null;
            if($i<1)
            $log= dbDelta($tableversionsstr[$i]);
            else
            $log= $wpdb->query( $tableversionsstr[$i] );

            if ( is_array( $log ) || is_object( $log ) ) {
                //error_log( print_r( $log, true ) );
             } else {
                //error_log( $log );
             }
       
        }
        update_option(TRAININGSSYSTEM_PLUGIN_DBUPDATE.$tableoptionname, $table_version);

	}
}

<?php
/**
 * Functions for Permissions Function Database Table
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Max Sternitzke
 */
class Trainingssystem_Plugin_Database_Permission_Function_Daoimple implements Trainingssystem_Plugin_Database_Permission_Function_Dao {

    public static $dbprefix = TRAININGSSYSTEM_PLUGIN_DB_PERMISSION_FUNCTION;

    public function __construct(){
        $this->iniDB();
    }

    public function iniDB(){
        global $wpdb;

        $table_name = $wpdb->prefix . self::$dbprefix;

        $charset_collate = $wpdb->get_charset_collate();

        $tableversionsstr =[
            "CREATE TABLE IF NOT EXISTS $table_name (
                `ID` varchar(128) NOT NULL,
                `title` varchar(256) NOT NULL,
                `description` text NOT NULL,
                PRIMARY KEY (`ID`)
              )$charset_collate;"
        ];
        Trainingssystem_Plugin_Database_Coach_impl::updateDBTable(self::$dbprefix,$table_name, $tableversionsstr);
    }

    /**
     * database updating beim plugin update
     *
     * @since    1.0.0
     */
	public function updating(){

    }

    /**
     * Fügt eine neue Funktion in die Datenbank ein
     * 
     * @param id
     * @param name
     * @param description
     * 
     * @return boolean success
     */
    public function insertFunction($id, $title, $description) {
        global $wpdb;
        $table_name_data = $wpdb->prefix . self::$dbprefix;

        $sql = $wpdb->insert($table_name_data,
                                array(
                                    "ID" => $id,
                                    "title" => $title,
                                    "description" => $description,
                                ),
                                array("%s", "%s", "%s")
                            );
        return $sql !== false;
    }

    /**
     * Fügt eine neue Funktion aus einem Trainingssystem_Plugin_Database_Permission_Function-Objekt in die Datenbank ein
     * 
     * @param Trainingssystem_Plugin_Database_Permission_Function Funktion
     * 
     * @return boolean success
     */
    public function insertFunctionObject(Trainingssystem_Plugin_Database_Permission_Function $function) {
        return $this->insertFunction($function->getId(), $function->getTitle(), $function->getDescription());
    }

    /**
     * Gibt alle Funktionen als Array von Trainingssystem_Plugin_Database_Permission_Function Objekten zurück
     * 
     * @return Array
     */
    public function getAllFunctions() {
        global $wpdb;
        $table_name = $wpdb->prefix . self::$dbprefix;

        $data = $wpdb->get_results("SELECT ID, title, description FROM $table_name ORDER BY title ASC", OBJECT);
        $result = [];

        if($data != null) {
            foreach ($data as $row){

                $result[] = new Trainingssystem_Plugin_Database_Permission_Function($row->ID, $row->title, $row->description);
            }
        }
        return $result;
    }

    /**
     * Gibt alle Funktionen als Array von Trainingssystem_Plugin_Database_Permission_Function Objekten zurück, 
     * deren ID im Array function_ids vorhanden ist
     * 
     * @param Array function_ids
     * 
     * @return Array
     */
    public function getFunctionsById($function_ids) {
        global $wpdb;
        $table_name = $wpdb->prefix . self::$dbprefix;
        $result = [];

        if(!empty($function_ids)) {
            $data = $wpdb->get_results("SELECT ID, title, description FROM $table_name WHERE ID in (" . implode(",", $function_ids) . ") ORDER BY title ASC", OBJECT);

            if($data != null) {
                foreach ($data as $row){

                    $result[] = new Trainingssystem_Plugin_Database_Permission_Function($row->ID, $row->title, $row->description);
                }
            }
        }
        return $result;
    }

    /**
     * Clears the table
     * 
     * @return boolean success
     */
    public function clearTable() {
        global $wpdb;
        $table_name = $wpdb->prefix . self::$dbprefix;

        $sql = "DELETE FROM $table_name";

        return $wpdb->query($sql) !== false;
    }
}

<?php

/**
 * Register all actions and filters for the plugin.
 *
 * @link       -
 * @since      1.0.0
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @author Ali Parnan
 * Implementation of evaluation system in the database
 */
class Trainingssystem_Plugin_Database_Bewertung_Daoimple
{
    public static $dbprefix = TRAININGSSYSTEM_PLUGIN_DB_TRAINING_BEWERTUNG;
    
    /**
     * Initialize the collections used to maintain the actions and filters.
     *
     * @since    1.0.0
     */
    public function __construct()
    {
        $this->iniDB();
    }

    /**
     * Initialize the collections used to maintain the actions and filters.
     *
     * @since    1.0.0
     */
    public function iniDB()
    {       



        global $wpdb;
        $table_name = $wpdb->prefix .''. self::$dbprefix;
        $charset_collate = $wpdb->get_charset_collate();
        $tableversionsstr =[
            "CREATE TABLE IF NOT EXISTS $table_name(
                bewertung DOUBLE NOT NULL ,
                pageid INT NOT NULL ,
                userid INT NOT NULL ,
                datum DATETIME NOT NULL,
                PRIMARY KEY (pageid,userid)
                )$charset_collate;"
        ];

        Trainingssystem_Plugin_Database_Coach_impl::updateDBTable(self::$dbprefix,$table_name, $tableversionsstr);
    }

    /**
     * database updating
     *
     * @since    1.0.0
     */
    public function updating()
    {

    }
    /**
     * save rating
     *
     * @since    1.0.0
     */
    public function save($pageid,$bewertung)
    {
        global $wpdb;
        $table_name = $wpdb->prefix.self::$dbprefix;


        $erfolgreich = $wpdb->insert(
            $table_name,
            array(
                'pageid' => $pageid,
                'bewertung' => $bewertung,
                'datum' => current_time('mysql'),
                'userid' => get_current_user_id()
            ),
            array(
                '%d',
                '%s',
                '%s',
                '%d'
            )
        );
        if(!$erfolgreich){
            $wpdb->update(
                $table_name,
                array(

                    'bewertung' => $bewertung,
                    'datum' => current_time('mysql')
                ),
                array(
                    'pageid' => $pageid,	// string
                    'userid' => get_current_user_id()
                ),
                array(
                    '%s',
                    '%s'
                ),
                array(
                    '%d',
                    '%d'
                )

            );
        }

    }

    /**
     * @return mixed
     * get ratings from the database
     *
     */
    public function getRating(){
        global $wpdb;
        $table_name = $wpdb->prefix.self::$dbprefix;

        $result = $wpdb->get_results("SELECT pageid, systempost.post_title, AVG(bewertung) as rating FROM $table_name, $wpdb->posts as systempost WHERE systempost.id = pageid GROUP BY pageid");
        //($wpdb->last_query);
        return $result;

    }
    function deleteUserInput($user_id){
        global $wpdb;
        $datatable=$wpdb->prefix.$this::$dbprefix;
        $wpdb->delete( $datatable, array( 'userid' => $user_id ) );
    }

}

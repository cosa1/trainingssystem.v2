<?php

/**
 * Register all actions and filters for the plugin.
 *
 * @link       -
 * @since      1.0.0
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @author     Markus Domin, Helge Nissen <->
 */
class Trainingssystem_Plugin_Database_Lektion_Daoimple implements Trainingssystem_Plugin_Database_Lektion_Dao
{
    public static $dbprefix = TRAININGSSYSTEM_PLUGIN_DB_TRAINING_USER;
    public static $dbprefixlastlogin = TRAININGSSYSTEM_PLUGIN_DB_TRAINING_LASTLOGIN;
    public static $dbprefixseiten = TRAININGSSYSTEM_PLUGIN_DB_TRAINING_SEITEN;

    /**
     * Initialize the collections used to maintain the actions and filters.
     *
     * @since    1.0.0
     */
    public function __construct()
    {
    }
    public function iniDB(){
        
    }
    public function updating(){

    }
    /**
     * [getAllLektion description]
     * @return [type] [description]
     */
    public function getAllLektion()
    {

    }

    public function getLektion($trainingsID)
    {
        global $wpdb;
        $tablename = $wpdb->prefix . TRAININGSSYSTEM_PLUGIN_DB_TRAINING_LEKTIONEN;
        $sql = $wpdb->prepare("SELECT * FROM $tablename where training_id = %d ORDER BY lektion_index", $trainingsID);
        $result = $wpdb->get_results($sql);

        $lektionarr = array();
        foreach ($result as $key => $value) {
            $post = get_post($value->lektion_id);
            $url = add_query_arg(array('idt1' => $trainingsID, 'idt2' => $value->lektion_id), get_permalink($value->lektion_id)); //esc_url(add_query_arg(array('idt1' => $trainingsID, 'idt2' => $value->lektion_id), get_permalink($value->lektion_id)));//get_permalink( $id );
            $imageurl = null;
            if (has_post_thumbnail($post->ID)) {
                $imageurl = get_the_post_thumbnail_url($post->ID); //, 'full' );
            }

            $lektionarr[] = new Trainingssystem_Plugin_Database_Lektion($value->lektion_id,
                $post->post_title, $post->post_excerpt, $url, $imageurl);
        }

        return $lektionarr;
    }

    public function getLektion2($trainingsID)
    {
        global $wpdb;
        $tablename = $wpdb->prefix . TRAININGSSYSTEM_PLUGIN_DB_TRAINING_LEKTIONEN;
        $sql = $wpdb->prepare("SELECT * FROM $tablename where training_id = %d ORDER BY lektion_index", $trainingsID);
        $result = $wpdb->get_results($sql);

        $lektionarr = array();
        foreach ($result as $key => $value) {
            $post = get_post($value->lektion_id);
            $url = add_query_arg(array('idt1' => $trainingsID, 'idt2' => $value->lektion_id), get_permalink($value->lektion_id)); //esc_url(add_query_arg(array('idt1' => $trainingsID, 'idt2' => $value->lektion_id), get_permalink($value->lektion_id)));//get_permalink( $id );
            $imageurl = null;
            if (has_post_thumbnail($post->ID)) {
                $imageurl = get_the_post_thumbnail_url($post->ID); //, 'full' );
            }

            $seiten = Trainingssystem_Plugin_Database::getInstance()->TrainingseitenDao->getLektionenSeiten($trainingsID, $value->lektion_id);
            $seiteurl = "";
            if (count($seiten) > 0) {
                $seiteurl = $seiten[0]->getUrl();
            }

            $lektionarr[] = new Trainingssystem_Plugin_Database_Lektion($value->lektion_id,
                $post->post_title, $post->post_excerpt, $url, $imageurl, null, null, null, $seiten, $seiteurl);
        }

        return $lektionarr;
    }

    /**
     * [getLektionenSeitenAutostartUser description]
     * @param  [type] $trainingsID [description]
     * @param  [type] $postID      [description]
     * @param  [type] $userid      [description]
     * @return [type]              [description]
     */
    public function getLektionenSeitenAutostartUser($trainingsID, $postID, $userid)
    {
        global $wpdb;

        $tablename = $wpdb->prefix . TRAININGSSYSTEM_PLUGIN_DB_TRAINING_USER;
        $tablenametrainingsseiten = $wpdb->prefix . TRAININGS_MANAGER_PLUGIN_DB_TRAININGS_SEITEN;
        $sql = $wpdb->prepare("SELECT nutzer.*, seiten.*
						FROM $tablename as nutzer
						LEFT JOIN $tablenametrainingsseiten as seiten
						ON nutzer.lektion_id = seiten.lektion_id
						WHERE nutzer.training_id=%d
						AND seiten.seiten_index=0
						AND nutzer.user_id = %d
						ORDER BY nutzer.lektion_index ASC", $trainingsID, $userid);

        $result = $wpdb->get_results($sql);
        //$output .= '<ul class="lektionliste keinlink oben30">';

        $lektionarr = array();
        foreach ($result as $key => $value) {
            $post = get_post($value->lektion_id);
            $url = esc_url(add_query_arg(array('idt1' => $trainingsID, 'idt2' => $value->lektion_id), get_permalink($value->seiten_id)));
            $imageurl = null;
            if (has_post_thumbnail($id)) {
                $imageurl = get_the_post_thumbnail_url($id); //, 'full' );
            }
            $lektionarr = new Trainingssystem_Plugin_Database_Lektion($value->lektion_id,
                $post->post_title, $post->post_excerpt, $url, $imageurl, null, $value->training_auto, $value->lektion_enable);
        }

        return $lektionarr;
    }

    public function getAllLektionen($indexedArray = false)
    {

        $query = new WP_Query(array('post_type' => 'lektionen', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC'));
        $posts = $query->posts;

        $lektionarr = array();
        foreach ($posts as $key => $value) {
            $post = get_post($value->ID);
            $url = null;
            $imageurl = null;
            $lektion = new Trainingssystem_Plugin_Database_Lektion($value->ID,
                $post->post_title, $post->post_excerpt, $url, $imageurl, null, null, null);
            if($indexedArray) {
                $lektionarr[$value->ID] = $lektion;
            } else {
                $lektionarr[] = $lektion;
            }
        }

        return $lektionarr;

    }

    public function getAllLektionenUnused() 
    {
        $allLektionen = $this->getAllLektionen(true);
        $usedLektionenIDs = Trainingssystem_Plugin_Database::getInstance()->TrainingseitenDao->getAllLektionIDs();

        $ret = [];

        foreach($allLektionen as $lektion) {
            if(!in_array($lektion->getId(), $usedLektionenIDs)) {
                $ret[] = $lektion;
            }
        }

        return $ret;
    }

    public function getLektionByID($id)
    {
        $post = get_post($id);
        $url = null;
        $imageurl = null;
        return new Trainingssystem_Plugin_Database_Lektion($id,
            $post->post_title, $post->post_excerpt, $url, $imageurl, null, null, null);
    }

    public function getLektionTitle() {
        global $wpdb;

        $sql = "SELECT ID, post_title FROM $wpdb->posts WHERE post_type = 'lektionen' AND post_status = 'publish'";
        $data = $wpdb->get_results($sql, OBJECT);
        $result = [];

        foreach ($data as $row){

            $result[$row->ID] = $row->post_title;
        }
        
        return $result;
    }

    public function getTrainingByLektion($lektionid) {
        global $wpdb;

        $tablename = $wpdb->prefix.TRAININGSSYSTEM_PLUGIN_DB_TRAINING_SEITEN;
        $sql = $wpdb->prepare("SELECT DISTINCT training_id FROM $tablename WHERE lektion_id = %d", $lektionid);
        $data = $wpdb->get_results($sql, OBJECT);

        $result = [];

        foreach ($data as $row){

            $result[] = $row->training_id;
        }
        
        return $result;
    }
}

<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Markus Domin, Helge Nissen <->
 */
class Trainingssystem_Plugin_Database_login_Daoimple implements Trainingssystem_Plugin_Database_login_Dao{

    public static $dbprefix = TRAININGSSYSTEM_PLUGIN_DB_TRAINING_LASTLOGIN;

    public function __construct(){
        $this->iniDB();

    }

    public function iniDB(){


        global $wpdb;
        $table_name = $wpdb->prefix . self::$dbprefix;
        $charset_collate = $wpdb->get_charset_collate();
        $tableversionsstr =[
            "CREATE TABLE IF NOT EXISTS $table_name(
                user_id BIGINT(20) NOT NULL,
                lastlogin DATETIME NOT NULL,
                PRIMARY KEY (user_id)
            )$charset_collate;"
        ];
        Trainingssystem_Plugin_Database_Coach_impl::updateDBTable(self::$dbprefix,$table_name, $tableversionsstr);
    }

        /**
     * database updating beim plugin update
     *
     * @since    1.0.0
     */
	public function updating(){

	}

    public function save($id){
        global $wpdb;
        $table_name = $wpdb->prefix.Trainingssystem_Plugin_Database_login_Daoimple::$dbprefix;
        $sql = $wpdb->replace(
            $table_name,
            array(
                'user_id' => $id,
                'lastlogin' => current_time('mysql'),

            ),
            array(
                '%d',
                '%s'
            )
        );
        //var_dump($sql);
        return $sql;

    }

    public function getLastlogin($userid){

        global $wpdb;
        $table_name = $wpdb->prefix.$this::$dbprefix;
        $sql = $wpdb->prepare("SELECT lastlogin FROM $table_name where user_id = %d LIMIT 1",$userid);

		$result = $wpdb->get_var($sql);
		return $result;
    }

    public function getAllLastLoginIndexed() {
        global $wpdb;

        $ret = [];

        $table_name = $wpdb->prefix.$this::$dbprefix;
		$result = $wpdb->get_results("SELECT * FROM $table_name",OBJECT);
		
        foreach($result as $row) {
            $ret[$row->user_id] = $row->lastlogin;
        }
        
        return $ret;
    }

    public function getAllLastlogin(){
        global $wpdb;
        $table_name = $wpdb->prefix.$this::$dbprefix;
		$result = $wpdb->get_results("SELECT lastlogin FROM $table_name",ARRAY_N);
		return $result;
    }

    // Intervalle für die Daten über die letzten Logins der Benutzer
    public function getAllLastloginInterval($startDate=null, $endDate=null){
        global $wpdb;
        $table_name = $wpdb->prefix.$this::$dbprefix;

        // Es wurden keine Grenzen für das Intervall angegeben
        if($startDate == null && $endDate == null){
            $result = $wpdb->get_results("SELECT lastlogin FROM $table_name",ARRAY_N);
        }
        // Es wurde die untere Grenze für das Intervall angegeben
        else if($startDate != null && $endDate == null){
            $sql = $wpdb->prepare("SELECT lastlogin FROM $table_name WHERE lastlogin >= '%s'",$startDate);
            $result = $wpdb->get_results($sql,ARRAY_N);
        }
        // Es wurde die obere Grenze für das Intervall angegeben
        else if($startDate == null && $endDate != null){
            $sql = $wpdb->prepare("SELECT lastlogin FROM $table_name WHERE lastlogin <= '%s'", $endDate);
            $result = $wpdb->get_results($sql,ARRAY_N);
        }
        // Es wurde sowohl die untere als auch obere Grenze angegeben
        else if($startDate != null && $endDate != null){
            $sql = $wpdb->prepare("SELECT lastlogin FROM $table_name WHERE lastlogin >= '%s' AND lastlogin <= '%s'", $startDate, $endDate);
            $result = $wpdb->get_results($sql,ARRAY_N);
        }
		return $result;
    }

    function deleteUserInput($user_id){
        global $wpdb;
        $datatable=$wpdb->prefix.$this::$dbprefix;
        $wpdb->delete( $datatable, array( 'user_id' => $user_id ) );
    }
}

<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Markus Domin, Helge Nissen <->
 */
class Trainingssystem_Plugin_Database
{



	public $NutzerDao;
	public $TrainingDao;
	public $TrainingseitenDao;
	public $LektionDao;
	public $LastLoginDao;
	public $CompanyDao;
	public $CompanygroupDao;
	public $CompanygroupUserDao;
	public $RegisterKey;
	public $UserLogs;
	public $Bewertung;
    public $NachrichtenDao;
    public $Formfield;
    public $Formdata;
	public $PermissionFunction;
	public $PermissionRole;
	public $PermissionRole2Function;
	public $Exercise;
	public $Certificate;
	public $TrainingDate;
	public $TrainingDep;
	public $UserEvent;

	// public $Settings;

	protected static $_instance = null;

	public static function getInstance()
	{
		if (null === self::$_instance) {
			self::$_instance = new self;
		}
		return self::$_instance;
	}

	/**
	 * clone
	 *
	 * Kopieren der Instanz von aussen ebenfalls verbieten
	 */
	protected function __clone()
	{ }

	/**
	 * Initialize the collections used to maintain the actions and filters.
	 *
	 * @since    1.0.0
	 */
	protected function __construct()
	{
		$this->load_dependencies();

		$this->NutzerDao = new Trainingssystem_Plugin_Database_Nutzer_Daoimple();
		$this->TrainingDao = new Trainingssystem_Plugin_Database_Training_Daoimple();
		$this->TrainingseitenDao = new Trainingssystem_Plugin_Database_Trainingseiten_Daoimple();
		$this->LektionDao = new Trainingssystem_Plugin_Database_Lektion_Daoimple();
		$this->LastLoginDao = new Trainingssystem_Plugin_Database_login_Daoimple();
		$this->RegisterKey = new Trainingssystem_Plugin_Database_Registerkey_Daoimple();
		$this->UserLogs = new Trainingssystem_Plugin_Database_Userlogs_Daoimple();
		$this->CompanyDao = new Trainingssystem_Plugin_Database_Company_Daoimple();
		$this->CompanygroupDao = new Trainingssystem_Plugin_Database_Companygroup_Daoimple();
		$this->CompanygroupUserDao = new Trainingssystem_Plugin_Database_CompanygroupUser_Daoimple();
		$this->Bewertungen = new Trainingssystem_Plugin_Database_Bewertung_Daoimple();
		$this->NachrichtenDao = new Trainingssystem_Plugin_Database_Nachrichten_Daoimple();
        $this->Formfield = new Trainingssystem_Plugin_Database_Formfield_Daoimple();
        $this->Formdata = new Trainingssystem_Plugin_Database_Formdata_Daoimple();
		$this->PermissionFunction = new Trainingssystem_Plugin_Database_Permission_Function_Daoimple();
		$this->PermissionRole = new Trainingssystem_Plugin_Database_Permission_Role_Daoimple();
		$this->PermissionRole2Function = new Trainingssystem_Plugin_Database_Permission_Role2Function_Daoimple();
		$this->Exercise = new Trainingssystem_Plugin_Database_Exercise_Daoimple();
		$this->Certificate = new Trainingssystem_Plugin_Database_Certificate_Daoimple();
		$this->TrainingDate = new Trainingssystem_Plugin_Database_Training_Date_Daoimple();
		$this->TrainingDep = new Trainingssystem_Plugin_Database_Training_Dep_Daoimple();
		$this->UserEvent = new Trainingssystem_Plugin_Database_User_Event_Daoimple();

		// $this->Settings= null;

	}

	public function updating()
	{

		$this->NutzerDao->updating();
		$this->TrainingDao->updating();
		$this->TrainingseitenDao->updating();
		$this->LektionDao->updating();
		$this->LastLoginDao->updating();
		$this->RegisterKey->updating();
		$this->CompanyDao->updating();
		$this->UserLogs->updating();
		$this->Bewertungen->updating();
		$this->NachrichtenDao->updating();
        $this->Formfield->updating();
        $this->Formdata->updating();
		$this->PermissionFunction->updating();
		$this->PermissionRole->updating();
		$this->PermissionRole2Function->updating();
		$this->Exercise->updating();
		$this->Certificate->updating();
		$this->TrainingDate->updating();
		$this->TrainingDep->updating();
		$this->UserEvent->updating();
	}

	public function load_dependencies()
	{
		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/interface/class-trainingssystem-plugin-database-dao.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/impl/class-trainingssystem-plugin-database-impl.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'database/model/class-trainingssystem-plugin-database-nutzer.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/interface/class-trainingssystem-plugin-database-nutzer-dao.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/impl/class-trainingssystem-plugin-database-nutzer-daoimpl.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'database/impl/class-trainingssystem-plugin-database-userlogs-daoimpl.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/model/class-trainingssystem-plugin-database-userlogs.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'database/model/class-trainingssystem-plugin-database-training.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/interface/class-trainingssystem-plugin-database-training-dao.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/impl/class-trainingssystem-plugin-database-training-daoimpl.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'database/model/class-trainingssystem-plugin-database-trainingseiten.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/interface/class-trainingssystem-plugin-database-trainingseiten-dao.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/impl/class-trainingssystem-plugin-database-trainingseiten-daoimpl.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'database/model/class-trainingssystem-plugin-database-lektion.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/interface/class-trainingssystem-plugin-database-lektion-dao.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/impl/class-trainingssystem-plugin-database-lektion-daoimpl.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'database/model/class-trainingssystem-plugin-database-login.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/interface/class-trainingssystem-plugin-database-login-dao.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/impl/class-trainingssystem-plugin-database-login-daoimpl.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'database/model/class-trainingssystem-plugin-database-company.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/interface/class-trainingssystem-plugin-database-company-dao.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/impl/class-trainingssystem-plugin-database-company-daoimpl.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'database/model/class-trainingssystem-plugin-database-companygroup.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/interface/class-trainingssystem-plugin-database-companygroup-dao.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/impl/class-trainingssystem-plugin-database-companygroup-daoimpl.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'database/model/class-trainingssystem-plugin-database-companygroupuser.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/interface/class-trainingssystem-plugin-database-companygroupuser-dao.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/impl/class-trainingssystem-plugin-database-companygroupuser-daoimpl.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'database/model/class-trainingssystem-plugin-database-registerkey.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/interface/class-trainingssystem-plugin-database-registerkey-dao.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/impl/class-trainingssystem-plugin-database-registerkey-daoimpl.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'database/model/class-trainingssystem-plugin-database-bewertung.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/impl/class-trainingssystem-plugin-database-bewertung-daoimpl.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'database/impl/class-trainingssystem-plugin-database-nachrichten-daoimpl.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'database/model/class-trainingssystem-plugin-database-list-user.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/model/class-trainingssystem-plugin-database-list-training.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/model/class-trainingssystem-plugin-database-list-lektion.php';
		
        require_once plugin_dir_path(dirname(__FILE__)) . 'database/model/class-trainingssystem-plugin-database-formfield.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'database/interface/class-trainingssystem-plugin-database-formfield-dao.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'database/impl/class-trainingssystem-plugin-database-formfield-daoimpl.php';
        
        require_once plugin_dir_path(dirname(__FILE__)) . 'database/model/class-trainingssystem-plugin-database-formdata.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'database/interface/class-trainingssystem-plugin-database-formdata-dao.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'database/impl/class-trainingssystem-plugin-database-formdata-daoimpl.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'database/model/class-trainingssystem-plugin-database-permission-function.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/interface/class-trainingssystem-plugin-database-permission-function-dao.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/impl/class-trainingssystem-plugin-database-permission-function-daoimpl.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'database/model/class-trainingssystem-plugin-database-permission-role.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/interface/class-trainingssystem-plugin-database-permission-role-dao.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/impl/class-trainingssystem-plugin-database-permission-role-daoimpl.php';
		
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/model/class-trainingssystem-plugin-database-permission-role2function.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/interface/class-trainingssystem-plugin-database-permission-role2function-dao.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/impl/class-trainingssystem-plugin-database-permission-role2function-daoimpl.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'database/model/class-trainingssystem-plugin-database-exercise.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/interface/class-trainingssystem-plugin-database-exercise-dao.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/impl/class-trainingssystem-plugin-database-exercise-daoimpl.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'database/model/class-trainingssystem-plugin-database-certificate.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/interface/class-trainingssystem-plugin-database-certificate-dao.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/impl/class-trainingssystem-plugin-database-certificate-daoimpl.php';
		
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/model/class-trainingssystem-plugin-database-training-date.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/interface/class-trainingssystem-plugin-database-training-date-dao.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/impl/class-trainingssystem-plugin-database-training-date-daoimpl.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'database/model/class-trainingssystem-plugin-database-training-dep.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/interface/class-trainingssystem-plugin-database-training-dep-dao.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/impl/class-trainingssystem-plugin-database-training-dep-daoimpl.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'database/model/class-trainingssystem-plugin-database-user-event.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/interface/class-trainingssystem-plugin-database-user-event-dao.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'database/impl/class-trainingssystem-plugin-database-user-event-daoimpl.php';
	}
}

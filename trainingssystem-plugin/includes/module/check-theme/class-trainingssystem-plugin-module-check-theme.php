<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 */
class Trainingssystem_Plugin_Module_Check_Theme{

	public function __construct() {

	}

	/**
	 * check auf das Standart Theme vom Trainingsystem instaliert ist
	 *
	 * @since    1.0.0
	 */
	function checktheme(){

		$defaulttheme = wp_get_theme();
		if($defaulttheme->exists()) {
			$themename = $defaulttheme->get("Name");
		}

		if ( TRAININGSSYSTEM_PLUGIN_THEME_SLUG != $defaulttheme){
			?>
			<div class="notice notice-warning is-dismissible">
				<p><?php _e( 'Um das optimale Ergebnis zu erzielen, installieren Sie bitte das Coachlight-V2-Theme.', TRAININGSSYSTEM_PLUGIN_SLUG ); ?></p>
			</div>
			<?php
		}
	}

}

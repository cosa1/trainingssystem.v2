<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 */
class Trainingssystem_Plugin_Module_User_Modus
{

    public function __construct()
    {

    }

    // function to change in user mode
    public function startUsermodusById()
    {
        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

        if(Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("usermodus")) 
        {
            $current_user = wp_get_current_user();
            if (0 == $current_user->ID) {
                echo "denied";
            } else {

                if (isset($_POST['coach_select_user']) && is_numeric($_POST['coach_select_user'])) {

                    $coach_select_userid = sanitize_text_field($_POST['coach_select_user']);

                    if(update_user_meta($current_user->ID, 'coach_select_user', $coach_select_userid)) {
                        echo "1";
                    } else {
                        echo "0";
                    }
                }
            }
        }
        else
        {
            echo $twig->render('modal-unauth.html', ["title" => get_bloginfo('name'), "url" => get_site_url(), "modalid" => uniqid()]);    
        }
        wp_die();
    }
    
    public function disableUserModus(){
        if(Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("usermodus")) 
        {
            $current_user = wp_get_current_user();
            if (0 == $current_user->ID) {
                echo "denied";
            } else {

                if (isset($_POST['coach_clear_user'])) {
                    if(update_user_meta($current_user->ID, 'coach_select_user', '')) {
                        echo "1";
                    } else {
                        echo "0";
                    }
                }
            }
        }
        wp_die();
    }

	// function to show user in user mode
	public function showUser()
    {
        if(Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("usermodus")) 
        {
            $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

            $userid = get_user_meta(wp_get_current_user()->ID, 'coach_select_user',true);
            
            if($userid != ''){
                $tmpselectuser_info = get_userdata($userid);
                if($tmpselectuser_info){
                    
                    echo $twig->render('user-modus/user-modus-select-user-overlay.html',[
                                        "username"=>$tmpselectuser_info->user_login,
                                        ]);
                }
                
            }
        }       
    }
    

}

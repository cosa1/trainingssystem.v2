<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 */
class Trainingssystem_Plugin_Module_Media {

	public function __construct() {

	}

	/**
	 * shortcode ändern das auch relative pfade möglich sind
	 *
	 * @since    1.0.0
	 */
	public function ast_audio_shortcode_enhancer( $html, $atts, $audio, $post_id, $library ) {
		$html = preg_replace('/http:\/\/.\//', './', $html);
		$html = preg_replace('/https:\/\/.\//', './', $html);
		return $html;
	}
}

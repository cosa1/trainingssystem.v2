<?php

/**
 * CSV Import von Usern über das Frontend
 *
 * @author      Markus Domin, Helge Nissen <markus.domin@fh-luebeck.de>
 * @renewed     Max Sternitzke <max.sternitzke@th-luebeck.de>, 2021
 */
class Trainingssystem_Plugin_Module_User_Importer
{
    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     */
    public function __construct()
    {
    }

    /**
     * FRONTEND
     * 
     * Zeigt Hinweise und das Formular zum Upload einer Datei an
     * 
     * @return HTML-Code
     */
    public static function showCsvImportOverview($atts)
    {
        $heading = isset($atts['titel']) ? $atts['titel'] : 'Teilnehmende importieren'; 
        
        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

        if(Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("nutzerimport"))
        {
            return $twig->render('csv-import/csv-import.html', [
                                                            'demofile' => plugin_dir_url(__FILE__).'../../../assets/csv/bsp-csv-user-import.csv', 
                                                            'heading' => $heading,
                                                            ]);           
        }
        else
        {
            return $twig->render('modal-unauth.html', ["title" => get_bloginfo('name'), "url" => get_site_url(), "modalid" => uniqid()]);
        }
    }

    /**
     * AJAX
     * 
     * Wird mit der CSV als Upload aufgerufen, um Firmen/Gruppen sowie ggf. vergebene E-Mail-Adressen und Freischaltcodes zu überprüfen.
     * 
     * @return JSON-encoded Array mit Keys status und html
     */
    public function testCsvFile() {
       
        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

        if(Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("nutzerimport") && isset($_FILES['file']) && $_FILES['file']['error'] == 0)
        {
            $filecontent = array();
            $line = 0;
            if (($handle = fopen($_FILES['file']['tmp_name'], 'r')) !== false) {
                while (($data = fgetcsv($handle, 1000, ';')) !== false) {
                    if ($line > 0) {
                        $filecontent[] = $data;
                    }
                    $line++;
                }
                if(sizeof($filecontent) == 0) {
                    $line = 0;
                    while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                        if ($line > 0) {
                            $filecontent[] = $data;
                        }
                        $line++;
                    }
                }
            }
            fclose($handle);

            foreach($filecontent as $linekey => $line) {
                foreach($line as $key => $value) {
                    $tmp = trim($value);
                    $filecontent[$linekey][$key] = (strlen($tmp) > 0) ? $tmp : '-';
                }
            }

            $validate = $this->validateImport($filecontent);

            $ret = array();
            $ret['status'] = $validate["ret"]["status"];
            $ret['code'] = $twig->render("csv-import/csv-import-test.html", ["filecontent" => $filecontent, "status" => $validate["status"]]);

            echo json_encode($ret);
        }
        wp_die();
    }

    /**
     * AJAX
     * 
     * Wird mit Teilen der CSV als multi-dim JSON-Array (je nach Batch-Size) aufgerufen, wenn der Check erfolgreich war und die Datei importiert werden kann.
     * Importiert die Nutzer und legt Verknüpfungen zur Firmen/Gruppen an, etc.
     * 
     * @return JSON-encoded Array mit Key status, success-Array und failed-Array
     */
    public function importCsvFile() {

        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

        if(Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("nutzerimport") && isset($_POST['content']) && strlen(trim($_POST['content'])) > 0)
        {
            $filecontent = json_decode(stripslashes($_POST['content']), true);
            
            $ret = array();

            if(json_last_error() == JSON_ERROR_NONE) {

                foreach($filecontent as $linekey => $line) {
                    foreach($line as $key => $value) {
                        $tmp = trim(sanitize_text_field($value));
                        $filecontent[$linekey][$key] = (strlen($tmp) > 0) ? $tmp : '-';
                    }
                }

                $validate = $this->validateImport($filecontent);

                if($validate["ret"]["status"] == "success") {
                    $ret['success'] = array();
                    $ret['failed'] = array();

                    foreach($filecontent as $linekey => $line) {
                        
                        if(is_array($line) && isset($line[0]) && isset($line[1]) && isset($line[2]) && isset($line[3]) && isset($line[4]) && isset($line[5]) && isset($line[6])) {

                            $password = wp_generate_password(10, true, true);
                            $user = wp_create_user($line[0], $password, sanitize_email($line[1]));

                            if(!is_wp_error($user)) {

                                wp_new_user_notification($user,null,'both');

                                if($line[2] != "-" && $line[3] != "-") {
                                    $company = Trainingssystem_Plugin_Database::getInstance()->CompanyDao->getCompanyByName($line[2]);
                                    if($company != null){
                                        $group = Trainingssystem_Plugin_Database::getInstance()->CompanygroupDao->getCompanygroupByName($line[3], $company->getId());
                                        if($group != null) {
                                            if(Trainingssystem_Plugin_Database::getInstance()->CompanygroupUserDao->addUserToGroup($user, $group->getId()) === false) {
                                                $ret['failed'][$linekey] = "Fehler beim Zuordnen der Gruppe";
                                                continue;
                                            }
                                        } else {
                                            $ret['failed'][$linekey] = "Gruppe nicht gefunden";
                                            continue;
                                        }
                                    } else {
                                        $ret['failed'][$linekey] = "Firma nicht gefunden";
                                        continue;
                                    }
                                }
                                                
                                if($line[4] != "-") {
                                    if(!add_user_meta($user, 'studienid', $line[4])) {
                                        $ret['failed'][$linekey] = "Fehler beim Eintragen der Studien-Teilnehmer ID";
                                        continue;
                                    }
                                }

                                if($line[5] != "-") {
                                    if(!add_user_meta($user, 'studiengruppenid', $line[5])) {
                                        $ret['failed'][$linekey] = "Fehler beim Eintragen der Studiengruppen ID";
                                        continue;
                                    }
                                }

                                if($line[6] != "-") {
                                    $regkey = new Trainingssystem_Plugin_Module_Register_Key();
                                    if(!$regkey->redeemRegisterkey($user, $line[6], true)) {
                                        $ret['failed'][$linekey] = "Fehler beim Einlösen des Freischaltcodes";
                                        continue;
                                    }
                                }

                                if(!isset($failed[$linekey])) {
                                    $ret['success'][$linekey] = "Nutzer erfolgreich angelegt";
                                }

                            } else {
                                $ret['failed'][$linekey] = "Fehler beim Anlegen des Benutzers";
                                continue;
                            }
                        } else {
                            $ret['failed'][$linekey] = "Fehlende Angaben";
                        }
                    }

                    if(sizeof($ret['failed']) == 0) {
                        $ret['status'] = "success";
                    } else {
                        $ret['status'] = "error";
                    }

                } else {
                    $ret['status'] = "error";
                }
            } else {
                $ret['status'] = "error0";
            }
            echo json_encode($ret);
        }
        wp_die();
    }

    /**
     * HELPER
     * 
     * Prüft und vaidiert eine als Array übergebene CSV-Datei
     */
    private function validateImport(Array $filecontent) {
        $ret = array();
        $ret['status'] = "success";

        $companies = Trainingssystem_Plugin_Database::getInstance()->CompanyDao->getCompanyandCompanygroups();
        $registerkeys_raw = Trainingssystem_Plugin_Database::getInstance()->RegisterKey->getAll();
        $registerkeys = array();
        foreach($registerkeys_raw as $rk) {
            $registerkeys[$rk->getCode()] = intval($rk->getMaxCount()) - intval($rk->getCountCode());
        }
        $alluser = get_users();
        $mails = array();
        $loginnames = array();
        foreach($alluser as $user) {
            $mails[] = $user->user_email;
            $loginnames[] = $user->user_login;
        }

        $status = array();
        foreach($filecontent as $linekey => $line) {

            $status[$linekey] = array();

            if(is_array($line) && isset($line[0]) && isset($line[1]) && isset($line[2]) && isset($line[3]) && isset($line[4]) && isset($line[5]) && isset($line[6])) {

                $status[$linekey]['name'] = true;
                $status[$linekey]['mail'] = true;
                $status[$linekey]['company'] = true;
                $status[$linekey]['group'] = true;
                $status[$linekey]['studyid'] = true;
                $status[$linekey]['studygroupid'] = true;
                $status[$linekey]['registerkey'] = true;

                if($line[0] == "") {
                    $status[$linekey]['name'] = false;
                    $ret['status'] = "error";
                } else {
                    $ln_f = false;
                    foreach($loginnames as $ln) {
                        if(strcmp($ln, $line[0]) === 0) {
                            $ln_f = true;
                            break;
                        }
                    }
                    if($ln_f) {
                        $status[$linekey]['name'] = false;
                        $ret['status'] = "error";
                    }
                }

                if(!filter_var($line[1], FILTER_VALIDATE_EMAIL)) {
                    $status[$linekey]['mail'] = false;
                    $ret['status'] = "error";
                } else {
                    $m_f = false;
                    foreach($mails as $m) {
                        if(strcmp($m, $line[1]) === 0) {
                            $m_f = true;
                            break;
                        }
                    }
                    if($m_f) {
                        $status[$linekey]['mail'] = false;
                        $ret['status'] = "error";
                    }
                }

                $c_f = null;
                if($line[2] != "-") {
                    foreach($companies as $c) {
                        if(strcmp($c['companyname'], $line[2]) === 0) {
                            $c_f = $c['companyid'];
                            break;
                        }
                    }
                    if($c_f == null) {
                        $status[$linekey]['company'] = false;
                        $ret['status'] = "error";
                    }
                }

                if($line[3] != "-") {
                    if($c_f != null) {

                        $g_f = null;
                        foreach($companies[$c_f]['groups'] as $g_id => $g) {
                            if(strcmp($g, $line[3]) === 0) {
                                $g_f = $g_id;
                                break;
                            }
                        }
                        if($g_id == null) {
                            $status[$linekey]['group'] = false;
                            $ret['status'] = "error";
                        }

                    } else {
                        $status[$linekey]['group'] = false;
                        $ret['status'] = "error";
                    }
                }

                if($line[6] != "-") {
                    if(isset($registerkeys[$line[6]])) {
                        if($registerkeys[$line[6]] > 0) {
                            $registerkeys[$line[6]]--;
                        } else {
                            $status[$linekey]['registerkey'] = false;
                            $ret['status'] = "error";
                        }
                    } else {
                        $status[$linekey]['registerkey'] = false;
                        $ret['status'] = "error";
                    }
                }
            } else {
                $ret['status'] = "error";
                $status[$linekey]['name'] = false;
                $status[$linekey]['mail'] = false;
                $status[$linekey]['company'] = false;
                $status[$linekey]['group'] = false;
                $status[$linekey]['studyid'] = false;
                $status[$linekey]['studygroupid'] = false;
                $status[$linekey]['registerkey'] = false;
            }
        }

        return ["ret" => $ret, "status" => $status];
    }

    /**
     * Plugin Name:  Chara-chan
     * Plugin URI: https://marcus-potthoff.de
     * Description: Ein Plugin damit Umlaute im Usernamen bei Anmeldung in einem Blog akzeptiert werden.
     * Version: 1.0
     * Author: Marcus Potthoff
     * Author URI: https://marcus-potthoff.de
     * License: GPL2
     * License URI:  https://www.gnu.org/licenses/gpl-2.0.html
     * Text Domain:  Chara-chan
     */
    function chara_chan_user ($username, $raw_username, $strict)
    {
        $username = wp_strip_all_tags ($raw_username);
        return $username;
    }
}

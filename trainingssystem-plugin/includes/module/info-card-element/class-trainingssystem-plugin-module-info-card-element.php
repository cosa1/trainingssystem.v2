<?php

class Trainingssystem_Plugin_Module_Info_Card_Element{

    public function __construct() {}
    
    public function showInfoCardElement($atts,$content=null){

        $gap = "10";
        $mode = "stack";
        if (isset($atts["mode"])) {
            if ($atts["mode"] === "single_card") {
                $mode = $atts["mode"];
                $gap = 0;
            }
            
        }
        $cardPadding = "2%";
        $contentTemp = trim($content);
        $colorError = false;
        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;    

        if($contentTemp == null){
            return $twig->render("info-card-element/info-card-element-error.html");
        }

        if(!is_numeric($gap) || $gap < 0){
            return $twig->render("info-card-element/info-card-element-error.html");
        }
        
        $doc = new DOMDocument();
        $doc->loadHTML($contentTemp);
        $cardArray = [];
        $classname = "infoCard";
        $dom = new DOMDocument();
        $dom->loadHTML('<?xml encoding="utf-8" ?>' .$contentTemp);
        $xpath = new DOMXpath($dom);
        $nodes = $xpath->query('//div[@class="' . $classname . '"]');
        $myArray = array();
        $myArray2 = array();
        $tempTestdoc = new DOMDocument();

        foreach ($nodes as $node) {
            $tempObj = new stdClass;
            $tempTestdoc->appendChild($tempTestdoc->importNode($node, true));
            $tempObj->bgColor = "#FFFFFF";
            $tempObj->cardAlignment = "center";
            foreach ($node->attributes as $attr) {
                if("bg_color" == $attr->nodeName){
                    // check if color matches hex format
                    if(preg_match("/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?\b/", $attr->nodeValue)){
                        $tempObj->bgColor = $attr->nodeValue;
                    }
                    else{
                        $colorError = true;
                    }
                }
                if ("card_alignment" == $attr->nodeName ) {
                    if ($attr->nodeValue === "center" || $attr->nodeValue === "left" || $attr->nodeValue === "right") {
                        $tempObj->cardAlignment = $attr->nodeValue;
                    }
                }
                if ("img_src" == $attr->nodeName){
                    $tempObj->imgSrc = $attr->nodeValue;
                }
            }

            $tempObj->cardContent = $tempTestdoc->saveHTML();
            array_push($myArray2, $tempObj);
            array_push($myArray, $tempTestdoc->saveHTML());
            $tempTestdoc = new DOMDocument();
        }
        $reversedArray = array_reverse($myArray);
        $reversedArray2 = array_reverse($myArray2);

        if(!$colorError){
            return $twig->render('info-card-element/info-card-element.html', [
                "content" => $reversedArray2,
                "len" => count($reversedArray2),
                "gap" => $gap,
                "cardPadding" => $cardPadding,
            ]);
        }else{
            return $twig->render("info-card-element/info-card-element-error.html");
        }
        
    
    
}
}

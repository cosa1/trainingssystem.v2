<?php

class Trainingssystem_Plugin_Module_Copy_Fieldtext{

    public function __construct() {

    }
    
    // Shortcode-Handler für den Shortcode copyfieldtext
    public function scCopyFieldtext($atts, $content=null){

        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

        $randId = mt_rand();

        if(isset($atts['id'])){
            if(trim($atts['id'])!= ""){
                $formid = trim($atts['id']);
            }
            else{
                return "Fehler: Bitte id angeben.";
            }
        }
        else{
            return "Fehler: Bitte id angeben.";
        }

        return $twig->render("copy-fieldtext/copy-fieldtext.html", [
            "rand" => $randId,
            "formid" => $formid
        ]);
    }
}
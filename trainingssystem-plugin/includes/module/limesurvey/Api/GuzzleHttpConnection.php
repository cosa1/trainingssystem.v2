<?php


namespace Trainingssystem\Module\Limesurvey\Api;


use Graze\GuzzleHttp\JsonRpc\Client;
use Graze\GuzzleHttp\JsonRpc\Exception\RequestException;

class GuzzleHttpConnection implements Connection
{
    public function sendRequest(string $url, string $method, array $parameters = [])
    {
        $client = Client::factory($url, [
            'rpc_error' => true,
            'verify' => !(WP_DEBUG ?? false),
        ]);

        $guzzleRequest = $client->request(1, $method, $parameters);

        try {
            $guzzleResponse = $client->send($guzzleRequest);
            return $guzzleResponse->getRpcResult();
        } catch (RequestException $e) {
            $message = $e->getResponse()->getRpcErrorMessage();
            error_log($message);
            var_dump($message);
            exit;
        }
    }
}
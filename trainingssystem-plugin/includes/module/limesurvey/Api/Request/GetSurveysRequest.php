<?php


namespace Trainingssystem\Module\Limesurvey\Api\Request;


use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Trainingssystem\Module\Limesurvey\Api\Model\SimpleSurvey;

class GetSurveysRequest extends AbstractRequest implements Request
{
    private $surveys;

    public function getMethod(): string
    {
        return 'list_surveys';
    }

    public function getParameters(): array
    {
        return [];
    }

    public function handleResponse($response): void
    {
        $this->response = $response;

        $serializer = new Serializer([new ObjectNormalizer(), new ArrayDenormalizer()]);
        $this->surveys = $serializer->denormalize($this->response, 'Trainingssystem\Module\Limesurvey\Api\Model\SimpleSurvey[]');
    }

    public function isValid(): bool
    {
        return true;
    }

    public function getError(): string
    {
        return '';
    }

    /**
     * @return mixed
     */
    public function getSurveys()
    {
        return $this->surveys;
    }
}
<?php


namespace Trainingssystem\Module\Limesurvey\Api\Request;


use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Trainingssystem\Module\Limesurvey\Api\Model\Participant;
use Trainingssystem\Module\Limesurvey\Api\Model\Token;

class SetParticipantPropertiesRequest extends AbstractRequest implements Request
{
    /**
     * @var array
     */
    private $parameters;

    /**
     * @var array
     */
    private $participant;

    public function __construct(int $surveyId, int $tokenId, Token $token)
    {
        $this->parameters = [
            'iSurveyID' => $surveyId,
            'aTokenQueryProperties' => $tokenId,
            'aTokenData' => [
                'token' => $token->getToken(),
            ]
        ];
    }

    public function getMethod(): string
    {
        return 'set_participant_properties';
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function handleResponse($response): void
    {
        $this->response = $response;

        $serializer = new Serializer([new ObjectNormalizer()]);
        $this->participant = $serializer->denormalize($this->response, 'Trainingssystem\Module\Limesurvey\Api\Model\Participant');
    }
    public function isValid(): bool
    {
        return !isset($this->response['status']);
    }

    public function getError(): string
    {
        return $this->response['status'];
    }

    /**
     * @return array
     */
    public function getParticipant(): Participant
    {
        return $this->participant;
    }
}
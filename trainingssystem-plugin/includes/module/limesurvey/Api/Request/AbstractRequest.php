<?php


namespace Trainingssystem\Module\Limesurvey\Api\Request;


abstract class AbstractRequest
{
    /**
     * @var string
     */
    protected $response;

    /**
     * @return string
     */
    protected function getResponse(): string
    {
        return $this->response;
    }

    /**
     * @param string $response
     * @return AbstractRequest
     */
    protected function setResponse($response): AbstractRequest
    {
        $this->response = $response;
        return $this;
    }
}
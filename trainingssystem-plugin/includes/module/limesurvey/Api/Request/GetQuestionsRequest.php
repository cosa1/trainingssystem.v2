<?php


namespace Trainingssystem\Module\Limesurvey\Api\Request;


use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Trainingssystem\Module\Limesurvey\Api\Model\SimpleSurvey;

class GetQuestionsRequest extends AbstractRequest implements Request
{
    private $parameters;

    public function __construct(int $surveyId)
    {
        $this->parameters = [
            'iSurveyID' => $surveyId,
        ];
    }

    public function getMethod(): string
    {
        return 'list_questions';
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function handleResponse($response): void
    {
        $this->response = $response;
    }

    public function isValid(): bool
    {
        return true;
    }

    public function getError(): string
    {
        return '';
    }

    /**
     * @return mixed
     */
    public function getQuestions()
    {
        return $this->response;
    }
}
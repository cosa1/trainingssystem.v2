<?php


namespace Trainingssystem\Module\Limesurvey\Api\Request;


class GetSessionKeyRequest extends AbstractRequest implements Request
{
    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $sessionKey;

    public function __construct(string $username, string $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    public function getMethod(): string
    {
        return 'get_session_key';
    }

    public function handleResponse($response): void
    {
        $this->response = $response;
        $this->sessionKey = $response;
    }

    public function isValid(): bool
    {
        return !isset($this->response['status']);
    }

    public function getError(): string
    {
        return $this->response['status'];
    }

    public function getParameters(): array
    {
        return [
            'username' => $this->username,
            'password' => $this->password,
        ];
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return GetSessionKeyRequest
     */
    public function setUsername(string $username): GetSessionKeyRequest
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return GetSessionKeyRequest
     */
    public function setPassword(string $password): GetSessionKeyRequest
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getSessionKey(): string
    {
        return $this->sessionKey;
    }

    /**
     * @param string $sessionKey
     * @return GetSessionKeyRequest
     */
    public function setSessionKey(string $sessionKey): GetSessionKeyRequest
    {
        $this->sessionKey = $sessionKey;
        return $this;
    }
}
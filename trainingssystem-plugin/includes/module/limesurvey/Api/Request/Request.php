<?php


namespace Trainingssystem\Module\Limesurvey\Api\Request;


interface Request
{
    public function getMethod(): string;
    public function getParameters(): array;
    public function handleResponse($response): void;
    public function isValid(): bool;
    public function getError(): string;
}
<?php


namespace Trainingssystem\Module\Limesurvey\Api\Request;


use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Trainingssystem\Module\Limesurvey\Api\Model\SimpleSurvey;

class GetSurveyDetailsRequest extends AbstractRequest implements Request
{
    public function __construct(int $surveyId)
    {
        $this->parameters = [
            'iSurveyID' => $surveyId,
        ];
    }

    public function getMethod(): string
    {
        return 'get_survey_properties';
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function handleResponse($response): void
    {
        $this->response = $response;
    }

    public function isValid(): bool
    {
        return true;
    }

    public function getError(): string
    {
        return '';
    }

    /**
     * @return mixed
     */
    public function getSurvey()
    {
        return $this->response;
    }
}
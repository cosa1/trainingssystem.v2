<?php

namespace Trainingssystem\Module\Limesurvey\Api\Model;

class SimpleSurvey
{
    public $sid;
    public $surveyls_title;
    public $startdate;
    public $expires;
    public $active;
}
<?php


namespace Trainingssystem\Module\Limesurvey\Api\Model;


class Token
{
    private $token;

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     * @return Token
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }
}
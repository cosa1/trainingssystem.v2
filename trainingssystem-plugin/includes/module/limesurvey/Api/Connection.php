<?php


namespace Trainingssystem\Module\Limesurvey\Api;


interface Connection
{
    public function sendRequest(string $url, string $method, array $parameters = []);
}
<?php

class Trainingssystem_Plugin_Module_Tab_Element {

    public function __construct() {}

    public function showTabElement($atts,$content=null) {

        $colorError = false;


        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

        $orientation = "top";
        if(isset($atts['orientation']) && trim($atts['orientation']) != ''){
            if(trim(strtolower($atts['orientation'])) == 'top'){
                $orientation = "top";
            }
            else if(trim(strtolower($atts['orientation'])) == 'left'){
                $orientation = "left";
            }
        }

        $fade = true;
        if(isset($atts['fade']) && trim($atts['fade']) != ''){
            if(trim($atts['fade']) == 'false' || trim($atts['fade']) == '0'){
                $fade = false;
            }
        }

        $tabColorActive = "#FFFFFF";
        if(isset($atts['tab-color-active']) && trim($atts['tab-color-active']) != '') {
            // check if color matches hex format
            if(preg_match("/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){1}|([a-f]|[A-F]|[0-9]){3}|([a-f]|[A-F]|[0-9]){5})?\b/", $atts['tab-color-active'])){
                $tabColorActive = $atts['tab-color-active'];
            }
            else{
                $colorError = true;
            } 
        }

        $tabColorInactive = "#FFFFFF";
        if(isset($atts['tab-color-inactive']) && trim($atts['tab-color-inactive']) != '') {
            // check if color matches hex format
            if(preg_match("/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){1}|([a-f]|[A-F]|[0-9]){3}|([a-f]|[A-F]|[0-9]){5})?\b/", $atts['tab-color-inactive'])){
                $tabColorInactive = $atts['tab-color-inactive'];
            }
            else{
                $colorError = true;
            } 
        }

        $tabTextColorActive = "#000000";
        if(isset($atts['tab-text-color-active']) && trim($atts['tab-text-color-active']) != '') {
            // check if color matches hex format
            if(preg_match("/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){1}|([a-f]|[A-F]|[0-9]){3}|([a-f]|[A-F]|[0-9]){5})?\b/", $atts['tab-text-color-active'])){
                $tabTextColorActive = $atts['tab-text-color-active'];
            }
            else{
                $colorError = true;
            } 
        }

        $tabTextColorInactive = "#000000";
        if(isset($atts['tab-text-color-inactive']) && trim($atts['tab-text-color-inactive']) != '') {
            // check if color matches hex format
            if(preg_match("/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){1}|([a-f]|[A-F]|[0-9]){3}|([a-f]|[A-F]|[0-9]){5})?\b/", $atts['tab-text-color-inactive'])){
                $tabTextColorInactive = $atts['tab-text-color-inactive'];
            }
            else{
                $colorError = true;
            } 
        }

        $contentTemp = trim($content);
        if($contentTemp == null){
            return $twig->render("tab-element/tab-element-error.html");
        }
        $doc = new DOMDocument();
        $doc->loadHTML($contentTemp);
        $tabArray = [];

        $classname = "tabelement";

        $dom = new DOMDocument();
        $dom->loadHTML('<?xml encoding="utf-8" ?>' . $contentTemp);

        $xpath = new DOMXpath($dom);
        $nodes = $xpath->query('//div[@class="' . $classname . '"]');

        foreach ($nodes as $node) {
            $tempObj = new stdClass;
            foreach ($node->attributes as $attr) {

                if("title" == $attr->nodeName){
                    $tempObj->title = $attr->nodeValue;
                }

                if("icon" == $attr->nodeName){
                    $tempObj->icon = $attr->nodeValue;
                }
            }
            $tempObj->content = do_shortcode(trim($this->getInnerHtmlOfNode($node)));
            array_push($tabArray,$tempObj);
        }

        if(!$colorError){
            return $twig->render('tab-element/tab-element.html', [
                "tabArray" => $tabArray,
                "rand" => mt_rand(),
                "orientation" => $orientation,
                "fade" => $fade,
                "tabColorActive" => $tabColorActive,
                "tabColorInactive" => $tabColorInactive,
                "tabTextColorActive" => $tabTextColorActive,
                "tabTextColorInactive" => $tabTextColorInactive
            ]);
        }
        else{
            return $twig->render("tab-element/tab-element-error.html");
        }
    }

    /**
     * HELPER
     * 
     * Get the innerHTML of a XML Node as HTML
     * 
     * @param $node XML Node
     * @return HTML Code
     */
    private function getInnerHtmlOfNode($node) {

        $innerHTML = "";
    
        foreach ($node->childNodes as $child) {
            $innerHTML .= $child->ownerDocument->saveXML($child);
        }
    
        return $innerHTML;
    
    }

}
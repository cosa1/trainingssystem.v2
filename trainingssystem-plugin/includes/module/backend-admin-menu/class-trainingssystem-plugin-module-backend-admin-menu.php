<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Coach_User_Remover
 * @subpackage Coach_User_Remover/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Coach_User_Remover
 * @subpackage Coach_User_Remover/admin
 * @author     Markus Domin, Helge Nissen <markus.domin@fh-luebeck.de>
 */
class Trainingssystem_Plugin_Module_Backend_Admin_Menu
{

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     */
    public function __construct()
    {

    }

    /**
     * Register custom admin bar menu.
     *
     * @since    1.0.0
     */
    public function my_admin_bar_init()
    {
        if (!is_admin_bar_showing()) {
            return;
        }

        add_action('admin_bar_menu', array($this, 'my_admin_bar_menu'), 500);
    }

    /**
     * Register custom admin bar menu.
     *
     * @since    1.0.0
     */
    public function my_admin_bar_menu()
    {
        if(Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("trainingedit")) {
            global $wp_admin_bar;
            $data = get_user_meta(wp_get_current_user()->ID, 'coach_select_user', true);
            
            $username = 'Kein Nutzer ausgewählt';
            $usermail = '';
            if ($data != '') {
                $user_info = get_userdata($data);
                $username = '' . $user_info->user_login; //user_nicename;
                $usermail = 'mailto:' . $user_info->user_email;
            }

            $wp_admin_bar->add_menu(array(
                'title' => 'ausgewählter Nutzer: ' . $username,
                'id' => 'admin_bar_menu_select_user', // Diese id brauchst du für die Untermenüpunkte
            ));

            $wp_admin_bar->add_menu(array(
                'title' => 'Mail an ' . $username,
                'href' => $usermail,
                'parent' => 'admin_bar_menu_select_user',
                //'meta' => array('target' => '_blank')
                'id' => 'admin_bar_menu_select_user_sub2',
            ));

            $wp_admin_bar->add_menu(array(
                'title' => 'Nutzer wechseln ',
                'href' => '' . admin_url('admin.php?page=coach-nutzer'),
                'parent' => 'admin_bar_menu_select_user',
                //'meta' => array('target' => '_blank') //html erweiterung
                'id' => 'admin_bar_menu_select_user_sub3',
            ));

            $wp_admin_bar->add_menu(array(
                'title' => 'ausgewählter Nutzer: ' . $username,
                'href' => '' . admin_url('admin.php?page=coach-nutzer'),
                'parent' => 'site-name',
                //'meta' => array('target' => '_blank')
                'id' => 'admin_bar_menu_select_user_sub',
            ));
        }
    }

    public function md_trainingsmgr_admin_register_menu()
    {
        if(Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("trainingedit")) {

            $isAdmin = Trainingssystem_Plugin_Module_Berechtigung::getInstance()->userHasRole("admin");
            add_menu_page(
                'TrainOn', // page title
                'TrainOn', // menu title
                'manage_categories', // capability
                TRAININGSSYSTEM_PLUGIN_SLUG, // menu slug
                array($this, "md_trainingsmgr_admin_register_menu_start_render"), // callback function
                plugin_dir_url(dirname(__FILE__)) . '../../assets/img/logo3.png',
                2
            );


            add_submenu_page(TRAININGSSYSTEM_PLUGIN_SLUG, 'Trainings', 'Trainings',
                'manage_categories', 'edit.php?post_type=trainings', null);
            add_submenu_page(TRAININGSSYSTEM_PLUGIN_SLUG, 'Lektionen', 'Lektionen',
                'manage_categories', 'edit.php?post_type=lektionen', null);
            add_submenu_page(TRAININGSSYSTEM_PLUGIN_SLUG, 'Seiten', 'Seiten',
                'manage_categories', 'edit.php?post_type=seiten', null);
            add_submenu_page(TRAININGSSYSTEM_PLUGIN_SLUG, 'Übungen', 'Übungen',
                'manage_categories', 'edit.php?post_type=uebungen', null);
            add_submenu_page(TRAININGSSYSTEM_PLUGIN_SLUG, 'Formulare', 'Formulare',
                'manage_categories', 'edit.php?post_type=formulare', null);
            add_submenu_page(TRAININGSSYSTEM_PLUGIN_SLUG, 'Abzeichen', 'Abzeichen',
                'manage_categories', 'edit.php?post_type=abzeichen', null);
            add_submenu_page(TRAININGSSYSTEM_PLUGIN_SLUG, 'Zertifikate', 'Zertifikate',
                'manage_categories', 'edit.php?post_type=zertifikate', null);
            add_submenu_page(TRAININGSSYSTEM_PLUGIN_SLUG, 'Avatare', 'Avatare',
                'manage_categories', 'edit.php?post_type=avatare', null);
            $trainingssystem_Plugin_Module_Coach_User_Training_Mgr = new Trainingssystem_Plugin_Module_Coach_User_Training_Mgr_public();
            add_submenu_page(TRAININGSSYSTEM_PLUGIN_SLUG, 'Zukünftige Trainings-Zuweisungen', 'Zukünftige Trainings-Zuweisungen',
                'manage_categories', "future_trainings", array($trainingssystem_Plugin_Module_Coach_User_Training_Mgr, "backendAdminOverview"));
           
            if($isAdmin) {
                add_submenu_page(TRAININGSSYSTEM_PLUGIN_SLUG, 'Nachrichten', 'Nachrichten',
                    'manage_options', 'edit.php?post_type=nachrichten', null);
                
                add_submenu_page(TRAININGSSYSTEM_PLUGIN_SLUG, 'Einstellungen', 'Einstellungen',
                    'manage_options', 'options-general.php?page=trainingssystem-setting-admin', null);
                    
                $trainingssystem_Plugin_Module_Addon_Patch = new Trainingssystem_Plugin_Module_Addon_Patch();
                add_submenu_page(TRAININGSSYSTEM_PLUGIN_SLUG, 'Patch', 'Patch',
                    'manage_options', "patchtraining", array($trainingssystem_Plugin_Module_Addon_Patch, "backendAdminCreate"));
            }
             
            $trainingssystem_Plugin_Module_Berechtigung = Trainingssystem_Plugin_Module_Berechtigung::getInstance();
            add_submenu_page(TRAININGSSYSTEM_PLUGIN_SLUG, 'Berechtigungen', 'Berechtigungen',
                'manage_categories', "trainingssystem_permissions", array($trainingssystem_Plugin_Module_Berechtigung, "getPermissionOverview"));

            $trainingssystem_Plugin_Module_Help = new Trainingssystem_Plugin_Module_Help();
            add_submenu_page(TRAININGSSYSTEM_PLUGIN_SLUG, 'Hilfe', 'Hilfe',
                'manage_categories', "hilfetraining", array($trainingssystem_Plugin_Module_Help, "backendAdminCreate"));
        
            $appAdminPanel = new \Trainingssystem\Module\AppAdminPanel\AppAdminPanelModule();
            add_submenu_page(TRAININGSSYSTEM_PLUGIN_SLUG, 'App-Audio', 'App-Audio',
                'manage_categories', 'app-audio', $appAdminPanel);
        }

    }
    
    
    public function md_trainingsmgr_admin_register_menu_start_render()
    {
        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

        $changelog = array();
        $updatestring = @file_get_contents("https://devprojects2.th-luebeck.de/updateserver/wp-update-server/?action=get_metadata&slug=trainingssystem-plugin-v2");

        if($updatestring !== false) {
            $changelog_received = json_decode($updatestring, true);
            if(isset($changelog_received['sections']['changelog'])) {
                $x = $this->strposX($changelog_received['sections']['changelog'], "<h4>", 2);
                $changelog['recent'] = substr($changelog_received['sections']['changelog'], 0, $x);
                $changelog['old'] = substr($changelog_received['sections']['changelog'], $x);
            }
        }

        echo $twig->render("admin-backend/admin-backend-overview.html", [
            "changelog" => $changelog,
            "logo" => plugin_dir_url(dirname(__FILE__)) . '../../assets/img/logo2.png',
            'images' =>
                [
                    plugin_dir_url(dirname(__FILE__)) . '../../assets/img/terminzuweisung.png',
                    plugin_dir_url(dirname(__FILE__)) . '../../assets/img/Code.png',
                    plugin_dir_url(dirname(__FILE__)) . '../../assets/img/zuweisung.png',
                    plugin_dir_url(dirname(__FILE__)) . '../../assets/img/trainingsbild.png',
                    plugin_dir_url(dirname(__FILE__)) . '../../assets/img/terminzuweisungII.png',
                ],
        ]);
        
    }

    public function adminFooterText() {
        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
        return $twig->render("admin-backend/admin-backend-footer.html", [
            "tspv2_link" => get_admin_url(null, '/admin.php?page=trainingssystem-plugin-v2'),
            "logo" => plugin_dir_url(dirname(__FILE__)) . '../../assets/img/logo2.png',
        ]);
    }

    public function showAdminBar() {
        if(Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("trainingedit")) {
            show_admin_bar(true);
        } else {
            show_admin_bar(false);
        }
    }

    function strposX($haystack, $needle, $number = 0)
    {
        return strpos($haystack, $needle,
            $number > 1 ?
            $this->strposX($haystack, $needle, $number - 1) + strlen($needle) : 0
        );
    }
}

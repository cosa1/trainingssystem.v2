<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Show_Hide
 * @subpackage Show_Hide/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Show_Hide
 * @subpackage Show_Hide/public
 * @author     Helge Nissen, Markus Domin <a@b>
 */
class Trainingssystem_Plugin_Module_Show_Hide
{

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     */
    public function __construct()
    {

    }

    /**
     * [quizhn antwort="Hier steht meine Antwort" id=1] In einem Div mit der passenden Id und der Klasse class kann eine Antwort formuliert werden. Die vergebene class und die id im Shortcode müssen den Ids und Klassen entsprechen, die angesteuert werden sollen.
     *
     * @since    1.0.0
     */
    public function show_quiz($q_atts)
    {
		$twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
		ob_start();

        $q_output = "";
        if (isset($q_atts['antwort']) && ($q_atts['id'])) {

			$values = array_values($q_atts);
			array_pop($values);
			array_pop($values);

            // $value = $values[0];
            // $laenge = sizeof($values) - 2;

            echo $twig->render('show-hide/show-quiz.html', [
                'values' => $values,
                'id' => $q_atts['id'],
                'class' => $q_atts['class'],
            ]);

        } else {
            echo "Attribute fehlen für antwort=x und id=y";
        }
        return ob_get_clean();
    }

    /**
     * Es können beliebig viele Textfelder mit jeweiles einem Radiobutton angesteuert werden. Das Attribut 'name' muss mindestens einmal gesetzt sein. Das letzte Attribut ist 'wert'. Diese Variable ist auch an die Ids und die Klasse 'textfeld' zu hängen und nur einmalig pro Seite zu verwenden.
     * @since    1.0.0
     */
    public function show_radio($r_atts, $content=null)
    {
		$twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
		ob_start();
        if (isset($r_atts['name']) && ($r_atts['wert'])) {
            $type = 'version1';
			$values = array_values($r_atts);

			array_pop($values);
         

            echo $twig->render('show-hide/show-radio.html', [
                'values' => $values,
                'type' => $type,
                'wert' => $r_atts['wert'],
                'name' => $r_atts['name'],
            ]);

        }
        //Test. Muss noch ausgearbeitet werden. Version 2 für eine einfachere Nutzung. 
        else if(!isset($r_atts['wert'])){
            $type='version2'; 
            $content = do_shortcode( $content );

            if(isset($r_atts['names'])){
                extract(shortcode_atts(array(
                    'names' => array()                
                ), $r_atts ));
                $names = explode(", ", $names);
            }
            if(isset($r_atts['content'])){
                $contentName = $r_atts['content'];
                $AllContent = do_shortcode($content);
            }
            echo $twig->render('show-hide/show-radio.html', [
                'type' => $type,
                "content" => $AllContent,
                "contentName" => $contentName,
                'names' => $names,
            ]);
        }  
        else {
            echo "Attribute fehlen für name=x und wert=y";
        }
		return ob_get_clean();
    }

}

<?php

namespace Trainingssystem\Module\App\RestHandler;

use Trainingssystem\Module\App\Query\DeleteJournalEntriesQuery;
use Trainingssystem\Module\App\Query\GetAllJournalEntriesForUserIdQuery;
use Trainingssystem\Module\App\Query\SaveJournalEntriesQuery;
use Trainingssystem\Module\App\Rule\FilterDeletedJournalEntriesRule;
use Trainingssystem\Module\App\Rule\FilterNewJournalEntriesRule;
use Trainingssystem\Module\App\Query\SaveJournalEntryQuery;
use Trainingssystem\Module\App\Query\DeleteJournalEntryQuery;


class JournalRestHandler
{
    private GetAllJournalEntriesForUserIdQuery $getAllJournalEntriesForUserIdQuery;
    private SaveJournalEntriesQuery $saveJournalEntriesQuery;
    private FilterNewJournalEntriesRule $filterNewJournalEntriesRule;
    private FilterDeletedJournalEntriesRule $filterDeletedJournalEntriesRule;
    private DeleteJournalEntriesQuery $deleteJournalEntriesQuery;
    private SaveJournalEntryQuery $saveJournalEntryQuery;
    private DeleteJournalEntryQuery $deleteJournalEntryQuery;

    public function __construct(
        GetAllJournalEntriesForUserIdQuery $getAllJournalEntriesForUserIdQuery,
        SaveJournalEntriesQuery $saveJournalEntriesQuery,
        FilterNewJournalEntriesRule $filterNewJournalEntriesRule,
        FilterDeletedJournalEntriesRule $filterDeletedJournalEntriesRule,
        DeleteJournalEntriesQuery $deleteJournalEntriesQuery,
        SaveJournalEntryQuery $saveJournalEntryQuery,
        DeleteJournalEntryQuery $deleteJournalEntryQuery
    ){
        $this->getAllJournalEntriesForUserIdQuery = $getAllJournalEntriesForUserIdQuery;
        $this->saveJournalEntriesQuery = $saveJournalEntriesQuery;
        $this->filterNewJournalEntriesRule = $filterNewJournalEntriesRule;
        $this->filterDeletedJournalEntriesRule = $filterDeletedJournalEntriesRule;
        $this->deleteJournalEntriesQuery = $deleteJournalEntriesQuery;
        $this->saveJournalEntryQuery = $saveJournalEntryQuery;
        $this->deleteJournalEntryQuery = $deleteJournalEntryQuery;

    }


    /**
     * @param \WP_REST_Request $request
     * @return \WP_Error|\WP_HTTP_Response|\WP_REST_Response
     */
    public function handle(\WP_REST_Request $request)
    {
        $allEntries = json_decode($request->get_body());

        if(is_array(!$allEntries)) {
            throw new \Exception('Not an array of entries');
        }

        //$deletedEntries = $this->filterDeletedJournalEntriesRule->apply($allEntries);
        //$this->deleteJournalEntriesQuery->execute($allEntries);

        //$newEntries = $this->filterNewJournalEntriesRule->apply($allEntries);
        //$this->saveJournalEntriesQuery->execute($newEntries);

        $entries = $this->getAllJournalEntriesForUserIdQuery->execute();

        return rest_ensure_response($entries);
    }

    public function getAllEntries()
    {
        $entries = $this->getAllJournalEntriesForUserIdQuery->execute();
        return rest_ensure_response($entries);
    }

    public function addEntry(\WP_REST_Request $request)
    {
        $entry = json_decode($request->get_body());
        $entryid = $this->saveJournalEntryQuery->execute($entry);
     
         return rest_ensure_response( $entryid);
    }

     public function delEntry(\WP_REST_Request $request)
    {
        $entry = json_decode($request->get_param("_delEntry"));
        $this->deleteJournalEntryQuery->execute($entry);
        return rest_ensure_response($entry);
    }

}

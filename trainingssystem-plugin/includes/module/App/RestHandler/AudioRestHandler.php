<?php

namespace Trainingssystem\Module\App\RestHandler;

use Trainingssystem\Module\App\Query\GetAudioForTrainingIdAndUserIdQuery;

class AudioRestHandler
{
    private GetAudioForTrainingIdAndUserIdQuery $getAudioForTrainingIdAndUserIdQuery;

    public function __construct(GetAudioForTrainingIdAndUserIdQuery $getAudioForTrainingIdAndUserIdQuery)
    {
        $this->getAudioForTrainingIdAndUserIdQuery = $getAudioForTrainingIdAndUserIdQuery;
    }

    /**
     * @param \WP_REST_Request $request
     * @return \WP_Error|\WP_HTTP_Response|\WP_REST_Response
     */
    public function handle(\WP_REST_Request $request)
    {
        $user = wp_get_current_user();
        $trainingId = $request->get_param('trainingId');

        $trainingsForUser = $this->getAudioForTrainingIdAndUserIdQuery->execute($trainingId, $user->ID);

        return rest_ensure_response($trainingsForUser);
    }
}

<?php

namespace Trainingssystem\Module\App\RestHandler;

use Trainingssystem\Module\App\Query\SaveSleepEntryQuery;
use Trainingssystem\Module\App\Query\DelSaveAllSleepEntriesQuery;
use Trainingssystem\Module\App\Query\GetAllSleepEntriesForUserIdQuery;
use Trainingssystem\Module\App\Query\DeleteSleepEntryQuery;
use Trainingssystem\Module\App\Query\SaveSleepWeekQuery;
use Trainingssystem\Module\App\Query\GetAllSleepWeeksQuery;
use Trainingssystem\Module\App\Query\DelSaveAllSleepWeeksQuery;
use Trainingssystem\Module\App\Query\DeleteSleepWeekQuery;
use Trainingssystem\Module\App\Query\UpdateSleepWeekQuery;


class SleepRestHandler
{

    private SaveSleepEntryQuery $saveSleepEntryQuery;
    private DelSaveAllSleepEntriesQuery $delSaveAllSleepEntriesQuery;
    private GetAllSleepEntriesForUserIdQuery $getAllSleepEntriesForUserIdQuery;
    private DeleteSleepEntryQuery $deleteSleepEntryQuery;
    private SaveSleepWeekQuery $saveSleepWeekQuery;
    private GetAllSleepWeeksQuery $getAllSleepWeeksQuery;
    private DelSaveAllSleepWeeksQuery $delSaveAllSleepWeeksQuery;
    private DeleteSleepWeekQuery $deleteSleepWeekQuery;
    private UpdateSleepWeekQuery $updateSleepWeekQuery;



    public function __construct(
        GetAllSleepEntriesForUserIdQuery $getAllSleepEntriesForUserIdQuery,
        SaveSleepEntryQuery $saveSleepEntryQuery,
        DelSaveAllSleepEntriesQuery $delSaveAllSleepEntriesQuery,
        DeleteSleepEntryQuery $deleteSleepEntryQuery,
        SaveSleepWeekQuery $saveSleepWeekQuery,
        GetAllSleepWeeksQuery $getAllSleepWeeksQuery,
        DelSaveAllSleepWeeksQuery $delSaveAllSleepWeeksQuery,
        DeleteSleepWeekQuery $deleteSleepWeekQuery,
        UpdateSleepWeekQuery $updateSleepWeekQuery
    ){
        $this->getAllSleepEntriesForUserIdQuery = $getAllSleepEntriesForUserIdQuery;
        $this->saveSleepEntryQuery = $saveSleepEntryQuery;
        $this->delSaveAllSleepEntriesQuery = $delSaveAllSleepEntriesQuery;
        $this->deleteSleepEntryQuery = $deleteSleepEntryQuery;
        $this->saveSleepWeekQuery = $saveSleepWeekQuery;
        $this->getAllSleepWeeksQuery = $getAllSleepWeeksQuery;
        $this->delSaveAllSleepWeeksQuery = $delSaveAllSleepWeeksQuery;
        $this->deleteSleepWeekQuery = $deleteSleepWeekQuery;
        $this->updateSleepWeekQuery = $updateSleepWeekQuery;
    }


    public function update(\WP_REST_Request $request)
    {
        $allEntries = json_decode($request->get_body());

        if(is_array(!$allEntries)) {
            throw new \Exception('Not an array of entries');
        }

        $this->delSaveAllSleepEntriesQuery->execute($allEntries);

        $entries = $this->getAllSleepEntriesForUserIdQuery->execute();

        return rest_ensure_response($entries);
    }


    public function addEntry(\WP_REST_Request $request)
    {
        $entry = json_decode($request->get_body());
       // $entryid = $this->saveSleepEntryQuery->execute($entry);

       $entryid = $this->saveSleepEntryQuery->execute($entry);
     
         return rest_ensure_response( $entryid);
    }


    public function updateEntry(\WP_REST_Request $request)
    {
        $entry = json_decode($request->get_body());
       // $entryid = $this->saveSleepEntryQuery->execute($entry);

       $this->saveSleepEntryQuery->updateEntry($entry);
     
         return rest_ensure_response( $entry);
    }

    public function delEntry(\WP_REST_Request $request)
    {
        //$entry = json_decode($request->get_param("_delSleepEntry"));
        $entry = json_decode($request->get_body());
        $this->deleteSleepEntryQuery->execute($entry);
       // $entry = json_decode($request->get_body());
       //$this->saveSleepEntryQuery->deleteEntry($entry);


        return rest_ensure_response($entry);
    }

    public function addPostmetaForUserId(\WP_REST_Request $request){
        $user = wp_get_current_user();
        $data = json_decode($request->get_body());
        update_user_meta($user->ID, $data->metaKey, $data->metaValue);
    }

    public function getPostmetaForUserId(\WP_REST_Request $request){
        $user = wp_get_current_user();
        $data = json_decode($request->get_body());
        $retValue =  get_user_meta($user->ID, $data->metaKey, true);
        return $retValue;
    }

    //for cosa-app version 1 - master branch
    public function addWeek(\WP_REST_Request $request)
    {
        $entry = json_decode($request->get_body());
        $entryid = $this->saveSleepWeekQuery->execute($entry);
     
         return rest_ensure_response( $entryid);
    }

    public function updateWeeks(\WP_REST_Request $request)
    {
        $allEntries = json_decode($request->get_body());

        if(is_array(!$allEntries)) {
            throw new \Exception('Not an array of entries');
        }

        $this->delSaveAllSleepWeeksQuery->execute($allEntries);

        $entries = $this->getAllSleepWeeksQuery->execute();

        return rest_ensure_response($entries);
    }

    public function delWeek(\WP_REST_Request $request)
    {
        $entry = json_decode($request->get_param("_delSleepEntry"));
        $this->deleteSleepWeekQuery->execute($entry);
        return rest_ensure_response($entry);
    }

    public function updateWeek(\WP_REST_Request $request){
        $entry = json_decode($request->get_body());
        $this->updateSleepWeekQuery->execute($entry);
        return rest_ensure_response($entry);
    }

}
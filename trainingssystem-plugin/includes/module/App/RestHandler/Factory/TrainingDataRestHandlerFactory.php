<?php

namespace Trainingssystem\Module\App\RestHandler\Factory;

use Trainingssystem\Module\App\Query\Factory\GetAllTrainingsForUserIdQueryFactory;
use Trainingssystem\Module\App\RestHandler\TrainingDataRestHandler;

class TrainingDataRestHandlerFactory
{
    public function __invoke(): TrainingDataRestHandler
    {
        return new TrainingDataRestHandler(
            (new GetAllTrainingsForUserIdQueryFactory())()
        );
    }
}

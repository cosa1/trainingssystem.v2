<?php

namespace Trainingssystem\Module\App\RestHandler\Factory;

use Trainingssystem\Module\App\Query\DeleteJournalEntriesQuery;
use Trainingssystem\Module\App\Query\GetAllJournalEntriesForUserIdQuery;
use Trainingssystem\Module\App\Query\SaveJournalEntriesQuery;
use Trainingssystem\Module\App\RestHandler\JournalRestHandler;
use Trainingssystem\Module\App\Rule\FilterDeletedJournalEntriesRule;
use Trainingssystem\Module\App\Rule\FilterNewJournalEntriesRule;
use Trainingssystem\Module\App\Query\DeleteJournalEntryQuery;
use Trainingssystem\Module\App\Query\SaveJournalEntryQuery;

class JournalRestHandlerFactory
{
    public function __invoke(): JournalRestHandler
    {
        return new JournalRestHandler(
            new GetAllJournalEntriesForUserIdQuery(),
            new SaveJournalEntriesQuery(),
            new FilterNewJournalEntriesRule(),
            new FilterDeletedJournalEntriesRule(),
            new DeleteJournalEntriesQuery(),
            new SaveJournalEntryQuery(),
            new DeleteJournalEntryQuery()
        );
    }
}

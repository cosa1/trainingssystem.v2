<?php

namespace Trainingssystem\Module\App\RestHandler\Factory;

use Trainingssystem\Module\App\Query\GetAudioForTrainingIdAndUserIdQuery;
use Trainingssystem\Module\App\RestHandler\AudioRestHandler;

class AudioRestHandlerFactory
{
    public function __invoke(): AudioRestHandler
    {
        return new AudioRestHandler(
            new GetAudioForTrainingIdAndUserIdQuery()
        );
    }
}

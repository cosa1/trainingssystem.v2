<?php

namespace Trainingssystem\Module\App\RestHandler\Factory;

use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;
use Trainingssystem\Module\App\RestHandler\IssueJwtRestHandler;

class IssueJwtRestHandlerFactory
{
    public function __invoke(): IssueJwtRestHandler
    {
        return new IssueJwtRestHandler(Configuration::forSymmetricSigner(
            new Sha256(),
            InMemory::plainText(AUTH_KEY)
        ));
    }
}

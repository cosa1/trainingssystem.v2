<?php

namespace Trainingssystem\Module\App\RestHandler\Factory;

use Trainingssystem\Module\App\Query\GetAllSleepEntriesForUserIdQuery;
use Trainingssystem\Module\App\RestHandler\SleepRestHandler;
use Trainingssystem\Module\App\Query\SaveSleepEntryQuery;
use Trainingssystem\Module\App\Query\DelSaveAllSleepEntriesQuery;
use Trainingssystem\Module\App\Query\DeleteSleepEntryQuery;
use Trainingssystem\Module\App\Query\SaveSleepWeekQuery;
use Trainingssystem\Module\App\Query\GetAllSleepWeeksQuery;
use Trainingssystem\Module\App\Query\DelSaveAllSleepWeeksQuery;
use Trainingssystem\Module\App\Query\DeleteSleepWeekQuery;
use Trainingssystem\Module\App\Query\UpdateSleepWeekQuery;


class SleepRestHandlerFactory
{

    public function __invoke(): SleepRestHandler
    {
        return new SleepRestHandler(
            new GetAllSleepEntriesForUserIdQuery(),
            new SaveSleepEntryQuery(),
            new DelSaveAllSleepEntriesQuery(),
            new DeleteSleepEntryQuery(),
            new SaveSleepWeekQuery(),
            new GetAllSleepWeeksQuery(),
            new DelSaveAllSleepWeeksQuery(),
            new DeleteSleepWeekQuery(),
            new UpdateSleepWeekQuery()
        );
    }

}

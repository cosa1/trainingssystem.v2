<?php

namespace Trainingssystem\Module\App\Rule;

class FilterNewJournalEntriesRule
{
    public function apply(array $entries): array
    {
        $newEntries = [];

        foreach ($entries as $entry) {
            if (isset($entry->id)) {
                continue;
            }

            $newEntries[] = $entry;
        }

        return $newEntries;
    }
}

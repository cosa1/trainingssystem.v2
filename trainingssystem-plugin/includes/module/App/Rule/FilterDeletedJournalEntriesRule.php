<?php //Not needed at the moment

namespace Trainingssystem\Module\App\Rule;

class FilterDeletedJournalEntriesRule
{
    public function apply(array $entries): array
    {
        $deleted = [];

        foreach ($entries as $entry) {
            if (!isset($entry->status)) {
                continue;
            }

            if ($entry->status !== 'deleted') {
                continue;
            }

            $deleted[] = $entry;
        }

        return $deleted;
    }
}

<?php


namespace Trainingssystem\Module\App\Rule\Factory;


use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;
use Trainingssystem\Module\App\Rule\HasRequestValidJwtRule;

class HasRequestValidJwtRuleFactory
{
    public function __invoke(): HasRequestValidJwtRule
    {
        return new HasRequestValidJwtRule(
            Configuration::forSymmetricSigner(
                new Sha256(),
                InMemory::plainText(AUTH_KEY)
            )
        );
    }
}

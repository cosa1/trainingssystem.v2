<?php

namespace Trainingssystem\Module\App\Query;

class GetAllTrainingsForUserIdQuery
{
    private GetLessonsForTrainingIdAndUserIdQuery $getLessonsForTrainingIdQuery;

    public function __construct(GetLessonsForTrainingIdAndUserIdQuery $getLessonsForTrainingIdQuery)
    {
        $this->getLessonsForTrainingIdQuery = $getLessonsForTrainingIdQuery;
    }


    /**
     * Ergibt ein array von:
     * [
     *  id - ID des Trainings
     *  name - Bezeichnung des Trainings
     *  image - Bild des Trainings
     * ]
     */
    public function execute(int $userId): array
    {
        global $wpdb;

        $trainingsmgr_tranings_user_table = $wpdb->prefix . 'md_trainingsmgr_tranings_user';

        $queryString = $wpdb->prepare(
            "SELECT DISTINCT training_id id, post.post_title name, img.guid image , meta0.meta_value saveModeSingle FROM $trainingsmgr_tranings_user_table INNER JOIN (SELECT * FROM $wpdb->postmeta WHERE meta_key = 'showAppSleepSingleSave') meta0 ON meta0.meta_value = '1' AND meta0.post_id = training_id
            INNER JOIN $wpdb->posts post ON ID = training_id
            LEFT JOIN (SELECT * FROM $wpdb->postmeta WHERE meta_key = '_thumbnail_id') meta ON meta.post_id = training_id
            LEFT JOIN (SELECT * FROM $wpdb->posts) img ON img.ID = meta.meta_value WHERE user_id = %d",$userId
        );
        $trainings = $wpdb->get_results($queryString, ARRAY_A);

        for ($i = 0; $i < sizeof($trainings); $i++) {
            $trainings[$i]['lessons'] = $this->getLessonsForTrainingIdQuery->execute($trainings[$i]['id'], $userId);
            $trainings[$i]['enabledFeatures'] = ['audio' => true];
        }

        $queryString2 = $wpdb->prepare(
            "SELECT DISTINCT training_id id, post.post_title name, img.guid image , meta0.meta_value saveModeMulti FROM $trainingsmgr_tranings_user_table INNER JOIN (SELECT * FROM $wpdb->postmeta WHERE meta_key = 'showAppSleepMultiSave') meta0 ON meta0.meta_value = '1' AND meta0.post_id = training_id
            INNER JOIN $wpdb->posts post ON ID = training_id
            LEFT JOIN (SELECT * FROM $wpdb->postmeta WHERE meta_key = '_thumbnail_id') meta ON meta.post_id = training_id
            LEFT JOIN (SELECT * FROM $wpdb->posts) img ON img.ID = meta.meta_value WHERE user_id = %d",$userId
        );

        $trainings2 = $wpdb->get_results($queryString2, ARRAY_A);

        for ($i = 0; $i < sizeof($trainings2); $i++) {
            $trainings2[$i]['lessons'] = $this->getLessonsForTrainingIdQuery->execute($trainings2[$i]['id'], $userId);
            $trainings2[$i]['enabledFeatures'] = ['audio' => true];
        }
        
        $combined = array_merge($trainings,$trainings2);

        return $combined;
    }


    /**
     * Returns the amount of formfields
     * 
     * @param int ids
     * 
     * @return int Amount of form groups
     */
    public function getFormField($formids)
    {
        global $wpdb;
        $table_name_fields = $wpdb->prefix . 'md_trainingsmgr_ts_forms_fields';
        $sql = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name_fields WHERE form_ID IN ($formids)"));

        return  $sql;
    }

    public function getFormData($formids)
    {
        global $wpdb;
        $user = wp_get_current_user();

        $table_name_data = $wpdb->prefix . 'md_trainingsmgr_ts_forms_data';

        $sql = $wpdb->prepare("SELECT * FROM $table_name_data WHERE field_ID IN ($formids) AND user_ID = %d", $user->ID);

        $row = $wpdb->get_results($sql);


        if ($row != null) {
            return $row;
        }
        return 0;
    }

    /**
     * 
     * Get all the Lektion's Seiten IDs as Array
     * 
     * @param Integer Lektion-ID
     * @return Int-Array with Page-IDs
     */
    public function getLektionseiten($lektionid)
    {
        global $wpdb;
        $dbprefix_seiten = $wpdb->prefix . 'md_trainingsmgr_tranings_seiten';

        $sql = $wpdb->prepare("SELECT `seiten_id` FROM $dbprefix_seiten WHERE lektion_id = %d ORDER BY seiten_index ASC", $lektionid);
        $ids = $wpdb->get_col($sql);

        return $ids;
    }
}


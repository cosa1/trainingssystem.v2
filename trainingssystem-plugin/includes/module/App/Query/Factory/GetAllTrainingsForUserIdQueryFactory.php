<?php

namespace Trainingssystem\Module\App\Query\Factory;

use Trainingssystem\Module\App\Query\GetAllTrainingsForUserIdQuery;
use Trainingssystem\Module\App\Query\GetLessonsForTrainingIdAndUserIdQuery;

class GetAllTrainingsForUserIdQueryFactory
{
    public function __invoke(): GetAllTrainingsForUserIdQuery
    {
        return new GetAllTrainingsForUserIdQuery(
            new GetLessonsForTrainingIdAndUserIdQuery()
        );
    }
}

<?php

namespace Trainingssystem\Module\App\Query;

class UpdateSleepWeekQuery
{
    public function execute($entry): void
    {
    $my_post = array(
        'ID'           => $entry->id,
        'post_content' => json_encode($entry, JSON_UNESCAPED_UNICODE ),
    );
   
  // Update the post into the database
    wp_update_post( $my_post );
}

}

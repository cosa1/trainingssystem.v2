<?php

namespace Trainingssystem\Module\App\Query;

class SaveJournalEntryQuery
{
    private const POST_TYPE_NAME = 'appjournalentry';

    public function execute($entry)
    {
            $post = wp_insert_post(
                [
                    'post_content' => json_encode($entry, JSON_UNESCAPED_UNICODE ),
                    'post_type' => self::POST_TYPE_NAME,
                    'post_status' => 'publish',
                    'post_parent' => $entry->training
                ]
            );
    return $post;
            
    }

}
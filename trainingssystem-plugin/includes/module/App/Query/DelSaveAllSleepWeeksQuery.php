<?php

namespace Trainingssystem\Module\App\Query;

use Trainingssystem\Module\App\Exception\AuthException;

class DelSaveAllSleepWeeksQuery
{
    private const POST_TYPE_NAME = 'appsleepweek';
    public function execute(array $entries): void
    {
        foreach ($entries as $entry) {
                if($entry->status == 'added'){
                    $entry->status = '';
                    wp_insert_post(
                        [
                            'post_content' => json_encode($entry, JSON_UNESCAPED_UNICODE ),
                            'post_type' => self::POST_TYPE_NAME,
                            'post_status' => 'publish',
                        ]
                    );
                }
               else if($entry->status == 'deleted') {
                wp_delete_post($entry->id);
               }
               else if($entry->status == 'updated') {
                $entry->status = '';

                if(null == get_post( $post_id )){
                    wp_insert_post(
                        [
                            'post_content' => json_encode($entry, JSON_UNESCAPED_UNICODE ),
                            'post_type' => self::POST_TYPE_NAME,
                            'post_status' => 'publish',
                        ]
                    );
                }
                $my_post = array(
                    'ID'           => $entry->id,
                    'post_content' => json_encode($entry, JSON_UNESCAPED_UNICODE),
                );
                wp_update_post( $my_post );
                
               }
        }
    }
}

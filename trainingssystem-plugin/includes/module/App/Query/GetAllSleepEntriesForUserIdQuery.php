<?php

namespace Trainingssystem\Module\App\Query;

class GetAllSleepEntriesForUserIdQuery
{
    private const POST_TYPE_NAME = 'appsleepentry';

     public function execute(): array
     {
        $posts = get_posts(
            [
                'author' => get_current_user_id(),
                'post_type' => self::POST_TYPE_NAME,
                'numberposts' => -1,
            ]
        );

        $sleepEntries = [];

         foreach ($posts as $post) {
             $sleepEntries[] = array_merge(
                 json_decode($post->post_content, true),
                 [
                     'id' => $post->ID
                 ]
             );
        }

        return $sleepEntries;
     }
}

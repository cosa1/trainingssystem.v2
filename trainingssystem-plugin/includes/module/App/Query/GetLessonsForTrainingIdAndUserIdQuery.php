<?php


namespace Trainingssystem\Module\App\Query;


class GetLessonsForTrainingIdAndUserIdQuery
{
    public function execute(int $trainingId, int $userId): array
    {
        global $wpdb;

        $trainingsmgr_tranings_user_table = $wpdb->prefix . 'md_trainingsmgr_tranings_user';

        $queryString = $wpdb->prepare(
            "SELECT lektion_id id, post.post_title name
FROM $trainingsmgr_tranings_user_table
    LEFT JOIN $wpdb->posts post ON post.ID = lektion_id
WHERE training_id = %d AND user_id = %d",
            $trainingId, $userId
        );
        return $wpdb->get_results($queryString);
    }
}

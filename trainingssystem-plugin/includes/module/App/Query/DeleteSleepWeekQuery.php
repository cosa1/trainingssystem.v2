<?php 

namespace Trainingssystem\Module\App\Query;

use Trainingssystem\Module\App\Exception\AuthException;

class DeleteSleepWeekQuery
{
    public function execute($entry): void
    {
            $response = new \WP_REST_Response();
            $post = get_post($entry);
            if ((int)$post->post_author != get_current_user_id()) {
                throw new AuthException();
            }else{
                if( is_null(get_post($entry))){
                    echo "post $entry does not exists or was deleted";
                    throw new AuthException();
              }else{
                wp_delete_post($entry);
              }
            }
            
    }
}
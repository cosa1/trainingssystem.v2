<?php

namespace Trainingssystem\Module\App\Query;

use Trainingssystem\Module\App\Exception\AuthException;

class DelSaveAllSleepEntriesQuery
{
    private const POST_TYPE_NAME = 'appsleepentry';
    public function execute(array $entries): void
    {
        foreach ($entries as $entry) {
                if($entry->status == 'added'){
                    $entry->status = '';
                    wp_insert_post(
                        [
                            'post_content' => json_encode($entry, JSON_UNESCAPED_UNICODE ),
                            'post_type' => self::POST_TYPE_NAME,
                            'post_status' => 'publish',
                            //'post_parent' => $entry->training
                        ]
                    );
                }
               else if($entry->status == 'deleted') {
                wp_delete_post($entry->id);
               }
        }
    }
}

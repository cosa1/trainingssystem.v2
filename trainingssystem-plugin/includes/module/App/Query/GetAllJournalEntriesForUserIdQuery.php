<?php

namespace Trainingssystem\Module\App\Query;

class GetAllJournalEntriesForUserIdQuery
{
    private const POST_TYPE_NAME = 'appjournalentry';

     public function execute(): array
     {
        $posts = get_posts(
            [
                'author' => get_current_user_id(),
                'post_type' => self::POST_TYPE_NAME,
                'numberposts' => -1,
            ]
        );

        $journalEntries = [];

         foreach ($posts as $post) {
            if(($post->post_content) != null){
                $journalEntries[] = array_merge(
                    json_decode($post->post_content, true),
                    [
                        'id' => $post->ID
                    ]
                );
            }
        }

        return $journalEntries;
     }
}
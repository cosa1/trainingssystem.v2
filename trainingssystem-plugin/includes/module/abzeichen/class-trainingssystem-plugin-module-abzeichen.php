<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 * 			   Tim Mallwitz <tim.mallwitz@th-luebeck.de>
 */
class Trainingssystem_Plugin_Module_Abzeichen{


	public function __construct() {

	}

	/**
	* BACKEND
	*  Adding the meta boxes to the edit page
	*/
	public function add_abzeichen_meta_box(){
		add_meta_box("abzeichen-option-meta-box", "Abzeichen-Optionen", array($this, "custom_abzeichen_options_meta_box_markup"), "abzeichen", "normal", "high", null);
	}

	/**
	* BACKEND 
	*  Add the abzeichen options box
	*/
	public function custom_abzeichen_options_meta_box_markup(){

		global $post;

		$twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

		$allTrainings = Trainingssystem_Plugin_Database::getInstance()->TrainingDao->getAllTrainings();
		$selectedTraining = get_post_meta($post->ID, 'training_dropdown', true);
		$hasLektionsabzeichen = get_post_meta($post->ID, 'hasLektionsabzeichen', true) == "1";
		$countLektionsabzeichen = get_post_meta($post->ID, 'countLektionsabzeichen', true);
		$lektionsabzeichenStr = get_post_meta($post->ID, 'lektionsabzeichen', true);
		$lektionsabzeichen = json_decode($lektionsabzeichenStr, true);
		
		$trainings = array();
		foreach($allTrainings as $allTraining){
			$training = array();
			$training['title'] = $allTraining->getTitle();
			$training['id'] = $allTraining->getId();
			$lektionsliste = array();
			foreach($allTraining->getLektionsliste() as $allLektion){
				$lektion = array();
				$lektion['title'] = $allLektion->getTitle();
				$lektion['id'] = $allLektion->getId();
				$lektion['lektionsabzeichenCompleteURL'] = "";
				$lektion['lektionsabzeichenCompleteID'] = "";
				$lektion['lektionsabzeichenIncompleteURL'] = "";
				$lektion['lektionsabzeichenIncompleteID'] = "";
				if(isset($lektionsabzeichen[$allLektion->getId()])){
					$lektionabzeichenImages = $lektionsabzeichen[$allLektion->getId()];
					$lektion['lektionsabzeichenCompleteURL'] = $lektionabzeichenImages['lektionsabzeichenCompleteURL'];
					$lektion['lektionsabzeichenCompleteID'] = $lektionabzeichenImages['lektionsabzeichenCompleteID'];
					$lektion['lektionsabzeichenIncompleteURL'] = $lektionabzeichenImages['lektionsabzeichenIncompleteURL'];
					$lektion['lektionsabzeichenIncompleteID'] = $lektionabzeichenImages['lektionsabzeichenIncompleteID'];
				}
				$lektionsliste[] = $lektion; 
			}
			$training['lektionsliste'] = $lektionsliste;
			$trainings[] = $training;
		}
       		
		echo $twig->render('abzeichen/backend-abzeichen-options.html', [
			"postid" => $post->ID,
			"selectedTraining" => $selectedTraining,
			"hasLektionsabzeichen" => $hasLektionsabzeichen,
			"countLektionsabzeichen" => $countLektionsabzeichen,
			"trainings" => $trainings
		]);
	}

	/**
	* BACKEND
	* Function to save the options for an abzeichen 
	*/
	public function saveAbzeichenOptions(){

		if(isset($_POST['abzeichenId']) && is_numeric($_POST['abzeichenId']) &&
			isset($_POST['trainingId']) && is_numeric($_POST['trainingId']) &&
			isset($_POST['hasLektionsabzeichen']) && ($_POST['hasLektionsabzeichen'] == "false" || $_POST['hasLektionsabzeichen'] == "true") &&
			isset($_POST['countLektionsabzeichen']) && ($_POST['countLektionsabzeichen'] == "single" || $_POST['countLektionsabzeichen'] == "double") &&
			isset($_POST['lektionsabzeichen']) && strlen(trim($_POST['lektionsabzeichen'])) > 1){
				
				$abzeichenId = sanitize_text_field($_POST['abzeichenId']);
				$trainingId = sanitize_text_field($_POST['trainingId']);
				$hasLektionsabzeichen = $_POST['hasLektionsabzeichen'] == "false" ? false : true;
				$countLektionsabzeichen = sanitize_text_field($_POST['countLektionsabzeichen']);
				$lektionsabzeichenStr = $_POST['lektionsabzeichen'];

				$lektionsabzeichen = json_decode(stripslashes($lektionsabzeichenStr), true);

				if(is_string(get_post_status($abzeichenId)) && json_last_error() === JSON_ERROR_NONE){
					update_post_meta($abzeichenId, 'lektionsabzeichen', json_encode($lektionsabzeichen));
				}
				update_post_meta($abzeichenId, 'training_dropdown', $trainingId);
				update_post_meta($abzeichenId, 'hasLektionsabzeichen', $hasLektionsabzeichen);
				update_post_meta($abzeichenId, 'countLektionsabzeichen', $countLektionsabzeichen);
		}
		wp_die();
	}

	public function listabzeichen($atts) {

        $heading = isset($atts['titel']) ? $atts['titel'] : 'Abzeichen'; 
		
		$twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

		ob_start();

		$current_user = wp_get_current_user();
		if (0 == $current_user->ID) {
			return $twig->render('modal-unauth.html', ["title" => get_bloginfo('name'), "url" => get_site_url(), "modalid" => uniqid()]);
		} else {

			$trainings = Trainingssystem_Plugin_Database::getInstance()->NutzerDao->getTrainings($current_user->ID);

			$notrainings = true;
			if(sizeof($trainings)>0){
				$notrainings = false;
			}

			$posts = get_posts([
				'post_type' => 'abzeichen',
				'post_status' => 'publish',
				'numberposts' => -1
			  ]);
			
			$outputarr = array();

			foreach($trainings as $training){
				foreach($posts as $abzeichen){
					if($abzeichen->training_dropdown == $training->getID()){
						$abzeichenHasLektionsabzeichen = get_post_meta($abzeichen->ID, 'hasLektionsabzeichen', true) == "1";
						$abzeichenCountLektionsabzeichen = get_post_meta($abzeichen->ID,"countLektionsabzeichen",true);
						$abzeichenLektionsabzeichenStr = get_post_meta($abzeichen->ID, 'lektionsabzeichen', true);
						$abzeichenLektionsabzeichen = json_decode($abzeichenLektionsabzeichenStr, true);
						$outputTraining = array();
						$outputTraining['title'] = $training->getTitle();
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $abzeichen->ID ), 'single-post-thumbnail' );
						if(!$image){
							$image = $training->getImageUrl();
						}
						else{
							$image = $image[0];
						}
						$outputTraining['imageurl'] = $image;
						$outputTraining['fin'] = $training->getFin();
						$lektionsliste = $training->getLektionsliste();
						$outputLektionsliste = array();
						foreach($lektionsliste as $lektion){
							$outputLektion = array();
							$outputLektion['title'] = $lektion->getTitle();
							$outputLektion['fin'] = $lektion->getFin();
							$outputLektion['complete_image_url'] = "";
							$outputLektion['incomplete_image_url'] = "";
							if(isset($abzeichenLektionsabzeichen[$lektion->getId()])){
								$lektionabzeichenImages = $abzeichenLektionsabzeichen[$lektion->getId()];
								$outputLektion['complete_image_url'] = $lektionabzeichenImages['lektionsabzeichenCompleteURL'];
								$outputLektion['incomplete_image_url'] = $lektionabzeichenImages['lektionsabzeichenIncompleteURL'];
							}
							
							$outputLektionsliste[] = $outputLektion;
						}
						$outputTraining['lektionsliste'] = $outputLektionsliste;
						$outputTraining['hasLektionsabzeichen'] = $abzeichenHasLektionsabzeichen;
						$outputTraining['countLektionsabzeichen'] = $abzeichenCountLektionsabzeichen;
						$outputarr[] = $outputTraining;
					}
				}
			}

			$customtitle = null;
			if(isset($atts['customtitle']) && trim($atts['customtitle'])) {
				$customtitle = $atts['customtitle'];
			}

			$showtitle = null;
			if(isset($atts['showtitle']) && trim($atts['showtitle'])) {
				$showtitle = $atts['showtitle'];
			}

			echo $twig->render('abzeichen/abzeichen-overview.html',[
				"trainings"=>$outputarr,
				"customtitle" =>$customtitle,
				"showtitle" => $showtitle,
				"notrainings" => $notrainings,
				"heading" => $heading
			]);
			
		}



		return ob_get_clean();
        

	}

	public function checkProgress() {
		$userid = Trainingssystem_Plugin_Public::getTrainingUser();
		$trainingid=intval(sanitize_text_field($_POST['trainingid']));
		$training = Trainingssystem_Plugin_Database::getInstance()->TrainingDao->getTrainingByUser($trainingid, $userid);
		echo ($training->getFin()); 
	}
 
	public function showAbzeichenModal($atts){

		$twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
		$current_user = wp_get_current_user();
		if (0 == $current_user->ID) {
			return $twig->render('modal-unauth.html', ["title" => get_bloginfo('name'), "url" => get_site_url(), "modalid" => uniqid()]);
		} else {
			$userid = Trainingssystem_Plugin_Public::getTrainingUser();
			$trainingsID = htmlspecialchars(preg_replace('/[^0-9]/', '', get_query_var('idt1') . ''));
			$training = Trainingssystem_Plugin_Database::getInstance()->TrainingDao->getTrainingByUser($trainingsID, $userid);

			if(!isset($atts['titel']) || trim($atts['titel']) == ""){
				$heading = 'Herzlichen Glückwunsch!';
			}
			else{
				$heading = $atts['titel'];
			}

			if(!isset($atts['buttontitel']) || trim($atts['buttontitel']) == ""){
				$buttonTitel = 'Weiter';
			}
			else{
				$buttonTitel = $atts['buttontitel'];
			}

			if(!isset($atts['buttonlink']) || trim($atts['buttonlink']) == ""){
				$buttonLink = $training->getUrl();
			}
			else{
				$buttonLink = $atts['buttonlink'];
			}

			$textString = "Sie haben das Training erfolgreich abgeschlossen!";
			if(isset($atts['texttraining'])){
				if(trim($atts['texttraining']) != ""){
					$textString = $atts['texttraining'];
				}
			}

			$textStringLektion = "Sie haben die Lektion erfolgreich abgeschlossen!";
			if(isset($atts['textlektion'])){
				if(trim($atts['textlektion']) != ""){
					$textStringLektion = $atts['textlektion'];
				}
			}

			if(isset($atts['srctraining'])){
				if(trim($atts['srctraining']) != ""){
					$srcTraining = $atts['srctraining'];
				}
			}

			if(isset($atts['srclektion'])){
				if(trim($atts['srclektion']) != ""){
					$srcLektion = $atts['srclektion'];
				}
			}
			
			$overlayClass='img-overlay';
			if(isset($atts['overlaynone'])){
				if(trim($atts['overlaynone']) != ""){
					$overlayClass = 'img-overlay-none';
				}
			}

			$textTag = "p";
			if(isset($atts['texttag'])){
				if(trim($atts['texttag']) != ""){
					$textTag = $atts['texttag'];
				}
			}

			$page_user_progress_url = "";
			$settings = get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS);
			if(isset($settings["page_user_progress"])){
				$page_user_progress_url = get_permalink($settings["page_user_progress"]);
			}
			
			echo $twig->render('abzeichen/abzeichen-modal.html',["training" => $training,
																"page_user_progress" => $page_user_progress_url,
																"textTag" => $textTag,
																"srcTraining" => $srcTraining,
																"srcLektion" => $srcLektion,
																"textStringLektion" => $textStringLektion,
																"textString" => $textString,
																"buttonTitel" => $buttonTitel,
																"buttonLink" => $buttonLink,
																"heading" => $heading,
																"overlay_class" =>$overlayClass,
			]);
		}

    }
	
}

<?php

class Trainingssystem_Plugin_Module_Copy_Checkbox_List{

    public function __construct() {

    }
    
    // Shortcode-Handler für den Shortcode copycheckbox 
    public function scCopyCheckboxHandler($atts, $content=null){

        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
        
        $randId = mt_rand();

        if(isset($atts['id'])){
            if(trim($atts['id'])!= ""){
                $formid = trim($atts['id']);
            }
            else{
                return "Fehler: Bitte id angeben.";
            }
        }
        else{
            return "Fehler: Bitte id angeben.";
        }

        return $twig->render("copy-checkbox-list/copy-checkbox-list.html", [
            "rand" => $randId,
            "formid" => $formid
        ]);

    }
}
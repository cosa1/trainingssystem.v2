<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Markus Domin, Helge Nissen <->
 */
class Trainingssystem_Plugin_Module_Demodata
{

    //hilfe http://jsoneditoronline.org/
    //private $trainingdataraw='{"name":"Demo Training","id":"0","inhalt":"lorem ipsum" ,"lektionen":[{"name":"demolektion1","id":"0","inhalt":"lorem ipsum","seiten":[{"name":"demo Seite1","id":"0","inhalt":"lorem ipsum"},{"name":"demo Seite2","id":"0","inhalt":"lorem ipsum"},{"name":"demo Seite3","id":"0","inhalt":"lorem ipsum"}]},{"name":"demolektion2","id":"0","inhalt":"lorem ipsum","seiten":[{"name":"demo Seite4","id":"0","inhalt":"lorem ipsum"},{"name":"demo Seite5","id":"0","inhalt":"lorem ipsum"},{"name":"demo Seite6","id":"0","inhalt":"lorem ipsum"},{"name":"demo Seite7","id":"0","inhalt":"lorem ipsum"}]}]}';

    private $trainingdataraw = '{"name": "Wiki\n","id": "0", "lektionen": [{"name": "Anwendung","id": "0",  "inhalt": "Die Verwendung des Trainingssystems, also das Bereitstellen und das Erstellen von Inhalten, ist ohne Programmierkenntnisse möglich. Zur Erstellung von Inhalten muss das Wordpress-Backend zugänglich sein. Das weitere Betreiben der Plattform und das Arbeiten mit Usern ist über ein entsprechendes Menü im Frontend möglich, sodass nach Fertigstellung der Inhalte für eine geplante Plattform ausschließlich über das Frontend mit dem Trainingssystem gearbeitet werden kann. ", "seiten": [ {   "name": "Trainings, Lektionen, Seiten",  "id": "0",  "inhalt": "Unter dem Menüpunkt \"Trainingssystem\" sind Trainings, Lektionen und Seiten zu finden. Um eine Trainingsseite anzulegen, ist der Punkt \"Seiten\" zu wählen und im oberen Bereich die Schaltfläche \"Hinzufügen\" zu klicken. Im nun angezeigten Editor kann ein Titel (oben) für die Seite vergeben und der gewünschte Inhalt (mittig) erstellt werden.\nJede Seite gehört zu einer Lektion und jede Lektion ist Teil eines Trainings. Diese müssen ebenfalls erstellt werden. Das Erstellen funktioniert genau wie die Seitenerstellung. \nIm Backend unter dem Menüpunkt \"Trainings\" werden schließlich die zuvor angelegten Seiten und Lektionen zu einem Training zusammengeschaltet. Dies ist im unteren Bereich unter \"Training zusammenstellen\" möglich. Mit \"Lektion hinzufügen\" können hier Lektionen und nach Betätigung des Buttons \"Seiten\" auch Seiten über einen entsprechenden Button hinzugefügt werden.\nDas Ergebnis der Zusammenstellung bleibt stets ablesbar, da jeder Balken eine Lektion innerhalb des Trainings darstellt. Daneben kann für jede Lektion auch die Auflistung aller dazugehörigen Seiten über den entsprechenden Button eingeblendet werden.\nÜber den kleinen Button in der oberen rechten Ecke jedes Lektion-Balkens kann eine Lektion samt der dazugehörigen Seiten aus dem Training entfernt werden."    },  {   "name": "User Management",  "id": "0",    "inhalt": "Die User können über das Frontend organisiert werden. Über den Menüpunkt \"Verwaltung\" sind diesbezüglich verschiedene Optionen zugänglich. \nHinzufügen\nEine Tabelle aller registrierten Personen bildet den Kern der User-Verwaltung. In dieser sind auch Informationen zum Stand der jeweiligen Trainings oder zur Aktivität ablesbar. Weiterhin kann jeder einzelne User ausgewählt werden, um weitere Details abzurufen. Das Hinzufügen von Usern zur Plattform funktioniert einzeln oder als CSV-Import für eine größere Gruppe. Außerdem besteht die Möglichkeit, User zu Firmen und Abteilungen zu gliedern und darüber in entsprechenden Trainings Gruppenauswertungen zu erhalten. \nTrainings freigeben\nIst ein Training angelegt und ein User zur Plattform hinzugefügt, kann ganz individuell eine entsprechende Zusammenstellung an Trainings einem User zugewiesen werden. Diese Option ist entweder über den Menüpunkt \"Nutzer-Trainings\" oder über die User-Detail-Seite möglich. In der linken Spalte dieser zweigeteilten Seiten wird ein User ausgewählt, in der rechten Spalte erfolgt die Zusammenstellung der Trainings.\nProfil-User\nEs können auch vorgefertigte Profile (\"Vorlagen-User\") angelegt werden. Dies sind keine wirklichen User, sondern Profile. Eine Zusammenstellung von verschiedenen Trainings kann enem Profil zugewiesen werden. Über Zugangscodes ist ein solches vorgefertigtes Profil dann möglichen neuen Usern bereitzustellen.\nUser-Modus\nDer \"User-Modus\" erlaubt das Betrachten der Eingaben in den Trainings, beispielsweise zu Analysezwecken durch einen Coach. Er kann über die User-Tabelle in der linken Spalte für jeden User gestartet werden. Navigiert man im aktiven User-Modus in ein Training, sind die Eingaben des jeweiligen Users zu sehen.\nAktivität\nDaneben können auch verschiedene Statistiken, beispielsweise Informationen darüber, welche Seiten wie lange besucht wurden, abgerufen werden."  },  {   "name": "Darstellungsempfehlungen für Trainingsseiten",    "id": "0",   "inhalt": "Benennung \nDie Benennung der Seiten folgt bestenfalls dem Muster „Trainingsname + fortlaufende, dreistellige Nummer“. \nBeispiel: „Stress101“ – Stresstraining, Lektion 1, Seite 1Beispiel: „Regeneration409“ – Regenerationstraining, Lektion 4, Seite 9\nDie Formulare folgen auch diesem Muster, um sie in der Formularliste schnell einer Seite zuordnen zu können. \nBeispiel: „Stress104: Meine Kraftgeber“ – Formular im Stresstraining, Lektion 1, Seite 4 zum Thema Kraftgeber \nSchrift\nDie erste Überschrift einer Lektion ist immer h1.Die Überschrift direkt darunter (Kapitelüberschrift, meistens mit einer Ziffer versehen) ist h2.Alle weiteren Überschriften innerhalb der Lektion sind h3.Fragen im Quiz oder Überschriften einer unteren inhaltlichen Ebene sind h4.Alle Absätze sind im p-Tag.\nMedien\nBeim Erstellen von Trainingsseiten können Bilder über „Dateien hinzufügen“ eingebunden werden. Die Größenverhältnisse sind global überschrieben. Jedes Bild ist damit so groß wie das übergeordnete Element (siehe Layout). Im Sinne des Responsive-Design sorgt dies auch auf kleineren Bildschirmen für eine angemessene Darstellung.\nVideos werden über „Dateien hinzufügen“ eingebunden. Auch hier sind die Größenverhältnisse global geregelt und Eintragungen im Tag unwirksam.\nAudio-Sequenzen werden über „Dateien hinzufügen“ eingebunden.\n"   }  ] }, {  "name": "Entwicklung",  "id": "0", "inhalt": "lorem ipsum",  "seiten": [  {     "name": "Funktionalität hinzufügen",  "id": "0",  "inhalt": "Zur Weiterentwicklung des Plugins ist die vorgegebene Struktur einzuhalten. Es stehen Klassen für... zur Verfügung."   },    {     "name": "Design ändern",     "id": "0",    "inhalt": "Das Layout, die verwendeten Abstände und grundlegende Regeln zur Erscheinung der Trainings werden im Trainingssystem selbst unter  bestimmt. Die visuelle Erscheinung der mit diesem Trainingssystem entwickelten Trainings wird hauptsächlich durch das Theme Coachlight erzeugt. Prinzipiell ist es möglich, dieses Theme durch andere auszutauschen, die ebenfalls auf Bootstrap basieren. Dadurch können jedoch mehr oder weniger umfangreiche Anpassungen erforderlich werden. Wegen der engen Verzahnung von Plugin und Theme empfiehlt es sich, Farben, Schriften, Größen, Formen etc. im Theme Coachlight und ggf. im Plugin selbst nach eigenen Bedürfnissen anzupassen. Eine einfache Farbänderung beispielsweise ist aufgrund der Verwendung von scss-Variablen im oberen Bereich des Plugins und Themes schnell und einfach möglich.\n"    },   {    "name": "Rollen und Rechte",     "id": "0",    "inhalt": "Neben den Rollen von Wordpress wurden für das Trainingssystem weitere Rollen implementiert.\n"     },  {      "name": "Codestyle",     "id": "0",     "inhalt": "cd trainingssystem-plugin/utilities Download / Install PHP CodeSniffer using Pear or Composer. git clone https://github.com/squizlabs/PHP_CodeSniffer.git phpcs Download / Install the WordPress ruleset for the PHP CodeSniffer. git clone -b master https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards.git wpcs config CodeSniffer cd wpcs php bin/phpcs --config-set installed_paths ../wpcs/ check -> php bin/phpcs -i -> The installed coding standards are MySource, PEAR, PSR1, PSR12, PSR2, Squiz, Zend, WordPress, WordPress-Core, WordPress-Docs, WordPress-Extra and WordPress-VIP Download a copy of the Linter PHPCS package. https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/wiki/Setting-up-WPCS-to-work-in-AtomIn Atom, specify the executable path to the PHPCS binary. -> e.g: In Atom, define the set of rules you want to apply when it’s evaluating your code. -> e.g. WordPress, WordPress-VIP"      }    ]    }  ], "inhalt": "Das Trainingssystem ist ein Wordpress-Plugin zum Hosting einer Trainingsplattform auf Wordpress-Basis. Trainingseinheiten lassen sich damit in Trainings, Lektionen und Seiten gliedern. Einzelne Trainings können für spezielle User oder Gruppen von Usern (z. dB. Firmen oder Abteilungen) freigeschaltet werden. Es gelten die Voraussetzungen, die auch für Wordpress gelten. Es ist empfohlen, das Theme Coachlight zu installieren. Das Trainingssystem verwendet Bootstrap, jQuery, Fontawesome. Daneben basieren verschiedene Funktionalitäten, insbesondere das Speichern von User-Eingaben, auf dem Plugin Ninja-Forms. \nDas Trainingssystem ist im Verbund eines Forschungsprojekts entstanden, in dem spezielle Inhalte zu integrieren waren. Aus diesem Grund enthält das System und die vorliegende Dokumentation vereinzelt Sonderlösungen, beispielsweise im Bereich des Layouts oder in Form von speziellen Scripten. Diese werden aber hier aufgeführt, um sie der Nutzung oder Weiterentwicklung zugänglich zu machen. \n"}';
    private $trainingdata;
    private $trainingStatus;

    public static $demoCreatePages = [
        "Profil", //0
        "Abzeichenübersicht", //1
        "Trainingsübersichtsseite", //2
        "Trainings zuweisen", //3
        "Nutzerliste", //4
        "VorlagenNutzer", //5
        "Unternehmen/Nutzer", //6
        "CSV Import", //7
        "Nutzer anlegen", //8
        "Freischaltecodes", //9
        "Systemstatistiken", //10
        "mailbox", //11
        "Trainings Ex-/Import", //12
        "Unternehmen Details", //13
        "Nutzerliste Detailansicht", //14
        "Coaching-Übersicht", //15
        "Datenexport", //16
        "Zertifikate-Übersicht", //17
        "Hilfe-/Kontaktinformationen", //18
        "Dashboard", //19
        "Studienübersicht", //20
        "Favoriten", // 21
        "Check-Übersicht", // 22
        // "Error update Einstellungen",//12
    ];

    /**
     * Initialize the collections used to maintain the actions and filters.
     *
     * @since    1.0.0
     */
    public function __construct()
    {
        $this->trainingdata = json_decode($this->trainingdataraw, true);

    }

    public function create()
    {
        $trainingsTitleArr = array();
        $current_user_id = get_current_user_id();
        $trainings = Trainingssystem_Plugin_Database::getInstance()->NutzerDao->getTrainings($current_user_id);
        foreach ($trainings as $training) {
            array_push($trainingsTitleArr, $training->getTitle());
        }
        if (!in_array(trim($this->trainingdata["name"]), $trainingsTitleArr)) {
            $this->trainingdata["id"] = $this->createPost("trainings", $this->trainingdata["name"], $this->trainingdata["inhalt"]);
            //post-seiten anlegen
            for ($i = 0; $i < count($this->trainingdata["lektionen"]); $i++) {
                $this->trainingdata["lektionen"][$i]["id"] = $this->createPost("lektionen", $this->trainingdata["lektionen"][$i]["name"], $this->trainingdata["lektionen"][$i]["inhalt"]);
                for ($k = 0; $k < count($this->trainingdata["lektionen"][$i]["seiten"]); $k++) {
                    $this->trainingdata["lektionen"][$i]["seiten"][$k]["id"] = $this->createPost("seiten", $this->trainingdata["lektionen"][$i]["seiten"][$k]["name"], $this->trainingdata["lektionen"][$i]["seiten"][$k]["inhalt"]);
                }
            }
            Trainingssystem_Plugin_Database::getInstance()->TrainingDao->createTrainingFull($this->trainingdata);
            Trainingssystem_Plugin_Database::getInstance()->NutzerDao->setTraining($this->trainingdata);
        }else {
            $trainingStatus = 'Training existiert bereits!';
        }
    }

    public function backendAdminCreate()
    {

        if (!current_user_can('administrator')) {
            return false;
        } else {

            $nonceaction_createdemodata = TRAININGSSYSTEM_PLUGIN_SLUG . 'createdemodata';
			$noncefield = TRAININGSSYSTEM_PLUGIN_SLUG . 'createdemodata';
            $pages = self::$demoCreatePages;

            if (isset($_POST["insert_demo_data"]) && wp_verify_nonce($_POST['noncefield'], $nonceaction_createdemodata)) {
                $this->create();
            }
            if (isset($_POST["insert_demo_pages"]) && wp_verify_nonce($_POST['noncefield'], $nonceaction_createdemodata) ) { 
                $this->createDefaultPages(true);
            }
            if (isset($_POST["insert_demo_pages_single"]) && wp_verify_nonce($_POST['noncefield'], $nonceaction_createdemodata)) {
                // var_dump($_POST);
                $pageoverride=false;
                if(isset($_POST['pageoverride']) && trim($_POST['pageoverride']) == 1) {
                   $pageoverride= true; 
                } 
                $checkedPages = $_POST['checkedPages'];
				foreach ($checkedPages as $checkedPage) {
					$this->createDefaultPages(false,$checkedPage,$pageoverride);
				}
				// if(isset($_POST["page0"])) $this->createDefaultPages(false,0);
				// if(isset($_POST["page1"])) $this->createDefaultPages(false,1);
				// if(isset($_POST["page2"])) $this->createDefaultPages(false,2);
				// if(isset($_POST["page3"])) $this->createDefaultPages(false,3);
				// if(isset($_POST["page4"])) $this->createDefaultPages(false,4);
				// if(isset($_POST["page5"])) $this->createDefaultPages(false,5);
				// if(isset($_POST["page6"])) $this->createDefaultPages(false,6);
				// if(isset($_POST["page7"])) $this->createDefaultPages(false,7);
				// if(isset($_POST["page8"])) $this->createDefaultPages(false,8);
				// if(isset($_POST["page9"])) $this->createDefaultPages(false,9);
				// if(isset($_POST["page10"])) $this->createDefaultPages(false,10);
				// if(isset($_POST["page11"])) $this->createDefaultPages(false,11);
            }
        }

    }

    private function createPost($type, $trainingname, $traininginhalt,$pageoverride=false, $template="default")
    {
        // Initialize the page ID to -1. This indicates no action has been taken.
        $post_id = -1;
        // If the page doesn't already exist, then create it
        $posts = get_posts(array("post_type" => $type, "title" => $trainingname));
        //var_dump($postexist);
        if (count($posts) === 0) {

            //Set the post ID so that we know the post was created successfully
            //// Setup the author, slug, and title for the post
            $author_id = 1;
            $slug = sanitize_title($trainingname); //'example-post';
            $post_id = wp_insert_post(
                array(
                    'comment_status' => 'closed',
                    'ping_status' => 'closed',
                    'post_author' => $author_id,
                    'post_name' => $slug,
                    'post_title' => $trainingname,
                    'post_status' => 'publish',
                    'post_type' => $type,
                    'post_content' => $traininginhalt,
                    'post_excerpt' => '',
                    'page_template'  => $template
                )
            );
            echo "Erstellt, ";

        } elseif(isset($posts[0])) {
            echo "Vorhanden, ";
            if($pageoverride){
                $kv_edited_post = array(
                    'ID' => $posts[0]->ID,
                    'post_title' => $posts[0]->post_title,
                    'post_content' => $traininginhalt,
                    'page_template'  => $template
                );
                wp_update_post($kv_edited_post);
                echo "ersetzt: ";
            }else{
                echo "nicht geändert: ";
            }
            $post_id = $posts[0]->ID;
        }
        return "".$post_id;
    }

    // PAGE TEMPLATE:
    // default = Standard-Template
    // page-landing-relative.php = Landing Page
    // single-seiten.php = Large Margins
    // single-seiten-wide-content.php = Medium Margins
    // overview-page.php = Small Margins
    public function createDefaultPages($all, $page=-1,$pageoverride=false)
    {
        $options = get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS);
        // var_dump($options);

        //datenstrucktur von Settings
        //$options['page_user_progress']=$this->createPost("page","Fortschritt","[coach_edit_user_profile]");
        //$options["page_user_profil"]=$this->createPost("page","Profil","[coach_edit_user_profile]");
        // $options['coach_nutzer']=$this->createPost("page","Profil","[coach_edit_user_profile]");
        // $options['user_grouper']=$this->createPost("page","Profil","[coach_edit_user_profile]");
        // $options['trainings_overview']=$this->createPost("page","Profil","[coach_edit_user_profile]");
        // $options['lessons_overview']=$this->createPost("page","Profil","[coach_edit_user_profile]");
        // $options['psych_admin_overview']=$this->createPost("page","Profil","[coach_edit_user_profile]");
        // $options['user_menu']=$this->createPost("page","Profil","[coach_edit_user_profile]");

        // 'coach_nutzer' => string '2' (length=1)
        // 'user_grouper' => string '2' (length=1)
        // 'trainings_overview' => string '2' (length=1)
        // 'lessons_overview' => string '2' (length=1)
        // 'psych_admin_overview' => string '2' (length=1)
        // 'user_menu' => string '2' (length=1)
        // 'page_user_progress' => string '155' (length=3)
        // 'page_user_profil' => string '202' (length=3)

        //idee für die ausgabe

        //########## Alle -> user sicht ########
        if ($all || $page == 0) {
            //Profil
            $options["page_user_profil"] = $this->createPost("page", "Profil", "[" . TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . "user_profil]",$pageoverride);
            echo "Profileseite <br>";
        }if ($all || $page == 1) {
//User progress
            $options["page_user_progress"] = $this->createPost("page", "Abzeichen", "[" . TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . "abzeichen]",$pageoverride);
            echo "Abzeichenübersicht<br>";
        }if ($all || $page == 2) {
//trainingsübersicht
            $options["trainings_overview"] = $this->createPost("page", "Trainingsübersicht", "[" . TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . "user_trainings]",$pageoverride);
            echo "Trainingsübersicht<br>";

        }
//########## Admin -> user sicht ########
        if ($all || $page == 3) {
//zuweisung trainings
            $options["coach_nutzer"] = $this->createPost("page", "Trainings zuweisen", "[" . TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . "coach_nutzer]",$pageoverride, "overview-page.php");
            echo "Trainings zuweisen<br>";
        }
        if ($all || $page == 4) {
//Nutzerliste
            $options["user_list"] = $this->createPost("page", "Nutzerliste", "[" . TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . "user_list]",$pageoverride, "overview-page.php");
            echo "Nutzerliste<br>";
        }
        if ($all || $page == 5) {
//vorlagenuser anlegen
            $options["coach_vorlage"] = $this->createPost("page", "Vorlagen", "[" . TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . "vorlagen-user-grouper]",$pageoverride);
            echo "Vorlagen<br>";
        }
        if ($all || $page == 6) {
//Frimen nutzer zuweisung -> optional
            $options["user_grouper"] = $this->createPost("page", "Unternehmen", "[" . TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . "user_grouper]",$pageoverride, "overview-page.php");
            echo "Unternehmen/Nutzer<br>";
        }
        if ($all || $page == 7) {
//csv import
            $options["user_importer"] = $this->createPost("page", "CSV import", "[" . TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . "csv_import]",$pageoverride);
            echo "CSV Import<br>";
        }
        if ($all || $page == 8) {
//Nutzer anlegen
            $options["create_user"] = $this->createPost("page", "Nutzer anlegen", "[" . TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . "create_user_manually]",$pageoverride);
            echo "Nutzer anlegen<br>";
        }
        if ($all || $page == 9) {
//freischaltecode
            $options["user_code"] = $this->createPost("page", "Freischaltecodes", "[" . TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . "addregiserkey][" . TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . "showregiserkeylist]",$pageoverride);
            echo "Freischaltecode-Übersicht<br>";
        }
        if ($all || $page == 10) {
//systemstatistiken
            $options["system_statistics"] = $this->createPost("page", "Systemstatistiken", "[" . TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . "system_statistics]",$pageoverride);
            echo "Systemstatistiken<br>";
        }
        if ($all || $page == 11) {
        //mailbox
            $options["mailbox_view"] = $this->createPost("page", "Nachrichten", "[" . TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . "mailbox]",$pageoverride);
            echo "Nachrichten<br>";
        }
        if ($all || $page == 12) {
        //training im/export
            $options["trainings_ex_import"] = $this->createPost("page", "Trainings Ex-/Import", "[" . TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . "trainings_ex_import]",$pageoverride);
            echo "Trainings ex-/importieren <br>";
        }

        if ($all || $page == 13) {
            //unternehmen details
            $options["company_details"] = $this->createPost("page", "Unternehmen Details", "[" . TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . "company_details]",$pageoverride);
            echo "Unternehmen Details <br>";
        }
        if ($all || $page == 14) {
            //userliste detailansicht 
            $options["user_list_detail"] = $this->createPost("page", "Nutzerliste Detail", "[" . TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . "user_list_detail]",$pageoverride);
            echo "Nutzerliste Detailansicht <br>";
        }
		
		if ($all || $page == 15) {
            //Coaching-Overview 
            $options["coaching_overview"] = $this->createPost("page", "Coaching-Übersicht", "[" . TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . "coaching_overview]",$pageoverride);
            echo "Coaching-Übersicht <br>";
        }

		if ($all || $page == 16) {
            //Datenexport 
            $options["dataexport"] = $this->createPost("page", "Datenexport", "[" . TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . "data_export]",$pageoverride);
            echo "Datenexport <br>";
        }

        if ($all || $page == 17) {
            //Zertifikate 
            $options["zertifikate"] = $this->createPost("page", "Zertifikate-Übersicht", "[" . TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . "zertifikate]",$pageoverride);
            echo "Zertifikate-Übersicht <br>";
        }

        if ($all || $page == 18) {
            //Hilfe 
            $options["show_contactinfo"] = $this->createPost("page", "Hilfe-/Kontaktinformationen", "[" . TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . "show_contactinfo]",$pageoverride);
            echo "Hilfe-/Kontaktinformationen <br>";
        }

        if ($all || $page == 19) {
            //Dashboard 
            $options["dashboard"] = $this->createPost("page", "Dashboard", "[" . TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . "dashboard]",$pageoverride, "page-landing-relative.php");
            echo "Dashboard <br>";
        }
        
        if ($all || $page == 20) {
            //Study-Overview 
            $options["study_overview"] = $this->createPost("page", "Studienübersicht", "[" . TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . "study_overview]",$pageoverride, "overview-page.php");
            echo "Studienübersicht <br>";
        }
        if ($all || $page == 21) {
            //Favoriten 
            $options["exercise_favorites"] = $this->createPost("page", "Favoriten", "[" . TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . "exercise_favorites]",$pageoverride);
            echo "Favoriten <br>";
        }
        if ($all || $page == 22) {
            //Check-Overview 
            $options["check_results"] = $this->createPost("page", "Check-Übersicht", "[" . TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . "check_result_chart tsform-id=ID DER ERGEBNISSE label-type=TYP DES CHECKS]",$pageoverride);
            echo "Check-Übersicht <br>";
        }
		
        // var_dump($options);
        // delete_option(TRAININGSSYSTEM_PLUGIN_SETTINGS);
        // add_option(TRAININGSSYSTEM_PLUGIN_SETTINGS, $options);

        if (!update_option(TRAININGSSYSTEM_PLUGIN_SETTINGS, $options)) {
            // echo "Error update Einstellungen <br>";
        }

    }

}

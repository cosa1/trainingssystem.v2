<?php

class Trainingssystem_Plugin_Module_TS_Carousel{

    public function __construct() {

    }

    public function showTSCarousel($atts, $content=null){

        $contentTemp = trim($content);
        $sliderImages = array();
        $doc = new DOMDocument();
        $doc->loadHTML($contentTemp);
        $images = $doc->getElementsByTagName('img');
        
        $sliderImagesCount = 0;
        foreach($images as $image){
            $sliderImages[$sliderImagesCount]['src'] = $image->getAttribute('src');
            $sliderImages[$sliderImagesCount]['alt'] = $image->getAttribute('alt');
            $sliderImagesCount++;
        }
        

        
        $fullwidth = false;
        if(isset($atts['fullwidth'])){
            if(trim($atts['fullwidth']) == "1"){
                $fullwidth = true;
            }
        }
        
        $indicators = false;
        if(isset($atts['indicators'])){
            if(trim($atts['indicators']) == "1"){
                $indicators = true;
            }
        }

        $controls = false;
        if(isset($atts['controls'])){
            if(trim($atts['controls']) == "1"){
                $controls = true;
            }
        }

        $imageText = array();
        if(isset($atts['text'])){
            if(trim($atts['text']) != ""){
                $imageTextString = $atts['text'];
                $imageText = explode("|", $imageTextString);
            }
        }
        
        $imageHeadlines = array();
        if(isset($atts['headline'])){
            if(trim($atts['headline']) != ""){
                $imageHeadlinesString = $atts['headline'];
                $imageHeadlines = explode("|", $imageHeadlinesString);
            }
        }
        $imageTextColor = "FFFFFF";
        if(isset($atts['textcolor'])){
            if(trim($atts['textcolor']) != ""){
                $imageTextColor = $atts['textcolor'];
            }
        }
        
        $imageHeadlinesColor = "FFFFFF";
        if(isset($atts['headlinecolor'])){
            if(trim($atts['headlinecolor']) != ""){
                $imageHeadlinesColor = $atts['headlinecolor'];
            }
        }

        $links = array();
        $linkTexts = array();
        if(isset($atts['links'])){
            if(trim($atts['links']) != ""){
                $linkString = $atts['links'];
                $links= explode("|", $linkString);
             }
             if(trim($atts['linktexts']) != ""){
                $linkTextString = $atts['linktexts'];
                $linkTexts = explode("|", $linkTextString);
             }
        }

        $interval = 5000;
        if(isset($atts['interval']) && trim($atts['interval']) != ""){
            if(is_numeric($atts['interval'])){
                $interval = $atts['interval'];
            }
        }

        $amount = 4;
        if(isset($atts['amount']) && trim($atts['amount']) != ""){
            if(is_numeric($atts['amount'])){
                $amount = $atts['amount'];

                if ($amount == 1) {
                    $amount = 12;
                } elseif ($amount == 2) {
                    $amount = 6;
                } elseif ($amount == 3) {
                    $amount = 4;
                } elseif ($amount == 4) {
                    $amount = 3;
                } elseif ($amount == 6) {
                    $amount = 2;
                }
            }
        }

        $border = "border-1";
        if(isset($atts['border']) && trim($atts['border']) != ""){
                $border = $atts['border'];
        }

        $border = "border-1";
        if(isset($atts['border'])){
            if(trim($atts['border']) == "0"){
                $border = "border-0";
            }
        }
      
        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
        return $twig->render('carousel/ts-carousel.html', [
            'sliderimages' => $sliderImages,
            'interval' => $interval,
            'imagetext' => $imageText,
            'imageheadline' => $imageHeadlines,
            'imagetextcolor' => $imageTextColor,
            'imageheadlinecolor' => $imageHeadlinesColor,
            'fullwidth' => $fullwidth,
            'indicators' => $indicators,
            'controls' => $controls,
            'link' => $links,
            'linktext' => $linkTexts,
            'amount' => $amount,
            'border' => $border,
            'rand' => mt_rand(),
        ]);
    }
}

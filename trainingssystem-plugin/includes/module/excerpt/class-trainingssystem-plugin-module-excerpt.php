<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Coach_User_Remover
 * @subpackage Coach_User_Remover/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Coach_User_Remover
 * @subpackage Coach_User_Remover/public
 * @author     Markus Domin, Helge Nissen <markus.domin@fh-luebeck.de>
 */
class Trainingssystem_Plugin_Module_Excerpt
{

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     */
    public function __construct() {
	}
  
	/**
	 * Remove metabox from post
	 */
	public function remove_excerpt_metabox() {
		//remove_meta_box('postexcerpt', 'post', 'normal');
		remove_meta_box('postexcerpt', 'trainings', 'normal');
		remove_meta_box('postexcerpt', 'lektionen', 'normal');
	}
  
	/**
	 * Strip tags
	 *
	 * @param string $text
	 * @return string
	 */
	public function wp_trim_excerpt($text = '') {
		return strip_tags($text, '<a><strong><em><b><i><code><ul><ol><li><blockquote><del><ins><img><pre><code><>');
	}
  
	/**
	 * More sign...
	 *
	 * @return string
	 */
	public function excerpt_more() {
		return '&hellip;';
	}
  
	/**
	 * Excerpt editor after post title.
	 *
	 * @param $post
	 */
	public function excerpt($post) {
		if ($post->post_type !== 'trainings' && $post->post_type !== 'lektionen' ) return;
		wp_editor(
			html_entity_decode($post->post_excerpt),
			'html-excerpt',
			[
				'teeny' => true,
				'quicktags' => true,
				'wpautop' => true,
				'media_buttons' => false,
				'textarea_rows' => 7,
				'textarea_name' => 'excerpt'
			]
		);
	}
	
}

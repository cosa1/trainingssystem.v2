<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 */
class Trainingssystem_Plugin_Module_Progress{


    public function __construct() {


    }

    public function get_progress($atts)
    {
        if(is_array($atts))
        if(array_key_exists('tname',$atts))
        {
            $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
            $userid = Trainingssystem_Plugin_Public::getTrainingUser();
            $args = array("post_type" => "trainings", "title" => $atts['tname']);
            $query = get_posts($args);
            if(count($query) === 0 || !isset($query[0])){
                return " Keine Seite gefunden für den Fortschritt";
            }else {
                $trainingsID = $query[0]->ID;
                $training = Trainingssystem_Plugin_Database::getInstance()->TrainingDao->getTrainingByUser($trainingsID,$userid);
                $percent=false;
                if(isset($atts['percent']))$percent=true;
                return $twig->render('progress.html', [
                    'lektionen'=>$training->getLektionsliste(),'percent'=> $percent
                ] );
            }
        }else{
            return "tname ist nicht angegeben für den Fortschritt";
        }
        
        
    }

    public function showTrainingProgress($atts){
  
        $heading = isset($atts['titel']) ? $atts['titel'] : 'Detail Trainingsfortschritt'; 
        
        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
        ob_start();
        $userid = Trainingssystem_Plugin_Public::getTrainingUser();
        $trainings = Trainingssystem_Plugin_Database::getInstance()->NutzerDao->getTrainings($userid);
        echo $twig->render('trainingprogress/showprogress.html', [
            'trainings'=>$trainings,
            'heading' => $heading,
        ] );


        return ob_get_clean();
    }

}

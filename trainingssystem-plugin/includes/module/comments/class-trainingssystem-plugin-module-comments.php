<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 */
class Trainingssystem_Plugin_Module_Comments{


	public function __construct() {

	}

	public function blockComments() {
		$settings = get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS);
		if(isset($settings['disablecommentblocking']) && $settings['disablecommentblocking'] == "1") {
			return true;
		} 
		return false;
	}
	
	public function	showform($atts = array(), $content = '' )
	{
		$settings = get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS);
		if(isset($settings['disablecommentblocking']) && $settings['disablecommentblocking'] == "1") {
			ob_start();
			if( is_singular() && post_type_supports( get_post_type(), 'comments' ) )
			{
				comment_form();

			}else{
				echo "error comment support";
			}           
			return ob_get_clean();
		}
		return "";
	}

	public function showlist(){

		$settings = get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS);
		if(isset($settings['disablecommentblocking']) && $settings['disablecommentblocking'] == "1") {

			$twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
			ob_start();

			$comments = get_comments('post_id='.get_the_ID());

			echo $twig->render('comments/comments-list.html',["comments"=>$comments]);
				
			return ob_get_clean();
		}
		return "";
	}


	 /**
     * [trainings_add_lektionen_footer_frontend description]
     * @param [type] $content [description]
     */
    public function comments_footer_frontend($content)
    {
		$settings = get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS);
		if(isset($settings['disablecommentblocking']) && $settings['disablecommentblocking'] == "1") {
			global $post;
			if( $post->comment_status == 'open' ) {
				$content .= '['.TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'comments_list]'.'['.TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'comments_form]';
			}
			return $content;
		} 
		return $content;
    }

}

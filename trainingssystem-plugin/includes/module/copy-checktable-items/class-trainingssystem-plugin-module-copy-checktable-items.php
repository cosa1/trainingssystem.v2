<?php

class Trainingssystem_Plugin_Module_Copy_Checktable_Items{

    public function __construct() {

    }
    
    // Shortcode-Handler für den Shortcode copychecktableitems
    public function scCopyChecktableItems($atts, $content=null){

        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

        if(isset($atts['id']) && isset($atts['pos']) && isset($atts['sammlung'])){

            if(trim($atts['id']) != "" && trim($atts['pos']) != "" && trim($atts['sammlung']) != ""){
                $tableid = trim($atts['id']);
                $tablepos = trim($atts['pos']);
                $formid = trim($atts['sammlung']);
            }
            else{
                return "Fehler: Bitte id, pos und sammlung angeben.";
            }

        }
        else{
            return "Fehler: Bitte id, pos und sammlung angeben.";
        }
        
        return $twig->render("copy-checktable-items/copy-checktable-items.html", [
            "tableid" => $tableid,
            "tablepos" => $tablepos,
            "formid" => $formid
        ]);        
    }
}
<?php
/**
 * Checks
 *
 * @author      Helge Nissen <helge.nissen@th-luebeck.de>
 *              <->
 *              Max Sternitzke <max.sternitzke@th-luebeck.de> (Integration to this Plugin)
 *
 * @package     Checks
 */
class Trainingssystem_Plugin_Module_Checks {

    private static $checkinfo = array(
            "arbeitsplatz_digiexist" => array(
                    "name" => "Arbeitsplatz-Check Digi-Exist",
                    "labels" => array("orga" => "Arbeitsorganisation", "zeit" => "Work-Life-Balance", "kultur" => "Unternehmenskultur"),
                    "colors" => array("orga" => "#fcba03", "zeit" => "#03fcba", "kultur" => "#d60d0d"),
                    "reverse" => array("orga" => false, "zeit" => false, "kultur" => false),
            ),
            "unternehmen_digiexist" => array(
                    "name" => "Unternehmens-Check Digi-Exist",
                    "labels" => array("orga" => "Arbeitsorganisation", "zeit" => "Work-Life-Balance", "kultur" => "Unternehmenskultur", "schutz" => "Arbeitsschutz", "belastung" => "Gefährdungsbeurteilung psychischer Belastung"),
                    "colors" => array("orga" => "#fcba03", "zeit" => "#03fcba", "kultur" => "#d60d0d", "schutz" => "#3ec72c", "belastung" => "#910d8d"),
                    "reverse" => array("orga" => false, "zeit" => false, "kultur" => false, "schutz" => false, "belastung" => false),
            ),
            "gesundheit_digiexist" => array(
                    "name" => "Gesundheits-Check Digi-Exist",
                    "labels" => array("phq" => "Erschöpfung", "isi" => "Schlafprobleme", "pss" => "Stress", "ptq" => "Grübeln", "audit" => "Alkoholkonsum"),
                    "colors" => array("phq" => "#fcba03", "isi" => "#03fcba", "pss" => "#d60d0d", "ptq" => "#3ec72c", "audit" => "#910d8d"),
                    "reverse" => array("phq" => false, "isi" => false, "pss" => false, "ptq" => false, "audit" => false),
            ),
            "arbeitsplatz_c4c" => array(
                    "name" => "Arbeitsplatz-Check",
                    "labels" => array("orga" => "Organisationskultur", "privatleben" => "Arbeit und Privatleben in Einklang", "gestaltung" => "Gestaltung von Arbeitsaufgaben und -abläufen", "umgang" => "Professioneller Umgang mit Tod, Sterben und Gewalt", "umgebung" => "Gesunde Arbeitsumgebung"),
                    "colors" => array("orga" => "#fcba03", "privatleben" => "#03fcba", "gestaltung" => "#d60d0d", "umgang" => "#3867d6", "umgebung" => "#92d638"),
                    "reverse" => array("orga" => false, "privatleben" => false, "gestaltung" => false, "umgang" => false, "umgebung" => false),
            ),
            "gesundheit_c4c" => array(
                    "name" => "Gesundheits-Check",
                    "labels" => array("stress" => "Stress", "selbstkritik" => "Selbstkritik", "sorgen" => "Grübeln und Sorgen", "sleepcare" => "Schlafbeschwerden"),
                    "colors" => array("stress" => "#fcba03", "selbstkritik" => "#03fcba", "sorgen" => "#910d8d", "sleepcare" => "#d60d0d"),
                    "reverse" => array("stress" => false, "selbstkritik" => false, "sorgen" => false, "sleepcare" => false),
            ),
            "arbeitsplatz_oc" => array(
                    "name" => "Arbeitsplatz-Check",
                    "labels" => array("arbeitsumgebung" => "Arbeitsumgebung", "arbeitsorganisation" => "Arbeitsorganisation",
                            "work_life_balance" => "Work-Life-Balance",
                            "unternehmenskultur" => "Unternehmenskultur"),
                    "colors" => array("arbeitsorganisation" => "#fcba03", "work_life_balance" => "#03fcba", "unternehmenskultur" => "#d60d0d",
                            "arbeitsumgebung" => "#3867d6"),
                    "reverse" => array("arbeitsorganisation" => false, "work_life_balance" => false, "unternehmenskultur" => false, "arbeitsumgebung" => false),
            ),
            "gesundheit_oc" => array(
                    "name" => "Gesundheits-Check",
                    "labels" => array("stress" => "Stress", "selbstkritik" => "Selbstkritik", "perseverativesdenken" => "Perseveratives Denken",
                            "regeneration" => "Schlaf und Regeneration","alkoholkonsum" => "Alkoholkonsum"),
                    "colors" => array("stress" => "#fcba03", "selbstkritik" => "#03fcba", "perseverativesdenken" => "#910d8d",
                            "regeneration" => "#d60d0d", "alkoholkonsum" => "#3867d6"),
                    "reverse" => array("stress" => false, "selbstkritik" => false, "perseverativesdenken" => false, "regeneration" => false, "alkoholkonsum" => false),
            ),
            "jolanda" => array(
                    "name" => "Jolanda-Check",
                    //"labels" => array("realistisch" => "realistisch", "intellektuell" => "intellektuell", "kuenstlerisch" => "kuenstlerisch", "sozial" => "sozial", "unternehmerisch" => "unternehmerisch", "konventionell" => "konventionell"),
                    "labels" => array("realistisch" => "Die Handwerklichen (R)", "intellektuell" => "Die Forschenden (I)", "kuenstlerisch" => "Die Kuenstlerischen (A)", "sozial" => "Die Helfenden (S)", "unternehmerisch" => "Die Unternehmerischen (E)", "konventionell" => "Die Organisierenden (C)"),
                    "colors" => array("realistisch" => "#fcba03", "intellektuell" => "#03fcba", "kuenstlerisch" => "#910d8d", "sozial" => "#d60d0d",  "unternehmerisch" =>"#2980b9", "konventionell" => "#16a085" ),
                    "reverse" => array("realistisch" => false, "intellektuell" => false, "kuenstlerisch" => false, "sozial" => false, "unternehmerisch" => false, "konventionell" =>false ),
            ),
    );

    function show_checkbplatz($atts)
    {
        $title = isset($atts['titel']) ? $atts['titel'] : 'Arbeitsplatzcheck';

        $orgaid = null;
        if(isset($atts['orga-id']) && trim($atts['orga-id']) != "" && is_numeric($atts['orga-id']) && get_post_type($atts['orga-id']) == "formulare") {
            $orgaid = $atts['orga-id'];
        }

        $zeitid = null;
        if(isset($atts['zeit-id']) && trim($atts['zeit-id']) != "" && is_numeric($atts['zeit-id']) && get_post_type($atts['zeit-id']) == "formulare") {
            $zeitid = $atts['zeit-id'];
        }

        $kulturid = null;
        if(isset($atts['kultur-id']) && trim($atts['kultur-id']) != "" && is_numeric($atts['kultur-id']) && get_post_type($atts['kultur-id']) == "formulare") {
            $kulturid = $atts['kultur-id'];
        }

        $resultid = null;
        if(isset($atts['result-id']) && trim($atts['result-id']) != "" && is_numeric($atts['result-id']) && get_post_type($atts['result-id']) == "formulare") {
            $resultid = $atts['result-id'];
        } elseif( !isset($atts['result-id'])) {
            $resultid = "";
        }

        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

        if(!is_null($orgaid) && !is_null($zeitid) && !is_null($kulturid) && !is_null($resultid)) {
            return $twig->render("checks/digiexist/check-b-platz.html", ["title" => $title,
                                                            "userid" => get_current_user_id(),
                                                            "orga" => do_shortcode("[ts_forms id='" . $orgaid . "']"),
                                                            "zeit" => do_shortcode("[ts_forms id='" . $zeitid . "']"),
                                                            "kultur" => do_shortcode("[ts_forms id='" . $kulturid . "']"),
                                                            "result_tsform" => $resultid != "" ? do_shortcode("[ts_forms id='" . $resultid . "']") : '',
                                                            "resultid" => $resultid,
                                                        ]);
        } else {
            return $twig->render("checks/digiexist/check-b-platz-error.html");
        }
    }

    function show_checkb($atts)
    {

        $title = isset($atts['titel']) ? $atts['titel'] : 'Unternehmenscheck';

        $orgaid = null;
        if(isset($atts['orga-id']) && trim($atts['orga-id']) != "" && is_numeric($atts['orga-id']) && get_post_type($atts['orga-id']) == "formulare") {
            $orgaid = $atts['orga-id'];
        }

        $zeitid = null;
        if(isset($atts['zeit-id']) && trim($atts['zeit-id']) != "" && is_numeric($atts['zeit-id']) && get_post_type($atts['zeit-id']) == "formulare") {
            $zeitid = $atts['zeit-id'];
        }

        $kulturid = null;
        if(isset($atts['kultur-id']) && trim($atts['kultur-id']) != "" && is_numeric($atts['kultur-id']) && get_post_type($atts['kultur-id']) == "formulare") {
            $kulturid = $atts['kultur-id'];
        }

        $schutzid = null;
        if(isset($atts['schutz-id']) && trim($atts['schutz-id']) != "" && is_numeric($atts['schutz-id']) && get_post_type($atts['schutz-id']) == "formulare") {
            $schutzid = $atts['schutz-id'];
        }

        $belastungid = null;
        if(isset($atts['belastung-id']) && trim($atts['belastung-id']) != "" && is_numeric($atts['belastung-id']) && get_post_type($atts['belastung-id']) == "formulare") {
            $belastungid = $atts['belastung-id'];
        }

        $resultid = null;
        if(isset($atts['result-id']) && trim($atts['result-id']) != "" && is_numeric($atts['result-id']) && get_post_type($atts['result-id']) == "formulare") {
            $resultid = $atts['result-id'];
        } elseif( !isset($atts['result-id'])) {
            $resultid = "";
        }

        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

        if(!is_null($orgaid) && !is_null($zeitid) && !is_null($kulturid) && !is_null($schutzid) && !is_null($resultid)) {
            return $twig->render("checks/digiexist/check-b.html", ["title" => $title,
                                                        "userid" => get_current_user_id(),
                                                        "orga" => do_shortcode("[ts_forms id='" . $orgaid . "']"),
                                                        "zeit" => do_shortcode("[ts_forms id='" . $zeitid . "']"),
                                                        "kultur" => do_shortcode("[ts_forms id='" . $kulturid . "']"),
                                                        "schutz" => do_shortcode("[ts_forms id='" . $schutzid . "']"),
                                                        "belastung" => do_shortcode("[ts_forms id='" . $belastungid . "']"),
                                                        "result_tsform" => $resultid != "" ? do_shortcode("[ts_forms id='" . $resultid . "']") : '',
                                                        "resultid" => $resultid,
                                                        ]);
        } else {
            return $twig->render("checks/digiexist/check-b-error.html");
        }
    }

    function show_checklg($atts)
    {

        $title = isset($atts['titel']) ? $atts['titel'] : 'Gesundheits-Check';

        $phqid = null;
        if(isset($atts['phq-id']) && trim($atts['phq-id']) != "" && is_numeric($atts['phq-id']) && get_post_type($atts['phq-id']) == "formulare") {
            $phqid = $atts['phq-id'];
        }

        $isiid = null;
        if(isset($atts['isi-id']) && trim($atts['isi-id']) != "" && is_numeric($atts['isi-id']) && get_post_type($atts['isi-id']) == "formulare") {
            $isiid = $atts['isi-id'];
        }

        $pssid = null;
        if(isset($atts['pss-id']) && trim($atts['pss-id']) != "" && is_numeric($atts['pss-id']) && get_post_type($atts['pss-id']) == "formulare") {
            $pssid = $atts['pss-id'];
        }

        $ptqid = null;
        if(isset($atts['ptq-id']) && trim($atts['ptq-id']) != "" && is_numeric($atts['ptq-id']) && get_post_type($atts['ptq-id']) == "formulare") {
            $ptqid = $atts['ptq-id'];
        }

        $auditid = null;
        if(isset($atts['audit-id']) && trim($atts['audit-id']) != "" && is_numeric($atts['audit-id']) && get_post_type($atts['audit-id']) == "formulare") {
            $auditid = $atts['audit-id'];
        }

        $resultid = null;
        if(isset($atts['result-id']) && trim($atts['result-id']) != "" && is_numeric($atts['result-id']) && get_post_type($atts['result-id']) == "formulare") {
            $resultid = $atts['result-id'];
        } elseif( !isset($atts['result-id'])) {
            $resultid = "";
        }

        $erschoepfungid = null;
        if(isset($atts['erschoepfung-id']) && trim($atts['erschoepfung-id']) != "" && is_numeric($atts['erschoepfung-id']) && get_post_type($atts['erschoepfung-id']) == "trainings") {
            $erschoepfungid = $atts['erschoepfung-id'];
        }

        $schlafid = null;
        if(isset($atts['schlaf-id']) && trim($atts['schlaf-id']) != "" && is_numeric($atts['schlaf-id']) && get_post_type($atts['schlaf-id']) == "trainings") {
            $schlafid = $atts['schlaf-id'];
        }

        $stressid = null;
        if(isset($atts['stress-id']) && trim($atts['stress-id']) != "" && is_numeric($atts['stress-id']) && get_post_type($atts['stress-id']) == "trainings") {
            $stressid = $atts['stress-id'];
        }

        $alkoholid = null;
        if(isset($atts['alkohol-id']) && trim($atts['alkohol-id']) != "" && is_numeric($atts['alkohol-id']) && get_post_type($atts['alkohol-id']) == "trainings") {
            $alkoholid = $atts['alkohol-id'];
        }

        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

        if(!is_null($phqid) && !is_null($isiid) && !is_null($pssid) && !is_null($ptqid) && !is_null($auditid) && !is_null($resultid) && !is_null($erschoepfungid) && !is_null($schlafid) && !is_null($stressid) && !is_null($alkoholid)) {
            return $twig->render("checks/digiexist/check-lg.html", ["title" => $title,
                                                            "userid" => get_current_user_id(),
                                                            "phq" => do_shortcode("[ts_forms id='" . $phqid . "']"),
                                                            "isi" => do_shortcode("[ts_forms id='" . $isiid . "']"),
                                                            "pss" => do_shortcode("[ts_forms id='" . $pssid . "']"),
                                                            "ptq" => do_shortcode("[ts_forms id='" . $ptqid . "']"),
                                                            "audit" => do_shortcode("[ts_forms id='" . $auditid . "']"),
                                                            "result_tsform" => $resultid != "" ? do_shortcode("[ts_forms id='" . $resultid . "']") : '',
                                                            "resultid" => $resultid,
                                                            "training" => array("erschoepfung" => do_shortcode("[tspv2_training_accordion training='" . $erschoepfungid . "']"),
                                                                                    "schlaf" => do_shortcode("[tspv2_training_accordion training='" . $schlafid . "']"),
                                                                                    "stress" => do_shortcode("[tspv2_training_accordion training='" . $stressid . "']"),
                                                                                    "alkohol" => do_shortcode("[tspv2_training_accordion training='" . $alkoholid . "']"),
                                                                            ),
                                                            "profile_link" => get_permalink(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)["page_user_profil"]),
                                                        ]);
        } else {
            return $twig->render("checks/digiexist/check-lg-error.html");
        }
    }

    /**
     *
     * C4C Check Arbeitsplatz
     */

    function show_check_arbeitsplatz($atts)
    {
        $title = isset($atts['titel']) ? $atts['titel'] : 'Arbeitsplatz-Check';

        $orgaid = null;
        if(isset($atts['orga-id']) && trim($atts['orga-id']) != "" && is_numeric($atts['orga-id']) && get_post_type($atts['orga-id']) == "formulare") {
            $orgaid = $atts['orga-id'];
        }

        $privatlebenid = null;
        if(isset($atts['privatleben-id']) && trim($atts['privatleben-id']) != "" && is_numeric($atts['privatleben-id']) && get_post_type($atts['privatleben-id']) == "formulare") {
            $privatlebenid = $atts['privatleben-id'];
        }

        $gestaltungid = null;
        if(isset($atts['gestaltung-id']) && trim($atts['gestaltung-id']) != "" && is_numeric($atts['gestaltung-id']) && get_post_type($atts['gestaltung-id']) == "formulare") {
            $gestaltungid = $atts['gestaltung-id'];
        }

        $umgangid = null;
        if(isset($atts['umgang-id']) && trim($atts['umgang-id']) != "" && is_numeric($atts['umgang-id']) && get_post_type($atts['umgang-id']) == "formulare") {
            $umgangid = $atts['umgang-id'];
        }

        $umgebungid = null;
        if(isset($atts['umgebung-id']) && trim($atts['umgebung-id']) != "" && is_numeric($atts['umgebung-id']) && get_post_type($atts['umgebung-id']) == "formulare") {
            $umgebungid = $atts['umgebung-id'];
        }

        $resultid = null;
        if(isset($atts['result-id']) && trim($atts['result-id']) != "" && is_numeric($atts['result-id']) && get_post_type($atts['result-id']) == "formulare") {
            $resultid = $atts['result-id'];
        } elseif( !isset($atts['result-id'])) {
            $resultid = "";
        }

        $empfehlungen = null;
        if(isset($atts['empfehlungen']) && trim($atts['empfehlungen']) != "" ) {
            $empfehlungen = $atts['empfehlungen'];
        }elseif( !isset($atts['empfehlungen'])) {
            $empfehlungen = "";
        }

        //Option für den Direktstart nach Seitenaufruf:
        $start = null;
        if (isset($atts['start']) && trim($atts['start']) == 1) {
            $start = $atts['start'];
        }

        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

        if(!is_null($orgaid) && !is_null($privatlebenid) && !is_null($gestaltungid) && !is_null($umgangid) && !is_null($umgebungid) && !is_null($resultid)) {
            return $twig->render("checks/c4c/check_arbeitsplatz.html", ["title" => $title,
                                                            "userid" => get_current_user_id(),
                                                            "orga" => do_shortcode("[ts_forms id='" . $orgaid . "']"),
                                                            "privatleben" => do_shortcode("[ts_forms id='" . $privatlebenid . "']"),
                                                            "gestaltung" => do_shortcode("[ts_forms id='" . $gestaltungid . "']"),
                                                            "umgang" => do_shortcode("[ts_forms id='" . $umgangid . "']"),
                                                            "umgebung" => do_shortcode("[ts_forms id='" . $umgebungid . "']"),
                                                            "result_tsform" => $resultid != "" ? do_shortcode("[ts_forms id='" . $resultid . "']") : '',
                                                            "resultid" => $resultid,
                                                            "check_results_overview_link" => isset(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)["check_results"]) ? get_permalink(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)["check_results"]) : "",
                                                            "profile_link" => get_permalink(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)["page_user_profil"]),
                                                            "arbeitsplatz_empfehlungen_accordion" => ($empfehlungen != "" ? do_shortcode("[tspv2_training_accordion training='" . $empfehlungen . "']") : false),
                                                            "start" => $start,
                                                        ]);
        } else {
            return $twig->render("checks/c4c/check_arbeitsplatz-error.html");
        }
    }

     /**
     *
     * C4C Check Gesundheits
     */

    function show_check_gesundheit($atts)
    {
        $title = isset($atts['titel']) ? $atts['titel'] : 'Gesundheits-Check';

        $stressid = null;
        if(isset($atts['stress-id']) && trim($atts['stress-id']) != "" && is_numeric($atts['stress-id']) && get_post_type($atts['stress-id']) == "formulare") {
            $stressid = $atts['stress-id'];
        }

        $selbstkritikid = null;
        if(isset($atts['selbstkritik-id']) && trim($atts['selbstkritik-id']) != "" && is_numeric($atts['selbstkritik-id']) && get_post_type($atts['selbstkritik-id']) == "formulare") {
            $selbstkritikid = $atts['selbstkritik-id'];
        }

        $sorgenid = null;
        if(isset($atts['sorgen-id']) && trim($atts['sorgen-id']) != "" && is_numeric($atts['sorgen-id']) && get_post_type($atts['sorgen-id']) == "formulare") {
            $sorgenid = $atts['sorgen-id'];
        }

        $sleepcareid = null;
        if(isset($atts['sleepcare-id']) && trim($atts['sleepcare-id']) != "" && is_numeric($atts['sleepcare-id']) && get_post_type($atts['sleepcare-id']) == "formulare") {
            $sleepcareid = $atts['sleepcare-id'];
        }

        $resultid = null;
        if(isset($atts['result-id']) && trim($atts['result-id']) != "" && is_numeric($atts['result-id']) && get_post_type($atts['result-id']) == "formulare") {
            $resultid = $atts['result-id'];
        } elseif( !isset($atts['result-id'])) {
            $resultid = "";
        }

        // Empfehlungs-Accordion:
        $empfehlungen = null;
        if(isset($atts['empfehlungen']) && trim($atts['empfehlungen']) != "" ) {
            $empfehlungen = $atts['empfehlungen'];
        }elseif( !isset($atts['empfehlungen'])) {
            $empfehlungen = "";
        }

        //URL zu empfohlenen Inhalten übergeben:
        $empfehlung_url = null;
        if(isset($atts['empfehlung_url']) && trim($atts['empfehlung_url']) != "" ) {
            $empfehlung_url = $atts['empfehlung_url'];
        }elseif( !isset($atts['empfehlung_url'])) {
            $empfehlung_url = "";
        }

        //Option für den Direktstart nach Seitenaufruf:
        $start = null;
        if (isset($atts['start']) && trim($atts['start']) == 1) {
            $start = $atts['start'];
        }

        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

        if(!is_null($stressid) && !is_null($selbstkritikid) && !is_null($sorgenid) && !is_null($sleepcareid) && !is_null($resultid)) {
            return $twig->render("checks/c4c/check_gesundheit.html", ["title" => $title,
                                                            "userid" => get_current_user_id(),
                                                            "stress" => do_shortcode("[ts_forms id='" . $stressid . "']"),
                                                            "selbstkritik" => do_shortcode("[ts_forms id='" . $selbstkritikid . "']"),
                                                            "sorgen" => do_shortcode("[ts_forms id='" . $sorgenid . "']"),
                                                            "sleepcare" => do_shortcode("[ts_forms id='" . $sleepcareid . "']"),
                                                            "result_tsform" => $resultid != "" ? do_shortcode("[ts_forms id='" . $resultid . "']") : '',
                                                            "resultid" => $resultid,
                                                            "check_results_overview_link" => isset(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)["check_results"]) ? get_permalink(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)["check_results"]) : "",
                                                            "profile_link" => get_permalink(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)["page_user_profil"]),
                                                            "gesundheit_empfehlungen_accordion" => ($empfehlungen != "" ? do_shortcode("[tspv2_training_accordion training='" . $empfehlungen . "']") : false),
                                                            "empfehlung_url" => $empfehlung_url,
                                                            "start" => $start,
                                                        ]);
        } else {
            return $twig->render("checks/c4c/check_gesundheit-error.html");
        }
    }


    /**
     *
     * oncampus  Check arbeitsplatz
     *
     * @param $atts Array Uebergabeparameter
     */
    function show_check_arbeitsplatztrainings_oc($atts) {
        $title = "Arbeitsplatztrainings-Check";
        if (isset($atts['titel']) && trim($atts['titel']) != "") {
            $title = $atts['titel'];
        }

        $arbeitsumgebung_id = null;
        if (isset($atts['arbeitsumgebung-id']) && trim($atts['arbeitsumgebung-id']) != "" &&
                is_numeric($atts['arbeitsumgebung-id']) &&
                get_post_type($atts['arbeitsumgebung-id']) == "formulare") {
            $arbeitsumgebung_id = $atts['arbeitsumgebung-id'];
        }

        $arbeitsorganisation_id = null;
        if (isset($atts['arbeitsorganisation-id']) && trim($atts['arbeitsorganisation-id']) != "" &&
                is_numeric($atts['arbeitsorganisation-id']) &&
                get_post_type($atts['arbeitsorganisation-id']) == "formulare") {
            $arbeitsorganisation_id = $atts['arbeitsorganisation-id'];
        }

        $work_life_balance_id = null;
        if (isset($atts['work-life-balance-id']) && trim($atts['work-life-balance-id']) != "" &&
                is_numeric($atts['work-life-balance-id']) &&
                get_post_type($atts['work-life-balance-id']) == "formulare") {
            $work_life_balance_id = $atts['work-life-balance-id'];
        }

        $unternehmenskultur_id = null;
        if (isset($atts['unternehmenskultur-id']) && trim($atts['unternehmenskultur-id']) != "" &&
                is_numeric($atts['unternehmenskultur-id']) &&
                get_post_type($atts['unternehmenskultur-id']) == "formulare") {
            $unternehmenskultur_id = $atts['unternehmenskultur-id'];
        }

        //Option für den Direktstart nach Seitenaufruf:
        $start = null;
        if (isset($atts['start']) && trim($atts['start']) == 1) {
            $start = $atts['start'];
        }

        $empfehlungen = null;
        if (isset($atts['empfehlungen']) && trim($atts['empfehlungen']) != "") {
            $empfehlungen = $atts['empfehlungen'];
        } else if (!isset($atts['empfehlungen'])) {
            $empfehlungen = "";
        }

        $resultid = null;
        if (isset($atts['result-id']) && trim($atts['result-id']) != ""
                && is_numeric($atts['result-id']) && get_post_type($atts['result-id']) == "formulare") {
            $resultid = $atts['result-id'];
        } else if (!isset($atts['result-id'])) {
            $resultid = "";
        }
        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

        if (!is_null($arbeitsumgebung_id) && !is_null($arbeitsorganisation_id) && !is_null($work_life_balance_id) &&
                !is_null($unternehmenskultur_id)) {
            return $twig->render("checks/oc/check_arbeitsplatztrainings_oc.html", [
                    "title" => $title,
                    "start" => $start,
                    "userid" => get_current_user_id(),
                    "arbeitsumgebung" => do_shortcode("[ts_forms id='" . $arbeitsumgebung_id . "']"),
                    "arbeitsorganisation" => do_shortcode("[ts_forms id='" . $arbeitsorganisation_id . "']"),
                    "work_life_balance" => do_shortcode("[ts_forms id='" . $work_life_balance_id . "']"),
                    "unternehmenskultur" => do_shortcode("[ts_forms id='" . $unternehmenskultur_id . "']"),
                    "result_tsform" => $resultid != "" ? do_shortcode("[ts_forms id='" . $resultid . "']") : '',
                    "resultid" => $resultid,
                    "empfehlungen_accordion" => ($empfehlungen != "" ?
                            do_shortcode("[tspv2_training_accordion training='" . $empfehlungen . "']") : false),
                    "check_results_overview_link" => isset(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)["check_results"]) ?
                            get_permalink(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)["check_results"]) : "",
                    "profile_link" => get_permalink(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)["page_user_profil"]),

            ]);
        }
        return $twig->render("checks/oc/check_arbeitsplatztrainings_oc-error.html");
    }


    /**
     *
     * oncampus  Check Gesundheitstrainings
     *
     * @param $atts Array Uebergabeparameter
     */
    function show_check_gesundheitstrainings_oc($atts) {
        $title = "Gesundheitstrainings-Check";
        if (isset($atts['titel']) && trim($atts['titel']) != "") {
            $title = $atts['titel'];
        }

        $stress_id = null;
        if (isset($atts['stress-id']) && trim($atts['stress-id']) != "" &&
                is_numeric($atts['stress-id']) &&
                get_post_type($atts['stress-id']) == "formulare") {
            $stress_id = $atts['stress-id'];
        }

        $selbstkritik_id = null;
        if (isset($atts['selbstkritik-id']) && trim($atts['selbstkritik-id']) != "" &&
                is_numeric($atts['selbstkritik-id']) &&
                get_post_type($atts['selbstkritik-id']) == "formulare") {
            $selbstkritik_id = $atts['selbstkritik-id'];
        }

        $perseverativesdenken_id = null;
        if (isset($atts['perseverativesdenken-id']) && trim($atts['perseverativesdenken-id']) != "" &&
                is_numeric($atts['perseverativesdenken-id']) &&
                get_post_type($atts['perseverativesdenken-id']) == "formulare") {
            $perseverativesdenken_id = $atts['perseverativesdenken-id'];
        }

        $regeneration_id =null;
        if (isset($atts['regeneration-id']) && trim($atts['regeneration-id']) != ""
                && is_numeric($atts['regeneration-id']) &&
                get_post_type($atts['regeneration-id']) == "formulare") {
            $regeneration_id = $atts['regeneration-id'];
        }

        $alkoholkonsum1_id =null;
        if (isset($atts['alkoholkonsum1-id']) && trim($atts['alkoholkonsum1-id']) != "" &&
                is_numeric($atts['alkoholkonsum1-id']) &&
                get_post_type($atts['alkoholkonsum1-id']) == "formulare") {
            $alkoholkonsum1_id = $atts['alkoholkonsum1-id'];
        }

        $alkoholkonsum2_id =null;
        if (isset($atts['alkoholkonsum2-id']) && trim($atts['alkoholkonsum2-id']) != "" &&
                is_numeric($atts['alkoholkonsum2-id']) &&
                get_post_type($atts['alkoholkonsum2-id']) == "formulare") {
            $alkoholkonsum2_id = $atts['alkoholkonsum2-id'];
        }

        $alkoholkonsum3_id = null;
        if (isset($atts['alkoholkonsum3-id']) && trim($atts['alkoholkonsum3-id']) != "" &&
                is_numeric($atts['alkoholkonsum3-id']) &&
                get_post_type($atts['alkoholkonsum3-id']) == "formulare") {
            $alkoholkonsum3_id = $atts['alkoholkonsum3-id'];
        }

        //Option für den Direktstart nach Seitenaufruf:
        $start = null;
        if (isset($atts['start']) && trim($atts['start']) == 1) {
            $start = $atts['start'];
        }

        $resultid = null;
        if (isset($atts['result-id']) && trim($atts['result-id']) != ""
                && is_numeric($atts['result-id']) && get_post_type($atts['result-id']) == "formulare") {
            $resultid = $atts['result-id'];
        } else if (!isset($atts['result-id'])) {
            $resultid = "";
        }

        $empfehlungen = null;
        if (isset($atts['empfehlungen']) && trim($atts['empfehlungen']) != "") {
            $empfehlungen = $atts['empfehlungen'];
        } else if (!isset($atts['empfehlungen'])) {
            $empfehlungen = "";
        }

        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

        if (!is_null($stress_id) && !is_null($selbstkritik_id) &&
                !is_null($regeneration_id) && !is_null($perseverativesdenken_id) &&
                !is_null($alkoholkonsum1_id) && !is_null($alkoholkonsum2_id) &&
                !is_null($alkoholkonsum3_id) && !is_null($resultid)) {
            return $twig->render("checks/oc/check_gesundheitstrainings_oc.html", [
                    "title" => $title,
                    "start" => $start,
                    "userid" => get_current_user_id(),
                    "stress" => do_shortcode("[ts_forms id='" . $stress_id . "']"),
                    "selbstkritik" => do_shortcode("[ts_forms id='" . $selbstkritik_id . "']"),
                    "perseverativesdenken" => do_shortcode("[ts_forms id='" . $perseverativesdenken_id . "']"),
                    "regeneration" => do_shortcode("[ts_forms id='" . $regeneration_id . "']"),
                    "alkoholkonsum1" => do_shortcode("[ts_forms id='" . $alkoholkonsum1_id . "']"),
                    "alkoholkonsum2" => do_shortcode("[ts_forms id='" . $alkoholkonsum2_id . "']"),
                    "alkoholkonsum3" => do_shortcode("[ts_forms id='" . $alkoholkonsum3_id . "']"),
                    "result_tsform" => $resultid != "" ? do_shortcode("[ts_forms id='" . $resultid . "']") : '',
                    "resultid" => $resultid,
                    "empfehlungen_accordion" => ($empfehlungen != "" ?
                            do_shortcode("[tspv2_training_accordion training='" . $empfehlungen . "']") : false),
                    "check_results_overview_link" => isset(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)["check_results"]) ?
                            get_permalink(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)["check_results"]) : "",
                    "profile_link" => get_permalink(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)["page_user_profil"]),

            ]);
        } else {
            return $twig->render("checks/oc/check_gesundheitstrainings_oc-error.html");
        }
    }


     /**
     *
     * Jolanda Check
     */
    function show_check_jolanda($atts)
    {
        $title = isset($atts['titel']) ? $atts['titel'] : 'Jolanda-Check';

        $realistisch = null;
        if(isset($atts['realistisch-id']) && trim($atts['realistisch-id']) != "" && is_numeric($atts['realistisch-id']) && get_post_type($atts['realistisch-id']) == "formulare") {
            $realistisch_id = $atts['realistisch-id'];
        }

        $intellektuell = null;
        if(isset($atts['intellektuell-id']) && trim($atts['intellektuell-id']) != "" && is_numeric($atts['intellektuell-id']) && get_post_type($atts['intellektuell-id']) == "formulare") {
            $intellektuell_id = $atts['intellektuell-id'];
        }

        $kuenstlerisch = null;
        if(isset($atts['kuenstlerisch-id']) && trim($atts['kuenstlerisch-id']) != "" && is_numeric($atts['kuenstlerisch-id']) && get_post_type($atts['kuenstlerisch-id']) == "formulare") {
            $kuenstlerisch_id = $atts['kuenstlerisch-id'];
        }

        $sozial = null;
        if(isset($atts['sozial-id']) && trim($atts['sozial-id']) != "" && is_numeric($atts['sozial-id']) && get_post_type($atts['sozial-id']) == "formulare") {
            $sozial_id = $atts['sozial-id'];
        }

        $unternehmerisch = null;
        if(isset($atts['unternehmerisch-id']) && trim($atts['unternehmerisch-id']) != "" && is_numeric($atts['unternehmerisch-id']) && get_post_type($atts['unternehmerisch-id']) == "formulare") {
            $unternehmerisch_id = $atts['unternehmerisch-id'];
        }

        $konventionell = null;
        if(isset($atts['konventionell-id']) && trim($atts['konventionell-id']) != "" && is_numeric($atts['konventionell-id']) && get_post_type($atts['konventionell-id']) == "formulare") {
            $konventionell_id = $atts['konventionell-id'];
        }

        $resultid = null;
        if(isset($atts['result-id']) && trim($atts['result-id']) != "" && is_numeric($atts['result-id']) && get_post_type($atts['result-id']) == "formulare") {
            $resultid = $atts['result-id'];
        } elseif( !isset($atts['result-id'])) {
            $resultid = "";
        }


        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

        if(!is_null($realistisch_id) && !is_null($intellektuell_id) && !is_null($sozial_id) && !is_null($unternehmerisch_id) && !is_null($konventionell_id) && !is_null($resultid)) {
            return $twig->render("checks/jolanda/check_jolanda.html", ["title" => $title,
                                                            "userid" => get_current_user_id(),
                                                            "realistisch" => do_shortcode("[ts_forms id='" . $realistisch_id . "']"),
                                                            "intellektuell" => do_shortcode("[ts_forms id='" . $intellektuell_id . "']"),
                                                            "kuenstlerisch" => do_shortcode("[ts_forms id='" . $kuenstlerisch_id . "']"),
                                                            "sozial" => do_shortcode("[ts_forms id='" . $sozial_id . "']"),
                                                            "unternehmerisch" => do_shortcode("[ts_forms id='" . $unternehmerisch_id . "']"),
                                                            "konventionell" => do_shortcode("[ts_forms id='" . $konventionell_id . "']"),
                                                            "result_tsform" => $resultid != "" ? do_shortcode("[ts_forms id='" . $resultid . "']") : '',
                                                            "resultid" => $resultid,
                                                            "check_results_overview_link" => isset(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)["check_results"]) ? get_permalink(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)["check_results"]) : "",
                                                            "profile_link" => get_permalink(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)["page_user_profil"]),

                                                        ]);
        } else {
            return $twig->render("checks/jolanda/check_jolanda-error.html");
        }
    }


    /**
     * Zeigt die Check-Ergbnisse als zwei Diagramme an
     * - das aktuellste Ergebnis als Bar- oder Radar-Chart
     * - der Verlauf der letzten x/allen Ergebnissen als Line-Chart
     *
     * @param Array atts aus Wp-Shortcode
     *
     * @return HTML-Code
     */
    public function showCheckResultChart($atts) {
        $config_error = false;

        $title = isset($atts['titel']) ? $atts['titel'] : 'Check-Übersicht';

        $limit = -1;
        if(isset($atts['limit']) && is_numeric($atts['limit']) && ($atts['limit'] == -1 || $atts['limit'] > 0)) {
            $limit = $atts['limit'];
        }

        // Abwärtskompatibilität
        $tsformid = null;
        if(isset($atts['tsform-id']) && trim($atts['tsform-id']) != "" && is_numeric($atts['tsform-id']) && get_post_type($atts['tsform-id']) == "formulare") {
            $tsformid = $atts['tsform-id'];
        }

        // Abwärtskompatibilität
        $checkinfo = [];
        if(isset($atts['label-type']) && trim($atts['label-type']) != "" && isset(self::$checkinfo[$atts['label-type']])) {
            $checkinfo = self::$checkinfo[$atts['label-type']];
        }

        $mode = "both";
        if(isset($atts['mode']) && trim($atts['mode']) != "" && in_array($atts['mode'], array("both", "latest", "history"))) {
            $mode = $atts['mode'];
        } elseif(isset($atts['mode']) && trim($atts['mode']) != "" && !in_array($atts['mode'], array("both", "latest", "history"))) {
            $config_error = true;
        }

        $latest_limit = -1;
        if(isset($atts['latest-limit']) && is_numeric($atts['latest-limit']) && ($atts['latest-limit'] == -1 || $atts['latest-limit'] > 0)) {
            $latest_limit = $atts['latest-limit'];
        }
        // OR
        $latest_limit_each = null;
        if(isset($atts['latest-limit-each']) && is_numeric($atts['latest-limit-each']) && ($atts['latest-limit-each'] == -1 || $atts['latest-limit-each'] > 0)) {
            $latest_limit_each = $atts['latest-limit-each'];
        }
        // ELSE fail
        if($latest_limit != -1 && !is_null($latest_limit_each)) {
            $config_error = true;
        }

        $sorted = 1;
        if(isset($atts['sorted']) && is_numeric($atts['sorted']) && $atts['sorted'] == 0){
            if(($latest_limit == -1 && is_null($latest_limit_each))||  $latest_limit_each == -1){
                $sorted = 0;
            }
            else{
                $config_error = true;
            }
            
        }

        $selectedchecks = [];
        foreach($atts as $a_key => $a_tsformid) {
            if(isset(self::$checkinfo[$a_key]) && trim($a_tsformid) != "" && is_numeric($a_tsformid) && get_post_type($a_tsformid) == "formulare") {
                $selectedchecks[$a_key] = array();
                $selectedchecks[$a_key]['tsformid'] = $a_tsformid;
                $selectedchecks[$a_key]['checkname'] = $a_key;
                $selectedchecks[$a_key]['latest_results'] = $this->getCheckResults($a_tsformid, false, 1);
            }
        }

        if(sizeof($selectedchecks) == 0 && sizeof($checkinfo) > 0 && !is_null($tsformid)) {
            $selectedchecks[$atts['label-type']] = array();
            $selectedchecks[$atts['label-type']]['tsformid'] = $tsformid;
            $selectedchecks[$atts['label-type']]['checkname'] = $atts['label-type'];
            $selectedchecks[$atts['label-type']]['latest_results'] = $this->getCheckResults($tsformid, false, 1);
        } elseif(sizeof($selectedchecks) > 0 && sizeof($checkinfo) > 0 && !is_null($tsformid)) {
            $config_error = true;
        } elseif(sizeof($selectedchecks) == 0 && sizeof($checkinfo) == 0 && is_null($tsformid)) {
            $config_error = true;
        }

        // Aktuell:
        $latest = array();
        $i = 0;
        foreach($selectedchecks as $check) {
            $first = array_shift($check['latest_results']);
            if(!is_null($first)) {
                foreach($first->getDataJsonDecoded() as $d_key => $d) {
                    $latest[$i]['label'] = self::$checkinfo[$check['checkname']]['labels'][$d_key];
                    $latest[$i]['color'] = self::$checkinfo[$check['checkname']]['colors'][$d_key];
                    $latest[$i]['checkname'] = $check['checkname'];
                    $latest[$i]['check_param'] = $d_key;
                    $latest[$i]['tsformid'] = $check['tsformid'];
                    $latest[$i]['data'] = self::$checkinfo[$check['checkname']]['reverse'][$d_key] ? (100- $d) : $d;
                    $latest[$i]['data_display'] = $d;
                    $i++;
                }
            }

        }

        if(is_null($latest_limit_each)) {
            if($sorted == 1){
                usort($latest, function($a, $b) {
                    if($a['data'] == $b['data']) {
                        return strcmp($a['label'], $b['label']); // Label aufsteigend sortieren
                    } else {
                        if($a['data'] == $b['data']) {
                            return 0;
                        }
                        return ($a['data'] < $b['data']) ? 1 : -1; // Daten absteigend sortieren
                    }
                });
            }
            if($latest_limit != -1) {
                $latest = array_slice($latest, 0, $latest_limit);
            }
        } else {
            $latest_tmp = array();

            foreach($latest as $l) {
                $latest_tmp[$l['checkname']][] = $l;
            }
            $latest = [];

            foreach($latest_tmp as $l_t) {
                if($sorted == 1){
                    usort($l_t, function($a, $b) {
                        if($a['data'] == $b['data']) {
                            return strcmp($a['label'], $b['label']); // Label aufsteigend sortieren
                        } else {
                            if($a['data'] == $b['data']) {
                                return 0;
                            }
                            return ($a['data'] < $b['data']) ? 1 : -1; // Daten absteigend sortieren
                        }
                    });
                }
                
                if($latest_limit_each != -1) {
                    $l_t = array_slice($l_t, 0, $latest_limit_each);
                }
                $latest = array_merge($latest, $l_t);
            }
            if($sorted == 1){
                usort($latest, function($a, $b) {
                    if($a['data'] == $b['data']) {
                        return strcmp($a['label'], $b['label']); // Label aufsteigend sortieren
                    } else {
                        if($a['data'] == $b['data']) {
                            return 0;
                        }
                        return ($a['data'] < $b['data']) ? 1 : -1; // Daten absteigend sortieren
                    }
                });
            }

        }

        // Verlauf:
        $history_results = [];
        $history_tsform_data = [];
        if($mode == "both" || $mode == "history") {
            foreach($latest as $l) {
                if(!isset($history_tsform_data[$l['tsformid']])) {
                    $history_tsform_data[$l['tsformid']] = $this->getCheckResults($l['tsformid'], true, $limit);
                }
            }

            foreach($history_tsform_data as $his_data_form) {
                foreach($his_data_form as $his_data) {
                    if(!isset($history_results[$his_data->getSaveDate()])) {
                        $history_results[$his_data->getSaveDate()] = [];

                        foreach($latest as $l) {
                            $history_results[$his_data->getSaveDate()][$l['checkname'] . '-' . $l['check_param']] = [];
                            $history_results[$his_data->getSaveDate()][$l['checkname'] . '-' . $l['check_param']]['data'] = null;
                            $history_results[$his_data->getSaveDate()][$l['checkname'] . '-' . $l['check_param']]['label'] = self::$checkinfo[$l['checkname']]['labels'][$l['check_param']];
                            $history_results[$his_data->getSaveDate()][$l['checkname'] . '-' . $l['check_param']]['color'] = self::$checkinfo[$l['checkname']]['colors'][$l['check_param']];;
                        }
                    }

                    foreach($his_data->getDataJsonDecoded() as $data_key => $data) {
                        foreach($latest as $l) {
                            if($data_key == $l['check_param']) {
                                $history_results[$his_data->getSaveDate()][$l['checkname'] . '-' . $l['check_param']]['data'] = $data;
                            }
                        }
                    }
                }
            }
            ksort($history_results);
        }

        $max_value = 100;
        if(isset($atts['max_value']) && is_numeric($atts['max_value'])) {
            $max_value = $atts['max_value'];
        }

        $charttype_latest = "bar";
        if(isset($atts['charttype_latest']) && trim($atts['charttype_latest']) != "" && in_array($atts['charttype_latest'], array("bar", "radar"))){
            $charttype_latest = $atts['charttype_latest'];
        }

        $radar_alpha = 127; // 50%
		if(isset($atts['radar_alpha']) && trim($atts['radar_alpha']) != "" && is_numeric($atts['radar_alpha'])){
			$radar_alpha = intval((intval(trim($atts['radar_alpha']))/100)*255);
		}

        if(isset($atts['radar_backgroundcolor'])){
            if(preg_match("/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){1}|([a-f]|[A-F]|[0-9]){3}|([a-f]|[A-F]|[0-9]){5})?\b/", $atts['radar_backgroundcolor'])) {
                $radar_backgroundColor = $atts['radar_backgroundcolor'];
            }
            else{
                $config_error = true;
            }
        }
        else{
            $radar_backgroundColor = $this->random_color($radar_alpha);
        }

        if(isset($atts['radar_bordercolor'])){
            if(preg_match("/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){1}|([a-f]|[A-F]|[0-9]){3}|([a-f]|[A-F]|[0-9]){5})?\b/", $atts['radar_bordercolor'])) {
                $radar_borderColor = $atts['radar_bordercolor'];
            }
            else{
                $config_error = true;
            }
        }
        else{
            $radar_borderColor = $this->random_color($radar_alpha);
        }

        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
        if($config_error) {
            return $twig->render("checks/check-result-chart-error.html", ["checkinfo" => self::$checkinfo]);
        } else {
            return $twig->render("checks/check-result-chart.html", ["results" => $history_results,
                                                                    "latest" => $latest,
                                                                    "title" => $title,
                                                                    "random" => bin2hex(random_bytes(32)),
                                                                    "mode" => $mode,
                                                                    "max_value" => $max_value,
                                                                    "charttype_latest" => $charttype_latest,
                                                                    "radar_backgroundColor" => $radar_backgroundColor,
                                                                    "radar_borderColor" => $radar_borderColor
                                                                ]);
        }
    }

    /**
     * Zeigt die Check-Ergbnisse als Liste an
     *
     * @param Array atts aus Wp-Shortcode
     *
     * @return HTML-Code
     */
    public function showCheckResultList($atts) {

        $title = isset($atts['titel']) ? $atts['titel'] : 'Check-Übersicht';

        $limit = -1;
        if(isset($atts['limit']) && is_numeric($atts['limit']) && ($atts['limit'] == -1 || $atts['limit'] > 0)) {
            $limit = $atts['limit'];
        }

        $tsformid = null;
        if(isset($atts['tsform-id']) && trim($atts['tsform-id']) != "" && is_numeric($atts['tsform-id']) && get_post_type($atts['tsform-id']) == "formulare") {
            $tsformid = $atts['tsform-id'];
        }

        $checkinfo = [];
        if(isset($atts['label-type']) && trim($atts['label-type']) != "" && isset(self::$checkinfo[$atts['label-type']])) {
            $checkinfo = self::$checkinfo[$atts['label-type']];
        }

        $order_asc = false;
        if(isset($atts['order_asc']) && trim($atts['order_asc']) != "" && $atts['order_asc'] == "true") {
            $order_asc = true;
        }

        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

        if(sizeof($checkinfo) == 0 || is_null($tsformid)) {
            return $twig->render("checks/check-result-list-error.html", ["checkinfo" => self::$checkinfo]);
        } else {
            return $twig->render("checks/check-result-list.html", ["results" => $this->getCheckResults($tsformid, $order_asc, $limit),
                                                                    "checkinfo" => $checkinfo,
                                                                    "title" => $title,
                                                                ]);
        }

    }

    /**
     * AJAX
     *
     * Triggers a new user event for check completed
     *
     * @param $_POST['checktitle'] Title of Check
     * @param $_POST['url'] URL of Page where check was completed
     */
    public function checkCompleted() {
        if(isset($_POST['checktitle']) && trim($_POST['checktitle']) != "" &&
            isset($_POST['url']) && trim($_POST['url']) != "") {

            $usermodus = false;
            $uid = get_current_user_id();
            $selectui = '' . get_user_meta(wp_get_current_user()->ID, 'coach_select_user', true);
            if ($selectui != '') {
                $uid = $selectui;
                $usermodus = true;
            }

            if(!$usermodus) {

                $title = "Check „" . sanitize_text_field($_POST['checktitle']) . "“ abgeschlossen";
                $subtitle = "";
                $url = esc_url_raw($_POST['url']);

                // Params: $title, $subtitle, $userid, $postid currently null until checks v2, $url, $type, $level, $date = null) {
                Trainingssystem_Plugin_Database::getInstance()->UserEvent->insertUserEvent($title, $subtitle, $uid, null, $url, "check", "info");
            }
        }
        wp_die();
    }

    /**
     * AJAX
     *
     * Triggers a new user event for check saved
     *
     * @param $_POST['checktitle'] Title of Check
     * @param $_POST['url'] URL of Page where check was completed and saved
     */
    public function checkSaved() {
        if(isset($_POST['checktitle']) && trim($_POST['checktitle']) != "" &&
            isset($_POST['url']) && trim($_POST['url']) != "") {

            $usermodus = false;
            $uid = get_current_user_id();
            $selectui = '' . get_user_meta(wp_get_current_user()->ID, 'coach_select_user', true);
            if ($selectui != '') {
                $uid = $selectui;
                $usermodus = true;
            }

            if(!$usermodus) {

                $title = "Check „" . sanitize_text_field($_POST['checktitle']) . "“ gespeichert";
                $subtitle = "";
                $url = esc_url_raw($_POST['url']);

                // Params: $title, $subtitle, $userid, $postid (currently null until checks v2), $url, $type, $level, $date (= null)
                Trainingssystem_Plugin_Database::getInstance()->UserEvent->insertUserEvent($title, $subtitle, $uid, null, $url, "check", "detailed");
            }

        }
        wp_die();
    }

    /**
     * Getting all the formdata results for a specific ts form
     * note: first field is evaluated only!
     *
     * @param tsformid TS-Form ID
     * @param asc Boolean sort ascending or descending (default)
     *
     * @return empty array on failure | Array of Trainingssystem_Plugin_Database_Formdata-Objects
     */
    private function getCheckResults($tsformid, $asc = false, $limit = -1) {
        $uid = get_current_user_id();
        $selectui = '' . get_user_meta(wp_get_current_user()->ID, 'coach_select_user', true);
        if ($selectui != '') {
            $uid = $selectui;
        }

        $ret = [];

        $tsform_field = Trainingssystem_Plugin_Database::getInstance()->Formfield->getFormfieldsByForm($tsformid)[0] ?? null;
        if(!is_null($tsform_field)) {
            $formdata = Trainingssystem_Plugin_Database::getInstance()->Formdata->getAllFormfieldData($tsform_field->getFieldId(), $uid);

            foreach($formdata as $fd) {
                $ret[$fd->getSaveDate()] = $fd;
            }
        }

        if($asc) {
            ksort($ret);
        } else {
            krsort($ret);
        }

        if($limit != -1) {
            $ret = array_slice($ret, 0, $limit);
        }

        return $ret;
    }

    /**
	 * HELPER
	 *
	 * Generates a random color part
	 *
	 * @return 2 chars of [0-9a-f]
	 */
	private function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

	/**
	 * HELPER
	 *
	 * Generates a random hex-color #xxxxxx
	 *
	 * @param int alpha
	 *
	 * @return random color with leading #
	 */
    private function random_color($alpha) {
        return "#" . $this->random_color_part() . $this->random_color_part() . $this->random_color_part() . str_pad( dechex( $alpha ), 2, '0', STR_PAD_LEFT);
    }
}
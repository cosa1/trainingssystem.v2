<?php


class Trainingssystem_Plugin_Module_Interactive{


	public function __construct() {

	}

	/**
	 * display interactive the site.
	 *
	 * @since    1.0.0
	 */
	public function showDragDropGame($atts)
	{
	
	    if ( isset($atts['sections'])) {
				$twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
				ob_start();

				extract(shortcode_atts(array(
					'sections' => array(),
					'items1' => array(),
					'items2' => array(),
					'items3' => array(),
					'colors' => array()
				), $atts ));
				
				$Allitems1 = (is_array($items1)) ? array() : explode(", ", $items1);
				$Allitems2 = (is_array($items2)) ? array() : explode(", ", $items2);
				$Allitems3 = (is_array($items3)) ? array() : explode(", ", $items3);
				$Allsections = explode(", ", $sections);

				for ($i=0; $i< count($Allsections); $i++){
					$Allsections_noWhiteSpaces[$i] = str_replace(' ', '', $Allsections[$i]);

					$Allsections_array[$i] =array(
						"value" => $Allsections_noWhiteSpaces[$i],
						"heading" => $Allsections[$i]
					);
				}
				
				$random_number=mt_rand();

				for($i=0; $i<count($Allitems1); $i++){
					$temp_items1[$i]['html']= '<div class="ui-widget-content draggable'.$random_number.' d-none '.$Allsections_array[0]["value"].'-item ui-draggable ui-draggable-handle"><p>'.$Allitems1[$i].'</p></div>';
				}

				for($i=0; $i<count($Allitems2); $i++){
					$temp_items2[$i]['html']= '<div class="ui-widget-content draggable'.$random_number.' d-none '.$Allsections_array[1]["value"].'-item ui-draggable ui-draggable-handle"><p>'.$Allitems2[$i].'</p></div>';
				}
				
				if(!empty($Allitems3)){ 
					for($i=0; $i<count($Allitems3); $i++){
						$temp_items3[$i]['html']= '<div class="ui-widget-content draggable'.$random_number.' d-none '.$Allsections_array[2]["value"].'-item ui-draggable ui-draggable-handle"><p>'.$Allitems3[$i].'</p></div>';
					}
					$items=array_merge($temp_items1,$temp_items2,$temp_items3);	
				}else{
					$items=array_merge($temp_items1,$temp_items2);
				}

				shuffle($items);

				$numbItems=count($Allitems1)+count($Allitems2)+count($Allitems3);
				if($numbItems<=8){
					$areaheight='200';
				}if($numbItems>8){
					$areaheight='250';
				}if($numbItems>10){
					$areaheight='300';
				}

				$numbSections=count($Allsections);
				if($numbSections == 2){
					$sectionWith ='50';
					$Allitems3='';
				}else if($numbSections == 3){
					$sectionWith ='33.333333';
				}else{
					echo "Es müssen mindestens zwei und höchstens drei Sections angelegt werden.";
				}

				if(!isset($atts['colors'])){
					$Allcolors=['#dee5f3', '#bbc5d9', '#ced6e8'];
				}else{
					$Allcolors = explode(", ", $colors);
				}
			
	      	echo $twig->render('drag-drop.html',[
					"items" 	=> $items,
				  	"sections" => $Allsections_array,
				  	"colors" => $Allcolors,
				  	"sectionwith" => $sectionWith,
				  	"areaheight" => $areaheight,
					"rand"=>$random_number
				]
			);
	    }else{
	        echo "Es müssen mindestens zwei Bereiche mit sections='Name1, Name2' angelegt werden";
		}
		return ob_get_clean();
	}

}

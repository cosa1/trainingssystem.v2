<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 */
class Trainingssystem_Plugin_Module_Backend_Help_Top{


	public function __construct() {

	}

	function add_context_menu_help_comments(){
		
		$twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
		$current_screen = get_current_screen();
		
		//show only on comments page
		if($current_screen->post_type == 'Comments' && $current_screen->base == 'edit')
		{
		
		}
		$logourl = plugin_dir_url(dirname(__FILE__)) . '../../assets/img/logo2.png';
		$logohtml= "<img src='$logourl' style='    height: 30px;'>";
            
		$content = $logohtml.$twig->render('admin-backend/admin-backend-help.html',[]);;
		
		$current_screen->add_help_tab( array(
		'id' => TRAININGSSYSTEM_PLUGIN_SLUG.'_help_tab',
		'title' => "TrainOn",
		'content' => $content));
		
	}
	
}

<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Show_Hide
 * @subpackage Show_Hide/public
 */

/**
 * Reset radio button selection for several types. 
 *
 *
 * @package    Show_Hide
 * @subpackage Show_Hide/public
 * @author     Helge Nissen
 */
class Trainingssystem_Plugin_Module_Reset_Radios
{

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     */
    public function __construct()
    {

    }

    public function resetRadios($atts)
    {
        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
		ob_start();
        
        if(isset($atts['type']) && $atts['type'] == 'quizhn' || $atts['type'] == 'radiohn' || $atts['type'] == 'ts-form' ){
            if($atts['type']=='quizhn'){
                $value = $atts['class'];
            }

            if($atts['type']=='radiohn'){
                 $value = $atts['wert'];
            }

            if($atts['type']=='ts-form'){
                $value = $atts['ts-form-id'];
            }
 
            if(isset($atts['label'])){
                $button_label = $atts['label'];
            }
            else{
                $button_label = 'Auswahl aufheben';
            }

            echo $twig->render('show-hide/reset-radios.html',[	
                "type"=>$atts['type'],
                "value" => $value,
                "button_label" => $button_label,
                ]);
        }
        else{
            echo   '<p class="alert alert-danger"><i class="fas fa-exclamation-circle mr-2"></i><strong>Es muss ein gültiger Typ eingegeben werden</strong> <br>
                    Die Typen type=radiohn, type=quizhn oder type=ts-form können mit dieser Funktion zurückgesetzt werden.
                    Für den Typ ts-form wird der Parameter ts-form-id=123 übergeben. Für den Typ radiohn ist der der vergebene Wert mit wert=123 einzusetzen.
                    Für den Typ quizhn wird die vergebene Klasse mit class=123 übergeben.</p>';
        }
        
        return ob_get_clean();
    }

}

<?php

/**
 * Register all actions and filters for the plugin.
 *
 * @link       -
 * @since      1.0.0
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @author     Markus Domin, Helge Nissen <->
 */
class Trainingssystem_Plugin_Module_Frontend_Menu
{

    private $page_user_profil;
    private $page_user_progress;
    private $coach_nutzer;
    private $user_list;
    private $user_importer;
    private $create_user;
    private $dataexport;
    private $trainings_ex_import;
    private $coach_vorlage;
    private $user_grouper;
    private $user_code;
    private $system_statistics;
    private $coaching_overview;
    private $trainings_overview;
    private $mailbox_view;
    private $zertifikate;
    private $show_contactinfo;
    private $show_dashboard;
    private $study_overview;
    private $exercise_favorites;
    private $check_results;

    /**
     * Initialize the collections used to maintain the actions and filters.
     *
     * @since    1.0.0
     */
    public function __construct()
    {
    }

    /**
     * Initialize the collections used to maintain the actions and filters.
     *
     * @since    1.0.0
     */
    public function addMenuItems($items, $menu)
    {
        global $post;

        $currentPostId = !is_null($post) ? $post->ID : 0;

        if (!get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)) {

        } else {

            $settings = get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS);

            if(isset($settings["user_menu"])){

                $menuid = $settings["user_menu"];
                
                $nav_menu = get_term_by('id', $menuid, 'nav_menu');
                if (is_object($menu) && property_exists($menu, 'slug') && 
                    is_object($nav_menu) && property_exists($nav_menu, 'slug') && 
                        $menu->slug == $nav_menu->slug) {

                    $i = 1000;
                    // only add profile link if user is logged in
                    if (get_current_user_id()) {

                        $this->getItemNames();

                        $studygroup = get_user_meta( get_current_user_id(), "studiengruppenid", true );

                         // If Demo-User: Hide Profil, Fortschritt & Verwaltung
                         $canSave = get_user_meta(wp_get_current_user()->ID, 'can_save', true);
                         $demoUser = get_user_meta(wp_get_current_user()->ID, 'demo_user', true);
                         $current_user_is_admin = Trainingssystem_Plugin_Module_Berechtigung::getInstance()->userHasRole("admin");

                         if($canSave != 0 || $canSave == "" || $current_user_is_admin){

                            if($this->settingIssetAndNotEmpty($settings, "dashboard") && !isset($settings['hide_dashboard_menu']) && 
                                !$this->studygroupExcluded($studygroup, $settings, "page_dashboard_exclude_sg")) {
                                $posturl = get_permalink($settings["dashboard"]);
                                $items[] = $this->ts_nav_menu_item($i++, $this->show_dashboard, $posturl, 0, [], $currentPostId == $settings['dashboard']);
                            }

                            if(!isset($settings['hide_profile_menu'])) {
                                $profiltop = $this->ts_nav_menu_item($i++, "", "", 0, array("fas", "fa-user-alt"));
                                $items[] = $profiltop;

                                $profilentry = false;

                                if($this->settingIssetAndNotEmpty($settings, "page_user_profil") && !$this->studygroupExcluded($studygroup, $settings, "page_user_profile_exclude_sg") && $demoUser != "0" && $demoUser != "1") {
                                    $posturl = get_permalink($settings["page_user_profil"]);
                                    $items[] = $this->ts_nav_menu_item($i++, $this->page_user_profil, $posturl, $profiltop->ID, [], $currentPostId == $settings['page_user_profil']);
                                    $profilentry = true;
                                }

                                if($this->settingIssetAndNotEmpty($settings, "page_user_progress") && !$this->studygroupExcluded($studygroup, $settings, "page_user_progress_exclude_sg")) {
                                    $posturl = get_permalink($settings["page_user_progress"]);
                                    $items[] = $this->ts_nav_menu_item($i++, $this->page_user_progress, $posturl,$profiltop->ID, [], $currentPostId == $settings['page_user_progress']);
                                    $profilentry = true;
                                }

                                if($this->settingIssetAndNotEmpty($settings, "zertifikate") && !$this->studygroupExcluded($studygroup, $settings, "page_zertifikate_exclude_sg")) {
                                    $posturl = get_permalink($settings["zertifikate"]);
                                    $items[] = $this->ts_nav_menu_item($i++, $this->zertifikate, $posturl,$profiltop->ID, [], $currentPostId == $settings['zertifikate']);
                                    $profilentry = true;
                                }

                                if($this->settingIssetAndNotEmpty($settings, "exercise_favorites") && !$this->studygroupExcluded($studygroup, $settings, "page_exercise_favorites_exclude_sg")) {
                                    $posturl = get_permalink($settings["exercise_favorites"]);
                                    $items[] = $this->ts_nav_menu_item($i++, $this->exercise_favorites, $posturl,$profiltop->ID, [], $currentPostId == $settings['exercise_favorites']);
                                    $profilentry = true;
                                }
                              
                                if($this->settingIssetAndNotEmpty($settings, "check_results") && !$this->studygroupExcluded($studygroup, $settings, "page_check_results_exclude_sg")) {
                                    $posturl = get_permalink($settings["check_results"]);
                                    $items[] = $this->ts_nav_menu_item($i++, $this->check_results, $posturl,$profiltop->ID, [], $currentPostId == $settings['check_results']);
                                    $profilentry = true;
                                }

                                if(!$profilentry) {
                                    $profileid = null;
                                    foreach($items as $id => $item) {
                                        if($item->title == "") {
                                            $profileid = $id;
                                            break;
                                        }
                                    }
                                    if($profileid != null) {
                                        unset($items[$profileid]);
                                    }
                                }
                            }

                            if(!isset($settings['hide_config_menu'])) {
                                // VERWALTUNG START
                                $admintop = $this->ts_nav_menu_item($i++, 'Verwaltung', "");
                                $items[] = $admintop;

                                $adminemtry = false;

                                //Training Nutzer zuweisung
                                if($this->settingIssetAndNotEmpty($settings, "coach_nutzer") && !$this->studygroupExcluded($studygroup, $settings, "page_coach_nutzer_exclude_sg") &&
                                    Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("trainingmgr")) {
                                    $posturl = get_permalink($settings["coach_nutzer"]);
                                    $items[] = $this->ts_nav_menu_item($i++, $this->coach_nutzer, $posturl, $admintop->ID, [], $currentPostId == $settings['coach_nutzer']);
                                    $adminemtry = true;
                                }

                                //Nutzerliste
                                if($this->settingIssetAndNotEmpty($settings, "user_list") && !$this->studygroupExcluded($studygroup, $settings, "page_user_list_exclude_sg") &&
                                    Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("userlist")) {
                                    $posturl = get_permalink($settings["user_list"]);
                                    $items[] = $this->ts_nav_menu_item($i++, $this->user_list , $posturl, $admintop->ID, [], $currentPostId == $settings['user_list']);
                                    $adminemtry = true;
                                }

                                // Nutzerimport
                                if($this->settingIssetAndNotEmpty($settings, "user_importer") && !$this->studygroupExcluded($studygroup, $settings, "page_user_importer_exclude_sg") &&
                                    Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("nutzerimport")) {
                                    $posturl = get_permalink($settings["user_importer"]);
                                    $items[] = $this->ts_nav_menu_item($i++, $this->user_importer, $posturl, $admintop->ID, [], $currentPostId == $settings['user_importer']);
                                    $adminemtry = true;
                                }

                                // Nutzer anlegen
                                if($this->settingIssetAndNotEmpty($settings, "create_user") && !$this->studygroupExcluded($studygroup, $settings, "page_create_user_exclude_sg") &&
                                    Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("nutzernew")) {
                                    $posturl = get_permalink($settings["create_user"]);
                                    $items[] = $this->ts_nav_menu_item($i++, $this->create_user, $posturl, $admintop->ID, [], $currentPostId == $settings['create_user']);
                                    $adminemtry = true;
                                }

                                // Daten exportieren
                                if($this->settingIssetAndNotEmpty($settings, "dataexport") && !$this->studygroupExcluded($studygroup, $settings, "page_dataexport_exclude_sg") &&
                                    Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("dataexport")) {
                                    $posturl = get_permalink($settings["dataexport"]);
                                    $items[] = $this->ts_nav_menu_item($i++,  $this->dataexport, $posturl, $admintop->ID, [], $currentPostId == $settings['dataexport']);
                                    $adminemtry = true;
                                }

                                // Training im/exportieren
                                if($this->settingIssetAndNotEmpty($settings, "trainings_ex_import") && !$this->studygroupExcluded($studygroup, $settings, "page_trainings_ex_import_exclude_sg") &&
                                    Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("trainingseximport")) {
                                    $posturl = get_permalink($settings["trainings_ex_import"]);
                                    $items[] = $this->ts_nav_menu_item($i++, $this->trainings_ex_import, $posturl, $admintop->ID, [], $currentPostId == $settings['trainings_ex_import']);
                                    $adminemtry = true;
                                }

                                //Vorlagen Nutzer
                                if($this->settingIssetAndNotEmpty($settings, "coach_vorlage") && !$this->studygroupExcluded($studygroup, $settings, "page_coach_vorlage_exclude_sg") &&
                                    Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("vorlagen")) {
                                    $posturl = get_permalink($settings["coach_vorlage"]);
                                    $items[] = $this->ts_nav_menu_item($i++, $this->coach_vorlage, $posturl, $admintop->ID, [], $currentPostId == $settings['coach_vorlage']);
                                    $adminemtry = true;
                                }

                                //Firmen Nutzer zuweisung
                                if($this->settingIssetAndNotEmpty($settings, "user_grouper") && !$this->studygroupExcluded($studygroup, $settings, "page_user_grouper_exclude_sg") &&
                                    Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("companymgr")) {
                                    $posturl = get_permalink($settings["user_grouper"]);
                                    $items[] = $this->ts_nav_menu_item($i++, $this->user_grouper, $posturl, $admintop->ID, [], $currentPostId == $settings['user_grouper']);
                                    $adminemtry = true;
                                }

                                // Freischaltcode
                                if($this->settingIssetAndNotEmpty($settings, "user_code") && !$this->studygroupExcluded($studygroup, $settings, "page_user_code_exclude_sg") &&
                                    Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("registerkey")) {
                                    $posturl = get_permalink($settings["user_code"]);
                                    $items[] = $this->ts_nav_menu_item($i++, $this->user_code, $posturl, $admintop->ID, [], $currentPostId == $settings['user_code']);
                                    $adminemtry = true;
                                }

                                // Systemstatistiken
                                if($this->settingIssetAndNotEmpty($settings, "system_statistics") && !$this->studygroupExcluded($studygroup, $settings, "page_system_statistics_exclude_sg") &&
                                    Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("systemstatistiken")) {
                                    $posturl = get_permalink($settings["system_statistics"]);
                                    $items[] = $this->ts_nav_menu_item($i++, $this->system_statistics, $posturl, $admintop->ID, [], $currentPostId == $settings['system_statistics']);
                                    $adminemtry = true;
                                }
                                
                                // Coaching-Overview
                                if($this->settingIssetAndNotEmpty($settings, "coaching_overview") && !$this->studygroupExcluded($studygroup, $settings, "page_coaching_overview_exclude_sg") &&
                                    (Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("coachingoverview") || 
                                    Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("coachingoverviewsupervisor"))) {
                                    $posturl = get_permalink($settings["coaching_overview"]);
                                    $items[] = $this->ts_nav_menu_item($i++, $this->coaching_overview, $posturl, $admintop->ID);
                                    $adminemtry = true;
                                }

                                // Study-Overview
                                if($this->settingIssetAndNotEmpty($settings, "study_overview") && !$this->studygroupExcluded($studygroup, $settings, "page_study_overview_exclude_sg") &&
                                    (Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("studyoverview"))) {
                                    $posturl = get_permalink($settings["study_overview"]);
                                    $items[] = $this->ts_nav_menu_item($i++, $this->study_overview, $posturl, $admintop->ID);
                                    $adminemtry = true;
                                }
                                // VERWALTUNG ENDE

                                if(!$adminemtry) {
                                    $adminid = null;
                                    foreach($items as $id => $item) {
                                        if($item->title == "Verwaltung") {
                                            $adminid = $id;
                                            break;
                                        }
                                    }
                                    if($adminid != null) {
                                        unset($items[$adminid]);
                                    }
                                }
                            }

                            if($this->settingIssetAndNotEmpty($settings, "trainings_overview") && !isset($settings['hide_trainingoverview_menu']) && 
                                !$this->studygroupExcluded($studygroup, $settings, "page_training_overview_exclude_sg")) {
                                $posturl = get_permalink($settings["trainings_overview"]);
                                $items[] = $this->ts_nav_menu_item($i++, $this->trainings_overview, $posturl, 0, [], $currentPostId == $settings['trainings_overview']);
                            }

                            if((Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("nachrichtennew") || 
                                !empty(get_user_meta( get_current_user_id(), 'coachid' , true ))) && !$this->studygroupExcluded($studygroup, $settings, "page_mailbox_view_exclude_sg")) {

                                if($this->settingIssetAndNotEmpty($settings, "mailbox_view") && !isset($settings['hide_message_menu'])) {
                                    $posturl = get_permalink($settings["mailbox_view"]);
                                    $items[] = $this->ts_nav_menu_item($i++, $this->mailbox_view, $posturl, 0, [], $currentPostId == $settings['mailbox_view']);
                                }
                            }

                            if($this->settingIssetAndNotEmpty($settings, "show_contactinfo") && !isset($settings['hide_show_contactinfo_menu']) && 
                                !$this->studygroupExcluded($studygroup, $settings, "page_show_contactinfo_exclude_sg")) {
                                $posturl = get_permalink($settings["show_contactinfo"]);
                                $items[] = $this->ts_nav_menu_item($i++, $this->show_contactinfo, $posturl, 0, [], $currentPostId == $settings['show_contactinfo']);
                            }
                        }
                        $items[] = $this->ts_nav_menu_item($i++, 'Abmelden', wp_logout_url( home_url() ));
                    
                        if (!empty(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)['privacy_terms_URL'])){
                            $current_user = wp_get_current_user();   
                            $privacy_confirmed = get_user_meta($current_user->ID, 'privacy_terms_confirmed',true);
                            if($privacy_confirmed != 0 && $privacy_confirmed != ""){
                                return $items;
                            }
                            else{           
                                $profil_url = get_permalink($settings["page_user_profil"]);
                                if($profil_url != get_permalink()){
                                    $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
                                    echo $twig->render('error-terms-confirm.html', 
                                        [
                                            'profile_url' => $profil_url,
                                            'modalid' => uniqid(),
                                            'website_name' => get_bloginfo(),
                                        ]);
                                    die();
                                }
                                $logout_only[] = $this->ts_nav_menu_item(0, 'Abmelden', wp_logout_url( home_url() ));
                                return $logout_only;
                            }
                        }
                    }else{
                        if($this->settingIssetAndNotEmpty($settings, "show_contactinfo") && !isset($settings['hide_show_contactinfo_menu'])) {
                            $posturl = get_permalink($settings["show_contactinfo"]);
                            $items[] = $this->ts_nav_menu_item($i++, "Hilfe & Kontakt", $posturl, 0, [], $currentPostId == $settings['show_contactinfo']);
                        }

                        if(isset($settings["open_register_main_menu"])){

                            if($this->settingIssetAndNotEmpty($settings, "open_register_code")){
                                $registerCode = Trainingssystem_Plugin_Database::getInstance()->RegisterKey->getCodeDetails($settings["open_register_code"]);
                                if($registerCode->getCountCode() < $registerCode->getMaxCount()){
                                    $items[] = $this->ts_nav_menu_item($i++, 'Registrieren', add_query_arg("code", $settings["open_register_code"], wp_registration_url()), 0, [], $currentPostId == $settings['open_register_code']);
                                }
                            }
                            else if(isset($settings["register_redeem_code_manually"])){
                                $items[] = $this->ts_nav_menu_item($i++, 'Registrieren', wp_registration_url());
                            }

                            
                        }
                        $items[] = $this->ts_nav_menu_item($i++, 'Anmelden', wp_login_url( home_url() ));
                    }

                }
            }

            if(isset($settings["avatar_menu"])&&$settings["avatar_menu"] != ""&&$settings["avatar_menu"] != "-1"&&$settings["avatar_menu"] != "0"){
                $menuid = $settings["avatar_menu"];
                $menuname = get_term_by('id', $menuid, 'nav_menu')->slug;
                if ($menu->slug == $menuname) {

                    $i = 30;
                    // only add profile link if user is logged in
                    if (get_current_user_id()) {
                        $trainingssystem_Plugin_Module_Avatar = new Trainingssystem_Plugin_Module_Userprofil();
                        $items[] = $this->ts_nav_menu_item($i++, $trainingssystem_Plugin_Module_Avatar->menu() ,  "");//"<div>".get_avatar( get_current_user_id(), 32 )."</div>"
                    }

                }
            }
        }  
            return $items;

    }

    function getItemNames(){
        $settings = get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS);

        $this->page_user_profil = $this->settingIssetAndNotEmpty($settings, "page_user_profil_name") ? $settings['page_user_profil_name'] : 'Profil';
        $this->page_user_progress = $this->settingIssetAndNotEmpty($settings, "page_user_progress_name") ? $settings['page_user_progress_name'] : 'Abzeichen';
        $this->coach_nutzer = $this->settingIssetAndNotEmpty($settings, "coach_nutzer_name") ? $settings['coach_nutzer_name'] : 'Trainings zuweisen';
        $this->user_list = $this->settingIssetAndNotEmpty($settings, "user_list_name") ? $settings['user_list_name'] : 'Nutzerliste';
        $this->user_importer = $this->settingIssetAndNotEmpty($settings, "user_importer_name") ? $settings['user_importer_name'] : 'Nutzer-Import';
        $this->create_user = $this->settingIssetAndNotEmpty($settings, "create_user_name") ? $settings['create_user_name'] : 'Nutzer anlegen';
        $this->dataexport = $this->settingIssetAndNotEmpty($settings, "dataexport_name") ? $settings['dataexport_name'] : 'Datenexport';
        $this->trainings_ex_import = $this->settingIssetAndNotEmpty($settings, "trainings_ex_import_name") ? $settings['trainings_ex_import_name'] : 'Trainings Ex-/Import';
        $this->coach_vorlage = $this->settingIssetAndNotEmpty($settings, "coach_vorlage_name") ? $settings['coach_vorlage_name'] : 'Trainingsvorlagen';
        $this->user_grouper = $this->settingIssetAndNotEmpty($settings, "user_grouper_name") ? $settings['user_grouper_name'] : 'Firmen';
        $this->user_code = $this->settingIssetAndNotEmpty($settings, "user_code_name") ? $settings['user_code_name'] : 'Freischaltecode';
        $this->system_statistics = $this->settingIssetAndNotEmpty($settings, "system_statistics_name") ? $settings['system_statistics_name'] : 'Systemstatistiken';
        $this->coaching_overview = $this->settingIssetAndNotEmpty($settings, "coaching_overview_name") ? $settings['coaching_overview_name'] : 'Coaching-Übersicht';
        $this->trainings_overview = $this->settingIssetAndNotEmpty($settings, "trainings_overview_name") ? $settings['trainings_overview_name'] : 'Trainings';
        $this->mailbox_view = $this->settingIssetAndNotEmpty($settings, "mailbox_view_name") ? $settings['mailbox_view_name'] : 'Nachrichten';
        $this->zertifikate = $this->settingIssetAndNotEmpty($settings, "zertifikate_name") ? $settings['zertifikate_name'] : 'Zertifikate';
        $this->show_contactinfo = $this->settingIssetAndNotEmpty($settings, "show_contactinfo_name") ? $settings['show_contactinfo_name'] : 'Hilfe/Kontakt';
        $this->show_dashboard = $this->settingIssetAndNotEmpty($settings, "show_dashboard_name") ? $settings['show_dashboard_name'] : 'Dashboard';
        $this->study_overview = $this->settingIssetAndNotEmpty($settings, "study_overview_name") ? $settings['study_overview_name'] : 'Studienübersicht';
        $this->exercise_favorites = $this->settingIssetAndNotEmpty($settings, "exercise_favorites_name") ? $settings['exercise_favorites_name'] : 'Favoriten';
        $this->check_results = $this->settingIssetAndNotEmpty($settings, "check_results_name") ? $settings['check_results_name'] : 'Check-Ergebnisse';

    }
    /**
     * Checks wheter the setting name exists in the settings array and if its value is not empty
     * 
     * @param settings Array 
     * @param name String
     * 
     * @return true/false
     */
    private function settingIssetAndNotEmpty($settings, $name) {
        if(isset($settings[$name]) && !is_array($settings[$name]) && trim($settings[$name]) != "") {
            return true;
        }

        if(isset($settings[$name]) && is_array($settings[$name]) && count($settings[$name]) > 0) {
            return true;
        }

        return false;
    }

    /**
     * Checks if a user belongs to the excluded studygroup for a page
     * 
     * @param userstudygroup The User's studygroup, empty string if he does not have one
     * @param settings Array of settings
     * @param excludeSettingName Name of Setting the excluded studygroups are stored in
     * 
     * @return bool true if user is excluded, false if he is not excluded
     */
    private function studygroupExcluded($userstudygroup, $settings, $excludeSettingName) {
        if($userstudygroup == "") {
            return false; 
        }

        if(!$this->settingIssetAndNotEmpty($settings, $excludeSettingName)) {
            return false;
        }

        foreach($settings[$excludeSettingName] as $ex_sg) {
            if($ex_sg == $userstudygroup) {
                return true;
            }
        }

        return false;
    }

    public function ts_nav_menu_item($menuorder, $title, $url, $parentitem = 0, $classes = array(), $isCurrentPage = false)
    {
        $item = new stdClass();
        $item->ID = 99999999 + $menuorder + $parentitem;
        $item->db_id = $item->ID;
        $item->title = $title;
        $item->url = $url;
        $item->menu_order = $menuorder;
        $item->menu_item_parent = $parentitem;
        $item->type = '';
        $item->object = '';
        $item->object_id = '';
        if($isCurrentPage) {
            $classes[] = "menu-item-text-underlined";
        }
        $item->classes = $classes;
        $item->target = '';
        $item->attr_title = '';
        $item->description = '';
        $item->xfn = '';
        $item->status = '';
        return $item; 
    }






    function menueditor_register_menu_metabox() {
        $custom_param = array( 0 => 'This param will be passed to menu_metabox' );

        add_meta_box( TRAININGSSYSTEM_PLUGIN_SLUG.'-menueditor-metabox', 'Trainingssystem Plugin v2', [$this,'menueditor_render_menu_metabox'], 'nav-menus', 'side', 'default', $custom_param );
    }

    function createmetaboxitem($name,$link){
        return (object) array(
            'ID' => 1,
            'db_id' => 0,
            'menu_item_parent' => 0,
            'object_id' => 1,
            'post_parent' => 0,
            'type' => 'custom',//TRAININGSSYSTEM_PLUGIN_SLUG.'-custom-type',
            'object' => 'custom',//TRAININGSSYSTEM_PLUGIN_SLUG.'-object-slug',
            'type_label' => TRAININGSSYSTEM_PLUGIN_SLUG,
            'title' => $name,
            'url' => $link,//home_url( '/'.$link.'/' ),
            'target' => '',
            'attr_title' => '',
            'description' => '',
            'classes' => array(),
            'xfn' => '',
        );
    }
    /**
     * Displays a menu metabox
     *
     * @param string $object Not used.
     * @param array $args Parameters and arguments. If you passed custom params to add_meta_box(),
     * they will be in $args['args']
     */
    function menueditor_render_menu_metabox( $object, $args ) {
        global $nav_menu_selected_id;

        $my_items = [];


        if (!get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)) {

        } else {
            $this->getItemNames();
            $settings = get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS);
            $my_items[] = $this->createmetaboxitem($this->page_user_progress,get_permalink($settings["page_user_progress"]));
            $my_items[] = $this->createmetaboxitem($this->page_user_profil,get_permalink($settings["page_user_profil"]));
            $my_items[] = $this->createmetaboxitem($this->coach_nutzer,get_permalink($settings["coach_nutzer"]));
            $my_items[] = $this->createmetaboxitem($this->user_list,get_permalink($settings["user_list"]));
            $my_items[] = $this->createmetaboxitem($this->user_importer,get_permalink($settings["user_importer"]));
            $my_items[] = $this->createmetaboxitem($this->create_user,get_permalink($settings["create_user"]));
            $my_items[] = $this->createmetaboxitem($this->coach_vorlage,get_permalink($settings["coach_vorlage"]));
            $my_items[] = $this->createmetaboxitem($this->user_grouper,get_permalink($settings["user_grouper"]));
            $my_items[] = $this->createmetaboxitem($this->user_code,get_permalink($settings["user_code"]));
            $my_items[] = $this->createmetaboxitem($this->system_statistics,get_permalink($settings["system_statistics"]));
			$my_items[] = $this->createmetaboxitem($this->coaching_overview,get_permalink($settings["coaching_overview"]));
            $my_items[] = $this->createmetaboxitem($this->zertifikate,get_permalink($settings["zertifikate"]));
            $my_items[] = $this->createmetaboxitem($this->show_contactinfo,get_permalink($settings["show_contactinfo"]));
            $my_items[] = $this->createmetaboxitem($this->show_dashboard,get_permalink($settings["dashboard"]));
            $my_items[] = $this->createmetaboxitem($this->study_overview,get_permalink($settings["study_overview"]));
            $my_items[] = $this->createmetaboxitem($this->exercise_favorites,get_permalink($settings["exercise_favorites"]));
            $my_items[] = $this->createmetaboxitem($this->check_results,get_permalink($settings["check_results"]));


            $walker = new Walker_Nav_Menu_Checklist(false);

            $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
            echo $twig->render("admin-backend-menu/admin-backend-menu-panel.html", [
                                                            "slug" => TRAININGSSYSTEM_PLUGIN_SLUG, 
                                                            "walker" => walk_nav_menu_tree( array_map( 'wp_setup_nav_menu_item', $my_items ), 0, (object) array( 'walker' => $walker)),
                                                            "disable_check" => wp_nav_menu_disabled_check( $nav_menu_selected_id ),
                                                        ]);
        }
    }

    /**
     * BACKEND 
     * 
     * Function taht adds a hide-if-not-loggedin-checkbox to each menu item in the backend's menu designer
     * 
     * @param Int Item-ID
     * @param Object Item
     */
    function adminBackendMenuItemCustomEvents($item_id, $item) {
        $checked = "";
        $checked_meta = get_post_meta($item_id, "tspv2_menuitem_loggedin");
        if(!empty($checked_meta) && isset($checked_meta[0])) {
            $checked = checked($checked_meta[0], true, false);
        }

        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
        echo $twig->render("admin-backend-menu/admin-backend-menu-item-loggedin.html", ["itemid" => $item_id, "checked" => $checked]);

        $checked = "";
        $checked_meta = get_post_meta($item_id, "tspv2_menuitem_demouser");
        if(!empty($checked_meta) && isset($checked_meta[0])) {
            $checked = checked($checked_meta[0], true, false);
        }
        echo $twig->render("admin-backend-menu/admin-backend-menu-item-demouser.html", ["itemid" => $item_id, "checked" => $checked]);

        $studiengruppenid_meta = get_users(array("meta_key" => 'studiengruppenid'));
        $sg_ids = [];
        foreach($studiengruppenid_meta as $sg_meta) {
            if(trim($sg_meta->studiengruppenid) != "") {
                $sg_ids[$sg_meta->studiengruppenid] = $sg_meta->studiengruppenid;
            }
        }

        $sg_checked_meta = get_post_meta($item_id, "tspv2_menuitem_sgid");
        $sg_checked = [];
        if(!empty($sg_checked_meta) && isset($sg_checked_meta[0])) {
            $sg_checked = $sg_checked_meta[0];
        }

        echo $twig->render("admin-backend-menu/admin-backend-menu-item-studiengruppenids.html", ["itemid" => $item_id, "studiengruppen" => $sg_ids, "checked" => $sg_checked]);
    }

    /**
     * BACKEND
     * 
     * Funciton called when a mneu item is saved. Evaluates the hide-if-not-loggedin-checkbox
     * 
     * @param Menu-ID - not used
     * @param Int ID of menu Item
     */
    function adminBackendMenuItemsSave($menu_id, $menu_item_db_id) {
        $button_value = (isset($_POST['tspv2-menu-item-button-loggedin'][$menu_item_db_id]) && $_POST['tspv2-menu-item-button-loggedin'][$menu_item_db_id] == 'on') ? true : false;
	    update_post_meta($menu_item_db_id, 'tspv2_menuitem_loggedin', $button_value);

        $button_value = (isset($_POST['tspv2-menu-item-button-demouser'][$menu_item_db_id]) && $_POST['tspv2-menu-item-button-demouser'][$menu_item_db_id] == 'on') ? true : false;
	    update_post_meta($menu_item_db_id, 'tspv2_menuitem_demouser', $button_value);

        $selected_value = (isset($_POST['tspv2-menu-item-button-sgid'][$menu_item_db_id])) ? $_POST['tspv2-menu-item-button-sgid'][$menu_item_db_id] : [];
        foreach($selected_value as $sv_key => $sv) {
            if(trim($sv_key) == "") {
                unset($selected_value[$sv_key]);
            }
        }
        update_post_meta($menu_item_db_id, 'tspv2_menuitem_sgid', $selected_value);
    }

    /**
     * FRONTEND
     * 
     * Function to hide a menu item if option in backend is set and user is not logged in/not in required study group
     * 
     * @param Array items
     * @param Menu - not used
     * 
     * @return Array maybe filtered list of menu items
     */
    function adminBackendMenuItemsExclude($items, $menu) {
        $newitems = [];
        foreach($items as $item) {
            $hide = false;
            $checked_meta = get_post_meta($item->ID, "tspv2_menuitem_loggedin");
            if(!empty($checked_meta) && isset($checked_meta[0])) {
                $hide = checked($checked_meta[0], true, false);
            }

            $hide_demouser = false;
            $checked_meta = get_post_meta($item->ID, "tspv2_menuitem_demouser");
            if(!empty($checked_meta) && isset($checked_meta[0])) {
                $hide_demouser = checked($checked_meta[0], true, false);
            }

            $sg_checked_meta = get_post_meta($item->ID, "tspv2_menuitem_sgid");
            $sg_checked = [];
            if(!empty($sg_checked_meta) && isset($sg_checked_meta[0])) {
                $sg_checked = $sg_checked_meta[0];
            }

            $sgid = "";
            $user = wp_get_current_user();
            if(is_object($user)) {
                $sgid = $user->studiengruppenid;
            }

            $demoUser = get_user_meta(wp_get_current_user()->ID, 'demo_user', true);

            if(($hide && !get_current_user_id()) || 
                ($hide_demouser && ($demoUser == "0" || $demoUser == "1") ) ||
                (get_current_user_id() > 0 && in_array($sgid, $sg_checked))) {
            } else {
                $newitems[] = $item;
            }
        }
        return $newitems;
    }
}

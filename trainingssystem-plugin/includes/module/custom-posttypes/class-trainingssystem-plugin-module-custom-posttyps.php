<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 */
class Trainingssystem_Plugin_Module_Custom_Posttyps{


	public function __construct() {

	}
	 /**
 	 * Register custom posttype.
 	 *
 	 * @since    1.0.0
 	 */
 	public static function register_my_post_type($name, $icon, $supports, $all, $new, $edit, $view, $taxonomies)
 	{
 			$labels = array(
						'name' => $all,
						'singular_name' => ''.$name,
						'menu_name' => 'Coach_'.$name,
						'parent_item_colon' => '',
						'all_items' => $all,
						'view_item' => $view,
						'add_new_item' => $new,
						'add_new' => $new,
						'edit_item' => $edit,
						'update_item' => $edit,
						'search_items' => $name . ' durchsuchen',
						'not_found' => '',
						'not_found_in_trash' => '',
 						);
 			$rewrite = array(
						'slug' => ''.strtolower($name),
						'with_front' => true,
						'pages' => true,
						'feeds' => true,
					 	);
					 
 			$args = array(
						'labels' => $labels,
						'supports' => $supports,
						'taxonomies' => $taxonomies, //kategorien und schlagwörter
						'hierarchical' => false,
						'public' => true,
						'show_ui' => true,
						'show_in_menu'       => false,
						'show_in_nav_menus' => true,
						'show_in_admin_bar' => true,
						'menu_position' => 5,
						'can_export' => false,
						'has_archive' => true,
						'exclude_from_search' => false,
						'publicly_queryable' => true,
						'rewrite' => $rewrite,
						'capability_type' => 'page',
						'menu_icon' => ''.$icon,
						// 'show_in_rest'       => true,
					 	);
			
					 
			if (!get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)) {

			} else {
				$settings = get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS);

				if(isset($settings["gutenbergeditor"]) && $settings["gutenbergeditor"]){
					$args = array(
						'labels' => $labels,
                        'supports' => $supports,
						'taxonomies' => $taxonomies, //kategorien und schlagwörter
						'hierarchical' => false,
						'public' => true,
						'show_ui' => true,
						'show_in_menu' => false,
						'show_in_nav_menus' => true,
						'show_in_admin_bar' => true,
						'menu_position' => 5,
						'can_export' => false,
						'has_archive' => true,
						'exclude_from_search' => false,
						'publicly_queryable' => true,
						'rewrite' => $rewrite,
						'capability_type' => 'page',
						'menu_icon' => ''.$icon,
						'show_in_rest' => true,
					);
				}
			}

 			register_post_type(''.strtolower($name), $args);
			remove_post_type_support(''.strtolower($name), 'custom-fields' );
 	}

	/**
	 * Register custom posttype.
	 *
	 * @since    1.0.0
	 */
	public function add_custom_post_types()
	{		
			$categories = array('category', 'post_tag');
			$nocategories = array('');
            
			Trainingssystem_Plugin_Module_Custom_Posttyps::register_my_post_type('Trainings', 'dashicons-star-empty', array('title', 'editor', 'excerpt', 'thumbnail', 'comments','custom-fields','revisions'), "Alle Trainings", "Neues Training", "Training bearbeiten", "Training ansehen", $categories);
			Trainingssystem_Plugin_Module_Custom_Posttyps::register_my_post_type('Lektionen', 'dashicons-star-empty', array('title', 'editor', 'excerpt', 'thumbnail', 'comments','custom-fields','revisions'), "Alle Lektionen", "Neue Lektion", "Lektion bearbeiten", "Lektion ansehen", $nocategories);
			Trainingssystem_Plugin_Module_Custom_Posttyps::register_my_post_type('Seiten', 'dashicons-star-empty', array('title', 'editor', 'excerpt', 'thumbnail', 'comments','custom-fields','revisions'), "Alle Seiten", "Neue Seite", "Seite bearbeiten", "Seite ansehen", $nocategories);
			Trainingssystem_Plugin_Module_Custom_Posttyps::register_my_post_type('Formulare', 'dashicons-star-empty', array('title'), "Alle Formulare", "Neues Formular", "Formular bearbeiten", "Formular ansehen", $nocategories);
			Trainingssystem_Plugin_Module_Custom_Posttyps::register_my_post_type('Abzeichen', 'dashicons-star-empty', array('title', 'editor', 'excerpt', 'thumbnail', 'comments','custom-fields','revisions'), "Alle Abzeichen", "Neues Abzeichen", "Abzeichen bearbeiten", "Abzeichen ansehen", $nocategories);
			Trainingssystem_Plugin_Module_Custom_Posttyps::register_my_post_type('Zertifikate', 'dashicons-star-empty', array('title', 'editor', 'excerpt', 'thumbnail', 'comments','custom-fields','revisions'), "Alle Zertifikate", "Neues Zertifikat", "Zertifikat bearbeiten", "Zertifikat ansehen", $nocategories);
			Trainingssystem_Plugin_Module_Custom_Posttyps::register_my_post_type('Uebungen', 'dashicons-star-empty', array('title', 'editor', 'revisions'), "Alle Übungen", "Neue Übung", "Übung bearbeiten", "Übung ansehen", $categories);
			Trainingssystem_Plugin_Module_Custom_Posttyps::register_my_post_type('Avatare', 'dashicons-star-empty', array('title'), "Alle Avatare", "Neuer Avatar", "Avatar bearbeiten", "Avatar ansehen", $nocategories);

			Trainingssystem_Plugin_Module_Custom_Posttyps::register_my_post_type('Nachrichten', 'dashicons-star-empty', array('title', 'editor', 'excerpt', 'thumbnail', 'comments','custom-fields','revisions'), "Alle Nachrichten", "Neue Nachricht", "Nachricht bearbeiten", "Nachricht ansehen", $nocategories);

			Trainingssystem_Plugin_Module_Custom_Posttyps::remove_text_metabox();
	}

	function force_post_title_init() 
	{
		wp_enqueue_script('jquery');
	}

	function force_post_title( $post ) 
	{
		$post_type = strtolower(get_post_type());
		$force_title_for_post_types = array("trainings, lektionen", "seiten", "abzeichen", "zertifikate", "nachrichten", "formulare");
		$validation_message = "<b>Fehler!</b><br>Es muss ein Titel vergeben werden.";

		if(in_array($post_type, $force_title_for_post_types, true)) {
			echo "<script type='text/javascript'>\n";
			echo "
				jQuery( document ).ready(function() {

					jQuery('#title').on('keypress', function(e) {
						if(e.which =='13') {
							return testForEmptyTitle();
						}
					});
					jQuery('#publish').click(function(){
						return testForEmptyTitle();
					});

					function testForEmptyTitle() {
						var testervar = jQuery('[id^=\"titlediv\"]').find('#title');
						if (testervar.val().length < 1)
						{
							setTimeout(\"jQuery('#ajax-loading').css('visibility', 'hidden');\", 100);
							if(jQuery('.no-title-warning').length == 0) {
								var validator_snippet = '<div class=\"wrapper-admin-backend\"><div class=\"mt-3 no-title-warning alert alert-danger alert-dismissible fade show\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" . $validation_message . "</div></div>';
								jQuery('[id^=\"titlediv\"]').find('#titlewrap').append(validator_snippet);          
							}
							setTimeout(\"jQuery('#publish').removeClass('button-primary-disabled');\", 100);
							return false;
						} else {
							return true;
						}
					}
				});\n";
			echo "</script>\n";
		}
	}

	public static function remove_text_metabox() {

		$nametemp='abzeichen';
		remove_meta_box('postexcerpt', $nametemp, 'normal');
		remove_post_type_support($nametemp, 'editor' );
		remove_post_type_support( $nametemp, 'custom-fields' );
		remove_post_type_support( $nametemp, 'comments' );

		$nametemp='zertifikate';
		remove_meta_box('postexcerpt', $nametemp, 'normal');
		remove_post_type_support($nametemp, 'editor' );
		remove_post_type_support( $nametemp, 'custom-fields' );
		remove_post_type_support( $nametemp, 'comments' );
		remove_post_type_support( $nametemp, 'thumbnail' );
	}
}

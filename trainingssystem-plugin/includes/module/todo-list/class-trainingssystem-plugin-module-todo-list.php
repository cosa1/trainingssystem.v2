<?php

class Trainingssystem_Plugin_Module_Todo_List {

    private const POST_TYPE_NAME = 'mytodo';
    private const POST_TYPE_NAME_SYSTEM = 'systemtodo';

    //Input with button to save users Todo
    public function todoList($atts) {
        $title = "Todo Liste";
        if(isset($atts['titel'])) { // Allow empty title here!
            $title = $atts['titel'];
        }

        $systemlisttitle = "System Liste";
        if(isset($atts['systemlisttitle'])) { // Allow empty title here!
            $systemlisttitle = $atts['systemlisttitle'];
        }

        $mylisttitle = "Meine Liste";
        if(isset($atts['mylisttitle'])) { // Allow empty title here!
            $mylisttitle = $atts['mylisttitle'];
        }

        $mymode = 1;
        if(isset($atts['mymode']) && is_numeric($atts['mymode']) && $atts['mymode'] == 0) {
            $mymode = $atts['mymode'];
        }

         $systemmode = 1;
         if(isset($atts['systemmode']) && is_numeric($atts['systemmode']) && $atts['systemmode'] == 0) {
                $systemmode = $atts['systemmode'];
         }

         $linethrough = 1;
         if(isset($atts['linethrough']) && is_numeric($atts['linethrough'])) {
            $linethrough = $atts['linethrough'];
        }

        $myposts = [];
        $systemposts = [];

         if($mymode){
            $myposts = get_posts(
                [
                    'author' => get_current_user_id(),
                    'post_type' => self::POST_TYPE_NAME,
                    'numberposts' => -1,
                ]
            );
         }

         if($systemmode){
            $systemposts = get_posts(
                [
                    'author' => get_current_user_id(),
                    'post_type' => self::POST_TYPE_NAME_SYSTEM,
                    'numberposts' => -1,
                ]
            );
         }

        $myTodos = [];
        $myTodosIDs = [];
        $myTodosStatus = [];
        $systemTodos = [];
        $systemTodosIDs = [];
        $systemTodosStatus = [];

        if($mymode){
            foreach ($myposts as $post) {
                array_push($myTodos, $post->post_title);
                array_push($myTodosStatus, $post->post_content);
                array_push($myTodosIDs, $post->ID);
    
            }
        }

        if($systemmode){
            foreach ($systemposts as $post) {
                array_push($systemTodos, $post->post_title);
                array_push($systemTodosStatus, $post->post_content);
                array_push($systemTodosIDs, $post->ID);
    
            }
        }

        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
        
            return $twig->render("todo-list/todo-list.html", [
                                                                    "systemTodos" => $systemTodos,
                                                                    "myTodos" => $myTodos,
                                                                    "title" => $title,
                                                                    "systemlisttitle" => $systemlisttitle,
                                                                    "mylisttitle" => $mylisttitle,
                                                                    "myposts" => $myposts,
                                                                    "myTodosIDs" => $myTodosIDs,
                                                                    "myTodosStatus" => $myTodosStatus,
                                                                    "linethrough" => $linethrough,

                                                                    "systemTodos" => $systemTodos,
                                                                    "systemTodosIDs" => $systemTodosIDs,
                                                                    "systemTodosStatus" => $systemTodosStatus,
                                                                    "mymode" => $mymode,
                                                                    "systemmode" => $systemmode,

                                                                ]);
    }

    //Button to save system todo
    public function systemTodoForm($atts) {

        $title = "Füge das deiner Todo-Liste hinzu!";
        if(isset($atts['titel'])) { // Allow empty title here!
            $title = $atts['titel'];
        }

        $todo = "Mein Todo!";
        if(isset($atts['todo']) && trim($atts['todo']) != "") {
            $todo = $atts['todo'];
        }
        

        $btnname = "Hinzufügen";
        if(isset($atts['btnname'])) { // Allow empty title here!
            $btnname = $atts['btnname'];
        }

        //for future options
        $type = "button";
        if(isset($atts['type']) && trim($atts['type']) != "") {
            $type = $atts['type'];
        }

        $posttype = self::POST_TYPE_NAME_SYSTEM;
        if(isset($atts['posttype']) && trim($atts['posttype']) != "" && $atts['posttype'] == "mytodo") {
            $posttype = $atts['posttype'];
        }

        $titlepos = "center";
        if(isset($atts['titlepos']) && trim($atts['titlepos']) != "") {

            if($atts['titlepos'] == "left"){
                $titlepos = "left";
            }

            if($atts['titlepos'] == "right"){
                $titlepos = "right";
            }
            
        }

            $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
                return $twig->render("todo-list/system-todo-form.html", [
                                                                        "title" => $title,
                                                                        "type" => $type,
                                                                        "todo" => $todo,
                                                                        "btnname" => $btnname,
                                                                        "titlepos" => $titlepos,
                                                                        "posttype" => $posttype,
                                                                    ]);
    }

    public function addTodo(){
        if(isset($_POST['todo']) && trim($_POST['todo']) != "" && $_POST['todo'] != null && isset($_POST['mode'])) {
            $id = $post = wp_insert_post(
                [
                    'post_title' => $_POST['todo'],
                    'post_content' => 0,
                    'post_type' => $_POST['mode'],
                    'post_status' => 'publish',
                ]
            );
            echo $id;
        }
    }

    public function delTodo(){
        if(isset($_POST['dataId']) && trim($_POST['dataId']) != "" && $_POST['dataId'] != null && is_numeric($_POST['dataId'])) {
            wp_delete_post($_POST['dataId']);
        }
    }

    public function updateTodo(){
        if(isset($_POST['dataId']) && trim($_POST['dataId']) != "" && $_POST['dataId'] != null && is_numeric($_POST['dataId'])) {
            $my_post = array(
                'ID'           => $_POST['dataId'],
                'post_content' => $_POST['isChecked'],
            );
            wp_update_post( $my_post );
        }
    }

}
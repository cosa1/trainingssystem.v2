<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Coach_User_Remover
 * @subpackage Coach_User_Remover/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Coach_User_Remover
 * @subpackage Coach_User_Remover/admin
 * @author     Markus Domin, Helge Nissen <markus.domin@fh-luebeck.de>
 */
class Trainingssystem_Plugin_Module_Coach_User_Lastlogin {

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

	}
    // Save last login from user in database
    function user_last_login($user_login,$user ) {
        $lastlogin = Trainingssystem_Plugin_Database::getInstance()->LastLoginDao->save($user->ID);

		// User Event
		$title = "Anmeldung erfolgreich";
		$subtitle = "";
		// Params: $title, $subtitle, $userid, $postid (= null), $url (= null), $type, $level, $date (= null)
		Trainingssystem_Plugin_Database::getInstance()->UserEvent->insertUserEvent($title, $subtitle, $user->ID, null, null, "login", "info");
       	
		return $lastlogin;
    }

	function user_login_failed($user_name, $error) {
		$user = get_users(array("login" => $user_name));

		if(count($user) == 1) {
			// User Event
			$title = "Anmeldung fehlgeschlagen";
			$subtitle = "";
			// Params: $title, $subtitle, $userid, $postid (= null), $url (= null), $type, $level, $date (= null)
			Trainingssystem_Plugin_Database::getInstance()->UserEvent->insertUserEvent($title, $subtitle, $user[0]->ID, null, null, "login", "important");
		}
	}

	function user_logout($user_id) {
		$user = get_user_by("ID", $user_id);

		if($user !== false) {
			// User Event
			$title = "Abmeldung erfolgt";
			$subtitle = "";
			// Params: $title, $subtitle, $userid, $postid (= null), $url (= null), $type, $level, $date (= null)
			Trainingssystem_Plugin_Database::getInstance()->UserEvent->insertUserEvent($title, $subtitle, $user->ID, null, null, "logout", "info");
		}
 	}	
}

<?php
/**
 * Created by PhpStorm.
 * User: Ali Parnan
 * Date: 09.05.19
 * Time: 14:09
 */

class Trainingssystem_Plugin_Module_Bewertung{

    public function __construct(){

    }
        // save rating
    public function page_view_rating_save(){


        if(isset($_POST['pageid']) && isset($_POST['rating'])){
            $pageid = intval(sanitize_text_field($_POST['pageid']));
            $bewertung = floatval(sanitize_text_field($_POST['rating']));
            $allData=Trainingssystem_Plugin_Database::getInstance()->Bewertungen->save($pageid,$bewertung);

        }else {
            error_log("page_view_rating_save keine post");
        }
    }
    // show rating
    public function showRating(){
        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
        ob_start();
        global $post;
        echo $twig->render('bewertungen/rating.html', ["postid"=>$post->ID,"adminurl"=>admin_url('admin-ajax.php')
            ]);

        return ob_get_clean();
    }
}
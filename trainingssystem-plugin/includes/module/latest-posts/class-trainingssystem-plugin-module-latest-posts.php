<?php
/**
 * The functionality of the module.
 *
 * @package    Latest Posts
 * @author     Max Sternitzke <max.sternitzke@th-luebeck.de>
 */
class Trainingssystem_Plugin_Module_Latest_Posts {
   
   /**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

   }

   public function showLatestPosts($atts) {

      $heading = isset($atts['titel']) ? $atts['titel'] : 'Aktuelles'; 

      $mode = 'grid';
      if (isset($atts['mode'])) {
         $mode = $atts['mode'];
      }
      
      $autoslide = false;
      if (isset($atts['autoslide'])) {
         $autoslide =  $atts['autoslide'];
      }
      $autoslideduration = 4000;
      if (isset($atts['autoslideduration']) && is_numeric($atts['autoslideduration']) && $atts['autoslideduration'] > 0) {
         $autoslideduration =  $atts['autoslideduration'];
      }

      $numberposts = 3;
      if (isset($atts['numberposts']) && is_numeric($atts['numberposts']) && $atts['numberposts'] > 0) {
         if ($atts['numberposts'] >= 6) {
            $numberposts =  6;
         } else {
            $numberposts =  $atts['numberposts'];
         }
         
      }
      $slideduration = 700;
      if (isset($atts['slideduration'])&& is_numeric($atts['slideduration']) && $atts['slideduration'] > 0) {
         $slideduration =  $atts['slideduration'];
      }

      $number = 3;
      if(isset($atts['number']) && is_numeric($atts['number']) && ($atts['number'] > 0 || $atts['number'] == -1)) {
         $number = $atts['number'];
      }

      $numberchars = 150;
      if(isset($atts['numberchars']) && is_numeric($atts['numberchars']) && $atts['numberchars'] > 0) {
         $numberchars = $atts['numberchars'];
      }

      $post_type = array('post');
      if(isset($atts['post_type']) && trim($atts['post_type']) != "") {
         if(strpos($atts['post_type'], '|') !== false) {
            $post_type = explode('|', $atts['post_type']);
         } else {
            $post_type = array($atts['post_type']);
         }
      }

      $category = '0';
      if(isset($atts['category']) && trim($atts['category']) != "") {
         if(strpos($atts['category'], '|') !== false) {
            $cs = explode('|', $atts['category']);
            $category = "";
            foreach($cs as $c) {
               $category .= get_cat_ID($c) . ",";
            }
            $category = substr($category, 0, -1);
         } else {
            $category = get_cat_ID($atts['category']);
         }
      }

      $exclude = array();
      if(isset($atts['exclude']) && trim($atts['exclude']) != "") {
         if(strpos($atts['exclude'], '|') !== false) {
            $exclude = explode('|', $atts['exclude']);
         } else {
            $exclude = array($atts['exclude']);
         }
      }

      $include = array();
      if(isset($atts['include']) && trim($atts['include']) != "") {
         if(strpos($atts['include'], '|') !== false) {
            $include = explode('|', $atts['include']);
         } else {
            $include = array($atts['include']);
         }
      }

      $morelink = null;
      if(isset($atts['morelink']) && trim($atts['morelink']) != "") {
         $morelink = $atts['morelink'];
      }

      $morelinktext = "Weitere Beiträge";
      if(isset($atts['morelinktext']) && trim($atts['morelinktext']) != "") {
         $morelinktext = $atts['morelinktext'];
      }

      $posts = get_posts(array(
         "numberposts" => $number,
         "category" => $category,
         "post_type" => $post_type,
         "exclude" => $exclude,
         "include" => $include,
      ));


      $images = array();
      foreach($posts as $post) {
         if(get_the_post_thumbnail_url($post->ID) !== false) {
            $images[$post->ID] = get_the_post_thumbnail_url($post->ID);
         }
      }

      $content = array();
      foreach($posts as $post) {
         $content[$post->ID] = trim(preg_replace('/\s+/', ' ', strip_tags($post->post_content)));
      }

      $post_date = array();
      foreach($posts as $post) {
         $post_date[$post->ID] = get_the_date('d.m.Y', $post->ID);
      }

      $show_post_date = false;
      if (isset($atts['show_post_date']) && (trim($atts['show_post_date']) == "1" || trim($atts['show_post_date']) == "true")) {
         $show_post_date =  true;
      }

		$twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

      return $twig->render('latest-posts/latest-posts.html', ["posts" => $posts,
                                                               "heading" => $heading,
                                                               "morelink" => $morelink,
                                                               "morelinktext" => $morelinktext,
                                                               "numberchars" => $numberchars,
                                                               "images" => $images,
                                                               "content" => $content,
                                                               "mode" => $mode,
                                                               'numberposts' => $numberposts,
                                                               'slideduration' => $slideduration,
                                                               'autoslide' => $autoslide,
                                                               'autoslideduration' => $autoslideduration,
                                                               'rand' => mt_rand(),
                                                               'post_date' => $post_date,
                                                               'show_post_date' => $show_post_date
                                                            ]);
   }
}

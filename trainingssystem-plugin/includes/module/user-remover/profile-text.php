<?php

/**
 *  Übersetzungen für das Profil 
**/

$profile_text= array(
    'heading' => __("Mein Profil", TRAININGSSYSTEM_PLUGIN_SLUG),
    'change_mail' => __("E-Mail-Adresse ändern:", TRAININGSSYSTEM_PLUGIN_SLUG),
    'save' => __("Speichern", TRAININGSSYSTEM_PLUGIN_SLUG),
    'email_msg' => __("E-Mail-Benachrichtigungen:",TRAININGSSYSTEM_PLUGIN_SLUG),
    'change_pw' => __("Passwort ändern",TRAININGSSYSTEM_PLUGIN_SLUG),
    'old_pw' => __("Altes Passwort:",TRAININGSSYSTEM_PLUGIN_SLUG),
    'new_pw' => __("Neues Passwort:",TRAININGSSYSTEM_PLUGIN_SLUG),
    'old_pw' => __("Altes Passwort:",TRAININGSSYSTEM_PLUGIN_SLUG),
    'new_pw_repeat' => __("Neues Passwort wiederholen:",TRAININGSSYSTEM_PLUGIN_SLUG)
);

?>
<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Coach_User_Remover
 * @subpackage Coach_User_Remover/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Coach_User_Remover
 * @subpackage Coach_User_Remover/admin
 * @author     Markus Domin, Helge Nissen <markus.domin@fh-luebeck.de>
 */
class Trainingssystem_Plugin_Module_User_Remover_Admin
{

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     */
    public function __construct()
    {

    }

    /**
     * Inhalte löschen, wenn User gelöscht wird
     *
     * @since    1.0.0
     */
    public function user_delete_content($user_id)
    {
        Trainingssystem_Plugin_Database::getInstance()->NutzerDao->deleteNutzer($user_id);
        Trainingssystem_Plugin_Database::getInstance()->Bewertungen->deleteUserInput($user_id);
        Trainingssystem_Plugin_Database::getInstance()->UserLogs->deleteUserInput($user_id);
        Trainingssystem_Plugin_Database::getInstance()->CompanyDao->deleteUserInput($user_id);
        Trainingssystem_Plugin_Database::getInstance()->CompanygroupDao->deleteUserInput($user_id);
        Trainingssystem_Plugin_Database::getInstance()->CompanygroupUserDao->deleteUserInput($user_id);
        Trainingssystem_Plugin_Database::getInstance()->NutzerDao->deleteUserInput($user_id);
        Trainingssystem_Plugin_Database::getInstance()->LastLoginDao->deleteUserInput($user_id);
        Trainingssystem_Plugin_Database::getInstance()->Formdata->deleteAllFormDataForUser($user_id);
        Trainingssystem_Plugin_Database::getInstance()->TrainingDate->deleteAllTrainingDatesForUser($user_id);
        Trainingssystem_Plugin_Database::getInstance()->TrainingDep->deleteAllTrainingDepsForUser($user_id);
        Trainingssystem_Plugin_Database::getInstance()->UserEvent->deleteAllEventsForUser($user_id);
    }

    public function user_reassign_content($user_id) {
        global $wpdb;

        $settings = get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS);
        
        if(isset($settings["system_user_id"]) && $settings["system_user_id"] != "0") {
            $sys_user = $settings["system_user_id"];
            $sql = $wpdb->prepare("UPDATE {$wpdb->posts} SET post_author = '%d' WHERE post_author = '%d'", $sys_user, $user_id);

            if($wpdb->query($sql) === false) {
                wp_die("Fehler beim Zuweisen der Inhalte an den Systembenutzer.");
            }
        }

    }

}

<?php

/**
 * Swiping Card
 * 
 * @author      Hamid Mergan <hamid.mergan@th-luebeck.de> 
 *              Tim Mallwitz  <tim.mallwitz@th-luebeck.de>
 * 
 * @package     Swiping Card
 */


class Trainingssystem_Plugin_Module_Swiping_Card{

    public function __construct() {}

    public function showSwipingCard($atts,$content=null){
        $title = "";
        if(isset($atts['titel'])) { // Allow empty title here!
            $title = $atts['titel'];
        }

        $textComplete = "Sie haben alle Karten bearbeitet.";
        if(isset($atts['text-complete']) && trim($atts['text-complete'] != '')) {
            $textComplete = $atts['text-complete'];
        }
        //buggy with back-btn
        // $stack = "1";  
        // if(isset($atts['stack']) && trim($atts['stack'] != '')) {
        //     $stack = $atts['stack'];
        // }
        
        $colorError = false;

        // like button
        $likeBtnColor = "#d3d3d3";
        if(isset($atts['like-btn-color']) && trim($atts['like-btn-color'] != '')) {
            // check if color matches hex format
            if(preg_match("/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?\b/", $atts['like-btn-color'])){
                $likeBtnColor = $atts['like-btn-color'];
            }
            else{
                $colorError = true;
            }
        }

        $likeIcon = "fa fa-check";
        if(isset($atts['like-icon']) && trim($atts['like-icon'] != '')) {
            $likeIcon = $atts['like-icon'];
        }

        $likeIconHelper = explode(" ", $likeIcon);
        $likeIconHelper2 = $likeIconHelper[count($likeIconHelper)-1];

        $likeIconColor = "#008000";
        if(isset($atts['like-icon-color']) && trim($atts['like-icon-color'] != '')) {
            // check if color matches hex format
            if(preg_match("/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?\b/", $atts['like-icon-color'])){
                $likeIconColor = $atts['like-icon-color'];
            }
            else{
                $colorError = true;
            }
        }

        // dislike button
        $dislikeBtnColor = "#d3d3d3";
        if(isset($atts['dislike-btn-color']) && trim($atts['dislike-btn-color'] != '')) {
            // check if color matches hex format
            if(preg_match("/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?\b/", $atts['dislike-btn-color'])){
                $dislikeBtnColor = $atts['dislike-btn-color'];
            }
            else{
                $colorError = true;
            }            
        }

        $dislikeIcon = "fa fa-times";
        if(isset($atts['dislike-icon']) && trim($atts['dislike-icon'] != '')) {
            $dislikeIcon = $atts['dislike-icon'];
        }

        $dislikeIconHelper = explode(" ", $dislikeIcon);
        $dislikeIconHelper2 = $dislikeIconHelper[count($dislikeIconHelper)-1];

        $dislikeIconColor = "#ff0000";
        if(isset($atts['dislike-icon-color']) && trim($atts['dislike-icon-color'] != '')) {
            // check if color matches hex format
            if(preg_match("/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?\b/", $atts['dislike-icon-color'])){
                $dislikeIconColor = $atts['dislike-icon-color'];
            }
            else{
                $colorError = true;
            }
        }

        // undo button
        $undoBtnColor = "#d3d3d3";
        if(isset($atts['undo-btn-color']) && trim($atts['undo-btn-color'] != '')) {
            // check if color matches hex format
            if(preg_match("/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?\b/", $atts['undo-btn-color'])){
                $undoBtnColor = $atts['undo-btn-color'];
            }
            else{
                $colorError = true;
            } 
        }

        $undoIconColor = "#000000";
        if(isset($atts['undo-icon-color']) && trim($atts['undo-icon-color'] != '')) {
            // check if color matches hex format
            if(preg_match("/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?\b/", $atts['undo-icon-color'])){
                $undoIconColor = $atts['undo-icon-color'];
            }
            else{
                $colorError = true;
            }
        }

        $showUndoBtn = true;
        if(isset($atts['show-undo-btn'])){
            if(trim($atts['show-undo-btn']) == "0" || trim($atts['show-undo-btn']) == "false"){
                $showUndoBtn = false;
            }
        }

        $undoBtnMode = "all";
        if(isset($atts['undo-btn-mode'])){
            if(trim($atts['undo-btn-mode']) == "single"){
                $undoBtnMode = "single";
            }
        }

        // repeat button
        $repeatBtnColor = "#d3d3d3";
        if(isset($atts['repeat-btn-color']) && trim($atts['repeat-btn-color'] != '')) {
            // check if color matches hex format
            if(preg_match("/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?\b/", $atts['repeat-btn-color'])){
                $repeatBtnColor = $atts['repeat-btn-color'];
            }
            else{
                $colorError = true;
            } 
        }

        $repeatIconColor = "#0080ff";
        if(isset($atts['repeat-icon-color']) && trim($atts['repeat-icon-color'] != '')) {
            // check if color matches hex format
            if(preg_match("/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?\b/", $atts['repeat-icon-color'])){
                $repeatIconColor = $atts['repeat-icon-color'];
            }
            else{
                $colorError = true;
            }
        }

        $showRepeatBtn = false;
        if(isset($atts['show-repeat-btn'])){
            if(trim($atts['show-repeat-btn']) == "1" || trim($atts['show-repeat-btn']) == "true"){
                $showRepeatBtn = true;
            }
        }


        $formidlikes = (isset($atts['form_id_likes']) && is_numeric($atts['form_id_likes']) && get_post_type($atts['form_id_likes']) == "formulare") ? $atts['form_id_likes'] : null;
		
        $formDataLikes = Trainingssystem_Plugin_Database::getInstance()->Formfield->getUserFormfieldsByForm($formidlikes, Trainingssystem_Plugin_Public::getTrainingUser());

        $formiddislikes = (isset($atts['form_id_dislikes']) && is_numeric($atts['form_id_dislikes']) && get_post_type($atts['form_id_dislikes']) == "formulare") ? $atts['form_id_dislikes'] : null;

        $formDataDislikes = Trainingssystem_Plugin_Database::getInstance()->Formfield->getUserFormfieldsByForm($formiddislikes, Trainingssystem_Plugin_Public::getTrainingUser());

        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
        if(!is_null($formidlikes) && !is_null($formiddislikes) && !$colorError){
            $likeFormfieldId = $formDataLikes[0][0]->getFieldId();
            $dislikeFormfieldId = $formDataDislikes[0][0]->getFieldId();

            $contentTemp = trim($content);
            if($contentTemp == null){
                return $twig->render("swiping-element/swiping-card-error.html");
            }
            $doc = new DOMDocument();
            $doc->loadHTML($contentTemp);
            $cardArray = [];

            $classname = "swipingcard";

            $dom = new DOMDocument();
            $dom->loadHTML('<?xml encoding="utf-8" ?>' .$contentTemp);
        
            $xpath = new DOMXpath($dom);
            $nodes = $xpath->query('//div[@class="' . $classname . '"]');
        
            $tmp_dom = new DOMDocument();

            foreach ($nodes as $node) {
                $tempObj = new stdClass;
                $tmp_dom->appendChild($tmp_dom->importNode($node, true));
                $tempObj->bgColor = "#FFFFFF";
                foreach ($node->attributes as $attr) {

                    if("title" == $attr->nodeName){
                        $tempObj->title = $attr->nodeValue;
                    }
                    if("title-color" == $attr->nodeName){
                        // check if color matches hex format
                        if(preg_match("/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?\b/", $attr->nodeValue)){
                            $tempObj->titleColor = $attr->nodeValue;
                        }
                        else{
                            $colorError = true;
                        }
                    }
                    if("content-color" == $attr->nodeName){
                        // check if color matches hex format
                        if(preg_match("/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?\b/", $attr->nodeValue)){
                            $tempObj->contentColor = $attr->nodeValue;
                        }
                        else{
                            $colorError = true;
                        }
                    }
                    if("bg-color" == $attr->nodeName){
                        // check if color matches hex format
                        if(preg_match("/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?\b/", $attr->nodeValue)){
                            $tempObj->bgColor = $attr->nodeValue;
                        }
                        else{
                            $colorError = true;
                        }
                    }
                    //0 = background , 1 = img top
                    if("img-mode" == $attr->nodeName){
                        $tempObj->imgMode = $attr->nodeValue;
                    }
                    if("img-src" == $attr->nodeName){
                        $tempObj->imgSrc = $attr->nodeValue;
                    }

                }
                $tempObj->content = trim($node->nodeValue);
                array_push($cardArray,$tempObj);
            }

            if(!$colorError){
                return $twig->render('swiping-element/swiping-card.html', [
                    "cardArray" => $cardArray,
                    "title" => $title,
                    "likeBtnColor" => $likeBtnColor,
                    "dislikeBtnColor" => $dislikeBtnColor,
                    "undoBtnColor" => $undoBtnColor,
                    "likeIconColor" => $likeIconColor,
                    "dislikeIconColor" => $dislikeIconColor,
                    "undoIconColor" => $undoIconColor,
                    "likeIcon" => $likeIcon,
                    "likeIconHelper2" => $likeIconHelper2,
                    "dislikeIconHelper2" => $dislikeIconHelper2,
                    "dislikeIcon" => $dislikeIcon,
                    "textComplete" => $textComplete,
                    "formidlikes" => $formidlikes,
                    "formiddislikes" => $formiddislikes,
                    "likeFormfieldId" => $likeFormfieldId,
                    "dislikeFormfieldId" => $dislikeFormfieldId,
                    "tslikeform" => $formidlikes != "" ? do_shortcode("[ts_forms id='" . $formidlikes . "']") : '',
                    "tsdislikeform" => $formiddislikes != "" ? do_shortcode("[ts_forms id='" . $formiddislikes . "']") : '',
                    "showUndoBtn" => $showUndoBtn,
                    "undoBtnMode" => $undoBtnMode,
                    "repeatBtnColor" => $repeatBtnColor,
                    "repeatIconColor" => $repeatIconColor,
                    "showRepeatBtn" => $showRepeatBtn
                ]);
            }
            else{
                return $twig->render("swiping-element/swiping-card-error.html");
            }
        }
        else{
			return $twig->render("swiping-element/swiping-card-error.html");
		}
    }
}


<?php

class Trainingssystem_Plugin_Module_Contact_Info{

    public function __construct(){
    }

    function showContactInfo($atts){

        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

        $current_user = wp_get_current_user();
        $userid = $current_user->ID;

        $heading = isset($atts['titel']) ? $atts['titel'] : 'Kontakt'; 

        $singlecard = false;
        $multicard = false;

        if(isset($atts['mode'])){
            if(trim($atts['mode']) != ""){

                if(trim($atts['mode']) == "singlecard"){
                    $singlecard = true;
                }
                else if(trim($atts['mode']) == "multicard"){
                    $multicard = true;
                }
            }
        }

        $contactInfo = array();

        if ( 0 != $current_user->ID ) {
            $showBossInfo = true;
            $showKAdminInfo = true;
            $showCoachInfo = true;
            $showTeamLeaderInfo = true;

            if(isset($atts['exclude'])){
                if(trim($atts['exclude']) != ""){
                    $excludeValue = explode(",",trim($atts['exclude']));
                    foreach($excludeValue as $exV){
    
                        if(trim(strtolower($exV)) == "boss"){
                            $showBossInfo = false;
                        }
                        if(trim(strtolower($exV)) == "teamleader"){
                            $showTeamLeaderInfo = false;
                        }
                        else if(trim(strtolower($exV)) == "kadmin"){
                            $showKAdminInfo = false;
                        }
                        else if(trim(strtolower($exV)) == "coach"){
                            $showCoachInfo = false;
                        }
                    }
                }
            }

            $showBossMail = true;
            $showKAdminMail = true;
            $showCoachMail = true;
            $showTeamLeaderMail = true;

            if(isset($atts['hide_mail'])){
                if(trim($atts['hide_mail']) != ""){
                    $hideMailValue = explode(",",trim($atts['hide_mail']));
                    foreach($hideMailValue as $hV){
    
                        if(trim(strtolower($hV)) == "boss"){
                            $showBossMail = false;
                        }
                        if(trim(strtolower($hV)) == "teamleader"){
                            $showTeamLeaderMail = false;
                        }
                        else if(trim(strtolower($hV)) == "kadmin"){
                            $showKAdminMail = false;
                        }
                        else if(trim(strtolower($hV)) == "coach"){
                            $showCoachMail = false;
                        }
                    }
                }
            }

            $companyInformation = Trainingssystem_Plugin_Database::getInstance()->CompanyDao->getBossAndKAdminByUser($userid);

            if($showBossInfo){
                if(isset($companyInformation)){
                    $bossid = $companyInformation->bossid;
                    if($bossid != 0){
                        $boss = get_userdata($bossid);
                        $bossInfo = array();
                        $bossInfo['title'] = "Ihre Führungskraft";
        
                        $bossFirstName = get_user_meta($bossid, 'first_name', true);
                        $bossLastName = get_user_meta($bossid, 'last_name', true);
        
                        if(trim($bossFirstName) != "" && trim($bossLastName)){
                            $bossInfo["name"] = $bossFirstName." ".$bossLastName;
                        }
                        else{
                            $bossInfo["name"] = $boss->display_name;
                        }

                        if(trim($boss->user_email) != ""){
                            $bossInfo["mail"] = $boss->user_email;
                        }

                        
                        $bossPhone = get_user_meta($bossid, 'phonenumber', true);
        
                        if(trim($bossPhone) != ""){
                            $bossInfo["phone"] = $bossPhone;
                        }

                        $bossInfo["showMail"] = $showBossMail;
        
                        $contactInfo[] = $bossInfo;
                    }
                }

            }

            if($showTeamLeaderInfo){
                $groupID = Trainingssystem_Plugin_Database::getInstance()->CompanygroupUserDao->getGroupIdByUserId($userid);
                if(isset($groupID)){

                    $queryArgs = array(
                        'meta_key' => 'team_leader',
                        'meta_value' => $groupID->gruppenid
                    );
                
                    $user_query = new WP_User_Query( $queryArgs );
                    $team_leader = $user_query->get_results();

                    $team_leaderid ="";
                    if(!empty($team_leader)){
                        foreach($team_leader as $gLeader){
                            $team_leaderid = $gLeader->ID;
                        }
                    }

                    if(isset($team_leaderid)){
                        if($team_leaderid != 0){
                            $team_leader = get_userdata($team_leaderid);
                            $teamLeaderInfo = array();
                            $teamLeaderInfo['title'] = "Ihre Teamleitung";
        
                            $teamLeaderFirstName = get_user_meta($team_leaderid, 'first_name', true);
                            $teamLeaderLastName = get_user_meta($team_leaderid, 'last_name', true);
        
                            if(trim($teamLeaderFirstName) != "" && trim($teamLeaderLastName)){
                                $teamLeaderInfo["name"] = $teamLeaderFirstName." ".$teamLeaderLastName;
                            }
                            else{
                                $teamLeaderInfo["name"] = $team_leader->display_name;
                            }
                            
                            if(trim($team_leader->user_email) != ""){
                                $teamLeaderInfo["mail"] = $team_leader->user_email;
                            }
                            
                            $teamLeaderPhone = get_user_meta($team_leaderid, 'phonenumber', true);
        
                            if(trim($teamLeaderPhone) != ""){
                                $teamLeaderInfo["phone"] = $teamLeaderPhone;
                            }

                            $teamLeaderInfo["showMail"] = $showTeamLeaderMail;
        
                            $contactInfo[] = $teamLeaderInfo;
                        }
                    }
                }
            }
    
            if($showKAdminInfo){
                if(isset($companyInformation)){
                    $kAdminid = $companyInformation->kadminid;
                    if($kAdminid != 0){
                        $kAdmin = get_userdata($kAdminid);
                        $kAdminInfo = array();
                        $kAdminInfo['title'] = "Ihr Krankenkassenkontakt";
        
                        $kAdminFirstName = get_user_meta($kAdminid, 'first_name', true);
                        $kAdminLastName = get_user_meta($kAdminid, 'last_name', true);
        
                        if(trim($kAdminFirstName) != "" && trim($kAdminLastName)){
                            $kAdminInfo["name"] = $kAdminFirstName." ".$kAdminLastName;
                        }
                        else{
                            $kAdminInfo["name"] = $kAdmin->display_name;
                        }

                        if(trim($kAdmin->user_email) != ""){
                            $kAdminInfo["mail"] = $kAdmin->user_email;
                        }
                        
                        $kAdminPhone = get_user_meta($kAdminid, 'phonenumber', true);
        
                        if(trim($kAdminPhone) != ""){
                            $kAdminInfo["phone"] = $kAdminPhone;
                        }

                        $kAdminInfo["showMail"] = $showKAdminMail;
        
                        $contactInfo[] = $kAdminInfo;
                    }
                }

            }
    
            if($showCoachInfo){
                $coachid = get_user_meta($userid, 'coachid', true);
                if($coachid != ""){
                    $coach = get_userdata($coachid);
                    $coachInfo = array();
                    $coachInfo["title"] = "Ihr E-Coach";
    
                    $coachFirstName = get_user_meta($coachid, 'first_name', true);
                    $coachLastName = get_user_meta($coachid, 'last_name', true);
    
                    if(trim($coachFirstName) != "" && trim($coachLastName)){
                        $coachInfo["name"] = $coachFirstName." ".$coachLastName;
                    }
                    else{
                        $coachInfo["name"] = $coach->display_name;
                    }
                    
                    if(trim($coach->user_email) != ""){
                        $coachInfo["mail"] = $coach->user_email;
                    }
                    
                    $coachPhone = get_user_meta($coachid, 'phonenumber', true);
    
                    if(trim($coachPhone) != ""){
                        $coachInfo["phone"] = $coachPhone;
                    }

                    $coachInfo["showMail"] = $showCoachMail;
    
                    $contactInfo[] = $coachInfo;
                }
            }
        }

        if(isset($atts['contacts'])){
            if(trim($atts['contacts']) != ""){
                $contactArray = explode("||", trim($atts['contacts']));
                foreach($contactArray as $thisContact){
                    $thisContactValues = explode("|", $thisContact);

                    if(count($thisContactValues) == 2){
                        $thisContactInfo = array();
                        $thisContactInfo["title"] = $thisContactValues[0];
                        $thisContactId = $thisContactValues[1];
                        $thisContactData = get_userdata($thisContactId);
                        
                        $thisContactFirstName = get_user_meta($thisContactId, 'first_name', true);
                        $thisContactLastName = get_user_meta($thisContactId, 'last_name', true);
        
                        if(trim($thisContactFirstName) != "" && trim($thisContactLastName)){
                            $thisContactInfo["name"] = $thisContactFirstName." ".$thisContactLastName;
                        }
                        else{
                            $thisContactInfo["name"] = $thisContactData->display_name;
                        }
        
                        if(trim($thisContactData->user_email) != ""){
                            $thisContactInfo["mail"] = $thisContactData->user_email;
                        }
        
                        $thisContactPhone = get_user_meta($thisContactId, 'phonenumber', true);
        
                        if(trim($thisContactPhone) != ""){
                            $thisContactInfo["phone"] = $thisContactPhone;
                        }
                        
                        $thisContactInfo["showMail"] = true;

                        $contactInfo[] = $thisContactInfo;
                    }
                    else if(count($thisContactValues) == 4){
                        $thisContactInfo = array();
                        $thisContactInfo["title"] = trim($thisContactValues[0]);
                        $thisContactInfo["name"] = trim($thisContactValues[1]);
                        $thisContactInfo["mail"] = trim($thisContactValues[2]);
                        $thisContactInfo["phone"] = trim($thisContactValues[3]);
                        $thisContactInfo["showMail"] = true;
                        $contactInfo[] = $thisContactInfo;
                    }
                }
            }
        }
                
        return $twig->render("contact-info/contact-info.html", [
            "heading" => $heading,
            "singlecard" => $singlecard,
            "multicard" => $multicard,
            "contactInfo" => $contactInfo,
            "setting_hide_mails" => isset(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)['hide_mails'])
        ]);
    }


}
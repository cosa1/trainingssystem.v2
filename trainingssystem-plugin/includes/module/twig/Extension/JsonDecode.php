<?php
/**
 * Created by PhpStorm.
 * User: clair
 * Date: 29.01.19
 * Time: 13:47
 */

class Twig_Extension_JsonDecode extends Twig_Extension
{

    public function getName()
    {
        return 'json_decoder';
    }

    public function getFilters() {
        $this->filters['json_decode'] = new \Twig_SimpleFilter('json_decode', array($this, 'json_decode'));
        return $this->filters;

    }

    public function json_decode($str) {
 
        return json_decode($str,true);
    }
}

class_alias('Twig_Extension_JsonDecode', 'Twig\Extension\JsonDecodeExtension', false);
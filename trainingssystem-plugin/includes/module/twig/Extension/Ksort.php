<?php
/**
 * Created by PhpStorm.
 * User: clair
 * Date: 29.01.19
 * Time: 10:17
 */
class Twig_Extension_Ksort extends Twig_Extension
{
    public function ksort($array) {
        ksort($array);
        return $array;
    }
    public function getFilters() {
        $this->filters['ksort'] = new \Twig_SimpleFilter('ksort', array($this, 'ksort'));
        return $this->filters;
    }
    public function getName()
    {
        return 'ksort';
    }
}

class_alias('Twig_Extension_Ksort', 'Twig\Extension\KsortExtension', false);


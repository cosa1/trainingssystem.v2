<?php

class Trainingssystem_Plugin_Module_PDFDownload{

    public function __construct() {

    }

    // Shortcode-Handler für Shortcode pdfdownloadignorestart
	public function scPDFDownloadIgnoreStart($atts, $content=null){
		return '';
	}

	// Shortcode-Handler für Shortcode pdfdownloadignoreend
	public function scPDFDownloadIgnoreEnd($atts, $content=null){
		return '';
	}
    
    public function scPDFDownload($atts, $content=null){

        $a = shortcode_atts(array(
			'name' => 'mypdf',
			'format' => 'p', 
			'pdfid' => '0',
			'title' => 'Download als PDF',
			'divid' => '',
            'docheadline' => ''
        ), $atts);
        
        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
        ob_start();
        // Überprüfung auf verbotene Zeichen in Dateinamen
		if(preg_match('/[^A-Za-z0-9\-_.]/', $a['name'])){
            echo do_shortcode($content);
            echo $twig->render('pdf-download/pdf-download-warning-symbols.html');           
        }
        else{
            echo do_shortcode($content);
            $contentTemp = trim($content);
            $contentArray =  preg_split("/\r\n|\n|\r/", $contentTemp);
    
            echo $twig->render('pdf-download/pdf-download-main.html', [
                "name" => $a['name'],
                "format" => $a['format'],
                "pdfid" => $a['pdfid'],
                "title" => $a['title'],
                "divid" => $a['divid'],
                "docheadline" => $a['docheadline'],
                "content" => json_encode($contentArray)
            ]);
        }



        return ob_get_clean();
    }
}
<?php

namespace Trainingssystem\Module\AppAdminPanel\RequestHandler;

use Trainingssystem\Module\AppAdminPanel\Query\AddTrainingAudioMappingQuery;
use Trainingssystem\Module\AppAdminPanel\Query\DeleteTrainingAudioMappingQuery;

class AudioPostHandler
{
    private AddTrainingAudioMappingQuery $addTrainingAudioMappingQuery;
    private DeleteTrainingAudioMappingQuery $deleteTrainingAudioMapping;

    public function __construct(
        AddTrainingAudioMappingQuery $addTrainingAudioMappingQuery,
        DeleteTrainingAudioMappingQuery $deleteTrainingAudioMapping
    )
    {
        $this->addTrainingAudioMappingQuery = $addTrainingAudioMappingQuery;
        $this->deleteTrainingAudioMapping = $deleteTrainingAudioMapping;
    }


    public function handle(array $postData): void
    {
        switch ($postData['action']) {
            case 'add':
                $this->addTrainingAudioMappingQuery->execute($postData['trainingId'], $postData['audioId']);
                break;
            case 'del':
                $this->deleteTrainingAudioMapping->execute($postData['trainingId'], $postData['audioId']);
                break;
        }
    }
}

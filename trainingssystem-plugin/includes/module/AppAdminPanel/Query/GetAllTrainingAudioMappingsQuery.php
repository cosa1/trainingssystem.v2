<?php

namespace Trainingssystem\Module\AppAdminPanel\Query;

class GetAllTrainingAudioMappingsQuery
{
    private const TRAINING_AUDIO_MAPPING_META_KEY = 'training_audio_mapping';

    public function execute(): array
    {
        global $wpdb;

        $querystr = $wpdb->prepare("
            SELECT post_id training_id, meta_value audio_id
    FROM $wpdb->postmeta 
    WHERE meta_key = %s
    ORDER BY post_id
", self::TRAINING_AUDIO_MAPPING_META_KEY);

        $result = $wpdb->get_results($querystr);
        return $result;
    }
}

<?php

namespace Trainingssystem\Module\AppAdminPanel\Query;

class DeleteTrainingAudioMappingQuery
{
    private const TRAINING_AUDIO_MAPPING_META_KEY = 'training_audio_mapping';

    public function execute(int $trainingId, int $audioId): void
    {
        global $wpdb;

        $wpdb->delete(
            $wpdb->postmeta,
            [
                'post_id' => $trainingId,
                'meta_key' => self::TRAINING_AUDIO_MAPPING_META_KEY,
                'meta_value' => $audioId
            ]
        );
    }
}

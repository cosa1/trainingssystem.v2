<?php

/**
 * The functionality of the module.
 *
 * @package    TS Forms extended
 * @author     Max Sternitzke <max.sternitzke@th-luebeck.de>
 */
class Trainingssystem_Plugin_Module_Forms_Media_Grid {

    /**
     * FRONTEND 
     * 
     * Shows the media grid with various options
     * 
     * @param Array Shortcode Attributes
     * 
     * @return HTML-Code
     */
    public function showMediaGrid($atts) {
        $view = null;
        if(isset($atts['view']) && trim($atts['view']) != "" && trim($atts['view']) == "all") {
            $view = "all";
        } else if(isset($atts['view']) && trim($atts['view']) != "" && trim($atts['view']) == "boss") {
            $view = "boss";
        }else if(isset($atts['view']) && trim($atts['view']) != "" && trim($atts['view']) == "kAdmin"){
            $view = "kAdmin";
        }else if(isset($atts['view']) && trim($atts['view']) != "" && trim($atts['view']) == "my") {
            $view = "my";
        }

        $uploadfieldid = null;
        if(isset($atts['uploadfieldid']) && trim($atts['uploadfieldid']) != "") {
            $uploadfieldid = $atts['uploadfieldid'];
        }

        $projectfieldid = null;
        if(isset($atts['projectfieldid']) && trim($atts['projectfieldid']) != "") {
            $projectfieldid = $atts['projectfieldid'];
        }

        $titlefieldid = null;
        if(isset($atts['titlefieldid']) && trim($atts['titlefieldid']) != "") {
            $titlefieldid = $atts['titlefieldid'];
        }

        $descfieldid = null;
        if(isset($atts['descfieldid']) && trim($atts['descfieldid']) != "") {
            $descfieldid = $atts['descfieldid'];
        }

        $defaultimage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAYAAAD0eNT6AAAAAXNSR0IArs4c6QAAAERlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAA6ABAAMAAAABAAEAAKACAAQAAAABAAACAKADAAQAAAABAAACAAAAAAAL+LWFAAAnSUlEQVR4Ae3dCbSuZVUHcG4yCogDgkOamjiQ5oxiEBcFQkVcKuJEZM5jZg6ZUxrqcmlmLjOXkaaYA6YiToAiXNIcUtQ0XDSpaWmCM4qADO1nyYVz53POfr/ve/b7/t619rrnfOd7nnc/v33w/D3jmu2mfe0Txz8y6h5Rt4jaNcpFgEAfAjtEG9eK2jmqvbwm6hdR50d9JuqVUV+MchEgsAqB9h/UFK/2Af+4qEOmeHhnJjAigW/FWR4fdeqIzuQoBOYiMLUAsGOovirqD+ai6yYECMxL4CNxo/tHXT6vG7oPgeoCUwoAu8ewPhh1UPWh6Z8Agc0KXBiP7h31082+1YMECGwgMJUA0P6ff/sU4cEbnN4rBAiMUWCnONQlYzyYMxEYUuBXhtys471eFr354N/xgLRGYECBi2Ov9k2DLgIEtiIwhc8A3CnOf3bUFM66lVF7E4FJCXwuTntAlM8ETGrsDrsSgSl8BuClAeKD/0reKzyXQH2Bu8URToxqX/5zESCwGYGxf2C8eZz5a5s5t4cIEJiGwAfimEdHtS8LuAgQWCIw9s8APHjJWb1IgMD0BNov+npPVPvGQBcBAksExh4ADlxyVi8SIDBNgSPi2O+Lar9R0EWAwJUCYw8Av2HSBAgQCIH7Rp0UJQR4dyBwpcDYA8BeJk2AAIErBQ6Pf0+O2oUIAQLbbTf2AOA7gL2XEyCwVOCweEUIWCri5ckKjD0ATHawDk6AwBYFDo23tF8Lfs0tPsMbCExAQACYwJAdkQCBTQTuHY98KGrXTd7iAQITERAAJjJoxyRAYBOBg+MRIWATFg9MRUAAmMqknZMAgc0JrI0H258S3m1zb/QYgTELCABjnq6zESCwHIHfjiedEtX+ZLiLwGQEBIDJjNpBCRDYikD7w0FCwFaAvGl8AgLA+GbqRAQIrE7gt2LZaVHXWt1yqwjUEhAAas1LtwQIzFZg/9i+hYA9ZnsbuxNYvIAAsPgZ6IAAgb4E7hHtfDTq2n21pRsCwwoIAMN62o0AgXEI7BfHEALGMUun2IKAALAFGA8TIDB5gbuFwOlR15m8BIBRCggAoxyrQxEgMJDAXWKfFgKuO9B+tiHQjcCabjqZTSMXxbY7zWbrq3Z9XLx07lWveYEAgY0F1sYDx238YLHXvxT9HhL1/WJ9a5fAZAVaALhixnWbyeo6OIHlCRwVT5v1f4fz2L+FgD2Xd2TPItC/gC8B9D8jHRIg0IfAHaKNM6Ku30c7uiCQExAAcn5WEyAwLYHbx3FbCNhrWsd22jEKCABjnKozESAwS4HbxeZnRu09y5vYm8CsBQSAWQvbnwCBMQrsG4dqIeAGYzycM01DQACYxpydkgCB4QVuG1u2EHDD4be2I4HZCwgAszd2BwIExivQfgpoXdSNxntEJxurgAAw1sk6FwEC8xK4VdxoXdSN53VD9yEwhIAAMISiPQgQmLrAPgGwLupXpw7h/HUEBIA6s9IpAQJ9C9wy2lsXdZO+29QdgV8KCADeEwgQIDCcwK/HVuuibjrclnYiMBsBAWA2rnYlQGC6AreIo58VdbPpEjh5BQEBoMKU9EiAwLYEzo4nPHFbT5rj228W91oX1f51EehSQADociyaIkBgFQJvjDWPX8W6WS35tdi4fSagfUbARaA7AQGgu5FoiACBhMDxsfaxUVck9hhyaftegHVR7XsDXAS6EhAAuhqHZggQGEDgTbHHY6J6CQHtpwLWRbWfEnAR6EZAAOhmFBohQGBAgb+LvX4/qpcQ0H4/wLqo9vsCXAS6EBAAuhiDJggQmIHAW2PP34u6fAZ7r2bL9psC2/cE3Ho1i60hMLSAADC0qP0IEOhJ4G3RzLFRvYSA9oeDzoxqf0PARWChAgLAQvndnACBOQi8Pe5xTFRPIWBd9NP+mqCLwMIEBICF0bsxAQJzFHhn3OsRUZfN8Z5bu9Xe8cZ1Uftu7UneRmCWAgLALHXtTYBATwInRjMPj+olBOwVvayLul2Ui8DcBQSAuZO7IQECCxT4h7j3Q6MuXWAPS299/XilfU/A7Zc+6GUC8xAQAOah7B4ECPQk8N5o5uioXkLAntFLCwF3iHIRmJuAADA3ajciQKAjgZOil6OiftFJT9eLPj4edcdO+tHGBAQEgAkM2REJENiswMnxaI8h4M6b7daDBAYWEAAGBrUdAQKlBD4Q3T4o6pJOur5u9HF61F066UcbIxYQAEY8XEcjQGBZAh+KZz0wqpcQcJ3opYWAu0a5CMxMQACYGa2NCRAoJPCR6PUBURd30vO1o48WAvbrpB9tjFBAABjhUB2JAIFVCZwaq1oIuGhVq4dftEds+bGouw+/tR0JbLedAOC9gAABAlcLnBYvHhnVSwi4VvTSQsD+US4CgwoIAINy2owAgREItA+4R0T9vJOz7B59tGByz0760cZIBASAkQzSMQgQGFSg/Ux+jyHggEFPabNJCwgAkx6/wxMgsBWBM+Jt9426cCvPmeebdoubte9TOHCeN3Wv8QoIAOOdrZMRIJAXWBdb3CfqZ/mtBtlh19jllKiDBtnNJpMWEAAmPX6HJ0BgGQL/GM9pIeCny3juPJ7SQkD7scWD53Ez9xivgAAw3tk6GQECwwl8IrY6PKqXEHDN6OXDUfeKchFYlYAAsCo2iwgQmKDAP8WZfyfqgk7Ovkv00ULAIZ30o41iAgJAsYFplwCBhQp8Ku5+WNRPFtrF1TffOV78YFTryUVgRQICwIq4PJkAAQLbfSYMDo36cScWLQS0P2rUPjvhIrBsAQFg2VSeSIAAgasE/jleaiHgR1c9stgXdorbtz9v3L5Z0UVgWQICwLKYPIkAAQKbCHwuHmlff//hJm9ZzAMtBLw/6n6Lub27VhMQAKpNTL8ECPQkcHY001MI2DH6OSnq/j0h6aVPAQGgz7noigCBOgJfiFbvHfWDTlreIfp4b1T7y4YuAlsUEAC2SOMNBAgQWLbAF+OZ94r6/rJXzPaJLQS8J+qBs72N3SsLCACVp6d3AgR6EviXaKaFgO910tT20ce7ox7UST/a6ExAAOhsINohQKC0wJej+/Yres/v5BTrQ8BRnfSjjY4EBICOhqEVAgRGIfCvcYoWAs7r5DTXiD7eFXV0J/1ooxMBAaCTQWiDAIFRCZwTp2kh4LudnKqFgHdEPayTfrTRgYAA0MEQtECAwCgFvhqnWhv1f52croWAt0c9opN+tLFgAQFgwQNwewIERi1wbpxubdR3Ojll+9/8t0Ud00k/2liggACwQHy3JkBgEgL/FqdcG/XtTk7b/nf/hKhjO+lHGwsSEAAWBO+2BAhMSuDf47Rro/63k1OviT7eEvWoKNdEBQSAiQ7esQkQmLvAf8QdD4r61tzvvPkbthDw5qhHb/7NHh27gAAw9gk7HwECPQn8VzSzNuqbnTTVQsCboh7bST/amKOAADBHbLciQIBACHwtam3Uf0f1ch0fjTy+l2b0MR8BAWA+zu5CgACBpQJfj1fWRn0jqpfrjdHIE3tpRh+zFxAAZm/sDgQIENicwDfiwbVRLQz0cr0hGnlyL83oY7YCAsBsfe1OgACBrQm0LwOsjWpfFujlen008tRemtHH7AQEgNnZ2pkAAQLLEfhmPKn9dED7BsFertdFI0/vpRl9zEZAAJiNq10JECCwEoH/iSe3ENB+VLCX6y+jkWf00ow+hhcQAIY3tSMBAgRWI9B+SdDaqPZLg3q5/iIaeWYvzehjWAEBYFhPuxEgQCAj0H5d8Nqo9uuDe7n+PBp5di/N6GM4AQFgOEs7ESBAYAiB9oeD1ka1PyTUy/XKaOSPe2lGH8MICADDONqFAAECQwq0PyG8Nqr9SeFerldEI8/rpRl95AUEgLyhHQgQIDALge/GpgdHnTOLzVe558ti3QtWudayzgQEgM4Goh0CBAgsETgvXm4h4CtLHlv0i8dFAy9adBPunxcQAPKGdiBAgMAsBc6Pze8V9eVZ3mSFe78knv/iqDUrXOfpHQkIAB0NQysECBDYgsD34vEWAr60hbcv4uE/jZu2ICAELEJ/gHsKAAMg2oIAAQJzEPh+3OOQqC/O4V7LvcUL44ntSwJCwHLFOnqeANDRMLRCgACBbQisDwFnb+N583zz8+NmL48SAuapPsC9BIABEG1BgACBOQr8IO51aNTn53jPbd3qufGE9mOCQsC2pDp6uwDQ0TC0QoAAgWUK/DCe10LA55b5/Hk87Tlxk/YLg4SAeWgPcA8BYABEWxAgQGABAj+Ke7YQ8NkF3HtLt3xWvOHVUULAloQ6elwA6GgYWiFAgMAKBX4czz8s6tMrXDfLp7e/IPiaKCFglsoD7C0ADIBoCwIECCxQ4Cdx78OjPrXAHja+9dPjgddGCQEby3T0ugDQ0TC0QoAAgVUKrA8Bn1zl+lkse1ps+rooIWAWugPsKQAMgGgLAgQIdCBwQfRwn6hPdNDL+haeEi+8PsrHmvUiHf1rKB0NQysECBBICvw01t836qzkPkMuf1Js9tdRPt4MqTrAXgYyAKItCBAg0JFACwH3izqzo56eEL28McrHnI6GYhgdDUMrBAgQGEjgZ7HPEVFnDLTfENs8NjY5PsrHnSE0B9jDIAZAtAUBAgQ6FLgwerp/1Okd9fbo6OVNUdfoqKfJtiIATHb0Dk6AwAQEWgg4MuqjHZ31UdHLm6OEgAUPRQBY8ADcngABAjMW+Hns/4CoU2d8n5Vsf2w8+S1RQsBK1AZ+rgAwMKjtCBAg0KHARdHTA6NO6ai3Y6KXE6K276inSbUiAExq3A5LgMCEBdaHgA93ZPCI6OVtUULAAoYiACwA3S0JECCwIIGL474Pjvrggu6/uds+LB58e5QQsDmdGT4mAMwQ19YECBDoUKCFgKOiTu6ot6Ojl3dE7dBRT6NvRQAY/YgdkAABApsIXBKPtA+6J23ylsU98JC49TujhIA5zUAAmBO02xAgQKAzgRYCHhr13o76al+eODFqx456Gm0rAsBoR+tgBAgQ2KbAL+IZD496zzafOb8ntJ9WeHeUEDBjcwFgxsC2J0CAQOcCLQS078ZvH3R7udrvLWihZKdeGhpjHwLAGKfqTAQIEFiZQAsBj4x618qWzfTZ7dcYty9PCAEzYhYAZgRrWwIECBQTuDT6/d2o9t34vVztrxq+L2rnXhoaUx8CwJim6SwECBDICbQQ0H5N79/nthl09X1jt/bTCkLAoKz+LOPAnLYjQIBAeYHL4gSPijqho5McHr2031uwS0c9lW/FZwDKj9ABCBAgMLhACwGPjnrL4DuvfsPDYukHoq65+i2sXCogACzV8DIBAgQIrBdoIeAxUe1P9/ZyHRKNtF9jLAQMMBEBYABEWxAgQGCkApfHuR4X9bcdne9e0cuHonbtqKeSrQgAJcemaQIECMxNoIWAJ0T9zdzuuO0bHRxPaX/VUAjYttUWnyEAbJHGGwgQIEDgSoEWAp4U9YaORA6KXk6J2q2jnkq1IgCUGpdmCRAgsDCBFgKeEvX6hXWw6Y0PjIdaCNh90zd5ZFsCAsC2hLydAAECBNYLXBEvPC3qdesf6ODfA6KHU6Ou1UEvpVoQAEqNS7MECBBYuEALAU+Peu3CO7m6gXvGiy0E7HH1Q17aloAAsC0hbydAgACBjQVaCHhG1Gs2fsMCX98/7n1alBCwzCEIAMuE8jQCBAgQ2ECghYBnRr16g0cX+8rd4/Yfjbr2YtuocXcBoMacdEmAAIEeBVoIeHbUKztqbr/o5WNR1+mopy5bEQC6HIumCBAgUEaghYDnRr2io47vGr2cHnXdjnrqrhUBoLuRaIgAAQLlBFoIeF7Uyzvq/M7RixCwlYEIAFvB8SYCBAgQWLZACwEviDpu2Stm/8Q7xS0+HnW92d+q3h0EgHoz0zEBAgR6FWgh4EVRL+mowTtGL2dE7dlRT120IgB0MQZNECBAYFQCL47TtOrl+s1opIWA6/fSUA99CAA9TEEPBAgQGJ9A+yzACzs61u2jlxYC9uqop4W2IgAslN/NCRAgMGqBl8bpnt/RCW8XvZwZtXdHPS2sFQFgYfRuTIAAgUkItJ8M+JOOTrpv9NJCwA066mkhrQgAC2F3UwIECExKoP2OgOd0dOLbRi8tBNywo57m3ooAMHdyNyRAgMAkBV4Vp35WRye/TfSyLupGHfU011YEgLlyuxkBAgQmLdD+bsAfdSRwq+jlrKgbd9TT3FoRAOZG7UYECBAgEALtLwj+YUcSt4xezo66aUc9zaUVAWAuzG5CgAABAksEXhsvP23J64t+sf1UwLlR7TcHTuYSACYzagclQIBAVwJ/Fd08taOOdolePht1j456mmkr2890d5sTIEBgPgK7xW1auWoJvDXabR942zcI9nDtEE207wnYJ+qbPTQ0yx4EgFnq2psAgXkJ3DpudMG8buY+oxbYMU73yajRf0+ALwGM+v3Y4QgQIEBgFQI3iTXtzxuP+hIARj1ehyNAgACBVQr09NsLV3mErS8TALbu460ECBAgME2B9j0lDxnz0QWAMU/X2QgQIEAgI/DkzOLe1woAvU9IfwQIECCwKIH2J4RHewkAox2tgxEgQIBAUmCP5PqulwsAXY9HcwQIECCwQIFR/6i8ALDA9yy3JjARgUsnck7HJFBKQAAoNS7NEigpcF7JrjVNYLvtrhgzggAw5uk6G4E+BL4abYz6f0j7YNbFDARG/X4rAMzgPcaWBAhsIPCjeO0zGzziFQI1BC6v0ebquhQAVudmFQECKxN488qe7tkEuhC4rIsuZtSEADAjWNsSILCBwInx2oUbPOIVAgQWKiAALJTfzQlMRqD9pT6fBZjMuB20goAAUGFKeiQwDoEXxTHOH8dRnIJAfQEBoP4MnYBAFYEfRqPPrtKsPgmMXUAAGPuEnY9AXwInRDun9tWSbghMU0AAmObcnZrAogTaz1U/LOqcRTXgvgQI/FJAAPCeQIDAvAV+HDc8IspvCJy3vPsRWCIgACzB8CIBAnMT+Ebc6dCob8/tjm5EgMAGAgLABhxeIUBgjgJfjnvdI+orc7ynWxEgcKWAAOBdgQCBRQp8K25+YNT7F9mEexOYooAAMMWpOzOBvgTa9wQ8KOrBUb4k0NdsdDNiAQFgxMN1NAKFBNpPB7wvat+oP4sSBALBRWCWAmtmuXkHe18UPew04z5uG/ufO+N72J7A1AS2jwPfL+qYqP2ibhI19v+9iiO6OhO4OPrZubOeBmtn7P9BCQCDvavYiMBCBVogaOUisF7g1vHCl9a/MqN/Rx0A/Ac1o/ca2xIgMKjApbFbKxeB9QLtg7MrIeB7ABJ4lhIgQIAAgaoCAkDVyembAAECBAgkBASABJ6lBAgQIECgqoAAUHVy+iZAgAABAgkBASCBZykBAgQIEKgqIABUnZy+CRAgQIBAQkAASOBZSoAAAQIEqgoIAFUnp28CBAgQIJAQEAASeJYSIECAAIGqAgJA1cnpmwABAgQIJAQEgASepQQIECBAoKqAAFB1cvomQIAAAQIJAQEggWcpAQIECBCoKiAAVJ2cvgkQIECAQEJAAEjgWUqAAAECBKoKCABVJ6dvAgQIECCQEBAAEniWEiBAgACBqgICQNXJ6ZsAAQIECCQEBIAEnqUECBAgQKCqgABQdXL6JkCAAAECCQEBIIFnKQECBAgQqCogAFSdnL4JECBAgEBCQABI4FlKgAABAgSqCggAVSenbwIECBAgkBAQABJ4lhIgQIAAgaoCAkDVyembAAECBAgkBASABJ6lBAgQIECgqoAAUHVy+iZAgAABAgkBASCBZykBAgQIEKgqIABUnZy+CRAgQIBAQkAASOBZSoAAAQIEqgoIAFUnp28CBAgQIJAQEAASeJYSIECAAIGqAgJA1cnpmwABAgQIJAQEgASepQQIECBAoKqAAFB1cvomQIAAAQIJAQEggWcpAQIECBCoKiAAVJ2cvgkQIECAQEJAAEjgWUqAAAECBKoKCABVJ6dvAgQIECCQEBAAEniWEiBAgACBqgICQNXJ6ZsAAQIECCQEBIAEnqUECBAgQKCqgABQdXL6JkCAAAECCQEBIIFnKQECBAgQqCogAFSdnL4JECBAgEBCQABI4FlKgAABAgSqCggAVSenbwIECBAgkBAQABJ4lhIgQIAAgaoCAkDVyembAAECBAgkBASABJ6lBAgQIECgqoAAUHVy+iZAgAABAgkBASCBZykBAgQIEKgqIABUnZy+CRAgQIBAQkAASOBZSoAAAQIEqgoIAFUnp28CBAgQIJAQEAASeJYSIECAAIGqAgJA1cnpmwABAgQIJAQEgASepQQIECBAoKqAAFB1cvomQIAAAQIJAQEggWcpAQIECBCoKiAAVJ2cvgkQIECAQEJAAEjgWUqAAAECBKoKCABVJ6dvAgQIECCQEBAAEniWEiBAgACBqgICQNXJ6ZsAAQIECCQEBIAEnqUECBAgQKCqgABQdXL6JkCAAAECCQEBIIFnKQECBAgQqCogAFSdnL4JECBAgEBCQABI4FlKgAABAgSqCggAVSenbwIECBAgkBAQABJ4lhIgQIAAgaoCAkDVyembAAECBAgkBASABJ6lBAgQIECgqoAAUHVy+iZAgAABAgkBASCBZykBAgQIEKgqIABUnZy+CRAgQIBAQkAASOBZSoAAAQIEqgoIAFUnp28CBAgQIJAQEAASeJYSIECAAIGqAgJA1cnpmwABAgQIJAQEgASepQQIECBAoKqAAFB1cvomQIAAAQIJAQEggWcpAQIECBCoKiAAVJ2cvgkQIECAQEJAAEjgWUqAAAECBKoKCABVJ6dvAgQIECCQEBAAEniWEiBAgACBqgICQNXJ6ZsAAQIECCQEBIAEnqUECBAgQKCqgABQdXL6JkCAAAECCQEBIIFnKQECBAgQqCogAFSdnL4JECBAgEBCQABI4FlKgAABAgSqCggAVSenbwIECBAgkBAQABJ4lhIgQIAAgaoCAkDVyembAAECBAgkBASABJ6lBAgQIECgqoAAUHVy+iZAgAABAgkBASCBZykBAgQIEKgqIABUnZy+CRAgQIBAQkAASOBZSoAAAQIEqgoIAFUnp28CBAgQIJAQEAASeJYSIECAAIGqAgJA1cnpmwABAgQIJAQEgASepQQIECBAoKqAAFB1cvomQIAAAQIJAQEggWcpAQIECBCoKiAAVJ2cvgkQIECAQEJAAEjgWUqAAAECBKoKCABVJ6dvAgQIECCQEBAAEniWEiBAgACBqgICQNXJ6ZsAAQIECCQEBIAEnqUECBAgQKCqgABQdXL6JkCAAAECCQEBIIFnKQECBAgQqCogAFSdnL4JECBAgEBCQABI4FlKgAABAgSqCggAVSenbwIECBAgkBAQABJ4lhIgQIAAgaoCAkDVyembAAECBAgkBASABJ6lBAgQIECgqoAAUHVy+iZAgAABAgkBASCBZykBAgQIEKgqIABUnZy+CRAgQIBAQkAASOBZSoAAAQIEqgoIAFUnp28CBAgQIJAQEAASeJYSIECAAIGqAgJA1cnpmwABAgQIJAQEgASepQQIECBAoKqAAFB1cvomQIAAAQIJAQEggWcpAQIECBCoKiAAVJ2cvgkQIECAQEJAAEjgWUqAAAECBKoKCABVJ6dvAgQIECCQEBAAEniWEiBAgACBqgICQNXJ6ZsAAQIECCQEBIAEnqUECBAgQKCqgABQdXL6JkCAAAECCQEBIIFnKQECBAgQqCogAFSdnL4JECBAgEBCQABI4FlKgAABAgSqCggAVSenbwIECBAgkBAQABJ4lhIgQIAAgaoCAkDVyembAAECBAgkBASABJ6lBAgQIECgqoAAUHVy+iZAgAABAgkBASCBZykBAgQIEKgqIABUnZy+CRAgQIBAQkAASOBZSoAAAQIEqgoIAFUnp28CBAgQIJAQEAASeJYSIECAAIGqAgJA1cnpmwABAgQIJAQEgASepQQIECBAoKqAAFB1cvomQIAAAQIJAQEggWcpAQIECBCoKiAAVJ2cvgkQIECAQEJAAEjgWUqAAAECBKoKCABVJ6dvAgQIECCQEBAAEniWEiBAgACBqgICQNXJ6ZsAAQIECCQEBIAEnqUECBAgQKCqgABQdXL6JkCAAAECCQEBIIFnKQECBAgQqCogAFSdnL4JECBAgEBCQABI4FlKgAABAgSqCggAVSenbwIECBAgkBAQABJ4lhIgQIAAgaoCAkDVyembAAECBAgkBASABJ6lBAgQIECgqoAAUHVy+iZAgAABAgkBASCBZykBAgQIEKgqIABUnZy+CRAgQIBAQkAASOBZSoAAAQIEqgoIAFUnp28CBAgQIJAQEAASeJYSIECAAIGqAgJA1cnpmwABAgQIJAQEgASepQQIECBAoKqAAFB1cvomQIAAAQIJAQEggWcpAQIECBCoKiAAVJ2cvgkQIECAQEJAAEjgWUqAAAECBKoKCABVJ6dvAgQIECCQEBAAEniWEiBAgACBqgICQNXJ6ZsAAQIECCQEBIAEnqUECBAgQKCqgABQdXL6JkCAAAECCQEBIIFnKQECBAgQqCogAFSdnL4JECBAgEBCQABI4FlKgAABAgSqCggAVSenbwIECBAgkBAQABJ4lhIgQIAAgaoCAkDVyembAAECBAgkBASABJ6lBAgQIECgqoAAUHVy+iZAgAABAgkBASCBZykBAgQIEKgqIABUnZy+CRAgQIBAQkAASOBZSoAAAQIEqgoIAFUnp28CBAgQIJAQEAASeJYSIECAAIGqAgJA1cnpmwABAgQIJAQEgASepQQIECBAoKqAAFB1cvomQIAAAQIJAQEggWcpAQIECBCoKiAAVJ2cvgkQIECAQEJAAEjgWUqAAAECBKoKCABVJ6dvAgQIECCQEBAAEniWEiBAgACBqgICQNXJ6ZsAAQIECCQEBIAEnqUECBAgQKCqgABQdXL6JkCAAAECCQEBIIFnKQECBAgQqCogAFSdnL4JECBAgEBCQABI4FlKgAABAgSqCggAVSenbwIECBAgkBAQABJ4lhIgQIAAgaoCAkDVyembAAECBAgkBASABJ6lBAgQIECgqoAAUHVy+iZAgAABAgkBASCBZykBAgQIEKgqIABUnZy+CRAgQIBAQkAASOBZSoAAAQIEqgoIAFUnp28CBAgQIJAQEAASeJYSIECAAIGqAgJA1cnpmwABAgQIJAQEgASepQQIECBAoKqAAFB1cvomQIAAAQIJAQEggWcpAQIECBCoKiAAVJ2cvgkQIECAQEJAAEjgWUqAAAECBKoKCABVJ6dvAgQIECCQEBAAEniWEiBAgACBqgICQNXJ6ZsAAQIECCQEBIAEnqUECBAgQKCqgABQdXL6JkCAAAECCQEBIIFnKQECBAgQqCogAFSdnL4JECBAgEBCQABI4FlKgAABAgSqCggAVSenbwIECBAgkBAQABJ4lhIgQIAAgaoCAkDVyembAAECBAgkBASABJ6lBAgQIECgqoAAUHVy+iZAgAABAgkBASCBZykBAgQIEKgqIABUnZy+CRAgQIBAQkAASOBZSoAAAQIEqgoIAFUnp28CBAgQIJAQEAASeJYSIECAAIGqAgJA1cnpmwABAgQIJAQEgASepQQIECBAoKqAAFB1cvomQIAAAQIJAQEggWcpAQIECBCoKiAAVJ2cvgkQIECAQEJAAEjgWUqAAAECBKoKCABVJ6dvAgQIECCQEBAAEniWEiBAgACBqgICQNXJ6ZsAAQIECCQEBIAEnqUECBAgQKCqgABQdXL6JkCAAAECCQEBIIFnKQECBAgQqCogAFSdnL4JECBAgEBCQABI4FlKgAABAgSqCggAVSenbwIECBAgkBAQABJ4lhIgQIAAgaoCAkDVyembAAECBAgkBASABJ6lBAgQIECgqoAAUHVy+iZAgAABAgkBASCBZykBAgQIEKgqIABUnZy+CRAgQIBAQkAASOBZSoAAAQIEqgoIAFUnp28CBAgQIJAQEAASeJYSIECAAIGqAgJA1cnpmwABAgQIJAQEgASepQQIECBAoKqAAFB1cvomQIAAAQIJAQEggWcpAQIECBCoKiAAVJ2cvgkQIECAQEJAAEjgWUqAAAECBKoKCABVJ6dvAgQIECCQEBAAEniWEiBAgACBqgICQNXJ6ZsAAQIECCQEBIAEnqUECBAgQKCqgABQdXL6JkCAAAECCQEBIIFnKQECBAgQqCogAFSdnL4JECBAgEBCQABI4FlKgAABAgSqCggAVSenbwIECBAgkBAQABJ4lhIgQIAAgaoCAkDVyembAAECBAgkBASABJ6lBAgQIECgqoAAUHVy+iZAgAABAgkBASCBZykBAgQIEKgqIABUnZy+CRAgQIBAQkAASOBZSoAAAQIEqgoIAFUnp28CBAgQIJAQEAASeJYSIECAAIGqAgJA1cnpmwABAgQIJAQEgASepQQIECBAoKqAAFB1cvomQIAAAQIJAQEggWcpAQIECBCoKiAAVJ2cvgkQIECAQEJAAEjgWUqAAAECBKoKCABVJ6dvAgQIECCQEBAAEniWEiBAgACBqgICQNXJ6ZsAAQIECCQEBIAEnqUECBAgQKCqgABQdXL6JkCAAAECCYHtE2st/aXAAfHPnjAIECBAYK4Ct5nr3UZ4szUjPNPSI10Ur+y09AEvEyBAgACBZQpcHM/beZnPLfc0XwIoNzINEyBAgACBvIAAkDe0AwECBAgQKCcgAJQbmYYJECBAgEBeQADIG9qBAAECBAiUExAAyo1MwwQIECBAIC8gAOQN7UCAAAECBMoJCADlRqZhAgQIECCQFxAA8oZ2IECAAAEC5QQEgHIj0zABAgQIEMgLCAB5QzsQIECAAIFyAgJAuZFpmAABAgQI5AUEgLyhHQgQIECAQDmBsQeAS8pNRMMECBAg0IvAqD+GjD0AnNfLe5E+CBAgQKCcwHfLdbyChsceAM5ZgYWnEiBAgACBpQJfXfrK2F4eewD4xNgG5jwECBAgMDeBs+Z2pwXcaM0C7jnPW948bva1ed7QvQgQIEBgNAK3iJN8fTSn2eggY/8MQBvcRzY6s1cJECBAgMC2BNrHjtF+8G+HH/tnANoZ7xz1+YmctZ3XRYAAAQI5gSti+V2jvpDbpu/V1+i7vUG6+07sslvUPQfZzSYECBAgMHaBV8cBTxj7IafwGYA2wx2jTo06uL3iIkCAAAECWxA4Mx4/PGrUvwOgnX3s3wPQztiuNsgHRJ3VXnERIECAAIHNCLSPEe1jxeg/+LezT+FLAO2c7WoDfWfUHlF3j3IRIECAAIH1Aq+LF46NunD9A2P/d0oBoM3ysqhTok6LunlU+xEPFwECBAhMV+D0OPojo46Pah8jJnOtmcxJN3/QfeLhI6P2j2qBYPeoqXxZJI7qIkCAwKQELo/TXhDVfrzv01EnR/1n1CSv/wf5eiNBzx3l8gAAAABJRU5ErkJggg==";
        $imagefail = false;
        if(isset($atts['defaultimage']) && trim($atts['defaultimage']) != "") {
            if(is_file($atts['defaultimage'])) {
                $defaultimage = $atts['defaultimage'];
            } else {
                $imagefail = true;
            }
        }

        $latest = null;
        if(isset($atts['latest']) && trim($atts['latest']) != "" && is_numeric($atts['latest'])) {
            $latest = $atts['latest'];
        }
        
        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

        if($view != null && $uploadfieldid != null && !$imagefail) {

            $usermodus = false;
            $uid = get_current_user_id();
            $selectui = '' . get_user_meta(wp_get_current_user()->ID, 'coach_select_user', true);
            if ($selectui != '') {
                $uid = $selectui;
                $usermodus = true;
            }

            $includeFields = array();
            $includeFields[] = $uploadfieldid;
            if ($projectfieldid != null) { $includeFields[] = $projectfieldid; }
            if ($titlefieldid != null) { $includeFields[] = $titlefieldid; }
            if ($descfieldid != null) { $includeFields[] = $descfieldid; }

            $userids = null;
            if($view == "boss") {
                $uids = Trainingssystem_Plugin_Database::getInstance()->CompanygroupUserDao->getUserIdsByBossId($uid);
                if($uids == null) {
                    return $twig->render("forms-media-grid/media-grid-noboss.html");
                }
                $userids = $uids;
            } else if($view == "kAdmin"){
                $kAdminUsers = Trainingssystem_Plugin_Database::getInstance()->CompanyDao->getAllUserIdsByCreaterId($uid);
                
                if($kAdminUsers == null){
                    return $twig->render("forms-media-grid/media-grid-nokadmin.html");
                }
                $userids = $kAdminUsers;
            } else if($view == "all") {
                $userids = null;
            } else if($view == "my") {
                $userids = $uid;
            }

            $rawdata = Trainingssystem_Plugin_Database::getInstance()->Formdata->getMediaGridData($userids, $includeFields, $latest);
            
            $i = 0;
            $data = array();
            $data[$i] = array();

            if($projectfieldid == null) {
                $data[$i]['title'] = "";
                $data[$i]['id'] = bin2hex(random_bytes(32));
                $data[$i]['userid'] = "";
                $data[$i]['data'] = $rawdata;
            } else {
                foreach($rawdata as $savedate => $savepoint) {
                    foreach($savepoint as $fieldid => $value) {
                        if($fieldid == $projectfieldid) {
                            $found = false;
                            foreach($data as $index => $d) {
                                if(isset($d['title']) && trim($d['title']) == $value->getData() &&
                                    isset($d['userid']) && $d['userid'] == $value->getUserId()) {
                                    $data[$index]['data'][$savedate] = $savepoint;
                                    $found = true;
                                }
                            }
                            if(!$found) {
                                $data[$i]['title'] = $value->getData();
                                $data[$i]['id'] = bin2hex(random_bytes(32));
                                $data[$i]['userid'] = $value->getUserId();
                                $data[$i]['data'] = array();
                                $data[$i]['data'][$savedate] = $savepoint;
                                $i++;
                            }
                        }
                    }
                }
            }

            $usernames = array();
            if($view != "my") {
                $blogusers = get_users(['orderby' => 'display_name', 'order' => 'ASC']);
                foreach($blogusers as $user) {
                    $usernames[$user->ID] = $user->display_name;
                }
            } else {
                $user_info = get_userdata($uid);
                $usernames[$uid] = $user_info->display_name;
            }

            $weekdays = ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"];
            
            return $twig->render("forms-media-grid/media-grid.html", ["rand" => bin2hex(random_bytes(16)),
                                                                        "latest" => $latest,
                                                                        "defaultimage"=> $defaultimage,
                                                                        "data" => $data,
                                                                        "usernames" => $usernames,
                                                                        "weekdays" => $weekdays,
                                                                        "uploadfieldid" => $uploadfieldid,
                                                                        "projectfieldid" => $projectfieldid,
                                                                        "titlefieldid" => $titlefieldid,
                                                                        "descfieldid" => $descfieldid,
                                                                        "view" => $view,
                                                                        "user_is_admin" => Trainingssystem_Plugin_Module_Berechtigung::getInstance()->userHasRole("admin"),
                                                                    ]);
        } else {
            return $twig->render("forms-media-grid/media-grid-error.html");
        }
    }

    /**
     * AJAX
     * 
     * Downloads a File
     * 
     * @return FileStream
     */
    public function downloadFile() {
        if(isset($_POST['fileid']) && trim($_POST['fileid']) != "" && is_numeric($_POST['fileid']) && get_post_type($_POST['fileid']) === "attachment") {

            $fileid = $_POST['fileid'];
            $filepost = wp_get_attachment_url($fileid);

            if(!$filepost) {
                echo "404";
                wp_die();
            }

            $file_url  = stripslashes(trim($filepost));
            $file_name = basename($filepost);

            header("Content-Disposition:attachment; filename={$file_name}");
            header("Content-Type: application/force-download");

            readfile("{$file_url}");

        } else {
            echo "0";
        }
        wp_die();
    }

    /**
     * AJAX
     * 
     * Returns Details of a file
     * 
     * @return HTML-Code
     */
    public function getDetails() {
        if(isset($_POST['fileid']) && trim($_POST['fileid']) != "" && is_numeric($_POST['fileid']) && get_post_type($_POST['fileid']) === "attachment") {

            $fileid = $_POST['fileid'];
            $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

            $fileid = $_POST['fileid'];
            $path = wp_get_attachment_url($fileid);
            $filename = basename($path);
            $pathinfo = pathinfo($path);
            
            $type = "other";
            $mediatype = "";
            switch(strtolower($pathinfo['extension'])) {
                case "mp3":
                    $type = "audio";
                    $mediatype = "audio/mpeg";
                    break;
                case "m4a":
                    $type = "audio";
                    $mediatype = "audio/m4a";
                case "wav":
                    $type = "audio";
                    $mediatype = "audio/wav";
                    break;

                case "png":
                case "jpg":
                case "jpeg":
                    $type = "image";
                    break;

                case "mp4":
                    $type = "video";
                    $mediatype = "video/mp4";
                    break;
            }

            echo $twig->render("forms-media-grid/media-grid-details.html", ["type" => $type, "mediatype" => $mediatype, "path" => $path, "filename" => $filename]);

        } else {
            echo "0";
        }
        wp_die();
    }

    /**
     * AJAX
     * 
     * Deletes a file and the corresponding FormData
     * 
     * @return 0 on failure/ 1 on success
     */
    public function deleteFile() {
        if(isset($_POST['fileid']) && is_numeric($_POST['fileid']) && isset($_POST['dataid']) && is_numeric($_POST['dataid'])) {

            $fileid = sanitize_text_field($_POST['fileid']);
            $dataid = sanitize_text_field($_POST['dataid']);
            
            $formdata = Trainingssystem_Plugin_Database::getInstance()->Formdata->getFormData($dataid);

            $usermodus = false;
            $uid = get_current_user_id();
            $selectui = '' . get_user_meta(wp_get_current_user()->ID, 'coach_select_user', true);
            if ($selectui != '') {
                $uid = $selectui;
                $usermodus = true;
            }

            if($formdata != null && !$usermodus && strcmp($formdata->getData(), $fileid) === 0 && 
                ($formdata->getUserId() == $uid || Trainingssystem_Plugin_Module_Berechtigung::getInstance()->userHasRole("admin"))) {

                if(Trainingssystem_Plugin_Database::getInstance()->Formdata->deleteSavepoint($formdata->getUserId(), $formdata->getSaveDate()) &&       
                        wp_delete_attachment($fileid, true)) {
                    echo "1";
                } else {
                    echo "2";
                }

            } else {
                echo "403";
            }

        } else {
            echo "0";
        }
        wp_die();
    }

    /**
     * AJAX
     * 
     * Deletes a project and the corresponding files and FormDatas
     * 
     * @return 0 on failure/ 1 on success
     */
    public function deleteProject() {
        if(isset($_POST['projectname']) && 
            isset($_POST['uploadfieldid']) && is_numeric($_POST['uploadfieldid']) && 
            isset($_POST['projectfieldid'])) {

            $projectname = trim($_POST['projectname']) != "" ? sanitize_text_field($_POST['projectname']) : "";
            $uploadfieldid = sanitize_text_field($_POST['uploadfieldid']);
            $projectfieldid = trim($_POST['projectfieldid']) != "" && is_numeric($_POST['projectfieldid']) ? sanitize_text_field($_POST['projectfieldid']) : '';

            $usermodus = false;
            $uid = get_current_user_id();
            $selectui = '' . get_user_meta(wp_get_current_user()->ID, 'coach_select_user', true);
            if ($selectui != '') {
                $uid = $selectui;
                $usermodus = true;
            }

            if(!$usermodus) {
                $formdata = Trainingssystem_Plugin_Database::getInstance()->Formdata->getMediaGridDataByProjectname($uid, $projectname, $projectfieldid, $uploadfieldid);

                $failure = false;
                
                foreach($formdata as $fd) {
                    if(($fd->getUserId() == $uid || Trainingssystem_Plugin_Module_Berechtigung::getInstance()->userHasRole("admin"))) {
                        if($fd->getFieldId() == $uploadfieldid) {
                            if(!wp_delete_attachment($fd->getData(), true)) {
                                $failure = true;
                            }
                        }
                        if(!Trainingssystem_Plugin_Database::getInstance()->Formdata->deleteFormData($fd->getDataId())) {
                            $failure = true;
                        }
                    } else {
                        echo "403";
                        return;
                    }
                }

                if($failure) {
                    echo "0";
                } else {
                    echo "1";
                }

            } else {
                echo "401";
            }

        } else {
            echo "0";
        }
        wp_die();
    }
}
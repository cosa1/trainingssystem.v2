<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 */
class Trainingssystem_Plugin_Module_User_Meta_Fields
{

    public function __construct()
    {

    }

     /** user metadata extended by field studienid.
     * @param $user
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function coach_usermeta_form_field_studienid($user)
    {
        $studienid = $user != "add-new-user" ? esc_attr(get_user_meta($user->ID, 'studienid', true)) : "";
        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
        echo $twig->render('user-meta-fields/user-meta-fields-studienid.html', ["studienid" => $studienid]);


    }

    public function coach_usermeta_form_field_studienid_update($user_id)
    {
        if (!current_user_can('edit_user', $user_id)) {
            return false;
        }
        $studienid = isset($_POST['studienid']) ? sanitize_text_field(trim($_POST['studienid'])) : '';

        return update_user_meta($user_id, 'studienid', $studienid);
    }
    
    // user metadata extended by field coach id.
    public function coach_usermeta_form_field_coachid($user)
    {


        $editorusers = Trainingssystem_Plugin_Database::getInstance()->NutzerDao->getTrainer();

        $saveduserid = $user != "add-new-user" ? get_user_meta($user->ID, 'coachid', true) : '';
        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
        echo $twig->render('user-meta-fields/user-meta-fields-coach.html', ["saveduserid" => $saveduserid, "users" => $editorusers]);

    }

    public function coach_usermeta_form_field_coachid_update($user_id)
    {
        if (!current_user_can('edit_user', $user_id)) {
            return false;
        }
        if(isset($_POST['coachid'])){
            if(trim($_POST['coachid']) == "") {
                $coachid = "";
            } else {
                $coachid=intval(sanitize_text_field($_POST['coachid']));
            }
            return update_user_meta($user_id, 'coachid', $coachid);
        }
        return false;
       
    }

    // user metadata extended by field age.
    public function extendUserMetaAge_Update($user_id){
        if (!current_user_can('edit_user', $user_id)) {
            return false;
        }

        $age = isset($_POST['age']) ? sanitize_text_field(trim($_POST['age'])) : '';

        return  update_user_meta( $user_id, 'age', $age);
    }

    public function extendUserMetaAge($user){
        $age = $user != "add-new-user" ? esc_attr(get_user_meta($user->ID, 'age', true)) : "";
        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
        echo $twig->render('user-meta-fields/user-meta-fields-age.html', ["age" => $age]);

    }

    public function extendUserMetaGender_Update($user_id){

        if (!current_user_can('edit_user', $user_id)) {
            return false;
        }
        
        $gender = isset($_POST['gender']) ? sanitize_text_field(trim($_POST['gender'])) : '';

        return update_user_meta( $user_id, 'gender', $gender);
    }
        
    // user metadata extended by field gender
    public function extendUserMetaGender($user){
        $gender = $user != "add-new-user" ? esc_attr(get_user_meta($user->ID, 'gender', true)) : "";
        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
        echo $twig->render('user-meta-fields/user-meta-fields-gender.html', ["gender" => $gender]);

    }

    public function extendUserMetaPlace_Update($user_id){
        if (!current_user_can('edit_user', $user_id)) {
            return false;
        }
        $place = isset($_POST['place']) ? sanitize_text_field(trim($_POST['place'])) : '';

        return  $update = update_user_meta( $user_id, 'place', $place);
    }

    //user metadata extended by field place.
    public function extendUserMetaPlace($user){

        $place = $user != "add-new-user" ? esc_attr(get_user_meta($user->ID, 'place', true)) : "";
        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
        echo $twig->render('user-meta-fields/user-meta-fields-place.html', ["place" => $place]);

    }

}

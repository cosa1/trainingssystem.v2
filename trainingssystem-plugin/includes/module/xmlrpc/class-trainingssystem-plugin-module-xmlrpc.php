<?php
class Trainingssystem_Plugin_Module_XmlRpc
{

    public function __construct()
    {
    }

    public function toggleXmlRpc() {
        $xmlrpc_activated = false;
        if (isset(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)['xmlrpc_activated']) && get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)['xmlrpc_activated']) {
            $xmlrpc_activated = true;
        }
        
        if (!$xmlrpc_activated) {
            add_filter( 'xmlrpc_enabled', '__return_false' );
            if ( defined( 'XMLRPC_REQUEST' ) && XMLRPC_REQUEST ) {
			    exit;
            }
        } else {

        }
    }
}

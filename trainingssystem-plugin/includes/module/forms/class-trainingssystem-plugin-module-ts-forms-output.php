<?php
/**
 * The functionality of the module - section output.
 *
 * @package    TS Forms
 * @author     Max Sternitzke <max.sternitzke@th-luebeck.de>
 */
class Trainingssystem_Plugin_Module_TS_Forms_Output {
    /**
     * output custom db data shortcode
     *
     * @since    1.0.0
     */
    public function coach_output_format_data($atts)
    {
        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

        $id = $atts['id'];
        if(strtolower(get_post_type($id)) != "formulare") {
            return $twig->render("forms/form-error-notsform.html");
        }

        $uid = get_current_user_id();
        $selectui = '' . get_user_meta(wp_get_current_user()->ID, 'coach_select_user', true);
        if ($selectui != '') {
            $uid = $selectui;
        }

        if($uid != 0) {
            
            $data = Trainingssystem_Plugin_Database::getInstance()->Formfield->getUserFormfieldsByForm($id, $uid);

            if(count($data) > 0) {
                if(!isset($atts['title'])) {
                    $title = array();
                } else {
                    $title = explode("|", $atts['title']);
                }
                if(isset($atts['title2'])) {
                    array_push($title, $atts['title2']);
                }
                if(isset($atts['title3'])){
                    array_push($title, $atts['title3']);
                }
                if(isset($atts['title4'])){
                    array_push($title, $atts['title4']);
                }

                foreach($title as $i => $t) {
                    if(trim($t) == "") {
                        unset($title[$i]);
                    }
                }

                $placeholder = null;
                if(isset($atts['placeholder']) && trim($atts['placeholder']) != "") {
                    $placeholder = $atts['placeholder'];
                }

                $mode = "table";
                // Only allow table for multiform forms
                $is_multiform = get_post_meta($id, 'isMultiform', true) == "1";
                if(!$is_multiform) {
                    if (isset($atts['text'])) {
                        $mode = "text";
                    } elseif (isset($atts['list'])) {
                        $mode = "list";
                    }
                }

                return $twig->render("forms/form-output.html", ["mode" => $mode, "title" => $title, "data" => $data, "formid" => $id, "placeholder" => $placeholder]);
            } else {
                return "";
            }
        }
    }
}
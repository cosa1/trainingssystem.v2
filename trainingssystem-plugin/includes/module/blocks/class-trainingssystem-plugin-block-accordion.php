<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 */
class Trainingssystem_Plugin_Module_Block_Accordion
{
    public function __construct()
    {

    }

    public function accordion_block()
    {
        wp_register_script('gutenberg-block-accordion-js',
        plugins_url('../../../assets/js/blocks/accordion.js', __FILE__),
        array('wp-blocks',  'wp-i18n', 'wp-element', 'wp-editor'));
        //name muss exact der gleiche sein wie in dem javascript dokument
        register_block_type('gb/trainingssystem-block-accordion', array(
           'editor_script' => 'gutenberg-block-accordion-js',
          'render_callback' => array($this, 'trainingssystem_block_accordion_render_callback'),
          'attributes' => array(
                'content' => array(
                    'type' => 'string',
                    'default' => 'kein content',
                  ),
                  'headline' => array(
                    'type' => 'string',
                    'default' => 'keine headline',
                  )      
            ),
        ));
    }

    public function trainingssystem_block_accordion_render_callback($attributes)
    {
        if (!empty($attributes)) {
            $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

            return $twig->render('gutenberg-block-test/gutenberg-block-accordion.html', [
              'headline' => $attributes[ 'headline' ],
              'text' => $attributes[ 'content' ],
          ]);
        }

        return '<div></div>';
    }

        /**
         * eigene Category für das Trainingssystem angelegt mit palmen Icon.
         */
        public function mdblock_categories($categories, $post)
        {
        return array_merge(
            $categories,
            array(
                array(
                    'slug' => 'md-trainingssystem-9872',
                    'title' => 'Trainingssystem Blöcke',
                  'icon' => 'palmtree',
                ),
            )
        );
        }
}

<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 */
class Trainingssystem_Plugin_Module_Block_Settings
{
    public function __construct()
    {
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'blocks/class-trainingssystem-plugin-block-test2.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'blocks/class-trainingssystem-plugin-block-test3.php';

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'blocks/class-trainingssystem-plugin-block-accordion.php';

    }

    /**
     * eigene Category für das Trainingssystem angelegt mit palmen Icon.
     */
    public function mdblock_categories($categories, $post)
    {

      return array_merge(
          $categories,
          array(
              array(
                  'slug' => 'md-trainingssystem-9872',
                  'title' => 'Trainingssystem Blöcke',
                'icon' => 'palmtree',
              ),
          )
      );
    }

    /**
     * weitere blöcke initialisieren
     */
    public function init_blocks($loader){
      $Trainingssystem_Plugin_Module_Block_Test2 = new Trainingssystem_Plugin_Module_Block_Test2();
      $loader->add_action( 'init', $Trainingssystem_Plugin_Module_Block_Test2, 'yourgutenberg_block');
      $Trainingssystem_Plugin_Module_Block_Test3 = new Trainingssystem_Plugin_Module_Block_Test3();
      $loader->add_action( 'init', $Trainingssystem_Plugin_Module_Block_Test3, 'test3_block');

      $Trainingssystem_Plugin_Module_Block_Accordion = new Trainingssystem_Plugin_Module_Block_Accordion();
      $loader->add_action( 'init', $Trainingssystem_Plugin_Module_Block_Accordion, 'accordion_block');
    }
}

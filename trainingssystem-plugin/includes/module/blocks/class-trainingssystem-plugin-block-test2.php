<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 */
class Trainingssystem_Plugin_Module_Block_Test2
{
    public function __construct()
    {

    }

    public function yourgutenberg_block()
    {
        $path = plugin_dir_path(dirname(__FILE__)).'/assets';

        wp_register_script('name-ofyour-script-js', plugins_url('../../../assets/js/blocks/test1.js', __FILE__), array('wp-blocks',  'wp-i18n', 'wp-element'));
        wp_register_style('nameof-mystyle', plugins_url('../../../assets/css/blocks/test1.css', __FILE__), array('wp-edit-blocks'), $path.'/css/blocks/test1.css');
        wp_register_style('nameof-mystyle-editor', plugins_url('../../../assets/css/blocks/test1.css', __FILE__), array('wp-edit-blocks'), plugins_url('../../../assets/css/blocks/test1.css', __FILE__));

        register_block_type('profile/block', array(
              'editor_script' => 'name-ofyour-script-js',
              'editor_style' => 'nameof-mystyle-editor',
              'style' => 'nameof-mystyle',
             ));

        wp_register_script('name-ofyour-script-js2', plugins_url('../../../assets/js/blocks/test2.js', __FILE__), array('wp-blocks',  'wp-i18n', 'wp-element', 'wp-editor'));

        //name muss exact der gleiche sein wie in dem javascript dokument
        register_block_type('gb/trainingssystem-block-test1', array(
           'editor_script' => 'name-ofyour-script-js2',
          // 'editor_style' => 'nameof-mystyle-editor',
          // 'style' => 'nameof-mystyle',
          'render_callback' => array($this, 'trainingssystem_block_test1_render_callback'),
          'attributes' => array(
                'content' => array(
                    'type' => 'string',
                    'default' => 'kein content',
                  ),
                'images' => array(
                    'type' => 'array',
                    'default' => [],
                      'items' => [
                          'type' => 'string', 'integer',
                      ],
                ),
            ),
        ));
    }

    public function trainingssystem_block_test1_render_callback($attributes)
    {
        if (!empty($attributes)) {
            $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

            return $twig->render('gutenberg-block-test/gutenberg-block-test1.html', [
              'images' => $attributes[ 'images' ],
              'text' => $attributes[ 'content' ],
          ]);
        }

        return '<div></div>';
    }

        /**
         * eigene Category für das Trainingssystem angelegt mit palmen Icon.
         */
        public function mdblock_categories($categories, $post)
        {
        return array_merge(
            $categories,
            array(
                array(
                    'slug' => 'md-trainingssystem-9872',
                    'title' => 'Trainingssystem Blöcke',
                  'icon' => 'palmtree',
                ),
            )
        );
        }
}

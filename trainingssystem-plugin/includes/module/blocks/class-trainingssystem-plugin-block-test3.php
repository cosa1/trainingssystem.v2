<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 */
class Trainingssystem_Plugin_Module_Block_Test3
{
    public function __construct()
    {

    }

    public function test3_block()
    {
        wp_register_script('name-ofyour-script-js3',
        plugins_url('../../../assets/js/blocks/test3.js', __FILE__),
        array('wp-blocks',  'wp-i18n', 'wp-element', 'wp-editor'));
        //name muss exact der gleiche sein wie in dem javascript dokument
        register_block_type('gb/trainingssystem-block-test3', array(
           'editor_script' => 'name-ofyour-script-js3',
          'render_callback' => array($this, 'trainingssystem_block_test3_render_callback'),
          'attributes' => array(
                'content' => array(
                    'type' => 'string',
                    'default' => 'kein content',
                  ),
                'images' => array(
                    'type' => 'array',
                    'default' => [],
                      'items' => [
                          'type' => 'string', 'integer',
                      ],
                ),
            ),
        ));
    }

    public function trainingssystem_block_test3_render_callback($attributes)
    {
        if (!empty($attributes)) {
            $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

            return $twig->render('gutenberg-block-test/gutenberg-block-test1.html', [
              'images' => $attributes[ 'images' ],
              'text' => $attributes[ 'content' ],
          ]);
        }

        return '<div></div>';
    }

        /**
         * eigene Category für das Trainingssystem angelegt mit palmen Icon.
         */
        public function mdblock_categories($categories, $post)
        {
        return array_merge(
            $categories,
            array(
                array(
                    'slug' => 'md-trainingssystem-9872',
                    'title' => 'Trainingssystem Blöcke',
                  'icon' => 'palmtree',
                ),
            )
        );
        }
}

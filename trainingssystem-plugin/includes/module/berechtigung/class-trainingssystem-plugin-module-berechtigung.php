<?php
/**
 * Verantwortliches Modul für die Berechtigungsverwaltung
 * wie Zugriffsprüfung und die Berechtigungstabelle im WP Backend
 *
 * @package    Berechtigung
 * @author     Max Sternitzke <max.sternitzke@th-luebeck.de>
 */
class Trainingssystem_Plugin_Module_Berechtigung {

	protected static $_instance = null;

	private static $version = 22;

	private static $versionSetting = "tspv2_permissions_version";

	public static function getInstance()
	 {
			 if (null === self::$_instance)
			 {
					 self::$_instance = new self;
			 }
			 return self::$_instance;
	 }

	 /**
		* clone
		*
		* Kopieren der Instanz von aussen ebenfalls verbieten
		*/
	 protected function __clone() {}

	/**
	 * Initialize the collections used to maintain the actions and filters.
	 *
	 * @since    1.0.0
	 */
	protected function __construct() {

	}

	public function getPermissionOverview() {
		global $wpdb;

		$twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

		$roles = Trainingssystem_Plugin_Database::getInstance()->PermissionRole->getAllRoles();
		$functions = Trainingssystem_Plugin_Database::getInstance()->PermissionFunction->getAllFunctions();

		$wpdb->get_results("
                    SELECT * FROM " . $wpdb->usermeta . " 
                        WHERE meta_key = 'pAdminId' or 
								meta_key = 'kAdminId' or 
								meta_key = 'dAdminId' or 
								meta_key = 'pCoachId' or 
								meta_key = 'bossId'
				");
		$oldPermissionsFound = false;
		if($wpdb->num_rows > 0) {
			$oldPermissionsFound = true;
		}

		$updateAvailable = false;
		$installedVersion = get_option(self::$versionSetting, 1); // 1 is default
		if($installedVersion < self::$version) {
			$updateAvailable = true;
		}
		
		$showdefaultreset = false;

		if(!$updateAvailable) {
			$defaultRolesExist = true;
			foreach($this->getDefaultRoles() as $role) {
				$b = false;
				foreach($roles as $r_exist) {
					if($role->getId() == $r_exist->getId()) {
						$b = true;
					}
				}
				if(!$b) {
					$defaultRolesExist = false;
					break;
				}
			}
			if($defaultRolesExist && sizeof($roles) == sizeof($this->getDefaultRoles())) {
				$defaultRolesExist = true;
			} else {
				$defaultRolesExist = false;
			}

			$defaultFunctionsExist = true;
			foreach($this->getDefaultFunctions() as $function) {
				$b = false;
				foreach($functions as $f_exist) {
					if($function->getId() == $f_exist->getId()) {
						$b = true;
					}
				}
				if(!$b) {
					$defaultFunctionsExist = false;
					break;
				}
			}
			if($defaultFunctionsExist && sizeof($functions) == sizeof($this->getDefaultFunctions())) {
				$defaultFunctionsExist = true;
			} else {
				$defaultFunctionsExist = false;
			}

			$permissionsAreDefault = true;

			$permissionsInSystem = array();
			foreach($roles as $r) {
				foreach($r->getPermissions() as $p) {
					$permissionsInSystem[] = new Trainingssystem_Plugin_Database_Permission_Role2Function(null, $p->getFunctionId(), $r->getId(), 1);
				}
			}
			foreach($permissionsInSystem as $per) {		
				$b = false;
				foreach($this->getDefaultRole2Functions() as $r2f) {
					if($per->getRoleId() == $r2f->getRoleId() && $per->getFunctionId() == $r2f->getFunctionId() && $per->getValue() == $r2f->getValue()) {
						$b = true;
					}
				}
				if(!$b) {
					$permissionsAreDefault = false;
					break;
				}
			}
			if($permissionsAreDefault && sizeof($permissionsInSystem) == sizeof($this->getDefaultRole2Functions())) {
				$permissionsAreDefault = true;
			} else {
				$permissionsAreDefault = false;
			}
			
			$showdefaultreset = !($defaultRolesExist && $defaultFunctionsExist && $permissionsAreDefault);
		} else {
			$permissionsAreDefault = false;
			$defaultFunctionsExist = false;
			$defaultRolesExist = false;
		}
		

		echo $twig->render("permissions/permission-overview.html", ["roles" => $roles, 
																	"functions" => $functions, 
																	"defaultRolesExist" => $defaultRolesExist,
																	"defaultFunctionsExist" => $defaultFunctionsExist,
																	"permissionsAreDefault" => $permissionsAreDefault,
																	"oldPermissionsFound" => $oldPermissionsFound,
																	"updateAvailable" => $updateAvailable,
																	"showDefaultReset" => $showdefaultreset,
																	"isAdmin" => Trainingssystem_Plugin_Module_Berechtigung::getInstance()->userHasRole("admin"),
																	]);
	}

	public function getTableContent() {
		if(Trainingssystem_Plugin_Module_Berechtigung::getInstance()->userHasRole("admin")) {
			$twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

			$roles = Trainingssystem_Plugin_Database::getInstance()->PermissionRole->getAllRoles();
			$functions = Trainingssystem_Plugin_Database::getInstance()->PermissionFunction->getAllFunctions();

			echo $twig->render("permissions/permission-table-content.html", ["roles" => $roles, "functions" => $functions, "isAdmin" => Trainingssystem_Plugin_Module_Berechtigung::getInstance()->userHasRole("admin")]);
			wp_die();
		} else {
			wp_die('', '', array("response" => 403));
		}
	}

	/**
	 * Erzeugt eine neue Rolle im Berechtigungssystem
	 */
	public function newRole() {
		if(Trainingssystem_Plugin_Module_Berechtigung::getInstance()->userHasRole("admin")) {
			if(isset($_POST['permissionsNewRoleID']) && trim($_POST['permissionsNewRoleID']) != "" && preg_match("/^[a-zA-Z0-9]+$/", $_POST['permissionsNewRoleID']) === 1 &&
				isset($_POST['permissionsNewRoleTitle']) && trim($_POST['permissionsNewRoleTitle']) != "" &&
				isset($_POST['permissionsNewRoleDescription']) && trim($_POST['permissionsNewRoleDescription']) != "") {

				$newid = $_POST['permissionsNewRoleID'];
				$newtitle = $_POST['permissionsNewRoleTitle'];
				$newdesc = trim($_POST['permissionsNewRoleDescription']);

				$per_role = Trainingssystem_Plugin_Database::getInstance()->PermissionRole;
				$allroles = $per_role->getAllRoles();

				foreach($allroles as $role) {
					if(strcasecmp($newid, $role->getID()) === 0) {
						echo "Diese ID wird bereits bei einer anderen Rolle verwendet.";
						wp_die();
					}
				}

				if($per_role->insertRole($newid, $newtitle, $newdesc, null)) {
					echo "success";
				} else {
					echo "Fehler beim Einfügen in die Datenbank.";
				}
			} else {
				echo "Es wurden nicht alle Felder ausgefüllt.";
			}
			wp_die();
		} else {
			wp_die('', '', array("response" => 403));
		}
	}

	/**
	 * Löscht eine Rolle und die dazugehörigen Rollen-Funktion-Zuordnungen
	 */
	public function deleteRole() {
		if(Trainingssystem_Plugin_Module_Berechtigung::getInstance()->userHasRole("admin")) {
			if(isset($_POST['roleid']) && trim($_POST['roleid']) != "" && preg_match("/^[a-zA-Z-0-9]+$/", $_POST['roleid']) === 1) {

				$role_id = $_POST['roleid'];
				$this->deleteRoleFromUserMeta($role_id);

				if(Trainingssystem_Plugin_Database::getInstance()->PermissionRole->deleteRole($role_id)) {
					echo "success";
				} else {
					echo "error deleting";
				}
			} else {
				echo "invalid data";
			}
			wp_die();
		} else {
			wp_die('', '', array("response" => 403));
		}
	}

	private function deleteRoleFromUserMeta($role_id) {
		global $wpdb;

		$users = get_users( array(
							'meta_query' => array(
								array(
									'key' => TRAININGSSYSTEM_PLUGIN_USER_HAS_ROLES,
									'value' => $role_id,
									'compare' => 'LIKE'
								)
							)
						)
					);

		foreach($users as $user) {
			$user_roles = get_user_meta($user->ID, TRAININGSSYSTEM_PLUGIN_USER_HAS_ROLES, false);
			
			$new_roles = array();

			if($user_roles !== false && !empty($user_roles) && isset($user_roles[0])) {
				for($i = 0; $i < count($user_roles[0]); $i++) {
					if(strcmp($user_roles[0][$i], $role_id) !== 0) {
						$new_roles[] = $user_roles[0][$i];
					}
				}
			}
			update_user_meta($user->ID, TRAININGSSYSTEM_PLUGIN_USER_HAS_ROLES, $new_roles);
		}

	}

	/**
	 * Aktualisiert die Priorität einer Rolle
	 */
	public function updateRole() {
		if(Trainingssystem_Plugin_Module_Berechtigung::getInstance()->userHasRole("admin")) {
			if(isset($_POST['roleid']) && trim($_POST['roleid']) != "" && preg_match("/^[a-zA-Z-0-9]+$/", $_POST['roleid']) === 1 &&
				isset($_POST['priodir']) && (strcmp($_POST['priodir'], "left") === 0 || strcmp($_POST['priodir'], "right") === 0)) {

				$role_id = $_POST['roleid'];
				$priority_direction = $_POST['priodir'];

				if(Trainingssystem_Plugin_Database::getInstance()->PermissionRole->updatePriority($role_id, $priority_direction)) {
					echo "success";
				} else {
					echo "error updating";
				}
			} else {
				echo "invalid data";
			}
			wp_die();
		} else {
			wp_die('', '', array("response" => 403));
		}
	}

	/**
	 * Aktualisiert eine Rolle
	 */
	public function editRole() {
		if(Trainingssystem_Plugin_Module_Berechtigung::getInstance()->userHasRole("admin")) {
			if(isset($_POST['permissionsEditRoleId']) && trim($_POST['permissionsEditRoleId']) != "" && preg_match("/^[a-zA-Z0-9]+$/", $_POST['permissionsEditRoleId']) === 1 &&
				isset($_POST['permissionsEditRoleTitle']) && trim($_POST['permissionsEditRoleTitle']) != "" &&
				isset($_POST['permissionsEditRoleDesc']) && trim($_POST['permissionsEditRoleDesc']) != "") {

				$editid = $_POST['permissionsEditRoleId'];
				$edittile = $_POST['permissionsEditRoleTitle'];
				$editdesc = trim($_POST['permissionsEditRoleDesc']);

				$per_role = Trainingssystem_Plugin_Database::getInstance()->PermissionRole;
				$allroles = $per_role->getAllRoles();

				$found = false;
				foreach($allroles as $role) {
					if(strcasecmp($editid, $role->getID()) === 0) {
						$found = true;
						break;
					}
				}

				if($found) {
					if($per_role->updateRole($editid, $edittile, $editdesc)) {
						echo "success";
					} else {
						echo "Fehler beim Aktualisieren in der Datenbank.";
					}
				} else {
					echo "Es wurde keine Rolle mit dieser ID gefunden.";
				}
			} else {
				echo "Es wurden nicht alle Felder ausgefüllt.";
			}
			wp_die();
		} else {
			wp_die('', '', array("response" => 403));
		}
	}

	/**
	 * Speichert die Rollen-Funktionszuordnung
	 */
	public function savePermissions() {
		if(Trainingssystem_Plugin_Module_Berechtigung::getInstance()->userHasRole("admin")) {
			$roles = Trainingssystem_Plugin_Database::getInstance()->PermissionRole->getAllRoles();
			$functions = Trainingssystem_Plugin_Database::getInstance()->PermissionFunction->getAllFunctions();

			$new_r2f = array();

			if(count($_POST) == (count($roles) * count($functions) + 1)) { // $_POST muss für jede Rolllen-Funktions-Zuordnung einen Boolean enthalten und zzgl. noch das action Attribut

				foreach($_POST as $key => $bool_str) {

					if($key != "action") {
						
						if($bool_str == "true") {
							$bool = true;
						} else if($bool_str == "false") {
							$bool = false;
						} else {
							echo "invalid value";
							wp_die();
						}					

						$splitted = explode("_", $key);
						if(count($splitted) != 3) {
							echo "invalid key";
							wp_die();
						} else {
							if($bool) { // (Momentan) Nur für true Werte einen Eintrag generieren, kann in Zukunft noch für mehr als Ja/Nein ausgebaut werden
								$role_id = $splitted[1];
								$func_id = $splitted[2];
								$new_r2f[] = new Trainingssystem_Plugin_Database_Permission_Role2Function(null, $func_id, $role_id, $bool);
							}
						}
					}
				}

				if(Trainingssystem_Plugin_Database::getInstance()->PermissionRole2Function->clearTable()) {

					foreach($new_r2f as $permission) {
						if(!Trainingssystem_Plugin_Database::getInstance()->PermissionRole2Function->insertPermission($permission->getRoleId(), $permission->getFunctionId(), $permission->getValue())) {
							echo "error inserting permission";
							wp_die();
						}
					}

					echo "success";

				} else {
					echo "error clearing table";
				}

			} else {
				echo "invalid data";
			}

			wp_die();
		} else {
			wp_die('', '', array("response" => 403));
		}
	}

	/**
	 * Stellt den Standard-Zustand der Berechtigungen, Rollen und Funktionen (wieder) her. Setzt dabei die aktuell installierte Version neu.
	 */
	public function permissionsDefault() {
		if(Trainingssystem_Plugin_Module_Berechtigung::getInstance()->userHasRole("admin")) {
			$roles = $this->getDefaultRoles();
			$functions = $this->getDefaultFunctions();
			$role2functions = $this->getDefaultRole2Functions();

			if(Trainingssystem_Plugin_Database::getInstance()->PermissionRole->clearTable() && 
				Trainingssystem_Plugin_Database::getInstance()->PermissionFunction->clearTable() && 
				Trainingssystem_Plugin_Database::getInstance()->PermissionRole2Function->clearTable()) {

				foreach($roles as $role) {
					if(!Trainingssystem_Plugin_Database::getInstance()->PermissionRole->insertRoleObject($role)) {
						echo "error inserting role";
						wp_die();
					}
				}

				foreach($functions as $function) {
					if(!Trainingssystem_Plugin_Database::getInstance()->PermissionFunction->insertFunctionObject($function)) {
						echo "error inserting function";
						wp_die();
					}
				}

				foreach($role2functions as $r2f) {
					if(!Trainingssystem_Plugin_Database::getInstance()->PermissionRole2Function->insertPermissionObject($r2f)) {
						echo "error inserting r2f";
						wp_die();
					}
				}

				update_option(self::$versionSetting, self::$version);

				echo "success";

			} else {
				echo "error clearing tables";
			}
			wp_die();
		} else {
			wp_die('', '', array("response" => 403));
		}
	}

	/**
	 * Aktualisiert die Funktionen auf den neuesten Stand und setzt dabei die aktuell installierte Version neu.
	 */
	public function updateFunctions() {
		if(Trainingssystem_Plugin_Module_Berechtigung::getInstance()->userHasRole("admin")) {
			if(Trainingssystem_Plugin_Database::getInstance()->PermissionFunction->clearTable()) {

				$functions = $this->getDefaultFunctions();

				foreach($functions as $function) {
					if(!Trainingssystem_Plugin_Database::getInstance()->PermissionFunction->insertFunctionObject($function)) {
						echo "error inserting function";
						wp_die();
					}
				}

				$roles = $this->getDefaultRoles();
				foreach($roles as $role) {
					if(!Trainingssystem_Plugin_Database::getInstance()->PermissionRole->updateFunctionObject($role)) {
						echo "error updating role";
						wp_die();
					}
				}

				update_option(self::$versionSetting, self::$version);

				echo "success";

			} else {
				echo "error clearing table";
			}
			wp_die();
		} else {
			wp_die('', '', array("response" => 403));
		}
	}

	/**
	 * Migriert die alten Berechtigungen (jede Berechtigung 1 EIntrag in user_meta) in das neue Format (1 Array mit allen Berechtigungen in user_meta)
	 */
	public function migratePermissions() {
		if(Trainingssystem_Plugin_Module_Berechtigung::getInstance()->userHasRole("admin")) {
			global $wpdb;

			$users = get_users( array(
									'meta_query' => array(
										'relation' => 'OR',
										array(
											'key' => 'pAdminId',
											'value' => '',
											'compare' => 'LIKE'
										),
										array(
											'key' => 'kAdminId',
											'value' => '',
											'compare' => 'LIKE'
										),
										array(
											'key' => 'dAdminId',
											'value' => '',
											'compare' => 'LIKE'
										),
										array(
											'key' => 'pCoachId',
											'value' => '',
											'compare' => 'LIKE'
										),
										array(
											'key' => 'bossId',
											'value' => '',
											'compare' => 'LIKE'
										)
									)
								)
							);

			foreach($users as $user) {
				$new_roles = array();

				if(!empty(get_user_meta($user->ID, 'pAdminId', true))) {
					$new_roles[] = "pAdmin";
				}

				if(!empty(get_user_meta($user->ID, 'kAdminId', true))) {
					$new_roles[] = "kAdmin";
				}

				if(!empty(get_user_meta($user->ID, 'dAdminId', true))) {
					$new_roles[] = "dAdmin";
				}

				if(!empty(get_user_meta($user->ID, 'pCoachId', true))) {
					$new_roles[] = "pCoach";
				}

				if(!empty(get_user_meta($user->ID, 'bossId', true))) {
					$new_roles[] = "boss";
				}

				if(!empty($new_roles)) {
					if(!update_user_meta($user->ID, TRAININGSSYSTEM_PLUGIN_USER_HAS_ROLES, $new_roles)) {
						echo "error updating permissions for user " . $user->ID;
						wp_die();
					}
				}
			}

			$sql = "DELETE FROM $wpdb->usermeta 
						WHERE meta_key = 'pAdminId' or 
							meta_key = 'kAdminId' or 
							meta_key = 'dAdminId' or 
							meta_key = 'pCoachId' or 
							meta_key = 'bossId'";

			if($wpdb->query($sql) !== false) {
				echo "success";
			} else {
				echo "error deleting old permissions";
			}

			wp_die();
		} else {
			wp_die('', '', array("response" => 403));
		}
	}

	/**
	 * Prüft ob ein Benutzer Zugriff auf eine Funktion hat
	 * 
	 * @param String Funktion-ID
	 * @param Integer User-ID - optional: wenn nicht gegeben wird die User-ID des aktuellen Users gewählt
	 * 
	 * @return boolean Zugriff erlaubt
	 */
	public function accessAllowed($function_id, $user_id = null) {
		if($user_id == null) {
			$user_id = get_current_user_id();
		}
		if($user_id == 0) {
			return false;
		}

		if(user_can($user_id, 'manage_options')) { // Admins dürfen alles
			return true;
		}

		$user_roles = get_user_meta($user_id, TRAININGSSYSTEM_PLUGIN_USER_HAS_ROLES, false);
		if($user_roles === false || empty($user_roles)) {
			return false;
		}
		if(!isset($user_roles[0])) {
			return false;
		}

		$user_roles = $user_roles[0];

		$roles = Trainingssystem_Plugin_Database::getInstance()->PermissionRole->getAllRoles();

		foreach($roles as $role) {
			if(in_array($role->getId(), $user_roles)) {
				if($role->hasPermission($function_id)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Prüft ob ein Benutzer eine bestimmte Rolle inne hat
	 * 
	 * @param String Rollen-ID
	 * @param Integer User-ID - optional: wenn nicht gegeben wird die User-ID des aktuellen Users gewählt
	 * 
	 * @return boolean Zugriff erlaubt
	 */
	public function userHasRole($role_id, $user_id = null) {
		if($user_id == null) {
			$user_id = get_current_user_id();
		}
		if($user_id == 0) {
			return false;
		}

		if(user_can($user_id, 'manage_options')) { // Admins dürfen alles
			return true;
		}

		$user_roles = get_user_meta($user_id, TRAININGSSYSTEM_PLUGIN_USER_HAS_ROLES, false);
		if($user_roles === false || empty($user_roles)) {
			return false;
		}
		if(!isset($user_roles[0])) {
			return false;
		}

		$user_roles = $user_roles[0];

		foreach($user_roles as $role) {
			if(strcasecmp($role, $role_id) === 0) {
				return true;
			}
		}
		return false;
	}

	public function blockContent($content) {
		global $post;
		
		if($post->post_type == "nachrichten" || $post->post_type == "formulare" || $post->post_type == "abzeichen" || $post->post_type == "zertifikate" || $post->post_type == "uebungen" || $post->post_type == "avatare" || $post->post_status != "publish") {
			if(Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("trainingedit")) {
				return $content;
			} else {
				$twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
				return $twig->render('modal-unauth.html', ["title" => get_bloginfo('name'), "url" => get_site_url(), "modalid" => uniqid()]);
			}
		} else {
			return $content;
		}
	}

	public function isAdmin(){
		trigger_error("The permissions/role system was changed. Please use the accessAllowed/userHasRole functions instead.", E_USER_DEPRECATED);
		return current_user_can('administrator');
	}

	public function isPAdmin(){
		trigger_error("The permissions/role system was changed. Please use the accessAllowed/userHasRole functions instead.", E_USER_DEPRECATED);
		$permission = get_user_meta( get_current_user_id(), 'pAdminId' , true );
		if ( ! empty( $permission )) {
			return $permission;
		}
		return false;
	}

	public function isKAdmin(){
		trigger_error("The permissions/role system was changed. Please use the accessAllowed/userHasRole functions instead.", E_USER_DEPRECATED);
		$permission = get_user_meta( get_current_user_id(), 'kAdminId' , true );
		if ( ! empty( $permission )) {
			return $permission;
		}
		return false;
	}

	public function isDAdmin(){
		trigger_error("The permissions/role system was changed. Please use the accessAllowed/userHasRole functions instead.", E_USER_DEPRECATED);
		$permission = get_user_meta( get_current_user_id(), 'dAdminId' , true );
		if ( ! empty( $permission )) {
			return $permission;
		}
		return false;
	}

	public function isPCoach() {
		trigger_error("The permissions/role system was changed. Please use the accessAllowed/userHasRole functions instead.", E_USER_DEPRECATED);
		$permission = get_user_meta( get_current_user_id(), 'pCoachId' , true );
		if ( ! empty( $permission )) {
			return $permission;
		}
		return false;
	}

	public function isBoss() {
		trigger_error("The permissions/role system was changed. Please use the accessAllowed/userHasRole functions instead.", E_USER_DEPRECATED);
		$permission = get_user_meta( get_current_user_id(), 'bossId' , true );
		if ( ! empty( $permission )) {
			return $permission;
		}
		return false;
	}

	private function getDefaultRoles() {
		return array( // Sortiert nach Prio aufsteigend (muss nicht explizit gesetzt werden), $_id darf NIE verändert werden!
					// Format: 		$_id ID darf nur die Zeichen a-z, A-Z, 0-9 enthalten (!! KEINE Sonderzeichen !!), $_title, $_description
					new Trainingssystem_Plugin_Database_Permission_Role("pAdmin", "Training-Entwickler", "Eine Person mit der Rolle Training-Entwickler ist dazu da, Trainings, Formulare und Abzeichen im Backend hinzuzufügen, User zu erstellen und diese den Psychologischer Coach zuzuweisen."),
					new Trainingssystem_Plugin_Database_Permission_Role("pCoach", "Psychologischer Coach", "Psychologische Coaches werden für die Betreuung der Teilnehmenden im System angelegt. Beim Hinzufügen der Teilnehmenden kann der entsprechende psychologische Coach zugewiesen werden."),
					new Trainingssystem_Plugin_Database_Permission_Role("supervisor", "Supervisor", "Eine Person mit der Rolle Supervisor ist dazu da, die Coaches zu beaufsichtigen."),
					new Trainingssystem_Plugin_Database_Permission_Role("kAdmin", "Krankenkassenadmin", "Die Personen mit der Rolle Krankenkassenadmin organisieren das Arbeiten in Gruppen, wie beispielsweise Unternehmen oder Abteilungen. Personen mit dieser Rolle haben keinen Zugriff auf individuelle Eingaben der User."),
					new Trainingssystem_Plugin_Database_Permission_Role("boss", "Führungskraft", "Eine Führungskraft kann die Auswertung der einzelnen Abteilungen einsehen."),
					new Trainingssystem_Plugin_Database_Permission_Role("dAdmin", "Datenadmin", "Die Personen mit der Rolle Datenadmin haben die Berechtigung Nutzerdaten aus Eingabefeldern, Systemstatistiken und Umfragen für alle Nutzer der Plattform zu exportieren."),
					new Trainingssystem_Plugin_Database_Permission_Role("teamLeader", "Teamleitung", "Die Personen mit der Rolle Teamleitung haben die Führungskraft-berechtigungen auf Gruppen-Ebene."),
		);
	}

	private function getDefaultFunctions() {
		return array( // $_id darf NIE verändert werden!
					// Format:		$_id ID darf nur die Zeichen a-z, A-Z, 0-9 enthalten (!! KEINE Sonderzeichen !!), $_title, $_description
					new Trainingssystem_Plugin_Database_Permission_Function("trainingmgr", "Trainings zuweisen", "Diese Funktion kann den einzelnen Nutzern Trainings zuweisen sowie deren Trainings-Modus anpassen."),
					new Trainingssystem_Plugin_Database_Permission_Function("userlist", "Nutzerliste", "Diese Funktion kann die Nutzerliste einsehen. Benötigt eine der folgenden Funktionen zum Filtern der Nutzenden."),
					new Trainingssystem_Plugin_Database_Permission_Function("userdetails", "Nutzer-Details", "Diese Funktion kann Details zu einem einzelnen Nutzer einsehen. Benötigt eine der folgenden Funktionen zum Filtern der Nutzenden."),
					new Trainingssystem_Plugin_Database_Permission_Function("userdetailsstatistics", "Nutzer-Details - Nutzerstatistiken einsehen", "Diese Funktion kann die Statistiken eines einzelnen Nutzers in der Nutzer-Detail-Seite einsehen."),
					new Trainingssystem_Plugin_Database_Permission_Function("userlistall", "Nutzerliste - Alle Nutzenden", "Diese Funktion ist mit der Funktion Nutzerliste & Nutzer-Details verknüpft. Diese Funktion kann alle Nutzenden auf der Plattform einsehen."),
					new Trainingssystem_Plugin_Database_Permission_Function("userlistsubscriber", "Nutzerliste - Alle Abonnenten", "Diese Funktion ist mit der Funktion Nutzerliste & Nutzer-Details verknüpft. Diese Funktion kann alle Abonnenten der Plattform einsehen."),
					new Trainingssystem_Plugin_Database_Permission_Function("userlistcoach", "Nutzerliste - Alle Coach-Teilnehmer", "Diese Funktion ist mit der Funktion Nutzerliste & Nutzer-Details verknüpft. Diese Funktion kann alle Teilnehmer des Coachs einsehen."),
					new Trainingssystem_Plugin_Database_Permission_Function("userlistcompany", "Nutzerliste - Alle Firmenmitglieder", "Diese Funktion ist mit der Funktion Nutzerliste & Nutzer-Details verknüpft. Diese Funktion kann alle Nutzenden von selbst angelegten Unternehmen einsehen."),
					new Trainingssystem_Plugin_Database_Permission_Function("useredit", "Nutzer-Details bearbeiten", "Diese Funktion kann die Daten eines einzelnen Nutzers bearbeiten sowie den Benutzer löschen. Nur Administratoren können einem Benutzer Rollen zuweisen und entfernen."),
					new Trainingssystem_Plugin_Database_Permission_Function("usermodus", "User-Modus", "Diese Funktion lässt den Benutzer in der Rolle eines anderen Nutzers die Plattform sowie der Eingaben innerhalb der Trainings sehen."),
					new Trainingssystem_Plugin_Database_Permission_Function("nachrichtenverlauf", "Nachrichtenverlauf einsehen", "Diese Funktion kann den Nachrichtenverlauf eines einzelnen Benutzers einsehen (in Verbindung mit dem Recht des User-Modus)."),
					new Trainingssystem_Plugin_Database_Permission_Function("nutzernew", "Nutzer anlegen", "Diese Funktion kann neue Nutzer auf der Plattform anlegen und diesen eine erneute Registrierungsmail schicken."),
					new Trainingssystem_Plugin_Database_Permission_Function("nutzerpermissions", "Nutzerberechtigungen", "Diese Funktion kann (neue) Nutzer mit Berechtigungen ausstatten. Benötigt eine der folgenden Funktionen zur Vergabe entsprechender Berechtigungen."),
					new Trainingssystem_Plugin_Database_Permission_Function("nutzerpermissionsall", "Nutzerberechtigungen - Alle Berechtigungen", "Diese Funktion ist mit der Funktion Nutzerberechtigungen verknüpft. Diese Funktion kann alle Berechtigungen vergeben."),
					new Trainingssystem_Plugin_Database_Permission_Function("nutzerpermissionsboss", "Nutzerberechtigungen - Führungskraft", "Diese Funktion ist mit der Funktion Nutzerberechtigungen verknüpft. Diese Funktion kann die Berechtigung Führungskraft vergeben."),
					new Trainingssystem_Plugin_Database_Permission_Function("nutzerpermissionsteamleader", "Nutzerberechtigungen - Teamleitung", "Diese Funktion ist mit der Funktion Nutzerberechtigungen verknüpft. Diese Funktion kann die Berechtigung Teamleitung vergeben"),
					new Trainingssystem_Plugin_Database_Permission_Function("nutzerpermissionskAdmin", "Nutzerberechtigungen - Krankenkassenadmin", "Diese Funktion ist mit der Funktion Nutzerberechtigungen verknüpft. Diese Funktion kann die Berechtigung Krankenkassenadmin vergeben"),
					new Trainingssystem_Plugin_Database_Permission_Function("nutzerimport", "Nutzer-Import", "Diese Funktion kann eine Nutzerliste importieren, um mehrere Nutzer gleichzeitig anzulegen."),
					new Trainingssystem_Plugin_Database_Permission_Function("vorlagen", "Trainingsvorlagen", "Diese Funktion kann Vorlagen erstellen und den Vorlagen Trainings zuweisen."),
					new Trainingssystem_Plugin_Database_Permission_Function("registerkey", "Freischaltcodes", "Diese Funktion kann Freischaltcodes ansehen, erstellen und bearbeiten."),
					new Trainingssystem_Plugin_Database_Permission_Function("companymgr", "Unternehmen", "Diese Funktion kann die Liste der Unternehmen sehen, Mitglieder in Gruppen organisieren und Firmenstatistiken einsehen. Benötigt eine der folgenden Funktionen zum Filtern der Unternehmen."),
					new Trainingssystem_Plugin_Database_Permission_Function("companymgrall", "Unternehmen - Alle Unternehmen sehen", "Diese Funktion ist mit der Funktion Unternehmen verknüpft. Sie zeigt alle Unternehmen im System an."),
					new Trainingssystem_Plugin_Database_Permission_Function("companymgrown", "Unternehmen - Meine Unternehmen sehen", "Diese Funktion ist mit der Funktion Unternehmen verknüpft. Sie zeigt alle Unternehmen im System an, die man selbst angelegt hat."),
					new Trainingssystem_Plugin_Database_Permission_Function("systemstatistiken", "Systemstatistiken", "Diese Funktion kann die Systemstatistiken der Plattform sehen."),
					new Trainingssystem_Plugin_Database_Permission_Function("nachrichtennew", "Nachrichten versenden", "Diese Funktion kann neue Nachrichten an andere Nutezr senden."),
					new Trainingssystem_Plugin_Database_Permission_Function("trainingseximport", "Trainings Ex-/Import", "Diese Funktion kann Trainings der Plattform exportieren und neue Trainings importieren."),
					new Trainingssystem_Plugin_Database_Permission_Function("dataexport", "Datenexport", "Diese Funktion kann Eingaben aus Trainings, Eingaben aus LimeSurvey-Umfragen und Systemstatistiken exportieren."),
					new Trainingssystem_Plugin_Database_Permission_Function("coachingoverview", "Coaching-Übersicht", "Auf der Coaching-Übersicht können Coaches einen Überblick über ihre User erhalten."),
					new Trainingssystem_Plugin_Database_Permission_Function("coachingoverviewsupervisor", "Supervisoren-Übersicht", "Auf der Supervisoren-Übersicht können Supervisoren einen Überblick über ihre Coaches erhalten, deren Auslastung überprüfen und sie Usern zuteilen."),
					new Trainingssystem_Plugin_Database_Permission_Function("trainingedit", "Trainings bearbeiten", "Diese Funktion kann im Zusammenhang mit mindestens der WordPress-Rolle Redakteur im Backend Änderungen vornehmen. Dazu zählen u.a. Trainings mit Lektionen und Seiten erstellen, bearbeiten und zusammenstellen, Formulare bearbeiten, Abzeichen bearbeiten etc."),
					new Trainingssystem_Plugin_Database_Permission_Function("forwardcontentboss", "Freigegebene Texte einsehen (Führungskraft-Sicht)", "Diese Funktion zeigt die freigegebenen Texte aller Firmenmitglieder für eine Führungskraft."),
					new Trainingssystem_Plugin_Database_Permission_Function("forwardcontentkadmin", "Freigegebene Texte einsehen (Krankenkassenadmin-Sicht)", "Diese Funktion zeigt die freigegebenen Texte aller Firmenmitglieder für einen Krankenkassenadmin."),
					new Trainingssystem_Plugin_Database_Permission_Function("forwardcontentteamleader", "Freigegebene Texte einsehen (Teamleitung-Sicht)", "Diese Funktion zeigt die freigegebenen Texte aller Firmenmitglieder für eine Teamleitung."),
					new Trainingssystem_Plugin_Database_Permission_Function("gbuchartboss", "Visualisierungen von Befragungsergebnissen einsehen (Führungskraft-Sicht)", "Diese Funktion zeigt die Visualisierungen von Befragungsergebnissen eines Unternehmens für eine Führungskraft."),
					new Trainingssystem_Plugin_Database_Permission_Function("gbuchartkadmin", "Visualisierungen von Befragungsergebnissen einsehen (Krankenkassenadmin-Sicht)", "Diese Funktion zeigt die Visualisierungen von Befragungsergebnissen einer oder mehrerer Unternehmen für einen Krankenkassenadmin."),
					new Trainingssystem_Plugin_Database_Permission_Function("gbuchartteamleader", "Visualisierungen von Befragungsergebnissen einsehen (Teamleitung-Sicht)", "Diese Funktion zeigt die Visualisierungen von Befragungsergebnissen einer Gruppe für eine Teamleitung."),
					new Trainingssystem_Plugin_Database_Permission_Function("studyoverview", "Studienübersicht", "Diese Funktion kann die Studienübersicht einsehen."),
					new Trainingssystem_Plugin_Database_Permission_Function("gbumittelwerteboss", "Gemittelte Befragungsergebnissen einsehen (Führungskraft-Sicht)", "Diese Funktion zeigt die gemittelten Befragungsergebnissen eines Unternehmens für eine Führungskraft."),
					new Trainingssystem_Plugin_Database_Permission_Function("gbumittelwertekadmin", "Gemittelte Befragungsergebnissen einsehen (Krankenkassenadmin-Sicht)", "Diese Funktion zeigt die gemittelten Befragungsergebnissen einer oder mehrerer Unternehmen für einen Krankenkassenadmin."),
					new Trainingssystem_Plugin_Database_Permission_Function("gbumittelwerteteamleader", "Gemittelte Befragungsergebnissen einsehen (Teamleitung-Sicht)", "Diese Funktion zeigt die gemittelten Befragungsergebnissen einer Gruppe für eine Teamleitung."),
					new Trainingssystem_Plugin_Database_Permission_Function("kAdminEditor", "Krankenkassenadmin für Unternehmen zuweisen", "Die Funktion kann die Rolle kAdmin für ein Unternehmen zuweisen."),
					new Trainingssystem_Plugin_Database_Permission_Function("bossEditor", "Führungskraft für Unternehmen zuweisen", "Die Funktion kann die Rolle boss für ein Unternehmen zuweisen."),
					new Trainingssystem_Plugin_Database_Permission_Function("appAdmin", "Audio-Dateien für App zuweisen", "Die Funktion kann Audio-Dateien für die App zuweisen."),
					new Trainingssystem_Plugin_Database_Permission_Function("userlistown", "Nutzerliste - Meine Nutzenden", "Diese Funktion ist mit der Funktion Nutzerliste & Nutzer-Details verknüpft. Diese Funktion kann alle selbst angelegten Nutzenden auf der Plattform einsehen."),
					new Trainingssystem_Plugin_Database_Permission_Function("vorlagenall", "Trainingsvorlagen - Alle Vorlagen sehen", "Diese Funktion ist mit der Funktion Trainingsvorlagen verknüpft. Sie zeigt alle Vorlagen im System an."),
					new Trainingssystem_Plugin_Database_Permission_Function("vorlagenown", "Trainingsvorlagen - Meine Vorlagen sehen", "Diese Funktion ist mit der Funktion Trainingsvorlagen verknüpft. Sie zeigt alle Vorlagen im System an, die man selbst angelegt hat."),
					new Trainingssystem_Plugin_Database_Permission_Function("registerkeyall", "Freischaltcodes - Alle Freischaltcodes sehen", "Diese Funktion ist mit der Funktion Freischaltcodes verknüpft. Sie zeigt alle Freischaltcodes im System an."),
					new Trainingssystem_Plugin_Database_Permission_Function("registerkeyown", "Freischaltcodes - Meine Freischaltcodes sehen", "Diese Funktion ist mit der Funktion Freischaltcodes verknüpft. Sie zeigt alle Freischaltcodes im System an, die man selbst angelegt hat."),
					new Trainingssystem_Plugin_Database_Permission_Function("trainingprogress", "Trainingsfortschritt sehen", "Diese Funktion gibt Rollen Zugriff auf den Trainingsfortschritt von Nutzenden."),
					new Trainingssystem_Plugin_Database_Permission_Function("companymgrdetails", "Unternehmen - Details einsehen", "Diese Funktion gibt Rollen Zugriff auf die Details eines Unternehmens."),
					new Trainingssystem_Plugin_Database_Permission_Function("companymgrdetailsnothreshold", "Unternehmen - Details - Schwelle umgehen", "Diese Funktion ist mit der Funktion Unternehmen - Details verknüpft. Sie zeigt Details auch an, wenn die Mindestanzahl an Teilnehmenden noch nicht erreicht wurde."),
					new Trainingssystem_Plugin_Database_Permission_Function("trainingshidden", "Trainings - auch verborgene sehen", "Diese Funktion erlaubt Trainings, welche für User ausgeblendet wurden, trotzdem anzuzeigen."),
		);
	}

	private function getDefaultRole2Functions() {
		return array(
					// Format: 		$_id muss null sein (ID wird mittels AutoIncrement vergeben), $_function_id, $_role_id, $_value
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "trainingmgr", "pCoach", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "trainingmgr", "supervisor", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "trainingmgr", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "userlist", "pCoach", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "userlist", "supervisor", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "userlist", "kAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "userlist", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "userlistall", "supervisor", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "userlistall", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "userlistsubscriber", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "userlistcoach", "pCoach", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "userlistcompany", "kAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "userdetails", "pCoach", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "userdetails", "supervisor", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "userdetails", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "userdetails", "kAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "useredit", "supervisor", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "useredit", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "useredit", "kAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "usermodus", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "usermodus", "pCoach", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "usermodus", "supervisor", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "nachrichtenverlauf", "pCoach", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "nachrichtenverlauf", "supervisor", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "nutzernew", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "nutzernew", "kAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "nutzerimport", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "nutzerimport", "kAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "nutzerpermissions", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "nutzerpermissionsall", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "vorlagen", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "vorlagen", "kAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "registerkey", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "registerkey", "kAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "companymgr", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "companymgr", "kAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "companymgrall", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "companymgrown", "kAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "systemstatistiken", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "systemstatistiken", "dAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "nachrichtennew", "pCoach", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "nachrichtennew", "supervisor", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "dataexport", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "dataexport", "dAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "coachingoverview", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "coachingoverview", "pCoach", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "coachingoverviewsupervisor", "supervisor", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "trainingedit", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "forwardcontentboss", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "forwardcontentboss", "boss", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "forwardcontentkadmin", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "forwardcontentkadmin", "kAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "forwardcontentteamleader", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "forwardcontentteamleader", "teamLeader", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "gbuchartboss", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "gbuchartboss", "boss", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "gbuchartkadmin", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "gbuchartkadmin", "kAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "gbuchartteamleader", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "gbuchartteamleader", "teamLeader", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "studyoverview", "pCoach", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "studyoverview", "supervisor", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "studyoverview", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "gbumittelwerteboss", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "gbumittelwerteboss", "boss", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "gbumittelwertekadmin", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "gbumittelwertekadmin", "kAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "gbumittelwerteteamleader", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "gbumittelwerteteamleader", "teamLeader", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "kAdminEditor", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "trainingseximport", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "appAdmin", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "userlistown", "kAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "vorlagenall", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "vorlagenown", "kAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "registerkeyall", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "registerkeyown", "kAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "trainingprogress", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "trainingprogress", "pCoach", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "trainingprogress", "supervisor", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "companymgrdetails", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "companymgrdetails", "kAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "companymgrdetailsnothreshold", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "userdetailsstatistics", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "trainingshidden", "pAdmin", 1),
					new Trainingssystem_Plugin_Database_Permission_Role2Function(null, "bossEditor", "pAdmin", 1),
		);
	}
}

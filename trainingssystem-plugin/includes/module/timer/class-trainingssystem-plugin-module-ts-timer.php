<?php

class Trainingssystem_Plugin_Module_TS_Timer{

    public function __construct() {

    }

    public function scTSTimer($atts, $content=null){

        $a = shortcode_atts(array(
			'h' => '0',
			'm' => '0', 
			's' => '0',
            'stop' => '0',
			'format' => 'hms',
        ), $atts);

        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

        return $twig->render('timer/timer.html', [
            "h" => $a['h'],
            "m" => $a['m'],
            "s" => $a['s'],
            "stop" => $a['stop'],
            "format" => $a['format'],
            "ran" => mt_rand()
        ]);
    }

}
<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 */
class Trainingssystem_Plugin_Module_Help{

	public function __construct() {

	}

	/**
	 * display admin backend
	 *
	 * @since    1.0.0
	 */
	function backendAdminCreate(){
		if (!current_user_can('administrator')) {
			return false;
		}
		else {
			$twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
			echo $twig->render('admin-backend/admin-backend-help-pdf.html',["pdfurl"=>plugin_dir_url( dirname(__FILE__ )) . '../../assets/pdf/doku.pdf']);
		}
	}

	/**
	 * admin bar ausblenden
	 *
	 * @since    1.0.0
	 */
	function hideadminbaruser(){
		if (!current_user_can('administrator') && !is_admin()) {
			show_admin_bar(false);
		  }
	}
}

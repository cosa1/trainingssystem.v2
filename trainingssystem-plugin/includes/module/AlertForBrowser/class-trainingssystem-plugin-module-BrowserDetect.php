<?php
/**
 * Created by PhpStorm.
 * User: Ali Parnan
 * Date: 11.11.19
 * Time: 11:55
 */


/**
 * Class Trainingssystem_Plugin_Module_BrowserDetection
 * shows user pop op message when wrong browser is used
 */
class Trainingssystem_Plugin_Module_BrowserDetection{


    public function __construct() {

    }


    public function showBrowserlog(){

        $result = new WhichBrowser\Parser($_SERVER['HTTP_USER_AGENT']);

          if($result->browser->name != 'Chrome' && $result->browser->name != 'Firefox' && $result->browser->name != 'Safari'){
              $message = "Warnung: Bitte verwenden Sie für ein optimales Nutzungserlebnis den Chrome-Browser. Bei der Nutzung von ".$result->browser->name." kann es zu Fehlern in der Funktionalität und Darstellung kommen.";
               echo "<script>jQuery('#allcontent').after(\"<div class='warning-div center-block bg-danger' style='width:100%;'><p class='warning-p center-block' style='width:80%;'>".$message."</p></div>\")</script>";
          }
    }

    }

<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 */
class Trainingssystem_Plugin_Module_Accordion
{

    public function __construct()
    {

    }

    /**
     * display accordion the site.
     *
     * @since    1.0.0
     */
    public function show_accordion($atts, $content = null)
    {
		$twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

        if (isset($atts['title'])) {
            if (isset($atts['title-size']) && $atts['title-size'] == 'small') {
                $title_size_class = 'accordiontitle-small';
            } else {
                $title_size_class = '';
            }

            if (isset($atts['inner_classes']) && trim($atts['inner_classes'] != '')) {
                $inner_classes = $atts['inner_classes'];
            } else {
                $inner_classes = '';
            }

            if (isset($atts['header_classes']) && trim($atts['header_classes'] != '')) {
                $header_classes = $atts['header_classes'];
            } else {
                $header_classes = '';
            }

            if (isset($atts['div_classes']) && trim($atts['div_classes'] != '')) {
                $div_classes = $atts['div_classes'];
            } else {
                $div_classes = '';
            }

			if (isset($atts['icon']) && trim($atts['icon'] != '' && in_array($atts['icon'], array("plus", "chevron")))) {
                $icon = $atts['icon'];
            } else {
                $icon = 'plus';
            }

            if (isset($atts['show']) && $atts['show'] == '1') {
                $show = 1;
            } else {
                $show = 0;
            }

            return $twig->render('accordion/accordion.html', [
										"ran" => mt_rand(), 
										"title" => $atts['title'], 
										"title_size_class" => $title_size_class, 
										"header_classes" => $header_classes, 
										"inner_classes" => $inner_classes, 
										"div_classes" => $div_classes, 
										"icon" => $icon,
										"do_shortcode" => do_shortcode($content),
                                        "show" => $show
									]);
        } else {
            return $twig->render("accordion/accordion-error.html");
        }
    }
}

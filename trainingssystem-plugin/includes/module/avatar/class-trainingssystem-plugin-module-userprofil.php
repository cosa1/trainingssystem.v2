<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 */
class Trainingssystem_Plugin_Module_Userprofil{


	public function __construct() {

	}

    /** function to show user profile
     * @return false|string
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */



    public function show_profil(){
        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

        ob_start();
        $current_user = wp_get_current_user();

        $firstName = $current_user->user_firstname;
        $lastName = $current_user->user_lastname;
        $email = $current_user->user_email;
        $id = $current_user->ID;

        echo $twig->render('showUserProfil/show_user_profil.html', [
            "firstname" => $firstName,
            "lastname" => $lastName,
            "email" => $email,
            "id" => $id,
        ] );
        return ob_get_clean();
    }

    /** function to show the user profile with user attributes and avatar
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function frontend_avatar_upload_view()
    {
        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
        $current_user_id = wp_get_current_user()->ID;
        $profile_avatar = get_user_meta($current_user_id,'_avatar_url',true);
        wp_register_script( 'avatarjs', plugin_dir_url( __FILE__ ) .'../../../assets/external/avatar/avatar.js', array('jquery'));
        wp_enqueue_script( 'avatarjs');
        wp_localize_script( 'avatarjs', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
        wp_enqueue_media();


        $current_user = wp_get_current_user();
        $usercompany = Trainingssystem_Plugin_Database::getInstance()->NutzerDao->getUserCompanySystem($current_user->ID);


        $firstName = $current_user->user_firstname;
        $lastName = $current_user->user_lastname;
        $email = $current_user->user_email;
        $id = $current_user->ID;
        $age = get_user_meta($id,"age",true);
        $gender = get_user_meta($id,"gender",true);
        $place = get_user_meta($id,"place",true);

        $avatar= get_avatar(get_current_user_id(),100,'','',array('class'=>'img-responsive img-circle'));

        echo $twig->render('showUserProfil/user-profil.html', [
            "profil_avatar" => $profile_avatar,
            "avatar" => $avatar,
            "firstname" => $firstName,
            "lastname" => $lastName,
            "email" => $email,
            "company" => $usercompany,
            "id" => $id,
            "age" =>$age,
            "gender" =>$gender,
            "place" =>$place,
            'hideMails' => isset(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)['hide_mails']),

        ] );

    }
    function extendUserMetaData(){



    }

    /** function to display the attachment of user
     * @param array $query
     * @return array
     */
    function show_current_user_attachments( $query = array() ) {
        $roles = wp_get_current_user()->roles;
        if(in_array('subscriber',$roles)){
            $user_id = get_current_user_id();
            if( $user_id ) {
                $query['author'] = $user_id;
            }
            return $query;
        }
        return $query;
    }
    public function avatar_filter_callback($avatar, $id_or_email, $size, $default, $alt){
    $url = "".get_user_meta(wp_get_current_user()->ID, '_avatar_url',True);
    if ( $url!="" ) {
        $avatar = "<img alt='{$alt}' src='{$url}' class='avatar avatar-{$size} photo' height='{$size}' width='{$size}' />";
    }
}
    public function avatar_upload_action_callback()
    {
        update_user_meta(wp_get_current_user()->ID, '_avatar_url', $_POST['url']);
        wp_die();
    }



	public function menu() {

		$twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

		ob_start();

		$current_user = wp_get_current_user();
		if (0 == $current_user->ID) {
			return $twig->render('modal-unauth.html', ["title" => get_bloginfo('name'), "url" => get_site_url(), "modalid" => uniqid()]);
		} else {

			$avatarimg = get_avatar( get_current_user_id(), 32 );
			echo $twig->render('avatar/avatar-menu.html',["avatarimg"=>$avatarimg]);
			
		}
		return ob_get_clean();
        

	}

	public function register_avatar_widget(){
		register_widget( 'Trainingssystem_Plugin_Module_Avatar_Widget' );
	}

	public function avataroverlay(){

		$twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
		ob_start();
		$current_user = wp_get_current_user();
		if (0 == $current_user->ID) {
		} else {

			$profilurl = "";
			$display = -1;
			$settings = get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS);
            if(isset($settings["page_user_profil"]) && isset($settings["avatar_overlay_view"])){
				$profilurl = get_permalink($settings["page_user_profil"]);
				$display = $settings["avatar_overlay_view"];

                if($display != -1){
                    $trainingsID = htmlspecialchars(preg_replace('/[^0-9]/', '', get_query_var('idt1') . ''));
                    $lektionID = htmlspecialchars(preg_replace('/[^0-9]/', '', get_query_var('idt2') . ''));
                    
                    $training = Trainingssystem_Plugin_Database::getInstance()->TrainingDao->getTrainingByUser($trainingsID, $current_user->ID);
                    $tlektion = array_filter($training->getLektionsliste(),
                        function ($e) use ($lektionID) {
                            return $e->getId() == $lektionID;
                        }
                    );

					$avatarimg = get_avatar( get_current_user_id(), 32 );
					echo $twig->render('avatar/avatar-overlay.html',[
						"avatarimg"=>$avatarimg,
						"user"=>$current_user,
						"profilurl"=>$profilurl,
						"display"=> $display,
						"training"=> $training,
						"tlektion" => reset($tlektion),
                        "logouturl" => wp_logout_url( home_url() ),
                        'hideMails' => isset(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)['hide_mails']),
						]);
				}
			}
		}
		echo ob_get_clean();
	}

}

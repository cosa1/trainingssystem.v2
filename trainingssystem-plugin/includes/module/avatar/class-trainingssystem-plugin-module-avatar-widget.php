<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 */
class Trainingssystem_Plugin_Module_Avatar_Widget extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		$widget_ops = array( 
			'classname' => TRAININGSSYSTEM_PLUGIN_WIDGET_PREFIX.'_avatar',
			'description' => TRAININGSSYSTEM_PLUGIN_WIDGET_PREFIX.' Avatar',
		);
		parent::__construct(  TRAININGSSYSTEM_PLUGIN_WIDGET_PREFIX.'_avatar', TRAININGSSYSTEM_PLUGIN_WIDGET_PREFIX.' Avatar', $widget_ops );
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		// outputs the content of the widget

		$twig = Trainingssystem_Plugin_Twig::getInstance()->twig;

		ob_start();

		$current_user = wp_get_current_user();
		if (0 == $current_user->ID) {
			return $twig->render('modal-unauth.html', ["title" => get_bloginfo('name'), "url" => get_site_url(), "modalid" => uniqid()]);
		} else {



			$profilurl = "";
			$settings = get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS);
            if(isset($settings["page_user_profil"])){
				$profilurl = get_permalink($settings["page_user_profil"]);
			}
                       

			$avatarimg = get_avatar( get_current_user_id(), 32 );
			echo $twig->render('avatar/avatar-widget.html',[
				"avatarimg"=>$avatarimg,
				"user"=>$current_user,
				"profilurl"=>$profilurl,
				'hideMails' => isset(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)['hide_mails']),
				]);
			
		}
		echo ob_get_clean();
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		// outputs the options form on admin
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {

	}
}

<?php
/**
 * Studienübersicht - zeigt Nutzende mit zugewiesenen Training, zeitlich und abhängig zuzuweisenden Trainings 
 * gruppiert nach Studiengruppen-ID und ggf. nach unterschiedlichen Varianten
 * 
 * @package    Studienerweiterung
 * @author     Max Sternitzke <max.sternitzke@th-luebeck.de>
 */
class Trainingssystem_Plugin_Module_Study_Overview
{

    public function __construct(){}

    public function showStudyOverview($atts)
    {

        $heading = isset($atts['titel']) ? $atts['titel'] : 'Studienübersicht'; 
       
        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
        if (Trainingssystem_Plugin_Module_Berechtigung::getInstance()->accessAllowed("studyoverview")) {

            $current_user = wp_get_current_user();
            if (0 == $current_user->ID) {
			    return $twig->render('modal-unauth.html', ["title" => get_bloginfo('name'), "url" => get_site_url(), "modalid" => uniqid()]);
            } else {

                $studygroup_users = get_users(array("meta_key" => "studiengruppenid"));

                $studygroupuserids = [];
                foreach($studygroup_users as $user) {
                    $studygroupuserids[] = $user->ID;
                }

                $userWithTrainings = (count($studygroupuserids) > 0) ? Trainingssystem_Plugin_Database::getInstance()->NutzerDao->getMyNutzer(false, $studygroupuserids) : array();
                $usersDateTrainings = (count($studygroupuserids) > 0) ? Trainingssystem_Plugin_Database::getInstance()->TrainingDate->getTrainingDateByUsers($studygroupuserids) : array();
                $usersDepTrainings = (count($studygroupuserids) > 0) ? Trainingssystem_Plugin_Database::getInstance()->TrainingDep->getTrainingDepByUsers($studygroupuserids) : array();

                $studygroups = array();
                
                foreach($studygroup_users as $user) {
                    if(!empty($user->studiengruppenid)) {
                        if(!isset($studygroups[$user->studiengruppenid])) {
                            $studygroups[$user->studiengruppenid] = array();
                            $studygroups[$user->studiengruppenid]['variants'] = array();
                            $studygroups[$user->studiengruppenid]['variants'][0] = array();
                            $studygroups[$user->studiengruppenid]['variants'][0]['user'] = array();
                            $studygroups[$user->studiengruppenid]['variants'][0]['user'][] = $user;
                            
                            if(isset($userWithTrainings[$user->ID])) {
                                $studygroups[$user->studiengruppenid]['variants'][0]['trainings'] = $userWithTrainings[$user->ID]->getTrainings();
                            } else {
                                $studygroups[$user->studiengruppenid]['variants'][0]['trainings'] = array();
                            }
                            
                            if(isset($usersDateTrainings[$user->ID])) {
                                $studygroups[$user->studiengruppenid]['variants'][0]['datetrainings'] = $usersDateTrainings[$user->ID];
                            } else {
                                $studygroups[$user->studiengruppenid]['variants'][0]['datetrainings'] = array();
                            }
                            
                            if(isset($usersDepTrainings[$user->ID])) {
                                $studygroups[$user->studiengruppenid]['variants'][0]['deptrainings'] = $usersDepTrainings[$user->ID];;
                            } else {
                                $studygroups[$user->studiengruppenid]['variants'][0]['deptrainings'] = array();
                            }

                            $studygroups[$user->studiengruppenid]['studygroup_name'] = $user->studiengruppenid;
                        } else {
                            
                            $userTrainingsString = "";
                            if(isset($userWithTrainings[$user->ID])) {
                                foreach($userWithTrainings[$user->ID]->getTrainings() as $t) {
                                    $userTrainingsString .= $t->toJson();
                                }
                            }
                            if(isset($usersDateTrainings[$user->ID])) {
                                foreach($usersDateTrainings[$user->ID] as $t_date) {
                                    $userTrainingsString .= $t_date->toJson(true);
                                }
                            }
                            if(isset($usersDepTrainings[$user->ID])) {
                                foreach($usersDepTrainings[$user->ID] as $t_dep) {
                                    $userTrainingsString .= $t_dep->toJson(true);
                                }
                            }
                            
                            $foundVariant = false;
                            foreach($studygroups[$user->studiengruppenid]['variants'] as $var_key => $var) {
                                $varTrainingString = "";
                                foreach($var['trainings'] as $t) {
                                    $varTrainingString .= $t->toJson();
                                }
                                foreach($var['datetrainings'] as $t_date) {
                                    $varTrainingString .= $t_date->toJson(true);
                                }
                                foreach($var['deptrainings'] as $t_dep) {
                                    $varTrainingString .= $t_dep->toJson(true);
                                }

                                if(strcmp(trim($userTrainingsString), trim($varTrainingString)) === 0) { // existing variant found, append user

                                    $studygroups[$user->studiengruppenid]['variants'][$var_key]['user'][] = $user;
                                    $foundVariant = true;
                                    break;

                                } 
                            }

                            if(!$foundVariant) { // new variant

                                $variantIndex = count($studygroups[$user->studiengruppenid]['variants']);

                                $studygroups[$user->studiengruppenid]['variants'][$variantIndex] = array();

                                $studygroups[$user->studiengruppenid]['variants'][$variantIndex]['user'] = array();
                                $studygroups[$user->studiengruppenid]['variants'][$variantIndex]['user'][] = $user;
                                
                                if(isset($userWithTrainings[$user->ID])) {
                                    $studygroups[$user->studiengruppenid]['variants'][$variantIndex]['trainings'] = $userWithTrainings[$user->ID]->getTrainings();
                                } else {
                                    $studygroups[$user->studiengruppenid]['variants'][$variantIndex]['trainings'] = array();
                                }
                                
                                if(isset($usersDateTrainings[$user->ID])) {
                                    $studygroups[$user->studiengruppenid]['variants'][$variantIndex]['datetrainings'] = $usersDateTrainings[$user->ID];
                                } else {
                                    $studygroups[$user->studiengruppenid]['variants'][$variantIndex]['datetrainings'] = array();
                                }
                                
                                if(isset($usersDepTrainings[$user->ID])) {
                                    $studygroups[$user->studiengruppenid]['variants'][$variantIndex]['deptrainings'] = $usersDepTrainings[$user->ID];;
                                } else {
                                    $studygroups[$user->studiengruppenid]['variants'][$variantIndex]['deptrainings'] = array();
                                }
                            }
                        }
                    }
                }

                ksort($studygroups); // Nach Studiengruppen-Namen aufsteigend sortieren

                $singlevariants = true;
                foreach($studygroups as $sg) {
                    if(count($sg['variants']) > 1) {
                        $singlevariants = false;
                    }
                }
                
                return $twig->render('study-list/study-overview.html', [
                                                                'heading' => $heading,
                                                                'studygroups' => $studygroups,
                                                                'alltrainings' => $this->getAllTrainingsIndexed(),
                                                                'singlevariants' => $singlevariants,
                                                            ]);
            }
        } else {
            return $twig->render('modal-unauth.html', ["title" => get_bloginfo('name'), "url" => get_site_url(), "modalid" => uniqid()]);
        }
    }

    /**
	 * HELPER
	 * 
	 * Returns all Trainings with training-ID as array index
	 * 
	 * @return Array
	 */
	private function getAllTrainingsIndexed() {
		$alltrainings = [];
		$alltrainings_tmp=Trainingssystem_Plugin_Database::getInstance()->TrainingDao->getAllTrainings();
		foreach($alltrainings_tmp as $at) {
			$alltrainings[$at->getId()] = $at;
		}
		return $alltrainings;
	}

}



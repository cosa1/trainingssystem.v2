<?php

class Trainingssystem_Plugin_Module_Inactive_Logout
{
    public function __construct()
    {
    }

    public function inactiveLogoutModal($content)
    {
        $twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
        $current_user = wp_get_current_user();
        return $content . $twig->render('login-logout/inactive-logout-modal.html', ["userlogged" => (0 == $current_user->ID) ? 0 : 1, 'home'=> get_site_url()]);
    }
}

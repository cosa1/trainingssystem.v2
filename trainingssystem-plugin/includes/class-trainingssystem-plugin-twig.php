<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Markus Domin, Helge Nissen <->
 */
class Trainingssystem_Plugin_Twig {

	public $twig;
	protected static $_instance = null;

	public static function getInstance()
   {
       if (null === self::$_instance)
       {
           self::$_instance = new self;
       }
       return self::$_instance;
   }

   /**
    * clone
    *
    * Kopieren der Instanz von aussen ebenfalls verbieten
    */
   protected function __clone() {}

	/**
	 * Initialize the collections used to maintain the actions and filters.
	 *
	 * @since    1.0.0
	 */
	protected function __construct() {

		// Specify our Twig templates location
		$loader = new \Twig\Loader\FilesystemLoader(plugin_dir_path( dirname( __FILE__ ) ).'templates/'.TRAININGSSYSTEM_PLUGIN_TEMPLATE);

		 // Instantiate our Twig
		$this->twig = $twig = new \Twig\Environment($loader);



	}

}

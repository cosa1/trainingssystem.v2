<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 */
class Trainingssystem_Plugin_Hooks_Define_Admin
{


    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      Trainingssystem_Plugin_Loader    $loader    Maintains and registers all hooks for the plugin.
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $plugin_name    The string used to uniquely identify this plugin.
     */
    protected $plugin_name;

    /**
     * The current version of the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $version    The current version of the plugin.
     */
    protected $version;

    public function __construct($loader, $name, $version)
    {
        $this->loader = $loader;
        $this->name = $name;
        $this->version = $version;
    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   public
     */
    public function define_hooks()
    {

        $plugin_admin = new Trainingssystem_Plugin_Admin($this->name, $this->version);

        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');
        
        //neu

        $this->loader->add_filter("user_can_richedit", $plugin_admin, 'disableVisualEditor');

        $trainingssystem_Plugin_Module_User_Remover_Admin = new Trainingssystem_Plugin_Module_User_Remover_Admin();
        $this->loader->add_action('delete_user', $trainingssystem_Plugin_Module_User_Remover_Admin, 'user_delete_content');
        $this->loader->add_action('delete_user', $trainingssystem_Plugin_Module_User_Remover_Admin, 'user_reassign_content');

        $trainingssystem_Plugin_Module_User_Meta_Fields = new Trainingssystem_Plugin_Module_User_Meta_Fields();

        $this->loader->add_action('edit_user_profile', $trainingssystem_Plugin_Module_User_Meta_Fields, 'coach_usermeta_form_field_studienid'); // add the field to user profile editing screen
        $this->loader->add_action('show_user_profile', $trainingssystem_Plugin_Module_User_Meta_Fields, 'coach_usermeta_form_field_studienid');
        $this->loader->add_action('user_new_form', $trainingssystem_Plugin_Module_User_Meta_Fields, 'coach_usermeta_form_field_studienid');
        $this->loader->add_action('personal_options_update', $trainingssystem_Plugin_Module_User_Meta_Fields, 'coach_usermeta_form_field_studienid_update'); // add the save action to user's own profile editing screen update
        $this->loader->add_action('edit_user_profile_update', $trainingssystem_Plugin_Module_User_Meta_Fields, 'coach_usermeta_form_field_studienid_update'); // add the save action to user profile editing screen update
        $this->loader->add_action('user_register', $trainingssystem_Plugin_Module_User_Meta_Fields, 'coach_usermeta_form_field_studienid_update');

        $this->loader->add_action('edit_user_profile', $trainingssystem_Plugin_Module_User_Meta_Fields, 'extendUserMetaAge'); // add the field to user profile editing screen
        $this->loader->add_action('show_user_profile', $trainingssystem_Plugin_Module_User_Meta_Fields, 'extendUserMetaAge');
        $this->loader->add_action('user_new_form', $trainingssystem_Plugin_Module_User_Meta_Fields, 'extendUserMetaAge');
        $this->loader->add_action('personal_options_update', $trainingssystem_Plugin_Module_User_Meta_Fields, 'extendUserMetaAge_Update'); // add the save action to user's own profile editing screen update
        $this->loader->add_action('edit_user_profile_update', $trainingssystem_Plugin_Module_User_Meta_Fields, 'extendUserMetaAge_Update'); // add the save action to user profile editing screen update
        $this->loader->add_action('user_register', $trainingssystem_Plugin_Module_User_Meta_Fields, 'extendUserMetaAge_Update');


        $this->loader->add_action('edit_user_profile', $trainingssystem_Plugin_Module_User_Meta_Fields, 'extendUserMetaGender'); // add the field to user profile editing screen
        $this->loader->add_action('show_user_profile', $trainingssystem_Plugin_Module_User_Meta_Fields, 'extendUserMetaGender');
        $this->loader->add_action('user_new_form', $trainingssystem_Plugin_Module_User_Meta_Fields, 'extendUserMetaGender');
        $this->loader->add_action('personal_options_update', $trainingssystem_Plugin_Module_User_Meta_Fields, 'extendUserMetaGender_Update'); // add the save action to user's own profile editing screen update
        $this->loader->add_action('edit_user_profile_update', $trainingssystem_Plugin_Module_User_Meta_Fields, 'extendUserMetaGender_Update'); // add the save action to user profile editing screen update
        $this->loader->add_action('user_register', $trainingssystem_Plugin_Module_User_Meta_Fields, 'extendUserMetaGender_Update');


        $this->loader->add_action('edit_user_profile', $trainingssystem_Plugin_Module_User_Meta_Fields, 'extendUserMetaPlace'); // add the field to user profile editing screen
        $this->loader->add_action('show_user_profile', $trainingssystem_Plugin_Module_User_Meta_Fields, 'extendUserMetaPlace');
        $this->loader->add_action('user_new_form', $trainingssystem_Plugin_Module_User_Meta_Fields, 'extendUserMetaPlace');
        $this->loader->add_action('personal_options_update', $trainingssystem_Plugin_Module_User_Meta_Fields, 'extendUserMetaPlace_Update'); // add the save action to user's own profile editing screen update
        $this->loader->add_action('edit_user_profile_update', $trainingssystem_Plugin_Module_User_Meta_Fields, 'extendUserMetaPlace_Update'); // add the save action to user profile editing screen update
        $this->loader->add_action('user_register', $trainingssystem_Plugin_Module_User_Meta_Fields, 'extendUserMetaPlace_Update');

        $this->loader->add_action('edit_user_profile', $trainingssystem_Plugin_Module_User_Meta_Fields, 'coach_usermeta_form_field_coachid');
        $this->loader->add_action('show_user_profile', $trainingssystem_Plugin_Module_User_Meta_Fields, 'coach_usermeta_form_field_coachid');
        $this->loader->add_action('user_new_form', $trainingssystem_Plugin_Module_User_Meta_Fields, 'coach_usermeta_form_field_coachid');
        $this->loader->add_action('personal_options_update', $trainingssystem_Plugin_Module_User_Meta_Fields, 'coach_usermeta_form_field_coachid_update');
        $this->loader->add_action('edit_user_profile_update', $trainingssystem_Plugin_Module_User_Meta_Fields, 'coach_usermeta_form_field_coachid_update');
        $this->loader->add_action('user_register', $trainingssystem_Plugin_Module_User_Meta_Fields, 'coach_usermeta_form_field_coachid_update');

        $trainingssystem_Plugin_Module_Exercise = new Trainingssystem_Plugin_Module_Exercise();
        $this->loader->add_action('add_meta_boxes', $trainingssystem_Plugin_Module_Exercise, 'add_exercise_meta_box');
        $this->loader->add_action('wp_ajax_admin_backend_save_exercise_options', $trainingssystem_Plugin_Module_Exercise, 'saveExerciseOptions');
        $this->loader->add_filter('views_edit-uebungen', $trainingssystem_Plugin_Module_Exercise, 'backendAddImportAction');
        $this->loader->add_filter('manage_uebungen_posts_columns', $trainingssystem_Plugin_Module_Exercise, 'exercise_manage_columns');
        $this->loader->add_filter('manage_uebungen_posts_custom_column', $trainingssystem_Plugin_Module_Exercise, 'exercise_manage_custom_columns', 10, 2);
        $this->loader->add_filter('manage_edit-uebungen_sortable_columns', $trainingssystem_Plugin_Module_Exercise, 'exercise_manage_sortable_columns');
        $this->loader->add_action('pre_get_posts', $trainingssystem_Plugin_Module_Exercise, 'exercise_custom_search', 9);
        $this->loader->add_action('wp_ajax_backend_exercise_get_pages', $trainingssystem_Plugin_Module_Exercise, 'getPagesWithExercise');
        $this->loader->add_action('deleted_post', $trainingssystem_Plugin_Module_Exercise, 'backendDeleteExercise');
        $this->loader->add_action('wp_ajax_backend_exercise_export', $trainingssystem_Plugin_Module_Exercise, 'backendExportExercise');
        $this->loader->add_action('wp_ajax_backend_exercise_duplicate', $trainingssystem_Plugin_Module_Exercise, 'backendDuplicateExercise');
        $this->loader->add_action('wp_ajax_backend_exercise_import', $trainingssystem_Plugin_Module_Exercise, 'backendImportExercise');
        $this->loader->add_filter('post_row_actions', $trainingssystem_Plugin_Module_Exercise, 'backendAddExportAction', 10, 2);

        $trainingssystem_Plugin_Module_TS_Forms = new Trainingssystem_Plugin_Module_TS_Forms();
        $this->loader->add_action('add_meta_boxes', $trainingssystem_Plugin_Module_TS_Forms, 'add_formular_meta_box');
        $this->loader->add_action('wp_ajax_backend_forms_save_options', $trainingssystem_Plugin_Module_TS_Forms, 'saveOptions');
        $this->loader->add_action('wp_ajax_backend_forms_get_pages', $trainingssystem_Plugin_Module_TS_Forms, 'getPagesWithForm');
        $this->loader->add_action('wp_ajax_backend_forms_new_field', $trainingssystem_Plugin_Module_TS_Forms, 'newField');
        $this->loader->add_action('wp_ajax_backend_forms_edit_field', $trainingssystem_Plugin_Module_TS_Forms, 'editField');
        $this->loader->add_action('wp_ajax_backend_forms_duplicate_field', $trainingssystem_Plugin_Module_TS_Forms, 'duplicateField');
        $this->loader->add_action('wp_ajax_backend_forms_update_field', $trainingssystem_Plugin_Module_TS_Forms, 'updateField');
        $this->loader->add_action('wp_ajax_backend_forms_assemble_modal', $trainingssystem_Plugin_Module_TS_Forms, 'getAssembleModal');
        $this->loader->add_action('wp_ajax_backend_forms_get_fields', $trainingssystem_Plugin_Module_TS_Forms, 'getFormFields');
        $this->loader->add_action('wp_ajax_backend_forms_save_order', $trainingssystem_Plugin_Module_TS_Forms, 'saveOrder');
        $this->loader->add_action('wp_ajax_backend_forms_delete_field', $trainingssystem_Plugin_Module_TS_Forms, 'deleteField');
        $this->loader->add_action('wp_ajax_backend_forms_get_preview', $trainingssystem_Plugin_Module_TS_Forms, 'getFormPreview');
        $this->loader->add_action('wp_ajax_backend_forms_export', $trainingssystem_Plugin_Module_TS_Forms, 'backendExportForm');
        $this->loader->add_action('wp_ajax_backend_forms_export_bulk', $trainingssystem_Plugin_Module_TS_Forms, 'backendExportBulk');
        $this->loader->add_action('wp_ajax_backend_forms_duplicate', $trainingssystem_Plugin_Module_TS_Forms, 'backendDuplicateForm');
        $this->loader->add_action('wp_ajax_backend_forms_import', $trainingssystem_Plugin_Module_TS_Forms, 'backendImportForm');
        $this->loader->add_action('wp_ajax_backend_forms_delete_data', $trainingssystem_Plugin_Module_TS_Forms, 'backendDeleteData');
        $this->loader->add_action('wp_ajax_forms_get_multiform_append', $trainingssystem_Plugin_Module_TS_Forms, 'getMultiformAppend');
        $this->loader->add_action('deleted_post', $trainingssystem_Plugin_Module_TS_Forms, 'backendDeleteForm');
        $this->loader->add_filter('post_row_actions', $trainingssystem_Plugin_Module_TS_Forms, 'backendAddExportAction', 10, 2);
        $this->loader->add_filter('bulk_actions-edit-formulare', $trainingssystem_Plugin_Module_TS_Forms, 'backendAddBulkExportAction');
        $this->loader->add_filter('views_edit-formulare', $trainingssystem_Plugin_Module_TS_Forms, 'backendAddImportAction');
        $this->loader->add_filter('manage_formulare_posts_columns', $trainingssystem_Plugin_Module_TS_Forms, 'tsforms_manage_columns');
        $this->loader->add_filter('manage_formulare_posts_custom_column', $trainingssystem_Plugin_Module_TS_Forms, 'tsforms_manage_custom_columns', 10, 2);
        $this->loader->add_filter('manage_edit-formulare_sortable_columns', $trainingssystem_Plugin_Module_TS_Forms, 'tsforms_manage_sortable_columns');
        $this->loader->add_action('pre_get_posts', $trainingssystem_Plugin_Module_TS_Forms, 'tsforms_custom_search', 9);

        $trainingssystem_Plugin_Module_Forms_Media_Grid = new Trainingssystem_Plugin_Module_Forms_Media_Grid();
        $this->loader->add_action('wp_ajax_media_grid_download_file', $trainingssystem_Plugin_Module_Forms_Media_Grid, 'downloadFile');
        $this->loader->add_action('wp_ajax_media_grid_details', $trainingssystem_Plugin_Module_Forms_Media_Grid, 'getDetails');
        $this->loader->add_action('wp_ajax_media_grid_delete', $trainingssystem_Plugin_Module_Forms_Media_Grid, 'deleteFile');
        $this->loader->add_action('wp_ajax_media_grid_delete_project', $trainingssystem_Plugin_Module_Forms_Media_Grid, 'deleteProject');

        $trainingssystem_Plugin_Module_Backend_Trainings_Mgr = new Trainingssystem_Plugin_Module_Backend_Trainings_Mgr();
        $this->loader->add_action('add_meta_boxes', $trainingssystem_Plugin_Module_Backend_Trainings_Mgr, 'add_training_mgr_meta_box');
        $this->loader->add_action('wp_ajax_backend_trainings_mgr_gettraining_lektionen', $trainingssystem_Plugin_Module_Backend_Trainings_Mgr, 'backend_trainings_mgr_gettraining_lektionen');
        $this->loader->add_action('wp_ajax_backend_trainings_mgr_add_lektionen', $trainingssystem_Plugin_Module_Backend_Trainings_Mgr, 'backend_trainings_mgr_add_lektionen');
        $this->loader->add_action('wp_ajax_backend_trainings_mgr_delete_lektion', $trainingssystem_Plugin_Module_Backend_Trainings_Mgr, 'backend_trainings_mgr_delete_lektion');
        $this->loader->add_action('wp_ajax_backend_trainings_mgr_gettraining_seiten', $trainingssystem_Plugin_Module_Backend_Trainings_Mgr, 'backend_trainings_mgr_gettraining_seiten');
        $this->loader->add_action('wp_ajax_backend_trainings_mgr_delete_seite', $trainingssystem_Plugin_Module_Backend_Trainings_Mgr, 'backend_trainings_mgr_delete_seite');
        $this->loader->add_action('wp_ajax_backend_trainings_mgr_save_training', $trainingssystem_Plugin_Module_Backend_Trainings_Mgr, 'backend_trainings_mgr_save_training');
        $this->loader->add_action('wp_ajax_backend_trainings_mgr_save_options', $trainingssystem_Plugin_Module_Backend_Trainings_Mgr, 'backend_trainings_mgr_save_options');
        $this->loader->add_action('deleted_post', $trainingssystem_Plugin_Module_Backend_Trainings_Mgr, 'backend_trainings_mgr_trash_post');

        //training nutzer zuweisung
        $trainingssystem_Plugin_Module_Coach_User_Training_Mgr_Public = new Trainingssystem_Plugin_Module_Coach_User_Training_Mgr_Public();
        $this->loader->add_action('wp_ajax_coach_user_training_mgr_gettraining', $trainingssystem_Plugin_Module_Coach_User_Training_Mgr_Public, 'coach_user_training_mgr_gettraining');
        $this->loader->add_action('wp_ajax_coach_user_training_mgr_gettraining_lektion', $trainingssystem_Plugin_Module_Coach_User_Training_Mgr_Public, 'coach_user_training_mgr_gettraining_lektion');
        $this->loader->add_action('wp_ajax_coach_user_training_mgr_reset_users_training', $trainingssystem_Plugin_Module_Coach_User_Training_Mgr_Public, 'coach_user_training_mgr_reset_users_training');
        $this->loader->add_action('wp_ajax_coach_user_training_mgr_save_users_training', $trainingssystem_Plugin_Module_Coach_User_Training_Mgr_Public, 'coach_user_training_mgr_save_users_training');
        $this->loader->add_action('wp_ajax_coach_user_training_mgr_gettraining_user', $trainingssystem_Plugin_Module_Coach_User_Training_Mgr_Public, 'coach_user_training_mgr_gettraining_user');
        $this->loader->add_action('wp_ajax_coach_user_training_mgr_create_datedep', $trainingssystem_Plugin_Module_Coach_User_Training_Mgr_Public, 'coach_user_training_mgr_create_datedep');
        $this->loader->add_action('wp_ajax_coach_user_training_mgr_edit_datedep', $trainingssystem_Plugin_Module_Coach_User_Training_Mgr_Public, 'coach_user_training_mgr_edit_datedep');
        $this->loader->add_action('wp_ajax_coach_user_training_mgr_change_datedep', $trainingssystem_Plugin_Module_Coach_User_Training_Mgr_Public, 'coach_user_training_mgr_change_datedep');
        $this->loader->add_action('wp_ajax_backend_manually_assign_trainings', $trainingssystem_Plugin_Module_Coach_User_Training_Mgr_Public, 'assignTrainingsDate');
        $this->loader->add_action('wp_ajax_backend_assign_training_date_now', $trainingssystem_Plugin_Module_Coach_User_Training_Mgr_Public, 'assignTrainingDateNow');

        // Insert/update/display/delete company - hooks call the methods on Trainingssystem_Plugin_Module_Company
        // defined in includes\module\company\class-trainingssystem-plugin-module-company.php
        $trainingssystem_Plugin_Module_Company = new Trainingssystem_Plugin_Module_Company();
        $this->loader->add_action('wp_ajax_company_mgr_add_company', $trainingssystem_Plugin_Module_Company, 'company_mgr_add_company');
        $this->loader->add_action('wp_ajax_company_mgr_update_company', $trainingssystem_Plugin_Module_Company, 'company_mgr_update_company');
        $this->loader->add_action('wp_ajax_company_mgr_getgroups_company', $trainingssystem_Plugin_Module_Company, 'company_mgr_getgroups_company');
        $this->loader->add_action('wp_ajax_company_mgr_delete_company', $trainingssystem_Plugin_Module_Company, 'company_mgr_delete_company');
        $this->loader->add_action('wp_ajax_company_mgr_create_group', $trainingssystem_Plugin_Module_Company, 'company_mgr_create_group');
        $this->loader->add_action('wp_ajax_company_mgr_delete_group', $trainingssystem_Plugin_Module_Company, 'company_mgr_delete_group');
        $this->loader->add_action('wp_ajax_company_mgr_remove_groupmember', $trainingssystem_Plugin_Module_Company, 'company_mgr_remove_groupmember');
        $this->loader->add_action('wp_ajax_company_mgr_add_groupmember', $trainingssystem_Plugin_Module_Company, 'company_mgr_add_groupmember');
        $this->loader->add_action('wp_ajax_company_mgr_add_teamleader', $trainingssystem_Plugin_Module_Company, 'company_mgr_add_teamleader');
        $this->loader->add_action('wp_ajax_coach_download_csv', $trainingssystem_Plugin_Module_Company, 'download_csv');
        
        $Trainingssystem_Plugin_Module_Trainings_Ex_Import = new Trainingssystem_Plugin_Module_Trainings_Ex_Import();
        $this->loader->add_action('wp_ajax_trainings_ex_import_download_medien', $Trainingssystem_Plugin_Module_Trainings_Ex_Import, 'downloadMedien');
        $this->loader->add_action('wp_ajax_trainings_ex_import_import_medien', $Trainingssystem_Plugin_Module_Trainings_Ex_Import, 'importMedien');
        $this->loader->add_action('wp_ajax_trainings_ex_import_download_training', $Trainingssystem_Plugin_Module_Trainings_Ex_Import, 'downloadTraining');
        $this->loader->add_action('wp_ajax_trainings_ex_import_import_training', $Trainingssystem_Plugin_Module_Trainings_Ex_Import, 'importTraining');
        $this->loader->add_action('wp_ajax_trainings_ex_import_duplicate_training', $Trainingssystem_Plugin_Module_Trainings_Ex_Import, 'duplicateTraining');

        $trainingssystem_Plugin_Module_Vorlagen_User_Mgr_Public = new Trainingssystem_Plugin_Module_Vorlagen_User_Mgr_Public();
        $this->loader->add_action('wp_ajax_vorlagen_user_adduser', $trainingssystem_Plugin_Module_Vorlagen_User_Mgr_Public, 'addUser');
        $this->loader->add_action('wp_ajax_vorlagen_user_getvorlagenuser', $trainingssystem_Plugin_Module_Vorlagen_User_Mgr_Public, 'getVorlagenUser');
        $this->loader->add_action('wp_ajax_vorlagen_user_edit_modal', $trainingssystem_Plugin_Module_Vorlagen_User_Mgr_Public, 'getVorlagenUserEditModal');
        $this->loader->add_action('wp_ajax_vorlagen_user_update', $trainingssystem_Plugin_Module_Vorlagen_User_Mgr_Public, 'updateUser');
        $this->loader->add_action('wp_ajax_vorlagen_user_delete', $trainingssystem_Plugin_Module_Vorlagen_User_Mgr_Public, 'deleteUser');

        $trainingssystem_Plugin_Module_Register_Key = new Trainingssystem_Plugin_Module_Register_Key();
        $this->loader->add_action('wp_ajax_registerkey_add', $trainingssystem_Plugin_Module_Register_Key, 'addRegisterkey');
        $this->loader->add_action('wp_ajax_registerkey_delete', $trainingssystem_Plugin_Module_Register_Key, 'deleteRegisterkey');
        $this->loader->add_action('wp_ajax_registerkey_update', $trainingssystem_Plugin_Module_Register_Key, 'updateRegisterkey');
        $this->loader->add_action('wp_ajax_registerkey_vorlage_update', $trainingssystem_Plugin_Module_Register_Key, 'updateVorlage');

        $trainingssystem_Plugin_Module_User_List = new Trainingssystem_Plugin_Module_User_List();
        $this->loader->add_action('wp_ajax_user_update_details', $trainingssystem_Plugin_Module_User_List, 'saveUserDetail');
        $this->loader->add_action('wp_ajax_user_details_delete', $trainingssystem_Plugin_Module_User_List, 'userDelete');
        $this->loader->add_action('wp_ajax_user_details_resend_registration', $trainingssystem_Plugin_Module_User_List, 'resendRegistration');

        $trainingssystem_Plugin_Module_User_Remover_Public = new Trainingssystem_Plugin_Module_User_Remover_Public();
        $this->loader->add_action('wp_ajax_user_profil_save_mail',  $trainingssystem_Plugin_Module_User_Remover_Public, 'saveMail');
        $this->loader->add_action('wp_ajax_user_profil_save_password',  $trainingssystem_Plugin_Module_User_Remover_Public, 'savePassword');
        $this->loader->add_action('wp_ajax_user_profil_delete_content',  $trainingssystem_Plugin_Module_User_Remover_Public, 'deleteContent');
        $this->loader->add_action('wp_ajax_user_profil_delete_account',  $trainingssystem_Plugin_Module_User_Remover_Public, 'deleteAccount');
		$this->loader->add_action('wp_ajax_user_profil_save_name',  $trainingssystem_Plugin_Module_User_Remover_Public, 'saveName');
        $this->loader->add_action('wp_ajax_user_profil_save_phonenumber',  $trainingssystem_Plugin_Module_User_Remover_Public, 'savePhonenumber');
        $this->loader->add_action('wp_ajax_user_profil_save_privacy',  $trainingssystem_Plugin_Module_User_Remover_Public, 'savePrivacy');


        $trainingssystem_Plugin_Module_User_Create = new Trainingssystem_Plugin_Module_User_Create();
        $this->loader->add_action('wp_ajax_create_user_manually', $trainingssystem_Plugin_Module_User_Create, 'create_user_manually');
        $this->loader->add_action('user_register', $trainingssystem_Plugin_Module_User_Create, 'saveUserMetaCreatedBy');

        $trainingssystem_Plugin_Module_Register_Key = new Trainingssystem_Plugin_Module_Register_Key();
        $this->loader->add_action('wp_ajax_user_redeem_registerkey', $trainingssystem_Plugin_Module_Register_Key, 'userRedeemRegisterkey');

		$trainingssystem_Plugin_Module_Company_Forward_Content = new Trainingssystem_Plugin_Module_Company_Forward_Content();
		$this->loader->add_action('wp_ajax_forward_content_saveforwardcontentid', $trainingssystem_Plugin_Module_Company_Forward_Content, 'saveForwardContentId');
		$this->loader->add_action('wp_ajax_forward_content_resetforwardcontentid', $trainingssystem_Plugin_Module_Company_Forward_Content, 'resetForwardContentId');
		
        // Creates the TS Settings page
        $trainingssystem__settings_page = new Trainingssystem_Plugin_Module_Settingspage();
        $trainingssystem_Plugin_Module_Backend_Admin_Menu = new Trainingssystem_Plugin_Module_Backend_Admin_Menu();
        $this->loader->add_action('admin_bar_init', $trainingssystem_Plugin_Module_Backend_Admin_Menu, 'my_admin_bar_init');
        $this->loader->add_action('admin_menu', $trainingssystem_Plugin_Module_Backend_Admin_Menu, 'md_trainingsmgr_admin_register_menu');
        $this->loader->add_action('wp_ajax_saveSettings', $trainingssystem__settings_page, 'saveSettings');
        $this->loader->add_action('wp_ajax_delete_demouser', $trainingssystem__settings_page, 'deleteDemoUser');
        $this->loader->add_action('admin_footer_text', $trainingssystem_Plugin_Module_Backend_Admin_Menu, 'adminFooterText');

        $trainingssystem_Plugin_Module_Demodata = new Trainingssystem_Plugin_Module_Demodata();
        $this->loader->add_action('wp_ajax_backend_admin_create', $trainingssystem_Plugin_Module_Demodata, 'backendAdminCreate');

        $trainingssystem_Plugin_Module_Check_Theme = new Trainingssystem_Plugin_Module_Check_Theme();
        $this->loader->add_action('admin_notices', $trainingssystem_Plugin_Module_Check_Theme, 'checktheme');

        $trainingssystem_Plugin_Module_Logger = new Trainingssystem_Plugin_Module_Logger();
        $this->loader->add_action('wp_ajax_page_view_update_duration', $trainingssystem_Plugin_Module_Logger, 'page_view_update_duration');

       
        $trainingssystem_Plugin_Module_Bewertung = new Trainingssystem_Plugin_Module_Bewertung();
        $this->loader->add_action('wp_ajax_page_view_rating_save', $trainingssystem_Plugin_Module_Bewertung, 'page_view_rating_save');

        $trainingssystem_Plugin_Module_Mailbox = new Trainingssystem_Plugin_Module_Mailbox();
        $this->loader->add_action('wp_ajax_mailbox', $trainingssystem_Plugin_Module_Mailbox, 'mailbox_io');
        $this->loader->add_action('wp_ajax_nopriv_mailbox',  $trainingssystem_Plugin_Module_Mailbox, 'mailbox_io');


        $trainingssystem_Plugin_Module_Frontend_Menu = new Trainingssystem_Plugin_Module_Frontend_Menu();
        $this->loader->add_action('admin_head-nav-menus.php',  $trainingssystem_Plugin_Module_Frontend_Menu, 'menueditor_register_menu_metabox');
        $this->loader->add_action('wp_nav_menu_item_custom_fields', $trainingssystem_Plugin_Module_Frontend_Menu, 'adminBackendMenuItemCustomEvents', 10, 2);
        $this->loader->add_action('wp_update_nav_menu_item', $trainingssystem_Plugin_Module_Frontend_Menu, 'adminBackendMenuItemsSave', 20, 2);

        $trainingssystem_Plugin_Module_Backend_Help_Top = new Trainingssystem_Plugin_Module_Backend_Help_Top();
        $this->loader->add_action('admin_head',  $trainingssystem_Plugin_Module_Backend_Help_Top, 'add_context_menu_help_comments');

        $trainingssystem_Plugin_Module_Addon_Patch = new Trainingssystem_Plugin_Module_Addon_Patch();
        $this->loader->add_action('wp_ajax_admin_backend_patch_file',  $trainingssystem_Plugin_Module_Addon_Patch, 'patchFile');

        $trainingssystem_Plugin_Module_Berechtigung = Trainingssystem_Plugin_Module_Berechtigung::getInstance();
        $this->loader->add_action('wp_ajax_admin_backend_permissions_new_role',  $trainingssystem_Plugin_Module_Berechtigung, 'newRole');
        $this->loader->add_action('wp_ajax_admin_backend_permissions_delete_role',  $trainingssystem_Plugin_Module_Berechtigung, 'deleteRole');
        $this->loader->add_action('wp_ajax_admin_backend_permissions_update_role',  $trainingssystem_Plugin_Module_Berechtigung, 'updateRole');
        $this->loader->add_action('wp_ajax_admin_backend_permissions_edit_role',  $trainingssystem_Plugin_Module_Berechtigung, 'editRole');
        $this->loader->add_action('wp_ajax_admin_backend_permissions_table',  $trainingssystem_Plugin_Module_Berechtigung, 'getTableContent');
        $this->loader->add_action('wp_ajax_admin_backend_permissions_save',  $trainingssystem_Plugin_Module_Berechtigung, 'savePermissions');
        $this->loader->add_action('wp_ajax_admin_backend_permissions_default',  $trainingssystem_Plugin_Module_Berechtigung, 'permissionsDefault');
        $this->loader->add_action('wp_ajax_admin_backend_permissions_update',  $trainingssystem_Plugin_Module_Berechtigung, 'updateFunctions');
        $this->loader->add_action('wp_ajax_admin_backend_permissions_migrate',  $trainingssystem_Plugin_Module_Berechtigung, 'migratePermissions');
		
		$trainingssystem_Plugin_Module_Systemstatistiken = new Trainingssystem_Plugin_Module_Systemstatistics();
        $this->loader->add_action('wp_ajax_systemstatistiken_reset',  $trainingssystem_Plugin_Module_Systemstatistiken, 'reset');
		$this->loader->add_action('wp_ajax_systemstatistiken_settimeperiod',  $trainingssystem_Plugin_Module_Systemstatistiken, 'setTimePeriod');
		$this->loader->add_action('wp_ajax_systemstatistiken_generateloginhistory',  $trainingssystem_Plugin_Module_Systemstatistiken, 'generateLoginHistory');
		$this->loader->add_action('wp_ajax_systemstatistiken_setrangeofvaluescountpage',  $trainingssystem_Plugin_Module_Systemstatistiken, 'setRangeOfValuesCountpage');
		$this->loader->add_action('wp_ajax_systemstatistiken_setrangeofvaluessumpage',  $trainingssystem_Plugin_Module_Systemstatistiken, 'setRangeOfValuesSumpage');
		$this->loader->add_action('wp_ajax_systemstatistiken_exportusermetadata',  $trainingssystem_Plugin_Module_Systemstatistiken, 'exportMetadata');
		
		$trainingssystem_Plugin_Module_Coaching_Overview = new Trainingssystem_Plugin_Module_Coaching_Overview();
		$this->loader->add_action('wp_ajax_coaching_overview_assignCoach',  $trainingssystem_Plugin_Module_Coaching_Overview, 'assignCoach');
		$this->loader->add_action('wp_ajax_coaching_overview_multiAssignCoach',  $trainingssystem_Plugin_Module_Coaching_Overview, 'multiAssignCoach');
        $this->loader->add_action('wp_ajax_coaching_overview_save_coaching_notes',  $trainingssystem_Plugin_Module_Coaching_Overview, 'saveCoachingNotes');
		
		$trainingssystem_Plugin_Module_Coachingmode_Form = new Trainingssystem_Plugin_Module_Coachingmode_Form();
		$this->loader->add_action('wp_ajax_coachingmode_form_saveCoachingMode',  $trainingssystem_Plugin_Module_Coachingmode_Form, 'saveCoachingMode');
		$this->loader->add_action('wp_ajax_coachingmode_form_resetCoachingModeFormMeta',  $trainingssystem_Plugin_Module_Coachingmode_Form, 'resetCoachingModeFormMeta');

        $trainingssystem_Plugin_Module_Study_Export = new Trainingssystem_Plugin_Module_Study_Export();
        $this->loader->add_action('wp_ajax_studyexport', $trainingssystem_Plugin_Module_Study_Export, 'studyexportDownload');

        $trainingssystem_Plugin_Module_DataExport = new Trainingssystem_Plugin_Module_DataExport();
        $this->loader->add_action('wp_ajax_dataexport_export', $trainingssystem_Plugin_Module_DataExport, 'dataexportDownload');
        $this->loader->add_action('wp_ajax_dataexport_forms_filter', $trainingssystem_Plugin_Module_DataExport, 'filterForms');

        $trainingssystem_Plugin_Module_User_Importer = new Trainingssystem_Plugin_Module_User_Importer();
        $this->loader->add_action('wp_ajax_csvimport_test', $trainingssystem_Plugin_Module_User_Importer, 'testCsvFile');
        $this->loader->add_action('wp_ajax_csvimport_import', $trainingssystem_Plugin_Module_User_Importer, 'importCsvFile');

        $trainingssystem_Plugin_Module_Zertifikate = new Trainingssystem_Plugin_Module_Zertifikate();
        $this->loader->add_action('add_meta_boxes', $trainingssystem_Plugin_Module_Zertifikate, 'add_zertifikate_meta_box');
        $this->loader->add_action('wp_ajax_admin_backend_save_zertifikat', $trainingssystem_Plugin_Module_Zertifikate, 'saveZertifikat');
        $this->loader->add_action('deleted_post', $trainingssystem_Plugin_Module_Zertifikate, 'backendDeleteZertifikat');
        $this->loader->add_action('wp_ajax_admin_backend_duplicate_zertifikat', $trainingssystem_Plugin_Module_Zertifikate, 'backendDuplicateZertifikat');
        $this->loader->add_filter('post_row_actions', $trainingssystem_Plugin_Module_Zertifikate, 'backendZertifikatAddActions', 10, 2);

        $trainingssystem_Plugin_Module_User_Event = new Trainingssystem_Plugin_Module_User_Event();
        $this->loader->add_action('wp_ajax_save_settings_user_event', $trainingssystem_Plugin_Module_User_Event, 'saveUserEventSettings');
        $this->loader->add_action('wp_ajax_user_events_delete', $trainingssystem_Plugin_Module_User_Event, 'deleteUserEvents');
        $this->loader->add_action('deleted_post', $trainingssystem_Plugin_Module_User_Event, 'deleteUserEventsForPost');
        
        $trainingssystem_Plugin_Module_Avatar = new Trainingssystem_Plugin_Module_Avatar();
        $this->loader->add_action('add_meta_boxes', $trainingssystem_Plugin_Module_Avatar, 'add_avatar_meta_box');
        $this->loader->add_action('wp_ajax_admin_backend_save_avatar_options', $trainingssystem_Plugin_Module_Avatar, 'saveAvatarOptions');
        $this->loader->add_action('wp_ajax_select_avatar', $trainingssystem_Plugin_Module_Avatar, 'selectAvatar');

        $trainingssystem_Plugin_Module_Abzeichen = new Trainingssystem_Plugin_Module_Abzeichen();
        $this->loader->add_action('add_meta_boxes', $trainingssystem_Plugin_Module_Abzeichen, 'add_abzeichen_meta_box');
        $this->loader->add_action('wp_ajax_admin_backend_abzeichen_save_options', $trainingssystem_Plugin_Module_Abzeichen, 'saveAbzeichenOptions');
    }
}

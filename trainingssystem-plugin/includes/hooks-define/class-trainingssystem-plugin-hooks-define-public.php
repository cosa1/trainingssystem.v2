<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 */
class Trainingssystem_Plugin_Hooks_Define_Public{


	/**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      Trainingssystem_Plugin_Loader    $loader    Maintains and registers all hooks for the plugin.
     */
	protected $loader;

	/**
     * The unique identifier of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $plugin_name    The string used to uniquely identify this plugin.
     */
    protected $plugin_name;

    /**
     * The current version of the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $version    The current version of the plugin.
     */
    protected $version;

	public function __construct($loader,$name,$version) {
		$this->loader = $loader;
		$this->name = $name;
		$this->version = $version;
	}

	 /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   public
     */
    public function define_hooks()
    {

        $plugin_public = new Trainingssystem_Plugin_Public($this->name, $this->version);

        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_styles');
        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_scripts');

        $this->loader->add_action('login_head', $plugin_public, 'enqueue_styles');
        $this->loader->add_action('login_head', $plugin_public, 'enqueue_scripts');

        $this->loader->add_filter('query_vars', $plugin_public, 'add_custom_query_var');

        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'user_trainings', $plugin_public, 'training_user_overview'); // nutzer trainings übersicht anzeigen -> alt->training_user_overview
        /*Kompatible zu v1 */$this->loader->add_shortcode('user_trainings', $plugin_public, 'training_user_overview'); // nutzer trainings übersicht anzeigen -> alt->training_user_overview

        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'training_accordion', $plugin_public, 'training_accordion');

        // lektionsübersicht
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'user_training_lektionen', $plugin_public, 'lektion_overview'); //lektion pro training anzeigen ->alt-> lektion_overview
        $this->loader->add_filter('the_content', $plugin_public, "trainings_add_lektionen_footer_frontend"); //unter trainings seite eingebunden

        // bottom navigation
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'user_training_lektion_bottom_nav', $plugin_public, 'bottom_nav'); //->alt-> bottom_nav
        $this->loader->add_filter('the_content', $plugin_public, "seiten_add_button_footer_frontend"); //button unter die Trainings -> Lektion -> seite einbinden

        // Trainings-Manager
        $trainingssystem_Plugin_Module_Coach_User_Training_Mgr_Public = new Trainingssystem_Plugin_Module_Coach_User_Training_Mgr_Public();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'coach_nutzer', $trainingssystem_Plugin_Module_Coach_User_Training_Mgr_Public, 'coach_nutzer');
        /*Kompatible zu v1 */$this->loader->add_shortcode('coach_nutzer', $trainingssystem_Plugin_Module_Coach_User_Training_Mgr_Public, 'coach_nutzer');

        //ajax calls in admin

        // Accordion
        $trainingssystem_Plugin_Module_Accordion = new Trainingssystem_Plugin_Module_Accordion();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'accordionmd', $trainingssystem_Plugin_Module_Accordion, 'show_accordion');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'accordion', $trainingssystem_Plugin_Module_Accordion, 'show_accordion');
        /*Kompatible zu v1 */$this->loader->add_shortcode('accordionmd', $trainingssystem_Plugin_Module_Accordion, 'show_accordion');


        // $trainingssystem_Plugin_Module_User_Remover_Public = new Trainingssystem_Plugin_Module_User_Remover_Public();
        // $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'coach_edit_user_profile', $trainingssystem_Plugin_Module_User_Remover_Public, 'coach_edit_user_profile');
        //
        //profilseite
        $trainingssystem_Plugin_Module_User_Remover_Public = new Trainingssystem_Plugin_Module_User_Remover_Public();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'user_profil', $trainingssystem_Plugin_Module_User_Remover_Public, 'coach_edit_user_profile'); //alt -> coach_edit_user_profile
        /*Kompatible zu v1 */$this->loader->add_shortcode('user_profil', $trainingssystem_Plugin_Module_User_Remover_Public, 'coach_edit_user_profile'); //alt -> coach_edit_user_profile

		$trainingssystem_Plugin_Module_Copy_Checkbox_List = new Trainingssystem_Plugin_Module_Copy_Checkbox_List();
		$this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'copycheckbox', $trainingssystem_Plugin_Module_Copy_Checkbox_List, 'scCopyCheckboxHandler');
		/*Kompatible zu v1 */$this->loader->add_shortcode('copycheckbox', $trainingssystem_Plugin_Module_Copy_Checkbox_List, 'scCopyCheckboxHandler');

		$trainingssystem_Plugin_Module_Copy_Fieldtext = new Trainingssystem_Plugin_Module_Copy_Fieldtext();
		$this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'copyfieldtext', $trainingssystem_Plugin_Module_Copy_Fieldtext, 'scCopyFieldtext');
		/*Kompatible zu v1 */$this->loader->add_shortcode('copyfieldtext', $trainingssystem_Plugin_Module_Copy_Fieldtext, 'scCopyFieldtext');

		$trainingssystem_Plugin_Module_Copy_Checktable_Items = new Trainingssystem_Plugin_Module_Copy_Checktable_Items();
		$this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'copychecktableitems', $trainingssystem_Plugin_Module_Copy_Checktable_Items, 'scCopyChecktableItems');
		/*Kompatible zu v1 */$this->loader->add_shortcode('copychecktableitems', $trainingssystem_Plugin_Module_Copy_Checktable_Items, 'scCopyChecktableItems');

		// PDF V2:
		$trainingssystem_Plugin_Module_PDFDownload = new Trainingssystem_Plugin_Module_PDFDownload();
		$this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'pdfdownload', $trainingssystem_Plugin_Module_PDFDownload, 'scPDFDownload');
		$this->loader->add_shortcode('pdfdownload', $trainingssystem_Plugin_Module_PDFDownload, 'scPDFDownload');
		$this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'pdfdownloadignorestart', $trainingssystem_Plugin_Module_PDFDownload, 'scPDFDownloadIgnoreStart');
		$this->loader->add_shortcode('pdfdownloadignorestart', $trainingssystem_Plugin_Module_PDFDownload, 'scPDFDownloadIgnoreStart');
		$this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'pdfdownloadignoreend', $trainingssystem_Plugin_Module_PDFDownload, 'scPDFDownloadIgnoreEnd');
		$this->loader->add_shortcode('pdfdownloadignoreend', $trainingssystem_Plugin_Module_PDFDownload, 'scPDFDownloadIgnoreEnd');

		$trainingssystem_Plugin_Module_TS_Timer = new Trainingssystem_Plugin_Module_TS_Timer();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'ts_timer', $trainingssystem_Plugin_Module_TS_Timer, 'scTSTimer');
		$this->loader->add_shortcode('ts_timer', $trainingssystem_Plugin_Module_TS_Timer, 'scTSTimer');

        $trainingssystem_Plugin_Module_Show_Hide = new Trainingssystem_Plugin_Module_Show_Hide();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'quizhn', $trainingssystem_Plugin_Module_Show_Hide, 'show_quiz');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'radiohn', $trainingssystem_Plugin_Module_Show_Hide, 'show_radio');
        /*Kompatible zu v1 */$this->loader->add_shortcode('quizhn', $trainingssystem_Plugin_Module_Show_Hide, 'show_quiz');
        /*Kompatible zu v1 */$this->loader->add_shortcode('radiohn', $trainingssystem_Plugin_Module_Show_Hide, 'show_radio');

        $trainingssystem_Plugin_Module_Progress = new Trainingssystem_Plugin_Module_Progress();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'hnprogress', $trainingssystem_Plugin_Module_Progress, 'get_progress');
        /*Kompatible zu v1 */$this->loader->add_shortcode('hnprogress', $trainingssystem_Plugin_Module_Progress, 'get_progress');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'trainingprogress', $trainingssystem_Plugin_Module_Progress, 'showTrainingProgress');

        $trainingssystem_Plugin_Module_Frontend_Menu = new Trainingssystem_Plugin_Module_Frontend_Menu();
        $this->loader->add_filter('wp_get_nav_menu_items', $trainingssystem_Plugin_Module_Frontend_Menu, 'adminBackendMenuItemsExclude', 10, 2);
        $this->loader->add_filter('wp_get_nav_menu_items', $trainingssystem_Plugin_Module_Frontend_Menu, 'addMenuItems', 20, 2);

        $trainingssystem_Plugin_Module_Vorlagen_User_Mgr_Public = new Trainingssystem_Plugin_Module_Vorlagen_User_Mgr_Public();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'vorlagen-user-grouper', $trainingssystem_Plugin_Module_Vorlagen_User_Mgr_Public, 'vorlagen_user_grouper');

        $trainingssystem_Plugin_Module_User_Create = new Trainingssystem_Plugin_Module_User_Create();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'create_user_manually', $trainingssystem_Plugin_Module_User_Create, 'showCreateUserForm');

        $trainingssystem_Plugin_Module_User_Importer = new Trainingssystem_Plugin_Module_User_Importer();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'csv_import', $trainingssystem_Plugin_Module_User_Importer, 'showCsvImportOverview');

        $Trainingssystem_Plugin_Module_Trainings_Ex_Import = new Trainingssystem_Plugin_Module_Trainings_Ex_Import();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'trainings_ex_import', $Trainingssystem_Plugin_Module_Trainings_Ex_Import, 'showTrainings');

        $trainingssystem_Plugin_Module_User_List = new Trainingssystem_Plugin_Module_User_List();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'user_list', $trainingssystem_Plugin_Module_User_List, 'showuser');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'user_list_detail', $trainingssystem_Plugin_Module_User_List, 'showuserDetail');

        //user grouper
        $trainingssystem_Plugin_Module_Company = new Trainingssystem_Plugin_Module_Company();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'user_grouper', $trainingssystem_Plugin_Module_Company, 'user_grouper');
        /*Kompatible zu v1 */$this->loader->add_shortcode('user_grouper', $trainingssystem_Plugin_Module_Company, 'user_grouper');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'coach_output_gfbg', $trainingssystem_Plugin_Module_Company, 'output_gbu');
        /*Kompatible zu v1 */$this->loader->add_shortcode('coach_output_gbu', $trainingssystem_Plugin_Module_Company, 'output_gbu');

        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'coach_output_gbu_mittelwerte', $trainingssystem_Plugin_Module_Company, 'output_gbu_mittelwerte');

        //altes company system
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'coach_download_csv_link', $trainingssystem_Plugin_Module_Company, 'csv_link');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'coach_output_einzel_beurteilung1', $trainingssystem_Plugin_Module_Company, 'output_einzel_beurteilung1');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'coach_output_einzel_beurteilung2', $trainingssystem_Plugin_Module_Company, 'output_einzel_beurteilung2');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'coach_output_gesamt_haeufigkeit', $trainingssystem_Plugin_Module_Company, 'output_gesamt_haeufigkeit');
		$this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'coach_output_gesamt_haeufigkeit2', $trainingssystem_Plugin_Module_Company, 'output_gesamt_haeufigkeit2');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'coach_output_gesamt_haeufigkeit_gruppen', $trainingssystem_Plugin_Module_Company, 'output_gesamt_haeufigkeit_gruppen');
		$this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'coach_output_gesamt_haeufigkeit_gruppen2', $trainingssystem_Plugin_Module_Company, 'output_gesamt_haeufigkeit_gruppen2');
		$this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'coach_output_tabellenset3', $trainingssystem_Plugin_Module_Company, 'output_tabellenset3');
		$this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'coach_output_tabellenset3b', $trainingssystem_Plugin_Module_Company, 'output_tabellenset3b');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'coach_output_gefaerdungsbeurteilung_text', $trainingssystem_Plugin_Module_Company, 'output_gefaerdungsbeurteilung_text');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'coach_output_mittelwert_gesamt', $trainingssystem_Plugin_Module_Company, 'output_mittelwert_gesamt');

		/*Kompatible zu v1 */ $this->loader->add_shortcode('coach_output_einzel_beurteilung1', $trainingssystem_Plugin_Module_Company, 'output_einzel_beurteilung1');
		/*Kompatible zu v1 */ $this->loader->add_shortcode('coach_output_einzel_beurteilung2', $trainingssystem_Plugin_Module_Company, 'output_einzel_beurteilung2');
		/*Kompatible zu v1 */ $this->loader->add_shortcode('coach_output_gesamt_haeufigkeit', $trainingssystem_Plugin_Module_Company, 'output_gesamt_haeufigkeit');
		/*Kompatible zu v1 */ $this->loader->add_shortcode('coach_output_gesamt_haeufigkeit2', $trainingssystem_Plugin_Module_Company, 'output_gesamt_haeufigkeit2');
		/*Kompatible zu v1 */ $this->loader->add_shortcode('coach_output_gesamt_haeufigkeit_gruppen', $trainingssystem_Plugin_Module_Company, 'output_gesamt_haeufigkeit_gruppen');
		/*Kompatible zu v1 */ $this->loader->add_shortcode('coach_output_gesamt_haeufigkeit_gruppen2', $trainingssystem_Plugin_Module_Company, 'output_gesamt_haeufigkeit_gruppen2');
		/*Kompatible zu v1 */ $this->loader->add_shortcode('coach_output_tabellenset3', $trainingssystem_Plugin_Module_Company, 'output_tabellenset3');
		/*Kompatible zu v1 */ $this->loader->add_shortcode('coach_output_tabellenset3b', $trainingssystem_Plugin_Module_Company, 'output_tabellenset3b');
		/*Kompatible zu v1 */ $this->loader->add_shortcode('coach_output_gefaerdungsbeurteilung_text', $trainingssystem_Plugin_Module_Company, 'output_gefaerdungsbeurteilung_text');
        //ende user grouper

		$trainingssystem_Plugin_Module_Company_Forward_Content = new Trainingssystem_Plugin_Module_Company_Forward_Content();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'coach_output_forward_content', $trainingssystem_Plugin_Module_Company_Forward_Content, 'showForwardContentList');
        /*Kompatible zu v1 */$this->loader->add_shortcode('coach_output_forward_content', $trainingssystem_Plugin_Module_Company_Forward_Content, 'showForwardContentList');
		$this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'forward_content', $trainingssystem_Plugin_Module_Company_Forward_Content, 'showForwardContentForm');
        /*Kompatible zu v1 */$this->loader->add_shortcode('forward_content', $trainingssystem_Plugin_Module_Company_Forward_Content, 'showForwardContentForm');


        $trainingssystem_Plugin_Module_Company_GBU_Chart = new Trainingssystem_Plugin_Module_Company_GBU_Chart();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'coach_output_gbu_chart', $trainingssystem_Plugin_Module_Company_GBU_Chart, 'showGBUChart');
        /*Kompatible zu v1 */$this->loader->add_shortcode('coach_output_gbu_chart', $trainingssystem_Plugin_Module_Company_GBU_Chart, 'showGBUChart');


        $trainingssystem_Plugin_Module_Company_Details = new Trainingssystem_Plugin_Module_Company_Details();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'company_details', $trainingssystem_Plugin_Module_Company_Details, 'showDetails');

        $trainingssystem_Plugin_Module_User_Modus = new Trainingssystem_Plugin_Module_User_Modus();
        $this->loader->add_action('wp_footer', $trainingssystem_Plugin_Module_User_Modus, "showUser");
        $this->loader->add_action('admin_footer', $trainingssystem_Plugin_Module_User_Modus, "showUser");

        $trainingssystem_Plugin_Module_Register_Key = new Trainingssystem_Plugin_Module_Register_Key();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'showregiserkeylist', $trainingssystem_Plugin_Module_Register_Key, 'showRegisterkeyList');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'addregiserkey', $trainingssystem_Plugin_Module_Register_Key, 'showNewRegisterKeyForm');
        $this->loader->add_filter('option_users_can_register', $trainingssystem_Plugin_Module_Register_Key, 'maybeRemoveRegisterLink');

        $trainingssystem_Plugin_Module_Media = new Trainingssystem_Plugin_Module_Media();
        $this->loader->add_filter( 'wp_audio_shortcode', $trainingssystem_Plugin_Module_Media, 'ast_audio_shortcode_enhancer', 10, 5 );


        $trainingssystem_Plugin_Module_Systemstatistics = new Trainingssystem_Plugin_Module_Systemstatistics();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'system_statistics', $trainingssystem_Plugin_Module_Systemstatistics, 'showStatistics');

		$trainingssystem_Plugin_Module_Coaching_Overview = new Trainingssystem_Plugin_Module_Coaching_Overview();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'coaching_overview', $trainingssystem_Plugin_Module_Coaching_Overview, 'showCoachingOverview');

		$trainingssystem_Plugin_Module_Coachingmode_Form = new Trainingssystem_Plugin_Module_Coachingmode_Form();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'coachingmodeform', $trainingssystem_Plugin_Module_Coachingmode_Form, 'showCoachingmodeForm');
		/*Kompatible zu v1 */$this->loader->add_shortcode('coachingmodeform', $trainingssystem_Plugin_Module_Coachingmode_Form, 'showCoachingmodeForm');

        $trainingssystem_Plugin_Module_Logger = new Trainingssystem_Plugin_Module_Logger();
        $this->loader->add_action('wp_head', $trainingssystem_Plugin_Module_Logger, "logPageView");


        $trainingssystem_Plugin_Module_Bewertung = new Trainingssystem_Plugin_Module_Bewertung();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'rate', $trainingssystem_Plugin_Module_Bewertung, 'showRating');

        $trainingssystem_Plugin_Module_Interactive = new Trainingssystem_Plugin_Module_Interactive();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'drag_drop_game', $trainingssystem_Plugin_Module_Interactive, 'showDragDropGame');

        $trainingssystem_Plugin_Module_Evaluate = new Trainingssystem_Plugin_Module_Evaluate();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'drag_drop-categories', $trainingssystem_Plugin_Module_Evaluate, 'showDragDropCategories');

        // mailbox
        $trainingssystem_Plugin_Module_Mailbox = new Trainingssystem_Plugin_Module_Mailbox();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'mailbox', $trainingssystem_Plugin_Module_Mailbox, 'show_mailbox');
        $this->loader->add_filter('wp_nav_menu_items', $trainingssystem_Plugin_Module_Mailbox, 'mailbox_count_bubble_to_nav', 10, 2);


        $trainingssystem_Plugin_Module_Abzeichen = new Trainingssystem_Plugin_Module_Abzeichen();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'abzeichen', $trainingssystem_Plugin_Module_Abzeichen, 'listabzeichen');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'abzeichen_modal', $trainingssystem_Plugin_Module_Abzeichen, 'showAbzeichenModal');

        $trainingssystem_Plugin_Module_Zertifikate = new Trainingssystem_Plugin_Module_Zertifikate();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'zertifikate', $trainingssystem_Plugin_Module_Zertifikate, 'listzertifikate');

        $trainingssystem_Plugin_Module_Userprofil = new Trainingssystem_Plugin_Module_Userprofil();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'userprofil', $trainingssystem_Plugin_Module_Userprofil, 'frontend_avatar_upload_view');
        $this->loader->add_action('wp_head', $trainingssystem_Plugin_Module_Userprofil, "avataroverlay");
        $this->loader->add_filter('get_avatar',$trainingssystem_Plugin_Module_Userprofil,'avatar_filter_callback',1,5);




        $trainingssystem_Plugin_Module_Comments = new Trainingssystem_Plugin_Module_Comments();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'comments_form', $trainingssystem_Plugin_Module_Comments, 'showform');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'comments_list', $trainingssystem_Plugin_Module_Comments, 'showlist');
        $this->loader->add_filter('the_content', $trainingssystem_Plugin_Module_Comments, "comments_footer_frontend");

        $this->loader->add_filter("comments_open", $trainingssystem_Plugin_Module_Comments, 'blockComments');

        $trainingssystem_Plugin_Module_BrowserDetection = new Trainingssystem_Plugin_Module_BrowserDetection();
        $this->loader->add_action('wp_footer',$trainingssystem_Plugin_Module_BrowserDetection, "showBrowserlog");


        $trainingssystem_Plugin_Module_TS_Forms_Output = new Trainingssystem_Plugin_Module_TS_Forms_Output();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'coach_output_format_data', $trainingssystem_Plugin_Module_TS_Forms_Output, 'coach_output_format_data');
        $this->loader->add_shortcode('coach_output_format_data', $trainingssystem_Plugin_Module_TS_Forms_Output, 'coach_output_format_data');

        $trainingssystem_Plugin_Module_TS_Forms = new Trainingssystem_Plugin_Module_TS_Forms();
        $this->loader->add_shortcode('ts_forms', $trainingssystem_Plugin_Module_TS_Forms, 'frontendDisplayForm');
        $this->loader->add_shortcode('ts_forms_submit', $trainingssystem_Plugin_Module_TS_Forms, 'submitButtonManually');
        $this->loader->add_shortcode('ts_forms_conditional', $trainingssystem_Plugin_Module_TS_Forms, 'tsFormsConditional');

        $this->loader->add_shortcode('ts_forms_conditional-inner1', $trainingssystem_Plugin_Module_TS_Forms, 'tsFormsConditional');
        $this->loader->add_shortcode('ts_forms_conditional-inner2', $trainingssystem_Plugin_Module_TS_Forms, 'tsFormsConditional');
        $this->loader->add_shortcode('ts_forms_conditional-inner3', $trainingssystem_Plugin_Module_TS_Forms, 'tsFormsConditional');

        $trainingssystem_Plugin_Module_Forms_Media_Grid = new Trainingssystem_Plugin_Module_Forms_Media_Grid();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'media_grid', $trainingssystem_Plugin_Module_Forms_Media_Grid, 'showMediaGrid');

        $trainingssystem_Plugin_Module_Study_Export = new Trainingssystem_Plugin_Module_Study_Export();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'study_export', $trainingssystem_Plugin_Module_Study_Export, 'showStudyExportPage');

        $trainingssystem_Plugin_Module_Reset_Radios = new Trainingssystem_Plugin_Module_Reset_Radios();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'reset_radios', $trainingssystem_Plugin_Module_Reset_Radios, 'resetRadios');

        $trainingssystem_Plugin_Module_Latest_Posts = new Trainingssystem_Plugin_Module_Latest_Posts();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'latest_posts', $trainingssystem_Plugin_Module_Latest_Posts, 'showLatestPosts');

        $trainingssystem_Plugin_Module_TS_Carousel = new Trainingssystem_Plugin_Module_TS_Carousel();
        $this->loader->add_shortcode('ts_carousel', $trainingssystem_Plugin_Module_TS_Carousel, 'showTSCarousel');

        $trainingssystem_Plugin_Module_Demo_User_Login = new Trainingssystem_Plugin_Module_Demo_Login();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'demo_login', $trainingssystem_Plugin_Module_Demo_User_Login, 'showDemoLogin');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'demo_login_takeover', $trainingssystem_Plugin_Module_Demo_User_Login, 'showDemoLoginTakeoverForm');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'guest', $trainingssystem_Plugin_Module_Demo_User_Login, 'onlyForGuests');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'user', $trainingssystem_Plugin_Module_Demo_User_Login, 'onlyForUser');
        
        $trainingssystem_Plugin_Module_DataExport = new Trainingssystem_Plugin_Module_DataExport();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . 'data_export', $trainingssystem_Plugin_Module_DataExport, 'showExportOverview');

        $trainingssystem_Plugin_Module_Exercise = new Trainingssystem_Plugin_Module_Exercise();
        $this->loader->add_shortcode('ts_exercise', $trainingssystem_Plugin_Module_Exercise, 'showExercise');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . 'exercise_favorites', $trainingssystem_Plugin_Module_Exercise, 'showFavoriteExercises');

        $trainingssystem_Plugin_Module_Contact_Info = new Trainingssystem_Plugin_Module_Contact_Info();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . 'show_contactinfo', $trainingssystem_Plugin_Module_Contact_Info, 'showContactInfo');

        $trainingssystem_Plugin_Module_Checks = new Trainingssystem_Plugin_Module_Checks();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'check_result_chart', $trainingssystem_Plugin_Module_Checks, 'showCheckResultChart');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'check_result_list', $trainingssystem_Plugin_Module_Checks, 'showCheckResultList');

        $trainingssystem_Plugin_Module_todo_list = new Trainingssystem_Plugin_Module_todo_list();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'todo_list', $trainingssystem_Plugin_Module_todo_list, 'todoList');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'system_todo_form', $trainingssystem_Plugin_Module_todo_list, 'systemTodoForm');


        /*Care4Care checks*/
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'check_arbeitsplatz', $trainingssystem_Plugin_Module_Checks, 'show_check_arbeitsplatz');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'check_gesundheit', $trainingssystem_Plugin_Module_Checks, 'show_check_gesundheit');

        /*Jolanda check */
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'check_jolanda', $trainingssystem_Plugin_Module_Checks, 'show_check_jolanda');

        /*digi-exist checks*/
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'checkbplatz', $trainingssystem_Plugin_Module_Checks, 'show_checkbplatz');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'checkb', $trainingssystem_Plugin_Module_Checks, 'show_checkb');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'checklg', $trainingssystem_Plugin_Module_Checks, 'show_checklg');

        /*oncampus checks */
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'check_gesundheitstrainings_oc', $trainingssystem_Plugin_Module_Checks, 'show_check_gesundheitstrainings_oc');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'check_arbeitsplatztrainings_oc', $trainingssystem_Plugin_Module_Checks, 'show_check_arbeitsplatztrainings_oc');

        $trainingssystem_Plugin_Module_App_Data = new Trainingssystem_Plugin_Module_App_Data();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'coach_output_app_data_list', $trainingssystem_Plugin_Module_App_Data, 'coachOutputAppDataList');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'coach_output_app_data_chart', $trainingssystem_Plugin_Module_App_Data, 'coachOutputAppDataChart');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'isleep_output_app_data', $trainingssystem_Plugin_Module_App_Data, 'isleepOutputAppData');

        $trainingssystem_Plugin_Module_Study_Overview = new Trainingssystem_Plugin_Module_Study_Overview();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'study_overview', $trainingssystem_Plugin_Module_Study_Overview, 'showStudyOverview');

        $trainingssystem_Plugin_Module_User_Event = new Trainingssystem_Plugin_Module_User_Event();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'user_last_events', $trainingssystem_Plugin_Module_User_Event, 'showLastEvents');

        $trainingssystem_Plugin_Module_Nearest_Location = new Trainingssystem_Plugin_Module_Nearest_Location();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . 'nearest_location', $trainingssystem_Plugin_Module_Nearest_Location, 'showFrontend');

        $trainingssystem_Plugin_Module_Avatar = new Trainingssystem_Plugin_Module_Avatar();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . 'avatar_overview', $trainingssystem_Plugin_Module_Avatar, 'showAvatarOverview');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . 'my_avatar', $trainingssystem_Plugin_Module_Avatar, 'showMyAvatar');

        $trainingssystem_Plugin_Module_Swiping_Card = new Trainingssystem_Plugin_Module_Swiping_Card();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . 'swiping_card', $trainingssystem_Plugin_Module_Swiping_Card, 'showSwipingCard');

        $trainingssystem_Plugin_Module_Tab_Element = new Trainingssystem_Plugin_Module_Tab_Element();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . 'tab_element', $trainingssystem_Plugin_Module_Tab_Element, 'showTabElement');
        
        $trainingssystem_Plugin_Module_Info_Card_Element = new Trainingssystem_Plugin_Module_Info_Card_Element();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX . 'info_card_element', $trainingssystem_Plugin_Module_Info_Card_Element, 'showInfoCardElement');
    }

}

<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Accordion_Md_Plugin
 * @subpackage Accordion_Md_Plugin/public
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 */
class Trainingssystem_Plugin_Hooks_Define_Overall{

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      Trainingssystem_Plugin_Loader    $loader    Maintains and registers all hooks for the plugin.
     */
    protected $loader;
    
	public function __construct($loader) {
        $this->loader = $loader;
	}

	 /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   public
     */
    public function define_hooks()
    {

        Trainingssystem_Plugin_Database::getInstance();
        $Trainingssystem_Plugin_Module_Custom_Posttyps = new Trainingssystem_Plugin_Module_Custom_Posttyps();
        $this->loader->add_action('init', $Trainingssystem_Plugin_Module_Custom_Posttyps, 'add_custom_post_types', 0);
        $this->loader->add_action( 'admin_init', $Trainingssystem_Plugin_Module_Custom_Posttyps, 'force_post_title_init' );
        $this->loader->add_action( 'edit_form_after_title', $Trainingssystem_Plugin_Module_Custom_Posttyps, 'force_post_title' );

        $trainingssystem_Plugin_Module_Berechtigung = Trainingssystem_Plugin_Module_Berechtigung::getInstance();
        $this->loader->add_filter("the_content", $trainingssystem_Plugin_Module_Berechtigung, 'blockContent', 1, 1);

        $trainingssystem_Plugin_Module_Coach_User_Lastlogin = new Trainingssystem_Plugin_Module_Coach_User_Lastlogin();
        $this->loader->add_action('wp_login', $trainingssystem_Plugin_Module_Coach_User_Lastlogin, 'user_last_login', 10, 2);
        $this->loader->add_action('wp_login_failed', $trainingssystem_Plugin_Module_Coach_User_Lastlogin, 'user_login_failed', 10, 2);
        $this->loader->add_action('wp_logout', $trainingssystem_Plugin_Module_Coach_User_Lastlogin, 'user_logout', 10, 2);

        $trainingssystem_Plugin_Module_Redirect = new Trainingssystem_Plugin_Module_Redirect();
        $this->loader->add_action('admin_init',$trainingssystem_Plugin_Module_Redirect,'hidedashboard');

        $trainingssystem_Plugin_Module_Help = new Trainingssystem_Plugin_Module_Help();
        $this->loader->add_action('after_setup_theme', $trainingssystem_Plugin_Module_Help,'hideadminbaruser');

        $trainingssystem_Plugin_Module_Register_Key = new Trainingssystem_Plugin_Module_Register_Key();
        $this->loader->add_action( 'register_form',$trainingssystem_Plugin_Module_Register_Key, 'crf_registration_form');
        $this->loader->add_action( 'user_register',$trainingssystem_Plugin_Module_Register_Key, 'crf_user_register');
        $this->loader->add_filter( 'registration_errors',$trainingssystem_Plugin_Module_Register_Key ,'crf_registration_errors', 10, 3 );
        $this->loader->add_filter( 'register_url', $trainingssystem_Plugin_Module_Register_Key, 'adjustRegisterUrl');

        $trainingssystem_Plugin_Module_Excerpt = new Trainingssystem_Plugin_Module_Excerpt();
        $this->loader->add_filter('excerpt_more', $trainingssystem_Plugin_Module_Excerpt , 'excerpt_more');
		$this->loader->add_action('edit_form_after_title', $trainingssystem_Plugin_Module_Excerpt, 'excerpt');
		$this->loader->add_action('admin_menu', $trainingssystem_Plugin_Module_Excerpt, 'remove_excerpt_metabox');
		$this->loader->add_filter('wp_trim_excerpt', $trainingssystem_Plugin_Module_Excerpt, 'wp_trim_excerpt', 10, 2);

        $trainingssystem_Plugin_Module_Userprofil = new Trainingssystem_Plugin_Module_Userprofil();
        $this->loader->add_action('wp_ajax_avatar_action', $trainingssystem_Plugin_Module_Userprofil, 'avatar_upload_action_callback');
        $this->loader->add_filter('ajax_query_attachments_args', $trainingssystem_Plugin_Module_Userprofil, 'show_current_user_attachments', 10, 2);
        $this->loader->add_action('widgets_init', $trainingssystem_Plugin_Module_Userprofil, 'register_avatar_widget');

        $limesurveyModule = new Trainingssystem\Module\Limesurvey\LimesurveyModule();
        $this->loader->add_filter('get_limesurvey_status', $limesurveyModule, 'get_limesurvey_status', 10, 2);
        $this->loader->add_action('limesurvey_user', $limesurveyModule, 'limesurvey_user', 10, 2);

        $trainingssystem_Plugin_Module_Logout = new Trainingssystem_Plugin_Module_Logout();
        $this->loader->add_action('wp_ajax_logout_session', $trainingssystem_Plugin_Module_Logout, 'logout_session'); 

        $trainingssystem_Plugin_Module_Demo_Login = new Trainingssystem_Plugin_Module_Demo_Login();
        $this->loader->add_action('wp_ajax_demo_user', $trainingssystem_Plugin_Module_Demo_Login,'demo_user'); 
        $this->loader->add_action('wp_ajax_nopriv_demo_user', $trainingssystem_Plugin_Module_Demo_Login,'demo_user');
        $this->loader->add_action('wp_ajax_demo_user_takeover', $trainingssystem_Plugin_Module_Demo_Login,'accountTakeover');
        $this->loader->add_action('after_password_reset', $trainingssystem_Plugin_Module_Demo_Login, 'passwordChanged', 10, 2);

        $trainingssystem_Plugin_Module_Abzeichen = new Trainingssystem_Plugin_Module_Abzeichen();
        $this->loader->add_action('wp_ajax_check_progress', $trainingssystem_Plugin_Module_Abzeichen,'checkProgress'); 
        
        $trainingssystem_Plugin_Module_User_Modus = new Trainingssystem_Plugin_Module_User_Modus();
        $this->loader->add_action('wp_ajax_user_modus_start', $trainingssystem_Plugin_Module_User_Modus, 'startUserModusById');
        $this->loader->add_action('wp_ajax_user_modus_disable', $trainingssystem_Plugin_Module_User_Modus, 'disableUserModus');

        $trainingssystem_Plugin_Module_Backend_Admin_Menu = new Trainingssystem_Plugin_Module_Backend_Admin_Menu();
        $this->loader->add_action('after_setup_theme', $trainingssystem_Plugin_Module_Backend_Admin_Menu, 'showAdminBar');

        $trainingssystem_Plugin_Module_TS_Forms = new Trainingssystem_Plugin_Module_TS_Forms();
        $this->loader->add_action('wp_ajax_forms_save_data', $trainingssystem_Plugin_Module_TS_Forms, 'saveData');
        $this->loader->add_action('wp_ajax_forms_get_data', $trainingssystem_Plugin_Module_TS_Forms, 'getData');
        $this->loader->add_action('wp_ajax_nopriv_forms_save_data', $trainingssystem_Plugin_Module_TS_Forms, 'saveData');
        $this->loader->add_action('wp_ajax_forms_captcha_reload', $trainingssystem_Plugin_Module_TS_Forms, 'reloadCaptcha');
        $this->loader->add_action('wp_ajax_nopriv_forms_reload_captchaa', $trainingssystem_Plugin_Module_TS_Forms, 'reloadCaptcha');


        $trainingssystem_Plugin_Module_Inactive_Logout = new Trainingssystem_Plugin_Module_Inactive_Logout();
        $this->loader->add_action('the_content', $trainingssystem_Plugin_Module_Inactive_Logout, 'inactiveLogoutModal');

        $trainingssystem_Plugin_Module_Exercise = new Trainingssystem_Plugin_Module_Exercise();
        $this->loader->add_action('wp_ajax_exercise_click_handle', $trainingssystem_Plugin_Module_Exercise, 'handleFavoriteClicked');

        $trainingssystem_Plugin_Module_Dashboard = new Trainingssystem_Plugin_Module_Dashboard();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'dashboard', $trainingssystem_Plugin_Module_Dashboard, 'showDashboard');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'dashboard_item_checks', $trainingssystem_Plugin_Module_Dashboard, 'showCheckDashboardItem');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'dashboard_item_messages', $trainingssystem_Plugin_Module_Dashboard, 'showMessageDashboardItem');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'dashboard_item_favorite_exercises', $trainingssystem_Plugin_Module_Dashboard, 'showFavoriteExerciseDashboardItem');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'dashboard_item_progress', $trainingssystem_Plugin_Module_Dashboard, 'showProgressDashboardItem');
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'dashboard_item_certs', $trainingssystem_Plugin_Module_Dashboard, 'showCertDashboardItem');

        $trainingssystem_Plugin_Module_Evaluate = new Trainingssystem_Plugin_Module_Evaluate();
        $this->loader->add_shortcode(TRAININGSSYSTEM_PLUGIN_SHORTCODE_PREFIX.'evaluate-categories', $trainingssystem_Plugin_Module_Evaluate, 'evaluateCategories');

        $trainingssystem_Plugin_Module_XmlRpc = new Trainingssystem_Plugin_Module_XmlRpc();
        $this->loader->add_action("init", $trainingssystem_Plugin_Module_XmlRpc, 'toggleXmlRpc');

        $trainingssystem_Plugin_Module_Checks = new Trainingssystem_Plugin_Module_Checks();
        $this->loader->add_action('wp_ajax_check_completed', $trainingssystem_Plugin_Module_Checks, 'checkCompleted');
        $this->loader->add_action('wp_ajax_check_saved', $trainingssystem_Plugin_Module_Checks, 'checkSaved');

        $trainingssystem_Plugin_Module_Todo_List = new Trainingssystem_Plugin_Module_Todo_List();
        $this->loader->add_action('wp_ajax_add_todo', $trainingssystem_Plugin_Module_Todo_List,'addTodo');
        $this->loader->add_action('wp_ajax_del_todo', $trainingssystem_Plugin_Module_Todo_List,'delTodo'); 
        $this->loader->add_action('wp_ajax_update_todo', $trainingssystem_Plugin_Module_Todo_List,'updateTodo');

        $trainingssystem_Plugin_Module_User_Importer = new Trainingssystem_Plugin_Module_User_Importer();
        $this->loader->add_filter('sanitize_user', $trainingssystem_Plugin_Module_User_Importer, 'chara_chan_user', 10, 3);

        $trainingssystem_Plugin_Module_Nearest_Location = new Trainingssystem_Plugin_Module_Nearest_Location();
        $this->loader->add_action('wp_ajax_get_nearest_location', $trainingssystem_Plugin_Module_Nearest_Location, 'getNearestLocation');
        $this->loader->add_action('wp_ajax_nopriv_get_nearest_location', $trainingssystem_Plugin_Module_Nearest_Location, 'getNearestLocation');
    }

	

}

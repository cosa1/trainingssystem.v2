<?php
use ZxcvbnPhp\Zxcvbn;


/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Markus Domin, Helge Nissen <->
 */
class Trainingssystem_Plugin_zxcvbn {

	public $zxcvbn;
	protected static $_instance = null;

	public static function getInstance()
   {
       if (null === self::$_instance)
       {
           self::$_instance = new self;
       }
       return self::$_instance;
   }

   /**
    * clone
    *
    * Kopieren der Instanz von aussen ebenfalls verbieten
    */
   protected function __clone() {}

	/**
	 * Initialize the collections used to maintain the actions and filters.
	 *
	 * @since    1.0.0
	 */
	protected function __construct() {
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/module/php-passwordStrength/Matchers/MatchInterface.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/module/php-passwordStrength/Matchers/PasswordMatch.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/module/php-passwordStrength/Matchers/DigitMatch.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/module/php-passwordStrength/Matchers/Bruteforce.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/module/php-passwordStrength/Matchers/YearMatch.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/module/php-passwordStrength/Matchers/SpatialMatch.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/module/php-passwordStrength/Matchers/SequenceMatch.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/module/php-passwordStrength/Matchers/RepeatMatch.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/module/php-passwordStrength/Matchers/DictionaryMatch.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/module/php-passwordStrength/Matchers/L33tMatch.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/module/php-passwordStrength/Matchers/DateMatch.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/module/php-passwordStrength/Matcher.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/module/php-passwordStrength/Searcher.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/module/php-passwordStrength/ScorerInterface.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/module/php-passwordStrength/Scorer.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/module/php-passwordStrength/Zxcvbn.php';

        $this->zxcvbn = new Zxcvbn();



	}

}

<?php

/**
 * Fired during plugin activation
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/includes
 * @author     Markus Domin, Helge Nissen <->
 */
class Trainingssystem_Plugin_Updator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function updating() {
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/database/class-trainingssystem-plugin-database.php';
		$trainingssystem_Plugin_Database = Trainingssystem_Plugin_Database::getInstance();
		$trainingssystem_Plugin_Database->updating();
	}

}

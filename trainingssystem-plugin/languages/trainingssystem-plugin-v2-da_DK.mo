��          t       �       �      �      �      �                8     H  	   Y     c  `   o  �  �     m     ~     �  
   �     �     �     �     �     �  Y   �   Altes Passwort: E-Mail-Adresse ändern: E-Mail-Benachrichtigungen: Mein Profil Neues Passwort wiederholen: Neues Passwort: Passwort ändern Speichern Testausgabe This is a short description of what the plugin does. It's displayed in the WordPress admin area. Project-Id-Version: Trainingssystem v2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-10-08 08:12+0000
PO-Revision-Date: 2021-05-19 10:59+0000
Last-Translator: Helge Nissen
Language-Team: Dansk
Language: da_DK
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.5.2; wp-5.7.2 Gammelt kodeord: Skift e-mail-adresse: E-mail-underretninger: Min profil Gentag nyt kodeord: Nyt kodeord: Skift kodeord Gemme Test output Dette er en kort beskrivelse af, hvad pluginet gør. Det vises i WordPress-adminområdet. 
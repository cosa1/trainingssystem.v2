<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Trainingssystem_Plugin
 * @subpackage Trainingssystem_Plugin/admin
 * @author     Markus Domin, Helge Nissen <->
 */
class Trainingssystem_Plugin_Admin
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {
        wp_enqueue_style($this->plugin_name . '_admin_backend_fontawesome', plugin_dir_url(__FILE__) . '../assets/external/fontawesome-free-5.6.1-web/css/all.min.css');
        wp_enqueue_style($this->plugin_name.'_style', plugin_dir_url( __FILE__ ) .'../assets/css/admin-style.css');
        wp_enqueue_style($this->plugin_name.'_bs-slider', plugin_dir_url(__FILE__) . '../assets/external/bootstrap-4/bootstrap-slider.min.css');

    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        wp_enqueue_script($this->plugin_name . '_jqueryui_js',plugin_dir_url(__FILE__) . '../assets/external/jquery-ui-1.12.1/jquery-ui.min.js', array('jquery'));

        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . '../assets/external/bootstrap-4/bootstrap.min.js', array('jquery'), $this->version, false);
        wp_enqueue_script($this->plugin_name . '_bs-slider', plugin_dir_url(__FILE__) . '../assets/external/bootstrap-4/bootstrap-slider.min.js', array('jquery'), $this->version, false);

        wp_enqueue_script($this->plugin_name . "_script", plugin_dir_url(__FILE__) . '../assets/js/script.js', array('jquery'), $this->version, false);
        $logout_time = 15 * 60 * 1000;
        if(isset(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)['logout_time']) && is_numeric(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)['logout_time'])) {
            $logout_time = absint(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)['logout_time']) * 60 * 1000;
        }
        wp_localize_script($this->plugin_name . "_script", TRAININGSSYSTEM_PLUGIN_AJAX, array(TRAININGSSYSTEM_PLUGIN_AJAX_URL => admin_url('admin-ajax.php'), TRAININGSSYSTEM_PLUGIN_LOGOUT_TIME => $logout_time));

        wp_enqueue_script($this->plugin_name . '_pdf', plugin_dir_url(__FILE__) . '../assets/external/PDFObject/pdfobject.min.js');
		
		wp_enqueue_script($this->plugin_name . '_jspdf',plugin_dir_url(__FILE__) . '../assets/external/jspdf/jspdf.umd.min.js', array('jquery'));

        wp_enqueue_media();
    }

    function disableVisualEditor($default) {
        global $post;
        $relevant_posttypes = array("trainings", "lektionen", "seiten", "uebungen");
        
        if(in_array(get_post_type($post), $relevant_posttypes)) {
            if(isset(get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)['enablevisualeditor']) && get_option(TRAININGSSYSTEM_PLUGIN_SETTINGS)['enablevisualeditor']) {
                return true;
            } else {
                return false;
            }
        }
        return $default;
    }
}

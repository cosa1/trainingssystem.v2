=== Developer Doc ===

==============================================================================
==============================================================================
== Gitignore folder/files ==
# Ignore folder utilities
trainingssystem-plugin/utilities/


==============================================================================
==============================================================================
== Codestyle ==
- cd trainingssystem-plugin/utilities
- Download / Install PHP CodeSniffer using Pear or Composer.
- - git clone https://github.com/squizlabs/PHP_CodeSniffer.git phpcs
- Download / Install the WordPress ruleset for the PHP CodeSniffer.
- - git clone -b master https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards.git wpcs
- config CodeSniffer
- - cd wpcs
- - php bin/phpcs --config-set installed_paths ../wpcs/
- - check -> php bin/phpcs -i -> The installed coding standards are MySource, PEAR, PSR1, PSR12, PSR2, Squiz, Zend, WordPress, WordPress-Core, WordPress-Docs, WordPress-Extra and WordPress-VIP
- Download a copy of the Linter PHPCS package.
- - https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/wiki/Setting-up-WPCS-to-work-in-Atom
- - In Atom, specify the executable path to the PHPCS binary. -> e.g: /Users/markusdomin/Desktop/repo_cosa/trainingssystem.v2/trainingssystem-plugin/utilities/phpcs/bin/phpcs
- - In Atom, define the set of rules you want to apply when it’s evaluating your code. -> e.g. WordPress, WordPress-VIP

==============================================================================
==============================================================================
== Unit Test ==
- change php version to mamp
- - Within the Terminal, run vim ~/.bash_profile
- - Type i and then paste the following at the top of the file:
- - export PATH=/Applications/MAMP/bin/php/php5.4.10/bin:$PATH
- - Hit ESC, Type :wq, and hit Enter
- - In Terminal, run source ~/.bash_profile
- - In Terminal, type in which php again and look for the updated string. If everything was successful, It should output the new path to MAMP PHP install.
- - In case it doesn't output the correct path, try closing the terminal window (exit fully) and open again, it should apply the changes (Restart in short).

nano ~/.bash_profile
export PATH="/Applications/MAMP/Library/bin:$PATH"
$ source ~/.bash_profile

- cd trainingssystem-plugin/utilities -> mkdir wpcli -> cd wpcli
- Install WP-CLI (http://wp-cli.org/#installing)
- - curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
- - php wp-cli.phar --info
- - chmod +x wp-cli.phar
- - sudo mv wp-cli.phar /usr/local/bin/wp
- - wp --info -> test output

- Install PHPUnit (https://phpunit.de/)
- - https://phpunit.de/manual/5.7/en/installation.html
- - cd trainingssystem-plugin/utilities -> mkdir phpunit -> cd phpunit
- - curl -LO https://phar.phpunit.de/phpunit-6.phar
- - chmod +x phpunit-6.phar
- - sudo mv phpunit-6.phar /usr/local/bin/phpunit
- - phpunit --version

- neue wordpress installation / Datenbank anlegen
- - tabellenprefix ändern -> xyzwp_
- - name: wordpressunittest
- - dbname: wordpressunittest
- - cd wp-content/plugins
- - ln -s /Users/markusdomin/Desktop/repo_cosa/trainingssystem.v2/trainingssystem-plugin
- - create wp-tests-config.php (einfach kopie der normalen) change in wp-config.php and wp-tests-config.php -> database localhost to 127.0.0.1
#### einmalig #####
cd trainingssystem-plugin
wp scaffold plugin-tests trainingssystem-plugin
bin/install-wp-tests.sh wordpressunittest_trainingssystem_plugin root root 127.0.0.1 latest

phpunit.xml
<?xml version="1.0"?>
<phpunit
	bootstrap="tests/bootstrap.php"
	backupGlobals="false"
	colors="true"
	convertErrorsToExceptions="true"
	convertNoticesToExceptions="true"
	convertWarningsToExceptions="true"
	>
  #### hinzufügen ######
	<php>
		<ini name="display_errors" value="true"/>
	</php>
  ####################
	<testsuites>
		<testsuite>
			<directory prefix="test-" suffix=".php">./tests/</directory>
			<exclude>./tests/test-sample.php</exclude>
		</testsuite>
	</testsuites>
</phpunit>
###################

terminal -> phpunit

==============================================================================
==============================================================================
== e2e Test ==
- Chrome / Firefox record tests
- - https://chrome.google.com/webstore/detail/katalon-recorder-selenium/ljdobmomdgdljniojadhoplhkpialdid
- - Katalon Recorder (Selenium IDE for Chrome)
- - nach der installation im browser icon (bei mir grünes icon oben rechts) anklicken
- - ein neues fenster öffnet sich
- - klick folder icon öffne html test file in e2e folder
- - play all
- - save html -> kein später wieder eingelesen werden und erweitert werden
- - export as ruby /e2e/ruby/test.rb

- Selenium Testing with CBT and RSpec
- - gem install rspec
- - gem install selenium-webdriver
- - If I manually replace ${receiver} with @driver, then the file runs just fine.
- - #@driver.manage.timeouts.implicit_wait = 30
- - #@verification_errors.should == []
- - rspec e2etests/ruby/test.rb

==============================================================================
==============================================================================
== Code coverage ==
- mit phpuint
- // geht nicht brew install php71-xdebug
- https://dillieodigital.wordpress.com/2015/03/10/quick-tip-enabling-xdebug-in-mamp-for-osx/
- change
/Applications/MAMP/conf/php[version]/php.ini
/Applications/MAMP/bin/php/php[version]/conf/php.ini

In both of these files, go down to the very bottom of the config file, where the [xdebug] section is, and uncomment ( remove the ; ) zend_extension line. In addition, add the following lines:
xdebug.remote_autostart=1
xdebug.remote_enable=1
xdebug.remote_host=localhost
xdebug.remote_port=9000
xdebug.remote_handler=dbgp

to

[xdebug]
zend_extension="/Applications/MAMP/bin/php/php5.4.34/lib/php/extensions/no-debug-non-zts-20100525/xdebug.so"
xdebug.remote_autostart=1
xdebug.remote_enable=1
xdebug.remote_host=localhost
xdebug.remote_port=9000
xdebug.remote_handler=dbgp

Restart MAMP

add buttom phpunit.xml.dist
<filter>
	<whitelist addUncoveredFilesFromWhitelist="true">
		<directory>./</directory>
	</whitelist>
</filter>
</phpunit>


run -> phpunit --coverage-html output/reports/php_codecoverage

==============================================================================
==============================================================================
== Build Release ==
1. Install gulp globally:
If you have previously installed a version of gulp globally, please run npm rm --global gulp to make sure your old version doesn't collide with gulp-cli.

$ npm install --global gulp-cli
2. Install gulp in your project devDependencies:
$ npm install --save-dev gulp
3. Create a gulpfile.js at the root of your project:
var gulp = require('gulp');

gulp.task('default', function() {
  // place code for your default task here
});
4. Run gulp:
$ gulp

change to WordPress
boilerplate -> https://ahmadawais.com/introducing-wpgulp-an-easy-to-use-wordpress-gulp-boilerplate/

==============================================================================
==============================================================================
== translate plugin ==
- wordpress hilfe -> https://developer.wordpress.org/plugins/internationalization/how-to-internationalize-your-plugin/
- Loco Translate plugin instalieren
- - neues Template anlegen
- - neue sprachdatei anlegen -> nicht custom sondern Author
- - sprache bearbeiten
- - im template z.B.: "Testausgabe" -> hinzugügen
- - in der sprach datei de mit "Testausgabe test" hinterlegen
- - übersetzung ausgeben mit -> echo __("Testausgabe", TRAININGSSYSTEM_PLUGIN_SLUG);
- -  angezeigt wird "Testausgabe test"

==============================================================================
==============================================================================
== useful plugins and stuff ==
- https://github.com/peterjohnhunt/wordpress-suite
- php-getters-setters als erweiterung für Atom
- anleitung guttenberg blocks -> https://wisdomplugin.com/build-gutenberg-block-plugin/
- This is a (hopefully) comprehensive list of action hooks available in WordPress version >= 2.1 -> https://codex.wordpress.org/Plugin_API/Action_Reference


##############################################################################
##############################################################################
##############################################################################
##############################################################################
== code samples ==
##############################################################################
##############################################################################
==============================================================================
==============================================================================
== input data ==
- https://developer.wordpress.org/plugins/security/securing-input/
- als bsp.: wird hier der title eingelesen und wieder ausgegeben
- php -----------------------------------------------
$twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
ob_start();

$current_user = wp_get_current_user();
if ( 0 == $current_user->ID ) {
	echo $twig->render('notloggedin.html');
	exit;
}

$title="";

$nonceaction = TRAININGSSYSTEM_PLUGIN_SLUG.'submit';
$noncefield = 'teststring';

if (!isset( $_POST[$noncefield] ) ||
	!isset($_POST['title']) ||
	!wp_verify_nonce($_POST[$noncefield], $nonceaction ) ) {
	 print 'Sorry, your nonce did not verify.';
	 exit;
} else {
	 $title = sanitize_text_field($_POST['title']);
	 echo $twig->render('testview.html', [
			'title'=> $title,
			'submitnonce'=>wp_nonce_field($nonceaction,$noncefield,true,false)] );
}
return ob_get_clean();

- html -----------------------------------------------
<h2>{{title}}</h2>
<form method="post">
<input id="title" type="text" name="title">
<input type="submit" value="Submit">
{{submitnonce | raw}}
</form>


==============================================================================
==============================================================================
== gutenberg block == 4 datein 1xJS 1xCSS(optional) 1xPHP(geht auch ohne, wir nutzen das aber für twig templates) 1xTwig html

########################## JS
(function() {
  var __ = wp.i18n.__; // The __() for internationalization.
  var el = wp.element.createElement; // The wp.element.createElement() function to create elements.
  var registerBlockType = wp.blocks.registerBlockType; // The registerBlockType() to register blocks.
  var RichText = wp.editor.RichText;
  registerBlockType('gb/trainingssystem-block-test1', { // Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
    title: 'Test2', // Block title.
    icon: 'shield-alt', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
    category: 'md-trainingssystem-9872', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
    attributes: {
        images : {
            default: [],
            type:   'array',
        },
        content : {
            default: "kein content",
            type:   'string',
        }
    },
    edit({attributes, setAttributes, className, focus, id}) {
      var content =attributes.content;

         function onChangeContent( newContent ) {
             setAttributes( { content: newContent, images:["img1","img2"]} );
         }

         return el(RichText,
             {
                 tagName: 'p',
                 className: className,
                 onChange: onChangeContent,
                 value: content,
             }
         );
    },
    save({attributes, className}) {
        //gutenberg will save attributes we can use in server-side callback
       return null;
    },
  });
})();

########################## PHP

// muss in der haupt include datei wie folgt geladen werden -> $this->loader->add_action( 'init', $Trainingssystem_Plugin_Module_Block_Test, 'yourgutenberg_block');
class Trainingssystem_Plugin_Module_Block_Test
{
	public function yourgutenberg_block()
	{
			//optional
			wp_register_style('nameof-mystyle', plugins_url('../../../assets/css/blocks/test1.css', __FILE__), array('wp-edit-blocks'), $path.'/css/blocks/test1.css');
			//optional
			wp_register_style('nameof-mystyle-editor', plugins_url('../../../assets/css/blocks/test1.css', __FILE__), array('wp-edit-blocks'), plugins_url('../../../assets/css/blocks/test1.css', __FILE__));

			wp_register_script('name-ofyour-script-js2', plugins_url('../../../assets/js/blocks/test2.js', __FILE__), array('wp-blocks',  'wp-i18n', 'wp-element', 'wp-editor'));

			//name muss exact der gleiche sein wie in dem javascript dokument
			register_block_type('gb/trainingssystem-block-test1', array(
				 'editor_script' => 'name-ofyour-script-js2',
				// 'editor_style' => 'nameof-mystyle-editor', //optional
				// 'style' => 'nameof-mystyle',//optional
				'render_callback' => array($this, 'trainingssystem_block_test1_render_callback'),
				'attributes' => array(
							'content' => array(
									'type' => 'string',
									'default' => 'kein content',
								),
							'images' => array(
									'type' => 'array',
									'default' => [],
										'items' => [
												'type' => 'string', 'integer',
										],
							),
					),
			));
	}

	public function trainingssystem_block_test1_render_callback($attributes)
	{
			if (!empty($attributes)) {
					$twig = Trainingssystem_Plugin_Twig::getInstance()->twig;
					return $twig->render('gutenberg-block-test1.html', [
						'images' => $attributes[ 'images' ],
						'text' => $attributes[ 'content' ],
				]);
			}
			return '<div></div>';
	}
}

########################## html

<h2>test gutenberg mit twig </h2>
{#<div class=bullet-container>
  {% for image in images%}
  <span class=bullet-filled> {{image}} </span>
  {% endfor %}
</div>#}

<div class="info">
  <div class="icon-info"></div>
  <h4>{{text}}</h4>
</div>


==============================================================================
==============================================================================
== xxxx ==
